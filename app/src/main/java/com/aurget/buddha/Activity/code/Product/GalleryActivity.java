package com.aurget.buddha.Activity.code.Product;

import android.content.Intent;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Bundle;


import android.os.Handler;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.code.Adapter.MyPagerAdapter;
import com.aurget.buddha.Activity.code.Dashboard.Slider.CardPagerAdapter;
import com.aurget.buddha.Activity.code.Fragment.BannerAddapter;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;
import com.imagezoom.ImageAttacher;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import code.utils.AppUrls;
import me.relex.circleindicator.CircleIndicator;



public class GalleryActivity extends BaseActivity {

    public static int VB_PAGES;
    public static int VB_LOOPS = 1000;
    public static int VB_FIRST_PAGE;
    public static String regId = "";
    // ViewPager currentpage
    public static int currentPage = 0;
    // VviewPager no offpage
    public static int NUM_PAGES = 0;
    private LinearLayout dotsLayout;
    private ImageView[] dots;
    CardPagerAdapter mCardAdapter;
    MyPagerAdapter adapter;
    CircleIndicator circle;
    //Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();
    PointF startPoint = new PointF();
    PointF midPoint = new PointF();
    float oldDist = 1f;
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;
    public static Timer timer;
    private ScaleGestureDetector scaleGestureDetector;
    private Matrix matrix = new Matrix();
    ImageView ivProductPic;
    ViewPager pager;
    public static String url;
    public static String apiId;
    public static String apiComefrom;
    Intent i = getIntent();
    ArrayList<HashMap<String, String>> viewPagerList = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        ivProductPic = findViewById(R.id.imageView40);
        pager = findViewById(R.id.pager);
        circle = findViewById(R.id.circle);
        ivProductPic.setVisibility(View.GONE);
        pager.setVisibility(View.VISIBLE);
        pager.setPadding(15, 0, 15, 0);

        //getProductDetailApi();
        //Picasso.with(GalleryActivity.this).load(String.valueOf(AppConstants.imageList)).into(ivProductPic);
        /*Picasso.with(GalleryActivity.this).load(GalleryActivity.url).into(ivProductPic);
        usingSimpleImage(ivProductPic);*/

            JSONArray imageArray = null;

            Log.d("parmatma", AppConstants.imageList.toString());
            pager.setAdapter(new ImageAdapter(mActivity, AppConstants.imageList, 1) {
                @Override
                protected void viewClick(View view, String str) {

                }
            });
    }

    private void getProductDetailApi() {
        AppUtils.showRequestDialog(mActivity);
        Log.v("getProductDetailApi", AppUrls.getProductDetail);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("product_id", apiId);
            json_data.put("come_from", apiComefrom);
            json.put(AppConstants.result, json_data);
            Log.v("getProductDetailApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.getProductDetail)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        if (error.getErrorCode() != 0) {
                            // AppUtils.showErrorMessage( tvProductName, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            //  AppUtils.showErrorMessage( tvProductName, String.valueOf( error.getErrorDetail() ), mActivity );
                        }
                    }
                });
    }

    private void parseJsondata(JSONObject response) {
        AppUtils.hideDialog();
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            JSONArray imageArray = jsonObject.getJSONArray("images");
            for (int i = 0; i < imageArray.length(); i++) {
                JSONObject imageobject = imageArray.getJSONObject(i);
                HashMap<String, String> prodList1 = new HashMap();
                prodList1.put("id", imageobject.getString("id"));
                prodList1.put("name", imageobject.getString("name"));
                viewPagerList.add(prodList1);
                pager.setAdapter(new BannerAddapter(mActivity, viewPagerList));
                circle.setViewPager(pager);
            }
            final float density = getResources().getDisplayMetrics().density;

            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == viewPagerList.size()) {
                        currentPage = 0;
                    }
                    pager.setCurrentItem(currentPage++, true);
                }
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 5000, 3000);


            circle.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override

                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override

                public void onPageSelected(int i) {

                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });
        } catch (Exception e) {

        }

        //setValues( 0 );
    }

    public void usingSimpleImage(ImageView imageView) {
        ImageAttacher mAttacher = new ImageAttacher(imageView);
        ImageAttacher.MAX_ZOOM = 4.0f; // times the current Size
        ImageAttacher.MIN_ZOOM = 1.0f; // Half the current Size
        MatrixChangeListener mMaListener = new MatrixChangeListener();
        mAttacher.setOnMatrixChangeListener(mMaListener);
        PhotoTapListener mPhotoTap = new PhotoTapListener();
        mAttacher.setOnPhotoTapListener(mPhotoTap);
    }

    private class PhotoTapListener implements ImageAttacher.OnPhotoTapListener {

        @Override
        public void onPhotoTap(View view, float x, float y) {
        }
    }

    private class MatrixChangeListener implements ImageAttacher.OnMatrixChangedListener {
        @Override
        public void onMatrixChanged(RectF rect) {

        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        // scaleGestureDetector.onTouchEvent(ev);
        return true;
    }

    private class ScaleListener extends ScaleGestureDetector.
            SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            float scaleFactor = detector.getScaleFactor();
            scaleFactor = Math.max(0.1f, Math.min(scaleFactor, 5.0f));
            matrix.setScale(scaleFactor, scaleFactor);
            //ivProductPic.setImageMatrix(matrix);
            return true;
        }
    }

}

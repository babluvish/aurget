package com.aurget.buddha.Activity.MyProfile;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.Utils.CustomPreference;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Shipping_ddress_list_Activity extends AppCompatActivity {

    RecyclerView recycleView;
    TextView newShipingDdressTV;
    Button applyBtn;
    LinearLayout addShippingLl;
    private MyRecyclerAdapter myRecyclerAdapter;
    ArrayList<JSONObject> jsonObjectal = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping_ddress_list_);

        recycleView = findViewById(R.id.recycleView);
        newShipingDdressTV = findViewById(R.id.newShipingDdressTV);
        addShippingLl = findViewById(R.id.addShippingLl);
        applyBtn = findViewById(R.id.applyBtn);

        myRecyclerAdapter = new MyRecyclerAdapter();

        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recycleView.setLayoutManager(horizontalLayoutManager);
        recycleView.setItemAnimator(new DefaultItemAnimator());

        addShippingLl.setOnClickListener(view -> {
            Intent intent = new Intent(Shipping_ddress_list_Activity.this, Shipping_ddress.class);
            startActivity(intent);
        });

        applyBtn.setOnClickListener(view -> {

            if (lastSelectedPosition == -1) {
                Commonhelper.showToastLong(Shipping_ddress_list_Activity.this, "Please select shipping address");
                return;
            }

            Intent intent = new Intent();
            intent.putExtra("name", jsonObjectal.get(lastSelectedPosition).optString("name"));
            intent.putExtra("address", jsonObjectal.get(lastSelectedPosition).optString("address"));
            intent.putExtra("colony_street", jsonObjectal.get(lastSelectedPosition).optString("colony_street"));
            intent.putExtra("city", jsonObjectal.get(lastSelectedPosition).optString("city"));
            intent.putExtra("state", jsonObjectal.get(lastSelectedPosition).optString("state"));
            intent.putExtra("pin_code", jsonObjectal.get(lastSelectedPosition).optString("pin_code"));
            intent.putExtra("country", jsonObjectal.get(lastSelectedPosition).optString("country"));
            intent.putExtra("mobile", jsonObjectal.get(lastSelectedPosition).optString("mobile"));
            intent.putExtra("address_id", jsonObjectal.get(lastSelectedPosition).optString("address_id"));
            intent.putExtra("pos", "Shipping_ddress_list_Activity");
            setResult(101, intent);
            finish();
        });

    }

    void hitPIPinCode() {
        new AbstrctClss(Shipping_ddress_list_Activity.this, ConstntApi.get_address(Shipping_ddress_list_Activity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    JSONObject jsonArray = new JSONObject(Res);

                    if (jsonArray.optJSONArray("data").length() == 0) {
                        Commonhelper.showToastLong(Shipping_ddress_list_Activity.this, Constnt.notFound);
                        return;
                    }

                    jsonObjectal.clear();
                    for (int i = 0; i < jsonArray.optJSONArray("data").length(); i++) {
                        jsonObjectal.add(jsonArray.optJSONArray("data").getJSONObject(i));
                    }
                    recycleView.setAdapter(myRecyclerAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    void deleteData(String address_id, final int pos) {
        new AbstrctClss(Shipping_ddress_list_Activity.this, ConstntApi.delete_shipping_address(Shipping_ddress_list_Activity.this, address_id), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    JSONArray jsonArray = new JSONArray(Res);
                    if (jsonArray.optJSONObject(0).optString("Status").equalsIgnoreCase("true")) {
                        Commonhelper.showToastLong(Shipping_ddress_list_Activity.this, jsonArray.optJSONObject(0).optString("message"));
                        jsonObjectal.remove(pos);
                        myRecyclerAdapter.notifyDataSetChanged();
                    } else {
                        Commonhelper.showToastLong(Shipping_ddress_list_Activity.this, jsonArray.optJSONObject(0).optString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        hitPIPinCode();
    }


    public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

        public MyRecyclerAdapter() {
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.shipping_ddress_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            try {
                holder.nameTv.setText(jsonObjectal.get(position).optString("name"));
                holder.ddressTv.setText(jsonObjectal.get(position).optString("address") +
                        "\n" + jsonObjectal.get(position).optString("colony_street") +
                        "\n" + jsonObjectal.get(position).optString("city") + "  " + jsonObjectal.get(position).optString("state") +
                        jsonObjectal.get(position).optString("pin_code")
                        + "\n" + jsonObjectal.get(position).optString("country")
                        + "\n" + "Phone Number: " + jsonObjectal.get(position).optString("mobile"));

                holder.shippingAddressRb.setChecked(lastSelectedPosition == position);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView ddressTv, nameTv;
            private Button editBtn, delBtn;
            private RadioButton shippingAddressRb;

            public ViewHolder(View itemView) {
                super(itemView);
                ddressTv = itemView.findViewById(R.id.ddressTv);
                nameTv = itemView.findViewById(R.id.nameTv);
                editBtn = itemView.findViewById(R.id.editBtn);
                delBtn = itemView.findViewById(R.id.delBtn);
                shippingAddressRb = itemView.findViewById(R.id.shippingAddressRb);

                if (getIntent().hasExtra("SubCategoryDetails2")) {
                    itemView.findViewById(R.id.bottomRl).setVisibility(View.GONE);
                    shippingAddressRb.setVisibility(View.VISIBLE);
                } else {
                    applyBtn.setVisibility(View.GONE);
                }

                if (getIntent().hasExtra("RaiseReturn")) {
                    itemView.findViewById(R.id.bottomRl).setVisibility(View.GONE);
                    shippingAddressRb.setVisibility(View.VISIBLE);
                    applyBtn.setVisibility(View.VISIBLE);
                }

                shippingAddressRb.setOnClickListener(v -> {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                    address_id = jsonObjectal.get(lastSelectedPosition).optString("address_id");

                    CustomPreference.writeString(Shipping_ddress_list_Activity.this,CustomPreference.shippingName,nameTv.getText().toString());
                    CustomPreference.writeString(Shipping_ddress_list_Activity.this,CustomPreference.shippingAddress,ddressTv.getText().toString());
                    CustomPreference.writeString(Shipping_ddress_list_Activity.this,CustomPreference.getShippingAddressID,address_id);

                    //Commonhelper.showToastLong(Shipping_ddress_list_Activity.this,"done");
                });

                delBtn.setOnClickListener(v -> {
                    lastSelectedPosition = getAdapterPosition();
                    //notifyDataSetChanged();
                    address_id = jsonObjectal.get(lastSelectedPosition).optString("address_id");
                    deleteData(address_id, lastSelectedPosition);
                    //Commonhelper.showToastLong(Shipping_ddress_list_Activity.this,"done");
                });

                editBtn.setOnClickListener(view -> {
                    Intent intent = new Intent(Shipping_ddress_list_Activity.this, Shipping_ddress.class);
                    intent.putExtra("name", jsonObjectal.get(getAdapterPosition()).optString("name"));
                    intent.putExtra("address", jsonObjectal.get(getAdapterPosition()).optString("address"));
                    intent.putExtra("address_id", jsonObjectal.get(getAdapterPosition()).optString("address_id"));
                    intent.putExtra("colony_street", jsonObjectal.get(getAdapterPosition()).optString("colony_street"));
                    intent.putExtra("city", jsonObjectal.get(getAdapterPosition()).optString("city"));
                    intent.putExtra("state", jsonObjectal.get(getAdapterPosition()).optString("state"));
                    intent.putExtra("pin_code", jsonObjectal.get(getAdapterPosition()).optString("pin_code"));
                    intent.putExtra("country", jsonObjectal.get(getAdapterPosition()).optString("country"));
                    intent.putExtra("mobile", jsonObjectal.get(getAdapterPosition()).optString("mobile"));
                    intent.putExtra("pos", "Shipping_ddress_list_Activity");
                    startActivity(intent);
                });
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("address_id", address_id);
        setResult(101, intent);
        super.onBackPressed();
    }

    private String address_id = "";
    int lastSelectedPosition = -1;
}

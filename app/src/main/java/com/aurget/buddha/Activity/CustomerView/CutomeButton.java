package com.aurget.buddha.Activity.CustomerView;

import android.content.Context;
import android.graphics.Color;

import androidx.appcompat.widget.AppCompatButton;
import android.util.AttributeSet;

public class CutomeButton extends AppCompatButton {

    private static int color= Color.RED;

    public CutomeButton(Context context)
    {
        super(context);
        setFont();
        //this.setTextColor(color);
    }

    public CutomeButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setFont();
        //this.setTextColor(color);
    }

    public CutomeButton(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        setFont();
        //this.setTextColor(color);
    }

    public static void setGlobalColor(int gcolor)
    {
        color=gcolor;

    }

    private void setFont() {
        /*Typeface typeface = Typeface.createFromAsset(getContext().getAssets(),font);
        setTypeface(typeface); //function used to set font*/
    }
}

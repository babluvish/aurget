package code.Common;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;

@SuppressLint({"ParcelCreator"})
public class CustomTypefaceSpan extends TypefaceSpan {
    private int newColor;
    private Typeface newType;

    private void applyCustomTypeFace(android.graphics.Paint r1, android.graphics.Typeface r2, int r3) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.DecodeException: Load method exception in method: code.Common.CustomTypefaceSpan.applyCustomTypeFace(android.graphics.Paint, android.graphics.Typeface, int):void
	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:116)
	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:249)
	at jadx.core.ProcessClass.process(ProcessClass.java:34)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
Caused by: jadx.core.utils.exceptions.DecodeException: Unknown instruction: not-int
	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:568)
	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:56)
	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:102)
	... 5 more
*/
        /*
        // Can't load method instructions.
        */
        throw new UnsupportedOperationException("Method not decompiled: code.Common.CustomTypefaceSpan.applyCustomTypeFace(android.graphics.Paint, android.graphics.Typeface, int):void");
    }

    public CustomTypefaceSpan(String family, Typeface type, int color) {
        super(family);
        this.newType = type;
        this.newColor = color;
    }

    public CustomTypefaceSpan(String family, Typeface type) {
        super(family);
        this.newType = type;
    }

    public void updateDrawState(TextPaint ds) {
        applyCustomTypeFace(ds, this.newType, this.newColor);
    }

    public void updateMeasureState(TextPaint paint) {
        applyCustomTypeFace(paint, this.newType, this.newColor);
    }
}

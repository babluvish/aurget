package com.aurget.buddha.Activity.code.Main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.code.Account.AccountActivity;
import com.aurget.buddha.Activity.code.Common.RoundedImageView;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import code.utils.AppUrls;

public class ChangePassword extends BaseActivity implements View.OnClickListener {

    //RelativeLayout
    RelativeLayout rlSubmit,rlBack;

    //Edittext
    EditText etOldpassword,etNewpassword,etConfirmpassword;

    //Textview
    TextView tvPasswordInstruction,tvPasswordInstruction1,tvPasswordInstruction2;

    //Imageview
    RoundedImageView ivLogo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        //RelativeLayout
        rlSubmit           = findViewById(R.id.rlSubmit);
        rlBack             =findViewById(R.id.rlBack);


        //Edittext
        etNewpassword      =findViewById(R.id.etNewPassword);
        etOldpassword      =findViewById(R.id.etOldPassword);
        etConfirmpassword  =findViewById(R.id.etConfirmPassword);


        //Textview
        tvPasswordInstruction=findViewById(R.id.tvPasswordInstruction);
        tvPasswordInstruction1=findViewById(R.id.tvPasswordInstruction1);
        tvPasswordInstruction2=findViewById(R.id.tvPasswordInstruction2);


        //Imageview
        ivLogo=findViewById(R.id.ivLogo);


        etOldpassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean hasfocus) {
                if (hasfocus) {

                } else {
                    tvPasswordInstruction.setVisibility(View.GONE);
                }
            }
        });


        etNewpassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean hasfocus) {
                if (hasfocus) {
                    tvPasswordInstruction1.setVisibility(View.VISIBLE);
                } else {
                    tvPasswordInstruction1.setVisibility(View.GONE);
                }
            }
        });

        etConfirmpassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean hasfocus) {
                if (hasfocus) {
                    tvPasswordInstruction2.setVisibility(View.VISIBLE);
                } else {
                    tvPasswordInstruction2.setVisibility(View.GONE);
                }
            }
        });

        //SetOnClickListener
        rlSubmit.setOnClickListener(this);
        rlBack.setOnClickListener(this);
        etOldpassword.setOnClickListener(this);
        ivLogo.setOnClickListener(this);



    }



    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.rlBack:
                finish();
                break;

            case R.id.ivLogo:
                finish();
                break;

            case R.id.etOldPassword:
                tvPasswordInstruction.setVisibility(View.VISIBLE);
                break;
            case R.id.rlSubmit:
                tvPasswordInstruction.setVisibility(View.GONE);
                tvPasswordInstruction1.setVisibility(View.GONE);
                tvPasswordInstruction2.setVisibility(View.GONE);
                if (etOldpassword.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please enter your old password", Toast.LENGTH_SHORT).show();
                }
                /*else if (!isValidPassword(etOldpassword.getText().toString().trim()))
                {
                    Toast.makeText(this, "Please enter valid old password", Toast.LENGTH_SHORT).show();
                }
                else if (etOldpassword.getText().toString().length()<7)
                {
                    Toast.makeText(this, "Please enter minimum 7 digit old password", Toast.LENGTH_SHORT).show();
                }*/
                else if (etNewpassword.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please enter your new password", Toast.LENGTH_SHORT).show();
                }
               /* else if (etOldpassword.getText().toString().length()<7)
                {
                    Toast.makeText(this, "Please enter minimum 7 digit new password", Toast.LENGTH_SHORT).show();
                }
                else if (!isValidPassword(etNewpassword.getText().toString().trim()))
                {
                    Toast.makeText(this, "Please enter valid new password", Toast.LENGTH_SHORT).show();
                }*/
                else if (etConfirmpassword.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please enter your confirm password", Toast.LENGTH_SHORT).show();

                }
              /*  else if (etOldpassword.getText().toString().length()<7)
                {
                    Toast.makeText(this, "Please enter minimum 7 digit confirm new  password", Toast.LENGTH_SHORT).show();
                }*/
               /* else if (!isValidPassword(etConfirmpassword.getText().toString().trim()))
                {
                    Toast.makeText(this, "Please enter valid confirm password", Toast.LENGTH_SHORT).show();
                }*/
                else if (etConfirmpassword.getText().toString().equals(etNewpassword.getText().toString())) {

                    if (SimpleHTTPConnection.isNetworkAvailable(this.mActivity))
                    {
                        ChangePasswordApi();
                    }
                    else
                    {
                        Toast.makeText(this, getString(R.string.error_connection_message), Toast.LENGTH_SHORT).show();
                        //AppUtils.showErrorMessage(etEmail, getString(R.string.error_connection_message), this.mActivity);
                    }

                }
                else {
                    Toast.makeText(getApplicationContext(), "Please enter confirm password correct", Toast.LENGTH_SHORT).show();

                }

                break;
        }
    }



    //check vaildation for password
    public boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    //=======================================API==============================================//
    private  void ChangePasswordApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("ChangePassword", AppUrls.ChangePassword);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json_data.put("old_password", etOldpassword.getText().toString());
            json_data.put("new_password", etNewpassword.getText().toString());
            json_data.put("confirm_password", etConfirmpassword.getText().toString());
            json.put(AppConstants.result, json_data);
            Log.v("LoginApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.ChangePassword)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsonForgot(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(etNewpassword, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(etNewpassword, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private  void parseJsonForgot(JSONObject response) {

        Log.d("response ", response.toString());
        AppUtils.hideDialog();
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                Toast.makeText(this, jsonObject.getString("res_msg"), Toast.LENGTH_LONG).show();
                Intent i=new Intent(this, AccountActivity.class);
                startActivity(i);
            }
            else
            {
                AppUtils.showErrorMessage(etNewpassword, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(etNewpassword, String.valueOf(e), mActivity);
        }

    }
}

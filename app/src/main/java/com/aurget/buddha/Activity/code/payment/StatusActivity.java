package com.aurget.buddha.Activity.code.payment;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;

public class StatusActivity extends BaseActivity implements View.OnClickListener{

	//RelativeLayout
	RelativeLayout rlBack,rlButton;

	//TextView
	TextView txtView;

	ImageView imageView19;

	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_status);

		getId();

	}

	private void getId() {

		rlBack =   findViewById(R.id.rrBack);
		rlButton = findViewById(R.id.Ok);

		/*Intent mainIntent = getIntent();
		AppSettings.putString(AppSettings.status,mainIntent.getStringExtra("transStatus"));*/

		txtView =  findViewById(R.id.log);

		imageView19 = findViewById(R.id.imageView19);

		/*Animation anim = AnimationUtils.loadAnimation(this, R.anim.slide_in_top);
		anim.setInterpolator((new AccelerateDecelerateInterpolator()));
		anim.setFillAfter(true);
		imageView19.setAnimation(anim);

		rlButton.setOnClickListener(this);

		if(!SimpleHTTPConnection.isNetworkAvailable(this)){
			Utils.showCustomToast(this,"Please check your internet connection.",txtView);
		}
		else
		{
			hitPaymentAPI();
		}*/

	}

	public void showToast(String msg) {
		Toast.makeText(this, "Toast: " + msg, Toast.LENGTH_LONG).show();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.rrBack:
				finish();
				break;

			case R.id.Ok:
				//startActivity(new Intent(mActivity, DashBoard.class));
				//finishAffinity();
				break;
			default:break;
		}
	}

	/*private void hitPaymentAPI(){

		AppUtils.showRequestDialog(mActivity);

		String url = AppUrls.paymentSuccess;
		Log.v("hitPaymentAPI-URL", url);

		JSONObject json = new JSONObject();
		JSONObject json_data = new JSONObject();
		try {

			json_data.put("uuid",                            AppSettings.getString(AppSettings.UserId));
			json_data.put("tracking_id",          		AppSettings.getString(AppSettings.transId));
			json_data.put("transaction_id",          AppSettings.getString(AppSettings.orderId));
			json_data.put("status",         					 "1");
			json_data.put("deviceId",                   Utils.getDeviceID(this));

			json.put(AppConstants.newnuk, json_data);

			Log.v("hitPaymentAPI", json.toString());

		} catch (JSONException e) {
			e.printStackTrace();
		}

		AndroidNetworking.post(url)
				.addJSONObjectBody(json)
				.setPriority(Priority.HIGH)
				.build()
				.getAsJSONObject(new JSONObjectRequestListener() {
					@Override
					public void onResponse(JSONObject response) {
						parseJSON(response);
					}

					@Override
					public void onError(ANError error) {
						AppUtils.hideDialog();
						// handle error
						if (error.getErrorCode() != 0) {
							Utils.showCustomToast(mActivity, String.valueOf(error.getErrorCode()), txtView);
							Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
							Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
							Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
						} else {
							Utils.showCustomToast(mActivity, String.valueOf(error.getErrorDetail()), txtView);
						}
					}
				});
	}

	private void parseJSON(JSONObject response){
		Log.v("StateData", String.valueOf(response));

		try {
			if (response.has("newnuk")) {

				JSONObject resobj= response.getJSONObject("newnuk");

				if(resobj.getString("loginStatus").equals("false")){
					int state = AppSettings.getInt(AppSettings.stateCount);
					AppSettings.clearSharedPreference();
					AppSettings.putInt(AppSettings.stateCount,state);
					Utils.showCustomToast(mActivity,"You are logged in with another device.",txtView);
					startActivity(new Intent(mActivity, Login_Activity.class));
					finishAffinity();
				}
				else if (resobj.getString("resMessage").equals("Success")) {
					Utils.showCustomToast(mActivity,resobj.getString("response"),txtView);

				}
				else {
					Utils.showCustomToast(mActivity,resobj.getString("response"),txtView);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
			Toast.makeText(this, "Error: Connection fail try again.", Toast.LENGTH_LONG).show();
		}

		AppUtils.hideDialog();

	}*/
}
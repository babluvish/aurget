package com.aurget.buddha.Activity.Outlet;

import android.content.Intent;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aurget.buddha.Activity.AddtoCart.AddCartListActivity;
import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.EmptyCartActivity;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.Utils.SearchDate;
import com.aurget.buddha.Activity.Utils.SearchMonth;
import com.aurget.buddha.Activity.categories.SubCategoryDetailsActivity;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.aurget.buddha.Activity.Main2Activity.crtDetils2;

public class Outlet_details extends BaseActivity {

    RecyclerView recycleView;
    FloatingActionButton flotBtn;
    TextView allTv;
    TextView dateRnge;
    TextView mothTv;
    TextView crtCountTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outlet_details);

        recycleView = findViewById(R.id.recycleView);

        allTv = findViewById(R.id.allTv);
        dateRnge = findViewById(R.id.dateRnge);
        // findViewById(R.id.bkImg).setVisibility(View.GONE);
        //findViewById(R.id.crtContner).setVisibility(View.GONE);
        mothTv = findViewById(R.id.mothTv);
        crtCountTv = findViewById(R.id.crtCountTv);

        final LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false);
        recycleView.setLayoutManager(horizontalLayoutManager);
        recycleView.setItemAnimator(new DefaultItemAnimator());

        findViewById(R.id.bkImg).setVisibility(View.GONE);
        findViewById(R.id.bkImg2).setVisibility(View.VISIBLE);

        findViewById(R.id.bkImg2).setOnClickListener(v -> finish());

        final EditText searchEt = findViewById(R.id.searchEt);

        searchEt.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus)
            {
                searchEt.setCursorVisible(true);
            }
        });

        searchEt.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                //searchData(searchEt.getText().toString());
                Intent intent = new Intent(mActivity, SubCategoryDetailsActivity.class);
                intent.putExtra("search", searchEt.getText().toString());
                startActivity(intent);
                return true;
            }
            return false;
        });


        allTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateRnge.setBackgroundResource(R.drawable.ovel_shap3);
                allTv.setBackgroundResource(R.drawable.ovel_shape);
                mothTv.setBackgroundResource(R.drawable.ovel_shap3);
            }
        });

        dateRnge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateRnge.setBackgroundResource(R.drawable.ovel_shape);
                allTv.setBackgroundResource(R.drawable.ovel_shap3);
                mothTv.setBackgroundResource(R.drawable.ovel_shap3);

                new SearchDate(Outlet_details.this) {
                    @Override
                    public String fromDtToDt(String frmDate, String toDt) {
                        return null;
                    }
                }.dateRangeDialog();
            }
        });

        mothTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mothTv.setBackgroundResource(R.drawable.ovel_shape);
                allTv.setBackgroundResource(R.drawable.ovel_shap3);
                dateRnge.setBackgroundResource(R.drawable.ovel_shap3);

                new SearchMonth(Outlet_details.this) {
                    @Override
                    public String fromDt(String month__, String year) {
                        return null;
                    }
                }.monthNameDialog();
            }
        });

        findViewById(R.id.imghmb1).setOnClickListener(view -> {
            if (Integer.parseInt(crtCountTv.getText().toString().trim().replace("+","")) < 1) {
                Intent intent = new Intent(mActivity, EmptyCartActivity.class);
                startActivity(intent);
                return;
            }
            Intent intent = new Intent(mActivity, AddCartListActivity.class);
            intent.putExtra("res", crtDetils2);
            startActivity(intent);
        });

        fetchData();
        crtDetils();
    }

    private void DialogContct()
    {
        final BottomSheetDialog dialog = new BottomSheetDialog(Outlet_details.this);
        dialog.setContentView(R.layout.aadhar_fornt_back);

        TextView FrontScreenTv = dialog.findViewById(R.id.FrontScreenTv);
        TextView BackScreenTv = dialog.findViewById(R.id.BackScreenTv);
        TextView byMonthTv = dialog.findViewById(R.id.byMonthTv);

        FrontScreenTv.setText("Search All");
        BackScreenTv.setText("Search by Date");
        byMonthTv.setText("Search by Month");

        FrontScreenTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        BackScreenTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    void fetchData()
    {
        new AbstrctClss(Outlet_details.this, ConstntApi.outletDetals(Outlet_details.this), "g", true) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;
                try
                {
                    jsonArray = new JSONArray(s);

                    if (jsonArray.length()==0)
                    {
                        Commonhelper.showToastLong(Outlet_details.this, Constnt.notFound);
                        return;
                    }

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();
                    for (int i = 0;i<jsonArray.length();i++)
                    {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }
                    recycleView.setAdapter(new MyRecyclerAdapter(jsonObjectArrayList));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapter(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.outlet_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapter.ViewHolder holder, int position) {

            holder.nameTv.setText(jsonObjectal.get(position).optString("product_name"));
            holder.ipTv.setText("Total IP: "+jsonObjectal.get(position).optString("ip"));
            holder.sellID.setText("Sell ID: "+jsonObjectal.get(position).optString("order_id"));
            holder.grandTotalTv.setText("Grand total ₹"+jsonObjectal.get(position).optString("price"));
            holder.dateTimeTv.setText("Date & Time: "+jsonObjectal.get(position).optString("placed_on"));
            holder.product_code.setText("Product Code: "+jsonObjectal.get(position).optString("product_code"));
            Picasso.with(Outlet_details.this).load(jsonObjectal.get(position).optString("product_image")).into(holder.img);

//            if (jsonObjectal.get(position).optString("return_status").equals("1"))
//            {
//                holder.ReturnTv.setVisibility(View.VISIBLE);
//            }
//            else
//            {
//                holder.ReturnTv.setVisibility(View.GONE);
//            }
            holder.ReturnTv.setVisibility(View.GONE);
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView dateTimeTv,sellID,ipTv,nameTv,grandTotalTv,ReturnTv,product_code;
            ImageView img;
            public ViewHolder(View itemView) {
                super(itemView);
                dateTimeTv = itemView.findViewById(R.id.dateTimeTv);
                sellID  = itemView.findViewById(R.id.sellID);
                ipTv  = itemView.findViewById(R.id.ipTv);
                nameTv  = itemView.findViewById(R.id.nameTv);
                grandTotalTv  = itemView.findViewById(R.id.grandTotalTv);
                ReturnTv  = itemView.findViewById(R.id.ReturnTv);
                img  = itemView.findViewById(R.id.img);
                product_code  = itemView.findViewById(R.id.product_code);

                itemView.setOnClickListener(view -> {
                    Intent intent = new Intent(Outlet_details.this, OutletActivity.class);
                    intent.putExtra("ress",jsonObjectal.get(getAdapterPosition()).toString());
                    startActivity(intent);
                });
            }
        }
    }

    private void crtDetils()
    {
        new AbstrctClss(mActivity, ConstntApi.get_cart_details(mActivity), "g", false) {
            @Override
            public void responce(String s) {
                try {
                    JSONArray jsonArray = new JSONArray(s);
                    crtDetils2 = s;
                    if (jsonArray.length() > 9)
                        crtCountTv.setText("9+");
                    else crtCountTv.setText("" + jsonArray.length());
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("letestFeturePi", "sssss");
                }
            }
            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }
}

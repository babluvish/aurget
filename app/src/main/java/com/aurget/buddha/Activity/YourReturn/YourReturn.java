package com.aurget.buddha.Activity.YourReturn;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.YourOrder.YourOrder;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class YourReturn extends AppCompatActivity {

    RecyclerView recycleView;
    LinearLayout newReturnLl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_return);

        recycleView = findViewById(R.id.recycleView);
        newReturnLl = findViewById(R.id.newReturnLl);

        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(YourReturn.this, LinearLayoutManager.VERTICAL, false);
        recycleView.setLayoutManager(horizontalLayoutManager);
        recycleView.setItemAnimator(new DefaultItemAnimator());

        //recycleView.setAdapter(new MyRecyclerAdapter(null));

        return_reason();

        newReturnLl.setOnClickListener(view -> {
            Intent intent = new Intent(YourReturn.this, YourOrder.class);
            startActivity(intent);
        });

    }


    private void return_reason()
    {
        new AbstrctClss(YourReturn.this, ConstntApi.return_order_list(YourReturn.this), "g", true)
        {
            @Override
            public void responce(String s) {
                try {
                    Log.d("deal_product", s);

                    JSONObject jsonObject = new JSONObject(s);

                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                    ArrayList<JSONObject> jsonObjects = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        jsonObjects.add(jsonArray.optJSONObject(i));
                    }

                    recycleView.setAdapter(new MyRecyclerAdapter(jsonObjects));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }


    public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapter(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.your_return_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapter.ViewHolder holder, int position) {
            holder.prodNmeTv.setText(jsonObjectal.get(position).optString("product_name"));
            holder.ipTv.setText("IP: "+jsonObjectal.get(position).optString("ip"));
            holder.returnID.setText("Return ID: "+jsonObjectal.get(position).optString("ip"));
            holder.createdOnTv.setText("Return Created On: "+jsonObjectal.get(position).optString("Placed_On"));
            holder.status.setText(""+jsonObjectal.get(position).optString("status"));
            holder.refund_amount.setText("Refund Amount: "+jsonObjectal.get(position).optString("refund_amount"));
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private LinearLayout llContner;
            TextView prodNmeTv,ipTv,returnID,createdOnTv,status,refund_amount;
            public ViewHolder(View itemView) {
                super(itemView);
                llContner = itemView.findViewById(R.id.llContner);
                prodNmeTv = itemView.findViewById(R.id.prodNmeTv);
                ipTv = itemView.findViewById(R.id.ipTv);
                returnID = itemView.findViewById(R.id.returnID);
                createdOnTv = itemView.findViewById(R.id.createdOnTv);
                status = itemView.findViewById(R.id.status);
                refund_amount = itemView.findViewById(R.id.refund_amount);

                llContner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(YourReturn.this, ReturnDetail.class);
                        intent.putExtra("prodId",jsonObjectal.get(getAdapterPosition()).optString("product_name"));
                        startActivity(intent);
                    }
                });
            }
        }
    }
}

package com.aurget.buddha.Activity.code.Account;

import android.content.ContentValues;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Database.DatabaseController;
import com.aurget.buddha.Activity.code.Database.TableFavourite;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.Product.CartListActivity;
import com.aurget.buddha.Activity.code.Product.ProductDetailsActivity;
import com.aurget.buddha.Activity.code.Search.SearchActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import code.utils.AppUrls;

public class FavouriteListActivity extends BaseActivity implements View.OnClickListener {
    ProductAdapter productAdapter;
    ArrayList<HashMap<String, String>> productList = new ArrayList();

    RecyclerView recyclerView;
    RelativeLayout rrNoData;
    ImageView ivMenu,ivSearch,ivCart;

    Typeface typeface;
    String offset="0";
    GridLayoutManager mGridLayoutManager;

    //LinearLayout
    LinearLayout llFilter;

    TextView tvCount,tvNoProduct;

    boolean loadMore=true;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_product_list);
        findById();
    }

    private void findById() {
        rrNoData = findViewById(R.id.rrNoData);

        //ImageView
        ivMenu =    findViewById(R.id.iv_menu);
        ivSearch =  findViewById(R.id.searchmain);
        ivCart =    findViewById(R.id.ivCart);

        //Recycleview
        recyclerView =  findViewById(R.id.productRecyclerView);

        //LinearLayout
        llFilter =  findViewById(R.id.llFilter);
        llFilter.setVisibility(View.GONE);

        typeface = Typeface.createFromAsset(mActivity.getAssets(), "centurygothic.otf");

        //TextView
        tvCount =  findViewById(R.id.tvCount);
        tvNoProduct =  findViewById(R.id.textView5);
        TextView tvHeader =  findViewById(R.id.tvHeaderText);
        tvHeader.setText("Favourite List");

        tvNoProduct.setText("Oops!\n\nSeems like you haven't added any product to Favourite List");

        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        ivCart.setOnClickListener(this);
        ivMenu.setImageResource(R.drawable.ic_back);

        mGridLayoutManager = new GridLayoutManager(mActivity, 2);
        recyclerView.setLayoutManager(mGridLayoutManager);

        productList.clear();
        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getFavListApi();
        } else {
            AppUtils.showErrorMessage(rrNoData, getString(R.string.errorInternet), mActivity);
        }

    }

    private void getFavListApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("getFavListApi", AppUrls.getWishList);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));

            json.put(AppConstants.result, json_data);

            Log.v("getFavListApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.getWishList)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                parseJsondata(response);
            }

            @Override
            public void onError(ANError error) {
                AppUtils.hideDialog();
                // handle error
                if (error.getErrorCode() != 0) {
                    AppUtils.showErrorMessage(rrNoData, String.valueOf(error.getErrorCode()), mActivity);
                    Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                    Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                    Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                } else {
                    AppUtils.showErrorMessage(rrNoData, String.valueOf(error.getErrorDetail()), mActivity);
                }
            }
        });
    }

    private void parseJsondata(JSONObject response) {

        Log.d("response ", response.toString());

        DatabaseController.removeTable(TableFavourite.favourite);

        productList.clear();

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                JSONArray productArray = jsonObject.getJSONArray("wish_list");

                for (int i = 0; i < productArray.length(); i++) {

                    JSONObject productobject = productArray.getJSONObject(i);

                    ContentValues mContentValues = new ContentValues();

                    HashMap<String, String> prodList = new HashMap();

                    prodList.put("cart_id", productobject.getString("cart_id"));
                    prodList.put("product_name", productobject.getString("product_name"));
                    prodList.put("final_price", productobject.getString("final_price"));
                    prodList.put("id", productobject.getString("product_id"));
                    prodList.put("price", productobject.getString("price"));
                    prodList.put("product_discount_amount", productobject.getString("product_discount_amount"));
                    prodList.put("product_discount_type", productobject.getString("product_discount_type"));
                    prodList.put("product_image", productobject.getString("product_image"));

                    mContentValues.put(TableFavourite.favouriteColumn.productId.toString(), productobject.getString("product_id"));
                    DatabaseController.insertData(mContentValues,TableFavourite.favourite);

                    productList.add(prodList);
                }
            }
            else
            {
                loadMore=false;
                AppUtils.showErrorMessage(rrNoData, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(rrNoData, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();

        if (productList.size() <= 0) {
            rrNoData.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            rrNoData.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            productAdapter = new ProductAdapter(productList);
            recyclerView.setAdapter(productAdapter);
            recyclerView.setNestedScrollingEnabled(true);
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_menu:
                finish();
                return;
            case R.id.searchmain:
                startActivity(new Intent(getBaseContext(), SearchActivity.class));
                return;

            case R.id.ivCart:

                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else
                {
                    startActivity(new Intent(getBaseContext(), CartListActivity.class));
                }

                return;

            default:
                return;
        }
    }


    private class ProductAdapter extends RecyclerView.Adapter<FavNameHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

        public ProductAdapter(ArrayList<HashMap<String, String>> favList) {
            data = favList;
        }

        public FavNameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new FavNameHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_product, parent, false));
        }

        public void onBindViewHolder(FavNameHolder holder, final int position) {

           /* holder.tvName.setTypeface(typeface);
            holder.tvDiscount.setTypeface(typeface);
            holder.tvPrice.setTypeface(typeface);
            holder.tvFPrice.setTypeface(typeface);*/

            holder.tvName.setText((CharSequence) ((HashMap) data.get(position)).get("product_name"));
            if (((String) ((HashMap) data.get(position)).get("product_discount_type")).equals("1")) {
                holder.tvDiscount.setText("Flat RS" + ((String) ((HashMap) data.get(position)).get("product_discount_amount")) + " off");
            } else if (((String) ((HashMap) data.get(position)).get("product_discount_type")).equals("2")) {
                holder.tvDiscount.setText(((String) ((HashMap) data.get(position)).get("product_discount_amount")) + "% off");
            } else {
                holder.tvDiscount.setText("");
                holder.tvDiscount.setVisibility(View.INVISIBLE);
            }
            if (((String) ((HashMap) data.get(position)).get("final_price")).equalsIgnoreCase((String) ((HashMap) data.get(position)).get("price"))) {
                holder.tvFPrice.setText("RS" + ((String) ((HashMap) data.get(position)).get("final_price")));
                holder.tvDiscount.setText("");
                holder.tvPrice.setText("");
            } else {
                holder.tvFPrice.setText("RS" + ((String) ((HashMap) data.get(position)).get("final_price")));
                holder.tvPrice.setText("RS" + ((String) ((HashMap) data.get(position)).get("price")));
                holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | 16);
            }
            try {
                if (((HashMap) data.get(position)).get("product_image") != null && !((String) ((HashMap) data.get(position)).get("product_image")).isEmpty()) {
                    Picasso.with(mActivity).load((String) ((HashMap) data.get(position)).get("product_image")).placeholder((int) R.mipmap.ic_launcher).into(holder.ivPic);
                }
            } catch (NotFoundException e) {
                e.printStackTrace();
            }

            if(DatabaseController.checkRecordExist(TableFavourite.favourite, String.valueOf(TableFavourite.favouriteColumn.productId),data.get(position).get("id")))
            {
                holder.icWishlist.setImageResource(R.drawable.ic_heart_red);
            }
            else
            {
                holder.icWishlist.setImageResource(R.drawable.ic_heart_grey);
            }

            holder.icWishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(!AppSettings.getString(AppSettings.userId).isEmpty())
                    {
                        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                            addFavApi(data.get(position).get("id"));
                        } else {
                            AppUtils.showErrorMessage(tvCount, getString(R.string.errorInternet), mActivity);
                        }
                    }
                    else {
                        AppUtils.showErrorMessage(tvCount, "Kindly login first", mActivity);
                    }

                }
            });

            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent mIntent = new Intent(mActivity, ProductDetailsActivity.class);
                    mIntent.putExtra(AppConstants.productId, data.get(position).get("id"));
                    mIntent.putExtra(AppConstants.from, "4");
                    startActivity(mIntent);

                }
            });


        }

        public int getItemCount() {
            return data.size();
        }
    }

    private class FavNameHolder extends RecyclerView.ViewHolder {
        ImageView ivPic, icWishlist;
        LinearLayout linearLayout;
        TextView tvDiscount;
        TextView tvFPrice;
        TextView tvName;
        TextView tvPrice;

        public FavNameHolder(View itemView) {
            super(itemView);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.layout_item);
            ivPic = (ImageView) itemView.findViewById(R.id.ivProduct);
            icWishlist = (ImageView) itemView.findViewById(R.id.ic_wishlist);
            tvName = (TextView) itemView.findViewById(R.id.tvProductName);
            tvFPrice = (TextView) itemView.findViewById(R.id.tvFPrice);
            tvPrice = (TextView) itemView.findViewById(R.id.tvPrice);
            tvDiscount = (TextView) itemView.findViewById(R.id.tvDiscount);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        int count = Integer.parseInt(AppSettings.getString(AppSettings.cartCount));

        if(count>0)
        {
            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(String.valueOf(count));
        }
        else
        {
            tvCount.setVisibility(View.GONE);
        }

        try {
            productAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addFavApi(String productId) {

        AppUtils.showRequestDialog(mActivity);

        Log.v("addFavApi", AppUrls.wishList);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json_data.put("product_master_id", productId);

            json.put(AppConstants.result, json_data);

            Log.v("addFavApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.wishList)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseFavdata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvCount, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvCount, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseFavdata(JSONObject response) {

        AppUtils.hideDialog();
        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                if(jsonObject.getString("status").equals("1"))
                {
                    AppUtils.showErrorMessage(tvCount, "Product Added to Favourites", mActivity);
                }
                else if(jsonObject.getString("status").equals("2"))
                {
                    AppUtils.showErrorMessage(tvCount, "Product Removed from Favourites", mActivity);
                }
            }
            else
            {
                AppUtils.showErrorMessage(tvCount, "Product Removed from Favourites", mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvCount, String.valueOf(e), mActivity);
        }

        productList.clear();
        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getFavListApi();
        } else {
            AppUtils.showErrorMessage(rrNoData, getString(R.string.errorInternet), mActivity);
        }
    }
}

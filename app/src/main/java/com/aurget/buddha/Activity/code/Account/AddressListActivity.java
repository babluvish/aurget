package com.aurget.buddha.Activity.code.Account;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.Product.CartListActivity;
import com.aurget.buddha.Activity.code.Search.SearchActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import code.utils.AppUrls;

public class AddressListActivity extends BaseActivity implements View.OnClickListener {

    ArrayList<HashMap<String, String>> addresstList = new ArrayList();

    RecyclerView recyclerView;

    AddressApdapter addressApdapter;

    //ImageView header
    ImageView ivMenu;
    ImageView ivSearch;
    ImageView ivCart;

    //TextView
    TextView tvMain,tvCount,tvAddressCount,tvAddAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);
        initialise();
    }

    private void initialise() {

        //RecyclerView for list
        recyclerView =findViewById(R.id.recyclerview);

        //ImageView
        ivMenu =    findViewById(R.id.iv_menu);
        ivSearch =  findViewById(R.id.searchmain);
        ivCart =    findViewById(R.id.ivCart);

        //TextView
        tvCount =  findViewById(R.id.tvCount);
        tvMain =   findViewById(R.id.tvHeaderText);
        tvAddressCount =  findViewById(R.id.tvAddress);
        tvAddAddress =    findViewById(R.id.textView6);

        ivMenu.setImageResource(R.drawable.ic_back);
        tvMain.setText("My Addresses");
        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        ivCart.setOnClickListener(this);
        tvAddAddress.setOnClickListener(this);

        RecyclerView.LayoutManager recylerViewLayoutManager = new LinearLayoutManager(mActivity);
        recyclerView.setLayoutManager(recylerViewLayoutManager);
        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getAddressListApi();
        } else {
            AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.iv_menu:
                finish();
                return;

            case R.id.searchmain:
                startActivity(new Intent(getBaseContext(), SearchActivity.class));
                return;

            case R.id.ivCart:

                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else
                {
                    startActivity(new Intent(getBaseContext(), CartListActivity.class));
                }
                return;

            case R.id.textView6:
                Intent mIntent = new Intent(mActivity, AddEditAddressActivity.class);
                mIntent.putExtra("From", "0");
                startActivity(mIntent);
                finish();
                return;

        }

    }

    private void getAddressListApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("getAddressListApi", AppUrls.getAllUserAddress);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json.put(AppConstants.result, json_data);
            Log.v("getAddressListApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.getAllUserAddress)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata(response);
                    }
                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }
    private void parseJsondata(JSONObject response) {

        Log.d("response ", response.toString());

        addresstList.clear();

        try {

            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);

            if (jsonObject.getString("res_code").equals("1")) {

                JSONArray productArray = jsonObject.getJSONArray("data");

                tvAddressCount.setText(String.valueOf(productArray.length())+" Saved Addresses");

                for (int i = 0; i < productArray.length(); i++) {

                    JSONObject productobject = productArray.getJSONObject(i);

                    HashMap<String, String> addList = new HashMap();




                    addList.put("id", productobject.getString("id"));
                    addList.put("name", productobject.getString("name"));
                    addList.put("phone_no", productobject.getString("phone_no"));
                    addList.put("pincode", productobject.getString("pincode"));
                    addList.put("locality", productobject.getString("locality"));
                    addList.put("area_streat_address", productobject.getString("area_streat_address"));
                    addList.put("district_city_town", productobject.getString("district_city_town"));
                    addList.put("landmark_optional", productobject.getString("landmark_optional"));
                    addList.put("stateName", productobject.getString("stateName"));
                    addList.put("state_id", productobject.getString("state_id"));
                    addList.put("countery_id", productobject.getString("countery_id"));
                    addList.put("country_name", productobject.getString("country_name"));
                    addList.put("alternative_phone_optional", productobject.getString("alternative_phone_optional"));

                    if(i==0)
                    {
                        addList.put("primary", "1");
                    }
                    else
                    {
                        addList.put("primary", "0");
                    }

                    addresstList.add(addList);

                }

                addressApdapter = new AddressApdapter(addresstList);
                recyclerView.setAdapter(addressApdapter);
                recyclerView.setNestedScrollingEnabled(true);

            }
            else
            {
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(recyclerView, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
    }

    private class AddressApdapter extends RecyclerView.Adapter<FavNameHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

        public AddressApdapter(ArrayList<HashMap<String, String>> favList) {
            data = favList;
        }

        public FavNameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new FavNameHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_address, parent, false));
        }

        public void onBindViewHolder(FavNameHolder holder, final int position) {




            holder.tvName.setText(data.get(position).get("name"));

            holder.tvMobile.setText(data.get(position).get("phone_no"));

            String valueAddress="";


            if(!data.get(position).get("area_streat_address").equalsIgnoreCase("")){
                valueAddress=valueAddress+data.get(position).get("area_streat_address")+", ";
            }
            if(!data.get(position).get("locality").equalsIgnoreCase("")){
                valueAddress=valueAddress+data.get(position).get("locality")+", ";
            }
            if(!data.get(position).get("landmark_optional").equalsIgnoreCase("")){
                valueAddress=valueAddress+data.get(position).get("landmark_optional")+", ";
            }
            if(!data.get(position).get("district_city_town").equalsIgnoreCase("")){
                valueAddress=valueAddress+data.get(position).get("district_city_town")+", ";
            } if(!data.get(position).get("country_name").equalsIgnoreCase("")){
                valueAddress=valueAddress+data.get(position).get("country_name")+"- ";
            }if(!data.get(position).get("stateName").equalsIgnoreCase("")){
                valueAddress=valueAddress+data.get(position).get("stateName")+"- ";
            }if(!data.get(position).get("pincode").equalsIgnoreCase("")){
                valueAddress=valueAddress+data.get(position).get("pincode");
            }
            holder.tvAddress.setText(valueAddress);






          /*  if(data.get(position).get("landmark_optional").equals("")){

            }else if(data.get(position).get("landmark_optional").equals("")){

            }*/
          /*  String address = data.get(position).get("area_streat_address")+","
                    +data.get(position).get("locality")+","
                    +data.get(position).get("landmark_optional")+","+data.get(position).get("district_city_town")
                    +","+data.get(position).get("stateName")+
            " - "+data.get(position).get("pincode");


            address.replaceAll(",",", ,");
            address.replaceAll("a","5");
            Log.v("Mera_String",address.replaceAll(", ,",","));
            holder.tvAddress.setText(address);*/

            holder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                        DeleteAddressApi(data.get(position).get("id"));
                    } else {
                        AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                    }

                }
            });

            holder.llView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent mIntent = new Intent(mActivity, AddEditAddressActivity.class);
                    mIntent.putExtra("AddressId", data.get(position).get("id"));
                    mIntent.putExtra("Name", data.get(position).get("name"));
                    mIntent.putExtra("PhoneNo", data.get(position).get("phone_no"));
                    mIntent.putExtra("Pincode", data.get(position).get("pincode"));
                    mIntent.putExtra("Locality", data.get(position).get("locality"));
                    mIntent.putExtra("Flat", data.get(position).get("area_streat_address"));
                    mIntent.putExtra("City", data.get(position).get("district_city_town"));
                    mIntent.putExtra("Landmark", data.get(position).get("landmark_optional"));
                    mIntent.putExtra("Alternate", data.get(position).get("alternative_phone_optional"));
                    mIntent.putExtra("state", data.get(position).get("state_id"));
                    mIntent.putExtra("countrynam", data.get(position).get("countery_id"));
                    mIntent.putExtra("From", "1");
                    startActivity(mIntent);
                    finish();
                }
            });
        }

        public int getItemCount() {
            return data.size();
        }
    }

    private class FavNameHolder extends RecyclerView.ViewHolder {
        ImageView ivDelete;
        TextView tvName,tvAddress,tvMobile;

        LinearLayout llView;

        public FavNameHolder(View itemView) {
            super(itemView);
            llView =   itemView.findViewById(R.id.llAddress);
            ivDelete =  itemView.findViewById(R.id.ivDelete);
            tvName =    itemView.findViewById(R.id.textView4);
            tvAddress = itemView.findViewById(R.id.textView3);
            tvMobile =  itemView.findViewById(R.id.textView5);
        }
    }

    private void DeleteAddressApi(String AddressId) {
        AppUtils.showRequestDialog(mActivity);
        Log.v("DeleteAddressApi", AppUrls.addNewAddress);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("address_master_id", AddressId);
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json_data.put("add_update_delete", "3");
            json_data.put("name", "");
            json_data.put("phone_no", "");
            json_data.put("pincode", "");
            json_data.put("locality", "");
            json_data.put("area_streat_address", "");
            json_data.put("district_city_town", "");
            json_data.put("landmark_optional", "");
            json_data.put("alternative_phone_optional", "");
            json_data.put("state_master_id", "");
            json_data.put("countery_id", "");

            json.put(AppConstants.result, json_data);

            Log.v("DeleteAddressApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.addNewAddress)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedeletedata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedeletedata(JSONObject response) {

        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
                AppUtils.hideDialog();

                  startActivity(new Intent(getBaseContext(), AddressListActivity.class));
                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    getAddressListApi();
                } else {
                    AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                }
            }
            else
            {
                AppUtils.hideDialog();
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.hideDialog();
            AppUtils.showErrorMessage(recyclerView, String.valueOf(e), mActivity);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        int count = Integer.parseInt(AppSettings.getString(AppSettings.cartCount));

        if(count>0)
        {
            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(String.valueOf(count));
        }
        else
        {
            tvCount.setVisibility(View.GONE);
        }

    }
}

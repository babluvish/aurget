package com.aurget.buddha.Activity.code.Category;


import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.aurget.buddha.Activity.code.Adapter.CategoryAdapter;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Database.DatabaseController;
import com.aurget.buddha.Activity.code.Fragment.DashBoardFragment;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;

import java.util.ArrayList;
import java.util.HashMap;


public class TopFeaturedActivity extends BaseActivity implements View.OnClickListener {

    ArrayList<HashMap<String, String>> CategoryList = new ArrayList();

    CategoryAdapter categoryAdapter;

    GridView gridView;

    ImageView ivMenu, ivSearch, ivCart;

    TextView tvMain;
    RelativeLayout rrNoData;

    LinearLayout llHome, llCategory, llFavourites, llProfile, llNotification;

    TextView tvHome, tvCategory, tvFavourites, tvProfile, tvCount;

    ImageView ivHome, ivCategory, ivFavourites, ivProfile, ivNotification;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_featured);
        initialise();
      //  featuredApi("0");
    }


    private void initialise() {

        //TextView
        tvMain = findViewById(R.id.tvHeaderText);
        tvHome = findViewById(R.id.tvHomeBottom);
        tvCategory = findViewById(R.id.tvCategoryBottom);
        tvFavourites = findViewById(R.id.tvFavouritesBottom);
        tvProfile = findViewById(R.id.tvProfileBottom);
        tvCount = findViewById(R.id.tvCount);

        //ImageView
        ivMenu = findViewById(R.id.iv_menu);
        ivSearch = findViewById(R.id.searchmain);
        ivHome = findViewById(R.id.ivHomeBottom);
        ivCategory = findViewById(R.id.ivCategoryBottom);
        ivFavourites = findViewById(R.id.ivFavouritesBottom);
        ivProfile = findViewById(R.id.ic_profile);
        ivCart = findViewById(R.id.ivCart);
        ivNotification = findViewById(R.id.ic_notification);

        //GridView
        gridView = findViewById(R.id.gridView);

        //RelativeLayout
        rrNoData = findViewById(R.id.rrNoData);

        //LinearLayout fro bottom views
        llHome = findViewById(R.id.llHome);
        llCategory = findViewById(R.id.llCategory);
        llFavourites = findViewById(R.id.llFavourites);
        llProfile = findViewById(R.id.llProfile);
        llNotification = findViewById(R.id.llNotification);

        llHome.setOnClickListener(this);
        llCategory.setOnClickListener(this);
        llFavourites.setOnClickListener(this);
        llProfile.setOnClickListener(this);
        llNotification.setOnClickListener(this);

        tvMain.setText("Top Featured ");
        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        ivCart.setOnClickListener(this);

        ivMenu.setImageResource(R.drawable.ic_back);

        getCatData();
    }

    public void getCatData() {
        CategoryList.clear();
        CategoryList.addAll(DatabaseController.getCategoryData());


        if (CategoryList.size() > 0) {
            rrNoData.setVisibility(View.GONE);
            gridView.setVisibility(View.VISIBLE);
            setCatAdapter();
        } else {
            rrNoData.setVisibility(View.VISIBLE);
            gridView.setVisibility(View.GONE);
        }

    }

    public void setCatAdapter() {
        categoryAdapter = new CategoryAdapter(mActivity, CategoryList) {
            protected void catClick(View v, String position) {
                SubCategoryActivity.categoryId = (String) ((HashMap) CategoryList.get(Integer.parseInt(position))).get("category_id");
                startActivity(new Intent(getBaseContext(), SubCategoryActivity.class));
            }
        };
        gridView.setAdapter(categoryAdapter);
    }


   /* private void featuredApi(String offset) {

        AppUtils.showRequestDialog(mActivity);

        Log.v("featuredApi", AppUrls.FeaturedProduct);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {

            json_data.put("offset", offset);

            json.put(AppConstants.result, json_data);

            Log.v("featuredApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.FeaturedProduct)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }*/



    @Override
    protected void onResume() {
        super.onResume();
        tvHome.setTextColor(Color.parseColor("#9d9b9b"));
        tvCategory.setTextColor(Color.parseColor("#155b72"));
        tvFavourites.setTextColor(Color.parseColor("#9d9b9b"));
        tvProfile.setTextColor(Color.parseColor("#9d9b9b"));
        ivHome.setImageResource(R.drawable.ic_home_grey);
        ivCategory.setImageResource(R.drawable.ic_categories_darkgreen);
        ivFavourites.setImageResource(R.drawable.ic_heart_grey);
        ivProfile.setImageResource(R.drawable.ic_profile_grey);

        int count = Integer.parseInt(AppSettings.getString(AppSettings.cartCount));

        if(count>0)
        {
            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(String.valueOf(count));
        }
        else
        {
            tvCount.setVisibility(View.GONE);
        }
    }
    @Override
    public void onBackPressed() {
     startActivity(new Intent(getBaseContext(), DashBoardFragment.class));
    }
    @Override
    public void onClick(View view) {
        switch(view.getId()){
        case R.id.iv_menu:
        onBackPressed();
        break;
}
    }



}

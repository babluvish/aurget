package code.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class CategoryListAdapter extends BaseAdapter {
    public OnClickListener catClickListener ;
    Context context;
    ArrayList<HashMap<String, String>> data;

    protected abstract void catClick(View view, String str);

    public CategoryListAdapter(Context context, ArrayList<HashMap<String, String>> placeData) {
        this.context = context;
        this.data = placeData;

        catClickListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                catClick(v, String.valueOf(v.getTag()));
            }
        };
    }

    public int getCount() {
        return this.data.size();
    }

    public Object getItem(int position) {
        return this.data.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView = convertView;
        rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.subcategoryadapter, null);
        ImageView ivPic = (ImageView) rootView.findViewById(R.id.imageView24);
        RelativeLayout rr = (RelativeLayout) rootView.findViewById(R.id.rr);
        ((TextView) rootView.findViewById(R.id.textView29)).setText(((String) ((HashMap) this.data.get(position)).get("sub_category_name")).trim());
        if (((String) ((HashMap) data.get(position)).get("icon")).isEmpty()) {
            Picasso.with(context).load((int) R.mipmap.ic_launcher).placeholder((int) R.mipmap.ic_launcher).into(ivPic);
        } else {
            Picasso.with(context).load((String) ((HashMap) data.get(position)).get("icon")).placeholder((int) R.mipmap.ic_launcher).into(ivPic);
        }

        rr.setTag(position);
        rr.setOnClickListener(this.catClickListener);
        return rootView;
    }
}

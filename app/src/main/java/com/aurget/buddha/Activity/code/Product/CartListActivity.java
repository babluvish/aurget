package com.aurget.buddha.Activity.code.Product;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Vibrator;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.aurget.buddha.Activity.code.Common.LinearLayoutManagerWithSmoothScroller;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Fragment.DashBoardFragment;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.OrderModule.ChooseAddressListActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import code.Common.AnimationHelper;

import code.utils.AppUrls;

public class CartListActivity extends BaseActivity implements View.OnClickListener {

    ArrayList<HashMap<String, String>> cartList = new ArrayList();

    RecyclerView recyclerView;

    CartApdapter cartAdapter;

    //ImageView header
    ImageView ivMenu;
    ImageView ivSearch;
    ImageView ivCart;

    TextView tvTotal, tvPayment, tvContinueShopping, tvCount;

    RelativeLayout rlValue;

    String finalPrice = "";
    LinearLayoutManagerWithSmoothScroller recylerViewLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_cart_list);

        initialise();
    }

    private void initialise() {

        //RecyclerView for list
        recyclerView = findViewById( R.id.recyclerview );

        //ImageView
        ivMenu = findViewById( R.id.iv_menu );
        ivSearch = findViewById( R.id.searchmain );
        ivCart = findViewById( R.id.ivCart );

        rlValue = findViewById( R.id.llValue );

        //TextView
        tvTotal = findViewById( R.id.text_action_bottom1 );
        tvPayment = findViewById( R.id.text_action_bottom2 );
        tvContinueShopping = findViewById( R.id.tvContinueShopping );
        tvCount = findViewById( R.id.tvCount );

        TextView tvHeader = findViewById( R.id.tvHeaderText );
        tvHeader.setText( getString( R.string.cart ) );
        rlValue.setOnClickListener(this);
        ivMenu.setImageResource( R.drawable.ic_back );
        ivSearch.setVisibility( View.INVISIBLE );
        ivCart.setVisibility( View.INVISIBLE );

        ivMenu.setOnClickListener( this );
        tvTotal.setOnClickListener( this );
        tvPayment.setOnClickListener( this );
        tvContinueShopping.setOnClickListener( this );

        recylerViewLayoutManager = new LinearLayoutManagerWithSmoothScroller( mActivity );
        recyclerView.setLayoutManager( recylerViewLayoutManager );
        //recyclerView.setAdapter(new CartListActivity.SimpleStringRecyclerViewAdapter(recyclerView, cartlistImageUri));
        cartList.clear();
        if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
            getCartListApi(0);
        } else {
            AppUtils.showErrorMessage( recyclerView, getString( R.string.errorInternet ), mActivity );
        }
    }

    protected void setCartLayout(int position) {
        LinearLayout layoutCartItems = findViewById( R.id.layout_items );
        LinearLayout layoutCartPayments = findViewById( R.id.layout_payment );
        LinearLayout layoutCartNoItems = findViewById( R.id.layout_cart_empty );

        if (cartList.size() > 0) {
            layoutCartNoItems.setVisibility( View.GONE );
            layoutCartItems.setVisibility( View.VISIBLE );
            //  layoutCartPayments.setVisibility(View.VISIBLE);
            layoutCartPayments.setVisibility( View.GONE );
            String amountValue = AppSettings.getString( AppSettings.totalAmount );
            tvTotal.setText( "Proceed to Pay: "+getString(R.string.rs)+ amountValue );
            rlValue.setVisibility( View.VISIBLE );
            cartAdapter = new CartApdapter( cartList );
            recyclerView.setAdapter( cartAdapter );
            recyclerView.setNestedScrollingEnabled( true );
            recyclerView.smoothScrollToPosition(position);
        } else {
            layoutCartNoItems.setVisibility( View.VISIBLE );
            layoutCartItems.setVisibility( View.GONE );
            layoutCartPayments.setVisibility( View.GONE );
            rlValue.setVisibility( View.GONE );
            Button bStartShopping = findViewById( R.id.bAddNew );

            bStartShopping.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            } );
        }
    }

    private void getCartListApi(int position) {

        AppUtils.showRequestDialog(mActivity);

        // AppUtils.showRequestDialog( mActivity );
        Log.v( "getCartListApi", AppUrls.getCart );
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put( "user_master_id", AppSettings.getString( AppSettings.userId ) );
            json.put( AppConstants.result, json_data );
            Log.v( "getCartListApi", json.toString() );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post( AppUrls.getCart )
                .addJSONObjectBody( json )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata( response ,position);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage( recyclerView, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            AppUtils.showErrorMessage( recyclerView, String.valueOf( error.getErrorDetail() ), mActivity );
                        }
                    }
                } );
    }
    private void parseJsondata(JSONObject response, int position) {
        Log.d( "response ", response.toString() );
        finalPrice = "";
        AppSettings.putString( AppSettings.cartCount, "0" );
        AppSettings.putString( AppSettings.price, String.valueOf( finalPrice ) );
        AppSettings.putString( AppSettings.finalPrice, String.valueOf( finalPrice ) );
        try {
            JSONObject jsonObject = response.getJSONObject( AppConstants.responseTag );
            if (jsonObject.getString( "res_code" ).equals( "1" )) {
                JSONArray productArray = jsonObject.getJSONArray( "cart_product" );
                if (productArray.length() > 0) {
                    tvCount.setVisibility( View.VISIBLE );
                    ivCart.setVisibility( View.VISIBLE );
                    tvCount.setText( String.valueOf( productArray.length() ) );
                }
                AppSettings.putString( AppSettings.cartCount, String.valueOf( productArray.length() ) );
                rlValue.setVisibility( View.VISIBLE );
                float totalAmount = 0;
                for (int i = 0; i < productArray.length(); i++) {
                    JSONObject productobject = productArray.getJSONObject( i );
                    HashMap<String, String> prodList = new HashMap();
                    prodList.put( "cart_id", productobject.getString( "cart_id" ) );
                    prodList.put( "product_id", productobject.getString( "product_id" ) );
                    prodList.put( "product_name", productobject.getString( "product_name" ) );
                    prodList.put( "final_price", productobject.getString( "final_price" ) );
                    prodList.put( "price", productobject.getString( "price" ) );
                    prodList.put( "product_discount_amount", productobject.getString( "product_discount_amount" ) );
                    prodList.put( "product_discount_type", productobject.getString( "product_discount_type" ) );
                    prodList.put( "product_image", productobject.getString( "product_image" ) );
                    prodList.put( "quantity", productobject.getString( "quantity" ) );

                    cartList.add( prodList );
                    float price = Float.parseFloat( productobject.getString( "final_price" ) );
                    int quantity = Integer.parseInt( productobject.getString( "quantity" ) );
                    int id = Integer.parseInt( productobject.getString( "cart_id" ) );
                    float amount = price * quantity;
                    finalPrice = String.format( "%.2f", amount );
                    totalAmount = totalAmount + price * quantity;
                    Log.v( "datavalue", String.valueOf( totalAmount ) );
                }
                AppSettings.putString( AppSettings.totalAmount, String.valueOf( totalAmount ) );
                AppSettings.putString( AppSettings.price, String.valueOf( finalPrice ) );
                AppSettings.putString( AppSettings.finalPrice, String.valueOf( finalPrice ) );
                AppSettings.putString( AppSettings.discount, "0" );
                AppSettings.putString( AppSettings.couponId, "" );
            } else {
                tvCount.setVisibility( View.GONE );
                ivCart.setVisibility( View.GONE );
                AppUtils.showErrorMessage( recyclerView, String.valueOf( jsonObject.getString( "res_msg" ) ), mActivity );
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage( recyclerView, String.valueOf( e ), mActivity );
            Log.v( "fhhjfhjfh", String.valueOf( e ) );
        }
        AppUtils.hideDialog();
        setCartLayout(position);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_menu:
                finish();
                return;

            case R.id.tvContinueShopping:
                startActivity( new Intent( getBaseContext(), DashBoardFragment.class ) );
                return;

            case R.id.llValue:

                float min = Float.parseFloat( AppSettings.getString( AppSettings.min_order_bal ) );

                if (AppSettings.getString( AppSettings.userId ).isEmpty()) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else if (AppSettings.getString( AppSettings.verified ).contains( "0" )) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else {
                    startActivity( new Intent( getBaseContext(), ChooseAddressListActivity.class ) );
                }
                return;
                case R.id.text_action_bottom1:

                float min2 = Float.parseFloat( AppSettings.getString( AppSettings.min_order_bal ) );

                if (AppSettings.getString( AppSettings.userId ).isEmpty()) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else if (AppSettings.getString( AppSettings.verified ).contains( "0" )) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else {
                    startActivity( new Intent( getBaseContext(), ChooseAddressListActivity.class ) );
                }
                return;
        }
    }

    double roundTwoDecimals(double d) {
        DecimalFormat twoDForm = new DecimalFormat( "#.##" );
        return Double.valueOf( twoDForm.format( d ) );
    }

    private void UpdateCartApi(String quantity, String ProductId, int position) {
        Log.v( "AddToCartApi", AppUrls.updateCart );
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put( "product_id", ProductId );
            json_data.put( "user_master_id", AppSettings.getString( AppSettings.userId ) );
            json_data.put( "quantity", quantity );
            json.put( AppConstants.result, json_data );
            Log.v( "AddToCartApi", json.toString() );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post( AppUrls.updateCart )
                .addJSONObjectBody( json )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedata( response,position);
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage( recyclerView, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            AppUtils.showErrorMessage( recyclerView, String.valueOf( error.getErrorDetail() ), mActivity );
                        }
                    }
                } );
    }

    private void parsedata(JSONObject response, int position) {
        Log.d( "response ", response.toString() );
        try {
            JSONObject jsonObject = response.getJSONObject( AppConstants.responseTag );
            if (jsonObject.getString( "res_code" ).equals( "1" )) {
                AppUtils.showErrorMessage( recyclerView, String.valueOf( jsonObject.getString( "res_msg" ) ), mActivity );
                cartList.clear();
                if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                    getCartListApi(position);
                   // recyclerView.smoothScrollToPosition(position);
                  /*  RecyclerView.SmoothScroller smoothScroller = new
                            LinearSmoothScroller(mActivity) {
                                @Override protected int getVerticalSnapPreference() {
                                    return LinearSmoothScroller.SNAP_TO_START;
                                }
                            };
                    smoothScroller.setTargetPosition(position);
                    recylerViewLayoutManager.startSmoothScroll(smoothScroller);*/
                } else {
                    AppUtils.showErrorMessage( recyclerView, getString( R.string.errorInternet ), mActivity );
                }
            } else {
                AppUtils.hideDialog();
                AppUtils.showErrorMessage( recyclerView, String.valueOf( jsonObject.getString( "res_msg" ) ), mActivity );
            }
        } catch (Exception e) {
            AppUtils.hideDialog();
            AppUtils.showErrorMessage( recyclerView, String.valueOf( e ), mActivity );
        }

    }

    private void DeleteCartApi(String CartId, int position) {

        AppUtils.showRequestDialog( mActivity );

        Log.v( "AddToCartApi", AppUrls.deleteFromCart );

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put( "cart_id", CartId );
            json_data.put( "user_master_id", AppSettings.getString( AppSettings.userId ) );
            json.put( AppConstants.result, json_data );
            Log.v( "AddToCartApi", json.toString() );
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post( AppUrls.deleteFromCart )
                .addJSONObjectBody( json )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedeletedata( response ,position);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage( recyclerView, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            AppUtils.showErrorMessage( recyclerView, String.valueOf( error.getErrorDetail() ), mActivity );
                        }
                    }
                } );
    }

    private void parsedeletedata(JSONObject response, int position) {

        Log.d( "response ", response.toString() );

        try {
            JSONObject jsonObject = response.getJSONObject( AppConstants.responseTag );
            if (jsonObject.getString( "res_code" ).equals( "1" )) {
                AppUtils.showErrorMessage( recyclerView, String.valueOf( jsonObject.getString( "res_msg" ) ), mActivity );
                AppUtils.hideDialog();
               /* if(cartAdapter!=null) {
                    cartList.remove(position);
                    cartAdapter.notifyDataSetChanged();
                    cartAdapter.s
                }*/

                /*if(cartList.size()==0){
                    tvCount.setVisibility( View.GONE );
                    ivCart.setVisibility( View.GONE );*/
                    cartList.clear();
                    if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                        getCartListApi(position);
                   /*     RecyclerView.SmoothScroller smoothScroller = new
                                LinearSmoothScroller(mActivity) {
                                    @Override protected int getVerticalSnapPreference() {
                                        return LinearSmoothScroller.SNAP_TO_START;
                                    }
                                };
                        smoothScroller.setTargetPosition(position-1);
                        recylerViewLayoutManager.startSmoothScroll(smoothScroller);*/
                    } else {
                        AppUtils.showErrorMessage( recyclerView, getString( R.string.errorInternet ), mActivity );
                    }
              //  }

              //  setCartLayout();

             /*   cartList.clear();
                if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                    getCartListApi();
                } else {
                    AppUtils.showErrorMessage( recyclerView, getString( R.string.errorInternet ), mActivity );
                }*/
            } else {
                AppUtils.hideDialog();
                AppUtils.showErrorMessage( recyclerView, String.valueOf( jsonObject.getString( "res_msg" ) ), mActivity );
            }
        } catch (Exception e) {
            AppUtils.hideDialog();
            AppUtils.showErrorMessage( recyclerView, String.valueOf( e ), mActivity );
        }

    }

    private class CartApdapter extends RecyclerView.Adapter<FavNameHolder> {

        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

        public CartApdapter(ArrayList<HashMap<String, String>> favList) {
            data = favList;
        }

        public FavNameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new FavNameHolder( LayoutInflater.from( parent.getContext() ).inflate( R.layout.layout_cartlist_item, parent, false ) );
        }

        public void onBindViewHolder(final FavNameHolder holder, final int position) {

            AnimationHelper.animatate( mActivity, holder.itemView, R.anim.alfa_animation );
            int price = Integer.parseInt( data.get( position ).get( "final_price" ) );
            int quantity = Integer.parseInt( data.get( position ).get( "quantity" ) );

            final int amount = price * quantity;

            holder.tvName.setText( (CharSequence) ((HashMap) data.get( position )).get( "product_name" ) );
            holder.tvQuantityCount.setText( (CharSequence) ((HashMap) data.get( position )).get( "quantity" ) );
           //  holder.tvGram.setText();


            if (((HashMap) data.get( position )).get( "product_discount_type" ).equals( "1" )) {
                holder.tvDiscount.setText( getString(R.string.rs) + ((HashMap) data.get( position )).get( "product_discount_amount" )+ " /-off" );
            } else if (((HashMap) data.get( position )).get( "product_discount_type" ).equals( "2" )) {
                holder.tvDiscount.setText( ((HashMap) data.get( position )).get( "product_discount_amount" ) + "%off (RS" + amount + ")" );
            } else {
                holder.tvDiscount.setText( "" );
                holder.tvDiscount.setVisibility( View.INVISIBLE );
            }
            if (((String) ((HashMap) data.get( position )).get( "final_price" )).equalsIgnoreCase( (String) ((HashMap) data.get( position )).get( "price" ) )) {
                holder.tvFPrice.setText( getString(R.string.rs) + (((HashMap) data.get( position )).get( "final_price" ) + " ("+getString(R.string.rs)+"" + amount + ")") );
                holder.tvDiscount.setText( "" );
                holder.tvPrice.setText( "" );
            } else {
                holder.tvFPrice.setText( getString(R.string.rs) + (((HashMap) data.get( position )).get( "final_price" ) + " ("+getString(R.string.rs)+"" + amount + ")") );
                holder.tvPrice.setText( getString(R.string.rs) + ((HashMap) data.get( position )).get( "price" ) );
                holder.tvPrice.setPaintFlags( holder.tvPrice.getPaintFlags() | 16 );
            }
            try {
                if (data.get( position ).get( "product_image" ) != null && !data.get( position ).get( "product_image" ).isEmpty()) {
                    Picasso.with( mActivity ).load( data.get( position ).get( "product_image" ) ).into( holder.ivPic );
                }
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }
            holder.ivSubs.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        int quant = Integer.parseInt(data.get(position).get("quantity"));
                        if (quant > 1) {
                            quant = quant - 1;
                            Vibrator vibe = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
                            vibe.vibrate(100);

                            if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                                UpdateCartApi(String.valueOf(quant), data.get(position).get("product_id"),position);
                            } else {
                                AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                            }
                        } else {
                            DeleteCartApi(data.get(position).get("cart_id"),position);
                            AppUtils.showErrorMessage(recyclerView, getString(R.string.minusQuantityError), mActivity);
                        }
                    }catch (Exception e){

                    }
                }
            } );

             holder.ivAdd.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                    int quant = Integer.parseInt( data.get( position ).get( "quantity" ) );
                    if (quant < 10) {
                        quant = quant + 1;
                        Vibrator vibe = (Vibrator) mActivity.getSystemService( Context.VIBRATOR_SERVICE );
                        vibe.vibrate( 100 );
                        if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                            UpdateCartApi( String.valueOf( quant ), data.get( position ).get( "product_id" ), position);
                        } else {
                            AppUtils.showErrorMessage( recyclerView, getString( R.string.errorInternet ), mActivity );
                        }
                    } else {
                        AppUtils.showErrorMessage( recyclerView, getString( R.string.addQuantityError )+" "+10, mActivity );
                    } }catch (Exception e){

                    }
                }
            } );
            holder.ivDelete.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                      //  DeleteCartApi( data.get(position).get("cart_id") );
                    } else {
                        AppUtils.showErrorMessage( recyclerView, getString( R.string.errorInternet ), mActivity );
                    }
                }
            } );
        }

        public int getItemCount() {
            return data.size();
        }
    }

    private class FavNameHolder extends RecyclerView.ViewHolder {
        ImageView ivPic, ivAdd, ivSubs, ivDelete;
        TextView tvDiscount;
        TextView tvFPrice;
        TextView tvName;
        TextView tvGram;
        TextView tvPrice, tvQuantityCount;
        LinearLayout linearLayout;

        public FavNameHolder(View itemView) {
            super( itemView );
            ivPic = itemView.findViewById( R.id.image_cartlist );
            ivAdd = itemView.findViewById( R.id.imageView2 );
            ivDelete = itemView.findViewById( R.id.ivDelete );
            ivSubs = itemView.findViewById( R.id.imageView3 );
            tvName = itemView.findViewById( R.id.tvProductName );
            tvFPrice = itemView.findViewById( R.id.tvFPrice );
            tvPrice = itemView.findViewById( R.id.tvPrice );
            tvGram = itemView.findViewById( R.id.tvGram );
            tvDiscount = itemView.findViewById( R.id.tvDiscount );
            tvQuantityCount = itemView.findViewById( R.id.tvQuantityCount );
            linearLayout = itemView.findViewById( R.id.linearLayout );
        }
    }
}

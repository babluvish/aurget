package com.aurget.buddha.Activity.code.Database;

public class TableSection {
    static final String SQL_CREATE_SECTION = ("CREATE TABLE section (" +
            sectionCatColumn.class_id + " VARCHAR,"
            + sectionCatColumn.section + " VARCHAR,"
            + sectionCatColumn.section_id + " VARCHAR,"
            + sectionCatColumn.status + " VARCHAR" + ")");

    public static final String section = "section";

    public enum sectionCatColumn {
        class_id,
        section,
        section_id,
        status
    }
}

package com.aurget.buddha.Activity.code.Dashboard;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources.NotFoundException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;


import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.aurget.buddha.Activity.code.Account.AccountActivity;
import com.aurget.buddha.Activity.code.Account.FavouriteListActivity;
import com.aurget.buddha.Activity.code.Adapter.MyPagerAdapter;
import com.aurget.buddha.Activity.code.Category.CategoryActivity;
import com.aurget.buddha.Activity.code.Category.SubCategoryActivity;
import com.aurget.buddha.Activity.code.Common.NotificationUtils;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Dashboard.Slider.CardPagerAdapter;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Database.DatabaseController;
import com.aurget.buddha.Activity.code.Database.TableFavourite;
import com.aurget.buddha.Activity.code.Insurance.InsuranceMainActivity;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.Main.NotificationActivity;
import com.aurget.buddha.Activity.code.Product.CartListActivity;
import com.aurget.buddha.Activity.code.Product.ProductDetailsActivity;
import com.aurget.buddha.Activity.code.Product.ProductListActivity;
import com.aurget.buddha.Activity.code.Recharge.RechargeMainActivity;
import com.aurget.buddha.Activity.code.School.SchoolMainActivity;
import com.aurget.buddha.Activity.code.Search.SearchActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import code.Common.Config;
import code.utils.AppUrls;

public class DashboardActivity extends BaseActivity implements View.OnClickListener {


    ArrayList<HashMap<String, String>> CategoryList = new ArrayList();
    ArrayList<HashMap<String, String>> FeaturedBannerList = new ArrayList();
    ArrayList<HashMap<String, String>> OfferList = new ArrayList();
    ArrayList<ArrayList<HashMap<String, String>>> ProducttList = new ArrayList();
    ArrayList<HashMap<String, String>> TopBannerList = new ArrayList();
    ArrayList<HashMap<String, String>> FeaturedProductList = new ArrayList();


    boolean loadMore=true;
    private Handler handler;
    RecyclerView recyclerView;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    public  static  int VB_PAGES ;
    public static int VB_LOOPS = 1000;
    public static int VB_FIRST_PAGE;


    // ViewPager currentpage
    private static int currentPage = 0;

    // ViewPager no offpage
    private static int NUM_PAGES = 0;
    GridLayoutManager layoutManager;
    ImageView ivCat1,ivCat2,ivCat3,ivCat4,ivDeal1,ivDeal2,ivDeal3,ivDeal4,ivDrop,ivMenu,ivSearch,ivCart,ivLogo;

    LinearLayout llCat1,llCat2,llCat3,llCat4,llDealOfDay,llMore,llOffers,llHome,llCategory,llFavourites,llProfile,llNotification;

    TextView tvHome,tvCategory,tvFavourites,tvProfile,tvNotification,tvCount;

    ImageView ivHome,ivCategory,ivFavourites,ivProfile,ivNotification;

    CardPagerAdapter mCardAdapter;

    MyPagerAdapter adapter;

    ViewPager mViewPager;

    OfferAdapter offerAdapter;

    RelativeLayout rlDeal1,rlDeal2,rlDeal3,rlDeal4,rrRecharge,rrProfile,rrLogout,rrCategory,rrSchool,rrShare,rrInsurance,rrSearch;

    private Runnable runnable;

    ScrollView scrollView;
    String offset="0";
    Timer timer;
    TextView tvCat1,tvCat2,tvCat3,tvCat4,tvClock,tvDName1,tvDName2,tvDName3,tvDName4,tvDPrice1,tvDPrice2,tvDPrice3,
            tvDPrice4,tvLogin,tvUserName,tvMain,tvViewDOD,tvAll,tvTopFeatures;

    View view;

    Typeface typeface;

    public static String regId = "";

    int FIRST_PAGE;
    int PAGES;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private LinearLayout dotsLayout;
    private ImageView[] dots;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_dashboard);
        initialise();

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);
                Log.v("postiopn123", String.valueOf(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if(dy > 0) //check for scroll down
                {
                    Log.v("ksqbmbq","1");
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        Log.v("ksqbmbq","2");

                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            GetDashboardListApi();
                            Log.v("ksqbmbq","3");
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });
        Log.v("deviceId", AppUtils.getDeviceID(getApplicationContext()));
    }

    private void initialise() {

        getWindow().setSoftInputMode(2);
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    //FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId();

                    AppSettings.putString(AppSettings.fcmId,regId);

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {

                    String message = intent.getStringExtra("message");
                    Log.v("msg", message);
                }
            }
        };

        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d( "Key: ", key + " Value: " + value);
            }
        }

        //TextView
        tvMain =  findViewById(R.id.tvHeaderText);
        tvTopFeatures =  findViewById(R.id.tvTopFeatures);

        //ViewPager
        mViewPager =  findViewById(R.id.myviewpager);
        //LinearLayout
        llMore =  findViewById(R.id.llMore);
        llDealOfDay =  findViewById(R.id.llDealOfDay);
        llOffers =  findViewById(R.id.llOffers);
        llCat1 =  findViewById(R.id.llCat1);
        llCat2 =  findViewById(R.id.llCat2);
        llCat3 =  findViewById(R.id.llCat3);
        llCat4 =  findViewById(R.id.llCat4);

        //ScrollView
        scrollView = findViewById(R.id.scroll_side_menu);

        //View
        view = findViewById(R.id.view);
        //RelativeLayout
        rlDeal1 = findViewById(R.id.rlD1);
        rlDeal2 = findViewById(R.id.rlD2);
        rlDeal3 = findViewById(R.id.rlD3);
        rlDeal4 = findViewById(R.id.rlD4);
        rrLogout = findViewById(R.id.rr_logout);
        rrRecharge =  findViewById(R.id.rr_recharge);
        rrProfile =   findViewById(R.id.rr_profile);
        rrCategory =  findViewById(R.id.rr_category);
        rrSchool =    findViewById(R.id.rrSchool);
        rrShare =      findViewById(R.id.rrShare);
        rrInsurance =  findViewById(R.id.rrInsurance);
        rrSearch =     findViewById(R.id.rlSearch);

        //ImageView
        ivMenu =    findViewById(R.id.iv_menu);
        ivDrop =    findViewById(R.id.imageView3);
        ivSearch =  findViewById(R.id.searchmain);
        ivDeal1 =   findViewById(R.id.ivProduct);
        ivDeal2 =  findViewById(R.id.ivD2);
        ivDeal3 =  findViewById(R.id.ivD3);
        ivDeal4 =  findViewById(R.id.ivD4);
        ivCat1 =  findViewById(R.id.ivCat1);
        ivCat2 =  findViewById(R.id.ivCat2);
        ivCat3 =  findViewById(R.id.ivCat3);
        ivCat4 =  findViewById(R.id.ivCat4);
        ivHome =  findViewById(R.id.ivHomeBottom);
        ivCategory = findViewById(R.id.ivCategoryBottom);
        ivFavourites =  findViewById(R.id.ivFavouritesBottom);
        ivProfile =  findViewById(R.id.ic_profile);
        ivNotification =  findViewById(R.id.ic_notification);
        ivCart =  findViewById(R.id.ivCart);
        ivLogo =  findViewById(R.id.iv_logo);

        //TextView
        tvDName1 =  findViewById(R.id.tvProductName);
        tvDName2 =  findViewById(R.id.tvD2);
        tvDName3 =  findViewById(R.id.tvD3);
        tvDName4 =  findViewById(R.id.tvD4);
        tvDPrice1 =  findViewById(R.id.tvFPrice);
        tvDPrice2 =  findViewById(R.id.tvDPrice2);
        tvDPrice3 =  findViewById(R.id.tvDPrice3);
        tvDPrice4 =  findViewById(R.id.tvDPrice4);
        tvCat1 =  findViewById(R.id.tvCat1);
        tvCat2 =  findViewById(R.id.tvCat2);
        tvCat3 =  findViewById(R.id.tvCat3);
        tvCat4 =  findViewById(R.id.tvCat4);
        tvClock =  findViewById(R.id.tvClock);
        tvViewDOD =  findViewById(R.id.tvViewDOD);
        tvAll =  findViewById(R.id.tvAll);
        tvHome =  findViewById(R.id.tvHomeBottom);
        tvCategory =findViewById(R.id.tvCategoryBottom);
        tvFavourites =  findViewById(R.id.tvFavouritesBottom);
        tvProfile =  findViewById(R.id.tvProfileBottom);
        tvNotification =  findViewById(R.id.tvNotificationBottom);
        tvCount =  findViewById(R.id.tvCount);
        tvLogin =  findViewById(R.id.tv_login);
        tvUserName = findViewById(R.id.tvUserName);

        //LinearLayout fro bottom views
        llHome =  findViewById(R.id.llHome);
        llCategory = findViewById(R.id.llCategory);
        llFavourites =  findViewById(R.id.llFavourites);
        llProfile =  findViewById(R.id.llProfile);
        llNotification =  findViewById(R.id.llNotification);

        rrSearch.setVisibility(View.VISIBLE);
        //Recyclerview
        recyclerView=findViewById(R.id.recyclerview);

        dotsLayout = findViewById(R.id.layoutDots);
        // recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        layoutManager = new GridLayoutManager(mActivity, 2);
        recyclerView.setLayoutManager(layoutManager);
       // recyclerView.setNestedScrollingEnabled(false);


        //SetOnClickListener
        llHome.setOnClickListener(this);
        llCategory.setOnClickListener(this);
        llFavourites.setOnClickListener(this);
        llProfile.setOnClickListener(this);
        llNotification.setOnClickListener(this);

        tvTopFeatures.setOnClickListener(this);
        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        llMore.setOnClickListener(this);
        view.setOnClickListener(this);
        rlDeal1.setOnClickListener(this);
        rlDeal2.setOnClickListener(this);
        rlDeal3.setOnClickListener(this);
        rlDeal4.setOnClickListener(this);
        tvViewDOD.setOnClickListener(this);
        llCat1.setOnClickListener(this);
        llCat2.setOnClickListener(this);
        llCat3.setOnClickListener(this);
        llCat4.setOnClickListener(this);
        ivCart.setOnClickListener(this);
        tvAll.setOnClickListener(this);
        rrLogout.setOnClickListener(this);
        rrRecharge.setOnClickListener(this);
        rrProfile.setOnClickListener(this);
        rrCategory.setOnClickListener(this);
        rrSchool.setOnClickListener(this);
        rrShare.setOnClickListener(this);
        rrInsurance.setOnClickListener(this);

        ivLogo.setVisibility(View.VISIBLE);
        ivMenu.setVisibility(View.GONE);

        typeface = Typeface.createFromAsset(mActivity.getAssets(), "centurygothic.otf");


        int count = Integer.parseInt(AppSettings.getString(AppSettings.cartCount));

        if(count>0)
        {
            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(String.valueOf(count));
        }
        else
        {
            tvCount.setVisibility(View.VISIBLE);
        }

        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            GetDashboardListApi();
            sendFCMApi();
        } else {
            AppUtils.showErrorMessage(tvMain, getString(R.string.errorInternet), mActivity);
        }

        getCatData();
    }


    public void topBannerAdapter() {

        currentPage++;
        NUM_PAGES = TopBannerList.size();

        VB_PAGES=TopBannerList.size();
        VB_FIRST_PAGE= VB_PAGES / 2;

       // adapter=new MyPagerAdapter(getApplicationContext(),getSupportFragmentManager(),TopBannerList);

        mCardAdapter = new CardPagerAdapter(getApplication(), TopBannerList) {
            protected void onCategoryClick(View v, String position) {

                if(TopBannerList.get(Integer.parseInt(position)).get("banner_on").equals("1"))
                {
                    Intent mIntent = new Intent(mActivity, ProductListActivity.class);
                    mIntent.putExtra(AppConstants.subCategoryId, TopBannerList.get(Integer.parseInt(position)).get("id"));
                    mIntent.putExtra(AppConstants.from, "1");
                    startActivity(mIntent);
                }
                else  if(TopBannerList.get(Integer.parseInt(position)).get("banner_on").equals("2"))
                {
                    Intent mIntent = new Intent(mActivity, ProductDetailsActivity.class);
                    mIntent.putExtra(AppConstants.productId, TopBannerList.get(Integer.parseInt(position)).get("id"));
                    mIntent.putExtra(AppConstants.from, "1");
                    startActivity(mIntent);
                }
            }
        };
        mViewPager.setAdapter(mCardAdapter);

        addBottomDots(0);
        mViewPager.setPageTransformer(false,adapter);
        mViewPager.setCurrentItem(currentPage,true);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setClipToPadding(false);
        mViewPager.setPadding(40, 10, 40, 0);

        int margin=getResources().getDimensionPixelSize(R.dimen._10sdp);
        mViewPager.setPageMargin(margin);
       // mViewPager.setElevation(10f);


        mViewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                int pageWidth = mViewPager.getMeasuredWidth() -
                        mViewPager.getPaddingLeft() - mViewPager.getPaddingRight();
                int pageHeight = mViewPager.getHeight();
                int paddingLeft = mViewPager.getPaddingLeft();
                float transformPos = (float) (page.getLeft() -
                        (mViewPager.getScrollX() + paddingLeft)) / pageWidth;
                int max = pageHeight / 10;
                if (transformPos < -1)
                {

                    page.setScaleY(0.8f);
                }
                else if (transformPos <= 1)
                {

                    page.setScaleY(1f);
                }
                else
                {

                    page.setScaleY(0.8f);
                }
            }
        });
    }



    private void addBottomDots(final int currentPage) {

        dots = new ImageView[TopBannerList.size()];
        dotsLayout.removeAllViews();

        for (int i = 0; i < TopBannerList.size(); i++) {
            dots[i] = new ImageView(getApplicationContext());

            dots[i].setImageResource(R.drawable.ic_circle_white);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(20,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(5, 0, 5, 0);

            dotsLayout.addView(dots[i],params);

        }
        if (TopBannerList.size() > 0)
            dots[currentPage].setImageResource(R.drawable.ic_circle_blue);

    }

    public void onClick(View v) {
        Intent mIntent;
        switch (v.getId()) {
            case R.id.view:
                ivDrop.setVisibility(View.GONE);
                //scrollView.setVisibility(View.GONE);
                view.setVisibility(View.GONE);
                AppUtils.collapse(scrollView);
                break;
                case R.id.tvTopFeatures:
             //  startActivity(new Intent(mActivity, TopFeaturedActivity.class));
                break;
            case R.id.llCat1:
           /*    mIntent = new Intent(mActivity, SubCategoryActivity.class);
              //  mIntent.putExtra(AppConstants.subCategoryId, (String) ((HashMap) CategoryList.get(0)).get("sub_category_id"));
              //  mIntent.putExtra(AppConstants.from, "4");
                startActivity(mIntent);*/
                mIntent = new Intent(mActivity, SubCategoryActivity.class);
                SubCategoryActivity.categoryId = llCat1.getTag().toString();
                startActivity(mIntent);
                return;
                case R.id.tvAll:
                   startActivity(new Intent(getBaseContext(), CategoryActivity.class));
                return;
            case R.id.llCat2:
              /*  mIntent = new Intent(mActivity, SubCategoryActivity.class);
               // mIntent.putExtra(AppConstants.subCategoryId, (String) ((HashMap) CategoryList.get(1)).get("sub_category_id"));
               // mIntent.putExtra(AppConstants.from, "4");
                startActivity(mIntent);*/
                mIntent = new Intent(mActivity, SubCategoryActivity.class);
                SubCategoryActivity.categoryId = llCat2.getTag().toString();
                startActivity(mIntent);
                break;
            case R.id.llCat3:
                mIntent = new Intent(mActivity, SubCategoryActivity.class);
                SubCategoryActivity.categoryId = llCat3.getTag().toString();
                startActivity(mIntent);
                break;
            case R.id.llCat4:
                mIntent = new Intent(mActivity, SubCategoryActivity.class);
                SubCategoryActivity.categoryId = llCat4.getTag().toString();
                startActivity(mIntent);
                break;
            case R.id.llMore:
                startActivity(new Intent(getBaseContext(), CategoryActivity.class));
                break;
            case R.id.tvViewDOD:
                mIntent = new Intent(mActivity, ProductListActivity.class);
                mIntent.putExtra(AppConstants.subCategoryId, "");
                mIntent.putExtra(AppConstants.from, "2");
                startActivity(mIntent);
                break;
            case R.id.iv_menu:
                if (scrollView.getVisibility() == View.VISIBLE) {
                    ivDrop.setVisibility(View.GONE);//scrollView.setVisibility(View.GONE);
                    view.setVisibility(View.GONE);
                    AppUtils.collapse(scrollView);
                    break;
                }
                ivDrop.setVisibility(View.VISIBLE);
                view.setVisibility(View.VISIBLE);
                AppUtils.expand(scrollView);
                break;
            case R.id.searchmain:
                if (scrollView.getVisibility() == View.VISIBLE) {
                    ivDrop.setVisibility(View.GONE);
                    view.setVisibility(View.GONE);
                    AppUtils.collapse(scrollView);
                    break;
                }
                startActivity(new Intent(getBaseContext(), SearchActivity.class));
                break;

            case R.id.llCategory:

                tvHome.setTextColor(Color.parseColor("#9d9b9b"));
                tvCategory.setTextColor(Color.parseColor("#155b72"));
                tvFavourites.setTextColor(Color.parseColor("#9d9b9b"));
                tvProfile.setTextColor(Color.parseColor("#9d9b9b"));
                tvNotification.setTextColor(Color.parseColor("#9d9b9b"));
                ivHome.setImageResource(R.drawable.ic_home_grey);
                ivCategory.setImageResource(R.drawable.ic_categories_darkgreen);
                ivFavourites.setImageResource(R.drawable.ic_heart_grey);
                ivNotification.setImageResource(R.drawable.ic_notifications_grey);
                ivProfile.setImageResource(R.drawable.ic_profile_grey);

                startActivity(new Intent(getBaseContext(), CategoryActivity.class));

                break;

            case R.id.llFavourites:

                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else {
                    tvHome.setTextColor(Color.parseColor("#9d9b9b"));
                    tvCategory.setTextColor(Color.parseColor("#9d9b9b"));
                    tvFavourites.setTextColor(Color.parseColor("#155b72"));
                    tvProfile.setTextColor(Color.parseColor("#9d9b9b"));
                    tvNotification.setTextColor(Color.parseColor("#9d9b9b"));
                    ivHome.setImageResource(R.drawable.ic_home_grey);
                    ivCategory.setImageResource(R.drawable.ic_categories_grey);
                    ivFavourites.setImageResource(R.drawable.ic_heart_darkgreen);
                    ivNotification.setImageResource(R.drawable.ic_notifications_grey);
                    ivProfile.setImageResource(R.drawable.ic_profile_grey);

                    startActivity(new Intent(getBaseContext(), FavouriteListActivity.class));
                }

                break;

            case R.id.llNotification:
                tvHome.setTextColor(Color.parseColor("#9d9b9b"));
                tvCategory.setTextColor(Color.parseColor("#9d9b9b"));
                tvFavourites.setTextColor(Color.parseColor("#9d9b9b"));
                tvProfile.setTextColor(Color.parseColor("#9d9b9b"));
                tvNotification.setTextColor(Color.parseColor("#155b72"));
                ivHome.setImageResource(R.drawable.ic_home_grey);
                ivCategory.setImageResource(R.drawable.ic_categories_grey);
                ivFavourites.setImageResource(R.drawable.ic_heart_grey);
                ivNotification.setImageResource(R.drawable.ic_notifications_green);
                ivProfile.setImageResource(R.drawable.ic_profile_grey);

                startActivity(new Intent(getBaseContext(), NotificationActivity.class));

                break;

            case R.id.llProfile:

                    tvHome.setTextColor(Color.parseColor("#9d9b9b"));
                    tvCategory.setTextColor(Color.parseColor("#9d9b9b"));
                    tvFavourites.setTextColor(Color.parseColor("#9d9b9b"));
                    tvNotification.setTextColor(Color.parseColor("#9d9b9b"));
                    tvProfile.setTextColor(Color.parseColor("#155b72"));
                    ivHome.setImageResource(R.drawable.ic_home_grey);
                    ivCategory.setImageResource(R.drawable.ic_categories_grey);
                    ivFavourites.setImageResource(R.drawable.ic_heart_grey);
                    ivProfile.setImageResource(R.drawable.ic_profile_darkgreen);
                    ivNotification.setImageResource(R.drawable.ic_notifications_grey);

                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else
                {
                    startActivity(new Intent(getBaseContext(), AccountActivity.class));
                }

                break;

            case R.id.rr_profile:

                ivDrop.setVisibility(View.GONE);
                view.setVisibility(View.GONE);
                AppUtils.collapse(scrollView);

                startActivity(new Intent(getBaseContext(), LoginActivity.class));
                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else
                {
                    startActivity(new Intent(getBaseContext(), AccountActivity.class));
                }

                break;


            case R.id.rlD1:

                try {
                    mIntent = new Intent(mActivity, ProductDetailsActivity.class);
                    mIntent.putExtra(AppConstants.productId, rlDeal1.getTag().toString());
                    mIntent.putExtra(AppConstants.from, "2");
                    startActivity(mIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case R.id.rlD2:

                try {
                    mIntent = new Intent(mActivity, ProductDetailsActivity.class);
                    mIntent.putExtra(AppConstants.productId, rlDeal2.getTag().toString());
                    mIntent.putExtra(AppConstants.from, "2");
                    startActivity(mIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case R.id.rlD3:

                try {
                    mIntent = new Intent(mActivity, ProductDetailsActivity.class);
                    mIntent.putExtra(AppConstants.productId, rlDeal3.getTag().toString());
                    mIntent.putExtra(AppConstants.from, "2");
                    startActivity(mIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case R.id.rlD4:

                try {
                    mIntent = new Intent(mActivity, ProductDetailsActivity.class);
                    mIntent.putExtra(AppConstants.productId, rlDeal4.getTag().toString());
                    mIntent.putExtra(AppConstants.from, "2");
                    startActivity(mIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case R.id.ivCart:

                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else
                {
                    startActivity(new Intent(getBaseContext(), CartListActivity.class));
                }

                break;

            case R.id.rr_logout:

                ivDrop.setVisibility(View.GONE);
                view.setVisibility(View.GONE);
                AppUtils.collapse(scrollView);

                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else
                {
                    AlertPopUp();
                }

                return;

            case R.id.rr_recharge:

                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else
                {
                    ivDrop.setVisibility(View.GONE);
                    view.setVisibility(View.GONE);
                    AppUtils.collapse(scrollView);
                    startActivity(new Intent(getBaseContext(), RechargeMainActivity.class));
                }

                break;

            case R.id.rrInsurance:
                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else
                {
                    ivDrop.setVisibility(View.GONE);
                    view.setVisibility(View.GONE);
                    AppUtils.collapse(scrollView);
                    startActivity(new Intent(getBaseContext(), InsuranceMainActivity.class));
                }

                break;

            case R.id.rrSchool:

                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else
                {
                    ivDrop.setVisibility(View.GONE);
                    view.setVisibility(View.GONE);
                    AppUtils.collapse(scrollView);
                    startActivity(new Intent(getBaseContext(), SchoolMainActivity.class));
                }

                break;
            case R.id.rrShare:
                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else
                {
                    ivDrop.setVisibility(View.GONE);
                    view.setVisibility(View.GONE);
                    AppUtils.collapse(scrollView);

                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT,
                            "Hey check our new Shopping app at: https://play.google.com/store/apps/details?id=com.mrnmrsekart " +
                                    ".Use my Referral Id: "+AppSettings.getString(AppSettings.referralId)+" and get CASHBACK on your First Purchase.");
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);
                }

                return;


            case R.id.rr_category:

                ivDrop.setVisibility(View.GONE);
                view.setVisibility(View.GONE);
                AppUtils.collapse(scrollView);

                tvHome.setTextColor(getResources().getColor(R.color.dark_grey));
                tvCategory.setTextColor(getResources().getColor(R.color.textcolor));
                tvFavourites.setTextColor(getResources().getColor(R.color.dark_grey));
                tvProfile.setTextColor(getResources().getColor(R.color.dark_grey));
                tvNotification.setTextColor(getResources().getColor(R.color.dark_grey));
                ivHome.setImageResource(R.drawable.ic_home_grey);
                ivCategory.setImageResource(R.drawable.ic_categories_darkgreen);
                ivFavourites.setImageResource(R.drawable.ic_heart_grey);
                ivProfile.setImageResource(R.drawable.ic_profile_grey);
                ivNotification.setImageResource(R.drawable.ic_notifications_grey);

                startActivity(new Intent(getBaseContext(), CategoryActivity.class));

                break;

            default:
                break;
        }
    }

    public void countDownStart() {
        final String reg_date = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
        handler = new Handler();
        runnable = new Runnable() {
            public void run() {
                handler.postDelayed(this, 1000);
                try {
                    Date eventDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(reg_date + " 23:59:59");
                    Date currentDate = new Date();
                    if (currentDate.after(eventDate)) {
                        tvClock.setText("Deal Ended");
                        handler.removeCallbacks(runnable);
                        AppUtils.showErrorMessage(tvMain, "Deal Ended", mActivity);
                        return;
                    }
                    long diff = eventDate.getTime() - currentDate.getTime();
                    long days = diff / 86400000;
                    diff -= 86400000 * days;
                    long hours = diff / 3600000;
                    diff -= 3600000 * hours;
                    long minutes = diff / 60000;
                    long seconds = (diff - (60000 * minutes)) / 1000;
                    String hr = String.valueOf(hours);
                    if (days < 10) {
                        hr = "0" + String.valueOf(hours);
                    } else {
                        hr = String.valueOf(hours);
                    }
                    tvClock.setText(hr + ":" + minutes + ":" + seconds);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 0);
    }

    public void getCatData() {

        int j=0;

        CategoryList.clear();
        CategoryList.addAll(DatabaseController.getFeaturedCategoryData());
       // CategoryList.addAll(DatabaseController.getSubCategoryData(TableSubCategory.sub_category, TableSubCategory.subCatColumn.category_id.toString(), "4"));
        for (int i = 0; i < CategoryList.size(); i++) {
            if (j== 0) {
                j=j+1;
                llCat1.setVisibility(View.VISIBLE);
                llCat1.setTag(((HashMap) CategoryList.get(i)).get("category_id"));
                Log.v("vegetables", (String) ((HashMap) CategoryList.get(i)).get("category_name"));
                tvCat1.setText(AppUtils.split((String) ((HashMap) CategoryList.get(i)).get("category_name")));
                if (((String) ((HashMap) CategoryList.get(i)).get("app_icon")).isEmpty()) {
                    Picasso.with(mActivity).load((int) R.mipmap.ic_launcher).placeholder((int) R.mipmap.ic_launcher).into(ivCat1);
                } else {
                    Picasso.with(mActivity).load((String) ((HashMap) CategoryList.get(i)).get("app_icon")).placeholder((int) R.mipmap.ic_launcher).into(ivCat1);
                }
            }
            else if (j== 1) {
                j=j+1;
                llCat2.setVisibility(View.VISIBLE);
                llCat2.setTag(((HashMap) CategoryList.get(i)).get("category_id"));
                tvCat2.setText(AppUtils.split((String) ((HashMap) CategoryList.get(i)).get("category_name")));
                if (((String) ((HashMap) CategoryList.get(i)).get("app_icon")).isEmpty()) {
                    Picasso.with(mActivity).load((int) R.mipmap.ic_launcher).placeholder((int) R.mipmap.ic_launcher).into(ivCat2);
                } else {
                    Picasso.with(mActivity).load((String) ((HashMap) CategoryList.get(i)).get("app_icon")).placeholder((int) R.mipmap.ic_launcher).into(ivCat2);
                }

            }
            else if (j == 2) {
                j=j+1;
                llCat3.setVisibility(View.VISIBLE);
                llCat3.setTag(((HashMap) CategoryList.get(i)).get("category_id"));
                tvCat3.setText(AppUtils.split((String) ((HashMap) CategoryList.get(i)).get("category_name")));
                if (((String) ((HashMap) CategoryList.get(i)).get("app_icon")).isEmpty()) {
                    Picasso.with(mActivity).load((int) R.mipmap.ic_launcher).placeholder((int) R.mipmap.ic_launcher).into(ivCat3);
                } else {
                    Picasso.with(mActivity).load((String) ((HashMap) CategoryList.get(i)).get("app_icon")).placeholder((int) R.mipmap.ic_launcher).into(ivCat3);
                }

            }
            else if (j == 3) {
                j=j+1;
                llCat4.setVisibility(View.VISIBLE);
                llCat4.setTag(((HashMap) CategoryList.get(i)).get("category_id"));
                tvCat4.setText(AppUtils.split((String) ((HashMap) CategoryList.get(i)).get("category_name")));
                if (((String) ((HashMap) CategoryList.get(i)).get("app_icon")).isEmpty()) {
                    Picasso.with(mActivity).load((int) R.mipmap.ic_launcher).placeholder((int) R.mipmap.ic_launcher).into(ivCat4);
                } else {
                    Picasso.with(mActivity).load((String) ((HashMap) CategoryList.get(i)).get("app_icon")).placeholder((int) R.mipmap.ic_launcher).into(ivCat4);
                }

            }
        }
    }

    public void setAdapter() {
        llOffers.removeAllViews();

            for(int i=0;i<OfferList.size();i++) {

                LayoutInflater inflater2 = null;
                inflater2 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View mLinearView = inflater2.inflate(R.layout.inflate_offer, null);

                LinearLayout llSponsor =  mLinearView.findViewById(R.id.llSponsor);
                TextView tvOfferName =  mLinearView.findViewById(R.id.tvOfferName);
                final TextView tvViewAll =  mLinearView.findViewById(R.id.textView5);
                RecyclerView productRecyclerView =  mLinearView.findViewById(R.id.productRecyclerView);
                ImageView ivSponsorPic =  mLinearView.findViewById(R.id.imageView2);
                productRecyclerView.setLayoutManager(new GridLayoutManager(mActivity, 2));
                if (FeaturedBannerList.size() > i) {
                    llSponsor.setVisibility(View.VISIBLE);
                    try {
                        if (!(((HashMap) FeaturedBannerList.get(i)).get("banner_image") == null || ((String) ((HashMap) FeaturedBannerList.get(i)).get("banner_image")).isEmpty())) {
                            Picasso.with(mActivity).load((String) ((HashMap) FeaturedBannerList.get(i)).get("banner_image")).placeholder((int) R.mipmap.ic_launcher).into(ivSponsorPic);
                        }
                    } catch (NotFoundException e) {
                        e.printStackTrace();
                    }
                } else {
                    llSponsor.setVisibility(View.GONE);
                }
                tvViewAll.setTag(((HashMap) OfferList.get(i)).get("sub_category_id"));

                tvOfferName.setText((CharSequence) ((HashMap) OfferList.get(i)).get("offer_name"));

                if (OfferList.size() > 3) {
                    tvViewAll.setVisibility(View.VISIBLE);
                } else {
                    tvViewAll.setVisibility(View.GONE);
                }

                tvViewAll.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        String id = String.valueOf(tvViewAll.getTag());

                        Intent mIntent = new Intent(mActivity, ProductListActivity.class);
                        mIntent.putExtra(AppConstants.subCategoryId, id);
                        mIntent.putExtra(AppConstants.from, "3");
                        startActivity(mIntent);
                    }
                });

               /* offerAdapter = new OfferAdapter((ArrayList) ProducttList.get(i));
                layoutManager = new GridLayoutManager(mActivity,2);
                productRecyclerView.setLayoutManager(layoutManager);
                productRecyclerView.setAdapter(offerAdapter);
                productRecyclerView.setNestedScrollingEnabled(true);
                llOffers.addView(mLinearView);*/

            }
    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable(){

                @Override
                public void run() {
                    if (currentPage == NUM_PAGES) {
                        currentPage =1;
                    }
                    mViewPager.setCurrentItem(currentPage++, true);
                   // mViewPager.setCurrentItem((mViewPager.getCurrentItem() + 1) % TopBannerList.size());

                   // mViewPager.post(new MyTimerTask());
                }});
        }

    }

    public class OfferAdapter extends RecyclerView.Adapter<OfferHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList();

        public OfferAdapter(ArrayList<HashMap<String, String>> favList) {
            data = favList;
        }
        @Override
        public OfferHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_product, parent, false);
            return new OfferHolder(itemView);
        }
        public void onBindViewHolder(OfferHolder holder, final int position) {
            /*holder.tvName.setTypeface(typeface);
            holder.tvDiscount.setTypeface(typeface);
            holder.tvPrice.setTypeface(typeface);
            holder.tvFPrice.setTypeface(typeface);*/

            holder.tvName.setText((CharSequence) ((HashMap) data.get(position)).get("product_name"));
            if (((String) ((HashMap) data.get(position)).get("product_discount_type")).equals("1")) {
                holder.tvDiscount.setText("RS"+" "+ ((String) ((HashMap) data.get(position)).get("product_discount_amount")) + " off");
            } else if (((String) ((HashMap) data.get(position)).get("product_discount_type")).equals("2")) {
                holder.tvDiscount.setText(((String) ((HashMap) data.get(position)).get("product_discount_amount")) + "% off");
            } else {
                holder.tvDiscount.setText("");
                holder.tvDiscount.setVisibility(View.INVISIBLE);
            }
            if (((String) ((HashMap) data.get(position)).get("final_price")).equalsIgnoreCase((String) ((HashMap) data.get(position)).get("price"))) {
                holder.tvFPrice.setText("RS" +" "+ ((String) ((HashMap) data.get(position)).get("final_price")));
                holder.tvDiscount.setText("");
                holder.tvPrice.setText("");
            } else {
                holder.tvFPrice.setText("RS"+" " + ((String) ((HashMap) data.get(position)).get("final_price")));
                holder.tvPrice.setText("RS"+" "+ ((String) ((HashMap) data.get(position)).get("price")));
           //     holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.ANTI_ALIAS_FLAG);
                holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | 16);
            }
            try {
                if (((HashMap) data.get(position)).get("product_image") != null && !((String) ((HashMap) data.get(position)).get("product_image")).isEmpty()) {
                    Picasso.with(mActivity).load((String) ((HashMap) data.get(position)).get("product_image")).placeholder((int) R.mipmap.ic_launcher).into(holder.ivPic);
                }
            } catch (NotFoundException e) {
                e.printStackTrace();
            }

            if(DatabaseController.checkRecordExist(TableFavourite.favourite, String.valueOf(TableFavourite.favouriteColumn.productId),data.get(position).get("id")))
            {
                holder.icWishlist.setImageResource(R.drawable.ic_heart_red);
            }
            else
            {
                holder.icWishlist.setImageResource(R.drawable.ic_heart_grey);
            }

            holder.icWishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(!AppSettings.getString(AppSettings.userId).isEmpty())
                    {
                        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                            addFavApi(data.get(position).get("id"));
                        } else {
                            AppUtils.showErrorMessage(tvMain, getString(R.string.errorInternet), mActivity);
                        }
                    }
                    else {
                        AppUtils.showErrorMessage(tvMain, "Kindly login first", mActivity);
                    }

                }
            });

            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent mIntent = new Intent(mActivity, ProductDetailsActivity.class);
                    mIntent.putExtra(AppConstants.productId, data.get(position).get("id"));
                    mIntent.putExtra(AppConstants.from, "3");
                    startActivity(mIntent);

                }
            });
        }

        public int getItemCount() {
            return data.size();
        }
    }

    private class OfferHolder extends RecyclerView.ViewHolder {
        ImageView ivPic, icWishlist;
        LinearLayout linearLayout;
        TextView tvDiscount;
        TextView tvFPrice;
        TextView tvName;
        TextView tvPrice;

        public OfferHolder(View itemView) {
            super(itemView);
            linearLayout =  itemView.findViewById(R.id.layout_item);
            ivPic =  itemView.findViewById(R.id.ivProduct);
            icWishlist =  itemView.findViewById(R.id.ic_wishlist);
            tvName =  itemView.findViewById(R.id.tvProductName);
            tvFPrice =  itemView.findViewById(R.id.tvFPrice);
            tvPrice =  itemView.findViewById(R.id.tvPrice);
            tvDiscount =  itemView.findViewById(R.id.tvDiscount);
        }
    }

    private boolean exit = false;

    @Override
    public void onBackPressed() {
        AppUtils.hideSoftKeyboard(mActivity);
        if (view.getVisibility() == View.VISIBLE) {
            ivDrop.setVisibility(View.GONE);
            //scrollView.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
            AppUtils.collapse(scrollView);
        } else {
            if (exit) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                AppUtils.showErrorMessage(tvViewDOD, getString(R.string.existAppMessage),mActivity);
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 3 * 1000);

            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        if(AppSettings.getString(AppSettings.name).equals(""))
        {
            tvUserName.setText(getString(R.string.hi));
        }
        else
        {
            tvUserName.setText(AppSettings.getString(AppSettings.name));
        }

        tvHome.setTextColor(getResources().getColor(R.color.textcolor));
        tvCategory.setTextColor(getResources().getColor(R.color.dark_grey));
        tvFavourites.setTextColor(getResources().getColor(R.color.dark_grey));
        tvProfile.setTextColor(getResources().getColor(R.color.dark_grey));
        tvNotification.setTextColor(getResources().getColor(R.color.dark_grey));
        ivHome.setImageResource(R.drawable.ic_home_darkgreen);
        ivCategory.setImageResource(R.drawable.ic_categories_grey);
        ivFavourites.setImageResource(R.drawable.ic_heart_grey);
        ivNotification.setImageResource(R.drawable.ic_notifications_grey);
        ivProfile.setImageResource(R.drawable.ic_profile_grey);
        int count = Integer.parseInt(AppSettings.getString(AppSettings.cartCount));

        if(count>0)
        {
            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(String.valueOf(count));
        }
        else
        {
            tvCount.setVisibility(View.VISIBLE);
        }

        if(AppSettings.getString(AppSettings.userId).isEmpty())
        {
            tvLogin.setText("LoginActivity");
        }
        else if(AppSettings.getString(AppSettings.verified).equals("1"))
        {
            tvLogin.setText("Logout");
        }
        else
        {
            tvLogin.setText("LoginActivity");
        }

        setAdapter();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(mActivity).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));
        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(mActivity).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }



    //AlertPopUp
    private void AlertPopUp() {

        final Dialog dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alertyesno);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();

        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        //TextView
        TextView tvYes                              = dialog.findViewById(R.id.tvOk);
        TextView tvCancel                       =  dialog.findViewById(R.id.tvcancel);
        TextView tvReason                       =  dialog.findViewById(R.id.textView22);
        TextView tvAlertMsg                       = dialog.findViewById(R.id.tvAlertMsg);

        tvAlertMsg.setText("Confirmation");

        tvReason.setText("You will log out from the device. Are you sure?");

        dialog.show();

        tvYes.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                dialog.dismiss();

                AppSettings.putString(AppSettings.mobile,"");
                AppSettings.putString(AppSettings.verified,"");
                AppSettings.putString(AppSettings.userId,"");
                AppSettings.putString(AppSettings.cartCount,"0");
                onResume();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }


    //==================================API=================================================//

    private void GetDashboardListApi() {
        Log.v("GetDashboardListApi", AppUrls.getDashboard);

        AppUtils.showRequestDialog(mActivity);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("offset", offset);
            if(AppSettings.getString(AppSettings.userId) .isEmpty()){
                json_data.put( "user_id","0");
            }else{
                json_data.put( "user_id",AppSettings.getString(AppSettings.userId) );
            }
            json.put(AppConstants.result, json_data);
            Log.v("featuredApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.getDashboard)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                       // if (Build.VERSION.SDK_INT <Build.VERSION_CODES.O) {
                            parseJsondata(response);
                      //  }
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvCat4, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvCat4, String.valueOf(error.getErrorDetail()), mActivity);
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            //Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        }
                    }
                });
    }

    private void parseJsondata(JSONObject response) {
        Log.d("response ", response.toString());
        AppUtils.hideDialog();
        TopBannerList.clear();
        FeaturedBannerList.clear();
        OfferList.clear();
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                offset=jsonObject.getString("offset");
                int i;
                JSONArray bannerArray = jsonObject.getJSONArray("banner");
                for (i = 0; i < bannerArray.length(); i++) {
                    JSONObject bannerArrayJSONObject = bannerArray.getJSONObject(i);
                    HashMap<String, String> bannerlist = new HashMap();
                    bannerlist.put("banner_id", bannerArrayJSONObject.getString("banner_id"));
                    bannerlist.put("banner_on", bannerArrayJSONObject.getString("banner_on"));
                    bannerlist.put("id", bannerArrayJSONObject.getString("id"));
                    bannerlist.put("featured_banner", bannerArrayJSONObject.getString("featured_banner"));
                    bannerlist.put("banner_image", bannerArrayJSONObject.getString("banner_image"));
                    if (bannerArrayJSONObject.getString("featured_banner").equals("1")) {
                        TopBannerList.add(bannerlist);
                    } else {
                        FeaturedBannerList.add(bannerlist);
                    }
                }
                JSONArray productArray = jsonObject.getJSONArray("product_master");
                for (i = 0; i < productArray.length(); i++) {
                    JSONObject productArrayJSONObject = productArray.getJSONObject(i);
                    HashMap<String, String> productlist = new HashMap();
                    productlist.put("id", productArrayJSONObject.getString("id"));
                    productlist.put("category_id", productArrayJSONObject.getString("category_master_id"));
                    productlist.put("sub_category_master_id", productArrayJSONObject.getString("sub_category_master_id"));
                    productlist.put("product_name", productArrayJSONObject.getString("product_name"));
                    productlist.put("product_image", productArrayJSONObject.getString("image"));
                    productlist.put("price", productArrayJSONObject.getString("price"));
                    productlist.put("final_price", productArrayJSONObject.getString("final_price"));
                    productlist.put("product_discount_type", productArrayJSONObject.getString("product_discount_type"));
                    productlist.put("product_discount_amount", productArrayJSONObject.getString("product_discount_amount"));
                    FeaturedProductList.add(productlist);
                }
                        offerAdapter = new OfferAdapter(FeaturedProductList);
                        recyclerView.setAdapter(offerAdapter);
                        recyclerView.setNestedScrollingEnabled(true);
               /* JSONArray offerArray = jsonObject.getJSONArray("offer");
                for (i = 0; i < offerArray.length(); i++) {
                    JSONObject offerArrayJSONObject = offerArray.getJSONObject(i);
                    HashMap<String, String> offerlist = new HashMap();
                    String string = offerArrayJSONObject.getString("id");
                    offerlist.put("id", string);
                    string = offerArrayJSONObject.getString("sub_category_id");
                    offerlist.put("sub_category_id", string);
                    string = offerArrayJSONObject.getString("sub_category_name");
                    offerlist.put("sub_category_name", string);
                    string = offerArrayJSONObject.getString("offer_name");
                    offerlist.put("offer_name", string);
                    OfferList.add(offerlist);

                    ArrayList<HashMap<String, String>> AryList = new ArrayList();
                    JSONArray productsArray = offerArrayJSONObject.getJSONArray("products");
                    for (int j = 0; j < productsArray.length(); j++) {
                        JSONObject productJSONObject = productsArray.getJSONObject(j);
                        HashMap<String, String> productlist = new HashMap();
                        string = productJSONObject.getString("id");
                        productlist.put("id", string);
                        string = productJSONObject.getString("category_id");
                        productlist.put("category_id", string);
                        string = productJSONObject.getString("sub_category_master_id");
                        productlist.put("sub_category_master_id", string);
                        string = productJSONObject.getString("product_name");
                        productlist.put("product_name", string);
                        string = productJSONObject.getString("product_image");
                        productlist.put("product_image", string);
                        string = productJSONObject.getString("price");
                        productlist.put("price", string);
                        string = productJSONObject.getString("final_price");
                        productlist.put("final_price", string);
                        string = productJSONObject.getString("product_discount_type");
                        productlist.put("product_discount_type", string);
                        string = productJSONObject.getString("product_discount_amount");
                        productlist.put("product_discount_amount", string);
                        AryList.add(productlist);
                    }
                    ProducttList.add(AryList);
                }

                setAdapter();

                JSONArray dodArray = jsonObject.getJSONArray("DOD");

                if (dodArray.length() > 0) {

                    llDealOfDay.setVisibility(View.VISIBLE);

                    for (i = 0; i < dodArray.length(); i++) {
                        JSONObject dodArrayJSONObject = dodArray.getJSONObject(i);
                        if (i == 0) {
                            rlDeal1.setTag(dodArrayJSONObject.getString("product_id"));
                            if (dodArrayJSONObject.getString("product_image").isEmpty()) {
                                Picasso.with(mActivity).load((int) R.mipmap.ic_launcher).into(ivDeal1);
                            } else {
                                Picasso.with(mActivity).load(dodArrayJSONObject.getString("product_image")).into(ivDeal1);
                            }
                            tvDName1.setText(dodArrayJSONObject.getString("product_name"));
                            if (dodArrayJSONObject.getString("product_discount_type").equals("1")) {
                                tvDPrice1.setText("RS " + dodArrayJSONObject.getString("final_price") + " after RS " + dodArrayJSONObject.getString("product_discount_amount") + " Off");
                            } else {
                                tvDPrice1.setText("RS " + dodArrayJSONObject.getString("final_price") + " after " + dodArrayJSONObject.getString("product_discount_amount") + "%");
                            }
                        }
                        if (i == 1) {
                            rlDeal2.setTag(dodArrayJSONObject.getString("product_id"));
                            if (dodArrayJSONObject.getString("product_image").isEmpty()) {
                                Picasso.with(mActivity).load((int) R.mipmap.ic_launcher).into(ivDeal2);
                            } else {
                                Picasso.with(mActivity).load(dodArrayJSONObject.getString("product_image")).into(ivDeal2);
                            }
                            tvDName2.setText(dodArrayJSONObject.getString("product_name"));
                            if (dodArrayJSONObject.getString("product_discount_type").equals("1")) {
                                tvDPrice2.setText("RS" + dodArrayJSONObject.getString("final_price") + " after RS " + dodArrayJSONObject.getString("product_discount_amount") + " Off");
                            } else {
                                tvDPrice2.setText("RS" + dodArrayJSONObject.getString("final_price") + " after " + dodArrayJSONObject.getString("product_discount_amount") + "%");
                            }
                        }
                        if (i == 2) {
                            rlDeal3.setTag(dodArrayJSONObject.getString("product_id"));
                            if (dodArrayJSONObject.getString("product_image").isEmpty()) {
                                Picasso.with(mActivity).load((int) R.mipmap.ic_launcher).into(ivDeal3);
                            } else {
                                Picasso.with(mActivity).load(dodArrayJSONObject.getString("product_image")).into(ivDeal3);
                            }
                            tvDName3.setText(dodArrayJSONObject.getString("product_name"));
                            if (dodArrayJSONObject.getString("product_discount_type").equals("1")) {
                                tvDPrice3.setText("RS " + dodArrayJSONObject.getString("final_price") + " after RS" + dodArrayJSONObject.getString("product_discount_amount") + " Off");
                            } else {
                                tvDPrice3.setText("RS" + dodArrayJSONObject.getString("final_price") + " after " + dodArrayJSONObject.getString("product_discount_amount") + "%");
                            }
                        }
                        if (i == 3) {
                            rlDeal4.setTag(dodArrayJSONObject.getString("product_id"));
                            if (dodArrayJSONObject.getString("product_image").isEmpty()) {
                                Picasso.with(mActivity).load((int) R.mipmap.ic_launcher).into(ivDeal4);
                            } else {
                                Picasso.with(mActivity).load(dodArrayJSONObject.getString("product_image")).into(ivDeal4);
                            }
                            tvDName4.setText(dodArrayJSONObject.getString("product_name"));
                            if (dodArrayJSONObject.getString("product_discount_type").equals("1")) {
                                tvDPrice4.setText("RS" + dodArrayJSONObject.getString("final_price") + " after RS" + dodArrayJSONObject.getString("product_discount_amount") + " Off");
                            } else {
                                tvDPrice4.setText("RS" + dodArrayJSONObject.getString("final_price") + " after " + dodArrayJSONObject.getString("product_discount_amount") + "%");
                            }
                        }
                    }
                    countDownStart();
                } else {
                    llDealOfDay.setVisibility(View.GONE);
                }*/
            } else {
                AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
        topBannerAdapter();
        if (TopBannerList.size() > 0) {
            TimerTask timerTask = new MyTimerTask();
            timer = new Timer();
            timer.schedule(timerTask, 3000, 3000);
        }
        AppSettings.putString(AppSettings.cartCount, "0");
        if(!AppSettings.getString(AppSettings.userId).isEmpty())
        {
            if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                getCartListApi();
            } else {
                AppUtils.showErrorMessage(tvMain, getString(R.string.errorInternet), mActivity);
            }
        }
        else
        {
            if(AppSettings.getString(AppSettings.fcmSend).equals("0")||AppSettings.getString(AppSettings.fcmSend).isEmpty())
            {
                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    sendFCMApi();
                } else {
                    AppUtils.showErrorMessage(tvMain, getString(R.string.errorInternet), mActivity);
                }
            }
        }
    }

    private void getCartListApi() {
        AppUtils.showRequestDialog(mActivity);
        Log.v("getCartListApi", AppUrls.getCart);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));

            json.put(AppConstants.result, json_data);

            Log.v("getCartListApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.getCart)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedata(response);
                    }
                    @Override
                    public void onError(ANError error) {
                         AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedata(JSONObject response) {
        AppSettings.putString(AppSettings.cartCount, "0");
        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                JSONArray productArray = jsonObject.getJSONArray("cart_product");
                AppSettings.putString(AppSettings.cartCount, String.valueOf(productArray.length()));
                if(productArray.length()>0)
                {
                    tvCount.setVisibility(View.VISIBLE);
                    tvCount.setText(String.valueOf(productArray.length()));
                }
                else
                {
                    tvCount.setVisibility(View.VISIBLE);
                }
            }
            else
            {
                tvCount.setVisibility(View.VISIBLE);
                AppSettings.putString(AppSettings.cartCount, "0");
            }
        } catch (Exception e) {
            tvCount.setVisibility(View.VISIBLE);
            AppSettings.putString(AppSettings.cartCount, "0");
        }
         AppUtils.hideDialog();

        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
          //  getProfileApi();
        } else {
            AppUtils.showErrorMessage(tvMain, getString(R.string.errorInternet), mActivity);
        }
    }

    private void addFavApi(String productId) {

        AppUtils.showRequestDialog(mActivity);

        Log.v("addFavApi", AppUrls.wishList);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json_data.put("product_master_id", productId);

            json.put(AppConstants.result, json_data);

            Log.v("addFavApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.wishList)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        parseFavdata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                         AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseFavdata(JSONObject response) {
        Log.d("response ", response.toString());

        DatabaseController.removeTable(TableFavourite.favourite);

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                if(jsonObject.getString("status").equals("1"))
                {
                    AppUtils.showErrorMessage(tvMain, "Product Added to Favourites", mActivity);
                }
                else if(jsonObject.getString("status").equals("2"))
                {
                    AppUtils.showErrorMessage(tvMain, "Product Removed from Favourites", mActivity);
                }

                JSONArray wishArray = jsonObject.getJSONArray("wish_list_product");

                for (int i = 0; i < wishArray.length(); i++) {

                    JSONObject productobject = wishArray.getJSONObject(i);
                    ContentValues mContentValues = new ContentValues();
                    mContentValues.put(TableFavourite.favouriteColumn.productId.toString(), productobject.getString("product_id"));
                    DatabaseController.insertData(mContentValues,TableFavourite.favourite);
                }

            }
            else
            {
                AppUtils.showErrorMessage(tvMain, "Product Removed from Favourites", mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
         AppUtils.hideDialog();
        setAdapter();
    }

    private void getProfileApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("getProfileApi", AppUrls.getUserDetail);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_id", AppSettings.getString(AppSettings.userId));

            json.put(AppConstants.result, json_data);
            Log.v("getProfileApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.getUserDetail)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        parseProfiledata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseProfiledata(JSONObject response) {

        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                JSONObject userObject = jsonObject.getJSONObject("user_detail");
                AppSettings.putString(AppSettings.name,userObject.getString("username"));
                AppSettings.putString(AppSettings.email,userObject.getString("email_id"));
                AppSettings.putString(AppSettings.gender,userObject.getString("gender"));
                AppSettings.putString(AppSettings.mobile,userObject.getString("phone_no"));
                AppSettings.putString(AppSettings.wallet,userObject.getString("wallet_amount"));
                AppSettings.putString(AppSettings.referralId,userObject.getString("referel_id"));

            }
            else
            {
                AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();

        onResume();

        if(AppSettings.getString(AppSettings.fcmSend).equals("0")||AppSettings.getString(AppSettings.fcmSend).isEmpty())
        {
            if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                //sendFCMApi();
            } else {
                AppUtils.showErrorMessage(tvMain, getString(R.string.errorInternet), mActivity);
            }
        }
    }

    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("regId", null);
        Log.e("Firebase reg id: ", regId);
        if (!TextUtils.isEmpty(regId)) {            // txtRegId.setText("Firebase Reg Id: " + regId);
            Log.v("firebaseid", "Firebase Reg Id: " + regId);
        } else {
            Log.v("not_received", "Firebase Reg Id is not received yet!");
        }
    }

    private void sendFCMApi() {
        Log.v("sendFCMApi", AppUrls.notificationMaster);
        AppUtils.showRequestDialog(mActivity);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("device_id", AppUtils.getDeviceID(mActivity));
            json_data.put("fcm_id", AppSettings.getString(AppSettings.fcmId));
            json.put(AppConstants.result, json_data);

            Log.v("sendFCMApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
            Log.v("printStack", String.valueOf(e));
        }

        AndroidNetworking.post(AppUrls.notificationMaster)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        parseSendFCMdata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseSendFCMdata(JSONObject response) {
        Log.d("response ", response.toString());
             AppUtils.hideDialog();
      /*  AppSettings.putString(AppSettings.fcmSend,"0");*/
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
               /* AppSettings.putString(AppSettings.fcmSend,"1");*/
            }
            else
            {
                AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
    }
/*featured page*/
}

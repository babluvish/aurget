package code.model;

import java.io.Serializable;

public class Banner implements Serializable {
    String banner_id;
    String banner_on;
    String id;
    String featured_banner;
    String banner_image;

    public String getBanner_id() {
        return banner_id;
    }

    public void setBanner_id(String banner_id) {
        this.banner_id = banner_id;
    }

    public String getBanner_on() {
        return banner_on;
    }

    public void setBanner_on(String banner_on) {
        this.banner_on = banner_on;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFeatured_banner() {
        return featured_banner;
    }

    public void setFeatured_banner(String featured_banner) {
        this.featured_banner = featured_banner;
    }

    public String getBanner_image() {
        return banner_image;
    }

    public void setBanner_image(String banner_image) {
        this.banner_image = banner_image;
    }
}

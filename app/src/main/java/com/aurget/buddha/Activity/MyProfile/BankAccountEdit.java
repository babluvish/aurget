package com.aurget.buddha.Activity.MyProfile;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.provider.MediaStore;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aurget.buddha.Activity.Frgmnt.BnkDetlsFragment;
import com.aurget.buddha.Activity.Frgmnt.SolderDetilsFragment;
import com.aurget.buddha.Activity.Frgmnt.account_settingsFragment;
import com.aurget.buddha.Activity.Frgmnt.addressDetailsFragment;
import com.aurget.buddha.Activity.ImageCropper.CropImage;
import com.aurget.buddha.Activity.ImageCropper.CropImageView;
import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;
import static com.aurget.buddha.Activity.Commonhelper.bitMap;
import static com.aurget.buddha.Activity.Commonhelper.fileExe;

public class BankAccountEdit extends AppCompatActivity {

    private SharedPreferences Shpref;
    private SharedPreferences.Editor editShpref;

    LinearLayout previous,memberLL,permanent_account_numberLl;
    LinearLayout next,ddress2,ddress3,addressLL;
    RelativeLayout passbookRl,aadharRl,adharRl,pankPicRl,updateMobileNoRl;
    int count=0;
    TextView nextBtn,title,titletop;
    ImageView right_icon,passbookimg,aadharcardImg,aadharcardImgRemove,aadharcardImg1,aadharcardImgRemove2;
    EditText brnchneEt,ifcscodeEt,accountNoEt,bankNameEt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_account_edit);

        Shpref = getSharedPreferences("sp", MODE_PRIVATE);
        editShpref = Shpref.edit();

        previous = findViewById(R.id.previous);
        next = findViewById(R.id.next);
        nextBtn = findViewById(R.id.nextBtn);
        titletop = findViewById(R.id.title);
        right_icon = findViewById(R.id.right_icon);
        aadharcardImg = findViewById(R.id.aadharcardImg);
        aadharcardImgRemove = findViewById(R.id.aadharcardImgRemove);
        aadharcardImg1 = findViewById(R.id.aadharcardImg1);
        aadharcardImgRemove2 = findViewById(R.id.aadharcardImgRemove2);
        permanent_account_numberLl = findViewById(R.id.permanent_account_numberLl);
        aadharRl = findViewById(R.id.aadharRl);
        adharRl = findViewById(R.id.adharRl);
        pankPicRl = findViewById(R.id.pankPicRl);
        updateMobileNoRl = findViewById(R.id.updateMobileNoRl);

        ddress2 = findViewById(R.id.ddress2);
        ddress3 = findViewById(R.id.ddress3);
        addressLL = findViewById(R.id.addressLL);
        memberLL = findViewById(R.id.memberLL);
        passbookimg = findViewById(R.id.imgpssbook);
        passbookRl = findViewById(R.id.passbookRl);

        brnchneEt = findViewById(R.id.brnchneEt);
        bankNameEt = findViewById(R.id.bankNameEt);
        accountNoEt = findViewById(R.id.accountNoEt);
        ifcscodeEt = findViewById(R.id.ifcscodeEt);

        title =  findViewById(R.id.titleTv);

        titletop.setText(R.string.account__setting);


        frgment(getIntent().getStringExtra("pos"));

        //right_icon.setVisibility(View.VISIBLE);

        passbookRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                editShpref.putInt("num",1);
                editShpref.apply();

                popupUploadImage();
            }
        });

        findViewById(R.id.adharRl).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(BankAccountEdit.this);
                dialog.setContentView(R.layout.aadhar_fornt_back);

                TextView FrontScreenTv = dialog.findViewById(R.id.FrontScreenTv);
                TextView BackScreenTv = dialog.findViewById(R.id.BackScreenTv);

                FrontScreenTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        editShpref.putInt("num",2);
                        editShpref.apply();

                        popupUploadImage();
                        dialog.dismiss();

                    }
                });

                BackScreenTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        editShpref.putInt("num",3);
                        editShpref.apply();

                        popupUploadImage();
                        dialog.dismiss();

                    }
                });

                dialog.show();

            }
        });

        updateMobileNoRl.setOnClickListener(view -> dilog());

        findViewById(R.id.updateEMailAddressRl).setOnClickListener(view -> dilogEmil());

//        right_icon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Commonhelper.showToastLong(BankAccountEdit.this,title.getText().toString()+" Updated Successfully");
//            }
//        });

        next.setOnClickListener(view -> {

            count++;

            Log.i("count++", String.valueOf(count));

             if (count==1) {

                previous.setEnabled(true);
                title.setText(getResources().getString(R.string.addressDetils));
                nextBtn.setText(getString(R.string.continu));

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    previous.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                }

                addressLL.setVisibility(View.VISIBLE);
                memberLL.setVisibility(View.GONE);
                ddress2.setVisibility(View.GONE);
                ddress3.setVisibility(View.GONE);
                adharRl.setVisibility(View.GONE);
                permanent_account_numberLl.setVisibility(View.GONE);

                 pankPicRl.setVisibility(View.GONE);
                 aadharRl.setVisibility(View.GONE);
                 aadharcardImg1.setVisibility(View.GONE);
                 aadharcardImgRemove2.setVisibility(View.GONE);
            }

            else  if (count==2) {

                previous.setEnabled(true);
                 title.setText(getResources().getString(R.string.bankDetails));
                nextBtn.setText(getString(R.string.continu));

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    previous.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                }

                addressLL.setVisibility(View.GONE);
                memberLL.setVisibility(View.GONE);
                ddress2.setVisibility(View.VISIBLE);
                ddress3.setVisibility(View.GONE);
                adharRl.setVisibility(View.GONE);
                permanent_account_numberLl.setVisibility(View.GONE);

                if (stringsAl.size()>0)
                 pankPicRl.setVisibility(View.VISIBLE);
                else
                    pankPicRl.setVisibility(View.GONE);

                 aadharRl.setVisibility(View.GONE);
                 aadharcardImg1.setVisibility(View.GONE);
                 aadharcardImgRemove2.setVisibility(View.GONE);
            }

             else  if (count==3) {

                previous.setEnabled(true);
                title.setText(getResources().getString(R.string.your_adhar_Detils));
                nextBtn.setText(getString(R.string.continu));

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    previous.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                }

                addressLL.setVisibility(View.GONE);
                memberLL.setVisibility(View.GONE);
                ddress2.setVisibility(View.GONE);
                ddress3.setVisibility(View.GONE);
                 adharRl.setVisibility(View.VISIBLE);
                permanent_account_numberLl.setVisibility(View.GONE);

                pankPicRl.setVisibility(View.GONE);

                if (stringsAl.size()>1)
                 aadharRl.setVisibility(View.VISIBLE);

                else
                {
                    aadharRl.setVisibility(View.GONE);
                }

                if (stringsAl.size()>2) {
                    aadharcardImg1.setVisibility(View.VISIBLE);
                    aadharcardImgRemove2.setVisibility(View.VISIBLE);
                }
                else
                {
                    aadharcardImg1.setVisibility(View.GONE);
                    aadharcardImgRemove2.setVisibility(View.GONE);
                }

            }

            else  if (count==4)
            {
                previous.setEnabled(true);
                 title.setText(getResources().getString(R.string.permanent_account_Detils));
                nextBtn.setText(getString(R.string.continu));

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    previous.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                }

                addressLL.setVisibility(View.GONE);
                memberLL.setVisibility(View.GONE);
                ddress2.setVisibility(View.GONE);
                ddress3.setVisibility(View.GONE);
                adharRl.setVisibility(View.GONE);
                permanent_account_numberLl.setVisibility(View.VISIBLE);

                pankPicRl.setVisibility(View.GONE);
                aadharRl.setVisibility(View.GONE);
                aadharcardImg1.setVisibility(View.GONE);
                aadharcardImgRemove2.setVisibility(View.GONE);
            }

            else  if (count==5) {
                previous.setEnabled(true);
                 title.setText(getResources().getString(R.string.account__Seurity));
                nextBtn.setText(getString(R.string.submit));

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    previous.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                }

                addressLL.setVisibility(View.GONE);
                memberLL.setVisibility(View.GONE);
                ddress2.setVisibility(View.GONE);
                ddress3.setVisibility(View.VISIBLE);
                 adharRl.setVisibility(View.GONE);
                permanent_account_numberLl.setVisibility(View.GONE);

                 pankPicRl.setVisibility(View.GONE);
                 aadharRl.setVisibility(View.GONE);
                 aadharcardImg1.setVisibility(View.GONE);
                 aadharcardImgRemove2.setVisibility(View.GONE);
            }

            else
            {
                next.setEnabled(true);
                nextBtn.setText(R.string.continu);
                previous.setEnabled(false);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    next.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                    previous.setBackgroundTintList(getResources().getColorStateList(R.color.blueLight));
                }
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                count--;
                Log.i("count++", String.valueOf(count));

                if (count==1)
                {
                     title.setText(getResources().getString(R.string.addressDetils));
                    next.setEnabled(true);
                    nextBtn.setText(R.string.continu);
                    //tickImg.setImageResource(R.drawable.ic_check_black_24dp);
                    previous.setEnabled(true);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        next.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                        previous.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                    }

                    addressLL.setVisibility(View.VISIBLE);
                    memberLL.setVisibility(View.GONE);
                    ddress2.setVisibility(View.GONE);
                    ddress3.setVisibility(View.GONE);
                    adharRl.setVisibility(View.GONE);
                    permanent_account_numberLl.setVisibility(View.GONE);

                    pankPicRl.setVisibility(View.GONE);
                    aadharRl.setVisibility(View.GONE);
                    aadharcardImg1.setVisibility(View.GONE);
                    aadharcardImgRemove2.setVisibility(View.GONE);

                }

                else  if (count==2) {

                    previous.setEnabled(true);
                     title.setText(getResources().getString(R.string.bankDetails));
                    nextBtn.setText(getString(R.string.continu));

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        previous.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                    }

                    addressLL.setVisibility(View.GONE);
                    memberLL.setVisibility(View.GONE);
                    ddress2.setVisibility(View.VISIBLE);
                    ddress3.setVisibility(View.GONE);
                    adharRl.setVisibility(View.GONE);
                    permanent_account_numberLl.setVisibility(View.GONE);

                    if (stringsAl.size()>0)
                        pankPicRl.setVisibility(View.VISIBLE);

                    aadharRl.setVisibility(View.GONE);
                    aadharcardImg1.setVisibility(View.GONE);
                    aadharcardImgRemove2.setVisibility(View.GONE);
                }

                 else  if (count==3) {

                    previous.setEnabled(true);
                     title.setText(getResources().getString(R.string.your_adhar_Detils));
                    nextBtn.setText(getString(R.string.continu));

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        previous.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                    }

                    addressLL.setVisibility(View.GONE);
                    memberLL.setVisibility(View.GONE);
                    ddress2.setVisibility(View.GONE);
                    ddress3.setVisibility(View.GONE);
                    adharRl.setVisibility(View.VISIBLE);
                    permanent_account_numberLl.setVisibility(View.GONE);

                    if (stringsAl.size()>1)
                        aadharRl.setVisibility(View.VISIBLE);

                    if (stringsAl.size()>2) {
                        aadharcardImg1.setVisibility(View.VISIBLE);
                        aadharcardImgRemove2.setVisibility (View.VISIBLE);
                    }

                    pankPicRl.setVisibility(View.GONE);
                }

                else  if (count==4) {

                    previous.setEnabled(true);
                     title.setText(getResources().getString(R.string.permanent_account_Detils));
                    nextBtn.setText(getString(R.string.continu));

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        previous.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                    }

                    addressLL.setVisibility(View.GONE);
                    memberLL.setVisibility(View.GONE);
                    ddress2.setVisibility(View.GONE);
                    ddress3.setVisibility(View.GONE);
                    adharRl.setVisibility(View.GONE);
                    permanent_account_numberLl.setVisibility(View.VISIBLE);


                    aadharRl.setVisibility(View.GONE);
                    aadharcardImg1.setVisibility(View.GONE);
                    aadharcardImgRemove2.setVisibility (View.GONE);
                    pankPicRl.setVisibility(View.GONE);
                }

                else  {

                    previous.setEnabled(true);
                     title.setText(getResources().getString(R.string.memberDetails));
                    nextBtn.setText(getString(R.string.continu));

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        previous.setBackgroundTintList(getResources().getColorStateList(R.color.blueLight));
                    }

                    addressLL.setVisibility(View.GONE);
                    memberLL.setVisibility(View.VISIBLE);
                    ddress2.setVisibility(View.GONE);
                    ddress3.setVisibility(View.GONE);
                    adharRl.setVisibility(View.GONE);
                    permanent_account_numberLl.setVisibility(View.GONE);

                    aadharRl.setVisibility(View.GONE);
                    aadharcardImg1.setVisibility(View.GONE);
                    aadharcardImgRemove2.setVisibility (View.GONE);
                    pankPicRl.setVisibility(View.GONE);
                }
            }
        });

        findViewById(R.id.bkImg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        fetch_bank_details_();
    }


    void dilog()
    {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.login2);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final EditText mobilNoEt = dialog.findViewById(R.id.mobilNoEt);

        final EditText otp1 = dialog.findViewById(R.id.otp1);
        final EditText otp2 = dialog.findViewById(R.id.otp2);
        final EditText otp3 = dialog.findViewById(R.id.otp3);
        final EditText otp4 = dialog.findViewById(R.id.otp4);

        final LinearLayout otpLl = dialog.findViewById(R.id.otpLl);
        final Button loginBtn = dialog.findViewById(R.id.loginBtn);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (loginBtn.getText().toString().equalsIgnoreCase("Update"))
                {
                    Toast.makeText(BankAccountEdit.this,"Mobile Number Updated Successfully",Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
                else {
                    otpLl.setVisibility(View.VISIBLE);
                    mobilNoEt.setVisibility(View.GONE);
                    loginBtn.setText("Update");
                }
            }
        });

        otp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (otp1.getText().length()==1)
                    otp2.requestFocus();

            }
        });

        otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub

                if (otp2.getText().length()==1)
                    otp3.requestFocus();
            }
        });

        otp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub

                if (otp3.getText().length()==1)
                    otp4.requestFocus();
            }
        });

        otp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub

                //if (otp4.getText().length()==1)
                    //Toast.makeText(BankAccountEdit.this,"Mobile Number Updated Successfully",Toast.LENGTH_LONG).show();
            }
        });


        dialog.show();
    }

    void dilogEmil()
    {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.emil_edit);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final EditText mobilNoEt = dialog.findViewById(R.id.mobilNoEt);

        final EditText otp1 = dialog.findViewById(R.id.otp1);
        final EditText otp2 = dialog.findViewById(R.id.otp2);
        final EditText otp3 = dialog.findViewById(R.id.otp3);
        final EditText otp4 = dialog.findViewById(R.id.otp4);

        final LinearLayout otpLl = dialog.findViewById(R.id.otpLl);
        final Button loginBtn = dialog.findViewById(R.id.loginBtn);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (loginBtn.getText().toString().equalsIgnoreCase("Update"))
                {
                    Toast.makeText(BankAccountEdit.this,"E-mail ID Updated Successfully",Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
                else {
                    otpLl.setVisibility(View.VISIBLE);
                    mobilNoEt.setVisibility(View.GONE);
                    loginBtn.setText("Update");
                }
            }
        });

        otp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (otp1.getText().length()==1)
                    otp2.requestFocus();

            }
        });

        otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub

                if (otp2.getText().length()==1)
                    otp3.requestFocus();
            }
        });

        otp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub

                if (otp3.getText().length()==1)
                    otp4.requestFocus();
            }
        });

        otp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub

                //if (otp4.getText().length()==1)
                    //Toast.makeText(BankAccountEdit.this,"Mobile Number Updated Successfully",Toast.LENGTH_LONG).show();
            }
        });

        dialog.show();
    }

    private void popupUploadImage() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setActivityTitle("Crop image")
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setCropMenuCropButtonTitle("Done").setAspectRatio(Gravity.FILL,200)
                .setRequestedSize(1000, 1000)
                .setCropMenuCropButtonIcon(R.drawable.ic_fullscreen_black_24dp)
                .start(BankAccountEdit.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult cropResult = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                int i = Shpref.getInt("num",0);
                Log.i("vvv",""+i);
                if (i==1) {

                    BnkDetlsFragment.imgpssbook.setImageURI(cropResult.getUri());

                    if (stringsAl.size()>0)
                    stringsAl.set(0,"1");
                    else
                        stringsAl.add("1");

                    Bitmap panImgPthBitmap = null;
                    try {
                        panImgPthBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), cropResult.getUri());
                        passbookImgExt = fileExe(cropResult.getUri());
                        passbookImgPath = bitMap(panImgPthBitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                 else if(i==2) {

                    BnkDetlsFragment.aadharcardImg.setVisibility(View.VISIBLE);
                   BnkDetlsFragment.aadharcardImg.setImageURI(cropResult.getUri());
                    if (stringsAl.size()>1)
                    stringsAl.set(0,"2");
                    else
                        stringsAl.add("2");

                    Bitmap panImgPthBitmap = null;
                    try {
                        panImgPthBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), cropResult.getUri());
                        adharCrdImgPth = bitMap(panImgPthBitmap);
                        adharCrdImgext = fileExe(cropResult.getUri());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                 else if(i==3) {

                    BnkDetlsFragment.aadharcardImg1.setImageURI(cropResult.getUri());
                    BnkDetlsFragment.aadharcardImg1.setVisibility(View.VISIBLE);

                    //BnkDetlsFragment.aadharcardImgRemove2.setVisibility(View.VISIBLE);
                    if (stringsAl.size()>2)
                        stringsAl.set(0,"3");
                    else
                        stringsAl.add("3");

                    Bitmap panImgPthBitmap = null;
                    try {
                        panImgPthBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), cropResult.getUri());
                        adharCrdImgBackPth = bitMap(panImgPthBitmap);
                        adharCrdImgBackExt = fileExe(cropResult.getUri());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                 else if(i==4) {

                    BnkDetlsFragment.panImg.setImageURI(cropResult.getUri());

                    //BnkDetlsFragment.aadharcardImgRemove2.setVisibility(View.VISIBLE);
                    if (stringsAl.size()>2)
                        stringsAl.set(0,"3");
                    else
                        stringsAl.add("3");

                    Bitmap panImgPthBitmap = null;
                    try {
                        panImgPthBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), cropResult.getUri());
                        panImgPth = bitMap(panImgPthBitmap);
                        panImgext = fileExe(cropResult.getUri());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + cropResult.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

       ArrayList<String> stringsAl = new ArrayList<>();

    void frgment(String i)
    {
        Fragment fragment = null;

        if (i.equals("1"))
        {
            fragment = new SolderDetilsFragment();
        }

        else if (i.equals("2"))
       {
           fragment = new addressDetailsFragment();
       }

        else if (i.equals("3"))
       {
           fragment = new BnkDetlsFragment();
       }

        else if (i.equals("4"))
       {
           fragment = new account_settingsFragment();
       }

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.continer, fragment, null);
        fragmentTransaction.commit();
    }

    private void fetch_bank_details_() {

        new AbstrctClss(BankAccountEdit.this, ConstntApi.get_bank_details(BankAccountEdit.this), "p", true) {
            @Override
            public void responce(String s) {
                try {
                    JSONArray jsonArray = new JSONArray(s);
                    BnkDetlsFragment.bankNameEt.setText(jsonArray.optJSONObject(0).optString("bank"));
                    BnkDetlsFragment.accountNoEt.setText(""+jsonArray.optJSONObject(0).optString("account_no"));
                    BnkDetlsFragment.brnchneEt.setText(jsonArray.optJSONObject(0).optString("branch"));
                    BnkDetlsFragment.ifcscodeEt.setText(jsonArray.optJSONObject(0).optString("ifsc"));
                    BnkDetlsFragment.adharEt.setText(jsonArray.optJSONObject(0).optString("aadhar_number"));
                    BnkDetlsFragment.panEt.setText(jsonArray.optJSONObject(0).optString("pan_number"));

                    Picasso.with(BankAccountEdit.this).load(InterfaceClass.ipAddress3+jsonArray.optJSONObject(0).optString("adhar_image")).into(BnkDetlsFragment.aadharcardImg);
                    Picasso.with(BankAccountEdit.this).load(InterfaceClass.ipAddress3+jsonArray.optJSONObject(0).optString("pan_image")).into(BnkDetlsFragment.panImg);
                    Picasso.with(BankAccountEdit.this).load(InterfaceClass.ipAddress3+jsonArray.optJSONObject(0).optString("adhar_back")).into(BnkDetlsFragment.aadharcardImg1);
                    Picasso.with(BankAccountEdit.this).load(InterfaceClass.ipAddress3+jsonArray.optJSONObject(0).optString("pass_image")).into(BnkDetlsFragment.imgpssbook);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }

    public String panImgPth = "_",adharCrdImgPth="_",adharCrdImgBackPth="_",passbookImgPath="_";
    public String panImgext="_",adharCrdImgext="_",passbookImgExt="_",adharCrdImgBackExt="_";

}

package com.aurget.buddha.Activity.YourOrder;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Handler;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aurget.buddha.Activity.AddtoCart.AddCartListActivity;
import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.EmptyCartActivity;
import com.aurget.buddha.Activity.Rate.RateActivity;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.Utils.SearchDate;
import com.aurget.buddha.Activity.Utils.SearchMonth;
import com.aurget.buddha.Activity.categories.SubCategoryDetailsActivity;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.aurget.buddha.Activity.Main2Activity.crtDetils2;

public class YourOrder extends BaseActivity {

    RecyclerView recycleView;
    FloatingActionButton flotBtn;
    TextView allTv, crtCountTv;
    TextView dateRnge;
    TextView mothTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_order);

        recycleView = findViewById(R.id.recycleView);
        //rateRv = findViewById(R.id.rateRv);

        allTv = findViewById(R.id.allTv);
        dateRnge = findViewById(R.id.dateRnge);
        crtCountTv = findViewById(R.id.crtCountTv);
        //findViewById(R.id.bkImg).setVisibility(View.GONE);
        //findViewById(R.id.crtContner).setVisibility(View.GONE);
        mothTv = findViewById(R.id.mothTv);

        final LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recycleView.setLayoutManager(horizontalLayoutManager);
        recycleView.setItemAnimator(new DefaultItemAnimator());
        recycleView.setHasFixedSize(true);

        findViewById(R.id.bkImg).setVisibility(View.GONE);
        findViewById(R.id.bkImg2).setVisibility(View.VISIBLE);
        findViewById(R.id.bkImg2).setOnClickListener(v -> finish());

        crtDetils();

        findViewById(R.id.imghmb1).setOnClickListener(view -> {
            if (Integer.parseInt(crtCountTv.getText().toString().trim().replace("+","")) < 1) {
                Intent intent = new Intent(mActivity, EmptyCartActivity.class);
                startActivity(intent);
                return;
            }
            Intent intent = new Intent(mActivity, AddCartListActivity.class);
            intent.putExtra("res", crtDetils2);
            startActivity(intent);
        });


        final EditText searchEt = findViewById(R.id.searchEt);

        searchEt.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus)
            {
                searchEt.setCursorVisible(true);
            }
        });

        searchEt.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                //searchData(searchEt.getText().toString());
                Intent intent = new Intent(YourOrder.this, SubCategoryDetailsActivity.class);
                intent.putExtra("search", searchEt.getText().toString());
                startActivity(intent);
                return true;
            }
            return false;
        });

        allTv.setOnClickListener(view -> {
            dateRnge.setBackgroundResource(R.drawable.ovel_shap3);
            allTv.setBackgroundResource(R.drawable.ovel_shape);
            mothTv.setBackgroundResource(R.drawable.ovel_shap3);

            order_list();
        });

        dateRnge.setOnClickListener(view -> {
            dateRnge.setBackgroundResource(R.drawable.ovel_shape);
            allTv.setBackgroundResource(R.drawable.ovel_shap3);
            mothTv.setBackgroundResource(R.drawable.ovel_shap3);

            new SearchDate(YourOrder.this) {
                @Override
                public String fromDtToDt(String frmDate, String toDt) {
                    order_list_by_date(frmDate, toDt);
                    return null;
                }
            }.dateRangeDialog();
        });

        mothTv.setOnClickListener(view -> {
            mothTv.setBackgroundResource(R.drawable.ovel_shape);
            allTv.setBackgroundResource(R.drawable.ovel_shap3);
            dateRnge.setBackgroundResource(R.drawable.ovel_shap3);

            new SearchMonth(YourOrder.this) {
                @Override
                public String fromDt(String year, String month) {
                    order_list_by_month(year + "-" + month);
                    return null;
                }
            }.monthNameDialog();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                order_list();
            }
        }, 1000);
    }

    void order_list() {
        new AbstrctClss(YourOrder.this, ConstntApi.order_list(YourOrder.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    Log.d("deal_product", s);
                    JSONObject jsonObject = new JSONObject(s);

                    ArrayList<JSONObject> jsonObjects = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray();
                    jsonArray = jsonObject.optJSONArray("data");

                    if (jsonArray.length() < 1) {
                        Commonhelper.showToastLong(YourOrder.this, Constnt.notFound);
                        return;
                    }

                    arrayList.clear();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjects.add(jsonArray.optJSONObject(i));
                        arrayList.add("0");
                    }

                    Commonhelper.sop("->" + arrayList.toString());
                    recycleView.setAdapter(new MyRecyclerAdapter(jsonObjects));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    void order_list_by_date(String fromDt, String toDt) {
        new AbstrctClss(YourOrder.this, ConstntApi.order_list_by_date(YourOrder.this, fromDt, toDt), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    Log.d("order_list_by_date", s);
                    JSONObject jsonObject = new JSONObject(s);

                    ArrayList<JSONObject> jsonObjects = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray();
                    jsonArray = jsonObject.optJSONArray("data");

                    if (jsonArray.length() < 1) {
                        Commonhelper.showToastLong(YourOrder.this, Constnt.notFound);
                        return;
                    }

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjects.add(jsonArray.optJSONObject(i));
                    }

                    recycleView.setAdapter(new MyRecyclerAdapter(jsonObjects));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    void order_list_by_month(String year_month) {
        new AbstrctClss(YourOrder.this, ConstntApi.order_list_by_month(YourOrder.this, year_month), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    JSONObject jsonObject = new JSONObject(s);

                    ArrayList<JSONObject> jsonObjects = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray();
                    jsonArray = jsonObject.optJSONArray("data");

                    if (jsonArray.length() < 1) {
                        Commonhelper.showToastLong(YourOrder.this, Constnt.notFound);
                        return;
                    }

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjects.add(jsonArray.optJSONObject(i));
                    }

                    Commonhelper.sop("ttt=" + jsonObjectal3.toString());
                    recycleView.setAdapter(new MyRecyclerAdapter(jsonObjects));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapter(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.your_order_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapter.ViewHolder holder, int position) {
            try {
                holder.productNAme.setText(jsonObjectal.get(position).optString("product_name"));
                if (jsonObjectal.get(position).optString("delevery_status").equalsIgnoreCase("Pending")) {
                    holder.shipped.setTextColor(getResources().getColor(R.color.red));
                } else {
                    holder.shipped.setTextColor(getResources().getColor(R.color.greencol));
                }

                holder.shipped.setText(jsonObjectal.get(position).optString("delevery_status"));
                holder.amount.setText("₹" + jsonObjectal.get(position).optString("grand_total"));
                holder.orderId.setText("Order ID: " + jsonObjectal.get(position).optString("order_id"));
                holder.placedON.setText("Placed On: " + jsonObjectal.get(position).optString("Placed_On"));
                Picasso.with(YourOrder.this).load(jsonObjectal.get(position).optString("image")).into(holder.img);

                if (jsonObjectal.get(position).optString("delevery_status").equalsIgnoreCase("Delivered")) {
                    if (jsonObjectal.get(position).optString("rating").equals("1")) {
                        holder.stat1Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        //holder.writeCv.setVisibility(View.VISIBLE);
                    } else if (jsonObjectal.get(position).optString("rating").equals("2")) {
                        holder.stat1Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        holder.stat2Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        //holder.writeCv.setVisibility(View.VISIBLE);
                    } else if (jsonObjectal.get(position).optString("rating").equals("3")) {
                        holder.stat1Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        holder.stat2Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        holder.stat3Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        //holder.writeCv.setVisibility(View.VISIBLE);
                    } else if (jsonObjectal.get(position).optString("rating").equals("4")) {
                        holder.stat1Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        holder.stat2Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        holder.stat3Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        holder.stat4Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        //holder.writeCv.setVisibility(View.VISIBLE);
                    } else if (jsonObjectal.get(position).optString("rating").equals("5")) {
                        holder.stat1Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        holder.stat2Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        holder.stat3Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        holder.stat4Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        holder.stat5Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        //holder.writeCv.setVisibility(View.VISIBLE);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private TextView monthTv, productNAme, shipped, amount, orderId, placedON;
            LinearLayout llCon, rateLL, star1LL, star2LL, star3LL, star4LL, star5LL;
            ImageView img, stat1Img, stat2Img, stat3Img, stat4Img, stat5Img;
            RecyclerView rateRv;
            CardView writeCv;

            public ViewHolder(View itemView) {
                super(itemView);

                llCon = itemView.findViewById(R.id.llCon);
                img = itemView.findViewById(R.id.img);
                writeCv = itemView.findViewById(R.id.writeCv);

                star1LL = itemView.findViewById(R.id.star1LL);
                star2LL = itemView.findViewById(R.id.star2LL);
                star3LL = itemView.findViewById(R.id.star3LL);
                star4LL = itemView.findViewById(R.id.star4LL);
                star5LL = itemView.findViewById(R.id.star5LL);

                stat1Img = itemView.findViewById(R.id.stat1Img);
                stat2Img = itemView.findViewById(R.id.stat2Img);
                stat3Img = itemView.findViewById(R.id.stat3Img);
                stat4Img = itemView.findViewById(R.id.stat4Img);
                stat5Img = itemView.findViewById(R.id.stat5Img);

                productNAme = itemView.findViewById(R.id.productNAme);
                shipped = itemView.findViewById(R.id.shipped);
                amount = itemView.findViewById(R.id.amount);
                orderId = itemView.findViewById(R.id.orderId);
                placedON = itemView.findViewById(R.id.placedON);
                rateRv = itemView.findViewById(R.id.rateRv);
                rateLL = itemView.findViewById(R.id.rateLL);

                llCon.setOnClickListener(view -> {
                    Intent intent = new Intent(YourOrder.this, YourOrderDetils.class);
                    intent.putExtra("orderID", jsonObjectal.get(getAdapterPosition()).optString("order_id"));
                    startActivity(intent);
                });

                star1LL.setOnClickListener(view -> {
                    if (Commonhelper.compareImage(stat1Img, YourOrder.this, R.drawable.star_col_green)) {
                        stat2Img.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_black_24dp));
                        stat3Img.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_black_24dp));
                        stat4Img.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_black_24dp));
                        stat5Img.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_black_24dp));
                    } else {
                        stat1Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        //writeCv.setVisibility(View.VISIBLE);
                    }

                    hitApi("1", jsonObjectal.get(getAdapterPosition()).optString("product_id"));
                });

                star2LL.setOnClickListener(view -> {
                    if (Commonhelper.compareImage(stat2Img, YourOrder.this, R.drawable.star_col_green)) {
                        stat3Img.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_black_24dp));
                        stat4Img.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_black_24dp));
                        stat5Img.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_black_24dp));
                    } else {
                        stat1Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        stat2Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        //writeCv.setVisibility(View.VISIBLE);
                    }

                    hitApi("2", jsonObjectal.get(getAdapterPosition()).optString("product_id"));
                });

                star3LL.setOnClickListener(view -> {
                    if (Commonhelper.compareImage(stat3Img, YourOrder.this, R.drawable.star_col_green)) {
                        stat4Img.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_black_24dp));
                        stat5Img.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_black_24dp));
                    } else {
                        stat1Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        stat2Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        stat3Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        //writeCv.setVisibility(View.VISIBLE);
                    }

                    hitApi("3", jsonObjectal.get(getAdapterPosition()).optString("product_id"));
                });

                star4LL.setOnClickListener(view -> {
                    if (Commonhelper.compareImage(stat4Img, YourOrder.this, R.drawable.star_col_green)) {
                        stat5Img.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_black_24dp));
                    } else {
                        stat1Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        stat2Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        stat3Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        stat4Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        //writeCv.setVisibility(View.VISIBLE);
                    }

                    hitApi("4", jsonObjectal.get(getAdapterPosition()).optString("product_id"));
                });

                star5LL.setOnClickListener(view -> {
                    if (Commonhelper.compareImage(stat5Img, YourOrder.this, R.drawable.star_col_green)) {
                        //stat5Img.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_black_24dp));
                    } else {
                        stat1Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        stat2Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        stat3Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        stat4Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        stat5Img.setImageDrawable(getResources().getDrawable(R.drawable.star_col_green));
                        //writeCv.setVisibility(View.VISIBLE);
                    }

                    hitApi("5", jsonObjectal.get(getAdapterPosition()).optString("product_id"));
                });

                /*writeCv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (Commonhelper.compareImage(stat1Img, YourOrder.this, R.drawable.star_col_green)) {
                            count = "1";
                        }
                        if (Commonhelper.compareImage(stat2Img, YourOrder.this, R.drawable.star_col_green)) {
                            count = "2";
                        }
                        if (Commonhelper.compareImage(stat3Img, YourOrder.this, R.drawable.star_col_green)) {
                            count = "3";
                        }
                        if (Commonhelper.compareImage(stat4Img, YourOrder.this, R.drawable.star_col_green)) {
                            count = "4";
                        }
                        if (Commonhelper.compareImage(stat5Img, YourOrder.this, R.drawable.star_col_green)) {
                            count = "5";
                        }

                        hitApi(count,jsonObjectal.get(getAdapterPosition()).optString("product_id"));
                    }
                });*/

                /*ArrayList<JSONObject> jsonObjectal2 = new ArrayList<>();

                try {
                    for (int i = 0; i < 5; i++) {
                        JSONObject jsonObject = new JSONObject();
                        if (i == 0) {
                            jsonObject.put("name", "Horrible");
                        } else if (i == 1) {
                            jsonObject.put("name", "Bad");
                        } else if (i == 2) {
                            jsonObject.put("name", "Average");
                        } else if (i == 3) {
                            jsonObject.put("name", "Good");
                        } else if (i == 4) {
                            jsonObject.put("name", "Excellent");
                        }
                        jsonObject.put("pos", "0");
                        jsonObjectal2.add(jsonObject);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final LinearLayoutManager horizontalLayoutManager1 = new LinearLayoutManager(YourOrder.this, LinearLayoutManager.HORIZONTAL, false);
                rateRv.setLayoutManager(horizontalLayoutManager1);
                rateRv.setItemAnimator(new DefaultItemAnimator());
                rateRv.setAdapter(new RateAdapter(jsonObjectal2));*/
            }
        }
    }

    void hitApi(final String count, final String product_id) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("1", product_id);
            jsonObject.put("2", "1");
            jsonObject.put("3", count);
            jsonObject.put("4", "_");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new AbstrctClss(YourOrder.this, ConstntApi.insert_rating(YourOrder.this, jsonObject), "g", true) {
            @Override
            public void responce(String s) {
                JSONObject jsonArray = null;
                try {
                    jsonArray = new JSONObject(s);
                    Commonhelper.showToastLong(YourOrder.this, jsonArray.optString("message"));
                    Intent intent = new Intent(YourOrder.this, RateActivity.class);
                    intent.putExtra("count", count);
                    intent.putExtra("product_id", product_id);
                    startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };

    }

    public class RateAdapter extends RecyclerView.Adapter<RateAdapter.ViewHolder> {

        ArrayList<JSONObject> jsonObjectal;

        public RateAdapter(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public RateAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rate_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final RateAdapter.ViewHolder holder, final int position) {

            holder.txt.setText(jsonObjectal.get(position).optString("name"));
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView statImg, statImg2;
            LinearLayout star1LL;
            TextView txt;

            public ViewHolder(View itemView) {
                super(itemView);
                statImg = itemView.findViewById(R.id.statImg);
                txt = itemView.findViewById(R.id.txt);
                star1LL = itemView.findViewById(R.id.star1LL);

                star1LL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        jsonObjectal.get(getAdapterPosition()).optString("pos");
                        statImg.setColorFilter(ContextCompat.getColor(YourOrder.this, R.color.red), PorterDuff.Mode.SRC_IN);
                        //notifyDataSetChanged();
                        Commonhelper.sop("ttt=" + jsonObjectal.toString());
                    }
                });
            }
        }
    }

    private void crtDetils() {
        new AbstrctClss(mActivity, ConstntApi.get_cart_details(mActivity), "g", false) {
            @Override
            public void responce(String s) {
                try {
                    JSONArray jsonArray = new JSONArray(s);
                    crtDetils2 = s;
                    if (jsonArray.length() > 9)
                        crtCountTv.setText("9+");
                    else crtCountTv.setText("" + jsonArray.length());
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("letestFeturePi", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }

    private ArrayList arrayList = new ArrayList();
    private ArrayList<String> jsonObjectal3 = new ArrayList<>();
    String count = "";
}

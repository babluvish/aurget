package com.aurget.buddha.Activity.code.Main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;


import com.aurget.buddha.Activity.code.Adapter.MyPagerAdapter;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Dashboard.Slider.CardPagerAdapter;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Search.SearchActivity;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;

import code.Common.Config;

public class DashBoardActivity extends BaseActivity implements View.OnClickListener {
    public static int VB_PAGES;
    public static int VB_LOOPS = 1000;
    public static int VB_FIRST_PAGE;
    public static String regId = "";
    // ViewPager currentpage
    private static int currentPage = 0;
    // ViewPager no offpage
    private static int NUM_PAGES = 0;

    ArrayList<HashMap<String, String>> CategoryList = new ArrayList();
    ArrayList<HashMap<String, String>> FeaturedBannerList = new ArrayList();
    ArrayList<HashMap<String, String>> OfferList = new ArrayList();
    ArrayList<ArrayList<HashMap<String, String>>> ProducttList = new ArrayList();
    ArrayList<HashMap<String, String>> TopBannerList = new ArrayList();
    ArrayList<HashMap<String, String>> FeaturedProductList = new ArrayList();
    ArrayList<HashMap<String, String>> hashMapArrayList = new ArrayList<>();
    ArrayList<HashMap<String, String>> subProductListArrayList = new ArrayList();
    ArrayList<HashMap<String, String>> datalist = new ArrayList();
    ArrayList spinnerName = new ArrayList();
    ArrayList spinnerId = new ArrayList();
    boolean loadMore = true;
    RecyclerView recyclerView;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    GridLayoutManager layoutManager;
    ImageView ivCat1, ivCat2, ivCat3, ivCat4, ivDeal1, ivDeal2, ivDeal3, ivDeal4, ivDrop, ivMenu, ivSearch, ivCart, ivLogo;
    LinearLayout llCat1, llCat2, llCat3, llCat4, llDealOfDay, llMore, llOffers, llHome, llCategory, llFavourites, llProfile, llNotification;
    TextView tvHome, tvCategory, tvFavourites, tvProfile, tvNotification, tvCount;
    ImageView ivHome, ivCategory, ivFavourites, ivProfile, ivNotification;
    CardPagerAdapter mCardAdapter;
    MyPagerAdapter adapter;
    ViewPager mViewPager;
    MainDashboardActivity.OfferAdapter offerAdapter;
    RelativeLayout rlDeal1, rlDeal2, rlDeal3, rlDeal4, rrRecharge, rrProfile, rrCategory, rrSchool, rrShare, rrInsurance, rrSearch;
    ScrollView scrollView;
    String offset = "0";
    Timer timer;
    TextView tvCat1, tvCat2, tvCat3, tvCat4, tvClock, tvDName1, tvDName2, tvDName3, tvDName4, tvDPrice1, tvDPrice2, tvDPrice3,
            tvDPrice4, tvLogin, tvUserName, tvMain, tvViewDOD;
    View view;
    Typeface typeface;
    int FIRST_PAGE;
    int PAGES;
    TextView tvBestValue, tvTopFeatures, tvAll;
    EditText searchEdit;
    RelativeLayout relativeLayoutSearch;
    ImageView ivAll;
    private Handler handler;
    private boolean loading = true;
    private Runnable runnable;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private LinearLayout dotsLayout;
    private ImageView[] dots;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_dash_board );
        init();
    }

    private void init() {

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals( Config.REGISTRATION_COMPLETE )) {
                    //FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    //displayFirebaseRegId();

                    AppSettings.putString( AppSettings.fcmId, regId );
                } else if (intent.getAction().equals( Config.PUSH_NOTIFICATION )) {
                    String message = intent.getStringExtra( "message" );
                    Log.v( "msg", message );
                }
            }
        };
//Todo... commentLine
       /* if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d( "Key: ", key + " Value: " + value);
            }
        }
*/
        //TextView
        tvMain = findViewById( R.id.tvHeaderText );

        //ViewPager
        mViewPager = findViewById( R.id.myviewpager );
        //LinearLayout
        llMore = findViewById( R.id.llMore );
        llDealOfDay = findViewById( R.id.llDealOfDay );
        llOffers = findViewById( R.id.llOffers );
        llCat1 = findViewById( R.id.llCat1 );
        llCat2 = findViewById( R.id.llCat2 );
        llCat3 = findViewById( R.id.llCat3 );
        llCat4 = findViewById( R.id.llCat4 );

        //ScrollView
        scrollView = findViewById( R.id.scroll_side_menu );
        //View
        view = findViewById( R.id.view );
        //RelativeLayout
        rlDeal1 = findViewById( R.id.rlD1 );
        rlDeal2 = findViewById( R.id.rlD2 );
        rlDeal3 = findViewById( R.id.rlD3 );
        rlDeal4 = findViewById( R.id.rlD4 );

        rrRecharge = findViewById( R.id.rr_recharge );
        rrProfile = findViewById( R.id.rr_profile );
        rrCategory = findViewById( R.id.rr_category );
        rrSchool = findViewById( R.id.rrSchool );
        rrShare = findViewById( R.id.rrShare );
        rrInsurance = findViewById( R.id.rrInsurance );
        rrSearch = findViewById( R.id.rlSearch );

        //ImageView
        ivMenu = findViewById( R.id.iv_menu );
        ivDrop = findViewById( R.id.imageView3 );
        ivSearch = findViewById( R.id.searchmain );
        ivDeal1 = findViewById( R.id.ivProduct );
        ivDeal2 = findViewById( R.id.ivD2 );
        ivDeal3 = findViewById( R.id.ivD3 );
        ivDeal4 = findViewById( R.id.ivD4 );
        ivCat1 = findViewById( R.id.ivCat1 );
        ivCat2 = findViewById( R.id.ivCat2 );
        ivCat3 = findViewById( R.id.ivCat3 );
        ivCat4 = findViewById( R.id.ivCat4 );
        ivHome = findViewById( R.id.ivHomeBottom );
        ivCategory = findViewById( R.id.ivCategoryBottom );
        ivFavourites = findViewById( R.id.ivFavouritesBottom );
        ivProfile = findViewById( R.id.ic_profile );
        ivNotification = findViewById( R.id.ic_notification );
        ivCart = findViewById( R.id.ivCart );
        ivLogo = findViewById( R.id.iv_logo );

        //TextView
        tvDName1 = findViewById( R.id.tvProductName );
        tvDName2 = findViewById( R.id.tvD2 );
        tvDName3 = findViewById( R.id.tvD3 );
        tvDName4 = findViewById( R.id.tvD4 );
        tvDPrice1 = findViewById( R.id.tvFPrice );
        tvDPrice2 = findViewById( R.id.tvDPrice2 );
        tvDPrice3 = findViewById( R.id.tvDPrice3 );
        tvDPrice4 = findViewById( R.id.tvDPrice4 );
        tvCat1 = findViewById( R.id.tvCat1 );
        tvCat2 = findViewById( R.id.tvCat2 );
        tvCat3 = findViewById( R.id.tvCat3 );
        tvCat4 = findViewById( R.id.tvCat4 );
        tvClock = findViewById( R.id.tvClock );
        tvViewDOD = findViewById( R.id.tvViewDOD );

        tvHome = findViewById( R.id.tvHomeBottom );
        tvCategory = findViewById( R.id.tvCategoryBottom );
        tvFavourites = findViewById( R.id.tvFavouritesBottom );
        tvProfile = findViewById( R.id.tvProfileBottom );
        tvNotification = findViewById( R.id.tvNotificationBottom );
        tvCount = findViewById( R.id.tvCount );
        tvLogin = findViewById( R.id.tv_login );
        tvUserName = findViewById( R.id.tvUserName );

        //LinearLayout fro bottom views
        llHome = findViewById( R.id.llHome );
        llCategory = findViewById( R.id.llCategory );
        llFavourites = findViewById( R.id.llFavourites );
        llProfile = findViewById( R.id.llProfile );
        llNotification = findViewById( R.id.llNotification );

        rrSearch.setVisibility( View.VISIBLE );
        //Recyclerview
        recyclerView = findViewById( R.id.recyclerview );

        dotsLayout = findViewById( R.id.layoutDots );
        // recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        layoutManager = new GridLayoutManager( mActivity, 2 );
        recyclerView.setLayoutManager( layoutManager );

        recyclerView.addOnScrollListener( new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    Log.v( "ksqbmbq", "1" );
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        Log.v( "ksqbmbq", "2" );

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            //GetDashboardListApi();
                            loading = false;
                            Log.v( "ksqbmbq", "3" );
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        } );

        //SetOnClickListener


        ivLogo.setVisibility( View.VISIBLE );
        ivMenu.setVisibility( View.GONE );

        typeface = Typeface.createFromAsset( mActivity.getAssets(), "centurygothic.otf" );
        try {
            int count = Integer.parseInt( AppSettings.getString( AppSettings.cartCount ) );

            if (count > 0) {
                tvCount.setVisibility( View.VISIBLE );
                tvCount.setText( String.valueOf( count ) );
            } else {
                tvCount.setVisibility( View.GONE );
            }
        } catch (Exception e) {
        }
        if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
            //GetDashboardListApi();
            // getCartListApi();
            // sendFCMApi();
        } else {
            AppUtils.showErrorMessage( tvMain, getString( R.string.errorInternet ), mActivity );
        }

        // getCatData();


        getWindow().setSoftInputMode( 2 );
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals( Config.REGISTRATION_COMPLETE )) {
                    //displayFirebaseRegId();
                    AppSettings.putString( AppSettings.fcmId, regId );
                } else if (intent.getAction().equals( Config.PUSH_NOTIFICATION )) {
                    String message = intent.getStringExtra( "message" );
                    Log.v( "msg", message );
                }
            }
        };

        //GetDashboardListApi();
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get( key );
                Log.d( "Key: ", key + " Value: " + value );
            }
        }

        //TextView
        tvMain = findViewById( R.id.tvHeaderText );
        tvTopFeatures = findViewById( R.id.tvTopFeatures );
        //LinearLayout

        //ScrollView
        scrollView = findViewById( R.id.scroll_side_menu );

        //View
        view = findViewById( R.id.view );


        //RelativeLayout
        rrSearch = findViewById( R.id.rlSearch );
        relativeLayoutSearch = findViewById( R.id.relativeLayoutSearch );
        rrSearch.setVisibility( View.VISIBLE );

        //ImageView
        ivMenu = findViewById( R.id.iv_menu );
        ivDrop = findViewById( R.id.imageView3 );
        ivSearch = findViewById( R.id.searchmain );
        searchEdit = findViewById( R.id.searchEdit );
        ivDeal1 = findViewById( R.id.ivProduct );
        ivDeal2 = findViewById( R.id.ivD2 );
        ivDeal3 = findViewById( R.id.ivD3 );
        ivDeal4 = findViewById( R.id.ivD4 );
        ivCat1 = findViewById( R.id.ivCat1 );
        ivCat2 = findViewById( R.id.ivCat2 );
        ivCat3 = findViewById( R.id.ivCat3 );
        ivCat4 = findViewById( R.id.ivCat4 );
        ivHome = findViewById( R.id.ivHomeBottom );
        tvBestValue = findViewById( R.id.tvBestValue );
        ivCategory = findViewById( R.id.ivCategoryBottom );
        ivFavourites = findViewById( R.id.ivFavouritesBottom );
        ivProfile = findViewById( R.id.ic_profile );
        ivNotification = findViewById( R.id.ic_notification );
        ivCart = findViewById( R.id.ivCart );
        ivLogo = findViewById( R.id.iv_logo );

        //TextView
        tvDName1 = findViewById( R.id.tvProductName );
        tvDName2 = findViewById( R.id.tvD2 );
        tvDName3 = findViewById( R.id.tvD3 );
        ivAll = findViewById( R.id.ivAll );
        tvDName4 = findViewById( R.id.tvD4 );
        tvDPrice1 = findViewById( R.id.tvFPrice );
        tvDPrice2 = findViewById( R.id.tvDPrice2 );
        tvDPrice3 = findViewById( R.id.tvDPrice3 );
        tvDPrice4 = findViewById( R.id.tvDPrice4 );
        tvCat1 = findViewById( R.id.tvCat1 );
        tvCat2 = findViewById( R.id.tvCat2 );
        tvCat3 = findViewById( R.id.tvCat3 );
        tvCat4 = findViewById( R.id.tvCat4 );
        tvClock = findViewById( R.id.tvClock );

        tvAll = findViewById( R.id.tvAll );
        tvHome = findViewById( R.id.tvHomeBottom );
        tvCategory = findViewById( R.id.tvCategoryBottom );
        tvFavourites = findViewById( R.id.tvFavouritesBottom );
        tvProfile = findViewById( R.id.tvProfileBottom );
        tvNotification = findViewById( R.id.tvNotificationBottom );
        tvCount = findViewById( R.id.tvCount );
        tvLogin = findViewById( R.id.tv_login );
        tvUserName = findViewById( R.id.tvUserName );

        //LinearLayout fro bottom views
        llHome = findViewById( R.id.llHome );
        llCategory = findViewById( R.id.llCategory );
        llFavourites = findViewById( R.id.llFavourites );
        llProfile = findViewById( R.id.llProfile );
        llNotification = findViewById( R.id.llNotification );

        tvBestValue.setTextColor( Color.parseColor( "#000000" ) );
        tvTopFeatures.setTextColor( Color.parseColor( "#bfc2c2" ) );
        tvAll.setTextColor( Color.parseColor( "#bfc2c2" ) );

        //onclickListener();

        rrSearch.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity( new Intent( mActivity, SearchActivity.class ) );
            }
        } );

    }

    @Override
    public void onClick(View v) {


    }
}

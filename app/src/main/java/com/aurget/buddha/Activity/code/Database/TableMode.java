package com.aurget.buddha.Activity.code.Database;

public class TableMode {
    static final String SQL_CREATE_MODE = ("CREATE TABLE mode (" +
            modeColumn.name + " VARCHAR,"
            + modeColumn.id + " VARCHAR,"
            + modeColumn.section_id + " VARCHAR,"
            + modeColumn.status + " VARCHAR" + ")");
    public static final String mode = "mode";

    public enum modeColumn {
        name,
        id,
        section_id,
        status
    }
}

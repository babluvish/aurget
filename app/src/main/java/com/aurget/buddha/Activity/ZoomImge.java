package com.aurget.buddha.Activity;

import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.aurget.buddha.Activity.Adapter.SlideAdapterZoomImage;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import uk.co.senab.photoview.PhotoViewAttacher;

public class ZoomImge extends AppCompatActivity {

    ImageView imageView;
    LinearLayout linearLayout;
    int imgCount = 0;
    String product_id = "";
    RecyclerView recycleViewImg;
    ViewPager viewPager,viewPager2;
    TabLayout indicator,indicator2,tabLayoutHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zoom_img);
        recycleViewImg = findViewById(R.id.recycleViewImg);
        viewPager = (ViewPager)findViewById(R.id.viewPager);
        indicator = (TabLayout)findViewById(R.id.indicator);

        LinearLayoutManager manager = new LinearLayoutManager(this,  LinearLayoutManager.HORIZONTAL, false);
        recycleViewImg.setLayoutManager(manager);

        imgCount = getIntent().getIntExtra("imgCount",0);
        //Commonhelper.showToastLong(SubCategoryDetails2.this,""+imgCount);
        product_id = getIntent().getStringExtra("product_id");

        viewPager.setAdapter(new SlideAdapterZoomImage(ZoomImge.this, product_id, imgCount));
        indicator.setupWithViewPager(viewPager, true);

        //recycleViewImg.setAdapter(new MyRecyclerAdapterVendor(null));

        //Picasso.with(ZoomImge.this).load(getIntent().getStringExtra("img")).error(R.mipmap.ic_launcher).into(imageView);
        // final Animation zoomAnimation = AnimationUtils.loadAnimation(ZoomImge.this,R.anim.zoom);
        //imageView.startAnimation(zoomAnimation);
    }

    public class MyRecyclerAdapterVendor extends RecyclerView.Adapter<MyRecyclerAdapterVendor.ViewHolder> {

        private ArrayList<String> jsonObjectal;

        public MyRecyclerAdapterVendor(ArrayList<String> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapterVendor.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.zoom_img_item, parent, false);
            return new MyRecyclerAdapterVendor.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final MyRecyclerAdapterVendor.ViewHolder holder, final int position) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    int i = position+1;
                    PhotoViewAttacher photoViewAttacher = new PhotoViewAttacher(holder.imageView);
                    photoViewAttacher.update();
                    //Picasso.with(SubCategoryDetails2.this).load(getIntent().getStringExtra("image")).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(holder.imageView);
                    Picasso.with(ZoomImge.this).load(InterfaceClass.imgPth2+product_id+"_"+i+"_thumb.jpg").placeholder(R.drawable.img_loader).into(holder.imageView);
                }
            },1000);
        }
        @Override
        public int getItemCount() {
            return imgCount;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView imageView;
            public ViewHolder(View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.imageView);
            }
        }
    }
}
package com.aurget.buddha.Activity.Utils;

import android.app.Activity;
import android.app.Dialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public abstract class SearchMonth {

    public abstract String fromDt(String month__, String year);

    Activity activity;
    Spinner spin, spinner;

    protected SearchMonth(Activity activity) {
        this.activity = activity;
    }

    public void monthNameDialog() {
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.month_list);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        RecyclerView recycleViewmonth = dialog.findViewById(R.id.recycleViewmonth);
        final RadioButton rdo1 = dialog.findViewById(R.id.rdo1);
        final RadioButton rdo2 = dialog.findViewById(R.id.rdo2);
        final Button searchBt = dialog.findViewById(R.id.searchBt);
        final TextView month = dialog.findViewById(R.id.month);
        final TextView year = dialog.findViewById(R.id.year);
        spin = dialog.findViewById(R.id.spinner);
        spinner = dialog.findViewById(R.id.spinner2);

        getAllYear();

        year_ = "2019";

        rdo1.setOnClickListener(view -> {
            rdo2.setChecked(false);
            year_ = "2019";
        });

        rdo2.setOnClickListener(view -> {

            rdo1.setChecked(false);
            year_ = "2020";
        });

        month.setOnClickListener(view -> {
            pos = "1";
            month.setVisibility(View.GONE);
            spinner.performClick();
        });

        year.setOnClickListener(view -> {
            pos = "2";
            year.setVisibility(View.GONE);
            spin.performClick();
        });

        searchBt.setOnClickListener(view -> {
            if (month.getVisibility()==View.VISIBLE) {
                Commonhelper.showToastLong(activity, "Please select month");
            } else if (year.getVisibility()==View.VISIBLE) {
                Commonhelper.showToastLong(activity, "Please select year");
            } else {
                fromDt(year_, monthStr);
                Commonhelper.sop("->" + year_);
                Commonhelper.sop("->" + monthStr);
                dialog.dismiss();
            }
        });

        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recycleViewmonth.setLayoutManager(horizontalLayoutManager);
        recycleViewmonth.setItemAnimator(new DefaultItemAnimator());

        ArrayList<String> jsonObjects = new ArrayList<>();

        jsonObjects.add("January");
        jsonObjects.add("Fabruary");
        jsonObjects.add("March");
        jsonObjects.add("April");
        jsonObjects.add("May");
        jsonObjects.add("June");
        jsonObjects.add("July");
        jsonObjects.add("August");
        jsonObjects.add("September");
        jsonObjects.add("October");
        jsonObjects.add("November");
        jsonObjects.add("December");

        month(jsonObjects);

        recycleViewmonth.setAdapter(new MyRecyclerAdapterMonth(jsonObjects));
        dialog.show();

    }

    public class MyRecyclerAdapterMonth extends RecyclerView.Adapter<MyRecyclerAdapterMonth.ViewHolder> {

        private ArrayList<String> jsonObjectal;
        int lastSelectedPosition = -1;

        public MyRecyclerAdapterMonth(ArrayList<String> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapterMonth.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.month_list_item, parent, false);
            return new MyRecyclerAdapterMonth.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapterMonth.ViewHolder holder, int position) {
            holder.monthTv.setText(jsonObjectal.get(position));
            holder.rdo.setChecked(lastSelectedPosition == position);
            int i = position + 1;
            if (String.valueOf(i).length() == 1)
                holder.monthTv2.setText("0" + i);
            else
                holder.monthTv2.setText("" + i);
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView monthTv, monthTv2;
            RadioButton rdo;

            public ViewHolder(View itemView) {
                super(itemView);

                monthTv = itemView.findViewById(R.id.monthTv);
                monthTv2 = itemView.findViewById(R.id.monthTv2);
                rdo = itemView.findViewById(R.id.rdo);

                rdo.setOnClickListener(v -> {
                    monthStr = monthTv2.getText().toString();
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                });
            }
        }
    }

    void getAllYear() {
        try {
            ArrayList<String> ja = new ArrayList<>();
            int curYear = 2019;
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            //year = 3000;
            int year_ = year - curYear;
            String yeat__ = null;
            JSONObject jsonObject = null;

            for (int i = 0; i < year_; i++) {
                if (String.valueOf(i).length() == 1) {
                    jsonObject = new JSONObject();
                    yeat__ = "202" + String.valueOf(i);
                    Log.i("yaer", yeat__);
                    jsonObject.put("name", yeat__);
                    ja.add(yeat__);
                } else if (String.valueOf(i).length() == 2) {
                    if (i == 10) {
                        for (int i2 = 0; i2 < 10; i2++) {
                            jsonObject = new JSONObject();
                            yeat__ = "203" + String.valueOf(i2);
                            jsonObject.put("name", yeat__);
                            Log.i("yaer__", yeat__);
                            ja.add(yeat__);
                        }
                    } else if (i == 20) {
                        for (int i3 = 0; i3 < 10; i3++) {
                            jsonObject = new JSONObject();
                            yeat__ = "204" + String.valueOf(i3);
                            jsonObject.put("name", yeat__);
                            Log.i("yaer___", yeat__);
                            ja.add(yeat__);
                        }
                    } else if (i == 30) {
                        for (int i4 = 0; i4 < 10; i4++) {
                            jsonObject = new JSONObject();
                            yeat__ = "205" + String.valueOf(i4);
                            jsonObject.put("name", yeat__);
                            Log.i("yaer____", yeat__);
                            ja.add(yeat__);
                        }
                    } else if (i == 40) {
                        for (int i4 = 0; i4 < 10; i4++) {
                            jsonObject = new JSONObject();
                            yeat__ = "206" + String.valueOf(i4);
                            jsonObject.put("name", yeat__);
                            Log.i("yaer_____", yeat__);
                            ja.add(yeat__);
                        }
                    } else if (i == 50) {
                        for (int i4 = 0; i4 < 10; i4++) {
                            jsonObject = new JSONObject();
                            yeat__ = "207" + String.valueOf(i4);
                            jsonObject.put("name", yeat__);
                            Log.i("yaer______", yeat__);
                            ja.add(yeat__);
                        }
                    } else if (i == 60) {
                        for (int i4 = 0; i4 < 10; i4++) {
                            jsonObject = new JSONObject();
                            yeat__ = "208" + String.valueOf(i4);
                            jsonObject.put("name", yeat__);
                            Log.i("yaer_______", yeat__);
                            ja.add(yeat__);
                        }
                    } else if (i == 70) {
                        for (int i4 = 0; i4 < 10; i4++) {
                            jsonObject = new JSONObject();
                            yeat__ = "209" + String.valueOf(i4);
                            jsonObject.put("name", yeat__);
                            Log.i("yaer________", yeat__);
                            ja.add(yeat__);
                        }
                    }
                }
            }

            getBtCount(ja);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void getBtCount(ArrayList<String> ja) {
        Log.i("yaer++", ja.toString());
        ArrayAdapter aa = new ArrayAdapter(activity, android.R.layout.simple_spinner_item, ja);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(aa);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                Commonhelper.sop("->" + String.valueOf(spin.getItemAtPosition(position)));
                year_ = String.valueOf(spin.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Commonhelper.sop("->" +"noooooooooo");
            }
        });

    }

    void month(ArrayList<String> ja) {
        Log.i("yaer++", ja.toString());
        ArrayAdapter aa = new ArrayAdapter(activity, android.R.layout.simple_spinner_item, ja);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(aa);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                Commonhelper.sop("->" + String.valueOf(spinner.getItemAtPosition(position)));
                monthStr = String.valueOf(spinner.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    private String monthStr = "";
    private String year_ = "";
    String pos = "0";
}

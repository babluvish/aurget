package com.aurget.buddha.Activity.code.Product;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.Vibrator;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Database.DatabaseController;
import com.aurget.buddha.Activity.code.Database.TableFavourite;
import com.aurget.buddha.Activity.code.Database.TableSubCategory;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.Search.SearchActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import code.Adapter.SortAdapter;
import code.utils.AppUrls;

public class ProductListActivity extends BaseActivity implements View.OnClickListener, SearchView.OnQueryTextListener {

    ProductAdapter productAdapter;
    Parcelable listState;
    public static String categoryId = "";
    ArrayList<HashMap<String, String>> SubCategoryList = new ArrayList();
    ArrayList<HashMap<String, String>> productList = new ArrayList();
    ArrayList<HashMap<String, String>> sortsList = new ArrayList();
    ArrayList<HashMap<String, String>> datalist = new ArrayList();
    ArrayList<HashMap<String, String>> hashMapArrayList = new ArrayList<>();
    ArrayList<HashMap<String, String>> arrFilterList = new ArrayList<>();

    public ArrayList<String> cbarrayList = new ArrayList<>();

    RecyclerView recyclerView;
    RelativeLayout rrNoData;
    ImageView ivMenu, ivSearch, ivCart;
    String Subid, shortBy = "";
    Typeface typeface;
    String offset = "0", sortId = "";
    GridLayoutManager mGridLayoutManager;
    TextView tvCount, tvSort, tvFilter;
    SortAdapter sortAdapter;
    String sid = "";
    boolean loadMore = true;
    RecyclerView rv_bottom;

    SearchView searchView;
    ImageView search;
    TextView tvHeader;
    String filterId = "";

    // boolean loading = true;

    int pastVisiblesItems, visibleItemCount, totalItemCount;

    NewProductAdapter newProductAdapter;
    // NestedScrollView nsv;

    FilterAdpter filterAdapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        findById();
    }

    private void findById() {
        rrNoData = findViewById(R.id.rrNoData);
        //ImageView
        ivMenu = findViewById(R.id.iv_menu);
        ivSearch = findViewById(R.id.searchmain);
        ivCart = findViewById(R.id.ivCart);

        //Recycleview
        recyclerView = findViewById(R.id.productRecyclerView);
        rv_bottom = findViewById(R.id.recyclerview);

        searchView = findViewById(R.id.searchView);
        search = findViewById(R.id.search);
        searchView.setOnQueryTextListener(this);

        int searchSrcTextId = getResources().getIdentifier("android:id/search_src_text", null, null);
        EditText searchEditText = searchView.findViewById(searchSrcTextId);
        searchEditText.setTextColor(Color.BLACK); // set the text color
        searchEditText.setHintTextColor(Color.BLACK); // set the hint color
        //NestedScrollView
        //  nsv = findViewById( R.id.nsv );


        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        rv_bottom.setLayoutManager(linearLayoutManager);

        //typeface = Typeface.createFromAsset(mActivity.getAssets(), "centurygothic.otf");

        //TextView
        tvCount = findViewById(R.id.tvCount);
        tvSort = findViewById(R.id.textView7);
        tvFilter = findViewById(R.id.textView8);
        tvHeader = findViewById(R.id.tvHeaderText);
        tvHeader.setText("Product List");
        search.setVisibility(View.VISIBLE);

        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        ivCart.setOnClickListener(this);
        tvSort.setOnClickListener(this);
        tvFilter.setOnClickListener(this);
        search.setOnClickListener(this);
        ivMenu.setImageResource(R.drawable.ic_back);

        mGridLayoutManager = new GridLayoutManager(mActivity, 2);
        recyclerView.setLayoutManager(mGridLayoutManager);

        productList.clear();
        sortsList.clear();

        newProductAdapter = new NewProductAdapter(productList, datalist);
        recyclerView.setAdapter(newProductAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        // ViewCompat.setNestedScrollingEnabled(recyclerView, false);
        // recyclerView.setItemViewCacheSize(1000);
        // recyclerView.smoothScrollToPosition(0);
        recyclerView.setHasFixedSize(true);


        Log.v("MyPagePath", "ProductList");
        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getNewProductApi();
            //getProductListApi();
        } else {
            AppUtils.showErrorMessage(rrNoData, getString(R.string.errorInternet), mActivity);
        }

/*
        recyclerView.addOnScrollListener(new EndScrollListener(mGridLayoutManager) {
            @Override
            public void onScrolledToEnd() {
                Log.e("Position", "Last item reached");
                if (loadMore) {

                    if (listState != null) {
                        recyclerView.getLayoutManager().onRestoreInstanceState(listState);
                    }

                    callNewPage();
                }
            }
        });
*/

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = mGridLayoutManager.getChildCount();
                    totalItemCount = mGridLayoutManager.getItemCount();
                    pastVisiblesItems = mGridLayoutManager.findFirstVisibleItemPosition();

                    if (!loadMore) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loadMore = true;
                            //page = page+1;
                            getNewProductApi();
                        }
                    }
                }
            }
        });

/*
        nsv.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = (View) nsv.getChildAt(nsv.getChildCount() - 1);

                int diff = (view.getBottom() - (nsv.getHeight() + nsv.getScrollY()));

                if (diff == 0) {
                    // your pagination code
                    visibleItemCount = mGridLayoutManager.getChildCount();
                    totalItemCount = mGridLayoutManager.getItemCount();
                    pastVisiblesItems = mGridLayoutManager.findFirstVisibleItemPosition();
                    if (!loadMore) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loadMore = true;
                            //page = page+1;
                           // AppUtils.hideDialog();

                            getNewProductApi();
                        }
                    }
                }
            }
        });
*/

        findViewById(R.id.allRl).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productList.clear();
                datalist.clear();
                getNewProductApi2();
            }
        });

        getCatData();
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        productList.clear();
        sortsList.clear();
        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            offset = "0";
            productList.clear();
            sortsList.clear();
            //getNewProductApi();
            getNewProductApiSearch();
            //getProductListApi();
        } else {
            AppUtils.showErrorMessage(rrNoData, getString(R.string.errorInternet), mActivity);
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        productList.clear();
        sortsList.clear();
        if (!TextUtils.isEmpty(s) && s.length() >= 3)
        {
            search.setVisibility(View.GONE);

            if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                offset = "0";
                productList.clear();
                sortsList.clear();
                //getNewProductApi();
                getNewProductApiSearch();
                // getProductListApi();
            } else {
                AppUtils.showErrorMessage(rrNoData, getString(R.string.errorInternet), mActivity);
            }
            return false;
        } else {
            return true;
        }


    }

    public void getCatData() {
        SubCategoryList.clear();

        SubCategoryList.addAll(DatabaseController.getSubCategoryData(TableSubCategory.sub_category, TableSubCategory.subCatColumn.category_id.toString(), categoryId));

        Log.v("hello", categoryId);

        if (SubCategoryList.size() > 0) {
            Log.v("hello1", String.valueOf(SubCategoryList.size()));
            rrNoData.setVisibility(View.GONE);
            rv_bottom.setVisibility(View.VISIBLE);
            BottomAdapter bottomAdapter = new BottomAdapter(SubCategoryList);
            rv_bottom.setAdapter(bottomAdapter);
            // recyclerview.setNestedScrollingEnabled(true);
            // recyclerview.smoothScrollToPosition(0);
            // setSubCatAdapter();
        } else {
            rrNoData.setVisibility(View.VISIBLE);
            rv_bottom.setVisibility(View.GONE);
        }
    }

    //callNewPage
    protected void callNewPage() {

        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {

            getNewProductApi();
            // getProductListApi();
        } else {
            AppUtils.showErrorMessage(rrNoData, getString(R.string.errorInternet), mActivity);
        }
    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_menu:
                if (searchView.getVisibility() == View.VISIBLE) {
                    search.setVisibility(View.VISIBLE);
                    tvHeader.setVisibility(View.VISIBLE);
                    searchView.setVisibility(View.GONE);
                } else {
                    finish();
                }
                return;
            case R.id.searchmain:
                startActivity(new Intent(getBaseContext(), SearchActivity.class));
                return;

            case R.id.textView7:
                AlertPopUp();
                return;

            case R.id.textView8:
                arrFilterList.clear();
                cbarrayList.clear();
                AlertPopUpFilter();
                // AlertPopUp();
                return;

            case R.id.ivCart:

                if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else if (AppSettings.getString(AppSettings.verified).contains("0")) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else {
                    startActivity(new Intent(getBaseContext(), CartListActivity.class));
                }

                return;

            case R.id.search:
                searchView.setVisibility(View.VISIBLE);
                tvHeader.setVisibility(View.GONE);
                return;

            default:
                return;
        }
    }

    public void UpdateSize(final String units, final String quantity, View v, final String cartId, final TextView spVal, final TextView price, final TextView finalprice, final TextView tvDiscount, final LinearLayout llCartCountBtn, final LinearLayout llCartBtn) {
        Context wrapper = new ContextThemeWrapper(mActivity, R.style.PopupMenuOverlapAnchor);
        PopupMenu popup = new PopupMenu(wrapper, v);
        HashMap<String, String> hashMap = new HashMap<>();
        popup.getMenu().clear();
        try {
            for (int k = 0; k < datalist.size(); k++) {
                if (cartId.equalsIgnoreCase(datalist.get(k).get("parentId"))) {
                    popup.getMenu().add(k, k, k, datalist.get(k).get("sub_weight") + datalist.get(k).get("unit_name"));
                    hashMap.put("sub_id", datalist.get(k).get("sub_id"));
                    hashMap.put("aprice", datalist.get(k).get("actual_p"));
                    hashMap.put("price", datalist.get(k).get("price"));
                    hashMap.put("per_amount", datalist.get(k).get("per_amount"));
                    hashMap.put("dis_type", datalist.get(k).get("dis_type"));
                    hashMap.put("cart_quantity", datalist.get(k).get("cart_quantity"));
                    hashMapArrayList.add(hashMap);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_main, popup.getMenu());
        popup.show();
        llCartBtn.setVisibility(View.VISIBLE);
        llCartCountBtn.setVisibility(View.GONE);

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final MenuItem item) {

                int id = item.getItemId();
                Log.v("MenuitemIdSelected", String.valueOf(id));
                llCartBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                            Toast.makeText(mActivity, "Please Login to continue", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(mActivity, LoginActivity.class));
                        } else {
                            llCartBtn.setVisibility(View.GONE);
                            llCartCountBtn.setVisibility(View.VISIBLE);
                            AddToCartApi(datalist.get(item.getOrder()).get("sub_id"));
                        }
                    }
                });

                finalprice.setText(getString(R.string.rs) + datalist.get(item.getOrder()).get("actual_p"));
                /*spVal.setText( item.getTitle().toString() );*/
                spVal.setText(item.getTitle().toString());

                if (datalist.get(item.getOrder()).get("per_amount").equals("0.00")) {
                    tvDiscount.setVisibility(View.INVISIBLE);
                    price.setText(getString(R.string.rs) + datalist.get(item.getOrder()).get("price"));
                } else {
                    tvDiscount.setVisibility(View.VISIBLE);
                    tvDiscount.setText(getString(R.string.rs) + datalist.get(item.getOrder()).get("per_amount") + " off");
                }
                if (datalist.get(item.getOrder()).get("dis_type").equals("123")) {
                    price.setVisibility(View.GONE);

                } else {
                    price.setVisibility(View.VISIBLE);
                    price.setText(getString(R.string.rs) + datalist.get(item.getOrder()).get("price"));
                }
                //ToDo..
                Subid = datalist.get(item.getOrder()).get("sub_id");
                Log.v("subid", Subid);
                return true;
            }
        });
    }

    private void AddToCartApi(String productId) {
        AppUtils.showRequestDialog(mActivity);
        Log.v("AddToCartApi", AppUrls.addToCart);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("product_id", productId);
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json.put(AppConstants.result, json_data);
            Log.v("AddToCartApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.addToCart)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseCartdata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            //  AppUtils.showErrorMessage(tvDeatils, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            //  AppUtils.showErrorMessage(tvDeatils, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseCartdata(JSONObject response) {

        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                getCartListApi();
                //recreate();
                AppUtils.showErrorMessage(tvCount, String.valueOf(jsonObject.getString("res_msg")), mActivity);
                //  startActivity(new Intent(getBaseContext(), CartListActivity.class));
            } else {
                // startActivity(new Intent(mActivity, CartListActivity.class));
                AppUtils.showErrorMessage(tvCount, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            // AppUtils.showErrorMessage(tvDeatils, String.valueOf(e), this.mActivity);
        }
        AppUtils.hideDialog();
    }

    private void UpdateCartApi(String quantity, String ProductId) {

        Log.v("AddToCartApi", AppUrls.updateCart);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("product_id", ProductId);
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json_data.put("quantity", quantity);
            json.put(AppConstants.result, json_data);
            Log.v("AddToCartApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.updateCart)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedata(JSONObject response) {
        Log.d("response", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
                // cartList.clear();
                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    getCartListApi();
                } else {
                    AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                }
            } else {
                AppUtils.hideDialog();
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.hideDialog();
            AppUtils.showErrorMessage(recyclerView, String.valueOf(e), mActivity);
        }
    }

    private void DeleteCartApi(String cartId) {
        AppUtils.showRequestDialog(mActivity);
        Log.v("AddToCartApi", AppUrls.deleteFromCart);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("cart_id", cartId);
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json.put(AppConstants.result, json_data);
            Log.v("AddToCartApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.deleteFromCart)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedeletedata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedeletedata(JSONObject response) {
        Log.d("response ", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
                AppUtils.hideDialog();
                //cartList.clear();
                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    getCartListApi();
                } else {
                    AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                }
            } else {
                AppUtils.hideDialog();
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.hideDialog();
            AppUtils.showErrorMessage(recyclerView, String.valueOf(e), mActivity);
        }

    }

    private void getCartListApi() {
        // AppUtils.showRequestDialog(mActivity);
        Log.v("getCartListApi", AppUrls.getCart);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json.put(AppConstants.result, json_data);
            Log.v("getCartListApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.getCart)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsecarttdata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvCount, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvCount, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsecarttdata(JSONObject response) {
        AppSettings.putString(AppSettings.cartCount, "0");
        Log.d("response ", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                JSONArray productArray = jsonObject.getJSONArray("cart_product");
                AppSettings.putString(AppSettings.cartCount, String.valueOf(productArray.length()));
                if (productArray.length() > 0) {
                    tvCount.setVisibility(View.VISIBLE);
                    tvCount.setText(String.valueOf(productArray.length()));
                    AppSettings.putString(AppSettings.cartPoint, tvCount.getText().toString());
                    // ((DashBoardFragment) getActivity()).tvCount.setVisibility(View.VISIBLE);
                    Log.v("jjdj", String.valueOf(productArray.length()));
                } else {
                    tvCount.setVisibility(View.GONE);
                }
            } else {
                tvCount.setVisibility(View.GONE);
                AppSettings.putString(AppSettings.cartCount, "0");
            }
        } catch (Exception e) {
            tvCount.setVisibility(View.GONE);
            AppSettings.putString(AppSettings.cartCount, "0");
        }
        AppUtils.hideDialog();
        /*if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getProfileApi();
        } else {
            AppUtils.showErrorMessage(tvMain, getString(R.string.errorInternet), mActivity);
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();

        int count = Integer.parseInt(AppSettings.getString(AppSettings.cartCount));

        if (count > 0) {
            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(String.valueOf(count));
        } else {
            tvCount.setVisibility(View.GONE);
        }
    }

   /* @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
         listState = recyclerView.getLayoutManager().onSaveInstanceState();
         outState.putParcelable("1", listState);
    }

    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        if(state != null)
            listState = state.getParcelable("1");
    }*/


    private void addFavApi(String productId) {

        AppUtils.showRequestDialog(mActivity);

        Log.v("addFavApi", AppUrls.wishList);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json_data.put("product_master_id", productId);

            json.put(AppConstants.result, json_data);

            Log.v("addFavApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.wishList)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseFavdata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvCount, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvCount, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseFavdata(JSONObject response) {

        Log.d("response ", response.toString());

        DatabaseController.removeTable(TableFavourite.favourite);

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                if (jsonObject.getString("status").equals("1")) {
                    AppUtils.showErrorMessage(tvCount, "Product Added to Wishlist", mActivity);
                } else if (jsonObject.getString("status").equals("2")) {
                    AppUtils.showErrorMessage(tvCount, "Product Removed from Wishlist", mActivity);
                }

                JSONArray wishArray = jsonObject.getJSONArray("wish_list_product");

                for (int i = 0; i < wishArray.length(); i++) {

                    JSONObject productobject = wishArray.getJSONObject(i);

                    ContentValues mContentValues = new ContentValues();

                    mContentValues.put(TableFavourite.favouriteColumn.productId.toString(), productobject.getString("product_id"));

                    DatabaseController.insertData(mContentValues, TableFavourite.favourite);
                }

            } else {
                AppUtils.showErrorMessage(tvCount, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvCount, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();

        productAdapter.notifyDataSetChanged();
    }

    //AlertPopUp
    private void AlertPopUp() {

        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alertsort);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();

        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        //TextView
        TextView tvYes = dialog.findViewById(R.id.tvOk);
        TextView tvCancel = dialog.findViewById(R.id.tvcancel);
        TextView tvAlertMsg = dialog.findViewById(R.id.tvAlertMsg);
        ListView listView = dialog.findViewById(R.id.listView);
        RadioGroup rg = dialog.findViewById(R.id.rg);
        RadioButton rb1 = dialog.findViewById(R.id.rb1);
        RadioButton rb2 = dialog.findViewById(R.id.rb2);
        RadioButton rb3 = dialog.findViewById(R.id.rb3);
        RadioButton rb4 = dialog.findViewById(R.id.rb4);


        dialog.show();

        rb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    shortBy = "1";
                }
            }
        });

        rb2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    shortBy = "2";
                }
            }
        });

        rb3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    shortBy = "3";
                }
            }
        });

        rb4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    shortBy = "4";
                }
            }
        });

        tvYes.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog.dismiss();
                productList.clear();
                sortsList.clear();
                offset = "0";
                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    getNewProductApi();
                } else {
                    AppUtils.showErrorMessage(rrNoData, getString(R.string.errorInternet), mActivity);
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        sortAdapter = new SortAdapter(mActivity, sortsList) {
            protected void catClick(View v, String position) {

                for (int i = 0; i < sortsList.size(); i++) {
                    HashMap<String, String> sortList = new HashMap();
                    sortList.put("id", sortsList.get(i).get("id"));
                    sortList.put("sort_name", sortsList.get(i).get("sort_name"));
                    sortList.put("status", "0");
                    sortsList.set(i, sortList);
                }

                sortId = sortsList.get(Integer.parseInt(position)).get("id");

                HashMap<String, String> sortList = new HashMap();
                sortList.put("id", sortsList.get(Integer.parseInt(position)).get("id"));
                sortList.put("sort_name", sortsList.get(Integer.parseInt(position)).get("sort_name"));
                sortList.put("status", "1");

                sortsList.set(Integer.parseInt(position), sortList);

                sortAdapter.notifyDataSetChanged();
            }
        };
        listView.setAdapter(sortAdapter);
    }

    private class ProductAdapter extends RecyclerView.Adapter<FavNameHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();
        ArrayList<HashMap<String, String>> data2 = new ArrayList<HashMap<String, String>>();
        int count;

        public ProductAdapter(ArrayList<HashMap<String, String>> favList, ArrayList<HashMap<String, String>> subProductListArrayList) {
            data = favList;
            data2 = subProductListArrayList;
        }


        public FavNameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_product, parent, false);
            FavNameHolder holder = new FavNameHolder(view);
            holder.setIsRecyclable(false);
            return holder;

            // return new FavNameHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_product, parent, false));

        }

        public void onBindViewHolder(final FavNameHolder holder, final int position) {

            if (!data2.get(position).get("cart_quantity").equalsIgnoreCase("")) {
                count = Integer.parseInt(data2.get(position).get("cart_quantity"));
            } else {
                count = 1;
            }


            holder.tvName.setText((CharSequence) ((HashMap) data.get(position)).get("product_name"));
            if (((HashMap) data.get(position)).get("product_discount_type").equals("1")) {
                holder.tvPrice.setVisibility(View.VISIBLE);
                holder.tvDiscount.setText(getString(R.string.rs) + " " + ((HashMap) data.get(position)).get("product_discount_amount") + "  /-off");
            } else if (((HashMap) data.get(position)).get("product_discount_type").equals("2")) {
                holder.tvPrice.setVisibility(View.VISIBLE);
                holder.tvDiscount.setText(getString(R.string.rs) + " " + ((HashMap) data.get(position)).get("product_discount_amount") + "% off");
            } else if (data.get(position).get("product_discount_type").equals("123")) {
                // holder.tvDiscount.setText("");
                holder.tvDiscount.setVisibility(View.INVISIBLE);
                holder.tvPrice.setVisibility(View.GONE);
            }
            if (((String) ((HashMap) data.get(position)).get("final_price")).equalsIgnoreCase((String) ((HashMap) data.get(position)).get("price"))) {
                holder.tvFPrice.setText(getString(R.string.rs) + " " + ((HashMap) data.get(position)).get("final_price"));
                holder.tvDiscount.setText("");
                holder.tvDiscount.setVisibility(View.INVISIBLE);
                holder.tvPrice.setText("");
            } else {
                holder.tvFPrice.setText(getString(R.string.rs) + " " + ((HashMap) data.get(position)).get("final_price"));
                holder.tvPrice.setText(getString(R.string.rs) + " " + ((HashMap) data.get(position)).get("price"));
                //  holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.ANTI_ALIAS_FLAG);
                holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | 16);
            }
            try {
                if (((HashMap) data.get(position)).get("product_image") != null && !((String) ((HashMap) data.get(position)).get("product_image")).isEmpty()) {
                    Picasso.with(mActivity).load((String) ((HashMap) data.get(position)).get("product_image")).into(holder.ivPic);
                }
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }
            if (DatabaseController.checkRecordExist(TableFavourite.favourite, String.valueOf(TableFavourite.favouriteColumn.productId), data.get(position).get("id"))) {
                holder.icWishlist.setImageResource(R.drawable.ic_heart_red);
            } else {
                holder.icWishlist.setImageResource(R.drawable.ic_heart_grey);
            }


            holder.icWishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!AppSettings.getString(AppSettings.userId).isEmpty()) {
                        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                            addFavApi(data.get(position).get("id"));
                        } else {
                            AppUtils.showErrorMessage(tvCount, getString(R.string.errorInternet), mActivity);
                        }
                    } else {
                        AppUtils.showErrorMessage(tvCount, "Kindly login first", mActivity);
                    }
                }
            });

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent mIntent = new Intent(mActivity, ProductDetailsActivity.class);
                    mIntent.putExtra(AppConstants.productId, data.get(position).get("id"));
                    mIntent.putExtra(AppConstants.from, "3");
                    mIntent.putExtra(AppConstants.CartQuantityValue, "6");
                    startActivity(mIntent);
                }
            });


            //holder.llCartCountBtn.setVisibility(View.GONE);
            //holder.llCartBtn.setVisibility(View.VISIBLE);

            holder.llCartBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                        startActivity(new Intent(mActivity, LoginActivity.class));
                    } else {
                        holder.llCartCountBtn.setVisibility(View.VISIBLE);
                        holder.llCartBtn.setVisibility(View.GONE);
                        AddToCartApi(data.get(position).get("id"));
                    }
                }
            });

            //  holder.tvSpinner.setText( data.get( position ).get( "SpinerValue" ) );
            //  Log.v( "ndn", data.get( position ).get( "SpinerValue" ) );

            holder.tvSpinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UpdateSize(data.get(position).get("sub_weight"), data.get(position).get("cart_quantity"), view, data.get(position).get("id"), holder.tvSpinner, holder.tvPrice, holder.tvFPrice, holder.tvDiscount, holder.llCartCountBtn, holder.llCartBtn);
                    if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                        //  startActivity( new Intent( mActivity, LoginActivity.class ) );
                    } else {
                        count = (Integer.parseInt(holder.tvQuantityCount.getText().toString()));
                        holder.tvQuantityCount.setText(String.valueOf(count));
                        Log.v("CountValue", String.valueOf(count));
                       /* holder.llCartCountBtn.setVisibility( View.GONE );
                        holder. llCartBtn.   setVisibility( View.VISIBLE );*/
                    }
                }
            });
            Log.v("datasub", data2.toString());
            Log.v("data2", data2.toString());
            holder.tvQuantityCount.setText(String.valueOf(count));

          /*  holder.tvSpinner.setText( data.get( position ).get( "SpinerValue" ) );
            Log.v( "ndn", data.get( position ).get( "SpinerValue" ) );
*/
            holder.tvSpinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UpdateSize(data.get(position).get("sub_weight"), data.get(position).get("cart_quantity"), view, data.get(position).get("id"), holder.tvSpinner, holder.tvPrice, holder.tvFPrice, holder.tvDiscount, holder.llCartCountBtn, holder.llCartBtn);
                    if (AppSettings.getString(AppSettings.userId).isEmpty()) {

                    } else {
                        count = (Integer.parseInt(holder.tvQuantityCount.getText().toString()));
                        holder.tvQuantityCount.setText(String.valueOf(count));
                        Log.v("CountValue", String.valueOf(count));
                       /* holder.llCartCountBtn.setVisibility( View.GONE );
                        holder. llCartBtn.   setVisibility( View.VISIBLE );*/
                    }
                }
            });
            Log.v("datasub", data2.toString());
            Log.v("data2", data2.toString());
            holder.tvQuantityCount.setText(String.valueOf(count));

            AppSettings.putString(AppSettings.subiid, data2.get(position).get("sub_id"));
            holder.imageView3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    count = (Integer.parseInt(holder.tvQuantityCount.getText().toString())) - 1;
                    if (count <= 1) {
                        Vibrator vibe = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
                        vibe.vibrate(100);
                        holder.llCartCountBtn.setVisibility(View.GONE);
                        holder.llCartBtn.setVisibility(View.VISIBLE);
                        String dd = "714";
                        DeleteCartApi(dd);
                    } else if (count > 1) {
                        // if (count > 1) {
                        // count = count - 1;
                        // ((DashBoardFragment) getActivity()).tvCount.setText(Integer.toString(count));
                        Vibrator vibe = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
                        vibe.vibrate(100);
                        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                            UpdateCartApi(String.valueOf(count), data2.get(position).get("sub_id"));
                            holder.tvQuantityCount.setText((count) + "");
                        } else {
                            AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                        }
                    } /*else if (count == 1) {
                            DeleteCartApi( count );
                            AppUtils.showErrorMessage( recyclerView, getString( R.string.minusQuantityError ), mActivity );
                        }*/

                }
                  /*  int quant = Integer.parseInt( data2.get( position ).get( "quantity" ) );
                    if (quant > 1) {
                        quant = quant - 1;
                        Vibrator vibe = (Vibrator) mActivity.getSystemService( Context.VIBRATOR_SERVICE );
                        vibe.vibrate( 100 );
                        if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                            UpdateCartApi( String.valueOf( quant ), data2.get( position ).get( "sub_id" ), position );
                        } else {
                            AppUtils.showErrorMessage( recyclerView, getString( R.string.errorInternet ), mActivity );
                        }
                    } else {
                        DeleteCartApi( data2.get( position ).get( "sub_id" ) );
                        AppUtils.showErrorMessage( recyclerView, getString( R.string.minusQuantityError ), mActivity );
                    }*/

                   /* int quant = (Integer.parseInt( holder.tvQuantityCount.getText().toString() ));
                    if (quant > 1) {
                        quant = quant - 1;
                        Vibrator vibe = (Vibrator) mActivity.getSystemService( Context.VIBRATOR_SERVICE );
                        vibe.vibrate( 100 );

                        if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                            UpdateCartApi( String.valueOf( quant ), data.get( position ).get( "product_id" ) );
                        } else {
                            AppUtils.showErrorMessage( recyclerView, getString( R.string.errorInternet ), mActivity );
                        }
                    } else {
                      //  DeleteCartApi( data.get( position ).get( "cart_id" ) );
                        DeleteCartApi(quant );
                        AppUtils.showErrorMessage( recyclerView, getString( R.string.minusQuantityError ), mActivity );
                    }

*/

            });
            holder.imageView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    count = (Integer.parseInt(holder.tvQuantityCount.getText().toString())) + 1;
                    if (count < 10) {
                        // ((DashBoardFragment) getActivity()).tvCount.setText(Integer.toString(count));
                        AppSettings.putString(AppSettings.cartValue, tvCount.getText().toString());
                        //count = count + 1;
                        Vibrator vibe = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
                        vibe.vibrate(100);
                        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                            UpdateCartApi(String.valueOf(count), data2.get(position).get("sub_id"));
                            holder.tvQuantityCount.setText((count) + "");
                        } else {
                            AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                        }
                    } else {

                        AppUtils.showErrorMessage(recyclerView, getString(R.string.addQuantityError), mActivity);
                    }
                }
            });


        }

        public int getItemCount() {
            return data.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }
    }


    private void getProductListApi() {
        AppUtils.showRequestDialog(mActivity);
        Log.v("getProductListApi", AppUrls.getProduct);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                json_data.put("user_id", "0");
            } else {
                json_data.put("user_id", AppSettings.getString(AppSettings.userId));
            }
            json_data.put("sub_category_id", getIntent().getStringExtra(AppConstants.subCategoryId));
            json_data.put("come_from", getIntent().getStringExtra(AppConstants.from));
            json_data.put("pageindex", offset);
            json_data.put("sort_id", sortId);
            json_data.put("filter_ids", "");
            json.put(AppConstants.result, json_data);
            Log.v("getProductListApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.getProduct)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        parseJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(rrNoData, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(rrNoData, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseJsondata(JSONObject response) {
        Log.d("getProductListApi", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                offset = jsonObject.getString("pageindex");
                JSONArray productArray = jsonObject.getJSONArray("products");
                loadMore = productArray.length() > 8;
                for (int i = 0; i < productArray.length(); i++) {
                    JSONObject productobject = productArray.getJSONObject(i);
                    HashMap<String, String> prodList = new HashMap();
                    prodList.put("product_name", productobject.getString("product_name"));
                    prodList.put("final_price", productobject.getString("final_price"));
                    prodList.put("id", productobject.getString("id"));
                    prodList.put("price", productobject.getString("price"));
                    prodList.put("product_discount_amount", productobject.getString("product_discount_amount"));
                    prodList.put("product_discount_type", productobject.getString("product_discount_type"));
                    prodList.put("product_image", productobject.getString("product_image"));
                    prodList.put("sub_product", productobject.getString("sub_product"));


                    if (productobject.has("sub_product")) {
                        JSONArray newsliderarray = productobject.getJSONArray("sub_product");
                        for (int j = 0; j < newsliderarray.length(); j++) {
                            JSONObject sliderObject = newsliderarray.getJSONObject(j);
                            HashMap<String, String> sliderMap = new HashMap<>();
                            if (j == 0) {
                                productobject.put("SpinerValue", sliderObject.get("sub_weight").toString() + sliderObject.get("unit_name").toString());
                            }
                            sliderMap.put("sub_id", sliderObject.get("sub_id").toString());
                            sliderMap.put("dis_type", sliderObject.get("dis_type").toString());
                            sliderMap.put("per_amount", sliderObject.get("per_amount").toString());
                            sliderMap.put("sub_weight", sliderObject.get("sub_weight").toString());
                            sliderMap.put("unit_name", sliderObject.get("unit_name").toString());
                            sliderMap.put("actual_p", sliderObject.get("actual_p").toString());
                            sliderMap.put("price", sliderObject.get("price").toString());
                            sliderMap.put("parentId", productobject.getString("id"));
                            sliderMap.put("cart_quantity", sliderObject.getString("cart_quantity"));

                            datalist.add(sliderMap);
                            Log.v("dshf", sliderObject.get("dis_type").toString());
                        }
                    }


                    productList.add(prodList);


                }
                if (productList.size() <= 0) {
                    rrNoData.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                } else {
                    rrNoData.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);

                  /*  if (productList.size() > 40) {
                        productAdapter.notifyDataSetChanged();
                    } else {*/
                    productAdapter = new ProductAdapter(productList, datalist);
                    recyclerView.setAdapter(productAdapter);
                    recyclerView.setNestedScrollingEnabled(false);
                    recyclerView.smoothScrollToPosition(0);

                    //  }
                }
                JSONArray sortArray = jsonObject.getJSONArray("sorts");

                for (int i = 0; i < sortArray.length(); i++) {
                    JSONObject sortobject = sortArray.getJSONObject(i);
                    HashMap<String, String> sortList = new HashMap();

                    sortList.put("id", sortobject.getString("id"));
                    sortList.put("sort_name", sortobject.getString("sort_name"));
                    sortList.put("status", "0");
                    sortsList.add(sortList);
                }
            } else {
                loadMore = false;
                AppUtils.showErrorMessage(rrNoData, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(rrNoData, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
    }


    private void getNewProductApiSearch() {
        filterId = String.valueOf(cbarrayList);
        filterId = filterId.replace("[", "");
        filterId = filterId.replace("]", "");
        filterId = filterId.replace(" ", "");

        AppUtils.showRequestDialog(mActivity);
        Log.v("getProductListApi", AppUrls.getComman);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                json_data.put("user_id", "0");
            } else {
                json_data.put("user_id", AppSettings.getString(AppSettings.userId));
            }

            //json_data.put("sub_category_id", sid);
            json_data.put("sub_category_id", "");
            json_data.put("home_product_type", "");
            json_data.put("search_key", searchView.getQuery());
            //json_data.put("category_id", categoryId);
            json_data.put("category_id", "");
            json_data.put("product_id", "");
            //json_data.put("offset", offset);
            json_data.put("offset", "0");
            //json_data.put("short_by", "2");
            //json_data.put("brand_id", filterId);
            json.put(AppConstants.result, json_data);
            Log.v("getProductListApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.getComman)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseNewJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(rrNoData, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(rrNoData, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void getNewProductApi() {
        filterId = String.valueOf(cbarrayList);
        filterId = filterId.replace("[", "");
        filterId = filterId.replace("]", "");
        filterId = filterId.replace(" ", "");

        AppUtils.showRequestDialog(mActivity);
        Log.v("getProductListApi", AppUrls.getComman);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                json_data.put("user_id", "0");
            } else {
                json_data.put("user_id", AppSettings.getString(AppSettings.userId));
            }

            json_data.put("sub_category_id", sid);
            json_data.put("home_product_type", "");
            json_data.put("search_key", searchView.getQuery());
            json_data.put("category_id", categoryId);
            json_data.put("product_id", "");
            //json_data.put("offset", offset);
            json_data.put("offset", "0");
            json_data.put("short_by", "2");
            json_data.put("brand_id", filterId);
            json.put(AppConstants.result, json_data);
            Log.v("getProductListApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.getComman)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseNewJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(rrNoData, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(rrNoData, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void getNewProductApi2()
    {
        filterId = String.valueOf(cbarrayList);
        filterId = filterId.replace("[", "");
        filterId = filterId.replace("]", "");
        filterId = filterId.replace(" ", "");

        AppUtils.showRequestDialog(mActivity);
        Log.v("getProductListApi", AppUrls.getComman);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                json_data.put("user_id", "0");
            } else {
                json_data.put("user_id", AppSettings.getString(AppSettings.userId));
            }
            json_data.put("sub_category_id", "");
            json_data.put("home_product_type", "");
            json_data.put("search_key", "");
            json_data.put("category_id", categoryId);
            json_data.put("product_id", "");
            //json_data.put("offset", offset);
            json_data.put("offset", "0");
            json_data.put("short_by", "2");
            json_data.put("brand_id", filterId);
            json.put(AppConstants.result, json_data);
            Log.v("getProductListApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.getComman)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseNewJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(rrNoData, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(rrNoData, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseNewJsondata(JSONObject response) {
        AppUtils.hideDialog();
        Log.d("getProductListApi", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                offset = jsonObject.getString("offset");
                JSONArray productArray = jsonObject.getJSONArray("product_master");

                //  loadMore = productArray.length() > 8;

                for (int i = 0; i < productArray.length(); i++) {

                    JSONObject productobject = productArray.getJSONObject(i);

                    HashMap<String, String> prodList = new HashMap();

                    prodList.put("id", productobject.getString("id"));
                    prodList.put("category_master_id", productobject.getString("category_master_id"));
                    prodList.put("sub_category_master_id", productobject.getString("sub_category_master_id"));
                    prodList.put("product_name", productobject.getString("product_name"));
                    prodList.put("image", productobject.getString("image"));
                    prodList.put("price", productobject.getString("price"));
                    prodList.put("final_price", productobject.getString("final_price"));
                    prodList.put("product_discount_type", productobject.getString("product_discount_type"));
                    prodList.put("product_discount_amount", productobject.getString("product_discount_amount"));
                    prodList.put("quantity", productobject.getString("quantity"));
                    prodList.put("weight", productobject.getString("weight"));
                    prodList.put("unit_name", productobject.getString("unit_name"));

                    prodList.put("sub_product", productobject.optString("sub_product"));
                    if (productobject.has("sub_product")) {
                        JSONArray newsliderarray = productobject.getJSONArray("sub_product");
                        for (int j = 0; j < newsliderarray.length(); j++) {
                            JSONObject sliderObject = newsliderarray.getJSONObject(j);
                            HashMap<String, String> sliderMap = new HashMap<>();
                            if (j == 0) {
                                productobject.put("SpinerValue", sliderObject.get("sub_weight").toString() + sliderObject.get("unit_name").toString());
                            }
                            sliderMap.put("sub_id", sliderObject.get("sub_id").toString());
                            sliderMap.put("dis_type", sliderObject.get("dis_type").toString());
                            sliderMap.put("per_amount", sliderObject.get("per_amount").toString());
                            sliderMap.put("sub_weight", sliderObject.get("sub_weight").toString());
                            sliderMap.put("unit_name", sliderObject.get("unit_name").toString());
                            sliderMap.put("actual_p", sliderObject.get("actual_p").toString());
                            sliderMap.put("price", sliderObject.get("price").toString());
                            sliderMap.put("parentId", productobject.getString("id"));
                            sliderMap.put("image", productobject.getString("image"));
                            sliderMap.put("cart_quantity", sliderObject.getString("cart_quantity"));
                            sliderMap.put("cartId", sliderObject.getString("cartId"));
                            datalist.add(sliderMap);
                            Log.v("dshf", sliderObject.get("dis_type").toString());
                        }
                    }

                    productList.add(prodList);
                }
                if (productList.size() <= 0) {
                    rrNoData.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                } else {
                    rrNoData.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
                newProductAdapter.notifyDataSetChanged();
                loadMore = false;

                // JSONArray sortArray = jsonObject.getJSONArray("sorts");

//                for (int i = 0; i < sortArray.length(); i++) {
//                    JSONObject sortobject = sortArray.getJSONObject(i);
//                    HashMap<String, String> sortList = new HashMap();
//
//                    sortList.put("id", sortobject.getString("id"));
//                    sortList.put("sort_name", sortobject.getString("sort_name"));
//                    sortList.put("status", "0");
//
//                    sortsList.add(sortList);
//                }
            } else {
                loadMore = false;
                AppUtils.showErrorMessage(rrNoData, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            // AppUtils.showErrorMessage(rrNoData, String.valueOf(e), mActivity);
        }

    }

    private class NewProductAdapter extends RecyclerView.Adapter<FavNameHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();
        ArrayList<HashMap<String, String>> data2 = new ArrayList<HashMap<String, String>>();
        int count = 1;

        public NewProductAdapter(ArrayList<HashMap<String, String>> favList, ArrayList<HashMap<String, String>> subProductListArrayList) {
            data = favList;
            data2 = subProductListArrayList;
        }

        public FavNameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // return new FavNameHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_product, parent, false));
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_product, parent, false);
            FavNameHolder holder = new FavNameHolder(view);
            holder.setIsRecyclable(false);
            return holder;
        }

        public void onBindViewHolder(final FavNameHolder holder, final int position) {
            //AnimationHelper.animatate( mActivity, holder.itemView, R.anim.alfa_animation );

            if (!data2.get(position).get("cart_quantity").equalsIgnoreCase("")) {
                Log.d("Count", "onBindViewHolder: " + data2.get(position).get("cart_quantity"));
                count = Integer.parseInt(data2.get(position).get("cart_quantity"));
                holder.llCartCountBtn.setVisibility(View.VISIBLE);
                holder.llCartBtn.setVisibility(View.GONE);
                holder.tvQuantityCount.setText(count + "");
            } else {
                count = 1;
                holder.llCartCountBtn.setVisibility(View.GONE);
                holder.llCartBtn.setVisibility(View.VISIBLE);
            }


            holder.tvName.setText((CharSequence) ((HashMap) data.get(position)).get("product_name"));
            if (((HashMap) data.get(position)).get("product_discount_type").equals("1")) {
                holder.tvPrice.setVisibility(View.VISIBLE);
                holder.tvDiscount.setText(getString(R.string.rs) + " " + ((HashMap) data.get(position)).get("product_discount_amount") + "  /-off");
            } else if (((HashMap) data.get(position)).get("product_discount_type").equals("2")) {
                holder.tvPrice.setVisibility(View.VISIBLE);
                holder.tvDiscount.setText(getString(R.string.rs) + " " + ((HashMap) data.get(position)).get("product_discount_amount") + "% off");
            } else if (data.get(position).get("product_discount_type").equals("123")) {
                // holder.tvDiscount.setText("");
                holder.tvDiscount.setVisibility(View.INVISIBLE);
                holder.tvPrice.setVisibility(View.GONE);
            }
            if (((String) ((HashMap) data.get(position)).get("final_price")).equalsIgnoreCase((String) ((HashMap) data.get(position)).get("price"))) {
                holder.tvFPrice.setText(getString(R.string.rs) + " " + ((HashMap) data.get(position)).get("final_price"));
                holder.tvDiscount.setText("");
                holder.tvDiscount.setVisibility(View.INVISIBLE);
                holder.tvPrice.setText("");
            } else {
                holder.tvFPrice.setText(getString(R.string.rs) + " " + ((HashMap) data.get(position)).get("final_price"));
                holder.tvPrice.setText(getString(R.string.rs) + " " + ((HashMap) data.get(position)).get("price"));
                //  holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.ANTI_ALIAS_FLAG);
                holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | 16);
            }
            try {
                if (((HashMap) data.get(position)).get("image") != null && !((String) ((HashMap) data.get(position)).get("image")).isEmpty()) {
                    Picasso.with(mActivity).load((String) ((HashMap) data.get(position)).get("image")).into(holder.ivPic);
                }
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }
         /*   if (DatabaseController.checkRecordExist(TableFavourite.favourite, String.valueOf(TableFavourite.favouriteColumn.productId), data.get(position).get("id"))) {
                holder.icWishlist.setImageResource(R.drawable.ic_heart_red);
            } else {
                holder.icWishlist.setImageResource(R.drawable.ic_heart_grey);
            }*/


            holder.icWishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!AppSettings.getString(AppSettings.userId).isEmpty()) {
                        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                            addFavApi(data.get(position).get("id"));
                        } else {
                            AppUtils.showErrorMessage(tvCount, getString(R.string.errorInternet), mActivity);
                        }
                    } else {
                        AppUtils.showErrorMessage(tvCount, "Kindly login first", mActivity);
                    }
                }
            });

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent mIntent = new Intent(mActivity, ProductDetailsActivity.class);
                    mIntent.putExtra(AppConstants.productId, data.get(position).get("id"));
                    mIntent.putExtra(AppConstants.from, "3");
                    mIntent.putExtra(AppConstants.CartQuantityValue, "6");
                    startActivity(mIntent);
                }
            });


            // holder.llCartCountBtn.setVisibility(View.GONE);
            // holder.llCartBtn.setVisibility(View.VISIBLE);

            holder.llCartBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                        Toast.makeText(mActivity, "Please Login to continue", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(mActivity, LoginActivity.class));
                    } else {
                        holder.llCartCountBtn.setVisibility(View.VISIBLE);
                        holder.llCartBtn.setVisibility(View.GONE);
                        AddToCartApi(data.get(position).get("id"));
                        datalist.get(position).put("cart_quantity", "1");
                    }
                }
            });

            //  holder.tvSpinner.setText( "Select Quantity" );

            //  Log.v( "ndn", data.get( position ).get( "SpinerValue" ) );

            holder.tvSpinner.setText(data.get(position).get("weight") + data.get(position).get("unit_name"));

            holder.tvSpinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UpdateSize(data.get(position).get("weight"), data.get(position).get("quantity"), view, data.get(position).get("id"), holder.tvSpinner, holder.tvPrice, holder.tvFPrice, holder.tvDiscount, holder.llCartCountBtn, holder.llCartBtn);
                    if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                        //  startActivity( new Intent( mActivity, LoginActivity.class ) );
                    } else {
                        count = (Integer.parseInt(holder.tvQuantityCount.getText().toString()));
                        holder.tvQuantityCount.setText(String.valueOf(count));
                        Log.v("CountValue", String.valueOf(count));
                       /* holder.llCartCountBtn.setVisibility( View.GONE );
                        holder. llCartBtn.   setVisibility( View.VISIBLE );*/
                    }
                }
            });
            //   Log.v("datasub", data2.toString());
            //   Log.v("data2", data2.toString());

            holder.tvQuantityCount.setText(String.valueOf(count));

          /*  holder.tvSpinner.setText( data.get( position ).get( "SpinerValue" ) );
            Log.v( "ndn", data.get( position ).get( "SpinerValue" ) );
*/
            holder.tvSpinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UpdateSize(data.get(position).get("weight"), data.get(position).get("quantity"), view, data.get(position).get("id"), holder.tvSpinner, holder.tvPrice, holder.tvFPrice, holder.tvDiscount, holder.llCartCountBtn, holder.llCartBtn);
                    if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                        //  startActivity( new Intent( mActivity, LoginActivity.class ) );
                    } else {
                        count = (Integer.parseInt(holder.tvQuantityCount.getText().toString()));
                        holder.tvQuantityCount.setText(String.valueOf(count));
                        Log.v("CountValue", String.valueOf(count));
                       /* holder.llCartCountBtn.setVisibility( View.GONE );
                        holder. llCartBtn.   setVisibility( View.VISIBLE );*/
                    }
                }
            });
            // Log.v("datasub", data2.toString());
            // Log.v("data2", data2.toString());
            holder.tvQuantityCount.setText(String.valueOf(count));

            AppSettings.putString(AppSettings.subiid, data2.get(position).get("sub_id"));
            holder.imageView3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    count = (Integer.parseInt(holder.tvQuantityCount.getText().toString())) - 1;
                    if (count == 0) {
                        Vibrator vibe = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
                        vibe.vibrate(100);
                        holder.llCartCountBtn.setVisibility(View.GONE);
                        holder.llCartBtn.setVisibility(View.VISIBLE);
                        String dd = "714";
                        DeleteCartApi(data2.get(position).get("cartId"));
                    } else if (count > 0) {
                        // if (count > 1) {
                        // count = count - 1;
                        // ((DashBoardFragment) getActivity()).tvCount.setText(Integer.toString(count));
                        Vibrator vibe = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
                        vibe.vibrate(100);
                        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                            UpdateCartApi(String.valueOf(count), data2.get(position).get("sub_id"));
                            holder.tvQuantityCount.setText((count) + "");
                            datalist.get(position).put("cart_quantity", count + "");
                        } else {
                            AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                        }
                    } /*else if (count == 1) {
                            DeleteCartApi( count );
                            AppUtils.showErrorMessage( recyclerView, getString( R.string.minusQuantityError ), mActivity );
                        }*/

                }
                  /*  int quant = Integer.parseInt( data2.get( position ).get( "quantity" ) );
                    if (quant > 1) {
                        quant = quant - 1;
                        Vibrator vibe = (Vibrator) mActivity.getSystemService( Context.VIBRATOR_SERVICE );
                        vibe.vibrate( 100 );
                        if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                            UpdateCartApi( String.valueOf( quant ), data2.get( position ).get( "sub_id" ), position );
                        } else {
                            AppUtils.showErrorMessage( recyclerView, getString( R.string.errorInternet ), mActivity );
                        }
                    } else {
                        DeleteCartApi( data2.get( position ).get( "sub_id" ) );
                        AppUtils.showErrorMessage( recyclerView, getString( R.string.minusQuantityError ), mActivity );
                    }*/

                   /* int quant = (Integer.parseInt( holder.tvQuantityCount.getText().toString() ));
                    if (quant > 1) {
                        quant = quant - 1;
                        Vibrator vibe = (Vibrator) mActivity.getSystemService( Context.VIBRATOR_SERVICE );
                        vibe.vibrate( 100 );

                        if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                            UpdateCartApi( String.valueOf( quant ), data.get( position ).get( "product_id" ) );
                        } else {
                            AppUtils.showErrorMessage( recyclerView, getString( R.string.errorInternet ), mActivity );
                        }
                    } else {
                      //  DeleteCartApi( data.get( position ).get( "cart_id" ) );
                        DeleteCartApi(quant );
                        AppUtils.showErrorMessage( recyclerView, getString( R.string.minusQuantityError ), mActivity );
                    }

*/

            });
            holder.imageView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    count = (Integer.parseInt(holder.tvQuantityCount.getText().toString())) + 1;
                    if (count <= Integer.parseInt(data.get(position).get("quantity"))) {
                        // ((DashBoardFragment) getActivity()).tvCount.setText(Integer.toString(count));
                        AppSettings.putString(AppSettings.cartValue, tvCount.getText().toString());
                        //count = count + 1;
                        Vibrator vibe = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
                        vibe.vibrate(100);
                        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                            UpdateCartApi(String.valueOf(count), data2.get(position).get("sub_id"));
                            holder.tvQuantityCount.setText((count) + "");
                            datalist.get(position).put("cart_quantity", count + "");
                        } else {
                            AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                        }
                    } else {

                        AppUtils.showErrorMessage(recyclerView, getString(R.string.addQuantityError) + " " + data.get(position).get("quantity"), mActivity);
                    }
                }
            });
        }

        public int getItemCount() {
            return data.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }
    }

    private class FavNameHolder extends RecyclerView.ViewHolder {
        ImageView ivPic, icWishlist, imageView2, imageView3;
        LinearLayout linearLayout, llCartBtn, llCartCountBtn;
        TextView tvDiscount;
        TextView tvFPrice;
        TextView tvName;
        RelativeLayout btnSpinner;
        TextView tvPrice, tvSpinner, tvQuantityCount;
        Spinner spinners;

        public FavNameHolder(View itemView) {
            super(itemView);
            linearLayout = itemView.findViewById(R.id.layout_item);
            ivPic = itemView.findViewById(R.id.ivProduct);
            icWishlist = itemView.findViewById(R.id.ic_wishlist);
            tvName = itemView.findViewById(R.id.tvProductName);
            tvFPrice = itemView.findViewById(R.id.tvFPrice);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvDiscount = itemView.findViewById(R.id.tvDiscount);
            tvSpinner = itemView.findViewById(R.id.tvSpinner);
            spinners = itemView.findViewById(R.id.spinners);
            tvQuantityCount = itemView.findViewById(R.id.tvQuantityCount);
            llCartCountBtn = itemView.findViewById(R.id.llCartCountBtn);
            llCartBtn = itemView.findViewById(R.id.llCartBtn);
            btnSpinner = itemView.findViewById(R.id.btnSpinner);
            imageView2 = itemView.findViewById(R.id.imageView2);
            imageView3 = itemView.findViewById(R.id.imageView3);
        }
    }


    private class BottomAdapter extends RecyclerView.Adapter<NameHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

        public BottomAdapter(ArrayList<HashMap<String, String>> favList) {
            this.data = favList;
        }

        public NameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new NameHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.subcategoryadapter, parent, false));
        }

        public void onBindViewHolder(NameHolder holder, final int position) {

            holder.tvSubcategory.setText(((String) ((HashMap) this.data.get(position)).get("sub_category_name")).trim());
            if (((String) ((HashMap) data.get(position)).get("icon")).isEmpty()) {
                Picasso.with(mActivity).load(R.mipmap.ic_launcher).into(holder.ivPic);
            } else {
                Picasso.with(mActivity).load((String) ((HashMap) data.get(position)).get("icon")).into(holder.ivPic);
            }

            holder.rr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sid = (String) ((HashMap) SubCategoryList.get(position)).get("sub_category_id");
                    productList.clear();
                    datalist.clear();
                    getNewProductApi();

                    //                    Intent mIntent = new Intent( mActivity, ProductListActivity.class );
                    //                    mIntent.putExtra( AppConstants.subCategoryId, (String) ((HashMap) SubCategoryList.get( position )).get( "sub_category_id" ) );
                    //                    mIntent.putExtra( AppConstants.from, "4" );
                    //                    startActivity( mIntent );
                }
            });
        }

        public int getItemCount() {
            return data.size();
        }
    }


    private class NameHolder extends RecyclerView.ViewHolder {
        ImageView ivPic;
        RelativeLayout rr, rr2;
        TextView tvSubcategory;

        public NameHolder(View itemView) {
            super(itemView);

            rr = itemView.findViewById(R.id.rr);
            ivPic = itemView.findViewById(R.id.imageView24);
            tvSubcategory = itemView.findViewById(R.id.textView29);

        }
    }

    private void AlertPopUpFilter() {

        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_filter);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        //TextView
        TextView tvYes = dialog.findViewById(R.id.tvOk);
        TextView tvCancel = dialog.findViewById(R.id.tvcancel);
        TextView tvAlertMsg = dialog.findViewById(R.id.tvAlertMsg);
        RecyclerView rlFilter = dialog.findViewById(R.id.rlFilter);


        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getFilterData(rlFilter, dialog);
        } else {
            AppUtils.showErrorMessage(rrNoData, getString(R.string.errorInternet), mActivity);
        }

        tvYes.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog.dismiss();
                productList.clear();
                sortsList.clear();
                offset = "0";
                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    getNewProductApi();

                } else {
                    AppUtils.showErrorMessage(rrNoData, getString(R.string.errorInternet), mActivity);
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cbarrayList.clear();
                dialog.dismiss();
            }
        });


    }


    void getFilterData(final RecyclerView rvFilter, final Dialog dialog) {

        AppUtils.hideSoftKeyboard(mActivity);
        AppUtils.showRequestDialog(mActivity);


        String url = AppUrls.getBrand;
        Log.v("getBrand-URL", url);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {

            json_data.put("category_id", categoryId);
            json_data.put("sub_category_id", sid);

            json.put(AppConstants.result, json_data);

            Log.v("getBrand", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }


        AndroidNetworking.post(url)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .setTag("getBrand")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseFilterJsondate(response, rvFilter, dialog);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showToastSort(mActivity, String.valueOf(error.getErrorCode()));
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showToastSort(mActivity, String.valueOf(error.getErrorDetail()));
                        }
                    }
                });
    }


    private void parseFilterJsondate(JSONObject response, RecyclerView rvFilter, Dialog dialog) {
        Log.v("filter", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            AppUtils.hideDialog();
            String mesg = jsonObject.getString("res_msg");
            if (jsonObject.getString("res_code").equals("1")) {
                dialog.show();
                JSONArray jsonArray = jsonObject.getJSONArray("list");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("id", jsonObject1.getString("id"));
                    hashMap.put("brand_name", jsonObject1.getString("brand_name"));
                    arrFilterList.add(hashMap);
                }

                filterAdapter = new FilterAdpter(arrFilterList);
                GridLayoutManager gridLayoutManager = new GridLayoutManager(mActivity, 1);
                rvFilter.setLayoutManager(gridLayoutManager);
                rvFilter.setAdapter(filterAdapter);

            } else {
                Toast.makeText(this, mesg, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private class FilterAdpter extends RecyclerView.Adapter<Holder> {
        ArrayList<HashMap<String, String>> arrSourceIncomeList = new ArrayList<>();

        public FilterAdpter(ArrayList<HashMap<String, String>> arrSourceIncomeList) {

            this.arrSourceIncomeList = arrSourceIncomeList;
        }

        @NonNull

        @Override
        public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_filter, null));
        }

        @Override
        public void onBindViewHolder(@NonNull Holder holder, final int position) {

            holder.cb.setText(arrSourceIncomeList.get(position).get("brand_name"));

            holder.cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        cbarrayList.add(arrSourceIncomeList.get(position).get("id"));
                        Log.v("check", cbarrayList + "");
                    } else {
                        for (int i = 0; i < arrSourceIncomeList.size(); i++) {
                            if (cbarrayList.get(i).equals(arrSourceIncomeList.get(position).get("id"))) {
                                cbarrayList.remove(i);
                                break;
                            }
                        }
                        //cbarrayList.remove( arrSourceIncomeList.get( position ).get( "id" ) );
                        Log.v("uncheck", cbarrayList + "");
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return arrSourceIncomeList.size();
        }
    }

    private class Holder extends RecyclerView.ViewHolder {
        //CheckBox
        CheckBox cb;

        public Holder(View itemView) {
            super(itemView);
            cb = itemView.findViewById(R.id.cb);
        }
    }
}

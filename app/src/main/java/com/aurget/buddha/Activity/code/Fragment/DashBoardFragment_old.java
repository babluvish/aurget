package com.aurget.buddha.Activity.code.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.code.Account.AccountActivity;
import com.aurget.buddha.Activity.code.Account.FavouriteListActivity;
import com.aurget.buddha.Activity.code.Category.CategoryActivity;
import com.aurget.buddha.Activity.code.Common.NotificationUtils;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Database.DatabaseController;
import com.aurget.buddha.Activity.code.Database.TableFavourite;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.Main.NotificationActivity;
import com.aurget.buddha.Activity.code.Product.CartListActivity;
import com.aurget.buddha.Activity.code.Search.SearchActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


import code.Common.Config;
import code.utils.AppUrls;

public class DashBoardFragment_old extends BaseActivity implements View.OnClickListener {
    private boolean doubleBackToExitPressedOnce;
    public static int VB_PAGES;
    public static int VB_FIRST_PAGE;
    public static String regId = "";
    // ViewPager currentpage
    private static int currentPage = 0;
    // ViewPager no offpage
    private static int NUM_PAGES = 0;
    ArrayList<HashMap<String, String>> CategoryList = new ArrayList();
    ArrayList<HashMap<String, String>> FeaturedBannerList = new ArrayList();
    ArrayList<HashMap<String, String>> OfferList = new ArrayList();
    ArrayList<ArrayList<HashMap<String, String>>> ProducttList = new ArrayList();
    ArrayList<HashMap<String, String>> TopBannerList = new ArrayList();
    ArrayList<HashMap<String, String>> FeaturedProductList = new ArrayList();
    GridLayoutManager layoutManager;
    ImageView ivCat1, ivCat2, ivCat3, ivCat4,
            ivDeal1, ivDeal2, ivDeal3, ivDeal4,
            ivDrop, ivMenu, ivSearch, ivCart, ivLogo, ivAll;

    LinearLayout llHome, llCategory,
            llFavourites, llProfile, llNotification;

    TextView tvHome, tvCategory,
            tvFavourites, tvProfile, tvNotification;
     TextView tvCount;
    ImageView ivHome, ivCategory,
            ivFavourites, ivProfile, ivNotification;

    EditText searchEdit;

    TextView tvBestValue;
    ScrollView scrollView;
    TextView tvCat1, tvCat2, tvCat3, tvCat4, tvClock, tvDName1, tvDName2, tvDName3,
            tvDName4, tvDPrice1, tvDPrice2, tvDPrice3,
            tvDPrice4, tvLogin, tvUserName,
            tvMain, tvAll, tvTopFeatures;
    View view;
    FeatureDataFragment featuredFragment;
    RelativeLayout rrSearch, relativeLayoutSearch;
    Typeface typeface;
    //FramLayout
    FrameLayout flDashboard;
    //FragmentManager
    FragmentManager fragmentManager;
    private Handler handler;
    private Runnable runnable;
    //Fragments
    private Fragment fragment;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private LinearLayout dotsLayout;
    private ImageView[] dots;

    //BaseFragment
    private boolean exit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_dashboardfragment );
        init();
        displayView( 0 );
    }
    private void init() {
        flDashboard = findViewById( R.id.frame );
        getWindow().setSoftInputMode( 2 );
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals( Config.REGISTRATION_COMPLETE )) {
                    displayFirebaseRegId();
                    AppSettings.putString( AppSettings.fcmId, regId );
                } else if (intent.getAction().equals( Config.PUSH_NOTIFICATION )) {
                    String message = intent.getStringExtra( "message" );
                    Log.v( "msg", message );
                }
            }
        };


        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get( key );
                Log.d( "Key: ", key + " Value: " + value );
            }
        }

        //TextView
        tvMain = findViewById( R.id.tvHeaderText );
        tvTopFeatures = findViewById( R.id.tvTopFeatures );
        //LinearLayout

        //ScrollView
        scrollView = findViewById( R.id.scroll_side_menu );

        //View
        view = findViewById( R.id.view );
        //RelativeLayout
        rrSearch = findViewById( R.id.rlSearch );
        relativeLayoutSearch = findViewById( R.id.relativeLayoutSearch );
        rrSearch.setVisibility( View.VISIBLE );
        //ImageView
        ivMenu = findViewById( R.id.iv_menu );
        ivDrop = findViewById( R.id.imageView3 );
        ivSearch = findViewById( R.id.searchmain );
        searchEdit = findViewById( R.id.searchEdit );
        ivDeal1 = findViewById( R.id.ivProduct );
        ivDeal2 = findViewById( R.id.ivD2 );
        ivDeal3 = findViewById( R.id.ivD3 );
        ivDeal4 = findViewById( R.id.ivD4 );
        ivCat1 = findViewById( R.id.ivCat1 );
        ivCat2 = findViewById( R.id.ivCat2 );
        ivCat3 = findViewById( R.id.ivCat3 );
        ivCat4 = findViewById( R.id.ivCat4 );
        ivHome = findViewById( R.id.ivHomeBottom );
        tvBestValue = findViewById( R.id.tvBestValue );
        ivCategory = findViewById( R.id.ivCategoryBottom );
        ivFavourites = findViewById( R.id.ivFavouritesBottom );
        ivProfile = findViewById( R.id.ic_profile );
        ivNotification = findViewById( R.id.ic_notification );
        ivCart = findViewById( R.id.ivCart );
        ivLogo = findViewById( R.id.iv_logo );

        //TextView
        tvDName1 = findViewById( R.id.tvProductName );
        tvDName2 = findViewById( R.id.tvD2 );
        tvDName3 = findViewById( R.id.tvD3 );
        ivAll = findViewById( R.id.ivAll );
        tvDName4 = findViewById( R.id.tvD4 );
        tvDPrice1 = findViewById( R.id.tvFPrice );
        tvDPrice2 = findViewById( R.id.tvDPrice2 );
        tvDPrice3 = findViewById( R.id.tvDPrice3 );
        tvDPrice4 = findViewById( R.id.tvDPrice4 );
        tvCat1 = findViewById( R.id.tvCat1 );
        tvCat2 = findViewById( R.id.tvCat2 );
        tvCat3 = findViewById( R.id.tvCat3 );
        tvCat4 = findViewById( R.id.tvCat4 );
        tvClock = findViewById( R.id.tvClock );

        tvAll = findViewById( R.id.tvAll );
        tvHome = findViewById( R.id.tvHomeBottom );
        tvCategory = findViewById( R.id.tvCategoryBottom );
        tvFavourites = findViewById( R.id.tvFavouritesBottom );
        tvProfile = findViewById( R.id.tvProfileBottom );
        tvNotification = findViewById( R.id.tvNotificationBottom );
        tvCount = findViewById( R.id.tvCount );
        tvLogin = findViewById( R.id.tv_login );
        tvUserName = findViewById( R.id.tvUserName );

        //LinearLayout fro bottom views
        llHome = findViewById( R.id.llHome );
        llCategory = findViewById( R.id.llCategory );
        llFavourites = findViewById( R.id.llFavourites );
        llProfile = findViewById( R.id.llProfile );
        llNotification = findViewById( R.id.llNotification );

        tvBestValue.setTextColor( Color.parseColor( "#000000" ) );
        tvTopFeatures.setTextColor( Color.parseColor( "#bfc2c2" ) );
        tvAll.setTextColor( Color.parseColor( "#bfc2c2" ) );
        onclickListener();

        rrSearch.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity( new Intent( mActivity, SearchActivity.class ) );
            }
        } );


    }

    private void onclickListener() {
        llHome.setOnClickListener( this );
        llCategory.setOnClickListener( this );
        llFavourites.setOnClickListener( this );
        llProfile.setOnClickListener( this );
        llNotification.setOnClickListener( this );
        tvBestValue.setOnClickListener( this );
        tvTopFeatures.setOnClickListener( this );
        ivMenu.setOnClickListener( this );
        searchEdit.setOnClickListener( this );
        ivSearch.setOnClickListener( this );
        rrSearch.setOnClickListener( this );

        relativeLayoutSearch.setOnClickListener( this );
        ivCart.setOnClickListener( this );
        tvAll.setOnClickListener( this );
        ivAll.setOnClickListener( this );

        ivLogo.setVisibility( View.VISIBLE );
        ivMenu.setVisibility( View.GONE );
        typeface = Typeface.createFromAsset( mActivity.getAssets(), "centurygothic.otf" );
        try {
            int count = Integer.parseInt( AppSettings.getString( AppSettings.cartCount ) );

            if (count > 0) {
                tvCount.setVisibility( View.VISIBLE );
                tvCount.setText( String.valueOf( count ) );
                Log.v("value",String.valueOf( count ));
            } else {
                tvCount.setVisibility( View.GONE );
            }
        } catch (NumberFormatException e) {

        }
    }

    public void displayView(int position) {
        fragmentManager = getSupportFragmentManager();
        switch (position) {
            case 0:
                fragment = new BestValueFragment();
                break;

            case 1:
                //fragment = new FeaturedFragment();
                fragment = new FeatureDataFragment();
                break;
        }
        fragmentManager.beginTransaction()
                .replace( R.id.frame, fragment )
                .addToBackStack( "" )
                .commit();
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onClick(View v) {
        Intent mIntent;
        switch (v.getId()) {

            case R.id.view:
                ivDrop.setVisibility( View.GONE );
                //scrollView.setVisibility(View.GONE);
                view.setVisibility( View.GONE );
                AppUtils.collapse( scrollView );
                break;

            case R.id.tvBestValue:
                tvBestValue.setTextColor( Color.parseColor( "#000000" ) );
                tvTopFeatures.setTextColor( Color.parseColor( "#bfc2c2" ) );
                tvAll.setTextColor( Color.parseColor( "#bfc2c2" ) );
                displayView( 0 );
                break;

            case R.id.searchEdit:
                startActivity( new Intent( mActivity, SearchActivity.class ) );
                break;

            case R.id.tvTopFeatures:
                tvBestValue.setTextColor( Color.parseColor( "#bfc2c2" ) );
                tvAll.setTextColor( Color.parseColor( "#bfc2c2" ) );
                tvTopFeatures.setTextColor( Color.parseColor( "#000000" ) );
                displayView( 1 );
                break;

            case R.id.tvAll:
                tvBestValue.setTextColor( Color.parseColor( "#bfc2c2" ) );
                tvTopFeatures.setTextColor( Color.parseColor( "#bfc2c2" ) );
                tvAll.setTextColor( Color.parseColor( "#000000" ) );
                startActivity( new Intent( mActivity, CategoryActivity.class ) );
                return;

            case R.id.iv_menu:
                if (scrollView.getVisibility() == View.VISIBLE) {
                    ivDrop.setVisibility( View.GONE );//scrollView.setVisibility(View.GONE);
                    view.setVisibility( View.GONE );
                    AppUtils.collapse( scrollView );
                    break;
                }
                ivDrop.setVisibility( View.VISIBLE );
                view.setVisibility( View.VISIBLE );
                AppUtils.expand( scrollView );
                break;
            case R.id.searchmain:
/*                if (scrollView.getVisibility() == View.VISIBLE) {
                    ivDrop.setVisibility(View.GONE);
                    view.setVisibility(View.GONE);
                    AppUtils.collapse(scrollView);
                    break;
                }
               aiogrocerymartdev@gmail.com
               groxerymartdev
                */
                startActivity( new Intent( mActivity, SearchActivity.class ) );
                break;
            case R.id.rlSearch:
                startActivity( new Intent( mActivity, SearchActivity.class ) );
                break;
            case R.id.ivAll:
                startActivity( new Intent( mActivity, CategoryActivity.class ) );
                break;
            case R.id.relativeLayoutSearch:
                startActivity( new Intent( getBaseContext(), SearchActivity.class ) );
                break;

            case R.id.llCategory:
                tvHome.setTextColor( Color.parseColor( "#9d9b9b" ) );
                tvCategory.setTextColor( Color.parseColor( "#155b72" ) );
                tvFavourites.setTextColor( Color.parseColor( "#9d9b9b" ) );
                tvProfile.setTextColor( Color.parseColor( "#9d9b9b" ) );
                tvNotification.setTextColor( Color.parseColor( "#9d9b9b" ) );
                ivHome.setImageResource( R.drawable.ic_home_grey );
                ivCategory.setImageResource( R.drawable.ic_categories_darkgreen );
                ivFavourites.setImageResource( R.drawable.ic_heart_grey );
                ivNotification.setImageResource( R.drawable.ic_notifications_grey );
                ivProfile.setImageResource( R.drawable.ic_profile_grey );

                startActivity( new Intent( getBaseContext(), CategoryActivity.class ) );

                break;

            case R.id.llFavourites:

                if (AppSettings.getString( AppSettings.userId ).isEmpty()) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else if (AppSettings.getString( AppSettings.verified ).contains( "0" )) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else {

                    tvHome.setTextColor( Color.parseColor( "#9d9b9b" ) );
                    tvCategory.setTextColor( Color.parseColor( "#9d9b9b" ) );
                    tvFavourites.setTextColor( Color.parseColor( "#155b72" ) );
                    tvProfile.setTextColor( Color.parseColor( "#9d9b9b" ) );
                    tvNotification.setTextColor( Color.parseColor( "#9d9b9b" ) );
                    ivHome.setImageResource( R.drawable.ic_home_grey );
                    ivCategory.setImageResource( R.drawable.ic_categories_grey );
                    ivFavourites.setImageResource( R.drawable.ic_heart_darkgreen );
                    ivNotification.setImageResource( R.drawable.ic_notifications_grey );
                    ivProfile.setImageResource( R.drawable.ic_profile_grey );
                    startActivity( new Intent( getBaseContext(), FavouriteListActivity.class ) );
                }

                break;

            case R.id.llNotification:
                tvHome.setTextColor( Color.parseColor( "#9d9b9b" ) );
                tvCategory.setTextColor( Color.parseColor( "#9d9b9b" ) );
                tvFavourites.setTextColor( Color.parseColor( "#9d9b9b" ) );
                tvProfile.setTextColor( Color.parseColor( "#9d9b9b" ) );
                tvNotification.setTextColor( Color.parseColor( "#155b72" ) );
                ivHome.setImageResource( R.drawable.ic_home_grey );
                ivCategory.setImageResource( R.drawable.ic_categories_grey );
                ivFavourites.setImageResource( R.drawable.ic_heart_grey );
                ivNotification.setImageResource( R.drawable.ic_notifications_green );
                ivProfile.setImageResource( R.drawable.ic_profile_grey );

                startActivity( new Intent( getBaseContext(), NotificationActivity.class ) );

                break;

            case R.id.llProfile:

                tvHome.setTextColor( Color.parseColor( "#9d9b9b" ) );
                tvCategory.setTextColor( Color.parseColor( "#9d9b9b" ) );
                tvFavourites.setTextColor( Color.parseColor( "#9d9b9b" ) );
                tvNotification.setTextColor( Color.parseColor( "#9d9b9b" ) );
                tvProfile.setTextColor( Color.parseColor( "#155b72" ) );
                ivHome.setImageResource( R.drawable.ic_home_grey );
                ivCategory.setImageResource( R.drawable.ic_categories_grey );
                ivFavourites.setImageResource( R.drawable.ic_heart_grey );
                ivProfile.setImageResource( R.drawable.ic_profile_darkgreen );
                ivNotification.setImageResource( R.drawable.ic_notifications_grey );

                if (AppSettings.getString( AppSettings.userId ).isEmpty()) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else if (AppSettings.getString( AppSettings.verified ).contains( "0" )) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else {
                    startActivity( new Intent( getBaseContext(), AccountActivity.class ) );
                }

                break;

            case R.id.rr_profile:
                ivDrop.setVisibility( View.GONE );
                view.setVisibility( View.GONE );
                AppUtils.collapse( scrollView );
                startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                if (AppSettings.getString( AppSettings.userId ).isEmpty()) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else if (AppSettings.getString( AppSettings.verified ).contains( "0" )) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else {
                    startActivity( new Intent( getBaseContext(), AccountActivity.class ) );
                }

                break;


            case R.id.ivCart:

                if (AppSettings.getString( AppSettings.userId ).isEmpty()) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else if (AppSettings.getString( AppSettings.verified ).contains( "0" )) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else {
                    startActivity( new Intent( getBaseContext(), CartListActivity.class ) );
                }

                break;

            case R.id.rr_logout:

                ivDrop.setVisibility( View.GONE );
                view.setVisibility( View.GONE );
                AppUtils.collapse( scrollView );

                if (AppSettings.getString( AppSettings.userId ).isEmpty()) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else if (AppSettings.getString( AppSettings.verified ).contains( "0" )) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else {
                    AlertPopUp();
                }

                return;


            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press twice to exit", Toast.LENGTH_SHORT).show();


       /* AppUtils.hideSoftKeyboard( mActivity );
        if (view.getVisibility() == View.VISIBLE) {
            ivDrop.setVisibility( View.GONE );
            //scrollView.setVisibility(View.GONE);
            view.setVisibility( View.GONE );
            AppUtils.collapse( scrollView );
        } else {
            if (exit) {
                Intent intent = new Intent( Intent.ACTION_MAIN );
                intent.addCategory( Intent.CATEGORY_HOME );
                intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                startActivity( intent );
            } else {
                exit = true;
                new Handler().postDelayed( new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 3 * 1000 );
            }
        }*/
    }


    @Override
    protected void onResume() {
        super.onResume();
       /* if(AppSettings.getString(AppSettings.name).equals(""))
        {
            tvUserName.setText(getString(R.string.hi));
        }
        else
        {
            tvUserName.setText(AppSettings.getString(AppSettings.name));
        }

        tvHome.setTextColor(getResources().getColor(R.color.textcolor));
        tvCategory.setTextColor(getResources().getColor(R.color.dark_grey));
        tvFavourites.setTextColor(getResources().getColor(R.color.dark_grey));
        tvProfile.setTextColor(getResources().getColor(R.color.dark_grey));

        ivHome.setImageResource(R.drawable.ic_home_darkgreen);
        ivCategory.setImageResource(R.drawable.ic_categories_grey);
        ivFavourites.setImageResource(R.drawable.ic_heart_grey);
        ivNotification.setImageResource(R.drawable.ic_notifications_grey);
        ivProfile.setImageResource(R.drawable.ic_profile_grey);
*/
        try {
        int count = Integer.parseInt( AppSettings.getString( AppSettings.cartCount ) );
        tvNotification.setTextColor( getResources().getColor( R.color.dark_grey ) );
        if(AppSettings.getString( AppSettings.userId ).isEmpty()){
        }else {
          //  getCartListApi();

        }
        if (count > 0) {
            tvCount.setVisibility( View.VISIBLE );
           tvCount.setText( String.valueOf( count ) );
            Log.v("value2",String.valueOf( count ));
        } else {
            tvCount.setVisibility( View.GONE );
        }
        } catch (NumberFormatException ex) { // handle your exception
            tvCount.setVisibility( View.GONE );
        }
       /* if(AppSettings.getString(AppSettings.userId).isEmpty())
        {
            tvLogin.setText("LoginActivity");
        }
        else if(AppSettings.getString(AppSettings.verified).equals("1"))
        {
            tvLogin.setText("Logout");
        }
        else
        {
            tvLogin.setText("LoginActivity");
        }*/
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance( mActivity ).registerReceiver( mRegistrationBroadcastReceiver,
                new IntentFilter( Config.REGISTRATION_COMPLETE ) );
        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance( mActivity ).registerReceiver( mRegistrationBroadcastReceiver,
                new IntentFilter( Config.PUSH_NOTIFICATION ) );
        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications( getApplicationContext() );
    }
    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance( mActivity ).unregisterReceiver( mRegistrationBroadcastReceiver );
        super.onPause();
    }

    //AlertPopUp
    private void AlertPopUp() {
        final Dialog dialog = new Dialog( this, android.R.style.Theme_Translucent_NoTitleBar );
        dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
        dialog.setContentView( R.layout.alertyesno );
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes( wlp );
        dialog.getWindow().setLayout( WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT );
        dialog.show();
        dialog.setCancelable( true );
        dialog.setCanceledOnTouchOutside( true );

        //TextView
        TextView tvYes = dialog.findViewById( R.id.tvOk );
        TextView tvCancel = dialog.findViewById( R.id.tvcancel );
        TextView tvReason = dialog.findViewById( R.id.textView22 );
        TextView tvAlertMsg = dialog.findViewById( R.id.tvAlertMsg );

        tvAlertMsg.setText( "Confirmation" );
        tvReason.setText( "You will log out from the device. Are you sure?" );
        dialog.show();

        tvYes.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                AppSettings.putString( AppSettings.mobile, "" );
                AppSettings.putString( AppSettings.verified, "" );
                AppSettings.putString( AppSettings.userId, "" );
                AppSettings.putString( AppSettings.cartCount, "0" );
                onResume();
            }
        } );

        tvCancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        } );
    }

    //==================================API=================================================//

    private void getCartListApi() {
        // AppUtils.showRequestDialog(mActivity);
        Log.v( "getCartListApi", AppUrls.getCart );
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put( "user_master_id", AppSettings.getString( AppSettings.userId ) );
            json.put( AppConstants.result, json_data );
            Log.v( "getCartListApi", json.toString() );

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post( AppUrls.getCart )
                .addJSONObjectBody( json )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedata( response );
                    }
                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage( tvMain, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            AppUtils.showErrorMessage( tvMain, String.valueOf( error.getErrorDetail() ), mActivity );
                        }
                    }
                } );
    }
    private void parsedata(JSONObject response) {
        AppSettings.putString( AppSettings.cartCount, "0");
        Log.d( "response ", response.toString() );
        try {
            JSONObject jsonObject = response.getJSONObject( AppConstants.responseTag );
            if (jsonObject.getString( "res_code" ).equals( "1" )) {
                JSONArray productArray = jsonObject.getJSONArray( "cart_product" );
                AppSettings.putString( AppSettings.cartCount, String.valueOf( productArray.length() ) );
                if (productArray.length() > 0) {
                    tvCount.setVisibility( View.VISIBLE );
                    tvCount.setText( String.valueOf( productArray.length() ) );
                } else {
                    tvCount.setVisibility( View.GONE );
                }
            } else {
                tvCount.setVisibility( View.GONE );
                AppSettings.putString( AppSettings.cartCount, "0");
            }
        } catch (Exception e) {
            tvCount.setVisibility( View.GONE );
            AppSettings.putString( AppSettings.cartCount, "0" );
        }
        AppUtils.hideDialog();
        /*if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getProfileApi();
        } else {
            AppUtils.showErrorMessage(tvMain, getString(R.string.errorInternet), mActivity);
        }*/
    }

    private void addFavApi(String productId) {
        AppUtils.showRequestDialog( mActivity );
        Log.v( "addFavApi", AppUrls.wishList );
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put( "user_master_id", AppSettings.getString( AppSettings.userId ) );
            json_data.put( "product_master_id", productId );

            json.put( AppConstants.result, json_data );

            Log.v( "addFavApi", json.toString() );

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post( AppUrls.wishList )
                .addJSONObjectBody( json )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        parseFavdata( response );
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage( tvMain, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            AppUtils.showErrorMessage( tvMain, String.valueOf( error.getErrorDetail() ), mActivity );
                        }
                    }
                } );
    }

    private void parseFavdata(JSONObject response) {
        Log.d( "response ", response.toString() );

        DatabaseController.removeTable( TableFavourite.favourite );

        try {
            JSONObject jsonObject = response.getJSONObject( AppConstants.responseTag );
            if (jsonObject.getString( "res_code" ).equals( "1" )) {

                if (jsonObject.getString( "status" ).equals( "1" )) {
                    AppUtils.showErrorMessage( tvMain, "Product Added to Favourites", mActivity );
                } else if (jsonObject.getString( "status" ).equals( "2" )) {
                    AppUtils.showErrorMessage( tvMain, "Product Removed from Favourites", mActivity );
                }

                JSONArray wishArray = jsonObject.getJSONArray( "wish_list_product" );

                for (int i = 0; i < wishArray.length(); i++) {

                    JSONObject productobject = wishArray.getJSONObject( i );
                    ContentValues mContentValues = new ContentValues();
                    mContentValues.put( TableFavourite.favouriteColumn.productId.toString(), productobject.getString( "product_id" ) );
                    DatabaseController.insertData( mContentValues, TableFavourite.favourite );
                }

            } else {
                AppUtils.showErrorMessage( tvMain, "Product Removed from Favourites", mActivity );
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage( tvMain, String.valueOf( e ), mActivity );
        }
        AppUtils.hideDialog();

    }

    private void getProfileApi() {

        AppUtils.showRequestDialog( mActivity );

        Log.v( "getProfileApi", AppUrls.getUserDetail );

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put( "user_id", AppSettings.getString( AppSettings.userId ) );

            json.put( AppConstants.result, json_data );

            Log.v( "getProfileApi", json.toString() );

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post( AppUrls.getUserDetail )
                .addJSONObjectBody( json )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseProfiledata( response );
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage( tvMain, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            AppUtils.showErrorMessage( tvMain, String.valueOf( error.getErrorDetail() ), mActivity );
                        }
                    }
                } );
    }

    private void parseProfiledata(JSONObject response) {

        Log.d( "response ", response.toString() );

        try {
            JSONObject jsonObject = response.getJSONObject( AppConstants.responseTag );
            if (jsonObject.getString( "res_code" ).equals( "1" )) {

                JSONObject userObject = jsonObject.getJSONObject( "user_detail" );
                AppSettings.putString( AppSettings.name, userObject.getString( "username" ) );
                AppSettings.putString( AppSettings.email, userObject.getString( "email_id" ) );
                AppSettings.putString( AppSettings.gender, userObject.getString( "gender" ) );
                AppSettings.putString( AppSettings.mobile, userObject.getString( "phone_no" ) );
                AppSettings.putString( AppSettings.wallet, userObject.getString( "wallet_amount" ) );
                AppSettings.putString( AppSettings.referralId, userObject.getString( "referel_id" ) );

            } else {
                AppUtils.showErrorMessage( tvMain, String.valueOf( jsonObject.getString( "res_msg" ) ), mActivity );
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage( tvMain, String.valueOf( e ), mActivity );
        }
        AppUtils.hideDialog();

        onResume();

        if (AppSettings.getString( AppSettings.fcmSend ).equals( "0" ) || AppSettings.getString( AppSettings.fcmSend ).isEmpty()) {
            if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                //sendFCMApi();
            } else {
                AppUtils.showErrorMessage( tvMain, getString( R.string.errorInternet ), mActivity );
            }
        }
    }

    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences( Config.SHARED_PREF, 0 );
        regId = pref.getString( "regId", null );
        Log.e( "Firebase reg id: ", regId );
        if (!TextUtils.isEmpty( regId )) {            // txtRegId.setText("Firebase Reg Id: " + regId);
            Log.v( "firebaseid", "Firebase Reg Id: " + regId );
        } else {
            Log.v( "not_received", "Firebase Reg Id is not received yet!" );
        }
    }

    private void GetDashboardListApi() {
        Log.v( "GetDashboardListApi", AppUrls.getDashboard );
        AppUtils.showRequestDialog( mActivity );

        String offset = "0";
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put( "offset", offset );
            if(AppSettings.getString(AppSettings.userId) .isEmpty()){
                json_data.put( "user_id","0");
            }else{
                json_data.put( "user_id",AppSettings.getString(AppSettings.userId) );
            }
            json.put( AppConstants.result, json_data );
            Log.v( "featuredApi", json.toString() );

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post( AppUrls.getDashboard )
                .addJSONObjectBody( json )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Constants.MYJSON=response.toString();
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage( tvCat4, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            AppUtils.showErrorMessage( tvCat4, String.valueOf( error.getErrorDetail() ), mActivity );
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            //Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        }
                    }
                } );
    }

    private void sendFCMApi() {

        Log.v( "sendFCMApi", AppUrls.notificationMaster );

        AppUtils.showRequestDialog( mActivity );
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put( "device_id", AppUtils.getDeviceID( mActivity ) );
            json_data.put( "fcm_id", AppSettings.getString( AppSettings.fcmId ) );
            json.put( AppConstants.result, json_data );

            Log.v( "sendFCMApi", json.toString() );

        } catch (JSONException e) {
            e.printStackTrace();
            Log.v( "printStack", String.valueOf( e ) );
        }

        AndroidNetworking.post( AppUrls.notificationMaster )
                .addJSONObjectBody( json )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseSendFCMdata( response );
                    }

                    @Override
                    public void onError(ANError error) {

                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage( tvMain, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            AppUtils.showErrorMessage( tvMain, String.valueOf( error.getErrorDetail() ), mActivity );
                        }
                    }
                } );
    }

    private void parseSendFCMdata(JSONObject response) {

        Log.d( "response ", response.toString() );
        AppUtils.hideDialog();
        /*  AppSettings.putString(AppSettings.fcmSend,"0");*/

        try {
            JSONObject jsonObject = response.getJSONObject( AppConstants.responseTag );
            if (jsonObject.getString( "res_code" ).equals( "1" )) {
                /* AppSettings.putString(AppSettings.fcmSend,"1");*/
            } else {
                AppUtils.showErrorMessage( tvMain, String.valueOf( jsonObject.getString( "res_msg" ) ), mActivity );
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage( tvMain, String.valueOf( e ), mActivity );
        }
    }
}

package com.aurget.buddha.Activity.Rate;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class RateActivity extends AppCompatActivity {

    TextView productTv, submit;
    ImageView img, horribleImg;
    LinearLayout horribleLL;
    String product_id, count = "";
    EditText commentEt;

    LinearLayout star2LL;
    LinearLayout star3LL;
    LinearLayout star4LL;
    LinearLayout star5LL;

    ImageView statImg2, statImg3, statImg4, statImg5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rate_activity);
        //setContentView(R.layout.rate_product);

        productTv = findViewById(R.id.productTv);
        img = findViewById(R.id.img);
        horribleImg = findViewById(R.id.horribleImg);
        horribleLL = findViewById(R.id.horribleLL);
        commentEt = findViewById(R.id.commentEt);
        submit = findViewById(R.id.submit);

        star2LL = findViewById(R.id.star2LL);
        star3LL = findViewById(R.id.star3LL);
        star4LL = findViewById(R.id.star4LL);
        star5LL = findViewById(R.id.star5LL);

        statImg2 = findViewById(R.id.statImg2);
        statImg3 = findViewById(R.id.statImg3);
        statImg4 = findViewById(R.id.statImg4);
        statImg5 = findViewById(R.id.statImg5);

        if (getIntent().hasExtra("product_id")) {
            product_id = getIntent().getStringExtra("product_id");
            Picasso.with(RateActivity.this).load(InterfaceClass.imgPth2+product_id+"_"+1+"_thumb.jpg").into(img);
            productTv.setText(getIntent().getStringExtra("productName"));
        }

        if (getIntent().hasExtra("count")) {
            count = getIntent().getStringExtra("count");
            if (count.equals("1"))
                horribleImg.setColorFilter(ContextCompat.getColor(RateActivity.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
            else if (count.equals("2")) {
                horribleImg.setColorFilter(ContextCompat.getColor(RateActivity.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg2.setColorFilter(ContextCompat.getColor(RateActivity.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
            } else if (count.equals("3")) {
                horribleImg.setColorFilter(ContextCompat.getColor(RateActivity.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg2.setColorFilter(ContextCompat.getColor(RateActivity.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg3.setColorFilter(ContextCompat.getColor(RateActivity.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
            } else if (count.equals("4")) {
                horribleImg.setColorFilter(ContextCompat.getColor(RateActivity.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg2.setColorFilter(ContextCompat.getColor(RateActivity.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg3.setColorFilter(ContextCompat.getColor(RateActivity.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg4.setColorFilter(ContextCompat.getColor(RateActivity.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
            } else if (count.equals("5")) {
                horribleImg.setColorFilter(ContextCompat.getColor(RateActivity.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg2.setColorFilter(ContextCompat.getColor(RateActivity.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg3.setColorFilter(ContextCompat.getColor(RateActivity.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg4.setColorFilter(ContextCompat.getColor(RateActivity.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg5.setColorFilter(ContextCompat.getColor(RateActivity.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
            }
        }


        findViewById(R.id.ll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RateActivity.this, RateActivity2.class);
                intent.putExtra("product_id",product_id);
                startActivity(intent);
                finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit.setTextColor(getResources().getColor(R.color.blueLight));
                hitApi();
            }
        });
    }

    void hitApi() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("1", product_id);
            jsonObject.put("2", "1");
            jsonObject.put("3", count);
            jsonObject.put("4", commentEt.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new AbstrctClss(RateActivity.this, ConstntApi.insert_rating(RateActivity.this, jsonObject), "g", true) {
            @Override
            public void responce(String s) {
                JSONObject jsonArray = null;
                try {
                    jsonArray = new JSONObject(s);
                    Commonhelper.showToastLong(RateActivity.this, jsonArray.optString("message"));
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };

    }
}
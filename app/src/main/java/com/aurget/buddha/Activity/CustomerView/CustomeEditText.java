package com.aurget.buddha.Activity.CustomerView;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;

public class CustomeEditText  extends androidx.appcompat.widget.AppCompatEditText {

    private static int color= Color.RED;

    public CustomeEditText(Context context)
    {
        super(context);
        setFont();
        //this.setTextColor(color);
    }

    public CustomeEditText(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setFont();
        //this.setTextColor(color);
    }

    public CustomeEditText(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        setFont();
        //this.setTextColor(color);
    }

    public static void setGlobalColor(int gcolor)
    {
        color=gcolor;

    }

    private void setFont() {

        /*Typeface typeface = Typeface.createFromAsset(getContext().getAssets(),font);
        setTypeface(typeface); //function used to set font*/

    }
}

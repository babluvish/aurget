package com.aurget.buddha.Activity.categories;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.Activity.Main2Activity;
import com.aurget.buddha.Activity.MyProfile.Shipping_ddress_list_Activity;
import com.aurget.buddha.Activity.RajorPayment.CheckoutActivity;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.Utils.ConstntApi;

import com.aurget.buddha.Activity.Utils.CustomPreference;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.aurget.buddha.Activity.Commonhelper.discountAmt;
import static com.aurget.buddha.Activity.Commonhelper.roudOff;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.getInteger;
import static java.lang.Integer.parseInt;

public class BuyNow extends AppCompatActivity {

    TextView titleTv, buyNowTv2, subTOtTv, shippingchrgTv, totlamountTv, wlletTv, plusTv, priceBottomTv, ipBottom;
    TextView priceTv, addressTv;
    TextView priceTv2, priceTv_, priceTv2_, percentOffTv_;
    TextView percentOffTv, totalTv;
    TextView ipTv, totlIPTv;
    TextView productNmeTv, productNmeTv2;
    TextView soldTv, wallet_BalanceTv;
    TextView rtingTv, sizeTv, qtyTv;
    TextView minusTv, saveWallet;
    LinearLayout pymentMethodLl, shippingLL;
    String paymentMethod = "", walletAmt = "", size = "", color = "";
    EditText qtyEt;
    CardView shippingAddressCv;
    ImageView imageView1, imageView2, colorImg;

    Activity activity = this;
    Switch wlletSwitch;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_now);

        titleTv = findViewById(R.id.title);
        pymentMethodLl = findViewById(R.id.pymentMethodLl);
        shippingLL = findViewById(R.id.shippingLL);
        buyNowTv2 = findViewById(R.id.buyNowTv2);
        shippingAddressCv = findViewById(R.id.shippingAddressCv);
        plusTv = findViewById(R.id.plusTv);
        minusTv = findViewById(R.id.minusTv);
        saveWallet = findViewById(R.id.saveWallet);
        productNmeTv2 = findViewById(R.id.productNmeTv2);
        ipBottom = findViewById(R.id.ipBottom);
        qtyTv = findViewById(R.id.qtyTv);
        addressTv = findViewById(R.id.addressTv);

        rtingTv = findViewById(R.id.rtingTv);
        sizeTv = findViewById(R.id.sizeTv);
        priceTv = findViewById(R.id.priceTv);
        priceTv_ = findViewById(R.id.priceTv_);
        priceTv2 = findViewById(R.id.priceTv2);
        priceTv2_ = findViewById(R.id.priceTv2_);
        priceTv2.setPaintFlags(priceTv2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        priceTv2_.setPaintFlags(priceTv2_.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        percentOffTv = findViewById(R.id.percentOffTv);
        percentOffTv_ = findViewById(R.id.percentOffTv_);
        ipTv = findViewById(R.id.ipTv);
        productNmeTv = findViewById(R.id.productNmeTv);
        subTOtTv = findViewById(R.id.subTOtTv);
        soldTv = findViewById(R.id.soldTv);
        wlletTv = findViewById(R.id.wlletTv);
        qtyEt = findViewById(R.id.qtyEt);
        imageView1 = findViewById(R.id.img);
        imageView2 = findViewById(R.id.img2);
        colorImg = findViewById(R.id.colorImg);

        subTOtTv = findViewById(R.id.subTOtTv);
        shippingchrgTv = findViewById(R.id.shippingchrgTv);
        totlamountTv = findViewById(R.id.totlamountTv);
        totalTv = findViewById(R.id.totalTv);
        wallet_BalanceTv = findViewById(R.id.Wallet_BalanceTv);
        wlletSwitch = findViewById(R.id.wlletSwitch);
        priceBottomTv = findViewById(R.id.priceBottomTv);
        totlIPTv = findViewById(R.id.totlIPTv);

        titleTv.setText("Buy Now");

        percentOffTv.setTextColor(getResources().getColor(R.color.greencol));
        percentOffTv_.setTextColor(getResources().getColor(R.color.greencol));

        Picasso.with(activity).load(getIntent().getStringExtra("img")).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(imageView1);
        Picasso.with(activity).load(getIntent().getStringExtra("img")).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(imageView2);

        product_id = getIntent().getStringExtra("product_id");
        size = getIntent().getStringExtra("size");
        color = getIntent().getStringExtra("color");

        try {
            if (size.equalsIgnoreCase("")) {
                findViewById(R.id.sizeTv2).setVisibility(View.GONE);
                findViewById(R.id.sizeCv).setVisibility(View.GONE);
            } else {
                sizeTv.setText(size);
            }
        } catch (Exception e) {

        }

        try {
            if (color.trim().equalsIgnoreCase("")) {
                findViewById(R.id.colorCv).setVisibility(View.GONE);
                findViewById(R.id.colorTv).setVisibility(View.GONE);
            } else {
                Picasso.with(activity).load(color).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(colorImg);
            }
        } catch (Exception e) {
            e.getMessage();
        }

        new AbstrctClss(BuyNow.this, ConstntApi.wallet_ballanceUrl(BuyNow.this), "p", true) {
            @Override
            public void responce(String s) {

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(s);
                    walletAmt = jsonArray.optJSONObject(0).optString("Wallet_Balance");
                    //wlletTv.setText("₹" + walletAmt);
                    wallet_BalanceTv.setText(walletAmt);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };

        detils();

        wlletSwitch.setChecked(false);
        wlletSwitch.setOnCheckedChangeListener((compoundButton, b) -> {

            if (parseDouble(walletAmt) < 1) {
                wlletSwitch.setChecked(false);
                Commonhelper.showToastLong(BuyNow.this, Constnt.insufficientwalletbalance);
                return;
            }

            if (parseDouble(totlamountTv.getText().toString()) < 499 && wlletSwitch.isChecked()) {
                wlletSwitch.setChecked(false);
                Commonhelper.showToastLong(BuyNow.this, Constnt.pleaseorderminimum₹499forusecashback);
                return;
            }

            if (b) {
                if (parseDouble(walletAmt) > parseDouble(totlamountTv.getText().toString())) {
                    totalAmt = Double.parseDouble(totlamountTv.getText().toString());
                    totlamountTv.setText("0.00");
                    totalTv.setText("₹0.00");
                    wlletTv.setText("₹" + Commonhelper.roundOff_(Double.valueOf(totalAmt)));
                    double wallet3 = parseDouble(walletAmt) - totalAmt;
                    deductAmt = totalAmt;
                    wallet_BalanceTv.setText("" + Commonhelper.roundOff_(Double.valueOf(wallet3)));
                    saveWallet.setText("You will save ₹" + totalAmt + " using ₹+" + totalAmt + " Wallet/Cashback");
                } else {
                    totalAmt = Double.parseDouble(totlamountTv.getText().toString());
                    double totAmot = totalAmt - parseDouble(walletAmt);
                    deductAmt = Double.parseDouble(walletAmt);
                    totlamountTv.setText("" + Commonhelper.roundOff_(totAmot));
                    totalTv.setText("₹" + Commonhelper.roundOff_(totAmot));
                    wlletTv.setText("₹" + Commonhelper.roundOff_(Double.valueOf(walletAmt)));
                    wallet_BalanceTv.setText("0.00");
                    saveWallet.setText("You will save ₹" + walletAmt + " using ₹" + walletAmt + " Wallet/Cashback");
                }
            } else {
                saveWallet.setText("You will save ₹0 using ₹0 Wallet/Cashback");
                wallet_BalanceTv.setText("" + Commonhelper.roundOff_(Double.valueOf(walletAmt)));
                wlletTv.setText("₹0.00");
                deductAmt = 0.0;
                double totamt = calcTotAmt(String.valueOf(countPrice));
                totlamountTv.setText("" + totamt);
                totalTv.setText("₹" + totamt);
            }
        });

        buyNowTv2.setOnClickListener(view -> {

            if (address_id.equals("")) {
                Commonhelper.showToastLong(BuyNow.this, getString(R.string.select_shipping_address));
                return;
            }

            if (parseDouble(totlamountTv.getText().toString().trim()) < 1) {
                final Dialog dialog = Commonhelper.loadDialog(BuyNow.this);
                new Handler().postDelayed(() -> {
                    insert_invoice_details(product_id);
                    Commonhelper.dismiss();
                    dialog.dismiss();
                }, 2000);

                return;
            }

            final Dialog dialog = new Dialog(BuyNow.this);
            dialog.setContentView(R.layout.pyment_method);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.setCanceledOnTouchOutside(false);
            final RadioButton payOnlineRbn = dialog.findViewById(R.id.payOnlineRbn);
            final RadioButton codRbn = dialog.findViewById(R.id.codRbn);
            final RadioButton walletRbn = dialog.findViewById(R.id.walletRbn);
            final Button doneBtn = dialog.findViewById(R.id.doneBtn);
            final TextView Wallet_BalanceTv = dialog.findViewById(R.id.Wallet_BalanceTv);
            final TextView payTv = dialog.findViewById(R.id.payTv);
            final TextView cashDelCharge = dialog.findViewById(R.id.cashDelCharge);
            Wallet_BalanceTv.setText(wlletTv.getText().toString());

            if (parseDouble(cashDelChrg) > 0) {
                //shippingLL.setVisibility(View.GONE);
                shippingchrgTv.setText("₹" + cashDelChrg);
                shippingchrgTv.setTextColor(getResources().getColor(R.color.blackCol));
            } else {
                shippingLL.setVisibility(View.VISIBLE);
                shippingchrgTv.setText("Free");
                shippingchrgTv.setTextColor(getResources().getColor(R.color.greencol));
            }
            if (cod_charge > 0) {
                cashDelCharge.setText("" + cod_charge);
                cashDelCharge.setTextColor(getResources().getColor(R.color.blackCol));
            } else {
                cashDelCharge.setText("Free");
                cashDelCharge.setTextColor(getResources().getColor(R.color.greencol));
            }

            payOnlineRbn.setOnCheckedChangeListener((compoundButton, b) -> {
                if (b) {
                    codRbn.setChecked(false);
                    payOnlineRbn.setChecked(true);
                    walletRbn.setChecked(false);
                    paymentMethod = "card";
                    payTv.setText("" + totlamountTv.getText().toString());
                    shippingLL.setVisibility(View.VISIBLE);
                    /*shippingchrgTv.setText("Free");
                    shippingchrgTv.setTextColor(getResources().getColor(R.color.greencol));*/
                }
            });

            paymentMethod = "";

            codRbn.setOnCheckedChangeListener((compoundButton, b) -> {
                if (b) {
                    payOnlineRbn.setChecked(false);
                    codRbn.setChecked(true);
                    walletRbn.setChecked(false);
                    paymentMethod = "cod";
                    shippingLL.setVisibility(View.VISIBLE);

                    if (parseDouble(cashDelChrg) > 0) {
                        shippingchrgTv.setText("₹" + cashDelChrg);
                        shippingchrgTv.setTextColor(getResources().getColor(R.color.blackCol));
                    } else {
                        shippingchrgTv.setText("Free");
                        shippingchrgTv.setTextColor(getResources().getColor(R.color.greencol));
                    }

                    Double totamt = parseDouble(totlamountTv.getText().toString()) + cod_charge;
                    payTv.setText("" + totamt);
                }
            });

            walletRbn.setOnCheckedChangeListener((compoundButton, b) -> {
                if (b) {
                    payOnlineRbn.setChecked(false);
                    codRbn.setChecked(false);
                    walletRbn.setChecked(true);
                    paymentMethod = "wallet";
                    shippingLL.setVisibility(View.GONE);

                    double totaAmout = parseDouble(totlamountTv.getText().toString()) - parseDouble(walletAmt);

                    payTv.setText("" + totaAmout);
                }
            });

            doneBtn.setOnClickListener(view1 -> {
                if (paymentMethod.equals("")) {
                    Commonhelper.showToastLong(BuyNow.this, getString(R.string.selectPaymentType));
                    return;
                }
                if (paymentMethod.equals("cod") || parseDouble(payTv.getText().toString().trim()) < 1) {
                    final Dialog dialog1 = Commonhelper.loadDialog(BuyNow.this);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            if (address_id.equals("")) {
                                Commonhelper.showToastLong(BuyNow.this, "Select Address");
                                dialog1.dismiss();
                            } else {
                                insert_invoice_details(product_id);

                                //Commonhelper.showToastLong(BuyNow.this,"Payment has been successfully");
                                Commonhelper.dismiss();
                                dialog1.dismiss();

                               /* Intent intent = new Intent(BuyNow.this, Main2Activity.class);
                                startActivity(intent);
                                ActivityCompat.finishAffinity(BuyNow.this);*/
                            }
                        }
                    }, 2000);

                    return;
                }

                Intent intent = new Intent(BuyNow.this, CheckoutActivity.class);
                intent.putExtra("BuyNow", "true");
                intent.putExtra("totlamount", payTv.getText().toString());
                startActivityForResult(intent, 100);
                dialog.dismiss();
            });

            payTv.setText("" + totlamountTv.getText().toString());
            dialog.show();

        });

        shippingAddressCv.setOnClickListener(view -> {
            Intent intent = new Intent(BuyNow.this, Shipping_ddress_list_Activity.class);
            intent.putExtra("SubCategoryDetails2", "true");
            startActivityForResult(intent, 101);
        });
//
//        minusTv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(BuyNow.this, Shipping_ddress_list_Activity.class);
//                intent.putExtra("SubCategoryDetails2", "true");
//                startActivityForResult(intent, 101);
//            }
//        });

        plusTv.setOnClickListener(view -> incrementDecrementQty("+"));

        minusTv.setOnClickListener(view -> incrementDecrementQty("-"));

        qtyEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                totlamountTv.setText("" + calcTotAmt(qtyEt.getText().toString()));
            }
        });

        findViewById(R.id.bkImg).setOnClickListener(v -> finish());

        findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("img", getIntent().getStringExtra("img"));
                    jsonObject.put("name", productNme);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //Commonhelper.shareAndEarn(BuyNow.this, 2,jsonObject.toString());
            }
        });

    }


    private void incrementDecrementQty(String what) {
        if (what.equals("-")) {
            if (qtyEt.getText().toString().equals("1"))
                return;
            countPrice = countPrice - 1;
        } else {
            countPrice = countPrice + 1;
        }

        if (String.valueOf(countPrice).contains("-")) {
            Commonhelper.showToastLong(BuyNow.this, getResources().getString(R.string.invalidQty));
            return;
        }

        qtyEt.setText("" + countPrice);
        ipBottom.setText("IP:" + calcTotIP(String.valueOf(countPrice)));
        totlIPTv.setText("IP:" + calcTotIP(String.valueOf(countPrice)));
        qtyTv.setText("Quantity: " + qtyEt.getText().toString());
        subTOtTv.setText("₹" + price);


        double totamt = 0.0;

        if (wlletSwitch.isChecked()) {
            totamt = calcTotAmt(String.valueOf(countPrice)) - parseDouble(walletAmt);
        } else {
            totamt = calcTotAmt(String.valueOf(countPrice));
        }

        totalTv.setText("₹" + totamt);
        totlamountTv.setText("" + totamt);

    }

    private Double calcTotAmt(String qty) {
        Double aDouble = 0.0;
        try {
            aDouble = parseDouble(qty) * totPrice;
            String ouble = Commonhelper.roundOff(aDouble);
            return Double.parseDouble(ouble);
        } catch (NumberFormatException e) {
            return 0.0;
        }
    }

    private Double calcTotIP(String qty) {
        Double aDouble = 0.0;
        try {
            String ouble;
            if (parseDouble(qty) > 1) {
                aDouble = parseDouble(qty) * ip;
                ouble = Commonhelper.roundOff(aDouble);
            } else {
                ouble = String.valueOf(ip);
            }
            return Double.parseDouble(ouble);
        } catch (NumberFormatException e) {
            return 0.0;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //String name = CustomPreference.readString(BuyNow.this, CustomPreference.shippingName, "");
        String address = CustomPreference.readString(BuyNow.this, CustomPreference.shippingAddress, "");
        address_id = CustomPreference.readString(BuyNow.this, CustomPreference.getShippingAddressID, "");

        if (!address_id.equals(""))
            addressTv.setText(address);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            Log.i("onActivityResult", data.getStringExtra("res"));
            String res = data.getStringExtra("res");
            if (res.equals("success")) {
                insert_invoice_details(product_id);
            } else {
                try {
                    JSONObject jsonObject = new JSONObject(res);
                    Commonhelper.showToastLong(BuyNow.this, jsonObject.optJSONObject("error").optString("description"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    Commonhelper.showToastLong(BuyNow.this, "Payment cancelled");
                }
            }
        } else if (requestCode == 101) {
            /*try {
                if (data.hasExtra("address_id")) {
                    address_id = data.getStringExtra("address_id");
                    addressTv.setText(data.getStringExtra("name") + "\n" + data.getStringExtra("address")
                            + "\n" + data.getStringExtra("city") + " " + data.getStringExtra("pin_code"));
                }
            } catch (Exception e) {

            }*/
        }
    }

    private void detils() {
        ArrayList<String> strings = new ArrayList<>();
        strings.add(product_id);

        new AbstrctClss(BuyNow.this, ConstntApi.product_single_buy(String.valueOf(strings)), "p", true) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    cod_charge = Double.parseDouble(jsonObject.optString("cod_charge"));

                    double sp = parseDouble(jsonObject.optString("sale_price"));
                    double dis = parseDouble(jsonObject.optString("discount"));
                    double camt = parseDouble(jsonObject.optString("cut_amount"));
                    double gt = discountAmt(sp, dis);

                    long amt = roudOff(gt);

                    price = jsonObject.optString("sale_price");

                    priceTv.setText("₹" + price);
                    priceTv_.setText("₹" + price);

                    priceTv2.setText("₹" + jsonObject.optString("cut_amount"));
                    ipTv.setText("" + jsonObject.optString("ip"));
                    ipBottom.setText("IP: " + jsonObject.optString("ip"));
                    ip = Double.parseDouble(jsonObject.optString("ip"));
                    totlIPTv.setText("IP:" + ip);
                    //productNmeTv.setText("Product Name: " + jsonObject.optString("title"));
                    productNmeTv.setText("" + jsonObject.optString("title"));
                    productNmeTv2.setText("" + jsonObject.optString("title"));
                    productNme = jsonObject.optString("title");


                    cashDelChrg = jsonObject.optString("shipping_cost");

                    shippingLL.setVisibility(View.VISIBLE);

                    if (parseDouble(cashDelChrg) > 0) {
                        shippingchrgTv.setText("₹" + cashDelChrg);
                        totPrice = parseDouble(cashDelChrg) + parseDouble(jsonObject.optString("sale_price"));
                        totlamountTv.setText("" + totPrice);
                        totalTv.setText("₹" + totPrice);
                    } else {
                        totPrice = parseDouble(jsonObject.optString("sale_price"));
                        totlamountTv.setText("" + totPrice);
                        totalTv.setText("₹" + totPrice);
                        shippingchrgTv.setText("Free");
                        shippingchrgTv.setTextColor(getResources().getColor(R.color.greencol));
                    }

                    subTOtTv.setText("₹" + jsonObject.optString("sale_price"));


                    percentOffTv.setText("" + jsonObject.optString("discount") + "% Off");
                    percentOffTv_.setText("" + jsonObject.optString("discount") + "% Off");

                    if (parseInt(jsonObject.optString("discount")) > 0) {
                        percentOffTv.setText("₹" + jsonObject.optString("discount") + "% Off");
                        percentOffTv_.setText("₹" + jsonObject.optString("discount") + "% Off");
                        priceTv2.setText("₹" + camt);
                        priceTv2_.setText("₹" + camt);
                        priceTv.setText("₹" + sp);
                        priceTv_.setText("₹" + sp);

                        priceBottomTv.setText(priceTv.getText().toString() + " " + priceTv2.getText().toString() + " " + percentOffTv.getText().toString());

                    } else {
                        priceTv.setText("₹" + sp);
                        priceTv_.setText("₹" + sp);
                        priceBottomTv.setText(priceTv.getText().toString());
                        percentOffTv.setVisibility(View.GONE);
                        percentOffTv_.setVisibility(View.GONE);
                        priceTv2.setVisibility(View.GONE);
                        priceTv2_.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }

    private void insert_invoice_details(String productID) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("color", color);
            jsonObject.put("size", size);
            jsonObject.put("product_id", productID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String loginId = CustomPreference.readString(activity, CustomPreference.e_id, "");

        String res = InterfaceClass.ipAddress4 + "insert_invoice_details?product_details=" + jsonObject.toString() + "&shipping_address=" + address_id + "&tax=1&per_tax=1" +
                "&grand_total=" + totlamountTv.getText().toString() + "&login_id=" + loginId + "&payment_type=" + paymentMethod + "&product_id=" + productID + "&deductAmt=" + deductAmt
                + "&wallet_amount=" + wlletTv.getText().toString().replace("₹", "");
        Log.d("resss++", res);

        final Dialog dialog = Commonhelper.loadDialog(BuyNow.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, res,
                response -> {
                    try {
                        String Res = response;
                        Log.d("dsdss", Res);
                        //JSONArray jsonArray = new JSONArray(Res);
                        //String status = jsonArray.optJSONObject(0).optString("Status");
                        if (response.contains("control")) {
                            Commonhelper.showToastLong(BuyNow.this, getResources().getString(R.string.order_submitteed));
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent intent = new Intent(BuyNow.this, Main2Activity.class);
                                    startActivity(intent);
                                    ActivityCompat.finishAffinity(BuyNow.this);
                                }
                            }, 1000);
                            //paymentContent("success");
                            //paymentSuccessAPI();
                            //sendOtp(CustomPreference.readString(BuyNowCart.this, CustomPreference.mobileNO, ""));
                        } else {
                            Commonhelper.showToastLong(BuyNow.this, "Failed");
                            //paymentContent("fail");
                            //paymentSuccessAPI();
                            //sendOtp(CustomPreference.readString(BuyNowCart.this, CustomPreference.mobileNO, ""));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("dsd", "sssss");
                    }
                    dialog.dismiss();
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("onErrorResponse", "hitPi2");
                        dialog.dismiss();
                    }
                }) {
//            @Override
//            protected Map getParams()
//            {
//                Map params = new HashMap();
//                params.put("username", "");
//                params.put("email", "abc@androidhive.info");
//                params.put("password", "password123");
//
//                return params;
//            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(BuyNow.this);
        requestQueue.add(stringRequest);

    }

    private void paymentSuccessAPI() {

        String login_id = CustomPreference.readString(BuyNow.this, CustomPreference.e_id, "");

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("product_id", product_id);
            jsonObject.put("shiping_address_id", address_id);
            jsonObject.put("quantity", qtyEt.getText().toString());
            jsonObject.put("payment_type", paymentMethod);
            jsonObject.put("amount", totlamountTv.getText().toString());
            jsonObject.put("ip", ipTv.getText().toString());
            jsonObject.put("login_id", login_id);
            jsonObject.put("size", sizeTv.getText().toString());
            jsonObject.put("color", color);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //http://aurget.com/app/sale_details.php?sale_code=1&buyer=1&product_details=1&shipping_address=1&gst=1&gst_per=1&shipping_charge=10&payment_type=razerpay&payment_status=1&payment_details=135353535&grand_total=120&sale_datetime=1/1/2020&delivary_datetime=1/1/2020&delivery_status=pending

        new AbstrctClss(BuyNow.this, ConstntApi.paymentSuccessAPI(jsonObject), "p", true) {
            @Override
            public void responce(String s) {

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(s);

                    if (jsonArray.optJSONObject(0).optString("Status").equalsIgnoreCase("true")) {
                        //Commonhelper.showToastLong(BuyNow.this, jsonArray.optJSONObject(0).optString("Message"));

                        Commonhelper.showToastLong(BuyNow.this, getResources().getString(R.string.order_submitteed));

                        new Handler().postDelayed(() -> {
                            Intent intent = new Intent(BuyNow.this, Main2Activity.class);
                            startActivity(intent);
                            ActivityCompat.finishAffinity(BuyNow.this);
                        }, 3000);

                    } else {
                        Commonhelper.showToastLong(BuyNow.this, jsonArray.optJSONObject(0).optString("Message"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private String address_id = "";
    String product_id = "";
    private String price = "";
    long countPrice = 1;
    double totalAmt = 0, deductAmt = 0.0;
    double ip = 0.0, totPrice = 0.0;
    private double cod_charge = 0.0;
    String cashDelChrg, productNme;
}
//9c:6b:72:08:e5:3b
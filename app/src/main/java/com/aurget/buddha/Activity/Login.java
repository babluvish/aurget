package com.aurget.buddha.Activity;

import android.Manifest;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;

import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.aurget.buddha.Activity.SolderManagment.AddSolderActivity;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.Utils.CustomPreference;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import static com.aurget.buddha.Activity.Commonhelper.phoneCall;
import static com.aurget.buddha.Activity.Utils.Constnt.invaildOTP;
import static com.aurget.buddha.Activity.Utils.Constnt.notaregisterednumber;

public class Login extends BaseActivity {

    AppCompatButton venderSignUPBtn;
    TextView customerSignUPBtn;
    AppCompatButton loginTv, loginBtn;
    CheckBox chk;
    EditText pwd, otp1, otp2, otp3, otp4;
    EditText mobEt, mobilNoEt;
    TextView forgtPwdTv, otpTv;

    LinearLayout otpLl;
    SharedPreferences Shpref;
    SharedPreferences.Editor editShpref;
    String randomNO = "";
    private ClipboardManager clipboardManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        clipboardManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);

        venderSignUPBtn = findViewById(R.id.venderSignUPBtn);
        customerSignUPBtn = findViewById(R.id.customerSignUPBtn);
        loginTv = findViewById(R.id.loginTv);
        chk = findViewById(R.id.chk);
        pwd = findViewById(R.id.pwd);
        mobEt = findViewById(R.id.mobEt);
        otpLl = findViewById(R.id.otpLl);

        forgtPwdTv = findViewById(R.id.forgtPwdTv);

        //mobEt.setText("09662101102");
        //mobEt.setText("9235720166");


        loginTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mobEt.getText().toString().length() == 0) {
                    Toast.makeText(Login.this, "Enter Registered Mobile No.", Toast.LENGTH_LONG).show();
                    return;
                }
//                if (mobEt.getText().toString().length()==0 && pwd.getText().toString().length()==0)
//                {
//                    Toast.makeText(Login.this, "Invalid Login Credentials", Toast.LENGTH_LONG).show();
//                    return;
//                }

//                if (pwd.getText().toString().length()<4)
//                {
//                    Toast.makeText(Login.this, "At least 4 digits password are required.", Toast.LENGTH_LONG).show();
//                    return;
//                }

                sendOtp(mobEt.getText().toString());
                //new GetWeatherTask().execute(ConstntApi.send_otp_login(Login.this,mobEt.getText().toString()));

                //hitPI(mobEt.getText().toString().trim(),pwd.getText().toString().trim());
            }
        });

        otpLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dilog(2);
            }
        });

        forgtPwdTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dilog(1);
            }
        });

        venderSignUPBtn.setOnClickListener(view -> {
            Intent intent = new Intent(Login.this, AddSolderActivity.class);
            startActivity(intent);
        });

        findViewById(R.id.customerSignUPRl).setOnClickListener(view -> onCallBtnClick());

        chk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (chk.isChecked()) {
                    pwd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    pwd.setSelection(pwd.length());
                } else {
                    pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });
    }

    private void onCallBtnClick() {
        if (Build.VERSION.SDK_INT < 23) {
            phoneCall(Login.this);
        } else {
            if (ActivityCompat.checkSelfPermission(Login.this,
                    Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                phoneCall(Login.this);
            } else {
                final String[] PERMISSIONS_STORAGE = {Manifest.permission.CALL_PHONE};
                //Asking request Permissions
                ActivityCompat.requestPermissions(Login.this, PERMISSIONS_STORAGE, 9);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        boolean permissionGranted = false;
        switch (requestCode) {
            case 9:
                permissionGranted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (permissionGranted) {
            phoneCall(Login.this);
        } else {
            Toast.makeText(Login.this, "You don't assign permission.", Toast.LENGTH_SHORT).show();
        }
    }

    void dilog(int i) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.login2);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mobilNoEt = dialog.findViewById(R.id.mobilNoEt);
        final TextView resendOtp = dialog.findViewById(R.id.resendOtp);

        //mobilNoEt.setVisibility(View.VISIBLE);
        otp1 = dialog.findViewById(R.id.otp1);
        otp2 = dialog.findViewById(R.id.otp2);
        otp3 = dialog.findViewById(R.id.otp3);
        otp4 = dialog.findViewById(R.id.otp4);

        otpLl = dialog.findViewById(R.id.otpLl);
        loginBtn = dialog.findViewById(R.id.loginBtn);

        Commonhelper.countDown(resendOtp);
        //loginBtn.setText("Get OTP");

        resendOtp.setOnClickListener(view -> {

            if (resendOtp.getText().toString().equals("Resend OTP"))
                sendOtp(mobEt.getText().toString());
            //new GetWeatherTask().execute(ConstntApi.send_otp_login(Login.this,mobEt.getText().toString()));
            //otpLogin(mobEt.getText().toString(),mobilNoEt.getText().toString());
        });

        dialog.findViewById(R.id.closeLl).setOnClickListener(view -> {

            dialog.dismiss();
            //otpLogin(mobEt.getText().toString(),mobilNoEt.getText().toString());
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String otp = otp1.getText().toString() + otp2.getText().toString() + otp3.getText().toString() + otp4.getText().toString();

                //Commonhelper.showToastLong(Login.this, otp);

                if (otp.length() < 4) {
                    Commonhelper.showToastLong(Login.this, "Enter OTP");
                    return;
                }

            /*    if (mobilNoEt.getText().toString().equals(randomNO))
                {

                }*/

                otpLogin(mobEt.getText().toString(), otp);

               /* if (loginBtn.getText().toString().equals(getResources().getString(R.string.verifyOtp)))
                {
                    otpLogin(mobilNoEt.getText().toString(),otp1.getText().toString()+otp2.getText().toString()+otp3.getText().toString()+otp4.getText().toString());
                    Log.i("swe","1");
                }
                else {
                    sendOtp(mobilNoEt.getText().toString());
                    Log.i("swe","2");
                }*/
//                otpLl.setVisibility(View.VISIBLE);
//                mobilNoEt.setVisibility(View.GONE);
//                loginBtn.setText("Verify OTP");
            }
        });

        /*otp2.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == 67) {
                otp2.setText("");
                otp1.requestFocus();
            }
            return false;
        });

        otp3.setOnKeyListener((v, keyCode, event) ->
        {
            if (keyCode == 67) {
                otp3.setText("");
                otp2.requestFocus();
            }
            return false;
        });

        otp4.setOnKeyListener((v, keyCode, event) ->
        {
            if (keyCode == 67) {
                otp4.setText("");
                otp3.requestFocus();
            }
            return false;
        });
*/
        otp1.addTextChangedListener(new

                                            TextWatcher() {
                                                @Override
                                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                                    // TODO Auto-generated method stub
                                                }

                                                @Override
                                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                                    // TODO Auto-generated method stub
                                                    //Commonhelper.showToastLong(Login.this,"2");
                                                }

                                                @Override
                                                public void afterTextChanged(Editable s) {
//                                                    otp1.removeTextChangedListener(this);
//                                                    String str  = otp1.getText().toString();
//                                                    str  = str.substring(str.length()-1);
//                                                    otp1.setText(str);
                                                    if (otp1.getText().length() == 1)
                                                        otp2.requestFocus();

                                                }
                                            });

        otp2.addTextChangedListener(new

                                            TextWatcher() {
                                                @Override
                                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                                    // TODO Auto-generated method stub
                                                }

                                                @Override
                                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                                    // TODO Auto-generated method stub
                                                }

                                                @Override
                                                public void afterTextChanged(Editable s) {

                                                    // TODO Auto-generated method stub

                                                    if (otp2.getText().length() == 1)
                                                        otp3.requestFocus();
                                                }
                                            });
        otp3.addTextChangedListener(new

                                            TextWatcher() {
                                                @Override
                                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                                    // TODO Auto-generated method stub
                                                }

                                                @Override
                                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                                    // TODO Auto-generated method stub
                                                }

                                                @Override
                                                public void afterTextChanged(Editable s) {

                                                    // TODO Auto-generated method stub

                                                    if (otp3.getText().length() == 1)
                                                        otp4.requestFocus();
                                                }
                                            });

        otp4.addTextChangedListener(new

                                            TextWatcher() {
                                                @Override
                                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                                    // TODO Auto-generated method stub
                                                }

                                                @Override
                                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                                    // TODO Auto-generated method stub
                                                }

                                                @Override
                                                public void afterTextChanged(Editable s) {

                                                    // TODO Auto-generated method stub

                                                }
                                            });

        dialog.show();
        clipboardManager.setText("");
        startTimer();
    }

    void hitPI(final String mob, final String pwd) {
        final Dialog dialog = Commonhelper.loadDialog(Login.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://aurget.com/Api/api_login?mobile=" + mob + "&password=" + pwd + "",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String Res = response;
                            JSONArray jsonArray = new JSONArray(Res);
                            if (jsonArray.optJSONObject(0).optString("Status").equalsIgnoreCase("true")) {
                                Log.d("dsd", Res);
                                Toast.makeText(Login.this, "Login Successfully", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(Login.this, Main2Activity.class);
                                startActivity(intent);

                                CustomPreference.writeString(Login.this, CustomPreference.mlm_reg_id, jsonArray.optJSONObject(0).optString("mlm_reg_id"));
                                CustomPreference.writeString(Login.this, CustomPreference.e_id, jsonArray.optJSONObject(0).optString("e_id"));
                                CustomPreference.writeString(Login.this, CustomPreference.mobileNO, mobEt.getText().toString().trim());

                                finish();
                            } else {
                                Toast.makeText(Login.this, "Invalid Login Credentials", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("dsd", "sssss");
                        }
                        dialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Log.i("reer",error.getMessage());
                        finish();
                    }
                }) {
//            @Override
//            protected Map getParams()
//            {
//                Map params = new HashMap();
//                params.put("mobile", mob);
//                params.put("password", pwd);
//                //params.put("mobile", "9768178608");
//                //params.put("password", "123");
//                return params;
//            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(Login.this);
        requestQueue.add(stringRequest);
    }

    private void dashboardApi()
    {
        new AbstrctClss(mActivity, ConstntApi.DashboardApi(mActivity), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    JSONObject jsonArray = new JSONObject(s);
                    JSONArray product_list_latest  = jsonArray.optJSONArray("product_list_latest");
                    JSONArray product_list_set_most_view  = jsonArray.optJSONArray("product_list_set_most_view");
                    JSONArray product_list_set_recent = jsonArray.optJSONArray("product_list_set_recent");

                    JSONArray get_banners = jsonArray.optJSONArray("get_banners");
                    JSONArray allcategory = jsonArray.optJSONArray("allcategory");
                    JSONArray deal_product = jsonArray.optJSONArray("deal_product");
                    JSONArray product_list_set = jsonArray.optJSONArray("product_list_set");
                    JSONArray product_list_set_recent_all = jsonArray.optJSONArray("product_list_set_recent_all");

                    Constnt.product_list_latest.clear();
                    Constnt.deal_product.clear();
                    Constnt.product_list_set_most_view.clear();
                    Constnt.recentViewJAl.clear();
                    Constnt.get_banners.clear();
                    Constnt.all_category.clear();
                    Constnt.product_list_set.clear();
                    Constnt.product_list_set_recent_all.clear();
                    Constnt.get_sliders=null;
                    Constnt.get_profile=null;

                    Constnt.get_profile  = jsonArray.optJSONArray("get_profile");
                    Constnt.get_sliders = jsonArray.optJSONArray("get_sliders");

                    for (int i = 0; i < product_list_latest.length(); i++) {
                        Constnt.product_list_latest.add(product_list_latest.getJSONObject(i));
                    }

                    for (int i = 0; i < product_list_set_most_view.length(); i++) {
                        Constnt.product_list_set_most_view.add(product_list_set_most_view.getJSONObject(i));
                    }

                    for (int i = 0; i < product_list_set_recent.length(); i++) {
                        Constnt.recentViewJAl.add(product_list_set_recent.getJSONObject(i));
                    }
                    for (int i = 0; i < get_banners.length(); i++) {
                        Constnt.get_banners.add(get_banners.getJSONObject(i));
                    }

                    for (int i = 0; i < allcategory.length(); i++) {
                        Constnt.all_category.add(allcategory.getJSONObject(i));
                    }

                    for (int i = 0; i < deal_product.length(); i++) {
                        Constnt.deal_product.add(deal_product.getJSONObject(i));
                    }

                    for (int i = 0; i < product_list_set.length(); i++) {
                        Constnt.product_list_set.add(product_list_set.getJSONObject(i));
                    }

                    for (int i = 0; i < product_list_set_recent_all.length(); i++) {
                        Constnt.product_list_set_recent_all.add(product_list_set_recent_all.getJSONObject(i));
                    }

                    Intent intent = new Intent(Login.this, Main2Activity.class);
                    ActivityCompat.finishAffinity(Login.this);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                Commonhelper.showToastLong(Login.this,"Please check net connection");
            }
        };
    }

    void otpLogin(final String mob, final String otp) {
        final Dialog dialog = Commonhelper.loadDialog(Login.this);
        String url = InterfaceClass.ipAddress4 + "api_login_by_otp?mobile=" + mob + "&otp=" + otp;
        Log.i("url_", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    try {
                        String Res = response;
                        JSONArray jsonArray = new JSONArray(Res);
                        Log.d("dsd", Res);
                        if (jsonArray.optJSONObject(0).has("Status")) {
                            if (jsonArray.optJSONObject(0).optString("Status").equals("true")) {
                                //Intent intent = new Intent(Login.this, Main2Activity.class);
                                CustomPreference.writeString(Login.this, CustomPreference.mlm_reg_id, jsonArray.optJSONObject(0).optString("mlm_reg_id"));
                                CustomPreference.writeString(Login.this, CustomPreference.e_id, jsonArray.optJSONObject(0).optString("e_id"));
                                CustomPreference.writeString(Login.this, CustomPreference.mobileNO, mobEt.getText().toString());
                                CustomPreference.writeString(Login.this, CustomPreference.userName, jsonArray.optJSONObject(0).optString("solder_name"));
                                //ActivityCompat.finishAffinity(Login.this);
                                //startActivity(intent);

                                dashboardApi();

                            } else {
                                Commonhelper.showToastLong(Login.this, Constnt.notFound);
                            }
                        } else {
                            Commonhelper.showToastLong(Login.this, invaildOTP);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Commonhelper.showToastLong(Login.this, invaildOTP);
                    }
                    dialog.dismiss();
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("reer", error.getMessage());
                        Commonhelper.showToastLong(Login.this, "No response fron server end");
                    }
                }) {
//            @Override
//            protected Map getParams()
//            {
//                Map params = new HashMap();
//                params.put("mobile", mob);
//                params.put("password", pwd);
//                //params.put("mobile", "9768178608");
//                //params.put("password", "123");
//                return params;
//            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(Login.this);
        requestQueue.add(stringRequest);
    }

    void sendOtp(final String mob) {
        new AbstrctClss(Login.this, ConstntApi.send_otp_login(Login.this, mob), "p", true) {
            @Override
            public void responce(String s) {
                try {
                    JSONObject jsonObject = new JSONObject(s);

                    if (jsonObject.optString("status").equalsIgnoreCase("success")) {
                        dilog(2);
                        Toast.makeText(Login.this, Constnt.oTPsentsuccessfully, Toast.LENGTH_LONG).show();
                    } else if (jsonObject.optString("status").equalsIgnoreCase("error")) {
                        Toast.makeText(Login.this, jsonObject.optString("description"), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(Login.this, notaregisterednumber, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    sendOtp(mob);
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                sendOtp(mob);
            }
        };
    }

    void sendOtTemp(final String mob) {
        new AbstrctClss(Login.this, ConstntApi.send_otp_login(Login.this, ""), "p", true) {
            @Override
            public void responce(String s) {
                try {
                    sendOtp(mob);
                } catch (Exception e) {
                    sendOtp(mob);
                    e.printStackTrace();
                }
            }
            @Override
            public void onErrorResponsee(String s) {
                sendOtp(mob);
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("strt", "strt");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("strt", "onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("strt", "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("strt", "puse");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("strt", "stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("strt", "distroy");
    }


    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 1000, 1000); //
    }

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        try
                        {
                            Commonhelper.sop("oooo");
                            ClipData pData = clipboardManager.getPrimaryClip();

                            if (clipboardManager.hasPrimaryClip()) {
                                Commonhelper.sop("oooo000");
                            }

                            ClipData.Item item = pData.getItemAt(0);
                            String txtpaste = item.getText().toString().substring(0);
                            String txtpaste2 = item.getText().toString().substring(1);
                            String txtpaste3 = item.getText().toString().substring(2);
                            String txtpaste4 = item.getText().toString().substring(3);

                            otp1.setText(txtpaste);
                            otp2.setText(txtpaste2);
                            otp3.setText(txtpaste3);
                            otp4.setText(txtpaste4);

                            stoptimertask();

                        } catch (Exception e) {

                        }
                    }
                });
            }
        };
    }


    Timer timer;
    TimerTask timerTask;

    //we are going to use a handler to be able to run in our TimerTask
    final Handler handler = new Handler();

}

package com.aurget.buddha.Activity.code.Product;

import android.content.Context;


import android.graphics.RectF;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;


import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.R;
import com.imagezoom.ImageAttacher;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class ImageAdapter  extends PagerAdapter {

    ArrayList<HashMap<String, String>> data;
    private LayoutInflater inflater;
    private Context context;
    private int i=0;
    public View.OnClickListener viewClickListener;

    public ImageAdapter(Context context,ArrayList<HashMap<String, String>> images,int i) {
        this.context = context;
        this.data=images;
        this.i = i;
        inflater = LayoutInflater.from(context);
        viewClickListener = v -> viewClick(v, String.valueOf(v.getTag()));
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.layout_pager, view, false);
        ImageView myImage =  myImageLayout.findViewById(R.id.imageView40);
        ProgressBar loader  =  myImageLayout.findViewById(R.id.loader);
        //myImage.setImageResource(data.get(position));

        myImage.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            if (data.get(position).get("name").isEmpty()) {
                Picasso.with(context).load((int) R.mipmap.ic_launcher).into(myImage);
            } else
                {

                if (i==1)
                {
                    Picasso.with(context).load(data.get(position).get("name")).into(myImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            myImage.setVisibility(View.VISIBLE);
                            loader.setVisibility(View.GONE);
                            usingSimpleImage(myImage);
                        }
                        @Override
                        public void onError() {

                        }
                    });
                }
                else
                {
                    Picasso.with(context).load(data.get(position).get("name")).into(myImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            myImage.setVisibility(View.VISIBLE);
                            loader.setVisibility(View.GONE);
                        }
                        @Override
                        public void onError() {

                        }
                    });
                }
            }
        });


        view.addView(myImageLayout, 0);

        myImageLayout.setTag(data.get(position).get("name"));
        myImageLayout.setOnClickListener(viewClickListener);

        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    protected abstract void viewClick(View view, String str);


    public void usingSimpleImage(ImageView imageView) {
        ImageAttacher mAttacher = new ImageAttacher(imageView);
        ImageAttacher.MAX_ZOOM = 4.0f; // times the current Size
        ImageAttacher.MIN_ZOOM = 1.0f; // Half the current Size
        MatrixChangeListener mMaListener = new MatrixChangeListener();
        mAttacher.setOnMatrixChangeListener(mMaListener);
        PhotoTapListener mPhotoTap = new PhotoTapListener();
        mAttacher.setOnPhotoTapListener(mPhotoTap);
    }

    private class PhotoTapListener implements ImageAttacher.OnPhotoTapListener {

        @Override
        public void onPhotoTap(View view, float x, float y) {
        }
    }

    private class MatrixChangeListener implements ImageAttacher.OnMatrixChangedListener {
        @Override
        public void onMatrixChanged(RectF rect) {

        }
    }

}

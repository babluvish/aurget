package com.aurget.buddha.Activity.categories;

import android.app.Dialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.aurget.buddha.Activity.AddtoCart.AddCartListActivity;
import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.EmptyCartActivity;
import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.aurget.buddha.Activity.Main2Activity.crtDetils2;

public class Categories extends AppCompatActivity {

    RecyclerView recyclerView, recycleView2;
    ImageView bkImg, bkImg2;
    private ArrayList<JSONObject> jsonObjectal = new ArrayList<>();
    TextView crtCountTv;
    String catId ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        recyclerView = findViewById(R.id.recycleView);
        recycleView2 = findViewById(R.id.recycleView2);
        crtCountTv = findViewById(R.id.crtCountTv);

        GridLayoutManager manager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);

        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(Categories.this, LinearLayoutManager.VERTICAL, false);
        recycleView2.setLayoutManager(horizontalLayoutManager);
        recycleView2.setItemAnimator(new DefaultItemAnimator());

        crtDetils();
        getCategory();

        catId = getIntent().getStringExtra("category_id");

        hitPi(catId);

        findViewById(R.id.bkImg2).setVisibility(View.VISIBLE);
        findViewById(R.id.bkImg).setVisibility(View.GONE);
        findViewById(R.id.bkImg2).setOnClickListener(view -> finish());

        findViewById(R.id.imghmb1).setOnClickListener(view -> {
            if (Integer.parseInt(crtCountTv.getText().toString().trim().replace("+","")) < 1) {
                Intent intent = new Intent(Categories.this, EmptyCartActivity.class);
                startActivity(intent);
                return;
            }
            Intent intent = new Intent(Categories.this, AddCartListActivity.class);
            intent.putExtra("res", crtDetils2);
            startActivity(intent);
        });

        final EditText searchEt = findViewById(R.id.searchEt);

        searchEt.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus)
            {
                searchEt.setCursorVisible(true);
            }
        });

        searchEt.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                //searchData(searchEt.getText().toString());
                Intent intent = new Intent(Categories.this, SubCategoryDetailsActivity.class);
                intent.putExtra("search", searchEt.getText().toString());
                startActivity(intent);
                return true;
            }
            return false;
        });
    }

    private void crtDetils() {
        new AbstrctClss(Categories.this, ConstntApi.get_cart_details(Categories.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    try {
                        crtDetils2 = Res;
                        Log.d("crtDetils", crtDetils2);
                        JSONArray jsonArray = new JSONArray(crtDetils2);
                        if (jsonArray.length() > 9)
                            crtCountTv.setText("9+");
                        else crtCountTv.setText("" + jsonArray.length());
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("letestFeturePi", "sssss");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    private void getCategory() {
        //JSONArray jsonArray = null;
        //ArrayList<JSONObject> jsonObjects = new ArrayList<>();
        //            jsonArray = new JSONArray(getIntent().getStringExtra("jsonObjectal"));
//            for (int i = 0; i < jsonArray.length(); i++) {
//                jsonObjects.add(jsonArray.optJSONObject(i));
//            }
        recycleView2.setAdapter(new MyRecyclerAdapter2(Constnt.all_category));
    }

    private void hitPi(String id) {
        final Dialog dialog = Commonhelper.loadDialog(Categories.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, InterfaceClass.sub_category + id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String Res = response;
                            Log.d("dsd", Res);

                            JSONArray jsonArray = new JSONArray(Res);

                            if (jsonArray.optJSONObject(0).optString("status").equalsIgnoreCase("true")) {
                                jsonObjectal.clear();
                                JSONArray jsonArray1 = jsonArray.optJSONObject(1).optJSONArray("subCategory");

                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    jsonObjectal.add(jsonArray1.optJSONObject(i));
                                }
                                recyclerView.setAdapter(new MyRecyclerAdapter(jsonObjectal));

                            } else {
                                Commonhelper.showToastLong(Categories.this, jsonArray.optJSONObject(0).optString("Message"));
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("dsd", "sssss");
                        }
                        dialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("reer", error.getMessage());
                    }
                }) {
//            @Override
//            protected Map getParams()
//            {
//                Map params = new HashMap();
//                params.put("username", "");
//                params.put("email", "abc@androidhive.info");
//                params.put("password", "password123");
//
//                return params;
//            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(Categories.this);
        requestQueue.add(stringRequest);

    }

    public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapter(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ctegories, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            try {
                holder.productNAmeTv.setText(jsonObjectal.get(position).optString("productName"));
                //Picasso.with(Categories.this).load(InterfaceClass.imgPth + jsonObjectal.get(position).optString("productID") + ".png").into(holder.img);
                Commonhelper.picasso2(Categories.this,jsonObjectal.get(position).optString("image_path") ,holder.img);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private TextView productNAmeTv;
            private ImageView img;

            public ViewHolder(View itemView) {
                super(itemView);
                productNAmeTv = itemView.findViewById(R.id.nmeTv);
                img = itemView.findViewById(R.id.img);

                img.setOnClickListener(view -> {
                    Intent intent = new Intent(Categories.this, SubCategoryDetailsActivity.class);
                    intent.putExtra("category_id", catId);
                    intent.putExtra("subcategory_id", jsonObjectal.get(getAdapterPosition()).optString("productID"));
                    startActivity(intent);
                });
            }
        }
    }

    public class MyRecyclerAdapter2 extends RecyclerView.Adapter<MyRecyclerAdapter2.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapter2(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ctegories_lbl, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position)
        {
            try {
                holder.productNAmeTv.setText(jsonObjectal.get(position).optString("category_name"));
                //Picasso.with(Categories.this).load(InterfaceClass.imgPthCtegory + jsonObjectal.get(position).optString("category_id") + ".jpg").into(holder.img);
                Commonhelper.picasso_(Categories.this,InterfaceClass.imgPthCtegory + jsonObjectal.get(position).optString("category_id") + ".jpg",holder.img);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView productNAmeTv;
            ImageView img;

            public ViewHolder(View itemView) {
                super(itemView);

                productNAmeTv = itemView.findViewById(R.id.txt1);
                img = itemView.findViewById(R.id.img);

                itemView.setOnClickListener(view -> {
                    catId = jsonObjectal.get(getAdapterPosition()).optString("category_id");
                    hitPi(catId);
                });
            }
        }
    }
}


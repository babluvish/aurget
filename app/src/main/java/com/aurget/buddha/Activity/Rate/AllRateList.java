package com.aurget.buddha.Activity.Rate;

import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class AllRateList extends AppCompatActivity {

    RecyclerView rateRv;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rate_all);
        
        rateRv = findViewById(R.id.rateRv);

        LinearLayoutManager horizontalLayoutManager8 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rateRv.setLayoutManager(horizontalLayoutManager8);

        allRate();
    }
    
    private void allRate()
    {
        try {
            String Res = getIntent().getStringExtra("RateRes");
            JSONArray jsonArray = new JSONArray(Res);
            ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObjectArrayList.add(jsonArray.getJSONObject(i));
            }

            rateRv.setAdapter(new MyRecyclerAdapterRate(jsonObjectArrayList));

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("dsd", "sssss");
        }
    }

    public class MyRecyclerAdapterRate extends RecyclerView.Adapter<MyRecyclerAdapterRate.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterRate(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapterRate.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rate_item2, parent, false);
            return new MyRecyclerAdapterRate.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final MyRecyclerAdapterRate.ViewHolder holder, int position) {
            int i = position + 1;
            //Picasso.with(AllRateList.this).load(InterfaceClass.imgPth2+product_id+"_"+i+"_thumb.jpg").into(holder.imageView);
            holder.userNameTv.setText(jsonObjectal.get(position).optString("user_name"));
            //holder.rate.setRating(Float.parseFloat(jsonObjectal.get(position).optString("rating")));
            holder.commentTv.setText(jsonObjectal.get(position).optString("comment"));
            holder.createdOnTv.setText("Created on: " + jsonObjectal.get(position).optString("created_at"));

            if (jsonObjectal.get(position).optString("rating").equals("1")) {
                holder.horribleImg.setColorFilter(ContextCompat.getColor(AllRateList.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
            } else if (jsonObjectal.get(position).optString("rating").equals("2")) {
                holder.horribleImg.setColorFilter(ContextCompat.getColor(AllRateList.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                holder.statImg2.setColorFilter(ContextCompat.getColor(AllRateList.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
            } else if (jsonObjectal.get(position).optString("rating").equals("3")) {
                holder.horribleImg.setColorFilter(ContextCompat.getColor(AllRateList.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                holder.statImg2.setColorFilter(ContextCompat.getColor(AllRateList.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                holder.statImg3.setColorFilter(ContextCompat.getColor(AllRateList.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
            } else if (jsonObjectal.get(position).optString("rating").equals("4")) {
                holder.horribleImg.setColorFilter(ContextCompat.getColor(AllRateList.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                holder.statImg2.setColorFilter(ContextCompat.getColor(AllRateList.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                holder.statImg3.setColorFilter(ContextCompat.getColor(AllRateList.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                holder.statImg4.setColorFilter(ContextCompat.getColor(AllRateList.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
            } else if (jsonObjectal.get(position).optString("rating").equals("5")) {
                holder.horribleImg.setColorFilter(ContextCompat.getColor(AllRateList.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                holder.statImg2.setColorFilter(ContextCompat.getColor(AllRateList.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                holder.statImg3.setColorFilter(ContextCompat.getColor(AllRateList.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                holder.statImg4.setColorFilter(ContextCompat.getColor(AllRateList.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                holder.statImg5.setColorFilter(ContextCompat.getColor(AllRateList.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
            }
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView userNameTv, commentTv, createdOnTv;
            ImageView statImg2, statImg3, statImg4, statImg5, horribleImg;

            public ViewHolder(View itemView) {
                super(itemView);
                horribleImg = itemView.findViewById(R.id.horribleImg);
                statImg2 = itemView.findViewById(R.id.statImg2);
                statImg3 = itemView.findViewById(R.id.statImg3);
                statImg4 = itemView.findViewById(R.id.statImg4);
                statImg5 = itemView.findViewById(R.id.statImg5);
                userNameTv = itemView.findViewById(R.id.userNameTv);
                commentTv = itemView.findViewById(R.id.commentTv);
                createdOnTv = itemView.findViewById(R.id.createdOnTv);
            }
        }
    }
}

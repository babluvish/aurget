package com.aurget.buddha.Activity.code.Account;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.Product.CartListActivity;
import com.aurget.buddha.Activity.code.Search.SearchActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import code.utils.AppUrls;

public class OrderListActivity extends BaseActivity implements View.OnClickListener {
    ProductAdapter productAdapter;
    ArrayList<HashMap<String, String>> productList = new ArrayList();

    RecyclerView recyclerView;
    RelativeLayout rrNoData;
    ImageView ivMenu,ivSearch,ivCart;

    Typeface typeface;
    GridLayoutManager mGridLayoutManager;

    TextView tvCount,tvNoProduct;

    boolean loadMore=true;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_order_list);
        findById();
    }

    private void findById() {
        rrNoData =  findViewById(R.id.rrNoData);

        //ImageView
        ivMenu =  findViewById(R.id.iv_menu);
        ivSearch =  findViewById(R.id.searchmain);
        ivCart =  findViewById(R.id.ivCart);

        //Recycleview
        recyclerView =  findViewById(R.id.productRecyclerView);

//        typeface = Typeface.createFromAsset(mActivity.getAssets(), "centurygothic.otf");

        //TextView
        tvCount =  findViewById(R.id.tvCount);
        tvNoProduct = findViewById(R.id.textView5);
        TextView tvHeader =  findViewById(R.id.tvHeaderText);
        tvHeader.setText("Order List");

        tvNoProduct.setText("Oops!\n\nSeems like you haven't ordered anything yet!");

        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        ivCart.setOnClickListener(this);

        ivSearch.setVisibility(View.INVISIBLE);
        ivCart.setVisibility(View.INVISIBLE);
        ivMenu.setImageResource(R.drawable.ic_back);

        mGridLayoutManager = new GridLayoutManager(mActivity, 1);
        recyclerView.setLayoutManager(mGridLayoutManager);

    }

    private void getOrderListApi() {
        AppUtils.showRequestDialog(mActivity);
        Log.v("getOrderListApi", AppUrls.orderList);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            //String loginId = CustomPreference.readString(mActivity, CustomPreference.e_id, "");
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json.put(AppConstants.result, json_data);
            Log.v("getOrderListApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.orderList)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {

                parseJsondata(response);
            }

            @Override
            public void onError(ANError error) {
                 AppUtils.hideDialog();
                // handle error
                if (error.getErrorCode() != 0) {
                    AppUtils.showErrorMessage(rrNoData, String.valueOf(error.getErrorCode()), mActivity);
                    Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                    Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                    Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                } else {
                    AppUtils.showErrorMessage(rrNoData, String.valueOf(error.getErrorDetail()), mActivity);
                }
            }
        });
    }

    private void parseJsondata(JSONObject response) {

        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                JSONArray productArray = jsonObject.getJSONArray("order_history");

                for (int i = 0; i < productArray.length(); i++) {

                    JSONObject productobject = productArray.getJSONObject(i);

                    HashMap<String, String> prodList = new HashMap();

                    prodList.put("status", productobject.getString("status"));
                    prodList.put("total_price", productobject.getString("total_price"));
                    prodList.put("order_id", productobject.getString("order_id"));
                    prodList.put("id", productobject.getString("id"));
                    prodList.put("IP", productobject.getString("IP"));
                    prodList.put("add_date", productobject.getString("add_date")+"000");
                   // prodList.put("delivery_time", productobject.getString("delivery_time"));
                    productList.add(prodList);
                }
                if (productList.size() <= 0) {
                    rrNoData.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                } else {
                    rrNoData.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    productAdapter = new ProductAdapter(productList);
                    recyclerView.setAdapter(productAdapter);
                    recyclerView.setNestedScrollingEnabled(true);
                }
            }
            else
            {
                loadMore=false;
                AppUtils.showErrorMessage(rrNoData, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(rrNoData, String.valueOf(e), mActivity);
        }
         AppUtils.hideDialog();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_menu:
                finish();
                return;

            case R.id.searchmain:
                startActivity(new Intent(getBaseContext(), SearchActivity.class));
                return;

            case R.id.ivCart:

                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else
                {
                    startActivity(new Intent(getBaseContext(), CartListActivity.class));
                }
                return;

            default:
                return;
        }
    }

    private class ProductAdapter extends RecyclerView.Adapter<FavNameHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

        public ProductAdapter(ArrayList<HashMap<String, String>> favList) {
            data = favList;
        }

        public FavNameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new FavNameHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_order, parent, false));
        }
        public void onBindViewHolder(FavNameHolder holder, final int position) {

            /*holder.tvName.setTypeface(typeface);
            holder.tvPrice.setTypeface(typeface);
            holder.tvStatus.setTypeface(typeface);
            holder.tvDate.setTypeface(typeface);
            holder.tvPriceN.setTypeface(typeface);
            holder.tvStatusN.setTypeface(typeface);
            holder.tvDateN.setTypeface(typeface);*/
            holder.tvName.setText("Order Id : "+data.get(position).get("order_id"));
            holder.tvPrice.setText(getString(R.string.rs)+" "+data.get(position).get("total_price"));
            holder.IPTv.setText(""+" "+data.get(position).get("IP"));

            try {
                holder.tvDate.setText(AppUtils.getDateFromMilisecond(Long.parseLong(data.get(position).get("add_date"))));
            } finally {

            }

          //  holder.tvDeliveryDate.setText(data.get(position).get("delivery_time"));

            if(data.get(position).get("status").equals("1"))
            {
                holder.tvStatus.setTextColor(Color.parseColor("#d43f3a"));
                holder.tvStatus.setText("Pending Process");
            }
            else  if(data.get(position).get("status").equals("2"))
            {
                holder.tvStatus.setTextColor(Color.parseColor("#ff6c00"));
                holder.tvStatus.setText("Pending Delivery");
            }
            else  if(data.get(position).get("status").equals("3"))
            {
                holder.tvStatus.setTextColor(Color.parseColor("#157936"));
                holder.tvStatus.setText("Completed");
            }
            else if(data.get(position).get("status").equals("4"))
            {
                holder.tvStatus.setTextColor(Color.parseColor("#ee383e"));
                holder.tvStatus.setText("Cancelled");
            }
            else if(data.get(position).get("status").equals("5"))
            {
                holder.tvStatus.setTextColor(Color.parseColor("#ee383e"));
                holder.tvStatus.setText("Cancelled and Refund");
            }

            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent mIntent = new Intent(mActivity, OrderDetailActivity.class);
                    mIntent.putExtra(AppConstants.orderId, data.get(position).get("order_id"));
                    mIntent.putExtra(AppConstants.from, "1");
                    startActivity(mIntent);
                    finish();
                }
            });
        }

        public int getItemCount() {
            return data.size();
        }
    }

    private class FavNameHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayout;
        TextView tvName;
        TextView tvPrice;
        TextView tvDate;
        TextView tvDeliveryDate;
        TextView tvStatus;
        TextView tvPriceN;
        TextView tvDateN;
        TextView tvStatusN;
        TextView IPTv;

        public FavNameHolder(View itemView) {
            super(itemView);
            linearLayout =  itemView.findViewById(R.id.layout_item);
            tvName =  itemView.findViewById(R.id.tvProductName);
            tvStatus =  itemView.findViewById(R.id.tvStatus);
            tvPrice =  itemView.findViewById(R.id.tvPrice);
            tvDate =  itemView.findViewById(R.id.tvDate);
            tvDeliveryDate =  itemView.findViewById(R.id.tvDeliveryDate);

            tvPriceN =  itemView.findViewById(R.id.tvFPrice);
            tvStatusN =  itemView.findViewById(R.id.tv3);
            tvDateN =  itemView.findViewById(R.id.tv2);
            IPTv =  itemView.findViewById(R.id.IPTv);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        productList.clear();
        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getOrderListApi();
        } else {
            AppUtils.showErrorMessage(rrNoData, getString(R.string.errorInternet), mActivity);
        }
    }
}

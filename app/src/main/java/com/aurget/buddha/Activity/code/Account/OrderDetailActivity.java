package com.aurget.buddha.Activity.code.Account;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Fragment.DashBoardFragment;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.Product.CartListActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import code.utils.AppUrls;

public class OrderDetailActivity extends BaseActivity implements View.OnClickListener {

    ArrayList<HashMap<String, String>> cartList = new ArrayList();

    RecyclerView recyclerView;

    //CartApdapter cartAdapter;

    //ImageView header
    ImageView ivMenu;
    ImageView ivSearch;
    ImageView ivCart;

    String finalPrice = "";

    CartApdapter cartAdapter;

    TextView tvOrderId,tvDate,tvStatus,tvPrice,tvMode,tvAddress,tvName,tvPriceNew,tvDiscount,tvAmount
            ,tvShippingAmount,tvDiscountAmount,tvDeliveryDate,totIp;

    TextView tvCount,tvReorder;

    RelativeLayout rlShipping;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        initialise();
    }

    private void initialise() {

        //RecyclerView for list
        recyclerView = findViewById(R.id.recyclerview);

        //ImageView
        ivMenu =    findViewById(R.id.iv_menu);
        ivSearch =  findViewById(R.id.searchmain);
        ivCart =    findViewById(R.id.ivCart);

        //TextView
        tvCount =  findViewById(R.id.tvCount);
        TextView tvHeader =  findViewById(R.id.tvHeaderText);
        tvHeader.setText("Order Detail");

        tvOrderId=  findViewById(R.id.tvOrderId);
        tvDate=  findViewById(R.id.tvDate);
        tvStatus=  findViewById(R.id.tvStatus);
        tvPrice=  findViewById(R.id.tvPrice);
        tvMode=  findViewById(R.id.tvPayMode);
        tvAddress=  findViewById(R.id.textView5);
        tvName=  findViewById(R.id.textView3);
        tvPriceNew=  findViewById(R.id.textView11);
        tvAmount=   findViewById(R.id.textView14);
        tvReorder=   findViewById(R.id.tvReorder);
        tvShippingAmount=  findViewById(R.id.tvShippingValue);
        tvDiscount=  findViewById(R.id.tvDiscount);
        tvDiscountAmount=  findViewById(R.id.tvDiscountAmount);
        tvDeliveryDate=  findViewById(R.id.tvDeliveryDate);

        rlShipping= findViewById(R.id.rlShipping);
        totIp= findViewById(R.id.totIp);

        ivMenu.setImageResource(R.drawable.ic_back);
        ivSearch.setVisibility(View.INVISIBLE);
        ivCart.setVisibility(View.INVISIBLE);

        ivMenu.setOnClickListener(this);
        tvReorder.setOnClickListener(this);

        RecyclerView.LayoutManager recylerViewLayoutManager = new LinearLayoutManager(mActivity);

        recyclerView.setLayoutManager(recylerViewLayoutManager);
        //recyclerView.setAdapter(new CartListActivity.SimpleStringRecyclerViewAdapter(recyclerView, cartlistImageUri));

        tvDiscountAmount.setText("RS 0");

        cartList.clear();
        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getOrderDetailApi();
        } else {
            AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
        }
    }

    private void getOrderDetailApi() {
        AppUtils.showRequestDialog(mActivity);
        Log.v("getOrderDetailApi", AppUrls.orderDetails);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("order_id", getIntent().getStringExtra(AppConstants.orderId));

            json.put(AppConstants.result, json_data);

            Log.v("getOrderDetailApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.orderDetails)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        parseJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseJsondata(JSONObject response) {

        Log.d("response ", response.toString());

        finalPrice =" ";
        AppSettings.putString(AppSettings.price, String.valueOf(finalPrice));
        AppSettings.putString(AppSettings.finalPrice, String.valueOf(finalPrice));

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                JSONObject jsonObject1 = jsonObject.getJSONObject("order_details_history");
                tvOrderId.setText("Order Id : "+jsonObject1.getString("order_number"));
                try {
                    tvDate.setText("Date : "+AppUtils.getDateFromMilisecond(Long.parseLong(jsonObject1.getString("add_date")+"000")));
                } catch(NumberFormatException e) {
                }
              //  tvDeliveryDate.setText("Delivery Date : "+jsonObject1.getString("delivery_time"));
                String status="";
                //1=Pending,2=Cancel.3=Accept,4=Shipped,5=Delivered
                if(jsonObject1.getString("order_status").equals("1"))
                {
                    tvStatus.setTextColor(Color.parseColor("#d43f3a"));
                    status = "Pending Process";
                }
                else  if(jsonObject1.getString("order_status").equals("2"))
                {
                    tvStatus.setTextColor(Color.parseColor("#ff6c00"));
                    status = "Pending Delivery";
                }
                else  if(jsonObject1.getString("order_status").equals("3"))
                {
                    tvStatus.setTextColor(Color.parseColor("#157936"));
                    status = "Completed";
                }
                else  if(jsonObject1.getString("order_status").equals("4"))
                {
                    tvStatus.setTextColor(Color.parseColor("#ee383e"));
                    status = "Cancelled";
                }
                else  if(jsonObject1.getString("order_status").equals("5"))
                {
                    tvStatus.setTextColor(Color.parseColor("#ee383e"));
                    status = "Cancelled and Refund";
                }
                tvStatus.setText("Status : "+status);
                tvPrice.setText("Price : RS"+" "+jsonObject1.getString("total_price"));

                String mode="";
                if(jsonObject1.getString("order_mode").equals("1"))
                {
                    mode = "Cash On Delivery";
                }
                else  if(jsonObject1.getString("order_mode").equals("2"))
                {
                    mode = "Online Payment";
                }
                else  if(jsonObject1.getString("order_mode").equals("3"))
                {
                    mode = "Wallet";
                }

                tvMode.setText("Mode of Payment : "+mode);

                tvName.setText(jsonObject1.getString("person_name"));
                tvAddress.setText(jsonObject1.getString("shipping_address")+ "\n"+"Phone Number: "+jsonObject1.getString("phone_no"));

             //   tvShippingAmount.setText(getString(R.string.rs)+" "+jsonObject1.getString("shipping_amount"));
                tvShippingAmount.setText(getString(R.string.rs)+" "+"10.00");

                if(jsonObject1.getString("shipping_amount").isEmpty())
                {
                    //tvShippingAmount.setText("RS 0" );
                    tvShippingAmount.setText(getString(R.string.rs)+" "+"10.00");
                }
                else
                {
                  //  tvShippingAmount.setText(getString(R.string.rs)+" " +jsonObject1.getString("shipping_amount"));
                    tvShippingAmount.setText(getString(R.string.rs)+" "+"10.00");
                }

               // tvDiscount.setText("Discount (" +jsonObject1.getString("coupon_code")+")");
                tvDiscount.setText("Discount (NA"+")");
             //   tvDiscountAmount.setText(getString(R.string.rs)+" "+jsonObject1.getString("discount"));
                tvDiscountAmount.setText("RS 0");
                JSONArray productArray = jsonObject1.getJSONArray("product_list");

                for (int i = 0; i < productArray.length(); i++) {

                    JSONObject productobject = productArray.getJSONObject(i);
                    HashMap<String, String> prodList = new HashMap();

                    prodList.put("product_id", productobject.getString("product_id"));
                    prodList.put("product_name", productobject.getString("product_name"));
                    prodList.put("final_price", productobject.getString("final_price"));
                    prodList.put("price", productobject.getString("price"));
                    prodList.put("product_discount_amount", productobject.getString("discount_amount"));
                    prodList.put("product_discount_type", productobject.getString("discount_type"));
                    prodList.put("discount_applied_in", productobject.getString("discount_applied_in"));
                    prodList.put("product_image", productobject.getString("product_image"));
                    prodList.put("quantity", productobject.getString("quantity"));
                    prodList.put("ip", productobject.getString("ip"));
                    prodList.put("status", productobject.getString("status"));

                    cartList.add(prodList);

                    float price = Float.parseFloat(productobject.getString("final_price"));
                    float quantity = Float.parseFloat(productobject.getString("quantity"));

                    float amount = price*quantity;

                    //finalPrice = finalPrice+amount;
                    finalPrice = String.format("%.2f",amount);
                }

                AppSettings.putString(AppSettings.price, String.valueOf(finalPrice));
                AppSettings.putString(AppSettings.finalPrice, String.valueOf(finalPrice));

                String total_price=String.format("%.2f",Float.parseFloat(jsonObject1.getString("total_price"))-10);

                tvPriceNew.setText(getString(R.string.rs)+" " + total_price);
                tvAmount.setText(getString(R.string.rs)+" "+ jsonObject1.getString("total_price"));
            }
            else
            {
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(recyclerView, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();

        cartAdapter = new CartApdapter(cartList);
        recyclerView.setAdapter(cartAdapter);
        recyclerView.setNestedScrollingEnabled(true);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_menu:

                onBackPressed();

                return;

            case R.id.tvReorder:

                AlertPopUp(getIntent().getStringExtra(AppConstants.orderId),2);

                return;

            case R.id.ivCart:

                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else
                {
                    startActivity(new Intent(getBaseContext(), CartListActivity.class));
                }

                return;
        }

    }

    private class CartApdapter extends RecyclerView.Adapter<FavNameHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

        public CartApdapter(ArrayList<HashMap<String, String>> favList) {
            data = favList;
        }

        public FavNameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new FavNameHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_order_item, parent, false));
        }

        public void onBindViewHolder(FavNameHolder holder, final int position) {

            float price = Float.parseFloat(data.get(position).get("final_price"));
            float quantity = Float.parseFloat(data.get(position).get("quantity"));

            float amount = price*quantity;
             /* if (((HashMap) data.get( position )).get( "product_discount_type" ).equals( "1" )) {
                holder.tvDiscount.setText( getString(R.string.rs) + " " + ((HashMap) data.get( position )).get( "product_discount_amount" ) + " /-off" );
            } else if (((HashMap) data.get( position )).get( "product_discount_type" ).equals( "2" )) {
                holder.tvDiscount.setText( getString(R.string.rs) + " " + ((HashMap) data.get( position )).get( "product_discount_amount" ) + "% off" );
            } else {
                // holder.tvDiscount.setText("");
                holder.tvDiscount.setVisibility( View.INVISIBLE );
                holder.tvPrice.setVisibility( View.INVISIBLE );
            }*/

            holder.tvName.setText(""+(CharSequence) ((HashMap) data.get(position)).get("product_name"));
            holder.ip.setText((CharSequence) ((HashMap) data.get(position)).get("ip"));

            totIp3 =  totIp3 +Double.parseDouble(holder.ip.getText().toString());

            totIp.setText(""+totIp3);

            holder.tvQuantityCount.setText((CharSequence) ((HashMap) data.get(position)).get("quantity"));
            if (((String) ((HashMap) data.get(position)).get("product_discount_type")).equals("1")) {
                holder.tvDiscount.setText(getString(R.string.rs)+" " + ((String) ((HashMap) data.get(position)).get("product_discount_amount")) + " /- off (RS"+" "+amount+")");
            } else if (((String) ((HashMap) data.get(position)).get("product_discount_type")).equals("2")) {
                holder.tvDiscount.setText(((String) ((HashMap) data.get(position)).get("product_discount_amount")) + "%off (RS"+" "+amount+")");
            } else {
                holder.tvDiscount.setText("");
                holder.tvDiscount.setVisibility(View.INVISIBLE);
                holder.tvPrice.setVisibility( View.INVISIBLE );
            }
            if (((String) ((HashMap) data.get(position)).get("final_price")).equalsIgnoreCase((String) ((HashMap) data.get(position)).get("price"))) {
                holder.tvFPrice.setText(getString(R.string.rs)+" " + ((String) ((HashMap) data.get(position)).get("final_price")));
                holder.tvDiscount.setText("");
                holder.tvPrice.setText("");
            } else {
                holder.tvFPrice.setText(getString(R.string.rs)+" " + ((String) ((HashMap) data.get(position)).get("final_price")));
                holder.tvPrice.setText(getString(R.string.rs)+" " + ((String) ((HashMap) data.get(position)).get("price")));
                holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | 16);
            }
            try {
                if (data.get(position).get("product_image")!= null && !data.get(position).get("product_image").isEmpty()) {
                    Picasso.with(mActivity).load(data.get(position).get("product_image")).into(holder.ivPic);
                }
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }

            holder.ivDelete.setVisibility(View.VISIBLE);
            String status="";
            //1=Pending,2=Cancel.3=Accept,4=Shipped,5=Delivered
            if(data.get(position).get("status").equals("1"))
            {
                tvStatus.setTextColor(Color.parseColor("#d43f3a"));
                status = "Pending Process";
            }
            else  if(data.get(position).get("status").equals("2"))
            {
                tvStatus.setTextColor(Color.parseColor("#ff6c00"));
                status = "Pending Delivery";
            }
            else  if(data.get(position).get("status").equals("3"))
            {
                tvStatus.setTextColor(Color.parseColor("#157936"));
                status = "Completed";
            }
            else  if(data.get(position).get("status").equals("4"))
            {
                holder.tvStatus.setTextColor(Color.parseColor("#eeffaa00"));
                status = "Shipped";
            }
            else  if(data.get(position).get("status").equals("5"))
            {
                holder.tvStatus.setTextColor(Color.parseColor("#4CAF50"));
                holder.ivDelete.setVisibility(View.GONE);
                status = "Delivered";
            }

            holder.tvStatus.setText("Status : "+status);

            holder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   AlertPopUp(data.get(position).get("product_id"),1);
                }
            });

        }

        public int getItemCount() {
            return data.size();
        }
    }

    private class FavNameHolder extends RecyclerView.ViewHolder {
        ImageView ivPic,ivAdd,ivSubs,ivDelete;
        TextView tvDiscount,tvStatus;
        TextView tvFPrice;
        TextView tvName,ip;
        TextView tvPrice,tvQuantityCount;

        public FavNameHolder(View itemView) {
            super(itemView);
            ivPic = (ImageView) itemView.findViewById(R.id.image_cartlist);
            ip =  itemView.findViewById(R.id.ipTv);
            ivAdd = (ImageView) itemView.findViewById(R.id.imageView2);
            ivDelete = (ImageView) itemView.findViewById(R.id.ivDelete);
            ivSubs = (ImageView) itemView.findViewById(R.id.imageView3);
            tvName = (TextView) itemView.findViewById(R.id.tvProductName);
            tvFPrice = (TextView) itemView.findViewById(R.id.tvFPrice);
            tvPrice = (TextView) itemView.findViewById(R.id.tvPrice);
            tvDiscount = (TextView) itemView.findViewById(R.id.tvDiscount);
            tvStatus = (TextView) itemView.findViewById(R.id.tvStatus);
            tvQuantityCount = (TextView) itemView.findViewById(R.id.tvQuantityCount);

        }
    }

    private void deleteProductApi(String productId) {

        AppUtils.showRequestDialog(mActivity);

        Log.v("deleteProductApi", AppUrls.cancelOrder);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("order_id", getIntent().getStringExtra(AppConstants.orderId));
            json_data.put("product_id", productId);
            json.put(AppConstants.result, json_data);
            Log.v("deleteProductApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.cancelOrder)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedeletedata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedeletedata(JSONObject response) {

        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
                AppUtils.hideDialog();
                cartList.clear();
                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    getOrderDetailApi();
                } else {
                    AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                }
            }
            else
            {
                 AppUtils.hideDialog();
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
             AppUtils.hideDialog();
            AppUtils.showErrorMessage(recyclerView, String.valueOf(e), mActivity);
        }

    }


    private void reOrderApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("reOrderApi", AppUrls.rePlaceOrder);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("order_id", getIntent().getStringExtra(AppConstants.orderId));

            json.put(AppConstants.result, json_data);
            Log.v("reOrderApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.rePlaceOrder)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsereorderdata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                         AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsereorderdata(JSONObject response) {

        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
                 AppUtils.hideDialog();

                onBackPressed();
            }
            else
            {
                 AppUtils.hideDialog();
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
             AppUtils.hideDialog();
            AppUtils.showErrorMessage(recyclerView, String.valueOf(e), mActivity);
        }

    }


    //AlertPopUp
    private void AlertPopUp(final String productId, final int from) {

        final Dialog dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alertyesno);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();

        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        //TextView
        TextView tvYes                              = (TextView)dialog.findViewById(R.id.tvOk);
        TextView tvCancel                       = (TextView)dialog.findViewById(R.id.tvcancel);
        TextView tvReason                       = (TextView)dialog.findViewById(R.id.textView22);
        TextView tvAlertMsg                       = (TextView)dialog.findViewById(R.id.tvAlertMsg);

      //  tvAlertMsg.setText("Confirmation");

        if(from==1)
        {
            tvReason.setText("Order will be cancelled. Are you sure?");
        }
        else
        {
            tvReason.setText("Order will be Re-Ordered. Are you sure?");
        }

        dialog.show();

        tvYes.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                dialog.dismiss();

                if(from==1)
                {
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                        deleteProductApi(productId);
                    } else {
                        AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                    }
                }
                else
                {
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                        reOrderApi();
                    } else {
                        AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                    }
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(getIntent().getStringExtra(AppConstants.from).equals("1"))
        {
            Intent mIntent = new Intent(mActivity, OrderListActivity.class);
            startActivity(mIntent);
            finish();
        }
        else
        {
            Intent mIntent = new Intent(mActivity, DashBoardFragment.class);
            startActivity(mIntent);
            finish();
        }
    }

    double totIp3 =0.0;
}

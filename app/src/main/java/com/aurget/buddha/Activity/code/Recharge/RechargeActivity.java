package com.aurget.buddha.Activity.code.Recharge;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.payment.WebViewActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import code.utility.AvenuesParams;
import code.utility.Constants;
import code.utils.AppUrls;

public class RechargeActivity extends BaseActivity implements View.OnClickListener {

    //ImageView
    ImageView ivCart,ivMenu,ivSearch,ivHome,ivCheck,ivMobile;

    //TextView
    TextView tvHeaderText,tvRecharge,tvDth,tvRechargeType,tvSubmit,tvUseWallet
    ,tvRech;

    //ArrayList
    public static ArrayList<String> blankList = new ArrayList<String>();
    public static ArrayList<String> operator_List = new ArrayList<String>();
    public static ArrayList<String> operator_Code = new ArrayList<String>();

    //ArrayList
    public static ArrayList<String> postpaid_operator_List = new ArrayList<String>();
    public static ArrayList<String> postpaid_operator_Code = new ArrayList<String>();

    //ArrayList
    public static ArrayList<String> circle_List = new ArrayList<String>();
    public static ArrayList<String> circle_Code = new ArrayList<String>();

    //ArrayList
    public static ArrayList<String> dth_operator_List = new ArrayList<String>();
    public static ArrayList<String> dth_operator_Code = new ArrayList<String>();

    //ArrayList
    public static ArrayList<String> typeList = new ArrayList<String>();
    public static ArrayList<String> typeCode = new ArrayList<String>();

    //Spinner
    Spinner spinner,spinner_operator,spinner_circle;

    String rechargetype="";
    String operatorId="",circle_code="";
    String recharge="";

    View v1;

    //EditText
    EditText edMobile,edAmount;

    //Typeface
    Typeface typeface;

    //RelativeLayout
    RelativeLayout rrUseWallet;

    float wallet=0;
    int check=0;

    // Declare
    static final int PICK_CONTACT=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge);

        initialise();
    }

    private void initialise() {

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        operator_List.clear(); operator_Code.clear();
        operator_List.add("Select Operator"); operator_Code.add("Select Operator");
        operator_List.add("Aircel");operator_Code.add("AC");
        operator_List.add("Airtel");operator_Code.add("AT");
        operator_List.add("BSNL 3G");operator_Code.add("BSG");
        operator_List.add("BSNL Recharge/Validity (RCV)");operator_Code.add("BSV");
        operator_List.add("BSNL Special");operator_Code.add("BSS");
        operator_List.add("BSNL TopUp");operator_Code.add("BSNL");
        operator_List.add("DOCOMO CDMA");operator_Code.add("DOCD");
        operator_List.add("Idea");operator_Code.add("ID");
        operator_List.add("Jio Reliance");operator_Code.add("JIO");
        operator_List.add("Loop Mobile");operator_Code.add("LM");
        operator_List.add("MTNL DELHI");operator_Code.add("MD");
        operator_List.add("MTNL DELHI Special");operator_Code.add("MDS");
        operator_List.add("MTNL MUMBAI");operator_Code.add("MM");
        operator_List.add("MTNL MUMBAI SPECIAL");operator_Code.add("MMS");
        operator_List.add("MTNL Recharge/Special");operator_Code.add("MTR");
        operator_List.add("MTNL TopUp");operator_Code.add("MTT");
        operator_List.add("MTS");operator_Code.add("MTS");
        operator_List.add("Reliance CDMA");operator_Code.add("REC");
        operator_List.add("Reliance GSM");operator_Code.add("REG");
        operator_List.add("T24");operator_Code.add("T24");
        operator_List.add("T24 Special");operator_Code.add("T24S");
        operator_List.add("Tata Docomo GSM");operator_Code.add("TD");
        operator_List.add("Tata Docomo GSM Special");operator_Code.add("TDC");
        operator_List.add("Tata Indicom");operator_Code.add("TI");
        operator_List.add("Tata Walky");operator_Code.add("TW");
        operator_List.add("Telenor");operator_Code.add("UN");
        operator_List.add("Telenor Special");operator_Code.add("UNS");
        operator_List.add("Videocon");operator_Code.add("VD");
        operator_List.add("Videocon Special");operator_Code.add("VDS");
        operator_List.add("Virgin CDMA");operator_Code.add("VC");
        operator_List.add("Virgin GSM");operator_Code.add("VG");
        operator_List.add("Vodafone");operator_Code.add("VF");

        postpaid_operator_List.clear();postpaid_operator_Code.clear();
        postpaid_operator_List.add("Select Operator"); postpaid_operator_Code.add("Select Operator");
        postpaid_operator_List.add("Aircel Postpaid"); postpaid_operator_Code.add("ARCP");
        postpaid_operator_List.add("Airtel"); postpaid_operator_Code.add("ATP");
        postpaid_operator_List.add("BSNL Mobile"); postpaid_operator_Code.add("BSP");
        postpaid_operator_List.add("Idea"); postpaid_operator_Code.add("IDP");
        postpaid_operator_List.add("Loop Mobile"); postpaid_operator_Code.add("LMP");
        postpaid_operator_List.add("Reliance CDMA"); postpaid_operator_Code.add("RCP");
        postpaid_operator_List.add("Reliance GSM"); postpaid_operator_Code.add("RGP");
        postpaid_operator_List.add("Tata Docomo"); postpaid_operator_Code.add("TDP");
        postpaid_operator_List.add("Tata Indicom"); postpaid_operator_Code.add("TIP");
        postpaid_operator_List.add("Vodafone"); postpaid_operator_Code.add("VFP");

        dth_operator_List.clear();dth_operator_Code.clear();
        dth_operator_List.add("Select Operator"); dth_operator_Code.add("Select Operator");
        dth_operator_List.add("Airtel DTH"); dth_operator_Code.add("ATD");
        dth_operator_List.add("Big TV"); dth_operator_Code.add("BTD");
        dth_operator_List.add("Dish TV"); dth_operator_Code.add("DTD");
        dth_operator_List.add("Sun DTH"); dth_operator_Code.add("SD");
        dth_operator_List.add("Tata Sky"); dth_operator_Code.add("TSD");
        dth_operator_List.add("Videocon DTH"); dth_operator_Code.add("VDD");

        typeList.clear();typeCode.clear();
        typeList.add("Select Recharge Type"); typeCode.add("Select Recharge Type");
        typeList.add("Prepaid"); typeCode.add("1");
        typeList.add("Postpaid"); typeCode.add("2");


        circle_List.clear();circle_Code.clear();
        circle_List.add("Select Your Region"); circle_Code.add("Select Your Region");
        circle_List.add("Andhra Pradesh"); circle_Code.add("1");
        circle_List.add("Assam"); circle_Code.add("2");
        circle_List.add("Bihar"); circle_Code.add("3");
        circle_List.add("Chennai"); circle_Code.add("4");
        circle_List.add("Delhi"); circle_Code.add("5");
        circle_List.add("Gujarat"); circle_Code.add("6");
        circle_List.add("Haryana"); circle_Code.add("7");
        circle_List.add("Himachal Pradesh"); circle_Code.add("8");
        circle_List.add("Jammu & Kashmir"); circle_Code.add("9");
        circle_List.add("Karnataka"); circle_Code.add("10");
        circle_List.add("Kerala"); circle_Code.add("11");
        circle_List.add("Kolkata"); circle_Code.add("12");
        circle_List.add("Maharashtra & Goa (except Mumbai)"); circle_Code.add("13");
        circle_List.add("Madhya Pradesh & Chhattisgarh"); circle_Code.add("14");
        circle_List.add("Mumbai"); circle_Code.add("15");
        circle_List.add("North East"); circle_Code.add("16");
        circle_List.add("Orissa"); circle_Code.add("17");
        circle_List.add("Punjab"); circle_Code.add("18");
        circle_List.add("Rajasthan"); circle_Code.add("19");
        circle_List.add("Tamil Nadu"); circle_Code.add("20");
        circle_List.add("Uttar Pradesh - East"); circle_Code.add("21");
        circle_List.add("Uttar Pradesh - West"); circle_Code.add("22");
        circle_List.add("West Bengal"); circle_Code.add("23");
        circle_List.add("Jharkhand"); circle_Code.add("24");
        circle_List.add("Uttarakhand"); circle_Code.add("25");


        //Typeface
        typeface   = Typeface.createFromAsset(mActivity.getAssets(),"centurygothic.otf");

        //RelativeLayout
        rrUseWallet= (RelativeLayout) findViewById(R.id.rrUseWallet);

        //ImageView
        ivCart =      findViewById(R.id.ivCart);
        ivMenu =      findViewById(R.id.iv_menu);
        ivSearch =    findViewById(R.id.searchmain);
        ivHome =      findViewById(R.id.ivHomeBottom);
        ivCheck =     findViewById(R.id.ivWalletCheck);
        ivMobile =    findViewById(R.id.imageView20);

        //EditText
        edMobile =  findViewById(R.id.editText3);
        edAmount =  findViewById(R.id.editText4);

        //TextView
        tvHeaderText    =  findViewById(R.id.tvHeaderText);
        tvRecharge      =  findViewById(R.id.textView20);
        tvDth                   =  findViewById(R.id.textView21);
        tvRechargeType=  findViewById(R.id.tv_type);
        tvSubmit=  findViewById(R.id.textView27);
        tvUseWallet=  findViewById(R.id.tvWalletCheck);
        tvRech=  findViewById(R.id.tvh2r);

        //View
        v1 =findViewById(R.id.v1);

        //Spinner
        spinner = findViewById(R.id.spinner);
        spinner_operator = findViewById(R.id.spinner2);
        spinner_circle = findViewById(R.id.spinner3);

        ivCart.setOnClickListener(this);
        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        ivMobile.setOnClickListener(this);
        tvRecharge.setOnClickListener(this);
        tvDth.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        rrUseWallet.setOnClickListener(this);

        ivCart.setVisibility(View.INVISIBLE);
        ivSearch.setVisibility(View.INVISIBLE);

        ivMenu.setImageResource(R.drawable.ic_back);

        tvHeaderText.setText(getString(R.string.recharge));

        if(!AppSettings.getString(AppSettings.wallet).equals(""))
        {
            wallet = Float.parseFloat(AppSettings.getString(AppSettings.wallet));
        }

        if(wallet<=0)
        {
            check=0;
            ivCheck.setImageResource(R.drawable.ic_check_box_unselected);
            rrUseWallet.setVisibility(View.GONE);
        }
        else
        {
            check=1;
            ivCheck.setImageResource(R.drawable.ic_check_box_selected);
            rrUseWallet.setVisibility(View.VISIBLE);
        }

        tvUseWallet.setText("Use RS "+AppSettings.getString(AppSettings.wallet));

        spinner.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, typeList));
        spinner.setSelection(0);

        spinner_circle.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, circle_List));
        spinner_circle.setSelection(0);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                if(position==0)
                {
                    rechargetype = "";
                    spinner_operator.setSelection(0);
                }
                else if(position==1)
                {
                    rechargetype = typeCode.get(position);
                    spinner_operator.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, operator_List));
                    spinner_operator.setSelection(0);
                }
                else if(position==2)
                {
                    rechargetype = typeCode.get(position);
                    spinner_operator.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, postpaid_operator_List));
                    spinner_operator.setSelection(0);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        spinner_operator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                if(position==0)
                {
                    operatorId = "";
                }
                else
                {
                    if(rechargetype.equals("1"))
                    {
                        AppSettings.putString(AppSettings.operatorName,operator_List.get(position));
                        operatorId = operator_Code.get(position);
                    }
                    else if(rechargetype.equals("2"))
                    {
                        AppSettings.putString(AppSettings.operatorName,postpaid_operator_List.get(position));
                        operatorId = postpaid_operator_Code.get(position);
                    }
                    else if(rechargetype.equals("3"))
                    {
                        AppSettings.putString(AppSettings.operatorName,dth_operator_List.get(position));
                        operatorId = dth_operator_Code.get(position);
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        spinner_circle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                if(position==0)
                {
                    circle_code = "";
                }
                else
                {
                    AppSettings.putString(AppSettings.circleName,circle_List.get(position));
                    circle_code = circle_Code.get(position);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getProfileApi();
        } else {
            AppUtils.showErrorMessage(tvDth, getString(R.string.errorInternet), mActivity);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.iv_menu:
                finish();
                return;

            case R.id.imageView20:

                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, PICK_CONTACT);

                return;

            case R.id.rrUseWallet:

                if(check==0)
                {
                    check=1;
                    ivCheck.setImageResource(R.drawable.ic_check_box_selected);
                }
                else
                {
                    check=0;
                    ivCheck.setImageResource(R.drawable.ic_check_box_unselected);
                }

                return;

            case R.id.textView20:

                recharge="1";
                tvRech.setText(getString(R.string.mobile_number));
                tvRecharge.setBackgroundResource(R.color.blue);
                tvRecharge.setTextColor(Color.parseColor("#FFFFFF"));
                tvDth.setBackgroundResource(R.color.white);
                tvDth.setTextColor(Color.parseColor("#000000"));
                tvRechargeType.setVisibility(View.VISIBLE);
                v1.setVisibility(View.VISIBLE);
                spinner.setVisibility(View.VISIBLE);
                ivMobile.setVisibility(View.VISIBLE);
                edMobile.setText("");
                edAmount.setText("");
                spinner.setSelection(0);
                spinner_circle.setSelection(0);
                spinner_operator.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, blankList));

                return;

            case R.id.textView21:

                recharge="2";
                rechargetype="3";
                tvRech.setText(getString(R.string.subscriber));
                tvRecharge.setBackgroundResource(R.color.white);
                tvRecharge.setTextColor(Color.parseColor("#000000"));
                tvDth.setBackgroundResource(R.color.blue);
                tvDth.setTextColor(Color.parseColor("#FFFFFF"));
                tvRechargeType.setVisibility(View.GONE);
                v1.setVisibility(View.GONE);
                spinner.setVisibility(View.GONE);
                ivMobile.setVisibility(View.GONE);
                edMobile.setText("");
                edAmount.setText("");
                spinner_circle.setSelection(0);
                spinner_operator.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, dth_operator_List));
                spinner_operator.setSelection(0);

                return;

            case R.id.textView27:

                AppUtils.hideSoftKeyboard(mActivity);

                int amount = 0;
                try {
                    amount = Integer.parseInt(edAmount.getText().toString().trim());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                if(recharge.equals("1"))
               {
                   if(rechargetype.equals(""))
                   {
                       AppUtils.showErrorMessage(tvRecharge,getString(R.string.errorrechargetype),mActivity);
                   }
                   else  if(operatorId.equals(""))
                   {
                       AppUtils.showErrorMessage(tvRecharge,getString(R.string.errorrechargeoperator),mActivity);
                   }
                   else  if(circle_code.equals(""))
                   {
                       AppUtils.showErrorMessage(tvRecharge,getString(R.string.errorCircle),mActivity);
                   }
                   else if(edMobile.getText().toString().isEmpty())
                   {
                       AppUtils.showErrorMessage(tvRecharge,getString(R.string.errorMobileNumber),mActivity);
                   }
                   else if(amount<10||amount>1000)
                   {
                       AppUtils.showErrorMessage(tvRecharge,getString(R.string.errorAmount),mActivity);
                   }
                   else if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {

                       startRechargeApi();

                   } else {
                       AppUtils.showErrorMessage(tvRecharge, getString(R.string.errorInternet), mActivity);
                   }
               }
               else
               {
                   if(operatorId.equals(""))
                   {
                       AppUtils.showErrorMessage(tvRecharge,getString(R.string.errorrechargeoperator),mActivity);
                   }
                   else if(circle_code.equals(""))
                   {
                       AppUtils.showErrorMessage(tvRecharge,getString(R.string.errorCircle),mActivity);
                   }
                   else if(edMobile.getText().toString().isEmpty())
                   {
                       AppUtils.showErrorMessage(tvRecharge,getString(R.string.errorDth),mActivity);
                   }
                   else if(amount<10||amount>1000)
                   {
                       AppUtils.showErrorMessage(tvRecharge,getString(R.string.errorAmount),mActivity);
                   }
                   else if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {

                       startRechargeApi();

                   } else {
                       AppUtils.showErrorMessage(tvRecharge, getString(R.string.errorInternet), mActivity);
                   }
               }

                return;

        }
    }

    public class adapter_spinner extends ArrayAdapter<String> {

        ArrayList<String> data;

        public adapter_spinner(Context context, int textViewResourceId, ArrayList <String> arraySpinner_time) {

            super(context, textViewResourceId, arraySpinner_time);

            this.data = arraySpinner_time;

        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row=inflater.inflate(R.layout.spinner_textview_rel, parent, false);
            TextView label=(TextView)row.findViewById(R.id.tv_spinner_name);

            label.setText(data.get(position).toString());

            //label.setTypeface(typeface);

            return row;
        }
    }

    private void startRechargeApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("startRechargeApi", AppUrls.startRecharge);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {

            json_data.put("uuid", AppSettings.getString(AppSettings.userId));
            json_data.put("recharge_amt", edAmount.getText().toString().trim());
            json_data.put("recharge_type", rechargetype);
            json_data.put("dth_mobile_number",edMobile.getText().toString().trim());
            json_data.put("service_provider", operatorId);
            json_data.put("circle_code", circle_code);
            json_data.put("wallet", check);

            json.put(AppConstants.result, json_data);

            Log.v("startRechargeApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.startRecharge)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(edAmount, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(edAmount, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedata(JSONObject response) {

        Log.d("response ", response.toString());
        AppUtils.hideDialog();

        try {

            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);

            if (jsonObject.getString("res_code").equals("1")) {

                JSONObject object = jsonObject.getJSONObject("transaction_detial");

                AppSettings.putString(AppSettings.paymentType,"0");
                AppSettings.putString(AppSettings.transId,object.getString("txn_id"));
                AppSettings.putString(AppSettings.recAmount,edAmount.getText().toString().trim());
                AppSettings.putString(AppSettings.amount,object.getString("amount"));
                AppSettings.putString(AppSettings.wallet,object.getString("wallet_amount"));
                AppSettings.putString(AppSettings.recMobile,edMobile.getText().toString().trim());

                if(object.getString("payment_req").equals("1"))
                {
                    AppSettings.putString(AppSettings.paymentType,"1");

                    AppUtils.showErrorMessage(tvRecharge, getString(R.string.paymentRequired), mActivity);

                    AppSettings.putString(AppSettings.orderId, object.getString("txn_id"));

                    Intent intent = new Intent(mActivity, WebViewActivity.class);
                    intent.putExtra(AvenuesParams.ACCESS_CODE, Constants.ACCESS_CODE);
                    intent.putExtra(AvenuesParams.MERCHANT_ID, Constants.MERCHANT_ID);
                    intent.putExtra(AvenuesParams.BILLING_NAME, "Test");
                    intent.putExtra(AvenuesParams.BILLING_ADDRESS, "Test");
                    intent.putExtra(AvenuesParams.BILLING_CITY, "Lucknow");
                    intent.putExtra(AvenuesParams.BILLING_STATE, "Uttar Pradesh");
                    intent.putExtra(AvenuesParams.BILLING_ZIP, "226001");
                    intent.putExtra(AvenuesParams.ORDER_ID,  AppSettings.getString(AppSettings.orderId));
                    intent.putExtra(AvenuesParams.CURRENCY, "INR");
                    intent.putExtra(AvenuesParams.AMOUNT, AppSettings.getString(AppSettings.amount));
                    //intent.putExtra(AvenuesParams.AMOUNT, "1");
                    intent.putExtra(AvenuesParams.BILLING_COUNTRY, "India");

                    intent.putExtra(AvenuesParams.BILLING_TEL, AppSettings.getString(AppSettings.mobile));
                    //intent.putExtra(AvenuesParams.BILLING_EMAIL, AppSettings.getString(AppSettings.email));
                    intent.putExtra(AvenuesParams.BILLING_EMAIL, "test@gmail.com");
                    intent.putExtra(AvenuesParams.REDIRECT_URL, Constants.REDIRECT_URL);
                    intent.putExtra(AvenuesParams.CANCEL_URL, Constants.CANCEL_URL);
                    intent.putExtra(AvenuesParams.RSA_KEY_URL, Constants.RSA_KEY_URL);

                    startActivityForResult(intent, 2);
                }
                else
                {
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                        rechargeApi();
                    } else {
                        AppUtils.showErrorMessage(tvRecharge, getString(R.string.errorInternet), mActivity);
                    }
                }
            }
            else
            {
                AppUtils.showErrorMessage(edMobile, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }

        } catch (Exception e) {
            AppUtils.showErrorMessage(edAmount, String.valueOf(e), mActivity);
        }

    }

    private void rechargeApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("rechargeApi", AppUrls.rechargeApi);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {

            json_data.put("txn_id", AppSettings.getString(AppSettings.transId));

            json.put(AppConstants.result, json_data);

            Log.v("rechargeApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.rechargeApi)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseRechagreResponse(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();

                        startActivity(new Intent(getBaseContext(), RecStatusActivity.class));
                        finish();

                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(edAmount, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(edAmount, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseRechagreResponse(JSONObject response) {

        Log.d("response ", response.toString());

        try {

            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);

            if (jsonObject.getString("res_code").equals("1")) {

                startActivity(new Intent(getBaseContext(), RecStatusActivity.class));
                finish();
            }
            else
            {
                AppUtils.showErrorMessage(edMobile, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }


        } catch (Exception e) {
            AppUtils.showErrorMessage(edAmount, String.valueOf(e), mActivity);
        }

        AppUtils.hideDialog();

    }

    //code
    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (PICK_CONTACT) :
                if (resultCode == Activity.RESULT_OK) {

                    Uri contactData = data.getData();
                    Cursor c =  managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst()) {


                        String id =c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

                        String hasPhone =c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (hasPhone.equalsIgnoreCase("1")) {
                            Cursor phones = getContentResolver().query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id,
                                    null, null);
                            phones.moveToFirst();
                            String cNumber = phones.getString(phones.getColumnIndex("data1"));

                            cNumber = cNumber.replaceAll(" ","");
                            cNumber = cNumber.replaceAll("-","");

                            if(cNumber.contains("+91"))
                            {
                                String[] parts = cNumber.split("\\+91");
                                String part1 = parts[0]; // 004
                                String part2 = parts[1]; // 034556

                                cNumber = part2;
                            }
                            else if(cNumber.startsWith("0"))
                            {
                                String[] parts = cNumber.split("0");
                                String part2 = parts[1]; // 034556

                                cNumber = part2;
                            }
                            else if(cNumber.startsWith("+"))
                            {
                                String[] parts = cNumber.split("\\+");
                                String part2 = parts[1]; // 034556

                                cNumber = part2;
                            }

                            System.out.println("number is:"+cNumber);

                            edMobile.setText(cNumber);
                        }
                        String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));


                    }
                }
                break;

            case (2) :

                if(AppSettings.getString(AppSettings.status).equalsIgnoreCase("1"))
                {
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                        rechargeApi();
                    } else {
                        AppUtils.showErrorMessage(tvRecharge, getString(R.string.errorInternet), mActivity);
                    }
                }
                else
                {
                    //AppUtils.showErrorMessage(tvRecharge, "Payment Failed", mActivity);

                    if(AppSettings.getString(AppSettings.paymentType).equalsIgnoreCase("1"))
                    {
                        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                            getRefundApi();
                        } else {
                            AppUtils.showErrorMessage(tvRecharge, getString(R.string.errorInternet), mActivity);
                        }
                    }
                }

        }
    }

    private void getRefundApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("getRefundApi", AppUrls.paymentRefund);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("order_id", AppSettings.getString(AppSettings.orderId));

            json.put(AppConstants.result, json_data);

            Log.v("getRefundApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.paymentRefund)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseRefunddata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorBody());
                        Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorCode());
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvRecharge, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvRecharge, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseRefunddata(JSONObject response) {

        Log.d("response ", response.toString());
        AppUtils.hideDialog();

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(tvRecharge, String.valueOf(jsonObject.getString("res_msg")), mActivity);

                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    getProfileApi();
                } else {
                    AppUtils.showErrorMessage(tvDth, getString(R.string.errorInternet), mActivity);
                }

            }
            else
            {
                AppUtils.showErrorMessage(tvRecharge, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvRecharge, String.valueOf(e), mActivity);
        }
    }

    private void getProfileApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("getProfileApi", AppUrls.getUserDetail);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_id", AppSettings.getString(AppSettings.userId));

            json.put(AppConstants.result, json_data);

            Log.v("getProfileApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.getUserDetail)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseProfiledata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvDth, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvDth, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseProfiledata(JSONObject response) {

        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                JSONObject userObject = jsonObject.getJSONObject("user_detail");

                AppSettings.putString(AppSettings.name,userObject.getString("username"));
                AppSettings.putString(AppSettings.email,userObject.getString("email_id"));
                AppSettings.putString(AppSettings.gender,userObject.getString("gender"));
                AppSettings.putString(AppSettings.mobile,userObject.getString("phone_no"));
                AppSettings.putString(AppSettings.wallet,userObject.getString("wallet_amount"));
                AppSettings.putString(AppSettings.referralId,userObject.getString("referel_id"));

                if(!AppSettings.getString(AppSettings.wallet).equals(""))
                {
                    wallet = Float.parseFloat(AppSettings.getString(AppSettings.wallet));
                }

                if(wallet<=0)
                {
                    check=0;
                    ivCheck.setImageResource(R.drawable.ic_check_box_unselected);
                    rrUseWallet.setVisibility(View.GONE);
                }
                else
                {
                    check=1;
                    ivCheck.setImageResource(R.drawable.ic_check_box_selected);
                    rrUseWallet.setVisibility(View.VISIBLE);
                }

                tvUseWallet.setText("Use RS "+AppSettings.getString(AppSettings.wallet));
            }
            else
            {
                AppUtils.showErrorMessage(tvDth, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvDth, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
    }
}

package com.aurget.buddha.Activity.code.Database;

public class TableFeatured {
    static final String SQL_CREATE_FEATURED = ("CREATE TABLE featured (" + TableFeatured.featuredColumn.id + " VARCHAR" + ")");
    public static final String featured = "featured";

    public enum featuredColumn {
      id, category_master_id,sub_category_master_id,product_name,image,price,final_price,product_discount_type,product_discount_amount
    }

}

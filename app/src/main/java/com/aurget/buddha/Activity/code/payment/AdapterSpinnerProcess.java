package com.aurget.buddha.Activity.code.payment;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aurget.buddha.R;

import java.util.ArrayList;


public class AdapterSpinnerProcess extends ArrayAdapter<String> {

    ArrayList<String> data;
    ArrayList<String> data2;

    public AdapterSpinnerProcess(Context context, int textViewResourceId, ArrayList <String> arraySpinner_time,ArrayList <String> arraySpinner_Id) {

        super(context, textViewResourceId, arraySpinner_time);
        this.data = arraySpinner_time;
        this.data2 = arraySpinner_Id;
    }
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
    public View getCustomView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View row=inflater.inflate(R.layout.spinnerinflat, parent, false);
        TextView tvName=row.findViewById(R.id.tvDocumentName);
        Log.v("dasasss",data.get(position));
        tvName.setText(data.get(position)+" "+data2.get(position));
        Log.v("dasasss",data.get(position));
        return row;
    }
}
package com.aurget.buddha.Activity.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.Activity.Login;
import com.aurget.buddha.Activity.Main2Activity;
import com.journeyapps.barcodescanner.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public abstract class AbstrctClss {

    public abstract void responce(String s);

    public abstract void onErrorResponsee(String s);

    Activity activity;
    String url;
    String method;
    int methodInt;
    Dialog dialog = null;

    public AbstrctClss(Activity activity, final String url, String method, boolean bool) {
        this.activity = activity;
        this.url = url;
        this.method = method;
        Commonhelper.sop("AbstrctClssURL: " + url);

        if (bool) {
            dialog = Commonhelper.loadDialog(activity);
        }
        if (method.equals("p")) {
            methodInt = Request.Method.POST;
        } else {
            methodInt = Request.Method.GET;
        }

        final Request.Priority mPriority = Request.Priority.HIGH;
        StringRequest stringRequest = new StringRequest(methodInt, url,
                response -> {
                    try {
                        Commonhelper.sop("AbstrctClssRes: " + response);
                        responce(response);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Commonhelper.sop("AbstrctClssRes" + e.getMessage());
                    }
                    if (bool) {
                        if (dialog!=null)
                        dialog.dismiss();
                    }
                },
                error -> {
                    onErrorResponsee(error.getMessage());
                    Commonhelper.sop("onErrorResponse" + error.getMessage());
                    if (bool) {
                        if (dialog!=null)
                        dialog.dismiss();
                    }
                }) {
            @Override
            public Priority getPriority() {
                return mPriority;
            }

        };

        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(18000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
       /* AppController.getInstance().addToRequestQueue(stringRequest);
        stringRequest.getCacheEntry().*/
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);
        requestQueue.getCache().clear();
    }
}



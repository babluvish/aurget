package code.Adapter;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

public class BannerAdapter extends PagerAdapter {

    ArrayList<HashMap<String, String>> arrBanner;
    Context context;
    LayoutInflater inflater;

    public BannerAdapter(Context context, ArrayList<HashMap<String, String>> arrBanner) {
        this.arrBanner = arrBanner;
        this.context=context;
        inflater = LayoutInflater.from( context );
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView( (View) object );
    }

    @Override
    public int getCount() {
        return arrBanner.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View view1 = inflater.inflate( R.layout.inflate_banner, view, false );
        ImageView ivBanner = view1.findViewById( R.id.ivBanner );

        Picasso.with(this.context).load((int) R.mipmap.ic_launcher).resize(815,315).into(ivBanner);

        //Picasso.get().load( arrBanner.get( position ).get( "image" ) ).into( ivBanner );
        // Toast.makeText( mActivity, arrBanner.get( position ).get( "image" ), Toast.LENGTH_SHORT ).show();
        view.addView( view1, 0 );
        return view1;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals( object );
    }
}


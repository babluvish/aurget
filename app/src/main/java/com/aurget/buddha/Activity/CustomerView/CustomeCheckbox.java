package com.aurget.buddha.Activity.CustomerView;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatCheckBox;
import android.util.AttributeSet;

import static com.aurget.buddha.Activity.Utils.Constnt.font;

public class CustomeCheckbox  extends AppCompatCheckBox {

    private static int color= Color.RED;

    public CustomeCheckbox(Context context)
    {
        super(context);
        setFont();
        //this.setTextColor(color);
    }

    public CustomeCheckbox(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setFont();
        //this.setTextColor(color);
    }

    public CustomeCheckbox(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        setFont();
        //this.setTextColor(color);
    }

    public static void setGlobalColor(int gcolor)
    {
        color=gcolor;

    }

    private void setFont() {

        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(),font);
        setTypeface(typeface); //function used to set font

    }
}

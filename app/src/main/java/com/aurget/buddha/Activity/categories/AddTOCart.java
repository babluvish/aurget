package com.aurget.buddha.Activity.categories;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;

import com.aurget.buddha.R;

public class AddTOCart extends AppCompatActivity {

    TextView titleTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_cart);

        titleTv = findViewById(R.id.title);
        titleTv.setText("Add to Cart");
    }
}

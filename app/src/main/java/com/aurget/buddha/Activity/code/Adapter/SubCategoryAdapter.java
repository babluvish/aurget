package com.aurget.buddha.Activity.code.Adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class SubCategoryAdapter extends BaseAdapter {
    public OnClickListener catClickListener ;
    Context context;
    ArrayList<HashMap<String, String>> data;

    protected abstract void catClick(View view, String str);

    public SubCategoryAdapter(Context context, ArrayList<HashMap<String, String>> placeData) {
        this.context = context;
        this.data = placeData;

        catClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catClick(v, String.valueOf(v.getTag()));
            }
        };
    }

    public int getCount() {
        return this.data.size();
    }

    public Object getItem(int position) {
        return this.data.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView = convertView;
        rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_sub_product, null);

        ImageView ivPic, icWishlist, imageView2, imageView3;
        LinearLayout linearLayout, llCartBtn, llCartCountBtn;
        TextView tvDiscount;
        TextView tvFPrice;
        TextView tvName;
        TextView tvPrice;
        RelativeLayout btnSpinner;
        TextView tvSpinner,tvQuantityCount;
        Spinner spinners;


        linearLayout = rootView.findViewById( R.id.layout_item );
        tvQuantityCount = rootView.findViewById( R.id.tvQuantityCount );
        spinners = rootView.findViewById( R.id.spinners );
        ivPic = rootView.findViewById( R.id.ivProduct );
        icWishlist = rootView.findViewById( R.id.ic_wishlist );
        llCartCountBtn = rootView.findViewById( R.id.llCartCountBtn );
        tvName = rootView.findViewById( R.id.tvProductName );
        tvSpinner = rootView.findViewById( R.id.tvSpinner );

        llCartBtn = rootView.findViewById( R.id.llCartBtn );
        tvFPrice = rootView.findViewById( R.id.tvFPrice );
        tvPrice = rootView.findViewById( R.id.tvPrice );
        tvDiscount = rootView.findViewById( R.id.tvDiscount );
        btnSpinner = rootView.findViewById( R.id.btnSpinner );
        imageView2 = rootView.findViewById( R.id.imageView2 );
        imageView3 = rootView.findViewById( R.id.imageView3 );

       tvName.setText(data.get(position).get("product_name"));

         /* if (((HashMap) data.get( position )).get( "product_discount_type" ).equals( "1" )) {
                holder.tvDiscount.setText( "RS" + " " + ((HashMap) data.get( position )).get( "product_discount_amount" ) + " /-off" );
            } else if (((HashMap) data.get( position )).get( "product_discount_type" ).equals( "2" )) {
                holder.tvDiscount.setText( "RS" + " " + ((HashMap) data.get( position )).get( "product_discount_amount" ) + "% off" );
            } else {
                // holder.tvDiscount.setText("");
                holder.tvDiscount.setVisibility( View.INVISIBLE );
                holder.tvPrice.setVisibility( View.INVISIBLE );
            }*/

        if (((String) ((HashMap) data.get(position)).get("product_discount_type")).equals("1")) {
            tvPrice.setPaintFlags(tvPrice.getPaintFlags() | 16);
            tvDiscount.setVisibility(View.VISIBLE);
            tvPrice.setVisibility( View.VISIBLE );
          tvDiscount.setText("RS"+" "+ ((String) ((HashMap) data.get(position)).get("discount")) + "/-off");
        } else if (((String) ((HashMap) data.get(position)).get("product_discount_type")).equals("2")) {
            tvPrice.setPaintFlags(tvPrice.getPaintFlags() | 16);
            tvDiscount.setVisibility(View.VISIBLE);
            tvPrice.setVisibility( View.VISIBLE );
           tvDiscount.setText(((String) ((HashMap) data.get(position)).get("discount")) + "% off");
        } else {
            tvDiscount.setText("");
            tvDiscount.setVisibility(View.INVISIBLE);
            tvPrice.setVisibility( View.GONE );
        }
        if (((String) ((HashMap) data.get(position)).get("final_price")).equalsIgnoreCase((String) ((HashMap) data.get(position)).get("price"))) {
            tvFPrice.setText("RS" +" "+ ((String) ((HashMap) data.get(position)).get("final_price")));
            tvDiscount.setText("");
            tvDiscount.setVisibility(View.INVISIBLE);
            tvPrice.setText("");
        } else {
            tvFPrice.setText("RS"+" " + ((String) ((HashMap) data.get(position)).get("final_price")));
            tvPrice.setText("RS"+" " + ((String) ((HashMap) data.get(position)).get("price")));
           tvPrice.setPaintFlags(tvPrice.getPaintFlags() | 16);
        }
        try {
            if (((HashMap) data.get(position)).get("image") != null && !((String) ((HashMap) data.get(position)).get("image")).isEmpty()) {
                Picasso.with(context).load((String) ((HashMap) data.get(position)).get("image")).into(ivPic);
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

        linearLayout.setTag(position);
        linearLayout.setOnClickListener(this.catClickListener);
        return rootView;
    }

}

package com.aurget.buddha.Activity.Rate;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class RateActivity2 extends AppCompatActivity {

    LinearLayout horribleLL;
    LinearLayout star2LL;
    LinearLayout star3LL;
    LinearLayout star4LL;
    LinearLayout star5LL;

    ImageView horribleImg, statImg2, statImg3, statImg4, statImg5,img;
    String product_id;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rate_product);

        horribleLL = findViewById(R.id.horribleLL);
        star2LL = findViewById(R.id.star2LL);
        star3LL = findViewById(R.id.star3LL);
        star4LL = findViewById(R.id.star4LL);
        star5LL = findViewById(R.id.star5LL);

        horribleImg = findViewById(R.id.horribleImg);
        statImg2 = findViewById(R.id.statImg2);
        statImg3 = findViewById(R.id.statImg3);
        statImg4 = findViewById(R.id.statImg4);
        statImg5 = findViewById(R.id.statImg5);
        img = findViewById(R.id.img);

        if (getIntent().hasExtra("product_id")) {
            product_id = getIntent().getStringExtra("product_id");
            Picasso.with(RateActivity2.this).load(InterfaceClass.imgPth2 + product_id + "_" + 1 + "_thumb.jpg").into(img);
        }

        horribleLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horribleImg.setColorFilter(ContextCompat.getColor(RateActivity2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                hitApi("1");
            }
        });

        star2LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horribleImg.setColorFilter(ContextCompat.getColor(RateActivity2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg2.setColorFilter(ContextCompat.getColor(RateActivity2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                hitApi("2");
            }
        });

        star3LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horribleImg.setColorFilter(ContextCompat.getColor(RateActivity2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg2.setColorFilter(ContextCompat.getColor(RateActivity2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg3.setColorFilter(ContextCompat.getColor(RateActivity2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                hitApi("3");
            }
        });

        star4LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horribleImg.setColorFilter(ContextCompat.getColor(RateActivity2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg2.setColorFilter(ContextCompat.getColor(RateActivity2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg3.setColorFilter(ContextCompat.getColor(RateActivity2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg4.setColorFilter(ContextCompat.getColor(RateActivity2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                hitApi("4");
            }
        });

        star5LL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horribleImg.setColorFilter(ContextCompat.getColor(RateActivity2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg2.setColorFilter(ContextCompat.getColor(RateActivity2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg3.setColorFilter(ContextCompat.getColor(RateActivity2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg4.setColorFilter(ContextCompat.getColor(RateActivity2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                statImg5.setColorFilter(ContextCompat.getColor(RateActivity2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                hitApi("5");
            }
        });
    }

    void hitApi(final String count)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("1", product_id);
            jsonObject.put("2", "1");
            jsonObject.put("3", count);
            jsonObject.put("4", "_");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new AbstrctClss(RateActivity2.this, ConstntApi.insert_rating(RateActivity2.this, jsonObject), "g", true) {
            @Override
            public void responce(String s) {
                JSONObject jsonArray = null;
                try {
                    jsonArray = new JSONObject(s);
                    Commonhelper.showToastLong(RateActivity2.this, jsonArray.optString("message"));
                    Intent intent = new Intent(RateActivity2.this, RateActivity.class);
                    intent.putExtra("count", count);
                    intent.putExtra("product_id",product_id);
                    startActivity(intent);
                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };

    }
}

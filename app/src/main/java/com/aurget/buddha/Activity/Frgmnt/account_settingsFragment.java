package com.aurget.buddha.Activity.Frgmnt;

import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aurget.buddha.R;

public class account_settingsFragment extends Fragment {

    public account_settingsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_account_settings, container, false);

        view.findViewById(R.id.updateMobileNoRl).setOnClickListener(view1 -> dilog());

        view.findViewById(R.id.updateEMailAddressRl).setOnClickListener(view12 -> dilogEmil());

        return  view;
    }

    void dilog()
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.login2);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final EditText mobilNoEt = dialog.findViewById(R.id.mobilNoEt);

        dialog.findViewById(R.id.previous).setVisibility(View.VISIBLE);
        mobilNoEt.setHint("Enter New Mobile Number");

        final EditText otp1 = dialog.findViewById(R.id.otp1);
        final EditText otp2 = dialog.findViewById(R.id.otp2);
        final EditText otp3 = dialog.findViewById(R.id.otp3);
        final EditText otp4 = dialog.findViewById(R.id.otp4);

        final LinearLayout otpLl = dialog.findViewById(R.id.otpLl);
        final LinearLayout previousLL = dialog.findViewById(R.id.previousLL);
        final LinearLayout nextLL = dialog.findViewById(R.id.nextLL);

        final Button loginBtn = dialog.findViewById(R.id.loginBtn);
        final TextView nextBtn = dialog.findViewById(R.id.nextBtn);
        loginBtn.setText(getString(R.string.chngeMobileNumber));
        loginBtn.setVisibility(View.GONE);

        previousLL.setBackgroundResource(R.drawable.blue_light);

        previousLL.setOnClickListener(view -> {
            mobilNoEt.setHint("Enter New Mobile Number");
            nextBtn.setText(getString(R.string.continu));
            previousLL.setBackgroundResource(R.drawable.blue_light);
        });

        nextLL.setOnClickListener(view -> {
            mobilNoEt.setHint("Enter Login Password");
            nextBtn.setText(getString(R.string.update));
            previousLL.setBackgroundResource(R.drawable.ovel_shape);
        });

        loginBtn.setOnClickListener(view -> {

            if (loginBtn.getText().toString().equalsIgnoreCase("Update"))
            {
                Toast.makeText(getActivity(),"Mobile Number Updated Successfully",Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
            else {
                otpLl.setVisibility(View.VISIBLE);
                mobilNoEt.setVisibility(View.GONE);
                loginBtn.setText("Update");
            }
        });

        otp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (otp1.getText().length()==1)
                    otp2.requestFocus();

            }
        });

        otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub

                if (otp2.getText().length()==1)
                    otp3.requestFocus();
            }
        });

        otp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub

                if (otp3.getText().length()==1)
                    otp4.requestFocus();
            }
        });

        otp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub

                //if (otp4.getText().length()==1)
                //Toast.makeText(BankAccountEdit.this,"Mobile Number Updated Successfully",Toast.LENGTH_LONG).show();
            }
        });


        dialog.show();
    }

    void dilogEmil()
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.emil_edit);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final EditText mobilNoEt = dialog.findViewById(R.id.mobilNoEt);

        final EditText otp1 = dialog.findViewById(R.id.otp1);
        final EditText otp2 = dialog.findViewById(R.id.otp2);
        final EditText otp3 = dialog.findViewById(R.id.otp3);
        final EditText otp4 = dialog.findViewById(R.id.otp4);

        final LinearLayout otpLl = dialog.findViewById(R.id.otpLl);
        final Button loginBtn = dialog.findViewById(R.id.loginBtn);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (loginBtn.getText().toString().equalsIgnoreCase("Update"))
                {
                    Toast.makeText(getActivity(),"E-mail ID Updated Successfully",Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
                else {
                    otpLl.setVisibility(View.VISIBLE);
                    mobilNoEt.setVisibility(View.GONE);
                    loginBtn.setText("Update");
                }
            }
        });

        otp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (otp1.getText().length()==1)
                    otp2.requestFocus();

            }
        });

        otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub

                if (otp2.getText().length()==1)
                    otp3.requestFocus();
            }
        });

        otp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub

                if (otp3.getText().length()==1)
                    otp4.requestFocus();
            }
        });

        otp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub

                //if (otp4.getText().length()==1)
                //Toast.makeText(BankAccountEdit.this,"Mobile Number Updated Successfully",Toast.LENGTH_LONG).show();
            }
        });


        dialog.show();
    }
}

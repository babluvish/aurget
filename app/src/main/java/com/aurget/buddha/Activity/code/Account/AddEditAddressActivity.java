package com.aurget.buddha.Activity.code.Account;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.Product.CartListActivity;
import com.aurget.buddha.Activity.code.Search.SearchActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


import code.utils.AppUrls;

public class AddEditAddressActivity extends BaseActivity implements View.OnClickListener {

    //ImageView header
    ImageView ivMenu,ivSearch,ivCart;

    public static ArrayList<String> arrlistSubstation = new ArrayList<String>();
    public static ArrayList<String> arrlistSubstationID = new ArrayList<String>();
    public static ArrayList<String> arrayListState = new ArrayList<String>();
    public static ArrayList<String> arrayListStateID = new ArrayList<String>();
    //TextView
    TextView tvMain,tvCount;

    //Button
    Button btnSave;


    SearchableSpinner spinnerMainState;
    SearchableSpinner spinnerState;
    String spinnerMainSelected="";
    String state="";
    String countrynam="";
    String  strJobId="";

    ArrayList<HashMap<String,String>>stateList;
    ArrayList<String> Statelist;
    ArrayList<String> StatelistID;

    ArrayList<HashMap<String,String>>stateMainList;
    ArrayList<String> StateMainlist;
    ArrayList<String> StateMainlistID;

    adapter_spinner adapter_spinner;
    adapter_mainspinner adapter_mainspinner;
    Intent i=getIntent();
    //EditText
    EditText edName,edMobile,edAlternate,edFlat,edLocality,edCity,edPincode,edLandmark;

    //RelativeLayout
    RelativeLayout rlName,rlMobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_address);
        initialise();
        if (SimpleHTTPConnection.isNetworkAvailable(this.mActivity))
        {
           GetStateListApi();
        }
        else
        {
            AppUtils.showErrorMessage(edMobile, getString(R.string.error_connection_message), this.mActivity);
        }

        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                if (parent.getItemAtPosition(position).toString() == "select Country") {
                } else {


                    strJobId = StatelistID.get(spinnerState.getSelectedItemPosition());
                    // spinnerState.setSelection( arrayListState.indexOf( strJobId ) );


                        GetMainStateListApi(strJobId);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        } );
    spinnerMainState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
    @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getItemAtPosition(position).toString() == "select state") {
        } else {
            spinnerMainSelected = StateMainlistID.get(spinnerMainState.getSelectedItemPosition());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
});
    }

    private void initialise() {
        AppUtils.hideSoftKeyboard(mActivity);

        //Arraylist
        stateList            =new ArrayList<>();
        Statelist            =new ArrayList<>();
        StatelistID          =new ArrayList<>();

        stateMainList            =new ArrayList<>();
        StateMainlist            =new ArrayList<>();
        StateMainlistID          =new ArrayList<>();
        //RelativeLayout
        rlMobile=findViewById(R.id.rlMobile);
        rlName=findViewById(R.id.rlName);


        //Spinner
        spinnerState=findViewById(R.id.spinner);
        spinnerMainState=findViewById(R.id.spinnerState);

        //ImageView
        ivMenu =    findViewById(R.id.iv_menu);
        ivSearch =  findViewById(R.id.searchmain);
        ivCart =    findViewById(R.id.ivCart);

        //TextView
        tvCount =  findViewById(R.id.tvCount);
        tvMain =   findViewById(R.id.tvHeaderText);

        //EditText
        edName=    findViewById(R.id.edName);
        edMobile=  findViewById(R.id.edMobile);
        edAlternate=  findViewById(R.id.edAlternate);
        edFlat=       findViewById(R.id.edFlat);
        edLocality=   findViewById(R.id.edLocality);
        edCity=       findViewById(R.id.edCity);
        edPincode=    findViewById(R.id.edPincode);
        edLandmark=   findViewById(R.id.edLandmark);

        //Button
        btnSave =  findViewById(R.id.btnSave);

        ivMenu.setImageResource(R.drawable.ic_back);

        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        ivCart.setOnClickListener(this);
        btnSave.setOnClickListener(this);

        if(getIntent().getStringExtra("From").equals("1"))
        {
            tvMain.setText("Edit Addresses");
            state=getIntent().getStringExtra("state");
            countrynam=getIntent().getStringExtra("countrynam");
            Log.v("statename",state);
            /*Appsettings.getString(Appsettings.fbname);
            * AppSettings.getString(Appsettings.fbdob)*/
            edName.setText(getIntent().getStringExtra("Name"));
            edMobile.setText(getIntent().getStringExtra("PhoneNo"));
            edPincode.setText(getIntent().getStringExtra("Pincode"));
            edLocality.setText(getIntent().getStringExtra("Locality"));
            edFlat.setText(getIntent().getStringExtra("Flat"));
            edCity.setText(getIntent().getStringExtra("City"));
            edLandmark.setText(getIntent().getStringExtra("Landmark"));
            edAlternate.setText(getIntent().getStringExtra("Alternate"));
        }
        else
        {
            tvMain.setText("Add Addresses");
            edName.setText(AppSettings.getString(AppSettings.name));
            edMobile.setText(AppSettings.getString(AppSettings.mobile));
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.iv_menu:

                onBackPressed();

                return;

            case R.id.searchmain:
                startActivity(new Intent(getBaseContext(), SearchActivity.class));
                return;

            case R.id.ivCart:

                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else
                {
                    startActivity(new Intent(getBaseContext(), CartListActivity.class));
                }

                return;

            case R.id.btnSave:

                if(edName.getText().toString().isEmpty())
                {
                    AppUtils.showErrorMessage(edMobile, getString(R.string.nameError), this.mActivity);
                }
                else if(edMobile.getText().toString().isEmpty())
                {
                    AppUtils.showErrorMessage(edMobile, getString(R.string.errorMobileNumber), mActivity);
                }
                else if(edMobile.getText().toString().trim().length()<10)
                {
                    AppUtils.showErrorMessage(edMobile, getString(R.string.errorProperMobileNumber), mActivity);
                }
                else if(edFlat.getText().toString().isEmpty())
                {
                    AppUtils.showErrorMessage(edMobile, getString(R.string.flatError), this.mActivity);
                }
                else if(edLocality.getText().toString().isEmpty())
                {
                    AppUtils.showErrorMessage(edMobile, getString(R.string.localityError), this.mActivity);
                }
                else if(edCity.getText().toString().isEmpty())
                {
                    AppUtils.showErrorMessage(edMobile, getString(R.string.cityError), this.mActivity);
                }
                else if(edPincode.getText().toString().isEmpty() )
                {
                    AppUtils.showErrorMessage(edMobile, getString(R.string.pincodeError), this.mActivity);
                }  else if( edPincode.getText().toString().trim().length()<6 )
                {
                    AppUtils.showErrorMessage(edMobile, getString(R.string.pincodeError), this.mActivity);
                }
                else if(spinnerState.getSelectedItemPosition()==0) {
                        AppUtils.showErrorMessage(edMobile, "Please select city", this.mActivity);
                } else if(spinnerMainState.getSelectedItemPosition()==0) {
                        AppUtils.showErrorMessage(edMobile, getString(R.string.stateError), this.mActivity);
                    }
               else if (SimpleHTTPConnection.isNetworkAvailable(this.mActivity)) {
                            AddEditAddress();
                  } else {
                            AppUtils.showErrorMessage(edMobile, getString(R.string.error_connection_message), this.mActivity);
                    }

                return;
        }
    }

    private void AddEditAddress() {

        AppUtils.showRequestDialog(mActivity);
        Log.v("AddEditAddress", AppUrls.addNewAddress);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            if(getIntent().getStringExtra("From").equals("1"))
            {
                json_data.put("address_master_id", getIntent().getStringExtra("AddressId"));
                json_data.put("add_update_delete", "2");
            }
            else
            {
                json_data.put("address_master_id", "");
                json_data.put("add_update_delete", "1");
            }

            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json_data.put("name", edName.getText().toString().trim());
            json_data.put("phone_no",  edMobile.getText().toString().trim());
            json_data.put("pincode", edPincode.getText().toString().trim());
            json_data.put("locality",  edLocality.getText().toString().trim());
            json_data.put("area_streat_address",  edFlat.getText().toString().trim());
            json_data.put("district_city_town",  edCity.getText().toString().trim());
            json_data.put("landmark_optional",  edLandmark.getText().toString().trim());
            json_data.put("alternative_phone_optional",  edAlternate.getText().toString().trim());
            json_data.put("state_master_id",  spinnerMainSelected);
            json_data.put("countery_id", strJobId);
            json.put(AppConstants.result, json_data);
            Log.v("AddEditAddress", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.addNewAddress)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        parsedeletedata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(edAlternate, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(edAlternate, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedeletedata(JSONObject response) {
        Log.d("response ", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(edAlternate, String.valueOf(jsonObject.getString("res_msg")), mActivity);
                AppUtils.hideDialog();
                onBackPressed();
            }
            else
            {
                AppUtils.hideDialog();
                AppUtils.showErrorMessage(edAlternate, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.hideDialog();
            AppUtils.showErrorMessage(edAlternate, String.valueOf(e), mActivity);
        }

    }
    private void GetStateListApi() {
        AppUtils.hideSoftKeyboard(mActivity);
        AppUtils.showRequestDialog(mActivity);
        Log.v("GetStateListApi", AppUrls.getCountry);
        AndroidNetworking.get(AppUrls.getCountry)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata(response);
                    }
                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(edAlternate, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(edAlternate, String.valueOf(error.getErrorDetail()), mActivity);
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            //Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        }
                    }
                });
    }

    private void parseJsondata(JSONObject response) {
        Log.d("response ", response.toString());
        AppUtils.hideSoftKeyboard(mActivity);
        stateList.clear();
        StatelistID.clear();
        Statelist.clear();
       /* Statelist.add("Select Country");
        StatelistID.add("Select Country");*/
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                JSONArray bannerArray = jsonObject.getJSONArray("AllCountry");
             //   JSONArray bannerArray = jsonObject.getJSONArray("AllStates");
                for (int i = 0; i < bannerArray.length(); i++) {
                    JSONObject bannerArrayJSONObject = bannerArray.getJSONObject(i);
                    HashMap<String, String> list = new HashMap();
                    list.put("id", bannerArrayJSONObject.getString("id"));
                    list.put("name", bannerArrayJSONObject.getString("name"));
                    stateList.add(list);
                    Statelist.add(stateList.get( i ).get( "name" ));
                    StatelistID.add(stateList.get( i ).get( "id" ));
                }
                adapter_spinner=new adapter_spinner(getApplicationContext(), R.layout.spinner_textview, Statelist);
                adapter_spinner.notifyDataSetChanged();
                spinnerState.setAdapter(adapter_spinner);
                Log.v( "statefoundAt", countrynam );

                for (int i=0;i<stateList.size();i++)
                {
                    if(stateList.get( i ).get( "id" ).equalsIgnoreCase( countrynam )) {

                        Log.v( "statefound", stateList.get( i ).get( "id" ) );
                        spinnerState.setSelection( i );
                    }
                }
            } else {
                AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
    }

   /* private void parseJsondata(JSONObject response) {
        Log.d("response ", response.toString());
        AppUtils.hideSoftKeyboard(mActivity);
        arrayListState.add("Select Counttry");
        try {
           *//* arrayListState.clear();
            arrayListStateID.clear();

            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                JSONArray jsonArray = jsonObject.getJSONArray( "AllCountry");

                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject obj = jsonArray.getJSONObject( i );
                    arrayListStateID.add( obj.getString( "id" ) );
                    arrayListState.add( obj.getString( "name" ) );
                    spinnerState.setAdapter( new SubStationAdapter( mActivity, R.layout.inflatesubstation, arrayListState, arrayListStateID ) );
                }
            }*//*
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                int i;
                JSONArray bannerArray = jsonObject.getJSONArray("AllStates");
                //   JSONArray bannerArray = jsonObject.getJSONArray("AllStates");
                for (i = 0; i < bannerArray.length(); i++) {
                    JSONObject bannerArrayJSONObject = bannerArray.getJSONObject(i);
                    HashMap<String, String> list = new HashMap();
                    list.put("id", bannerArrayJSONObject.getString("id"));
                    list.put("name", bannerArrayJSONObject.getString("name"));
                    stateMainList.add(list);
                    StateMainlist.add(bannerArrayJSONObject.getString("name"));
                    StateMainlistID.add(bannerArrayJSONObject.getString("id"));
                }
                int j=0;
                for (int k=0;k<StateMainlist.size();k++)
                {
                    if(state.equals(StateMainlist.get(k)))
                    {
                        j=k;
                        Log.v("8900", String.valueOf(j));
                        break;
                    }
                }

                // spinnerState.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, Statelist));
                adapter_spinner=new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, StateMainlist);
                adapter_spinner.notifyDataSetChanged();
                spinnerMainState.setAdapter(adapter_spinner);
                spinnerMainState.setSelection(j);

            } else {
                AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
    }*/



    private void GetMainStateListApi(String strJobId) {
        AppUtils.hideSoftKeyboard(mActivity);
        AppUtils.showRequestDialog(mActivity);
        Log.v("GetStateListApi", AppUrls.getState);
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonmain = new JSONObject();
        try {
             jsonObject.put("Country_id", strJobId);
             jsonmain.put("result",jsonObject);

            Log.v("######", String.valueOf(jsonmain));
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.getState)
                .addJSONObjectBody(jsonmain) // posting json
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {
                            parseMainJsondata(response);
                        }
                        catch (Exception exception)
                        {
                        }
                    }
                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(edAlternate, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(edAlternate, String.valueOf(error.getErrorDetail()), mActivity);
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            //Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        }
                    }
                });
    }

    private void parseMainJsondata(JSONObject response) {
        Log.d("response ", response.toString());
        AppUtils.hideSoftKeyboard(mActivity);
        stateMainList.clear();
        StateMainlist.clear();
        StateMainlistID.clear();
        try {

            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                JSONArray bannerArray = jsonObject.getJSONArray("AllStates");
                for (int i = 0; i < bannerArray.length(); i++) {
                    JSONObject bannerArrayJSONObject = bannerArray.getJSONObject(i);
                    HashMap<String, String> list = new HashMap();
                    list.put("id", bannerArrayJSONObject.getString("id"));
                    list.put("name", bannerArrayJSONObject.getString("name"));
                    stateMainList.add(list);
                    StateMainlist.add(stateMainList.get( i ).get( "name" ));
                    StateMainlistID.add(stateMainList.get( i ).get( "id" ));
                }

                adapter_mainspinner=new adapter_mainspinner(getApplicationContext(), R.layout.spinner_textview, StateMainlist);
                adapter_mainspinner.notifyDataSetChanged();
                spinnerMainState.setAdapter(adapter_mainspinner);
                Log.v( "statefoundAt", countrynam );

                for (int i=0;i<stateMainList.size();i++)
                {
                    if(stateMainList.get( i ).get( "id" ).equalsIgnoreCase( state )) {

                        Log.v( "statefound", stateMainList.get( i ).get( "id" ) );
                        spinnerMainState.setSelection( i );
                    }
                }
            } else {
                AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }

        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
    }


    @Override
    protected void onResume() {
        super.onResume();

        int count = Integer.parseInt(AppSettings.getString(AppSettings.cartCount));

        if(count>0)
        {
            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(String.valueOf(count));
        }
        else
        {
            tvCount.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(getBaseContext(), AddressListActivity.class));
        finish();
    }
    public class adapter_spinner extends ArrayAdapter<String> {
        ArrayList<String> data;

        public adapter_spinner(Context context, int textViewResourceId, ArrayList <String> arraySpinner_time) {

            super(context, textViewResourceId, arraySpinner_time);

            this.data = arraySpinner_time;

        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row=inflater.inflate(R.layout.spinner_textview_rel, parent, false);
            TextView label=row.findViewById(R.id.tv_spinner_name);

            label.setText(data.get(position).toString());
            //label.setTypeface(typeface);
            return row;
        }
    }



    public class adapter_mainspinner extends ArrayAdapter<String> {
        ArrayList<String> data;

        public adapter_mainspinner(Context context, int textViewResourceId, ArrayList <String> arraySpinner_time) {

            super(context, textViewResourceId, arraySpinner_time);

            this.data = arraySpinner_time;

        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row=inflater.inflate(R.layout.spinner_textview_rel, parent, false);
            TextView label=row.findViewById(R.id.tv_spinner_name);

            label.setText(data.get(position).toString());
            //label.setTypeface(typeface);
            return row;
        }
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

}

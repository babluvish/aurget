package com.aurget.buddha.Activity.code.Fragment;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

public class BannerAddapter extends PagerAdapter {

    private  ArrayList<HashMap<String, String>> images;

    private LayoutInflater inflater;

    private Context context;


    public BannerAddapter(Context context, ArrayList<HashMap<String, String>> images) {

        this.context = context;
        this.images=images;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {

        View myImageLayout = inflater.inflate(R.layout.bannerlayout, view, false);
        final ImageView myImage =  myImageLayout.findViewById(R.id.img_banner);
        Log.v("imageurl", String.valueOf(AppConstants.pagerlistd));
        Picasso.with(context).load(images.get(position).get("name")).into(myImage);
        view.addView(myImageLayout);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {

        return  view.equals(o);
    }

}

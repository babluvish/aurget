package com.aurget.buddha.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.Utils.CustomPreference;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONObject;

public class ShareApp extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        String id = CustomPreference.readString(mActivity, CustomPreference.e_id, "");

        if (id.equalsIgnoreCase("")) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(mActivity, Login.class);
                    startActivity(intent);
                    finish();
                }
            }, 3000);
        } else {
            handler.postDelayed(() -> dashboardApi(), 0);
        }
    }

    private void dashboardApi()
    {
        new AbstrctClss(mActivity, ConstntApi.DashboardApi(mActivity), "g", false) {
            @Override
            public void responce(String s) {
                try {
                    JSONObject jsonArray = new JSONObject(s);
                    JSONArray product_list_latest  = jsonArray.optJSONArray("product_list_latest");
                    JSONArray product_list_set_most_view  = jsonArray.optJSONArray("product_list_set_most_view");
                    JSONArray product_list_set_recent = jsonArray.optJSONArray("product_list_set_recent");

                    JSONArray get_banners = jsonArray.optJSONArray("get_banners");
                    JSONArray allcategory = jsonArray.optJSONArray("allcategory");
                    JSONArray deal_product = jsonArray.optJSONArray("deal_product");
                    JSONArray product_list_set = jsonArray.optJSONArray("product_list_set");
                    JSONArray product_list_set_recent_all = jsonArray.optJSONArray("product_list_set_recent_all");

                    Constnt.product_list_latest.clear();
                    Constnt.product_list_set_most_view.clear();
                    Constnt.recentViewJAl.clear();
                    Constnt.get_banners.clear();
                    Constnt.deal_product.clear();
                    Constnt.all_category.clear();
                    Constnt.product_list_set.clear();
                    Constnt.product_list_set_recent_all.clear();
                    Constnt.get_sliders=null;
                    Constnt.get_profile=null;

                    Constnt.get_profile  = jsonArray.optJSONArray("get_profile");
                    Constnt.get_sliders = jsonArray.optJSONArray("get_sliders");

                    for (int i = 0; i < product_list_latest.length(); i++) {
                        Constnt.product_list_latest.add(product_list_latest.getJSONObject(i));
                    }

                    for (int i = 0; i < product_list_set_most_view.length(); i++) {
                        Constnt.product_list_set_most_view.add(product_list_set_most_view.getJSONObject(i));
                    }

                    for (int i = 0; i < product_list_set_recent.length(); i++) {
                        Constnt.recentViewJAl.add(product_list_set_recent.getJSONObject(i));
                    }
                    for (int i = 0; i < get_banners.length(); i++) {
                        Constnt.get_banners.add(get_banners.getJSONObject(i));
                    }

                    for (int i = 0; i < allcategory.length(); i++) {
                        Constnt.all_category.add(allcategory.getJSONObject(i));
                    }
                    for (int i = 0; i < deal_product.length(); i++) {
                        Constnt.deal_product.add(deal_product.getJSONObject(i));
                    }

                    for (int i = 0; i < product_list_set.length(); i++) {
                        Constnt.product_list_set.add(product_list_set.getJSONObject(i));
                    }

                    for (int i = 0; i < product_list_set_recent_all.length(); i++) {
                        Constnt.product_list_set_recent_all.add(product_list_set_recent_all.getJSONObject(i));
                    }

                    handler.postDelayed(() -> {
                        Intent intent_ = getIntent();
                        if (intent_ != null && intent_.getData() != null)
                        {
                            Intent intent = new Intent(mActivity, Main2Activity.class);
                            intent.putExtra("ShareApp1",intent_.getData().toString());
                            startActivity(intent);
                            finish();
                        }
                    },0);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                Commonhelper.showToastLong(mActivity,"Please check net connection");
            }
        };
    }


    Handler handler = new Handler();
}

package com.aurget.buddha.Activity.SolderManagment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.Activity.Login;
import com.aurget.buddha.Activity.Main2Activity;
import com.aurget.buddha.Activity.Service.AppSignatureHashHelper;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.Utils.CustomPreference;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class AddSolderActivity extends BaseActivity {

    LinearLayout previous;
    LinearLayout next;
    LinearLayout sponsorLL;
    LinearLayout memberLL, previous2Ll;
    LinearLayout bankDetailsLl;
    LinearLayout mobilNoLl, next2;
    LinearLayout previous2, previousChild, fillSponsorIDLl;

    TextView titleTv, nextBtn, termAndServiceTv, privecyPolicyTv, sponserIDSetTv, resendOtp;
    EditText dobEt, mobile_noEt, emailEt, lastNameEt, firstNameEt, sponserNameEt, sponserIDTv, dateofBirth;
    ImageView tickImg;
    RadioButton male, female, other;
    TextView nextBtn2, greenCheckImg;
    CheckBox turmConChk;

    EditText otp1;
    EditText otp2;
    EditText otp3;
    EditText otp4;

    private ClipboardManager clipboardManager;
    private ClipData clipData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_solder);

        titleTv = findViewById(R.id.titleTv);

        dobEt = findViewById(R.id.dobEt);
        mobile_noEt = findViewById(R.id.mobile_noEt);
        emailEt = findViewById(R.id.emailEt);
        lastNameEt = findViewById(R.id.lastNameEt);
        firstNameEt = findViewById(R.id.firstNameEt);
        sponserNameEt = findViewById(R.id.sponserNameEt);
        sponserIDTv = findViewById(R.id.sponserIDTv);
        dateofBirth = findViewById(R.id.dateofBirth);
        previous2Ll = findViewById(R.id.previous2Ll);
        male = findViewById(R.id.male);
        female = findViewById(R.id.female);
        sponserIDSetTv = findViewById(R.id.sponserIDSetTv);
        other = findViewById(R.id.other);
        greenCheckImg = findViewById(R.id.greenCheckImg);
        nextBtn2 = findViewById(R.id.nextBtn2);

        nextBtn2.setText("Get Verification Code");

        next2 = findViewById(R.id.next2);
        previous = findViewById(R.id.previous);
        next = findViewById(R.id.next);
        nextBtn = findViewById(R.id.nextBtn);
        turmConChk = findViewById(R.id.turmConChk);
        fillSponsorIDLl = findViewById(R.id.fillSponsorIDLl);

        sponsorLL = findViewById(R.id.sponsorLL);
        memberLL = findViewById(R.id.memberLL);
        bankDetailsLl = findViewById(R.id.bankDetailsLl);
        mobilNoLl = findViewById(R.id.mobilNoLl);
        tickImg = findViewById(R.id.tickImg);
        previous2 = findViewById(R.id.previous2);
        previousChild = findViewById(R.id.previousChild);

        termAndServiceTv = findViewById(R.id.termAndServiceTv);
        privecyPolicyTv = findViewById(R.id.privecyPolicyTv);

        clipboardManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // previous.setBackgroundTintList(getResources().getColorStateList(R.color.blueLight));
        }

        Commonhelper.hyperLink(termAndServiceTv, "terms of service", "http://www.aurget.com/home/page/Notice");
        Commonhelper.hyperLink(privecyPolicyTv, "privacy policy", "https://www.aurget.com/home/legal/privacy_policy");

        sponserIDSetTv.setOnClickListener(v -> {
            autoFillSponsorID = true;
            sponserIDTv.setText("9662101102");
        });

        next.setOnClickListener(view -> {
            if (!validation()) {
                return;
            }
            count++;
            if (count == 2) {
                if (!validation__()) {
                    count = 1;
                    return;
                }
            }

            Log.i("count++", String.valueOf(count));

            if (count == 1) {
                titleTv.setText(getResources().getString(R.string.solderDetails));
                next.setEnabled(true);
                nextBtn.setText(getString(R.string.continu));
                tickImg.setImageResource(R.drawable.ic_arrow_forward_black_24dp);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    previousChild.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                }

                previous.setEnabled(true);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    next.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                    previous.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                }

                memberLL.setVisibility(View.VISIBLE);
                sponsorLL.setVisibility(View.GONE);
                bankDetailsLl.setVisibility(View.GONE);
                mobilNoLl.setVisibility(View.GONE);
                fillSponsorIDLl.setVisibility(View.GONE);
                previous.setVisibility(View.VISIBLE);
                previous2.setVisibility(View.GONE);
                previous2Ll.setVisibility(View.GONE);
            }

            if (count == 2) {
                mobilNoLl.setVisibility(View.VISIBLE);
                previous2.setVisibility(View.VISIBLE);
                previous2Ll.setVisibility(View.VISIBLE);
                previous.setVisibility(View.GONE);
                memberLL.setVisibility(View.GONE);
                sponsorLL.setVisibility(View.GONE);
                bankDetailsLl.setVisibility(View.GONE);
                //hitPI();
                Log.i("fff++", "dfd");
            }
        });

        previous.setOnClickListener(view -> {
            count--;
            Log.i("count++", String.valueOf(count));

            if (count == 1) {

                next.setEnabled(true);
                nextBtn.setText(getString(R.string.continu));
                tickImg.setImageResource(R.drawable.ic_arrow_forward_black_24dp);
                //previous.setBackgroundResource(R.color.colorPrimary);
                previous.setEnabled(true);

                //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //  next.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                //previous.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                //}

                memberLL.setVisibility(View.VISIBLE);
                sponsorLL.setVisibility(View.GONE);
                bankDetailsLl.setVisibility(View.GONE);
                mobilNoLl.setVisibility(View.GONE);
                previous.setVisibility(View.VISIBLE);
                previous2.setVisibility(View.GONE);
            } else {

                fillSponsorIDLl.setVisibility(View.VISIBLE);

                titleTv.setText(getResources().getString(R.string.sponsordetils));
                nextBtn.setText(getString(R.string.continu));
                tickImg.setImageResource(R.drawable.ic_arrow_forward_black_24dp);
                next.setEnabled(true);
                previous.setEnabled(false);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    next.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                    // previous.setBackgroundTintList(getResources().getColorStateList(R.color.blueLight));
                }

                bankDetailsLl.setVisibility(View.GONE);
                memberLL.setVisibility(View.GONE);
                sponsorLL.setVisibility(View.VISIBLE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    // previousChild.setBackgroundTintList(getResources().getColorStateList(R.color.blueLight2));
                }
            }
        });


        previous2Ll.setOnClickListener(view -> {
            previous2Ll.setVisibility(View.GONE);
            previous.performClick();
        });

        sponserIDTv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (sponserIDTv.getText().toString().length() == 10)
                    getSponsorNameByMobileNo(sponserIDTv.getText().toString());
            }
        });


        dateofBirth.setOnFocusChangeListener((view, b) -> {
            if (b) {
                piecker();
            }
        });

        dateofBirth.setOnClickListener(view -> piecker());

        next2.setOnClickListener(view -> {
            if (nextBtn2.getText().toString().equalsIgnoreCase(getResources().getString(R.string.submit))) {
                String id = CustomPreference.readString(AddSolderActivity.this, CustomPreference.e_id, "");
                if (id.equals("")) {
                    //hitPIBeforeLogin();
                } else {
                    //hitPi();
                }
            } else {
                if (!validation___()) {
                    return;
                }
                if (!turmConChk.isChecked()) {
                    Commonhelper.showToastLong(AddSolderActivity.this, "I Agree Checkbox for AurGet terms of service & privacy policy.");
                    return;
                }
                //hitPi();
                verifyMobileNO(mobile_noEt.getText().toString(), 0);
            }
            //verify_otp();
            //hitPI();
        });
    }

    void verify_otp()
    {
        customeDialog = new Dialog(this);
        customeDialog.setContentView(R.layout.login2);
        customeDialog.setCanceledOnTouchOutside(false);
        customeDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final EditText mobilNoEt = customeDialog.findViewById(R.id.mobilNoEt);
        resendOtp = customeDialog.findViewById(R.id.resendOtp);

        //mobilNoEt.setVisibility(View.VISIBLE);
        //otp1 = dialog.findViewById(R.id.otp1);
        otp1 = customeDialog.findViewById(R.id.otp1);
        otp2 = customeDialog.findViewById(R.id.otp2);
        otp3 = customeDialog.findViewById(R.id.otp3);
        otp4 = customeDialog.findViewById(R.id.otp4);

        //otpLl = dialog.findViewById(R.id.otpLl);
        Button loginBtn = customeDialog.findViewById(R.id.loginBtn);

        loginBtn.setText("Verify & Submit");

        Commonhelper.countDown(resendOtp);
        //loginBtn.setText("Get OTP");

        resendOtp.setOnClickListener(view -> {

            if (resendOtp.getText().toString().equals("Resend OTP"))
                verifyMobileNO(mobile_noEt.getText().toString(), 1);
            //new GetWeatherTask().execute(ConstntApi.send_otp_login(Login.this,mobEt.getText().toString()));
            //otpLogin(mobEt.getText().toString(),mobilNoEt.getText().toString());
        });

        customeDialog.findViewById(R.id.closeLl).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                customeDialog.dismiss();
                //otpLogin(mobEt.getText().toString(),mobilNoEt.getText().toString());
            }
        });

        loginBtn.setOnClickListener(view -> {

            String otp = otp1.getText().toString() + otp2.getText().toString() + otp3.getText().toString() + otp4.getText().toString();

            //Commonhelper.showToastLong(Login.this, otp);

            if (otp.length() < 4) {
                Commonhelper.showToastLong(AddSolderActivity.this, "Enter OTP");
                return;
            }

            verify_otpApi(mobile_noEt.getText().toString(), otp);
        });

        otp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (otp1.getText().length() == 1)
                    otp2.requestFocus();

            }
        });

        otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub

                if (otp2.getText().length() == 1)
                    otp3.requestFocus();
            }
        });

        otp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub

                if (otp3.getText().length() == 1)
                    otp4.requestFocus();
            }
        });

        otp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub

            }
        });

        customeDialog.show();
        clipboardManager.setText("");
        startTimer();
    }

    /*public void verify_otp()
    {
        dialog = new Dialog(AddSolderActivity.this);
        dialog.setContentView(R.layout.verify_otp);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        final EditText otpEt = dialog.findViewById(R.id.otpEt);
        final LinearLayout viewBtn = dialog.findViewById(R.id.next2);

        viewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (otpEt.getText().toString().length()==0)
                {
                    Commonhelper.showToastLong(AddSolderActivity.this,"Please Enter OTP");
                    return;
                }
                verify_otp(mobile_noEt.getText().toString(),otpEt.getText().toString());
            }
        });

        dialog.show();
    }*/

    private void verifyMobileNO(final String pnoneNo, final int i) {
        Commonhelper.sop("verifyMobileNO"+"verifyMobileNO");
        //http://aurget.com/app/send_otp.php?mobile_number=9235720162
        new AbstrctClss(AddSolderActivity.this, ConstntApi.send_otp(pnoneNo), "g", true) {
            @Override
            public void responce(String s) {
                JSONObject jsonArray = null;
                try {
                    jsonArray = new JSONObject(s);
                    String jsonObject = jsonArray.optString("status");

                    if (s.contains("Max OTP limit reached, please try after 15 minutes")) {
                        Commonhelper.showToastLong(AddSolderActivity.this, "Max OTP limit reached, please try after 15 minutes");
                        return;
                    }

                    if (jsonObject.equalsIgnoreCase("success")) {
                        if (i == 0) {
                            verify_otp();
                        } else {
                            otp1.setText("");
                            otp2.setText("");
                            otp3.setText("");
                            otp4.setText("");
                            otp1.requestFocus();
                            Commonhelper.countDown(resendOtp);
                            Commonhelper.sop("otp=" + jsonArray.optString("otp"));
                            Commonhelper.showToastLong(AddSolderActivity.this, jsonArray.optString("message"));
                        }
                    } else {
                        Commonhelper.showToastLong(AddSolderActivity.this, jsonArray.optString("message"));
                    }
                } catch (Exception e) { //9235720166
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }

    private void verify_otpApi(String pnoneNo, String otp) {
        Commonhelper.sop("verify_otpApi"+"verify_otpApi");
        //http://aurget.com/app/send_otp.php?mobile_number=9235720162
        new AbstrctClss(AddSolderActivity.this, ConstntApi.verify_otp(pnoneNo, otp), "g", true) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s);
                    String jsonObject = jsonArray.optJSONObject(0).optString("Status");

                    if (jsonObject.equalsIgnoreCase("true")) {
                        Commonhelper.showToastLong(AddSolderActivity.this, "OTP verified Successfully");
                        greenCheckImg.setVisibility(View.VISIBLE);
                        //nextBtn2.setText(getResources().getString(R.string.submit));
                        //next2.performClick();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run()
                            {
                                String id = CustomPreference.readString(AddSolderActivity.this, CustomPreference.e_id, "");
                                if (id.equals("")) {
                                    hitPIBeforeLogin();
                                } else {
                                    hitPi();
                                }
                            }
                        },1000);
                    } else {
                        Commonhelper.showToastLong(AddSolderActivity.this, "Invalid OTP");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    private void getSponsorNameByMobileNo(String pnoneNo) {
        //http://aurget.com/budhaa/User/sponsor_name?contact_no=9662101102
        new AbstrctClss(AddSolderActivity.this, ConstntApi.sponsor_name(pnoneNo), "p", true) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String sponsor_name = jsonObject.optString("sponsor_name");
                    sponserNameEt.setText(sponsor_name);
                    if (autoFillSponsorID) {
                        next.performClick();
                        autoFillSponsorID = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    sponserNameEt.setHint(sponserNameEt.getHint());
                    Commonhelper.showToastLong(AddSolderActivity.this, "Sponsor's number is not registered");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                sponserNameEt.setHint(sponserNameEt.getHint());
                Commonhelper.showToastLong(AddSolderActivity.this, "Sponsor's number is not registered");
            }
        };
    }

    boolean validation() {
        boolean b = false;
        if (sponserIDTv.getText().toString().length() == 0) {
            Commonhelper.showToastLong(AddSolderActivity.this, "Enter sponsor ID");
            b = false;
        } else if (sponserIDTv.getText().toString().length() != 10) {
            Commonhelper.showToastLong(AddSolderActivity.this, "Please enter the correct sponsor number");
            b = false;
        } else if (sponserNameEt.getText().toString().trim().length() == 0) {
            Commonhelper.showToastLong(AddSolderActivity.this, "Sponsor's number is not registered");
            b = false;
        } else {
            b = true;
        }
        return b;
    }

    boolean validation__() {
        boolean b = false;

        if (sponserIDTv.getText().toString().length() != 10) {
            Commonhelper.showToastLong(AddSolderActivity.this, "Please enter the correct sponsor number");
            b = false;
        }
        if (sponserIDTv.getText().toString().length() == 0) {
            Commonhelper.showToastLong(AddSolderActivity.this, "Enter sponsor ID");
            b = false;
        } else if (sponserNameEt.getText().toString().length() == 0) {
            Commonhelper.showToastLong(AddSolderActivity.this, "Enter sponsor Name");
            b = false;
        } else if (firstNameEt.getText().toString().length() == 0) {
            Commonhelper.showToastLong(AddSolderActivity.this, "Enter First Name");
            b = false;
        } else if (lastNameEt.getText().toString().length() == 0) {
            Commonhelper.showToastLong(AddSolderActivity.this, "Enter Last Name");
            b = false;
        } else if (dateofBirth.getText().toString().length() == 0) {
            Commonhelper.showToastLong(AddSolderActivity.this, "Enter Date of Birth");
            b = false;
        }

//        else if (emailEt.getText().toString().length()==0)
//        {
//            Commonhelper.showToastLong(AddSolderActivity.this,"Enter Email-ID");
//            b = false;
//        }

//        else if (mobile_noEt.getText().toString().length()==0)
//        {
//            Commonhelper.showToastLong(AddSolderActivity.this,"Enter Mobile Number");
//            b = false;
//        }
        else {
            b = true;
        }

        return b;
    }

    boolean validation___() {
        boolean b = false;
        if (mobile_noEt.getText().toString().length() != 10) {
            Commonhelper.showToastLong(AddSolderActivity.this, "Enter mobile No.");
            b = false;
        }
        else {
            b = true;
        }

        return b;
    }

    private void hitPi() {
        //Commonhelper.showToastLong(AddSolderActivity.this, "after login");
        Commonhelper.sop("afterLogin");
        final Dialog dialog = Commonhelper.loadDialog(AddSolderActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, InterfaceClass.after_login_registration
                + "username=" + firstNameEt.getText().toString()
                + "&email=" + emailEt.getText().toString()
                + "&phone=" + mobile_noEt.getText().toString()
                + "&surname=" + lastNameEt.getText().toString()
                + "&e_id=" + CustomPreference.readString(AddSolderActivity.this, CustomPreference.e_id, "")
                + "&sponsor=" + sponserIDTv.getText().toString(),
                response -> {
                    try {
                        String Res = response;
                        Log.d("dsd", Res);

                        JSONArray jsonArray = new JSONArray(Res);

                        if (jsonArray.optJSONObject(0).optString("Status").equalsIgnoreCase("true")) {

                            String mob = CustomPreference.readString(AddSolderActivity.this, CustomPreference.mobileNO, "");
                            String name = CustomPreference.readString(AddSolderActivity.this, CustomPreference.userName, "");

                            if (mob.equals("")) {
                                CustomPreference.writeString(AddSolderActivity.this, CustomPreference.mobileNO, mobile_noEt.getText().toString());
                            }

                            if (name.equals("")) {
                                CustomPreference.writeString(AddSolderActivity.this, CustomPreference.userName, firstNameEt.getText().toString() + " " + lastNameEt.getText().toString());
                            }

                            Intent intent = new Intent(AddSolderActivity.this, Main2Activity.class);
                            startActivity(intent);
                            ActivityCompat.finishAffinity(AddSolderActivity.this);
                            Commonhelper.showToastLong(AddSolderActivity.this, getString(R.string.success_registration));
                            //customeDialog.dismiss();
                            //customeDialog = null;
                            //finish();
                        } else {
                            Commonhelper.showToastLong(AddSolderActivity.this, jsonArray.optJSONObject(0).optString("message"));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("dsd", "sssss");
                    }
                    dialog.dismiss();
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        Commonhelper.showToastLong(AddSolderActivity.this, getString(R.string.serverError));
                    }
                }) {
        };

        RequestQueue requestQueue = Volley.newRequestQueue(AddSolderActivity.this);
        requestQueue.add(stringRequest);
    }

    void hitPIBeforeLogin()
    {
        //https://aurget.com/Api/Registration?username=sss&email=bobo@gmf&phone=9511185853&surname=sss&password1=1234&sponsor=4445
        String url = InterfaceClass.ipAddress4 + "registration?username=" + firstNameEt.getText().toString()
                + "&email=" + emailEt.getText().toString()
                + "&phone=" + mobile_noEt.getText().toString()
                + "&surname=" + lastNameEt.getText().toString() + "&password1=00&sponsor=" + sponserIDTv.getText().toString();

        new AbstrctClss(AddSolderActivity.this, ConstntApi.beforLogin(mActivity,url), "g", true) {
            @Override
            public void responce(String s)
            {
                JSONArray jsonArray = null;
                try
                {
                    String Res = s;

                    if (Res.trim().length() == 0) {
                        Commonhelper.showToastLong(AddSolderActivity.this, "No response from server end");
                        return;
                    }

                    jsonArray = new JSONArray(Res);

                    if (jsonArray.optJSONObject(0).optString("Status").equalsIgnoreCase("true")) {

                        CustomPreference.writeString(AddSolderActivity.this, CustomPreference.e_id, jsonArray.optJSONObject(0).optString("e_id"));
                        CustomPreference.writeString(AddSolderActivity.this, CustomPreference.mobileNO, mobile_noEt.getText().toString());
                        CustomPreference.writeString(AddSolderActivity.this, CustomPreference.userName, firstNameEt.getText().toString() + " " + lastNameEt.getText().toString());

                        //Intent intent = new Intent(AddSolderActivity.this, Main2Activity.class);
                        //startActivity(intent);
                        //ActivityCompat.finishAffinity(AddSolderActivity.this);
                        dashboardApi();
                        //Commonhelper.showToastLong(AddSolderActivity.this, getString(R.string.success_registration));
                    } else {
                        /*customeDialog.dismiss();
                        customeDialog = null;
                        finish();*/
                        Commonhelper.showToastLong(AddSolderActivity.this, jsonArray.optJSONObject(0).optString("message"));
                    }
                } catch (Exception e) {
                    Commonhelper.showToastLong(AddSolderActivity.this, Constnt.notFound);
                }
            }
            @Override
            public void onErrorResponsee(String s) {
                Commonhelper.showToastLong(AddSolderActivity.this, s);
            }
        };
        //Commonhelper.showToastLong(AddSolderActivity.this, "before login");
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("strt", "distroy");
    }


    private void piecker() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        dateofBirth.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    int count = 0;
    private int mYear, mMonth, mDay, mHour, mMinute;

    private void SignUpApi(String type) {
        AppUtils.showRequestDialog(AddSolderActivity.this);
        Log.v("SignUpApi", code.utils.AppUrls.SignUp);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            if (type.equals("1")) {
                json_data.put("phone_no", mobile_noEt.getText().toString());
                json_data.put("email_id", emailEt.getText().toString());
                json_data.put("username", firstNameEt.getText().toString() + lastNameEt.getText().toString());
                json_data.put("password", "-");
                json_data.put("gender", "1");
                json_data.put("dob", "_");
                json_data.put("googleid", "");
                json_data.put("fb_id", "");
                json_data.put("user_type_login", type);
                json_data.put("device_id", AppUtils.getDeviceID(getApplicationContext()));
            } else {
                json_data.put("email_id", emailEt.getText().toString());
                json_data.put("username", firstNameEt.getText().toString() + lastNameEt.getText().toString());
                json_data.put("password", "");
                json_data.put("gender", "1");
                json_data.put("dob", "1");
                json_data.put("googleid", "");
                json_data.put("fb_id", "");
                json_data.put("user_type_login", "1");
                json_data.put("device_id", AppUtils.getDeviceID(getApplicationContext()));
            }
            json.put(AppConstants.result, json_data);
            Log.v("SignUpApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(code.utils.AppUrls.SignUp)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(String.valueOf(error.getErrorCode()), AddSolderActivity.this);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(String.valueOf(error.getErrorCode()), AddSolderActivity.this);
                        }
                    }
                });
    }

    private void parseJsondata(JSONObject response) {
        Log.d("response ", response.toString());
        AppUtils.hideDialog();
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                //Toast.makeText(this, jsonObject.getString("res_msg"), Toast.LENGTH_LONG).show();
                AppSettings.putString(AppSettings.name, jsonObject.getString("username"));
                AppSettings.putString(AppSettings.email, jsonObject.getString("email_id"));
                AppSettings.putString(AppSettings.gender, jsonObject.getString("gender"));
                AppSettings.putString(AppSettings.dob, jsonObject.getString("dob"));
                AppSettings.putString(AppSettings.mobile, jsonObject.getString("phone_no"));
                AppSettings.putString(AppSettings.userId, jsonObject.getString("id"));
                //startActivity(new Intent(getBaseContext(), DashBoardFragment.class));
                Intent intent = new Intent(AddSolderActivity.this, Main2Activity.class);
                startActivity(intent);
                ActivityCompat.finishAffinity(AddSolderActivity.this);
                Toast.makeText(AddSolderActivity.this, getString(R.string.success_registration), Toast.LENGTH_LONG).show();
                Log.v("12398948954", "jfjjjjgjjgh");
            } else {
                AppUtils.showErrorMessage(jsonObject.getString("res_msg"), AddSolderActivity.this);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(String.valueOf(e), AddSolderActivity.this);
        }
    }

    private void dashboardApi()
    {
        new AbstrctClss(mActivity, ConstntApi.DashboardApi(mActivity), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    JSONObject jsonArray = new JSONObject(s);
                    JSONArray product_list_latest  = jsonArray.optJSONArray("product_list_latest");
                    JSONArray product_list_set_most_view  = jsonArray.optJSONArray("product_list_set_most_view");
                    JSONArray product_list_set_recent = jsonArray.optJSONArray("product_list_set_recent");

                    JSONArray get_banners = jsonArray.optJSONArray("get_banners");
                    JSONArray allcategory = jsonArray.optJSONArray("allcategory");
                    JSONArray deal_product = jsonArray.optJSONArray("deal_product");
                    JSONArray product_list_set = jsonArray.optJSONArray("product_list_set");
                    JSONArray product_list_set_recent_all = jsonArray.optJSONArray("product_list_set_recent_all");

                    Constnt.product_list_latest.clear();
                    Constnt.product_list_set_most_view.clear();
                    Constnt.recentViewJAl.clear();
                    Constnt.get_banners.clear();
                    Constnt.product_list_set.clear();
                    Constnt.product_list_set_recent_all.clear();
                    Constnt.deal_product.clear();
                    Constnt.all_category.clear();
                    Constnt.get_sliders=null;
                    Constnt.get_profile=null;

                    Constnt.get_profile  = jsonArray.optJSONArray("get_profile");
                    Constnt.get_sliders = jsonArray.optJSONArray("get_sliders");

                    for (int i = 0; i < product_list_latest.length(); i++) {
                        Constnt.product_list_latest.add(product_list_latest.getJSONObject(i));
                    }

                    for (int i = 0; i < product_list_set_most_view.length(); i++) {
                        Constnt.product_list_set_most_view.add(product_list_set_most_view.getJSONObject(i));
                    }

                    for (int i = 0; i < product_list_set_recent.length(); i++) {
                        Constnt.recentViewJAl.add(product_list_set_recent.getJSONObject(i));
                    }
                    for (int i = 0; i < get_banners.length(); i++) {
                        Constnt.get_banners.add(get_banners.getJSONObject(i));
                    }

                    for (int i = 0; i < allcategory.length(); i++) {
                        Constnt.all_category.add(allcategory.getJSONObject(i));
                    }

                    for (int i = 0; i < deal_product.length(); i++) {
                        Constnt.deal_product.add(deal_product.getJSONObject(i));
                    }

                    for (int i = 0; i < product_list_set.length(); i++) {
                        Constnt.product_list_set.add(product_list_set.getJSONObject(i));
                    }

                    for (int i = 0; i < product_list_set_recent_all.length(); i++) {
                        Constnt.product_list_set_recent_all.add(product_list_set_recent_all.getJSONObject(i));
                    }

                    Intent intent = new Intent(AddSolderActivity.this, Main2Activity.class);
                    startActivity(intent);
                    ActivityCompat.finishAffinity(AddSolderActivity.this);
                    Commonhelper.showToastLong(AddSolderActivity.this, getString(R.string.success_registration));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                Commonhelper.showToastLong(mActivity,"Please check net connection");
            }
        };
    }


    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 1000, 1000); //
    }

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        try
                        {
                            Commonhelper.sop("oooo");
                            ClipData pData = clipboardManager.getPrimaryClip();

                            if (clipboardManager.hasPrimaryClip()) {
                                Commonhelper.sop("oooo000");
                            }

                            ClipData.Item item = pData.getItemAt(0);
                            String txtpaste = item.getText().toString().substring(0);
                            String txtpaste2 = item.getText().toString().substring(1);
                            String txtpaste3 = item.getText().toString().substring(2);
                            String txtpaste4 = item.getText().toString().substring(3);

                            otp1.setText(txtpaste);
                            otp2.setText(txtpaste2);
                            otp3.setText(txtpaste3);
                            otp4.setText(txtpaste4);

                            stoptimertask();

                        } catch (Exception e) {

                        }
                    }
                });
            }
        };
    }

    private AppSignatureHashHelper appSignatureHashHelper;
    private Dialog customeDialog;
    boolean autoFillSponsorID = false;

    Timer timer;
    TimerTask timerTask;

    //we are going to use a handler to be able to run in our TimerTask
    final Handler handler = new Handler();
}

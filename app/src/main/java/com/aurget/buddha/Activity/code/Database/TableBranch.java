package com.aurget.buddha.Activity.code.Database;

public class TableBranch {
    static final String SQL_CREATE_BRANCH = ("CREATE TABLE branch (" +
            branchCatColumn.branch_id + " VARCHAR,"
            + branchCatColumn.branch_name + " VARCHAR,"
            + branchCatColumn.schoolId + " VARCHAR,"
            + branchCatColumn.status + " VARCHAR" + ")");
    public static final String branch = "branch";

    public enum branchCatColumn {
        branch_id,
        branch_name,
        schoolId,
        status
    }
}

package com.aurget.buddha.Activity.code.Main;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Fragment.DashBoardFragment;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.R;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import code.utils.AppUrls;

import static com.aurget.buddha.Activity.code.utils.AppUtils.isEmailValid;

public class SignUpActivity extends BaseActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    //Edittext
    EditText etPassword, etEmail, etName, etReenterPassword, etDob, etmobile;

    //Button
    Button btnSignUp;

    //LinearLayout
    LinearLayout llFacebook, llGoogle;

    //RelativeLayout
    RelativeLayout rlBack;

    //Textview
    TextView tvPasswordInstruction, tvPasswordInstruction1;

    //ImageView
    ImageView ivCheck, ivLogo;

    //Spinner
    Spinner spinnerGender;


    //Integer
    int status = 0;

    //String
    String facebook_id, name, email, google_id;
    String social = "";

    //Arraylist
    ArrayList<String> genderList;

    // For DatePicker
    private SimpleDateFormat dateFormatter;
    private DatePickerDialog fromDatePickerDialog;


    //CallbackManager callbackManager;
    private static final int RC_SIGN_IN = 9001;
    private GoogleApiClient mGoogleApiClient;
    public static GoogleSignInOptions gso;
    private boolean signedInUser;
    private boolean mIntentInProgress;
    private ConnectionResult mConnectionResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);

        //Edittext
        etPassword = findViewById(R.id.etPassword);
        etEmail = findViewById(R.id.etEmail);
        etName = findViewById(R.id.etName);
        etReenterPassword = findViewById(R.id.etReenterPassword);
        etDob = findViewById(R.id.etDob);

        //Button
        btnSignUp = findViewById(R.id.btnSignUp);
        etmobile = findViewById(R.id.etmobile);

        //Imageview
        ivCheck = findViewById(R.id.ivCheck);
        ivLogo = findViewById(R.id.ivLogo);

        //LinearLayout
        llFacebook = findViewById(R.id.llFacebook);
        llGoogle = findViewById(R.id.llGoogle);

        //Textview
        tvPasswordInstruction = findViewById(R.id.tvPasswordInstruction);
        tvPasswordInstruction1 = findViewById(R.id.tvPasswordInstruction1);

        //RelativeLayout
        rlBack = findViewById(R.id.rlBack);


        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        setDateTimeField();


        //   etPassword.requestFocusFromTouch();

        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean hasfocus) {
                if (hasfocus) {
                    //tvPasswordInstruction.setVisibility(View.VISIBLE);
                } else {
                    //tvPasswordInstruction.setVisibility(View.GONE);
                }
            }
        });


        etReenterPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean hasfocus) {
                if (hasfocus) {
                    tvPasswordInstruction1.setVisibility(View.VISIBLE);
                } else {
                    tvPasswordInstruction1.setVisibility(View.GONE);
                }
            }
        });


        genderList = new ArrayList<>();
        genderList.add("Select Gender");
        genderList.add("Male");
        genderList.add("Female");

        spinnerGender = findViewById(R.id.SpinnerGender);
        spinnerGender.setAdapter(new spinnerAdapter(getApplicationContext(), R.layout.spinner_layout, (ArrayList<String>) genderList));

        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tvPasswordInstruction.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        try {
            Googleplus();
        } catch (Exception e) {

        }
        //Calling Facebook
        //facebook();


        //SetonClickListener
        etPassword.setOnClickListener(this);
        llFacebook.setOnClickListener(this);
        llGoogle.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
        ivCheck.setOnClickListener(this);
        etDob.setOnClickListener(this);
        rlBack.setOnClickListener(this);
        ivLogo.setOnClickListener(this);

    }

    private void setDateTimeField() {
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                etDob.setText(dateFormatter.format(newDate.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
    }

    //check vaildation for password
    public boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
                finish();
                break;
            case R.id.ivLogo:
                finish();
                break;
            case R.id.etDob:
                tvPasswordInstruction.setVisibility(View.GONE);
                tvPasswordInstruction1.setVisibility(View.GONE);
                fromDatePickerDialog.show();
                break;
            case R.id.btnSignUp:
                tvPasswordInstruction.setVisibility(View.GONE);
                tvPasswordInstruction1.setVisibility(View.GONE);
//                if (etmobile.getText().toString().isEmpty()) {
//                    Toast.makeText(this, "Please enter mobile number", Toast.LENGTH_SHORT).show();
//                } else if (etmobile.getText().toString().trim().length() != 10) {
//                    Toast.makeText(this, "Please enter valid mobile number", Toast.LENGTH_SHORT).show();

                if (etName.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please enter your name", Toast.LENGTH_SHORT).show();
                }
                else if (etEmail.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please enter email id", Toast.LENGTH_SHORT).show();
                }
                else if (!isEmailValid(etEmail.getText().toString()))
                {
                    Toast.makeText(this, "Please enter valid email id", Toast.LENGTH_SHORT).show();
                }

                    //else if (etPassword.getText().toString().isEmpty()) {
                    //Toast.makeText(this, "Please enter your password", Toast.LENGTH_SHORT).show();
                 /*else if (etPassword.getText().toString().length() < 7) {
                    Toast.makeText(this, "Please enter minimum 7 digit password", Toast.LENGTH_SHORT).show();
                } else if (!isValidPassword(etPassword.getText().toString().trim())) {
                    Toast.makeText(this, "Please enter valid password", Toast.LENGTH_SHORT).show();
                }*/
              /*  else if (etReenterPassword.getText().toString().isEmpty())
                {
                    Toast.makeText(this, "Please enter your password", Toast.LENGTH_SHORT).show();
                }
                else if (etReenterPassword.getText().toString().length()<7)
                {
                    Toast.makeText(this, "Please enter minimum 7 digit password", Toast.LENGTH_SHORT).show();
                }
                else if (!isValidPassword(etReenterPassword.getText().toString().trim()))
                {
                    Toast.makeText(this, "Please enter valid password", Toast.LENGTH_SHORT).show();
                }
                else if (!etPassword.getText().toString().equals(etReenterPassword.getText().toString()))
                {
                    Toast.makeText(this, "Retype password value not matched with Password value", Toast.LENGTH_SHORT).show();
                }
                else if (etDob.getText().toString().isEmpty())
                {
                    Toast.makeText(this, "Please select your date of birth", Toast.LENGTH_SHORT).show();
                }*/
                /*else if (spinnerGender.getSelectedItem().equals("Select Gender")) {
                    Toast.makeText(this, "Please select your gender", Toast.LENGTH_SHORT).show();
                } else if (ivCheck.getDrawable().equals(getResources().getDrawable(R.drawable.ic_check_box_unselected))) {
                    Toast.makeText(this, "You have to agree with our upload terms in order to continue.", Toast.LENGTH_SHORT).show();
                }*/ else {
                    if (SimpleHTTPConnection.isNetworkAvailable(this.mActivity)) {
                        SignUpApi("1");
                    } else {
                        AppUtils.showErrorMessage(etEmail, getString(R.string.error_connection_message), this.mActivity);
                    }
                }

                break;
            case R.id.llFacebook:
                //LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email"));
                break;

            case R.id.llGoogle:
                signIn();
                break;

            case R.id.ivCheck:
                if (status == 0) {
                    ivCheck.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_box_selected));
                    status = 1;
                } else {
                    ivCheck.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_box_unselected));
                    status = 0;
                }

                break;


        }

    }


    //=====================================================Google Plus==================================================//
    public void Googleplus() {
//        //GooglePlus
//        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestEmail()
//                .build();
//
//
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(this, this)
//                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                .build();
//
//        mGoogleApiClient.connect();
//        super.onStart();


    }

    private void signIn() {

//        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
//        startActivityForResult(signInIntent, RC_SIGN_IN);
//
//        if (!mGoogleApiClient.isConnecting()) {
//            signedInUser = true;
//
//        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Facebook
        //callbackManager.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//
//            if (result.isSuccess()) {
//
//                GoogleSignInAccount acct = result.getSignInAccount();
//
//                name = acct.getDisplayName();
//                email = acct.getEmail();
//                google_id = acct.getId();
//                social = "1";
//
//
//                if (SimpleHTTPConnection.isNetworkAvailable(this.mActivity)) {
//                    SignUpApi("2");
//                } else {
//                    AppUtils.showErrorMessage(etEmail, getString(R.string.error_connection_message), this.mActivity);
//                }
//
//                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
//                        new ResultCallback<Status>() {
//                            @Override
//                            public void onResult(Status status) {
//                                mGoogleApiClient.disconnect();
//                            }
//                        });
//
//            }

        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (!connectionResult.hasResolution()) {

            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this, 0).show();

            return;

        }

        if (!mIntentInProgress) {

            // store mConnectionResult

            mConnectionResult = connectionResult;

            if (signedInUser) {


            }

        }
    }


    //===========================Facebook=============================//
    /*public void facebook() {

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {


            @Override
            public void onSuccess(LoginResult loginResult) {
                //loader.show();
                facebookSuccess(loginResult);


            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }


        });


    }*/

    /*public void facebookSuccess(LoginResult loginResult) {
        AccessToken accessToken = loginResult.getAccessToken();
        com.facebook.Profile profile = com.facebook.Profile.getCurrentProfile();

        if (profile != null) {
            facebook_id = profile.getId();
            name = profile.getName();

        }
        // Facebook Email address
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        Log.v("LoginActivity Response ", response.toString());

                        try {


                            name = object.getString("name");
                            email = object.getString("email");
                            facebook_id = object.getString("id");
                            social = "2";

                            if (SimpleHTTPConnection.isNetworkAvailable(SignUpActivity.this)) {
                                SignUpApi("2");
                            } else {
                                AppUtils.showErrorMessage(etEmail, getString(R.string.error_connection_message), SignUpActivity.this);
                            }


                            LoginManager.getInstance().logOut();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });


        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }*/


    //=============================SpinnerAdapter===================================//
    public class spinnerAdapter extends ArrayAdapter<String> {

        ArrayList<String> data;

        public spinnerAdapter(Context context, int textViewResourceId, ArrayList<String> arraySpinner_time) {

            super(context, textViewResourceId, arraySpinner_time);

            this.data = arraySpinner_time;

        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.spinner_layout, parent, false);
            TextView label = (TextView) row.findViewById(R.id.tvName);
            //   label.setTypeface(typeface);
            label.setText(data.get(position));

            return row;
        }
    }


    //======================================API==========================================================//
    private void SignUpApi(String type) {

        AppUtils.showRequestDialog(mActivity);

        Log.v("SignUpApi", AppUrls.SignUp);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            if (type.equals("1")) {
                json_data.put("phone_no", getIntent().getStringExtra("mobile"));
                json_data.put("email_id", etEmail.getText().toString());
                json_data.put("username", etName.getText().toString());
                json_data.put("password", etPassword.getText().toString());

                /*if (spinnerGender.getSelectedItem().toString().equals("Male")) {
                    json_data.put("gender", "1");
                } else {
                    json_data.put("gender", "2");
                }*/

                json_data.put("gender", "1");
                json_data.put("dob","_");
                json_data.put("googleid", "");
                json_data.put("fb_id", "");
                json_data.put("user_type_login", type);
                json_data.put("device_id", AppUtils.getDeviceID(getApplicationContext()));
            } else {
                if (social.equals("1")) {
                    json_data.put("email_id", email);
                    json_data.put("username", name);
                    json_data.put("password", "");
                    json_data.put("gender", "");
                    json_data.put("dob", "");
                    json_data.put("googleid", google_id);
                    json_data.put("fb_id", "");
                    json_data.put("user_type_login", type);
                    json_data.put("device_id", AppUtils.getDeviceID(getApplicationContext()));
                } else {
                    json_data.put("email_id", email);
                    json_data.put("username", name);
                    json_data.put("password", "");
                    json_data.put("gender", "");
                    json_data.put("dob", "");
                    json_data.put("googleid", "");
                    json_data.put("fb_id", facebook_id);
                    json_data.put("user_type_login", type);
                    json_data.put("device_id", AppUtils.getDeviceID(getApplicationContext()));
                }

            }
            json.put(AppConstants.result, json_data);
            Log.v("SignUpApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.SignUp)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(etEmail, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(etEmail, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }
    private void parseJsondata(JSONObject response) {

        Log.d("response ", response.toString());
        AppUtils.hideDialog();
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                Toast.makeText(this, jsonObject.getString("res_msg"), Toast.LENGTH_LONG).show();
                AppSettings.putString(AppSettings.name, jsonObject.getString("username"));
                AppSettings.putString(AppSettings.email, jsonObject.getString("email_id"));
                AppSettings.putString(AppSettings.gender, jsonObject.getString("gender"));
                AppSettings.putString(AppSettings.dob, jsonObject.getString("dob"));
                AppSettings.putString(AppSettings.mobile, jsonObject.getString("phone_no"));
                AppSettings.putString(AppSettings.userId, jsonObject.getString("id"));
                startActivity(new Intent(getBaseContext(), DashBoardFragment.class));
                Log.v("12398948954", "jfjjjjgjjgh");
            } else {
                AppUtils.showErrorMessage(etEmail, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(etEmail, String.valueOf(e), mActivity);
        }
    }
}

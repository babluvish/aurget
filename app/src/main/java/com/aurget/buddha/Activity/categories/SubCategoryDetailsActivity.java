package com.aurget.buddha.Activity.categories;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Paint;

import com.aurget.buddha.Activity.Main2Activity;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.aurget.buddha.Activity.AddtoCart.AddCartListActivity;
import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.EmptyCartActivity;
import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.Activity.SearchData;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.aurget.buddha.Activity.Commonhelper.getScreenWidth;
import static com.aurget.buddha.Activity.Main2Activity.crtDetils2;
import static com.aurget.buddha.Activity.Utils.Constnt.recentViewJAl;
import static java.lang.Double.parseDouble;

public class SubCategoryDetailsActivity extends AppCompatActivity {

    NavigationView nav_view;
    RangeSeekBar rangeSeekbar = null;
    TextView minVlTv;
    TextView maxVlTv, populerTv, crtCountTv;
    TextView sortTv, filterTv, newestTv;
    LinearLayout progressLl, brandLl, sizeLl, vendorLl;
    RelativeLayout priceRl;
    RecyclerView brndRv, vendorRv, recycleViewCenter, recentViewRv, brandRv;
    Button applyBtn;
    RangeSeekBar<Integer> seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category_details);

        rangeSeekbar = findViewById(R.id.rangeSeekbar);
        brandRv = findViewById(R.id.brandRv);
        nav_view = findViewById(R.id.nav_view);
        populerTv = findViewById(R.id.populerTv);
        minVlTv = findViewById(R.id.minVlTv);
        maxVlTv = findViewById(R.id.maxVlTv);
        sortTv = findViewById(R.id.sortTv);
        filterTv = findViewById(R.id.filterTv);
        progressLl = findViewById(R.id.progressLl);
        brandLl = findViewById(R.id.brandLl);
        vendorRv = findViewById(R.id.vendorRv);
        sizeLl = findViewById(R.id.sizeLl);
        vendorLl = findViewById(R.id.vendorLl);
        recycleViewCenter = findViewById(R.id.recycleViewCenter);
        recentViewRv = findViewById(R.id.recentViewRv);
        newestTv = findViewById(R.id.newestTv);
        applyBtn = findViewById(R.id.applyBtn);
        crtCountTv = findViewById(R.id.crtCountTv);

        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(SubCategoryDetailsActivity.this, LinearLayoutManager.VERTICAL, false);
        brandRv.setLayoutManager(horizontalLayoutManager);
        brandRv.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager manager = new LinearLayoutManager(SubCategoryDetailsActivity.this, LinearLayoutManager.VERTICAL, false);
        vendorRv.setLayoutManager(manager);
        vendorRv.setAdapter(new MyRecyclerAdapterVendor(null));

        GridLayoutManager manager2 = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recycleViewCenter.setLayoutManager(manager2);

        GridLayoutManager manager3 = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recentViewRv.setLayoutManager(manager3);

        ArrayList<String> stringArrayList = new ArrayList<>();
        stringArrayList.add("Armani");
        stringArrayList.add("Leecopper");
        stringArrayList.add("Adidas clothing/design/style");

        brandRv.setAdapter(new MyRecyclerAdapterBrnd(null));
        maxVlTv.setText("₹35500");

        if (getIntent().hasExtra("search")) {
            searchData(getIntent().getStringExtra("search"));
        } else {
            if (getIntent().hasExtra("banner_id"))
                hitPi2(getIntent().getStringExtra("banner_id"));
            else
                hitPi(getIntent().getStringExtra("category_id"), getIntent().getStringExtra("subcategory_id"));
        }

        if (getIntent().hasExtra("subcategory_id")) {
            filterApi();
            filterApi2();
        }

        findViewById(R.id.bkImg).setOnClickListener(view -> {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.openDrawer(GravityCompat.START);
        });

        findViewById(R.id.priceRl).setOnClickListener(view -> {
            if (findViewById(R.id.rangeSeekbarLL).getVisibility() == View.GONE) {
                findViewById(R.id.rangeSeekbarLL).setVisibility(View.VISIBLE);
                applyBtn.setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.rangeSeekbarLL).setVisibility(View.GONE);
                applyBtn.setVisibility(View.GONE);
            }
        });

        findViewById(R.id.crtContner).setOnClickListener(view -> {
            if (Integer.parseInt(crtCountTv.getText().toString().trim().replace("+","")) < 1) {
                Intent intent = new Intent(SubCategoryDetailsActivity.this, EmptyCartActivity.class);
                startActivity(intent);
                return;
            }
            Intent intent = new Intent(SubCategoryDetailsActivity.this, AddCartListActivity.class);
            intent.putExtra("res", crtDetils2);
            startActivity(intent);
        });

        applyBtn.setOnClickListener(view -> {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.END);
            price_range(minVlTv.getText().toString().replace("₹", ""), maxVlTv.getText().toString().replace("₹", ""));
        });

        findViewById(R.id.searchEt).setOnClickListener(view -> {
            Intent intent = new Intent(SubCategoryDetailsActivity.this, SearchData.class);
            startActivity(intent);
        });

        findViewById(R.id.brandTv).setOnClickListener(view -> {
            if (brandRv.getVisibility() == View.GONE) {
                //findViewById(R.id.search_barBrand).setVisibility(View.VISIBLE);
                brandRv.setVisibility(View.VISIBLE);
                applyBtn.setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.search_barBrand).setVisibility(View.GONE);
                brandRv.setVisibility(View.GONE);
                applyBtn.setVisibility(View.GONE);
            }
        });

        findViewById(R.id.vendorTv).setOnClickListener(view -> {
            if (vendorRv.getVisibility() == View.GONE) {
                //findViewById(R.id.search_barVender).setVisibility(View.VISIBLE);
                vendorRv.setVisibility(View.VISIBLE);
                applyBtn.setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.search_barVender).setVisibility(View.GONE);
                vendorRv.setVisibility(View.GONE);
                applyBtn.setVisibility(View.GONE);
            }
        });

        findViewById(R.id.populerTv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                recentViewPi();
            }
        });

        newestTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                newest();
            }
        });

        findViewById(R.id.oldestTv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                oldest();
            }
        });

        findViewById(R.id.lowToHighTv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                price_low();
            }
        });

        findViewById(R.id.priceHighTv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                price_high();
            }
        });

        sortTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // sortTv.setBackgroundResource(R.drawable.line2);
                //filterTv.setBackgroundResource(R.drawable.line);
//
//                BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(SubCategoryDetailsActivity.this);
//                bottomSheetDialog.setContentView(R.layout.sort_dilog);
//                bottomSheetDialog.show();

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.openDrawer(GravityCompat.START);

            }
        });

        filterTv.setOnClickListener(view -> {
//                sortTv.setBackgroundResource(R.drawable.line);
//                filterTv.setBackgroundResource(R.drawable.line2);
//
//                final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(SubCategoryDetailsActivity.this);
//                bottomSheetDialog.setContentView(R.layout.filter_dilog);
//                bottomSheetDialog.show();
//
//                TextView priceTv = bottomSheetDialog.findViewById(R.id.priceTv);
//                TextView brandTv = bottomSheetDialog.findViewById(R.id.brandTv);
//                TextView sizeTv = bottomSheetDialog.findViewById(R.id.sizeTv);
//                TextView vendorTv = bottomSheetDialog.findViewById(R.id.vendorTv);
//
//                priceTv.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        progressLl.setVisibility(View.VISIBLE);
//                        brandLl.setVisibility(View.GONE);
//                        sizeLl.setVisibility(View.GONE);
//                        vendorLl.setVisibility(View.GONE);
//
//                        bottomSheetDialog.dismiss();
//                    }
//                });
//
//                sizeTv.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        progressLl.setVisibility(View.GONE);
//                        brandLl.setVisibility(View.GONE);
//                        sizeLl.setVisibility(View.VISIBLE);
//                        vendorLl.setVisibility(View.GONE);
//                        bottomSheetDialog.dismiss();
//                    }
//                });
//
//                brandTv.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        progressLl.setVisibility(View.GONE);
//                        brandLl.setVisibility(View.VISIBLE);
//                        sizeLl.setVisibility(View.GONE);
//                        vendorLl.setVisibility(View.GONE);
//                        bottomSheetDialog.dismiss();
//                    }
//                });
//
//                vendorTv.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        progressLl.setVisibility(View.GONE);
//                        brandLl.setVisibility(View.GONE);
//                        sizeLl.setVisibility(View.GONE);
//                        vendorLl.setVisibility(View.VISIBLE);
//                        bottomSheetDialog.dismiss();
//                    }
//                });

            DrawerLayout drawer = findViewById(R.id.drawer_layout);

            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);
            } else {
                drawer.openDrawer(GravityCompat.END);
            }
        });

        rangeSeekbar.setNotifyWhileDragging(true);

        rangeSeekbar.setOnRangeSeekBarChangeListener((bar, minValue, maxValue) -> {
            minVlTv.setText("₹" + minValue);
            maxVlTv.setText("₹" + maxValue);
        });

        seekBar = new RangeSeekBar<Integer>(this);
        findViewById(R.id.bkImg).setOnClickListener(v -> finish());

        crtDetils();
    }

    private void crtDetils() {
        new AbstrctClss(SubCategoryDetailsActivity.this, ConstntApi.get_cart_details(SubCategoryDetailsActivity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    try {
                        crtDetils2 = Res;
                        Log.d("crtDetils", crtDetils2);
                        JSONArray jsonArray = new JSONArray(crtDetils2);
                        if (jsonArray.length() > 9)
                            crtCountTv.setText("9+");
                        else crtCountTv.setText("" + jsonArray.length());
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("letestFeturePi", "sssss");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    private void filterApi() {
        new AbstrctClss(SubCategoryDetailsActivity.this, ConstntApi.data_filter(SubCategoryDetailsActivity.this, getIntent().getStringExtra("subcategory_id")), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                        }

                        brandRv.setAdapter(new MyRecyclerAdapterBrnd(jsonObjectArrayList));
                        //seekBar.setRangeValues(0, 35500);

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("letestFeturePi", "sssss");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    private void filterApi2() {
        new AbstrctClss(SubCategoryDetailsActivity.this, ConstntApi.data_filterrate(SubCategoryDetailsActivity.this, getIntent().getStringExtra("subcategory_id")), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    try {
                        JSONObject jsonObject = new JSONObject(s);
                        JSONObject jsonArray = jsonObject.optJSONObject("data");

                        int max = jsonArray.optInt("maxs");
                        int min = jsonArray.optInt("minimum");
                        seekBar.setRangeValues(min, max);
                        //seekBar.setSelectedMaxValue(max);

                        minVlTv.setText("₹" + min);
                        maxVlTv.setText("₹" + max);

                        vendorRv.setAdapter(new MyRecyclerAdapterVendor(jsonArray.optString("vendor_name")));

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("letestFeturePi", "sssss");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    public class MyRecyclerAdapterBrnd extends RecyclerView.Adapter<MyRecyclerAdapterBrnd.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterBrnd(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapterBrnd.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.brnd_item, parent, false);
            return new MyRecyclerAdapterBrnd.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapterBrnd.ViewHolder holder, int position) {
            holder.checkbox.setText(jsonObjectal.get(position).optString("brand_name"));
        }

        @Override
        public int getItemCount() {
            try {
                return jsonObjectal.size();
            } catch (Exception e) {
                return 0;
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private AppCompatCheckBox checkbox;

            public ViewHolder(View itemView) {
                super(itemView);
                checkbox = itemView.findViewById(R.id.checkbox);
            }
        }
    }

    public class MyRecyclerAdapterVendor extends RecyclerView.Adapter<MyRecyclerAdapterVendor.ViewHolder> {

        private ArrayList<String> jsonObjectal;
        private String res;

        public MyRecyclerAdapterVendor(String res) {
            //this.jsonObjectal = jsonObjectal;
            this.res = res;
        }

        @Override
        public MyRecyclerAdapterVendor.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.brnd_item, parent, false);
            return new MyRecyclerAdapterVendor.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapterVendor.ViewHolder holder, int position) {
            holder.checkbox.setText(res);
        }

        @Override
        public int getItemCount() {
            return 1;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            CheckBox checkbox;

            public ViewHolder(View itemView) {
                super(itemView);

                checkbox = itemView.findViewById(R.id.checkbox);
            }
        }
    }


    public class MyRecyclerAdapterProduct extends RecyclerView.Adapter<MyRecyclerAdapterProduct.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterProduct(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapterProduct.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.subctegory_product_item, parent, false);
            v.getLayoutParams().width = (int) (getScreenWidth(SubCategoryDetailsActivity.this) / 2.0f);
            return new MyRecyclerAdapterProduct.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapterProduct.ViewHolder holder, int position) {
            //double disAmt = discountAmt(parseDouble(jsonObjectal.get(position).optString("sale_price")), parseDouble(jsonObjectal.get(position).optString("discount")));
            //long value = roudOff(disAmt);
            holder.pricetv.setText("₹" + jsonObjectal.get(position).optString("sale_price"));
            holder.cutPriceTv.setText("₹" + jsonObjectal.get(position).optString("cut_amount"));
            //holder.percTv.setText("" + jsonObjectal.get(position).optString("discount") + "% Off");
            holder.ipTv.setText("IP: " + jsonObjectal.get(position).optString("ip"));

            if (parseDouble(jsonObjectal.get(position).optString("discount")) > 0) {
                Commonhelper.isDiscountRate(jsonObjectal, position, holder.percTv);
                holder.percTv.setVisibility(View.VISIBLE);
                holder.cutPriceTv.setVisibility(View.VISIBLE);
            } else {
                holder.percTv.setVisibility(View.GONE);
                holder.cutPriceTv.setVisibility(View.GONE);
            }

            //holder.rangeSeekbarTv.setText("("+jsonObjectal.get(position).optString("rating_total")+")");
            //holder.rangeSeekbar.setRating((float) 3.0);

            int i = Integer.parseInt(jsonObjectal.get(position).optString("product_sub_id"));
            Picasso.with(SubCategoryDetailsActivity.this).load(InterfaceClass.imgPth2 + jsonObjectal.get(position).optString("product_id") + "_" + i + "_thumb.jpg").into(holder.img);
            Log.i("googlee", InterfaceClass.imgPth2 + jsonObjectal.get(position).optString("product_id") + "_" + i + "_thumb.jpg");
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            ImageView img;
            TextView pricetv, cutPriceTv, percTv, ipTv, rangeSeekbarTv, soldTv;
            LinearLayout llContner;
            RatingBar rangeSeekbar;

            public ViewHolder(View itemView) {
                super(itemView);

                pricetv = itemView.findViewById(R.id.pricetv);
                cutPriceTv = itemView.findViewById(R.id.cutPriceTv);
                percTv = itemView.findViewById(R.id.percTv);
                ipTv = itemView.findViewById(R.id.ipTv);
                rangeSeekbarTv = itemView.findViewById(R.id.rangeSeekbarTv);
                soldTv = itemView.findViewById(R.id.soldTv);
                img = itemView.findViewById(R.id.img);
                rangeSeekbar = itemView.findViewById(R.id.rangeSeekbar);

                llContner = itemView.findViewById(R.id.llContner);
                cutPriceTv.setPaintFlags(cutPriceTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                llContner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       /* int pos = getAdapterPosition()+1;
                        Intent intent = new Intent(SubCategoryDetailsActivity.this,SubCategoryDetails2.class);
                        intent.putExtra("res",jsonObjectal.get(getAdapterPosition()).toString());
                        intent.putExtra("image",InterfaceClass.imgPth2+jsonObjectal.get(getAdapterPosition()).optString("product_id")+"_"+pos+"_thumb.jpg");
                        startActivity(intent);*/

                        Intent intent = new Intent(SubCategoryDetailsActivity.this, SubCategoryDetails2.class);
                        intent.putExtra("res", jsonObjectal.get(getAdapterPosition()).toString());
                        int pos = getAdapterPosition() + 1;
                        intent.putExtra("imgCount", jsonObjectal.get(getAdapterPosition()).optString("num_of_imgs"));
                        intent.putExtra("product_id", jsonObjectal.get(getAdapterPosition()).optString("product_id"));
                        intent.putExtra("image", InterfaceClass.imgPth2 + jsonObjectal.get(getAdapterPosition()).optString("product_id") + "_" + pos + "_thumb.jpg");
                        startActivity(intent);
                    }
                });
            }
        }
    }

    void newest() {
        //http://aurget.com/api/sorting?sort=condition_new&sub_category_id=132
        new AbstrctClss(SubCategoryDetailsActivity.this, ConstntApi.condition_new(SubCategoryDetailsActivity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    JSONArray jsonArray = new JSONArray(s);

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }

                    recycleViewCenter.setAdapter(new MyRecyclerAdapterProduct(jsonObjectArrayList));
                    recentViewRv.setVisibility(View.GONE);
                    recycleViewCenter.setVisibility(View.VISIBLE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    void oldest() {
        final Dialog dialog = Commonhelper.loadDialog(SubCategoryDetailsActivity.this);
        new AbstrctClss(SubCategoryDetailsActivity.this, ConstntApi.condition_old(SubCategoryDetailsActivity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    JSONArray jsonArray = new JSONArray(s);

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }

                    recycleViewCenter.setAdapter(new MyRecyclerAdapterProduct(jsonObjectArrayList));
                    recentViewRv.setVisibility(View.GONE);
                    recycleViewCenter.setVisibility(View.VISIBLE);

                    dialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    void price_low() {
        final Dialog dialog = Commonhelper.loadDialog(SubCategoryDetailsActivity.this);
        new AbstrctClss(SubCategoryDetailsActivity.this, ConstntApi.price_low(SubCategoryDetailsActivity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    JSONArray jsonArray = new JSONArray(s);

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }

                    recycleViewCenter.setAdapter(new MyRecyclerAdapterProduct(jsonObjectArrayList));
                    recentViewRv.setVisibility(View.GONE);
                    recycleViewCenter.setVisibility(View.VISIBLE);

                    dialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    void price_high() {
        final Dialog dialog = Commonhelper.loadDialog(SubCategoryDetailsActivity.this);
        new AbstrctClss(SubCategoryDetailsActivity.this, ConstntApi.price_high(SubCategoryDetailsActivity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    JSONArray jsonArray = new JSONArray(s);

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }

                    recycleViewCenter.setAdapter(new MyRecyclerAdapterProduct(jsonObjectArrayList));
                    recentViewRv.setVisibility(View.GONE);
                    recycleViewCenter.setVisibility(View.VISIBLE);

                    dialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    void price_range(String from_price, String to_price) {
        final Dialog dialog = Commonhelper.loadDialog(SubCategoryDetailsActivity.this);
        new AbstrctClss(SubCategoryDetailsActivity.this, ConstntApi.price_range(SubCategoryDetailsActivity.this, from_price, to_price), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    JSONArray jsonArray = new JSONArray(s);

                    if (jsonArray.length() < 1) {
                        Commonhelper.showToastLong(SubCategoryDetailsActivity.this, Constnt.notFound);
                        dialog.dismiss();
                        return;
                    }

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }

                    recycleViewCenter.setAdapter(new MyRecyclerAdapterProduct(jsonObjectArrayList));
                    recentViewRv.setVisibility(View.GONE);
                    recycleViewCenter.setVisibility(View.VISIBLE);

                    dialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    private void hitPi(String category_id, String subcategory_id) {
        String req = "product.php?category_id=" + category_id + "&subcategory_id=" + subcategory_id + "";
        new AbstrctClss(SubCategoryDetailsActivity.this, ConstntApi.product(SubCategoryDetailsActivity.this,req), "p", true) {
            @Override
            public void responce(String s) {
                try
                {
                    String Res = s;
                    JSONArray jsonArray = new JSONArray(Res);

                    if (jsonArray.optJSONObject(0).has("Status"))
                    {
                        if (jsonArray.optJSONObject(0).optString("Status").equalsIgnoreCase("true"))
                        {
                            ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                            for (int i = 0; i < jsonArray.length(); i++) {
                                jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                            }

                            recycleViewCenter.setAdapter(new MyRecyclerAdapterProduct(jsonObjectArrayList));
                            recentViewRv.setVisibility(View.GONE);
                            recycleViewCenter.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            Commonhelper.showToastLong(SubCategoryDetailsActivity.this, jsonArray.optJSONObject(0).optString("Message"));
                        }
                    }
                    else if (jsonArray.length()>0)
                    {
                        ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                        }

                        recycleViewCenter.setAdapter(new MyRecyclerAdapterProduct(jsonObjectArrayList));
                        recentViewRv.setVisibility(View.GONE);
                        recycleViewCenter.setVisibility(View.VISIBLE);
                    }
                    else {
                        Commonhelper.showToastLong(SubCategoryDetailsActivity.this,Constnt.notFound);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Commonhelper.sop("dsd"+ "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }

    private void hitPi2(String category_id) {
        final Dialog dialog = Commonhelper.loadDialog(SubCategoryDetailsActivity.this);
        String req = InterfaceClass.ipAddress3 + "get_small_banner.php?category_id=" + category_id;
        Log.d("rrrr", req);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, req,
                response -> {
                    try {
                        String Res = response;
                        Log.d("hitPi2", Res);

                        JSONArray jsonArray = new JSONArray(Res);

                        if (jsonArray.length() == 0) {
                            Commonhelper.showToastLong(SubCategoryDetailsActivity.this, Constnt.notFound);
                            dialog.dismiss();
                            return;
                        }

                        if (jsonArray.optJSONObject(0).has("Status")) {
                            Commonhelper.showToastLong(SubCategoryDetailsActivity.this, jsonArray.optJSONObject(0).optString("Message"));
                        } else {
                            ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                            for (int i = 0; i < jsonArray.length(); i++) {
                                jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                            }

                            recycleViewCenter.setAdapter(new MyRecyclerAdapterProduct(jsonObjectArrayList));
                            recentViewRv.setVisibility(View.GONE);
                            recycleViewCenter.setVisibility(View.VISIBLE);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("hitPi2", "sssss");
                    }
                    dialog.dismiss();
                },
                error -> Log.i("reer", error.getMessage())) {
//            @Override
//            protected Map getParams()
//            {
//                Map params = new HashMap();
//                params.put("username", "");
//                params.put("email", "abc@androidhive.info");
//                params.put("password", "password123");
//
//                return params;
//            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(SubCategoryDetailsActivity.this);
        requestQueue.add(stringRequest);

    }

    private void recentViewPi() {
        new AbstrctClss(SubCategoryDetailsActivity.this, ConstntApi.recentViewPi(SubCategoryDetailsActivity.this), "g", true) {
            @Override
            public void responce(String s) {
                try
                {
                    String Res = s;
                    Log.d("recentViewPi", Res);
                    JSONArray jsonArray = new JSONArray(Res);

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }

                    recentViewRv.setAdapter(new MyRecyclerAdapterRecentllyView(jsonObjectArrayList));
                    recentViewRv.setVisibility(View.VISIBLE);
                    recycleViewCenter.setVisibility(View.GONE);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("recentViewPi", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                recentViewPi();
            }
        };
    }

    public class MyRecyclerAdapterRecentllyView extends RecyclerView.Adapter<MyRecyclerAdapterRecentllyView.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterRecentllyView(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.today_deal_item, parent, false);
            v.getLayoutParams().width = (int) (getScreenWidth(SubCategoryDetailsActivity.this) / 2.0f); /// THIS LINE WILL DIVIDE OUR VIEW INTO NUMBERS OF PARTS
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.prodNmeTv.setText(jsonObjectal.get(position).optString("title"));
            holder.ipTv.setText("IP " + jsonObjectal.get(position).optString("ip"));
            //double disAmt = discountAmt(parseDouble(jsonObjectal.get(position).optString("sale_price")), parseDouble(jsonObjectal.get(position).optString("discount")));
            //long value = roudOff(disAmt);
            holder.priceTv.setText("₹" + jsonObjectal.get(position).optString("sale_price"));
            int i = Integer.parseInt(jsonObjectal.get(position).optString("product_sub_id"));
            Picasso.with(SubCategoryDetailsActivity.this).load(InterfaceClass.imgPth2 + jsonObjectal.get(position).optString("product_id") + "_" + i + "_thumb.jpg").into(holder.img);
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView prodNmeTv, ipTv, priceTv;
            private ImageView img;

            public ViewHolder(View itemView) {
                super(itemView);

                prodNmeTv = itemView.findViewById(R.id.prodNmeTv);
                ipTv = itemView.findViewById(R.id.ipTv);
                priceTv = itemView.findViewById(R.id.priceTv);
                img = itemView.findViewById(R.id.img);

                itemView.setOnClickListener(view -> {
                    Intent intent = new Intent(SubCategoryDetailsActivity.this, SubCategoryDetails2.class);
                    intent.putExtra("res", jsonObjectal.get(getAdapterPosition()).toString());
                    int pos = getAdapterPosition() + 1;
                    intent.putExtra("image", InterfaceClass.imgPth2 + jsonObjectal.get(getAdapterPosition()).optString("product_id") + "_" + pos + "_thumb.jpg");
                    startActivity(intent);
                });
            }

        }
    }

    void searchData(String keyword) {
        new AbstrctClss(SubCategoryDetailsActivity.this, ConstntApi.search(SubCategoryDetailsActivity.this, keyword), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    Log.d("searchData", s);
                    JSONArray jsonArray = new JSONArray(s);

                    if (jsonArray.optJSONObject(0).optString("status").equalsIgnoreCase("false")) {
                        Commonhelper.showToastLong(SubCategoryDetailsActivity.this, jsonArray.optJSONObject(0).optString("message"));
                        return;
                    }

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }
                    recycleViewCenter.setAdapter(new MyRecyclerAdapterProduct(jsonObjectArrayList));
                    recentViewRv.setVisibility(View.GONE);
                    recycleViewCenter.setVisibility(View.VISIBLE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    float divideLine = 2.5f;
}

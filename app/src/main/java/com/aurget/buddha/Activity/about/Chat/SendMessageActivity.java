package com.aurget.buddha.Activity.about.Chat;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aurget.buddha.Activity.ImageCropper.CropImage;
import com.aurget.buddha.Activity.ImageCropper.CropImageView;
import com.aurget.buddha.R;

import org.json.JSONObject;

import java.util.ArrayList;

public class SendMessageActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);

        recyclerView = findViewById(R.id.recycler_view);

        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(SendMessageActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(new MyRecyclerAdapter(null));

        findViewById(R.id.browseImgLl).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(SendMessageActivity.this);
                bottomSheetDialog.setContentView(R.layout.chat_tools);


                bottomSheetDialog.findViewById(R.id.imageTv).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        popupUploadImage();
                        bottomSheetDialog.dismiss();
                    }
                });

                bottomSheetDialog.findViewById(R.id.videoTv).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent pickVideoIntent = new Intent(Intent.ACTION_GET_CONTENT);
                        pickVideoIntent.setType("video/*");
                        startActivityForResult(pickVideoIntent, REQUEST_PICK_VIDEO);
                        bottomSheetDialog.dismiss();
                    }
                });

                bottomSheetDialog.show();
            }
        });
    }

    public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapter(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.send_message_list, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

        }

        @Override
        public int getItemCount() {
            return 10;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public ViewHolder(View itemView) {
                super(itemView);
            }
        }

    }

    private void popupUploadImage()
    {
        CropImage cropImage = new CropImage();

        cropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setActivityTitle("Crop image")
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setCropMenuCropButtonTitle("Done")
                .setRequestedSize(1000, 1000)
                .setCropMenuCropButtonIcon(R.drawable.ic_fullscreen_black_24dp)
                .start(SendMessageActivity.this);
    }


    int REQUEST_PICK_VIDEO = 900;
}
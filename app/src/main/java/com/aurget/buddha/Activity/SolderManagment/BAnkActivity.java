package com.aurget.buddha.Activity.SolderManagment;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.ImageCropper.CropImage;
import com.aurget.buddha.Activity.ImageCropper.CropImageView;
import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.Activity.Login;
import com.aurget.buddha.Activity.Main2Activity;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.Utils.CustomPreference;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;
import com.google.zxing.integration.android.IntentIntegrator;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.aurget.buddha.Activity.Commonhelper.bitMap;
import static com.aurget.buddha.Activity.Commonhelper.fileExe;


/**
 * Created by Admin on 10/4/2019.
 */

public class BAnkActivity extends BaseActivity {

    private SharedPreferences Shpref;
    private SharedPreferences.Editor editShpref;

    private IntentIntegrator qrScan;


    public static RelativeLayout passbookRl, aadharRl, adharRl, panRl, panRl2;
    public static ImageView passbookimg, aadharcardImg, aadharcardImg1, aadharcardImgRemove2, imgpssbook, panImg;
    public static CardView aadharcardCV;
    EditText brnchneEt, ifcscodeEt, accountNoEt, bankNameEt, panNOEt, adharEt;
    public static LinearLayout aadharcardLl, frontAdharCard, backAdharcard, previous;
    TextView panTv, passbookTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bnk_dilog);

        passbookRl = findViewById(R.id.passbookRl);
        panTv = findViewById(R.id.panTv);
        //qrScan = new IntentIntegrator(this);
        //qrScan.initiateScan();

        passbookimg = findViewById(R.id.passbookimg);
        passbookTv = findViewById(R.id.passbookTv);
        adharEt = findViewById(R.id.adharEt);
        imgpssbook = findViewById(R.id.imgpssbook);
        aadharcardImg = findViewById(R.id.aadharcardImg);
        aadharcardImg1 = findViewById(R.id.aadharcardImg1);
        aadharcardImgRemove2 = findViewById(R.id.aadharcardImgRemove2);
        aadharRl = findViewById(R.id.aadharRl);
        adharRl = findViewById(R.id.adharRl);
        frontAdharCard = findViewById(R.id.frontAdharCard);

        previous = findViewById(R.id.previous);

        brnchneEt = findViewById(R.id.brnchneEt);
        panNOEt = findViewById(R.id.panNOEt);
        bankNameEt = findViewById(R.id.bankNameEt);
        accountNoEt = findViewById(R.id.accountNoEt);
        ifcscodeEt = findViewById(R.id.ifcscodeEt);
        aadharcardLl = findViewById(R.id.aadharcardLl);
        //aadharcardCV = findViewById(R.id.aadharcardCV);
        backAdharcard = findViewById(R.id.backAdharcard);
        panRl = findViewById(R.id.panRl);
        panRl2 = findViewById(R.id.panRl2);
        panImg = findViewById(R.id.panImg);

        Shpref = getSharedPreferences("sp", MODE_PRIVATE);
        editShpref = Shpref.edit();

        passbookRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                editShpref.putInt("num", 1);
                editShpref.apply();

                popupUploadImage();
            }
        });

        frontAdharCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editShpref.putInt("num", 2);
                editShpref.apply();

                popupUploadImage();
            }
        });

        aadharcardImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frontAdharCard.performClick();
            }
        });

        backAdharcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editShpref.putInt("num", 3);
                editShpref.apply();

                popupUploadImage();
            }
        });

        aadharcardImg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backAdharcard.performClick();
            }
        });

        /*bankNameEt.setEnabled(false);
        accountNoEt.setEnabled(false);
        ifcscodeEt.setEnabled(false);
        brnchneEt.setEnabled(false);
        adharEt.setEnabled(false);
        panNOEt.setEnabled(false);
        passbookRl.setClickable(false);
        frontAdharCard.setClickable(false);
        backAdharcard.setClickable(false);
        panRl.setClickable(false);*/

        findViewById(R.id.editImg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bankNameEt.isEnabled()) {
                    bankNameEt.setEnabled(false);
                    accountNoEt.setEnabled(false);
                    ifcscodeEt.setEnabled(false);
                    brnchneEt.setEnabled(false);
                    adharEt.setEnabled(false);
                    panNOEt.setEnabled(false);
                    passbookRl.setClickable(false);
                    frontAdharCard.setClickable(false);
                    backAdharcard.setClickable(false);
                    panRl.setClickable(false);
                } else {
                    bankNameEt.setEnabled(true);
                    accountNoEt.setEnabled(true);
                    ifcscodeEt.setEnabled(true);
                    brnchneEt.setEnabled(true);
                    adharEt.setEnabled(true);
                    panNOEt.setEnabled(true);
                    passbookRl.setClickable(true);
                    frontAdharCard.setClickable(true);
                    backAdharcard.setClickable(true);
                    panRl.setClickable(true);
                }
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //imageUpload();

                if (bankNameEt.getText().toString().trim().length() == 0) {
                    Commonhelper.showToastLong(BAnkActivity.this, "Please enter bank name");
                    return;
                }
                if (accountNoEt.getText().toString().trim().length() == 0) {
                    Commonhelper.showToastLong(BAnkActivity.this, "Please enter account no.");
                    return;
                }
                if (ifcscodeEt.getText().toString().trim().length() == 0) {
                    Commonhelper.showToastLong(BAnkActivity.this, "Please enter IFSC code.");
                    return;
                }
                if (adharEt.getText().toString().trim().length() == 0) {
                    Commonhelper.showToastLong(BAnkActivity.this, "Please enter adhar no.");
                    return;
                }

                if (adharEt.getText().toString().length() != 12) {
                    Commonhelper.showToastLong(BAnkActivity.this, getString(R.string.invalidAdharNo));
                    return;
                }

                if (panNOEt.getText().toString().length() != 0) {
                    if (panNOEt.getText().toString().length() != 10) {
                        Commonhelper.showToastLong(BAnkActivity.this, getString(R.string.invalidPanNO));
                        return;
                    }
                }

                showDialog();
                //add_bank_details();
            }
        });

        findViewById(R.id.adharRl).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(BAnkActivity.this);
                dialog.setContentView(R.layout.aadhar_fornt_back);

                TextView FrontScreenTv = dialog.findViewById(R.id.FrontScreenTv);
                TextView BackScreenTv = dialog.findViewById(R.id.BackScreenTv);

                dialog.findViewById(R.id.byMonthTv).setVisibility(View.GONE);

                FrontScreenTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        editShpref.putInt("num", 2);
                        editShpref.apply();

                        popupUploadImage();
                        dialog.dismiss();

                    }
                });

                BackScreenTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        editShpref.putInt("num", 3);
                        editShpref.apply();

                        popupUploadImage();
                        dialog.dismiss();

                    }
                });

                dialog.show();

            }
        });

        panRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                editShpref.putInt("num", 4);
                editShpref.apply();

                popupUploadImage();
            }
        });

        panRl2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                panRl.performClick();
            }
        });

        adharEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus) {
                    if (adharEt.getText().toString().length() != 12) {
                        Commonhelper.showToastLong(BAnkActivity.this, getString(R.string.invalidAdharNo));
                    }
                }
            }
        });

        panNOEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus) {
                    if (panNOEt.getText().toString().length() != 10) {
                        Commonhelper.showToastLong(BAnkActivity.this, getString(R.string.invalidPanNO));
                        panNOEt.setText("");
                    }
                }
            }
        });

        panNOEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
               /* if (panNOEt.getText().toString().length() > 4) {
                    panNOEt.setInputType(InputType.TYPE_CLASS_NUMBER);
                }
                if (panNOEt.getText().toString().length() > 9) {
                    panNOEt.setInputType(InputType.TYPE_CLASS_TEXT);
                }*/
            }
        });

        fetch_bank_details();

    }

    public void showDialog()
    {
        Button yes, no;
        final Dialog dialoglog = new Dialog(mActivity);
        dialoglog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialoglog.setContentView(R.layout.logout_dialog);
        Window window = dialoglog.getWindow();
        window.setLayout(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
        dialoglog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        yes = (Button) dialoglog.findViewById(R.id.yes);
        no = (Button) dialoglog.findViewById(R.id.no);
        TextView change = dialoglog.findViewById(R.id.lbleTv);
        change.setText("Are you sure you want to change this settings.");
        yes.setText("Yes");
        no.setText("No");

        dialoglog.show();
        yes.setOnClickListener(view -> {
            try {
                add_bank_details();
            } catch (Exception e) {
                e.printStackTrace();
            }
            dialoglog.dismiss();
        });

        no.setOnClickListener(view -> dialoglog.dismiss());
    }

    private void fetch_bank_details() {

        new AbstrctClss(BAnkActivity.this, ConstntApi.fetch_bank_details(BAnkActivity.this), "p", true) {
            @Override
            public void responce(String s) {

                try
                {
                    JSONArray jsonArray = new JSONArray(s);
                    bankNameEt.setText(jsonArray.optJSONObject(0).optString("bank"));
                    accountNoEt.setText("" + jsonArray.optJSONObject(0).optString("account_no"));
                    brnchneEt.setText(jsonArray.optJSONObject(0).optString("branch"));
                    ifcscodeEt.setText(jsonArray.optJSONObject(0).optString("ifsc"));
                    adharEt.setText(jsonArray.optJSONObject(0).optString("aadhar_number"));

                    try {
                        Picasso.with(BAnkActivity.this)
                                .load(InterfaceClass.ipAddress3 + jsonArray.optJSONObject(0).optString("pass_image"))
                                .into(passbookimg, new com.squareup.picasso.Callback() {
                                    @Override
                                    public void onSuccess() {
                                        passbookTv.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onError() {

                                    }
                                });
                        //Picasso.with(BAnkActivity.this).load(InterfaceClass.ipAddress3 + jsonArray.optJSONObject(0).optString("pass_image")).into(passbookimg);
                        //passbookTv.setVisibility(View.GONE);

                    } catch (Exception e) {
                    }
                    try {
                        Picasso.with(BAnkActivity.this).load(InterfaceClass.ipAddress3 + jsonArray.optJSONObject(0).optString("adhar_image")).into(aadharcardImg);
                        if (aadharcardImg.getDrawable() != null)
                            aadharcardImg.setVisibility(View.VISIBLE);
                    } catch (Exception e) {
                    }
                    try {
                        Picasso.with(BAnkActivity.this).load(InterfaceClass.ipAddress3 + jsonArray.optJSONObject(0).optString("adhar_back")).into(aadharcardImg1);
                        if (aadharcardImg1.getDrawable() != null)
                            aadharcardImg1.setVisibility(View.VISIBLE);
                    } catch (Exception e) {
                    }
                    try {
                        Picasso.with(BAnkActivity.this)
                                .load(InterfaceClass.ipAddress3 + jsonArray.optJSONObject(0).optString("pan_image"))
                                .into(panImg, new com.squareup.picasso.Callback() {
                                    @Override
                                    public void onSuccess() {
                                        panTv.setVisibility(View.GONE);
                                    }
                                    @Override
                                    public void onError() {
                                    }
                                });
                        //Picasso.with(BAnkActivity.this).load(InterfaceClass.ipAddress3 + jsonArray.optJSONObject(0).optString("pan_image")).into(panImg);

                    } catch (Exception e) {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    private void add_bank_details()
    {
        String id = CustomPreference.readString(BAnkActivity.this, CustomPreference.e_id, "");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("1", id);
            jsonObject.put("2", bankNameEt.getText().toString());
            jsonObject.put("3", accountNoEt.getText().toString());
            jsonObject.put("4", brnchneEt.getText().toString());
            jsonObject.put("5", ifcscodeEt.getText().toString());
            jsonObject.put("6", id);
            jsonObject.put("7", adharEt.getText().toString());
            jsonObject.put("8", panNOEt.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new AbstrctClss(BAnkActivity.this, ConstntApi.add_bank_details(BAnkActivity.this, jsonObject), "p", true) {
            @Override
            public void responce(String s) {
                try {
                    JSONArray jsonObject = new JSONArray(s);
                    Commonhelper.showToastLong(BAnkActivity.this, jsonObject.optJSONObject(0).optString("message"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }


    private void popupUploadImage() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setActivityTitle("Crop image")
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setCropMenuCropButtonTitle("Done")
                .setRequestedSize(1000, 1000).setScaleType(CropImageView.ScaleType.FIT_CENTER)
                .setCropMenuCropButtonIcon(R.drawable.ic_fullscreen_black_24dp)
                .start(BAnkActivity.this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

/*
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result.getContents()!=null)
        {
            Toast.makeText(BAnkActivity.this,result.getContents(),Toast.LENGTH_LONG).show();
        }
*/
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult cropResult = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                int i = Shpref.getInt("num", 0);
                Log.i("vvv", "" + i);
                Log.i("vvv-", "" + cropResult.getUri());

                if (i == 1) {

                    Picasso.with(BAnkActivity.this).load(cropResult.getUri()).into(passbookimg);
                    findViewById(R.id.passbookTv).setVisibility(View.GONE);
                    Bitmap panImgPthBitmap = null;
                    try {
                        panImgPthBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), cropResult.getUri());
                        passbookImgExt = fileExe(cropResult.getUri());
                        passbookImgPath = bitMap(panImgPthBitmap);

                        AsyncDataClass asyncRequestObject = new AsyncDataClass();
                        asyncRequestObject.execute(InterfaceClass.ipAddress3 + "upload_bank_img.php",
                                CustomPreference.readString(BAnkActivity.this, CustomPreference.e_id, ""),
                                passbookImgPath,
                                passbookImgExt,
                                "4"
                        );
                        //Toast.makeText(BAnkActivity.this, "" + passbookImgExt, Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (i == 2) {
                    // frontAdharCard.setVisibility(View.GONE);
                    aadharcardImg.setVisibility(View.VISIBLE);
                    Picasso.with(BAnkActivity.this).load(cropResult.getUri()).into(aadharcardImg);
                    Bitmap panImgPthBitmap = null;
                    try {
                        panImgPthBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), cropResult.getUri());
                        adharCrdImgPth = bitMap(panImgPthBitmap);
                        adharCrdImgext = fileExe(cropResult.getUri());
                        AsyncDataClass asyncRequestObject = new AsyncDataClass();
                        asyncRequestObject.execute(InterfaceClass.ipAddress3 + "upload_bank_img.php",
                                CustomPreference.readString(BAnkActivity.this, CustomPreference.e_id, ""),
                                adharCrdImgPth,
                                adharCrdImgext,
                                "1"
                        );
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (i == 3) {
                    aadharcardImg1.setVisibility(View.VISIBLE);
                    Picasso.with(BAnkActivity.this).load(cropResult.getUri()).into(aadharcardImg1);
                    Bitmap panImgPthBitmap = null;
                    try {
                        panImgPthBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), cropResult.getUri());
                        adharCrdImgBackPth = bitMap(panImgPthBitmap);
                        adharCrdImgBackExt = fileExe(cropResult.getUri());
                        AsyncDataClass asyncRequestObject = new AsyncDataClass();
                        asyncRequestObject.execute(InterfaceClass.ipAddress3 + "upload_bank_img.php",
                                CustomPreference.readString(BAnkActivity.this, CustomPreference.e_id, ""),
                                adharCrdImgBackPth,
                                adharCrdImgBackExt,
                                "2"
                        );
                        //Toast.makeText(BAnkActivity.this, "" + adharCrdImgBackExt, Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (i == 4) {
                    //panImg.setImageURI(cropResult.getUri());
                    Picasso.with(BAnkActivity.this).load(cropResult.getUri()).into(panImg);
                    panTv.setText("");
                    Bitmap panImgPthBitmap = null;
                    try {
                        panImgPthBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), cropResult.getUri());
                        panImgPth = bitMap(panImgPthBitmap);
                        panImgext = fileExe(cropResult.getUri());

                        AsyncDataClass asyncRequestObject = new AsyncDataClass();
                        asyncRequestObject.execute(InterfaceClass.ipAddress3 + "upload_bank_img.php",
                                CustomPreference.readString(BAnkActivity.this, CustomPreference.e_id, ""),
                                panImgPth,
                                panImgext,
                                "3"
                        );

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(BAnkActivity.this, "Cropping failed: " + cropResult.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private class AsyncDataClass extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            HttpParams httpParameters = new BasicHttpParams();
//
//            HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
//
//            HttpConnectionParams.setSoTimeout(httpParameters, 5000);

            HttpClient httpClient = new DefaultHttpClient(httpParameters);

            HttpPost httpPost = new HttpPost(params[0]);

            String jsonResult = "";

            try {

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                nameValuePairs.add(new BasicNameValuePair("e_id", params[1]));
                nameValuePairs.add(new BasicNameValuePair("profile_image", params[2]));
                nameValuePairs.add(new BasicNameValuePair("extensions", params[3]));
                nameValuePairs.add(new BasicNameValuePair("img_type", params[4]));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = httpClient.execute(httpPost);

                jsonResult = Commonhelper.inputStreamToString(response.getEntity().getContent()).toString();

                System.out.println("Returned Json object " + nameValuePairs.toString());

            } catch (ClientProtocolException e) {

                e.printStackTrace();

            } catch (IOException e) {

                e.printStackTrace();
            }
            return jsonResult;
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            dialog = Commonhelper.loadDialog(BAnkActivity.this);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            System.out.println("Resulted Value: " + result);

            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.optJSONObject(0).optString("Status").equalsIgnoreCase("true"))
                    Toast.makeText(BAnkActivity.this, jsonArray.optJSONObject(0).optString("message"), Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(BAnkActivity.this, jsonArray.optJSONObject(0).optString("message"), Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            dialog.dismiss();

        }
    }

    String panImgPth = "_", adharCrdImgPth = "_", adharCrdImgBackPth = "_", passbookImgPath = "_";
    String panImgext = "_", adharCrdImgext = "_", passbookImgExt = "_", adharCrdImgBackExt = "_";
    Dialog dialog;
}


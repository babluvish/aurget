package com.aurget.buddha.Activity.YourReturn;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.ImageCropper.CropImage;
import com.aurget.buddha.Activity.ImageCropper.CropImageView;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ReturnDetail extends BaseActivity {

    TextView AddmoreTv,prodNmeTv;
    ImageView Addmoreimg,img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_detail);

        AddmoreTv = findViewById(R.id.AddmoreTv);
        Addmoreimg = findViewById(R.id.Addmoreimg);
        img = findViewById(R.id.img);
        prodNmeTv = findViewById(R.id.prodNmeTv);
        Addmoreimg.setVisibility(View.GONE);

        if (getIntent().hasExtra("prodId")) {
            prodId = getIntent().getStringExtra("prodId");
            details(prodId);
        }

        AddmoreTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popupUploadImage();
            }
        });

        Addmoreimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dilog();
            }
        });
    }

    void dilog()
    {
        Dialog dialog = new Dialog(ReturnDetail.this);
        dialog.setContentView(R.layout.img_list);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);

        RecyclerView recycleViewmonth = dialog.findViewById(R.id.recycleViewmonth);
        GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recycleViewmonth.setLayoutManager(manager);
        recycleViewmonth.setAdapter(new MyRecyclerAdapterMonth(uriArrayList));
        dialog.show();

    }

    public class MyRecyclerAdapterMonth extends RecyclerView.Adapter<MyRecyclerAdapterMonth.ViewHolder> {

        private ArrayList<Uri> jsonObjectal;

        public MyRecyclerAdapterMonth(ArrayList<Uri> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapterMonth.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.img_list_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapterMonth.ViewHolder holder, int position) {

            holder.img.setImageURI(jsonObjectal.get(position));
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView img;


            public ViewHolder(View itemView) {
                super(itemView);
                img = itemView.findViewById(R.id.img);
            }
        }
    }


    private void popupUploadImage() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setActivityTitle("Crop image")
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setCropMenuCropButtonTitle("Done").setAspectRatio(Gravity.FILL,200)
                .setRequestedSize(1000, 1000)
                .setCropMenuCropButtonIcon(R.drawable.ic_fullscreen_black_24dp)
                .start(ReturnDetail.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult cropResult = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                uriArrayList.add(cropResult.getUri());
                Addmoreimg.setVisibility(View.VISIBLE);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + cropResult.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }


    private void details(String prodID)
    {
        new AbstrctClss(mActivity, ConstntApi.return_list_details(mActivity,prodID), "g", true)
        {
            @Override
            public void responce(String s) {
                try {
                    Log.d("deal_product", s);
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    prodNmeTv.setText(jsonArray.optJSONObject(0).optString("product_name"));
                    Commonhelper.picasso_(mActivity,jsonArray.optJSONObject(0).optString("image"),img);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    ArrayList<Uri> uriArrayList = new ArrayList<>();
    String prodId = "";
}


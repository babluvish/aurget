package com.aurget.buddha.Activity.code.OrderModule;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.service.GPSTracker;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.R;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.single.PermissionListener;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import code.utils.AppUrls;

import static com.aurget.buddha.Activity.code.utils.AppConstants.addressNewList;


public class ChooseAddressListActivity extends BaseActivity implements View.OnClickListener, LocationListener {

    ArrayList<HashMap<String, String>> addresstList = new ArrayList();
    RecyclerView recyclerView;
    AddressApdapter addressApdapter;
    //ImageView header
    ImageView ivMenu, ivSearch, ivCart;
    //TextView
    TextView tvMain, tvCount, tvAddressCount, tvAddAddress, tvProceed;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 2;
    private LocationManager locationManager;
    private String provider;
    private Location loc;

    private static final int REQUEST_CODE_PERMISSION = 2;
    String mPermission = Manifest.permission.ACCESS_FINE_LOCATION;

    // GPSTracker class
    GPSTracker gps;

    private String mLastUpdateTime;

    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

    private static final int REQUEST_CHECK_SETTINGS = 100;
    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;

    // boolean flag to toggle the ui
    private Boolean mRequestingLocationUpdates;
    private static final String TAG = "google";

    private void init()
    {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

                updateLocationUI();
            }
        };

        mRequestingLocationUpdates = false;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    private void restoreValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("is_requesting_updates")) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean("is_requesting_updates");
            }

            if (savedInstanceState.containsKey("last_known_location")) {
                mCurrentLocation = savedInstanceState.getParcelable("last_known_location");
            }

            if (savedInstanceState.containsKey("last_updated_on")) {
                mLastUpdateTime = savedInstanceState.getString("last_updated_on");
            }
        }

        updateLocationUI();
    }

    private void updateLocationUI()
    {
        if (mCurrentLocation != null)
        {
            Log.d("cccc","Lat: " + mCurrentLocation.getLatitude() + ", " +
                            "Lng: " + mCurrentLocation.getLongitude());
            Log.d("ssss",mLastUpdateTime);

            getAddress(mActivity,mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude());
        }
    }

    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, locationSettingsResponse -> {
                    Log.i(TAG, "All location settings are satisfied.");
                    //Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();
                    //noinspection MissingPermission
                    mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                            mLocationCallback, Looper.myLooper());

                    updateLocationUI();
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(ChooseAddressListActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                                Toast.makeText(ChooseAddressListActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                        updateLocationUI();
                    }
                });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("is_requesting_updates", mRequestingLocationUpdates);
        outState.putParcelable("last_known_location", mCurrentLocation);
        outState.putString("last_updated_on", mLastUpdateTime);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);

        init();

        //gps = new GPSTracker(mActivity);

        initialise();

        restoreValuesFromBundle(savedInstanceState);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        findViewById(R.id.textView3).setOnClickListener(v -> {

            //AppUtils.showRequestDialog_(mActivity);

            /*new Thread(() -> {
                if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                }
                loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                //runOnUiThread(() -> getAddress(mActivity, loc.getLatitude(), loc.getLongitude()));

            }).start();*/
            //onSomeButtonClick();
        });
    }

    void getLocation()
    {
        try
        {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull
            int[] grantResults) {
        if (requestCode == 123) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLocation();
            } else {
                Toast.makeText(this, "Location permission not granted, " + "restart the app if you want the", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initialise() {
        //RecyclerView for list
        recyclerView = findViewById(R.id.recyclerview);

        //ImageView
        ivMenu = findViewById(R.id.iv_menu);
        ivSearch = findViewById(R.id.searchmain);
        ivCart = findViewById(R.id.ivCart);

        //TextView
        tvCount = findViewById(R.id.tvCount);
        tvMain = findViewById(R.id.tvHeaderText);
        tvAddressCount = findViewById(R.id.tvAddress);
        tvAddAddress = findViewById(R.id.textView6);
        tvProceed = findViewById(R.id.textView9);

        ivMenu.setImageResource(R.drawable.ic_back);

        tvMain.setText("Choose Addresses");
        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        ivCart.setOnClickListener(this);
        tvAddAddress.setOnClickListener(this);
        tvProceed.setOnClickListener(this);
        ivCart.setVisibility(View.GONE);
        ivSearch.setVisibility(View.GONE);
        tvCount.setVisibility(View.GONE);
        tvProceed.setVisibility(View.VISIBLE);

        RecyclerView.LayoutManager recylerViewLayoutManager = new LinearLayoutManager(mActivity);
        recyclerView.setLayoutManager(recylerViewLayoutManager);

        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getAddressListApi();
        } else {
            AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
        }
    }

    @Override
    public void onClick(View view) {

        Intent mIntent;

        switch (view.getId()) {

            case R.id.iv_menu:
                finish();
                return;

            case R.id.textView6:
                /*mIntent = new Intent(mActivity, OrderAddEditAddressActivity.class);
                mIntent.putExtra("From", "0");
                startActivity(mIntent);
                finish();
                AppUtils.showRequestDialog(mActivity);*/
                // check if GPS enabled

                progressDialog = new ProgressDialog(mActivity);
                //progressDialog.setCancelable(false);
                progressDialog.setMessage(mActivity.getString(R.string.please_wait));
                progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                progressDialog.show();

                //AppUtils.showRequestDialog_(mActivity);

                Dexter.withActivity(this)
                        .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                        .withListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse response) {
                                mRequestingLocationUpdates = true;
                                startLocationUpdates();
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse response) {
                                if (response.isPermanentlyDenied()) {
                                    // open device settings when the permission is
                                    // denied permanently
                                    progressDialog.dismiss();
                                    openSettings();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(com.karumi.dexter.listener.PermissionRequest permission, PermissionToken token) {
                                token.continuePermissionRequest();
                            }

                        }).check();

                //onSomeButtonClick();

                return;

            case R.id.textView9:

                if (addresstList != null && addressNewList.size() > 0) {
                    startActivity(new Intent(getBaseContext(), PlaceOrderActivity.class));
                } else {
                    Toast.makeText(this, " Please add new address firstly.", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

    private void openSettings() {
        /*Intent intent = new Intent();
        intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",
                BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);*/
    }

    private void getAddressListApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("getAddressListApi", AppUrls.getAllUserAddress);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));

            json.put(AppConstants.result, json_data);

            Log.v("getAddressListApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.getAllUserAddress)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        parseJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseJsondata(JSONObject response) {

        Log.d("response ", response.toString());

        addresstList.clear();
        addressNewList.clear();

        try {

            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);

            if (jsonObject.getString("res_code").equals("1")) {

                JSONArray productArray = jsonObject.getJSONArray("data");

                tvAddressCount.setText(String.valueOf(productArray.length()) + " Saved Addresses");

                for (int i = 0; i < productArray.length(); i++) {

                    JSONObject productobject = productArray.getJSONObject(i);

                    HashMap<String, String> addList = new HashMap();

                    addList.put("id", productobject.getString("id"));
                    addList.put("name", productobject.getString("name"));
                    addList.put("phone_no", productobject.getString("phone_no"));
                    addList.put("pincode", productobject.getString("pincode"));
                    addList.put("locality", productobject.getString("locality"));
                    addList.put("area_streat_address", productobject.getString("area_streat_address"));
                    addList.put("district_city_town", productobject.getString("district_city_town"));
                    addList.put("landmark_optional", productobject.getString("landmark_optional"));
                    addList.put("stateName", productobject.getString("stateName"));
                    addList.put("state_id", productobject.getString("state_id"));
                    addList.put("countery_id", productobject.getString("countery_id"));
                    addList.put("country_name", productobject.getString("country_name"));
                    addList.put("alternative_phone_optional", productobject.getString("alternative_phone_optional"));

                    if (i == 0) {
                        addList.put("primary", "1");
                        addressNewList.add(addList);
                    } else {
                        addList.put("primary", "0");
                    }

                    addresstList.add(addList);

                }

                addressApdapter = new AddressApdapter(addresstList);
                recyclerView.setAdapter(addressApdapter);
                recyclerView.setNestedScrollingEnabled(true);

            } else {
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(recyclerView, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
    }

    @Override
    public void onLocationChanged(Location location) {
      /*  int lat = (int) (location.getLatitude());
        int lng = (int) (location.getLongitude());
      */
        try
        {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                //return;
            }

            loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            getAddress(mActivity, loc.getLatitude(), loc.getLongitude());
        }
        catch (Exception e)
        {
            loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            getAddress(mActivity, loc.getLatitude(), loc.getLongitude());
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

        /*Toast.makeText(this, "Enabled new provider " + provider,
                Toast.LENGTH_SHORT).show();*/
    }

    @Override
    public void onProviderDisabled(String provider) {
       /* Toast.makeText(this, "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();*/
    }

    private class AddressApdapter extends RecyclerView.Adapter<FavNameHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

        public AddressApdapter(ArrayList<HashMap<String, String>> favList) {
            data = favList;
        }

        public FavNameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new FavNameHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_address_new, parent, false));
        }

        public void onBindViewHolder(FavNameHolder holder, final int position) {
            holder.tvName.setText(data.get(position).get("name"));
            holder.tvMobile.setText(data.get(position).get("phone_no"));
            String valueAddress = "";

            if (!data.get(position).get("area_streat_address").equalsIgnoreCase("")) {
                valueAddress = valueAddress + data.get(position).get("area_streat_address") + ", ";
            }
            if (!data.get(position).get("locality").equalsIgnoreCase("")) {
                valueAddress = valueAddress + data.get(position).get("locality") + ", ";
            }
            if (!data.get(position).get("landmark_optional").equalsIgnoreCase("")) {
                valueAddress = valueAddress + data.get(position).get("landmark_optional") + ", ";
            }
            if (!data.get(position).get("district_city_town").equalsIgnoreCase("")) {
                valueAddress = valueAddress + data.get(position).get("district_city_town") + ", ";
            }
            if (!data.get(position).get("country_name").equalsIgnoreCase("")) {
                valueAddress = valueAddress + data.get(position).get("country_name") + "- ";
            }
            if (!data.get(position).get("stateName").equalsIgnoreCase("")) {
                valueAddress = valueAddress + data.get(position).get("stateName") + "- ";
            }
            if (!data.get(position).get("pincode").equalsIgnoreCase("")) {
                valueAddress = valueAddress + data.get(position).get("pincode");
            }
            holder.tvAddress.setText(valueAddress);

            if (data.get(position).get("primary").equals("1")) {
                holder.ivCheck.setImageResource(R.drawable.ic_address_selected);
            } else {
                holder.ivCheck.setImageResource(R.drawable.ic_address_unselected);
            }

            holder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                        DeleteAddressApi(data.get(position).get("id"));
                    } else {
                        AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                    }
                }
            });

            holder.ivCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    addressNewList.clear();

                    for (int i = 0; i < data.size(); i++) {

                        HashMap<String, String> addList = new HashMap();

                        addList.put("id", data.get(i).get("id"));
                        addList.put("name", data.get(i).get("name"));
                        addList.put("phone_no", data.get(i).get("phone_no"));
                        addList.put("pincode", data.get(i).get("pincode"));
                        addList.put("locality", data.get(i).get("locality"));
                        addList.put("area_streat_address", data.get(i).get("area_streat_address"));
                        addList.put("district_city_town", data.get(i).get("district_city_town"));
                        addList.put("landmark_optional", data.get(i).get("landmark_optional"));
                        addList.put("alternative_phone_optional", data.get(i).get("alternative_phone_optional"));
                        addList.put("country_name", data.get(i).get("country_name"));
                        addList.put("stateName", data.get(i).get("stateName"));


                        if (data.get(position).get("id").equals(data.get(i).get("id"))) {
                            if (data.get(i).get("primary").equals("1")) {
                                addList.put("primary", "0");
                            } else {
                                addList.put("primary", "1");
                                addressNewList.add(addList);
                            }
                        } else {
                            addList.put("primary", "0");
                        }

                        addresstList.set(i, addList);
                    }

                    addressApdapter.notifyDataSetChanged();

                }
            });

            holder.llView.setOnClickListener(view -> {

                Intent mIntent = new Intent(mActivity, OrderAddEditAddressActivity.class);
                mIntent.putExtra("AddressId", data.get(position).get("id"));
                mIntent.putExtra("Name", data.get(position).get("name"));
                mIntent.putExtra("PhoneNo", data.get(position).get("phone_no"));
                mIntent.putExtra("Pincode", data.get(position).get("pincode"));
                mIntent.putExtra("Locality", data.get(position).get("locality"));
                mIntent.putExtra("Flat", data.get(position).get("area_streat_address"));
                mIntent.putExtra("City", data.get(position).get("district_city_town"));
                mIntent.putExtra("Landmark", data.get(position).get("landmark_optional"));
                mIntent.putExtra("Alternate", data.get(position).get("alternative_phone_optional"));
                mIntent.putExtra("state", data.get(position).get("state_id"));
                mIntent.putExtra("countrynam", data.get(position).get("countery_id"));
                mIntent.putExtra("From", "1");
                startActivity(mIntent);
                finish();

            });

        }

        public int getItemCount() {
            return data.size();
        }
    }

    private class FavNameHolder extends RecyclerView.ViewHolder {
        ImageView ivDelete, ivCheck;
        TextView tvName, tvAddress, tvMobile;

        LinearLayout llView;

        public FavNameHolder(View itemView) {
            super(itemView);
            llView = (LinearLayout) itemView.findViewById(R.id.llAddress);
            ivDelete = (ImageView) itemView.findViewById(R.id.ivDelete);
            ivCheck = (ImageView) itemView.findViewById(R.id.imageView5);
            tvName = (TextView) itemView.findViewById(R.id.textView4);
            tvAddress = (TextView) itemView.findViewById(R.id.textView3);
            tvMobile = (TextView) itemView.findViewById(R.id.textView5);
        }
    }

    private void DeleteAddressApi(String AddressId) {

        AppUtils.showRequestDialog(mActivity);

        Log.v("DeleteAddressApi", AppUrls.addNewAddress);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("address_master_id", AddressId);
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json_data.put("add_update_delete", "3");
            json_data.put("name", "");
            json_data.put("phone_no", "");
            json_data.put("pincode", "");
            json_data.put("locality", "");
            json_data.put("area_streat_address", "");
            json_data.put("district_city_town", "");
            json_data.put("landmark_optional", "");
            json_data.put("alternative_phone_optional", "");
            json_data.put("state_master_id", "");
            json_data.put("countery_id", "");
            json_data.put("latitude", "0.0");
            json_data.put("longitude", "0.0");

            json.put(AppConstants.result, json_data);

            Log.v("DeleteAddressApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.addNewAddress)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedeletedata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedeletedata(JSONObject response) {

        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
                AppUtils.hideDialog();
                finish();
                startActivity(new Intent(getBaseContext(), ChooseAddressListActivity.class));
                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    getAddressListApi();
                } else {
                    AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                }
            } else {
                AppUtils.hideDialog();
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.hideDialog();
            AppUtils.showErrorMessage(recyclerView, String.valueOf(e), mActivity);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        //onSomeButtonClick();
        if (mRequestingLocationUpdates && checkPermissions()) {
            startLocationUpdates();
        }
        updateLocationUI();
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    public void onSomeButtonClick() {

        if (!permissionsGranted()) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 123);
        } else {
            statusCheck();
        }
    }

    private Boolean permissionsGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mRequestingLocationUpdates) {
            // pausing location updates
            stopLocationUpdates();
        }
    }

    public void stopLocationUpdates() {
        // Removing location updates
        mFusedLocationClient
                .removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, task -> Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT));
    }

    public String getAddress(Context ctx, double latitude, double longitude) {
        StringBuilder result = new StringBuilder();
        try {
            Log.e("tag---", String.valueOf(latitude));
            Log.e("tag---", String.valueOf(longitude));
            Geocoder geocoder = new Geocoder(ctx, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                String locality = address.getLocality();
                String city = address.getCountryName();
                String region_code = address.getCountryCode();
                String zipcode = address.getPostalCode();
                double lat = address.getLatitude();
                double lon = address.getLongitude();
                result.append(locality + " ");
                result.append(city + " " + region_code + " ");
                result.append(zipcode);
                Log.e("tag---", address.toString());

                progressDialog.dismiss();

                Intent mIntent = new Intent(mActivity, OrderAddEditAddressActivity.class);
                mIntent.putExtra("From", "2");
                mIntent.putExtra("city", city);
                mIntent.putExtra("zipcode", zipcode);
                mIntent.putExtra("lat", lat);
                mIntent.putExtra("lon", lon);
                mIntent.putExtra("getAddressLine", address.getAddressLine(0));
                mIntent.putExtra("locality", address.getSubAdminArea());
                mIntent.putExtra("countryName", address.getCountryName());
                mIntent.putExtra("countryName", address.getCountryName());
                mIntent.putExtra("getAdminArea", address.getAdminArea());
                startActivity(mIntent);
                finish();

            }
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }
        return result.toString();
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            getLocation();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    ProgressDialog progressDialog = null;
}

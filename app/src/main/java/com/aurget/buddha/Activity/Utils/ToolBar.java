package com.aurget.buddha.Activity.Utils;

import android.app.Activity;
import android.widget.ImageView;
import android.widget.TextView;

import com.aurget.buddha.R;

public class ToolBar {

    static ImageView backBtn ;
    static TextView title;

    public static TextView title(Activity activity)
    {
        title = activity.findViewById(R.id.title);
        return title;
    }

    public static ImageView backBtn(Activity activity)
    {
        return backBtn  = activity.findViewById(R.id.bkImg);
    }
}

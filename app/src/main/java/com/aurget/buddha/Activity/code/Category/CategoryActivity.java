package com.aurget.buddha.Activity.code.Category;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aurget.buddha.Activity.code.Account.AccountActivity;
import com.aurget.buddha.Activity.code.Account.FavouriteListActivity;
import com.aurget.buddha.Activity.code.Adapter.CategoryAdapter;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Database.DatabaseController;
import com.aurget.buddha.Activity.code.Fragment.DashBoardFragment;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.Main.NotificationActivity;
import com.aurget.buddha.Activity.code.Product.CartListActivity;
import com.aurget.buddha.Activity.code.Product.ProductListActivity;
import com.aurget.buddha.Activity.code.Search.SearchActivity;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;

import java.util.ArrayList;
import java.util.HashMap;


public class CategoryActivity extends BaseActivity implements OnClickListener {

    ArrayList<HashMap<String, String>> CategoryList = new ArrayList();
    CategoryAdapter categoryAdapter;
    GridView gridView;
    ImageView ivMenu,ivSearch,ivCart;
    TextView tvMain;
    RelativeLayout rrNoData;
    LinearLayout llHome,llCategory,llFavourites,llProfile,llNotification;
    TextView tvHome,tvCategory,tvFavourites,tvProfile,tvCount;
    ImageView ivHome,ivCategory,ivFavourites,ivProfile,ivNotification;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        initialise();
    }

    private void initialise() {
       //TextView
        tvMain =        findViewById(R.id.tvHeaderText);
        tvHome =        findViewById(R.id.tvHomeBottom);
        tvCategory =    findViewById(R.id.tvCategoryBottom);
        tvFavourites =  findViewById(R.id.tvFavouritesBottom);
        tvProfile =     findViewById(R.id.tvProfileBottom);
        tvCount =       findViewById(R.id.tvCount);
        //ImageView
        ivMenu =        findViewById(R.id.iv_menu);
        ivSearch =      findViewById(R.id.searchmain);
        ivHome =        findViewById(R.id.ivHomeBottom);
        ivCategory =    findViewById(R.id.ivCategoryBottom);
        ivFavourites =  findViewById(R.id.ivFavouritesBottom);
        ivProfile =     findViewById(R.id.ic_profile);
        ivCart =         findViewById(R.id.ivCart);
        ivNotification = findViewById(R.id.ic_notification);

        //GridView
        gridView =  findViewById(R.id.gridView);

        //RelativeLayout
        rrNoData =  findViewById(R.id.rrNoData);

        //LinearLayout fro bottom views
        llHome =  findViewById(R.id.llHome);
        llCategory =  findViewById(R.id.llCategory);
        llFavourites =    findViewById(R.id.llFavourites);
        llProfile =       findViewById(R.id.llProfile);
        llNotification =  findViewById(R.id.llNotification);

        llHome.setOnClickListener(this);
        llCategory.setOnClickListener(this);
        llFavourites.setOnClickListener(this);
        llProfile.setOnClickListener(this);
        llNotification.setOnClickListener(this);

        tvMain.setText("Categories List");
        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        ivCart.setOnClickListener(this);

        ivMenu.setImageResource(R.drawable.ic_back);
        Log.v("MyPagePath","CategoryList");

        getCatData();
    }

    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.iv_menu:

                finish();

                return;

            case R.id.searchmain:
                startActivity(new Intent(getBaseContext(), SearchActivity.class));
                return;

            case R.id.llHome:

                tvHome.setTextColor(Color.parseColor("#155b72"));
                tvCategory.setTextColor(Color.parseColor("#9d9b9b"));
                tvFavourites.setTextColor(Color.parseColor("#9d9b9b"));
                tvProfile.setTextColor(Color.parseColor("#9d9b9b"));
                ivHome.setImageResource(R.drawable.ic_home_darkgreen);
                ivCategory.setImageResource(R.drawable.menu);
                ivFavourites.setImageResource(R.drawable.ic_heart_grey);
                ivProfile.setImageResource(R.drawable.ic_notifications_grey);

              startActivity(new Intent(getBaseContext(), DashBoardFragment.class));

                return;

            case R.id.llFavourites:

                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else {

                    tvHome.setTextColor(Color.parseColor("#9d9b9b"));
                    tvCategory.setTextColor(Color.parseColor("#9d9b9b"));
                    tvFavourites.setTextColor(Color.parseColor("#155b72"));
                    tvProfile.setTextColor(Color.parseColor("#9d9b9b"));
                    ivHome.setImageResource(R.drawable.ic_home_grey);
                    ivCategory.setImageResource(R.drawable.menu);
                    ivFavourites.setImageResource(R.drawable.ic_heart_darkgreen);
                    ivProfile.setImageResource(R.drawable.ic_notifications_grey);

                    startActivity(new Intent(getBaseContext(), FavouriteListActivity.class));
                }

                return;

            case R.id.llNotification:
                tvHome.setTextColor(Color.parseColor("#9d9b9b"));
                tvCategory.setTextColor(Color.parseColor("#9d9b9b"));
                tvFavourites.setTextColor(Color.parseColor("#9d9b9b"));
                tvProfile.setTextColor(Color.parseColor("#9d9b9b"));
                //tvNotification.setTextColor(Color.parseColor("#155b72"));
                ivHome.setImageResource(R.drawable.ic_home_grey);
                ivCategory.setImageResource(R.drawable.menu);
                ivFavourites.setImageResource(R.drawable.ic_heart_grey);
                ivNotification.setImageResource(R.drawable.ic_notifications_green);
                ivProfile.setImageResource(R.drawable.ic_profile_grey);

                startActivity(new Intent(getBaseContext(), NotificationActivity.class));

                break;

            case R.id.llProfile:

                tvHome.setTextColor(Color.parseColor("#9d9b9b"));
                tvCategory.setTextColor(Color.parseColor("#9d9b9b"));
                tvFavourites.setTextColor(Color.parseColor("#9d9b9b"));
                tvProfile.setTextColor(Color.parseColor("#155b72"));
                ivHome.setImageResource(R.drawable.ic_home_grey);
                ivCategory.setImageResource(R.drawable.menu);
                ivFavourites.setImageResource(R.drawable.ic_heart_grey);
                ivProfile.setImageResource(R.drawable.ic_profile_darkgreen);

                startActivity(new Intent(getBaseContext(), AccountActivity.class));

                return;

            case R.id.ivCart:

                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else
                {
                    startActivity(new Intent(getBaseContext(), CartListActivity.class));
                }

                return;

            default:
                return;
        }
    }

    public void getCatData() {

        CategoryList.clear();
        CategoryList.addAll(DatabaseController.getCategoryData());
        Log.v("CategoryName", String.valueOf( DatabaseController.getCategoryData() ) );

        if(CategoryList.size()>0)
        {
            rrNoData.setVisibility(View.GONE);
            gridView.setVisibility(View.VISIBLE);

            setCatAdapter();
        }
        else
        {
            rrNoData.setVisibility(View.VISIBLE);
            gridView.setVisibility(View.GONE);
        }
    }

    public void setCatAdapter()
    {
        categoryAdapter = new CategoryAdapter(mActivity, CategoryList) {
            protected void catClick(View v, String position) {
                SubCategoryActivity.categoryId = (String) ((HashMap) CategoryList.get(Integer.parseInt(position))).get("category_id");
                ProductListActivity.categoryId = (String) ((HashMap) CategoryList.get(Integer.parseInt(position))).get("category_id");
                startActivity(new Intent(getBaseContext(), ProductListActivity.class).putExtra( "category_name",(String) ((HashMap) CategoryList.get(Integer.parseInt(position))).get("category_name") ));
            }
        };
        gridView.setAdapter(categoryAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvHome.setTextColor(Color.parseColor("#9d9b9b"));
        tvCategory.setTextColor(Color.parseColor("#155b72"));
        tvFavourites.setTextColor(Color.parseColor("#9d9b9b"));
        tvProfile.setTextColor(Color.parseColor("#9d9b9b"));
        ivHome.setImageResource(R.drawable.ic_home_grey);
        ivCategory.setImageResource(R.drawable.menu);
        ivFavourites.setImageResource(R.drawable.ic_heart_grey);
        ivProfile.setImageResource(R.drawable.ic_profile_grey);

        int count = Integer.parseInt(AppSettings.getString(AppSettings.cartCount));

        if(count>0)
        {
            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(String.valueOf(count));
        }
        else
        {
            tvCount.setVisibility(View.GONE);
        }
    }
}

package code.Common;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class MyLinearLayout extends LinearLayout {
    public MyLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyLinearLayout(Context context) {
        super(context);
    }

    public void setScaleBoth(float scale) {
        invalidate();
    }

    protected void onDraw(Canvas canvas) {
        int w = getWidth();
        int h = getHeight();
        super.onDraw(canvas);
    }
}

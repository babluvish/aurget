package com.aurget.buddha.Activity.code.Database;

public class TableAmount {
    static final String SQL_CREATE_AMOUNT = ("CREATE TABLE amount (" +
            amountCatColumn.price_last + " VARCHAR,"
            + amountCatColumn.realfees + " VARCHAR,"
            + amountCatColumn.discount + " VARCHAR,"
            + amountCatColumn.id + " VARCHAR,"
            + amountCatColumn.realId + " VARCHAR,"
            + amountCatColumn.mode_id + " VARCHAR,"
            + amountCatColumn.name + " VARCHAR,"
            + amountCatColumn.status + " VARCHAR" + ")");
    public static final String amount = "amount";

    public enum amountCatColumn {
        id,
        realId,
        realfees,
        price_last,
        discount,
        mode_id,
        name,
        status
    }
}

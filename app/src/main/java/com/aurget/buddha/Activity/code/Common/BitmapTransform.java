package code.Common;

import android.graphics.Bitmap;
import com.squareup.picasso.Transformation;

public class BitmapTransform implements Transformation {
    int maxHeight;
    int maxWidth;

    public BitmapTransform(int maxWidth, int maxHeight) {
        this.maxWidth = maxWidth;
        this.maxHeight = maxHeight;
    }

    public Bitmap transform(Bitmap source) {
        int targetWidth;
        int targetHeight;
        if (source.getWidth() > source.getHeight()) {
            targetWidth = this.maxWidth;
            targetHeight = (int) (((double) targetWidth) * (((double) source.getHeight()) / ((double) source.getWidth())));
        } else {
            targetHeight = this.maxHeight;
            targetWidth = (int) (((double) targetHeight) * (((double) source.getWidth()) / ((double) source.getHeight())));
        }
        Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
        if (result != source) {
            source.recycle();
        }
        return result;
    }

    public String key() {
        return this.maxWidth + "x" + this.maxHeight;
    }
}

package com.aurget.buddha.Activity.SolderManagment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.ImageCropper.CropImage;
import com.aurget.buddha.Activity.ImageCropper.CropImageView;
import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.Activity.Main2Activity;
import com.aurget.buddha.Activity.MyProfile.BankAccountEdit;
import com.aurget.buddha.Activity.MyProfile.Shipping_ddress_list_Activity;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.Utils.CustomPreference;
import com.aurget.buddha.R;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.aurget.buddha.Activity.Commonhelper.bitMap;
import static com.aurget.buddha.Activity.Commonhelper.fileExe;

/**
 * Created by Admin on 10/3/2019.
 */

public class MyProfileList extends AppCompatActivity {

    private RelativeLayout docmentionRl, shippingRl, account_settingsRl;
    private LinearLayout account_settingsContainerLl;
    private TextView solderName, mobile_no;
    ImageView circleImg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_profile_list);
        docmentionRl = findViewById(R.id.docmentionRl);
        shippingRl = findViewById(R.id.shippingRl);
        account_settingsRl = findViewById(R.id.account_settingsRl_);
        solderName = findViewById(R.id.solderName);
        mobile_no = findViewById(R.id.mobile_no);
        circleImg = findViewById(R.id.circleImg);
        account_settingsContainerLl = findViewById(R.id.account_settingsContainerLl);

        solderName.setText(CustomPreference.readString(MyProfileList.this, CustomPreference.userName, ""));
        mobile_no.setText(CustomPreference.readString(MyProfileList.this, CustomPreference.mobileNO, ""));

        fetch_bank_details();
        solder_level();

        docmentionRl.setOnClickListener(view -> {
            Intent intent = new Intent(MyProfileList.this, BAnkActivity.class);
            startActivity(intent);
        });

        shippingRl.setOnClickListener(view -> {
            Intent intent = new Intent(MyProfileList.this, Shipping_ddress_list_Activity.class);
            startActivity(intent);
        });

        circleImg.setOnClickListener(view -> popupUploadImage());

        account_settingsRl.setOnClickListener(view -> {

            if (account_settingsContainerLl.getVisibility() == View.GONE) {
                account_settingsContainerLl.setVisibility(View.VISIBLE);
            } else {
                account_settingsContainerLl.setVisibility(View.GONE);
            }

            findViewById(R.id.solderDetilsRl).setOnClickListener(view1 -> {

                Intent intent = new Intent(MyProfileList.this, BankAccountEdit.class);
                intent.putExtra("pos", "1");
                startActivity(intent);
            });

            findViewById(R.id.addressDetilsRl).setOnClickListener(view12 -> {
                Intent intent = new Intent(MyProfileList.this, BankAccountEdit.class);
                intent.putExtra("pos", "2");
                startActivity(intent);
            });

            findViewById(R.id.bankDetilsRl).setOnClickListener(view13 -> {

                Intent intent = new Intent(MyProfileList.this, BankAccountEdit.class);
                intent.putExtra("pos", "3");
                startActivity(intent);
            });

            findViewById(R.id.account_settingsRl).setOnClickListener(view14 -> {

                Intent intent = new Intent(MyProfileList.this, BankAccountEdit.class);
                intent.putExtra("pos", "4");
                startActivity(intent);
            });

        });
    }

    private void popupUploadImage() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setActivityTitle("Crop image")
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setRequestedSize(1000, 1000).setScaleType(CropImageView.ScaleType.FIT_CENTER)
                .setCropMenuCropButtonIcon(R.drawable.ic_fullscreen_black_24dp)
                .start(MyProfileList.this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

/*
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result.getContents()!=null)
        {
            Toast.makeText(BAnkActivity.this,result.getContents(),Toast.LENGTH_LONG).show();
        }
*/
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult cropResult = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                circleImg.setImageURI(cropResult.getUri());
                Bitmap panImgPthBitmap = null;
                try {
                    panImgPthBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), cropResult.getUri());
                    String passbookImgExt = fileExe(cropResult.getUri());
                    String passbookImgPath = bitMap(panImgPthBitmap);

                    AsyncDataClass asyncRequestObject = new AsyncDataClass();
                    asyncRequestObject.execute(InterfaceClass.ipAddress3 + "upload_profile.php",
                            CustomPreference.readString(MyProfileList.this, CustomPreference.e_id, ""),
                            passbookImgPath,
                            passbookImgExt
                    );
                    //Toast.makeText(BAnkActivity.this, "" + passbookImgExt, Toast.LENGTH_LONG).show();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(MyProfileList.this, "Cropping failed: " + cropResult.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void solder_level() {
        new AbstrctClss(MyProfileList.this, ConstntApi.solder_level(MyProfileList.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    try {
                        JSONArray jsonArray = new JSONArray(s);
                        TextView level = findViewById(R.id.level);
                        level.setText(jsonArray.optJSONObject(0).optString("level"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("letestFeturePi", "sssss");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    private void fetch_bank_details() {
        new AbstrctClss(MyProfileList.this, ConstntApi.get_profile(MyProfileList.this), "g", true) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;
                try {
                    jsonArray  = new JSONArray(s);
                    String profilePic = jsonArray.optJSONObject(0).optString("image_path");
                    Commonhelper.picasso_(MyProfileList.this,profilePic,circleImg);
                    Commonhelper.picasso_(MyProfileList.this, profilePic,Main2Activity.imageView);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private class AsyncDataClass extends AsyncTask<String, Void, String> {

        Dialog dialog = Commonhelper.loadDialog(MyProfileList.this);

        @Override
        protected String doInBackground(String... params) {

            HttpParams httpParameters = new BasicHttpParams();
//
//            HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
//
//            HttpConnectionParams.setSoTimeout(httpParameters, 5000);

            HttpClient httpClient = new DefaultHttpClient(httpParameters);

            HttpPost httpPost = new HttpPost(params[0]);

            String jsonResult = "";

            try {

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                nameValuePairs.add(new BasicNameValuePair("e_id", params[1]));
                nameValuePairs.add(new BasicNameValuePair("profile_image", params[2]));
                nameValuePairs.add(new BasicNameValuePair("extensions", params[3]));

                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = httpClient.execute(httpPost);

                jsonResult = Commonhelper.inputStreamToString(response.getEntity().getContent()).toString();

                System.out.println("Returned Json object " + jsonResult.toString());

            } catch (ClientProtocolException e) {

                e.printStackTrace();

            } catch (IOException e) {

                e.printStackTrace();
            }
            return jsonResult;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            System.out.println("Resulted Value: " + result);
            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.optJSONObject(0).optString("Status").equalsIgnoreCase("true")) {
                    Toast.makeText(MyProfileList.this, jsonArray.optJSONObject(0).optString("message"), Toast.LENGTH_LONG).show();
                    fetch_bank_details();
                }
                else
                    Toast.makeText(MyProfileList.this, jsonArray.optJSONObject(0).optString("message"), Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            dialog.dismiss();
        }
    }
}

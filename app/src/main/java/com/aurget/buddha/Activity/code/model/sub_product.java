package code.model;

import java.io.Serializable;

public class sub_product implements Serializable {
    String  sub_id,
           dis_type,
           per_amount,
           sub_weight,
           unit_name,
           actual_p,
           price;

    public String getSub_id() {
        return sub_id;
    }

    public void setSub_id(String sub_id) {
        this.sub_id = sub_id;
    }

    public String getDis_type() {
        return dis_type;
    }

    public void setDis_type(String dis_type) {
        this.dis_type = dis_type;
    }

    public String getPer_amount() {
        return per_amount;
    }

    public void setPer_amount(String per_amount) {
        this.per_amount = per_amount;
    }

    public String getSub_weight() {
        return sub_weight;
    }

    public void setSub_weight(String sub_weight) {
        this.sub_weight = sub_weight;
    }

    public String getUnit_name() {
        return unit_name;
    }

    public void setUnit_name(String unit_name) {
        this.unit_name = unit_name;
    }

    public String getActual_p() {
        return actual_p;
    }

    public void setActual_p(String actual_p) {
        this.actual_p = actual_p;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}

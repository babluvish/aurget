package com.aurget.buddha.Activity.code.Main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Fragment.DashBoardFragment;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;

import org.json.JSONException;
import org.json.JSONObject;

import code.utils.AppUrls;

public class OtpVerificationActivity extends BaseActivity implements View.OnClickListener {

    //RelativeLayout
    RelativeLayout rlBack;

    //Textview
    static EditText edOtp;

    //Button
    Button btnSubmit;
    
    //TextView
    TextView tvResend;

    static String part1;

    static Context context;

    //private BroadcastReceiver mRegistrationBroadcastReceiver;

    //public static String regId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        context = mActivity;

        /*mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId();

                    AppSettings.putString(AppSettings.fcmId,regId);

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {

                    String message = intent.getStringExtra("message");
                    Log.v("msg", message);
                }
            }
        };*/


        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d( "Key: ", key + " Value: " + value);
            }
        }

        initialise();

        setOnclicklistener();
    }

    //Initialise
    public void initialise()
    {
        AppUtils.hideSoftKeyboard(mActivity);

        //RelativeLayout
        rlBack=findViewById(R.id.rl_back);

        //Button
        btnSubmit=findViewById(R.id.btn_submit);

        //TextView
        tvResend=findViewById(R.id.tvResend);

        //EditText
        edOtp=findViewById(R.id.edOtp);
    }


    //SetOnClicklistener
    public void setOnclicklistener()
    {
        rlBack.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        tvResend.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.rl_back:

                finish();

                break;

            case R.id.btn_submit:

                if(edOtp.getText().toString().isEmpty())
                {
                    AppUtils.showErrorMessage(edOtp, getString(R.string.errorOTP), this.mActivity);
                }
                else if (SimpleHTTPConnection.isNetworkAvailable(this.mActivity))
                {
                    VerifyOtpApi();
                }
                else
                {
                    AppUtils.showErrorMessage(edOtp, getString(R.string.error_connection_message), this.mActivity);
                }

                break;

            case R.id.tvResend:

                if (SimpleHTTPConnection.isNetworkAvailable(this.mActivity))
                {
                    ResendOTPApi();
                }
                else
                {
                    AppUtils.showErrorMessage(edOtp, getString(R.string.error_connection_message), this.mActivity);
                }

                break;
        }
    }

    private static void VerifyOtpApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("VerifyOtpApi", AppUrls.otpVerfication);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_id", AppSettings.getString(AppSettings.userId));
            json_data.put("otp_code", edOtp.getText().toString().trim());
            json_data.put("device_id",AppUtils.getDeviceID(mActivity));
            json_data.put("fcm_id", AppSettings.getString(AppSettings.fcmId));
            json.put(AppConstants.result, json_data);
            Log.v("VerifyOtpApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.otpVerfication)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                         AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(edOtp, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(edOtp, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private static void parseJsondata(JSONObject response) {

        Log.d("response ", response.toString());
        AppUtils.hideDialog();
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                if (SimpleHTTPConnection.isNetworkAvailable(context))
                {
                    getProfileApi();
                }
                else
                {
                    AppUtils.showErrorMessage(edOtp, context.getString(R.string.error_connection_message),context);
                }
            }
            else
            {
                AppUtils.showErrorMessage(edOtp, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(edOtp, String.valueOf(e), mActivity);
        }

    }

    private void ResendOTPApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("ResendOTPApi", AppUrls.register);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("phone_number", AppSettings.getString(AppSettings.mobile));
            json_data.put("referel_id", AppSettings.getString(AppSettings.referral));
            json.put(AppConstants.result, json_data);
            Log.v("ResendOTPApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.register)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                         AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(edOtp, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(edOtp, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedata(JSONObject response) {

        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {


            }
            else
            {
                AppUtils.showErrorMessage(edOtp, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(edOtp, String.valueOf(e), mActivity);
        }
         AppUtils.hideDialog();
    }

    //OTP automatic retrival
    public static void recivedSms(String message) {
        try
        {
            String  parts[] = message.split("Dear User, Your OTP is: ");
            part1 = parts[1];
            edOtp.setText(part1);

            if(edOtp.getText().toString().isEmpty())
            {
                AppUtils.showErrorMessage(edOtp, context.getString(R.string.errorOTP), context);
            }
            else if (SimpleHTTPConnection.isNetworkAvailable(context))
            {
                VerifyOtpApi();
            }
            else
            {
                AppUtils.showErrorMessage(edOtp, context.getString(R.string.error_connection_message),context);
            }
        }
        catch (Exception e)
        {
            Log.v("OTPEX", e.toString());
        }
    }

    // and displays on the screen
   /* private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("regId", null);
        Log.e("Firebase reg id: ", regId);

        if (!TextUtils.isEmpty(regId)) {            // txtRegId.setText("Firebase Reg Id: " + regId);
            Log.v("id", "Firebase Reg Id: " + regId);
        } else {
            Log.v("not_received", "Firebase Reg Id is not received yet!");
        }
    }*/

    private static void getProfileApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("getProfileApi", AppUrls.getUserDetail);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_id", AppSettings.getString(AppSettings.userId));

            json.put(AppConstants.result, json_data);

            Log.v("getProfileApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.getUserDetail)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseProfiledata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(edOtp, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(edOtp, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private static void parseProfiledata(JSONObject response) {

        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                JSONObject userObject = jsonObject.getJSONObject("user_detail");

                AppSettings.putString(AppSettings.name,userObject.getString("username"));
                AppSettings.putString(AppSettings.email,userObject.getString("email_id"));
                AppSettings.putString(AppSettings.gender,userObject.getString("gender"));
                AppSettings.putString(AppSettings.mobile,userObject.getString("phone_no"));
                AppSettings.putString(AppSettings.wallet,userObject.getString("wallet_amount"));
                AppSettings.putString(AppSettings.referralId,userObject.getString("referel_id"));

                AppSettings.putString(AppSettings.verified,"1");
                context.startActivity(new Intent(context, DashBoardFragment.class));
                mActivity.finishAffinity();
            }
            else
            {
                AppUtils.showErrorMessage(edOtp, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(edOtp, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
    }
}

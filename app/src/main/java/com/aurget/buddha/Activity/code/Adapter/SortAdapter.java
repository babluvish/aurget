package code.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aurget.buddha.R;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class SortAdapter extends BaseAdapter {
    public OnClickListener catClickListener ;
    Context context;
    ArrayList<HashMap<String, String>> data;

    protected abstract void catClick(View view, String str);

    public SortAdapter(Context context, ArrayList<HashMap<String, String>> placeData) {
        this.context = context;
        this.data = placeData;

        catClickListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                catClick(v, String.valueOf(v.getTag()));
            }
        };
    }

    public int getCount() {
        return this.data.size();
    }

    public Object getItem(int position) {
        return this.data.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView = convertView;
        rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.sortadapter, null);
        ImageView ivPic = (ImageView) rootView.findViewById(R.id.imageView6);
        RelativeLayout rr = (RelativeLayout) rootView.findViewById(R.id.rrCredit);
        TextView tvName = (TextView)rootView.findViewById(R.id.textView18);

        tvName.setText(data.get(position).get("sort_name"));

        if(data.get(position).get("status").equals("1"))
        {
            ivPic.setImageResource(R.drawable.ic_radio_button_checked);
        }
        else
        {
            ivPic.setImageResource(R.drawable.ic_radio_button_unchecked);
        }

        rr.setTag(position);
        rr.setOnClickListener(this.catClickListener);

        return rootView;
    }
}

package com.aurget.buddha.Activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.aurget.buddha.Activity.AddtoCart.SAveforLattrerActivity;
import com.aurget.buddha.Activity.Sqlite.DBHelper;
import com.aurget.buddha.R;

public class EmptyCartActivity extends AppCompatActivity {

    ImageView imageView;
    private DBHelper mydb;
    LinearLayout save_for_latterLl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empty_cart);

        mydb = new DBHelper(this);
        mydb.getAllCotacts();

        imageView = findViewById(R.id.bkImg);
        save_for_latterLl = findViewById(R.id.save_for_latterLl);
        imageView.setColorFilter(ContextCompat.getColor(EmptyCartActivity.this, R.color.blackCol), PorterDuff.Mode.SRC_IN);

        if (mydb.numberOfRows() > 0) {
            save_for_latterLl.setVisibility(View.VISIBLE);
        } else {
            save_for_latterLl.setVisibility(View.GONE);
        }

        save_for_latterLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EmptyCartActivity.this, SAveforLattrerActivity.class);
                startActivity(intent);
                finish();
            }
        });

        findViewById(R.id.bkArrowLl).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.shopNowBt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EmptyCartActivity.this, Main2Activity.class);
                ActivityCompat.finishAffinity(EmptyCartActivity.this);
                startActivity(intent);
            }
        });
    }
}

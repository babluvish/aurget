package com.aurget.buddha.Activity.Adapter;

import android.content.Context;
import android.graphics.RectF;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.R;
import com.imagezoom.ImageAttacher;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SlideAdapterZoomImage extends PagerAdapter {

    private Context context;
    private String product_id;
    private int img_count;
    private List<String> color;
    private List<String> colorName;

    public SlideAdapterZoomImage(Context context, String product_id,int img_count) {
        this.context = context;
        this.product_id = product_id;
        this.img_count = img_count;
    }

    @Override
    public int getCount() {
        return img_count;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.zoom_img_item, null);
        // TextView textView = (TextView) view.findViewById(R.id.textView)
        final int i = position+1;
        final ImageView img =  view.findViewById(R.id.imageView);

        img.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                Picasso.with(context).load(InterfaceClass.imgPth2+product_id+"_"+i+"_thumb.jpg").into(img);
                //img.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                /*PhotoViewAttacher photoViewAttacher = new PhotoViewAttacher(img);
                photoViewAttacher.update();*/
                usingSimpleImage(img);
            }
        });
        //img.setImageResource(color.get(position));

        //LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);

        //textView.setText(colorName.get(position));
        //linearLayout.setBackgroundColor(color.get(position));


        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }

    public void usingSimpleImage(ImageView imageView) {
        ImageAttacher mAttacher = new ImageAttacher(imageView);
        ImageAttacher.MAX_ZOOM = 4.0f; // times the current Size
        ImageAttacher.MIN_ZOOM = 1.0f; // Half the current Size
        MatrixChangeListener mMaListener = new MatrixChangeListener();
        mAttacher.setOnMatrixChangeListener(mMaListener);
        PhotoTapListener mPhotoTap = new PhotoTapListener();
        mAttacher.setOnPhotoTapListener(mPhotoTap);
    }

    private class PhotoTapListener implements ImageAttacher.OnPhotoTapListener {
        @Override
        public void onPhotoTap(View view, float x, float y) {
        }
    }

    private class MatrixChangeListener implements ImageAttacher.OnMatrixChangedListener {
        @Override
        public void onMatrixChanged(RectF rect) {

        }
    }
}

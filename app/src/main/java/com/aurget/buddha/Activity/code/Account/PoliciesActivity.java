package com.aurget.buddha.Activity.code.Account;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aurget.buddha.Activity.code.Account.UserAgreementActivity;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.Product.CartListActivity;
import com.aurget.buddha.Activity.code.Search.SearchActivity;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;


public class PoliciesActivity extends BaseActivity implements View.OnClickListener {

    //ImageView
    ImageView ivCart,ivMenu,ivSearch,ivHome;

    public static int status;

    //LinearLayout
    LinearLayout llTerms,llPolicy,llLicence,llReturn,llAgreement;

    //RelativeLayout
    RelativeLayout  rrTerms,rrPolicy,rrLicence,rrReturn,rrAgreement,rrRefund,rrShipping;

    //ImageView
    ImageView ivTermsDrop,ivPolicyDrop,ivLicenceDrop,ivReturnDrop,ivAgreementDrop;

    //TextView
    TextView tvTerms,tvPolicy,tvLicence,tvReturn,tvAgreement;

    //TextView
    TextView tvCount;
    TextView tvMain;

    int terms=0,condition=0,lic=0,ret=0,agree=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policies);
        
        initialise();
    }

    private void initialise() {

        //RelativeLayout
        rrTerms = findViewById(R.id.rrTandC);
        rrPolicy= findViewById(R.id.rrPolicy);
        rrLicence=findViewById(R.id.rrLicence);
        rrReturn=findViewById(R.id.rrReturn);
        rrAgreement=findViewById(R.id.rrAgreement);
        rrRefund=findViewById(R.id.rrRefund);
        rrShipping=findViewById(R.id.rrShipping);

        //LinearLayout
        llTerms=findViewById(R.id.llTnC);
        llPolicy=findViewById(R.id.llPolicy);
        llLicence=findViewById(R.id.llLicence);
        llReturn=findViewById(R.id.llReturn);
        llAgreement=findViewById(R.id.llAgreement);

        //TextView
        tvCount =   findViewById(R.id.tvCount);
        tvMain =    findViewById(R.id.tvHeaderText);
        tvTerms =   findViewById(R.id.tvTnCData);
        tvPolicy =  findViewById(R.id.tvPolicyMain);
        tvLicence = findViewById(R.id.tvLicenceMain);
        tvReturn =  findViewById(R.id.tvReturnMain);
        tvAgreement =  findViewById(R.id.tvAgreementMain);

        //ImageView
        ivCart =          findViewById(R.id.ivCart);
        ivMenu =          findViewById(R.id.iv_menu);
        ivSearch =        findViewById(R.id.searchmain);
        ivHome =          findViewById(R.id.ivHomeBottom);
        ivTermsDrop =     findViewById(R.id.ivTnCDrop);
        ivPolicyDrop =    findViewById(R.id.ivPolicyDrop);
        ivLicenceDrop =   findViewById(R.id.ivLicenceDrop);
        ivReturnDrop =    findViewById(R.id.ivReturnDrop);
        ivAgreementDrop = findViewById(R.id.ivAgreementDrop);

        ivCart.setOnClickListener(this);
        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        rrTerms.setOnClickListener(this);
        rrPolicy.setOnClickListener(this);
        rrLicence.setOnClickListener(this);
        rrReturn.setOnClickListener(this);
        rrAgreement.setOnClickListener(this);
        rrRefund.setOnClickListener(this);
        rrShipping.setOnClickListener(this);

        ivMenu.setImageResource(R.drawable.ic_back);
        tvMain.setText("Policies");
        tvTerms.setText(getString(R.string.terms_and_privacy_policy));
        tvPolicy.setText(getString(R.string.cancellationPolicy));
        tvLicence.setText(getString(R.string.terms_and_privacy_policy));
        tvReturn.setText(getString(R.string.return_policy));
       // tvAgreement.setText(getString(R.string.user_agreement));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.iv_menu:
                finish();
                return;
            case R.id.searchmain:
                startActivity(new Intent(getBaseContext(), SearchActivity.class));
                return;

            case R.id.ivCart:

                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else
                {
                    startActivity(new Intent(getBaseContext(), CartListActivity.class));
                }

                return;

            case R.id.rrTandC:

                startActivity(new Intent(getBaseContext(), UserAgreementActivity.class));
                status=1;

               /* if (terms == 0) {

                    expand(llTerms);
                    collapse(llPolicy);
                    collapse(llLicence);
                    collapse(llReturn);
                    collapse(llAgreement);

                    terms = 1;
                    condition = 0;
                    lic = 0;
                    ret = 0;
                    agree=0;
                    ivTermsDrop.setRotation(270);
                    ivPolicyDrop.setRotation(90);
                    ivLicenceDrop.setRotation(90);
                    ivReturnDrop.setRotation(90);
                    ivAgreementDrop.setRotation(90);

                } else {
                    collapse(llTerms);
                    collapse(llPolicy);
                    collapse(llLicence);
                    collapse(llReturn);
                    collapse(llAgreement);
                    terms = 0;
                    condition = 0;
                    lic = 0;
                    ret = 0;
                    agree=0;
                    ivTermsDrop.setRotation(90);
                    ivPolicyDrop.setRotation(90);
                    ivLicenceDrop.setRotation(90);
                    ivReturnDrop.setRotation(90);
                    ivAgreementDrop.setRotation(90);
                }*/
                return;

            case R.id.rrPolicy:
                startActivity(new Intent(getBaseContext(), UserAgreementActivity.class));
                status=2;
               /* if (condition == 0) {

                    expand(llPolicy);
                    collapse(llTerms);
                    collapse(llLicence);
                    collapse(llReturn);
                    collapse(llAgreement);

                    terms = 0;
                    condition = 1;
                    lic = 0;
                    ret = 0;
                    agree = 0;
                    ivPolicyDrop.setRotation(270);
                    ivTermsDrop.setRotation(90);
                    ivLicenceDrop.setRotation(90);
                    ivReturnDrop.setRotation(90);
                    ivAgreementDrop.setRotation(90);

                } else {
                    collapse(llTerms);
                    collapse(llPolicy);
                    collapse(llLicence);
                    collapse(llReturn);
                    collapse(llAgreement);
                    terms = 0;
                    condition = 0;
                    lic = 0;
                    ret = 0;
                    agree=0;
                    ivTermsDrop.setRotation(90);
                    ivPolicyDrop.setRotation(90);
                    ivLicenceDrop.setRotation(90);
                    ivReturnDrop.setRotation(90);
                    ivAgreementDrop.setRotation(90);
                }*/
                return;


            case R.id.rrLicence:

               /* if (lic == 0) {

                    expand(llLicence);
                    collapse(llTerms);
                    collapse(llPolicy);
                    collapse(llReturn);
                    collapse(llAgreement);

                    terms = 0;
                    condition = 0;
                    lic = 1;
                    ret = 0;
                    agree=0;
                    ivTermsDrop.setRotation(90);
                    ivPolicyDrop.setRotation(90);
                    ivLicenceDrop.setRotation(270);
                    ivReturnDrop.setRotation(90);
                    ivAgreementDrop.setRotation(90);

                } else {
                    collapse(llTerms);
                    collapse(llPolicy);
                    collapse(llLicence);
                    collapse(llReturn);
                    collapse(llAgreement);
                    terms = 0;
                    condition = 0;
                    lic = 0;
                    ret = 0;
                    agree=0;
                    ivTermsDrop.setRotation(90);
                    ivPolicyDrop.setRotation(90);
                    ivLicenceDrop.setRotation(90);
                    ivReturnDrop.setRotation(90);
                    ivAgreementDrop.setRotation(90);
                }*/
                return;

            case R.id.rrReturn:
                startActivity(new Intent(getBaseContext(), UserAgreementActivity.class));
                status=3;
              /*  if (ret == 0) {

                    expand(llReturn);
                    collapse(llTerms);
                    collapse(llLicence);
                    collapse(llPolicy);
                    collapse(llAgreement);

                    terms = 0;
                    condition = 0;
                    lic = 0;
                    ret = 1;
                    agree=0;
                    ivTermsDrop.setRotation(90);
                    ivPolicyDrop.setRotation(90);
                    ivLicenceDrop.setRotation(90);
                    ivAgreementDrop.setRotation(90);
                    ivReturnDrop.setRotation(270);

                } else {
                    collapse(llTerms);
                    collapse(llPolicy);
                    collapse(llLicence);
                    collapse(llReturn);
                    collapse(llAgreement);
                    terms = 0;
                    condition = 0;
                    lic = 0;
                    ret = 0;
                    agree=0;
                    ivTermsDrop.setRotation(90);
                    ivPolicyDrop.setRotation(90);
                    ivLicenceDrop.setRotation(90);
                    ivReturnDrop.setRotation(90);
                    ivAgreementDrop.setRotation(90);
                }*/
                return;
            case  R.id.rrRefund:
                startActivity(new Intent(getBaseContext(), UserAgreementActivity.class));
                status=4;
                break;
            case  R.id.rrShipping:
                startActivity(new Intent(getBaseContext(), UserAgreementActivity.class));
                status=5;
                break;
            case R.id.rrAgreement:
                startActivity(new Intent(getBaseContext(), UserAgreementActivity.class));
                status=6;
              /*  if (agree== 0) {

                    expand(llAgreement);
                    collapse(llTerms);
                    collapse(llLicence);
                    collapse(llPolicy);
                    collapse(llReturn);

                    terms = 0;
                    condition = 0;
                    lic = 0;
                    ret = 0;
                    agree=1;
                    ivTermsDrop.setRotation(90);
                    ivPolicyDrop.setRotation(90);
                    ivLicenceDrop.setRotation(90);
                    ivReturnDrop.setRotation(90);
                    ivAgreementDrop.setRotation(270);

                } else {
                    collapse(llTerms);
                    collapse(llPolicy);
                    collapse(llLicence);
                    collapse(llReturn);
                    collapse(llAgreement);
                    terms = 0;
                    condition = 0;
                    lic = 0;
                    ret = 0;
                    agree = 0;
                    ivTermsDrop.setRotation(90);
                    ivPolicyDrop.setRotation(90);
                    ivLicenceDrop.setRotation(90);
                    ivReturnDrop.setRotation(90);
                    ivAgreementDrop.setRotation(90);
                }*/
                return;
        }
    }

    public static void expand(final View v) {
        v.measure(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? WindowManager.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void expandNew(final View v) {
        v.measure(WindowManager.LayoutParams.MATCH_PARENT, R.dimen._220sdp);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? WindowManager.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

}

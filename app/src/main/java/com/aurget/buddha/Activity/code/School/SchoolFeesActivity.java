package com.aurget.buddha.Activity.code.School;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Database.DatabaseController;
import com.aurget.buddha.Activity.code.Database.TableAmount;
import com.aurget.buddha.Activity.code.Database.TableBranch;
import com.aurget.buddha.Activity.code.Database.TableClass;
import com.aurget.buddha.Activity.code.Database.TableMode;
import com.aurget.buddha.Activity.code.Database.TableSchool;
import com.aurget.buddha.Activity.code.Database.TableSection;
import com.aurget.buddha.Activity.code.payment.WebViewActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;

import code.utility.AvenuesParams;
import code.utility.Constants;
import code.utils.AppUrls;


import static code.utils.AppUrls.getBranchClassFee;

public class SchoolFeesActivity extends BaseActivity implements View.OnClickListener {

    //ImageView
    ImageView ivCart,ivMenu,ivSearch,ivHome,ivCheck;

    //Calendar
    private Calendar myCalendar = Calendar.getInstance();

    //TextView
    TextView tvHeaderText,tvSubmit,tvUseWallet;

    //ArrayList
    public static ArrayList<String> school_List = new ArrayList<String>();
    public static ArrayList<String> school_Code = new ArrayList<String>();

    public static ArrayList<String> branch_List = new ArrayList<String>();
    public static ArrayList<String> branch_Code = new ArrayList<String>();

    public static ArrayList<String> class_List = new ArrayList<String>();
    public static ArrayList<String> class_Code = new ArrayList<String>();

    public static ArrayList<String> section_List = new ArrayList<String>();
    public static ArrayList<String> section_Code = new ArrayList<String>();

    public static ArrayList<String> mode_List = new ArrayList<String>();
    public static ArrayList<String> mode_Code = new ArrayList<String>();

    public static ArrayList<String> amount_List = new ArrayList<String>();
    public static ArrayList<String> amount_Code = new ArrayList<String>();
    public static ArrayList<String> amount_Id = new ArrayList<String>();

    //Spinner
    Spinner spinner_school, spinner_branch,spinner_class,spinner_section,spinner_mode,spinner_amount;

    String schoolId ="", branchId ="",classId="",sectionId="",mode="",amount="",amountId="";

    View v1;

    //EditText
    EditText edSName,edRollNo,edFather;

    //Typeface
    Typeface typeface;

    //RelativeLayout
    RelativeLayout rrUseWallet;

    float wallet=0;
    int check=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fees);

        initialise();
    }

    private void initialise() {

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        //Typeface
        typeface   = Typeface.createFromAsset(mActivity.getAssets(),"centurygothic.otf");

        //RelativeLayout
        rrUseWallet=   findViewById(R.id.rrUseWallet);

        //ImageView
        ivCart =   findViewById(R.id.ivCart);
        ivMenu =   findViewById(R.id.iv_menu);
        ivSearch =   findViewById(R.id.searchmain);
        ivHome =   findViewById(R.id.ivHomeBottom);
        ivCheck =   findViewById(R.id.ivWalletCheck);

        //EditText
        edSName =   findViewById(R.id.editText3);
        edRollNo =   findViewById(R.id.editText6);
        edFather =   findViewById(R.id.edFather);

        //TextView
        tvHeaderText    =   findViewById(R.id.tvHeaderText);
        tvSubmit=   findViewById(R.id.textView27);
        tvUseWallet=   findViewById(R.id.tvWalletCheck);
        
        //View
        v1 = findViewById(R.id.v1);

        //Spinner
        spinner_school =  findViewById(R.id.spinner);
        spinner_branch =  findViewById(R.id.spinner2);
        spinner_class =  findViewById(R.id.spinner3);
        spinner_section =  findViewById(R.id.spinner4);
        spinner_mode =  findViewById(R.id.spinner5);
        spinner_amount =  findViewById(R.id.spinner6);

        ivCart.setOnClickListener(this);
        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        rrUseWallet.setOnClickListener(this);
        
        ivCart.setVisibility(View.INVISIBLE);
        ivSearch.setVisibility(View.INVISIBLE);

        ivMenu.setImageResource(R.drawable.ic_back);

        tvHeaderText.setText(getString(R.string.school_fees));

        if(!AppSettings.getString(AppSettings.wallet).equals(""))
        {
            wallet = Float.parseFloat(AppSettings.getString(AppSettings.wallet));
        }

        if(wallet<=0)
        {
            check=0;
            ivCheck.setImageResource(R.drawable.ic_check_box_unselected);
            rrUseWallet.setVisibility(View.GONE);
        }
        else
        {
            check=1;
            ivCheck.setImageResource(R.drawable.ic_check_box_selected);
            rrUseWallet.setVisibility(View.VISIBLE);
        }

        tvUseWallet.setText("Use RS "+AppSettings.getString(AppSettings.wallet));

        //spinner_school.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, typeList));
        spinner_school.setSelection(0);

        spinner_school.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                if(position==0)
                {
                    schoolId = "";
                    spinner_branch.setSelection(0);
                }
                else
                {
                    schoolId = school_Code.get(position);
                    getBranchData(schoolId);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        spinner_branch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                if(position==0)
                {
                    branchId = "";
                }
                else
                {
                    branchId = branch_Code.get(position);
                    getClassData(branchId);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        spinner_class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                if(position==0)
                {
                    classId = "";
                }
                else
                {
                    classId = class_Code.get(position);
                    getSectionData(classId);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        spinner_section.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                if(position==0)
                {
                    sectionId = "";
                }
                else
                {
                    sectionId = section_Code.get(position);
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                        getBranchClassFee();
                    } else {
                        AppUtils.showErrorMessage(tvHeaderText, getString(R.string.errorInternet), mActivity);
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        spinner_mode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                if(position==0)
                {
                    mode = "";
                }
                else
                {
                    mode = mode_Code.get(position);
                    getAmountData(mode);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        spinner_amount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                if(position==0)
                {
                    amount = "";
                }
                else
                {
                    amount = amount_Code.get(position);
                    amountId = amount_Id.get(position);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        DatabaseController.removeTable(TableSchool.school);
        DatabaseController.removeTable(TableBranch.branch);
        DatabaseController.removeTable(TableClass.classroom);
        DatabaseController.removeTable(TableSection.section);
        DatabaseController.removeTable(TableMode.mode);
        DatabaseController.removeTable(TableAmount.amount);

        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getSchoolListApi();
        } else {
            AppUtils.showErrorMessage(tvHeaderText, getString(R.string.errorInternet), mActivity);
        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.iv_menu:
                finish();
                return;
            
            case R.id.rrUseWallet:

                if(check==0)
                {
                    check=1;
                    ivCheck.setImageResource(R.drawable.ic_check_box_selected);
                }
                else
                {
                    check=0;
                    ivCheck.setImageResource(R.drawable.ic_check_box_unselected);
                }

                return;

            case R.id.textView27:

                if(schoolId.equals(""))
                {
                    AppUtils.showErrorMessage(tvHeaderText,getString(R.string.errorSchool),mActivity);
                }
                else if(branchId.equals(""))
                {
                    AppUtils.showErrorMessage(tvHeaderText,getString(R.string.errorBranch),mActivity);
                }
                else if(classId.equals(""))
                {
                    AppUtils.showErrorMessage(tvHeaderText,getString(R.string.errorClass),mActivity);
                }
                else if(sectionId.equals(""))
                {
                    AppUtils.showErrorMessage(tvHeaderText,getString(R.string.errorSection),mActivity);
                }
                else if(mode.equals(""))
                {
                    AppUtils.showErrorMessage(tvHeaderText,getString(R.string.errorMode),mActivity);
                }
                else if(amount.equals(""))
                {
                    AppUtils.showErrorMessage(tvHeaderText,getString(R.string.errorFeesAmount),mActivity);
                }
                else if(edSName.getText().toString().isEmpty())
                {
                    AppUtils.showErrorMessage(tvHeaderText,getString(R.string.errorStuName),mActivity);
                }
                else if(edFather.getText().toString().isEmpty())
                {
                    AppUtils.showErrorMessage(tvHeaderText,getString(R.string.errorFathName),mActivity);
                }
                else if(edRollNo.getText().toString().isEmpty())
                {
                    AppUtils.showErrorMessage(tvHeaderText,getString(R.string.errorRoll),mActivity);
                }
                else if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {

                    startFeesApi();

                } else {
                    AppUtils.showErrorMessage(tvHeaderText, getString(R.string.errorInternet), mActivity);
                }


                return;

        }
    }

    public class adapter_spinner extends ArrayAdapter<String> {

        ArrayList<String> data;

        public adapter_spinner(Context context, int textViewResourceId, ArrayList <String> arraySpinner_time) {

            super(context, textViewResourceId, arraySpinner_time);

            this.data = arraySpinner_time;

        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row=inflater.inflate(R.layout.spinner_textview_rel, parent, false);
            TextView label=(TextView)row.findViewById(R.id.tv_spinner_name);

            label.setText(data.get(position).toString());

            //label.setTypeface(typeface);

            return row;
        }
    }

    private void startFeesApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("startFeesApi", AppUrls.startSchoolFee);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {

            json_data.put("uuid", AppSettings.getString(AppSettings.userId));
            json_data.put("school_id", schoolId);
            json_data.put("branch_id", branchId);
            json_data.put("std_class", classId);
            json_data.put("std_batch", sectionId);
            json_data.put("std_roll_no", edRollNo.getText().toString().trim());
            json_data.put("std_name", edSName.getText().toString().trim());
            json_data.put("std_fee", amount);
            json_data.put("father_name", edFather.getText().toString().trim());
            json_data.put("fee_mode", mode);
            json_data.put("fee_type", amountId);
            json_data.put("wallet", check);

            json.put(AppConstants.result, json_data);

            Log.v("startFeesApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.startSchoolFee)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedata(JSONObject response) {

        Log.d("response ", response.toString());
        AppUtils.hideDialog();

        try {

            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);

            if (jsonObject.getString("res_code").equals("1")) {

                //Toast.makeText(this, "Payment Successfull", Toast.LENGTH_SHORT).show();

                JSONObject object = jsonObject.getJSONObject("transaction_id");

                AppSettings.putString(AppSettings.transId,object.getString("txn_id"));
                AppSettings.putString(AppSettings.paymentType,"0");

                if(object.getString("payment_req").equals("1"))
                {
                    AppSettings.putString(AppSettings.paymentType,"1");

                    AppSettings.putString(AppSettings.amount,object.getString("amount"));
                    AppSettings.putString(AppSettings.orderId, object.getString("txn_id"));

                    //Toast.makeText(this, "Make Payment of RS "+object.getString("amount"), Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(mActivity, WebViewActivity.class);
                    intent.putExtra(AvenuesParams.ACCESS_CODE, Constants.ACCESS_CODE);
                    intent.putExtra(AvenuesParams.MERCHANT_ID, Constants.MERCHANT_ID);
                    intent.putExtra(AvenuesParams.BILLING_NAME, "Test");
                    intent.putExtra(AvenuesParams.BILLING_ADDRESS, "Test");
                    intent.putExtra(AvenuesParams.BILLING_CITY, "Lucknow");
                    intent.putExtra(AvenuesParams.BILLING_STATE, "Uttar Pradesh");
                    intent.putExtra(AvenuesParams.BILLING_ZIP, "226001");
                    intent.putExtra(AvenuesParams.ORDER_ID,  AppSettings.getString(AppSettings.orderId));
                    intent.putExtra(AvenuesParams.CURRENCY, "INR");
                    intent.putExtra(AvenuesParams.AMOUNT, AppSettings.getString(AppSettings.amount));
                    //intent.putExtra(AvenuesParams.AMOUNT, "1");
                    intent.putExtra(AvenuesParams.BILLING_COUNTRY, "India");

                    intent.putExtra(AvenuesParams.BILLING_TEL, AppSettings.getString(AppSettings.mobile));
                    //intent.putExtra(AvenuesParams.BILLING_EMAIL, AppSettings.getString(AppSettings.email));
                    intent.putExtra(AvenuesParams.BILLING_EMAIL, "test@gmail.com");
                    intent.putExtra(AvenuesParams.REDIRECT_URL, Constants.REDIRECT_URL);
                    intent.putExtra(AvenuesParams.CANCEL_URL, Constants.CANCEL_URL);
                    intent.putExtra(AvenuesParams.RSA_KEY_URL, Constants.RSA_KEY_URL);

                    startActivityForResult(intent, 3);
                }
                else
                {
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                        changeStatusApi("2");
                    } else {
                        AppUtils.showErrorMessage(tvHeaderText, getString(R.string.errorInternet), mActivity);
                    }
                }

            }
            else
            {
                AppUtils.hideDialog();
                AppUtils.showErrorMessage(edSName, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }

        } catch (Exception e) {
            AppUtils.hideDialog();
            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(e), mActivity);
        }

    }

    private void getSchoolListApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("getSchoolListApi", AppUrls.schoolBranch);

        AndroidNetworking.get(AppUrls.schoolBranch)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });

    }

    private void parseJsondata(JSONObject response) {
        Log.d("response ", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                JSONArray categoryArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < categoryArray.length(); i++) {
                    ContentValues mContentValues = new ContentValues();
                    JSONObject schoolObject = categoryArray.getJSONObject(i);
                    mContentValues.put(TableSchool.schoolColumn.schoo_id.toString(), schoolObject.get("schoo_id").toString());
                    mContentValues.put(TableSchool.schoolColumn.school_name.toString(), schoolObject.get("school_name").toString());
                    mContentValues.put(TableSchool.schoolColumn.status.toString(), "1");
                    DatabaseController.insertUpdateData(mContentValues, TableSchool.school, TableSchool.schoolColumn.schoo_id.toString(), schoolObject.get("schoo_id").toString());
                    JSONArray branchArray = schoolObject.getJSONArray("branch");
                    for (int j = 0; j < branchArray.length(); j++) {
                        mContentValues = new ContentValues();
                        JSONObject branchObject = branchArray.getJSONObject(j);
                        mContentValues.put(TableBranch.branchCatColumn.branch_id.toString(), branchObject.get("branch_id").toString());
                        mContentValues.put(TableBranch.branchCatColumn.branch_name.toString(), branchObject.get("address").toString());
                        mContentValues.put(TableBranch.branchCatColumn.schoolId.toString(),schoolObject.get("schoo_id").toString());
                        mContentValues.put(TableBranch.branchCatColumn.status.toString(), "1");
                        DatabaseController.insertUpdateData(mContentValues, TableBranch.branch, TableBranch.branchCatColumn.branch_id.toString(), branchObject.get("branch_id").toString());
                        JSONArray classesArray = branchObject.getJSONArray("classes");
                        for (int k = 0; k < classesArray.length(); k++) {
                            mContentValues = new ContentValues();
                            JSONObject classObject = classesArray.getJSONObject(k);
                            mContentValues.put(TableClass.classroomColumn.branch_id.toString(), branchObject.get("branch_id").toString());
                            mContentValues.put(TableClass.classroomColumn.class_id.toString(), classObject.get("class_id").toString());
                            mContentValues.put(TableClass.classroomColumn.class_name.toString(),classObject.get("class").toString());
                            mContentValues.put(TableClass.classroomColumn.status.toString(), "1");
                            DatabaseController.insertUpdateData(mContentValues, TableClass.classroom, TableClass.classroomColumn.class_id.toString(), classObject.get("class_id").toString());
                            JSONArray sectionArray = classObject.getJSONArray("Section");
                            for (int l = 0; l < sectionArray.length(); l++) {
                                mContentValues = new ContentValues();
                                JSONObject sectionObject = sectionArray.getJSONObject(l);
                                mContentValues.put(TableSection.sectionCatColumn.section_id.toString(), sectionObject.get("section_id").toString());
                                mContentValues.put(TableSection.sectionCatColumn.section.toString(), sectionObject.get("section").toString());
                                mContentValues.put(TableSection.sectionCatColumn.class_id.toString(), classObject.get("class_id").toString());
                                mContentValues.put(TableSection.sectionCatColumn.status.toString(), "1");
                                DatabaseController.insertUpdateData(mContentValues, TableSection.section, TableSection.sectionCatColumn.class_id.toString(), sectionObject.get("section_id").toString());
                            }
                        }
                    }

                }
            }

        } catch (Exception e) {
            AppUtils.showErrorMessage(this.tvHeaderText, String.valueOf(e), this.mActivity);
        }

        AppUtils.hideDialog();

        getSchoolData();

        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getProfileApi();
        } else {
            AppUtils.showErrorMessage(tvHeaderText, getString(R.string.errorInternet), mActivity);
        }
    }

    public void getSchoolData() {

        school_List.clear();
        school_List.add("Select School");
        school_List.addAll(DatabaseController.getSchoolData());

        school_Code.clear();
        school_Code.add("Select School");
        school_Code.addAll(DatabaseController.getSchoolIdData());

        spinner_school.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, school_List));
        spinner_school.setSelection(0);

        branch_List.clear();
        branch_List.add("Select Branch");
        spinner_branch.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, branch_List));
        spinner_branch.setSelection(0);

        class_List.clear();
        class_List.add("Select Class");
        spinner_class.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, class_List));
        spinner_class.setSelection(0);

        section_List.clear();
        section_List.add("Select Section");
        spinner_section.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, section_List));
        spinner_section.setSelection(0);
    }

    public void getBranchData(String schoolId) {

        branch_List.clear();
        branch_List.add("Select Branch");
        branch_List.addAll(DatabaseController.getBranchData(schoolId));

        branch_Code.clear();
        branch_Code.add("Select Branch");
        branch_Code.addAll(DatabaseController.getBranchIdData(schoolId));

        spinner_branch.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, branch_List));
        spinner_branch.setSelection(0);

    }

    public void getClassData(String branchId) {

        class_List.clear();
        class_List.add("Select Class");
        class_List.addAll(DatabaseController.getClassData(branchId));

        class_Code.clear();
        class_Code.add("Select Class");
        class_Code.addAll(DatabaseController.getClassIdData(branchId));

        spinner_class.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, class_List));
        spinner_class.setSelection(0);
    }

    public void getSectionData(String classId) {

        section_List.clear();
        section_List.add("Select Section");
        section_List.addAll(DatabaseController.getSectionData(classId));

        section_Code.clear();
        section_Code.add("Select Section");
        section_Code.addAll(DatabaseController.getSectionIdData(classId));

        spinner_section.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, section_List));
        spinner_section.setSelection(0);
    }


    public void getModeData(String sectionId) {

        mode_List.clear();
        mode_List.add("Select Mode of Payment");
        mode_List.addAll(DatabaseController.getModeData(sectionId));

        mode_Code.clear();
        mode_Code.add("Select Mode of Payment");
        mode_Code.addAll(DatabaseController.getModeIdData(sectionId));

        spinner_mode.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, mode_List));
        spinner_mode.setSelection(0);

        amount_List.clear();
        amount_List.add("Select Amount");
        spinner_amount.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, amount_List));
        spinner_amount.setSelection(0);
    }


    public void getAmountData(String mode) {

        amount_List.clear();
        amount_List.add("Select Amount");
        amount_List.addAll(DatabaseController.getAmountData(mode));

        amount_Code.clear();
        amount_Code.add("Select Amount");
        amount_Code.addAll(DatabaseController.getAmountFeesData(mode));

        amount_Id.clear();
        amount_Id.add("Select Amount");
        amount_Id.addAll(DatabaseController.getAmountIdData(mode));

        spinner_amount.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, amount_List));
        spinner_amount.setSelection(0);
    }


    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            myCalendar.set(year, monthOfYear, dayOfMonth);
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy"/*, new Locale("en_US")*/);
            String formattedDate = df.format(myCalendar.getTime());

            tvHeaderText.setText(formattedDate);
        }
    };


    private void changeStatusApi(String status) {

        AppUtils.showRequestDialog(mActivity);

        Log.v("changeSchoolStatus", AppUrls.changeSchoolStatus);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {

            json_data.put("txn_id", AppSettings.getString(AppSettings.transId));
            json_data.put("status", status);

            json.put(AppConstants.result, json_data);

            Log.v("changeSchoolStatus", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.changeSchoolStatus)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseRechagreResponse(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();

                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseRechagreResponse(JSONObject response) {

        Log.d("response ", response.toString());

        AppUtils.hideDialog();

        try {

            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);

            if (jsonObject.getString("res_code").equals("2")) {

                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    getProfileApi();
                } else {
                    AppUtils.showErrorMessage(tvHeaderText, getString(R.string.errorInternet), mActivity);
                }
            }
            else
            {
                AppUtils.showErrorMessage(tvHeaderText, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }


        } catch (Exception e) {
            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(e), mActivity);
        }

    }

    private void getBranchClassFee() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("getBranchClassFee", getBranchClassFee);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("branch_id", branchId);
            json_data.put("class_id", classId);

            json.put(AppConstants.result, json_data);

            Log.v("getBranchClassFee", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(getBranchClassFee)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata1(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });

    }

    private void parseJsondata1(JSONObject response) {
        Log.d("response ", response.toString());

        DatabaseController.removeTable(TableAmount.amount);
        DatabaseController.removeTable(TableMode.mode);

        try {

            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);

            if (jsonObject.getString("res_code").equals("1")) {

                JSONArray categoryArray = jsonObject.getJSONArray("data");

                for (int i = 0; i < categoryArray.length(); i++) {

                    ContentValues mContentValues = new ContentValues();

                    JSONObject modeJsonObject = categoryArray.getJSONObject(i);

                    mContentValues.put(TableMode.modeColumn.id.toString(), modeJsonObject.get("id").toString());
                    mContentValues.put(TableMode.modeColumn.name.toString(), modeJsonObject.get("name").toString());
                    mContentValues.put(TableMode.modeColumn.section_id.toString(), sectionId);
                    mContentValues.put(TableMode.modeColumn.status.toString(), "1");

                    DatabaseController.insertUpdateData(mContentValues, TableMode.mode, TableMode.modeColumn.section_id.toString(), modeJsonObject.get("id").toString());

                    JSONArray feesArray = modeJsonObject.getJSONArray("fee_pattern");

                    for (int j = 0; j < feesArray.length(); j++) {

                        mContentValues = new ContentValues();

                        JSONObject feesJSONObject = feesArray.getJSONObject(j);

                        UUID idOne = UUID.randomUUID();

                        mContentValues.put(TableAmount.amountCatColumn.id.toString(), String.valueOf(idOne));
                        mContentValues.put(TableAmount.amountCatColumn.realId.toString(), feesJSONObject.get("id").toString());
                        mContentValues.put(TableAmount.amountCatColumn.name.toString(), feesJSONObject.get("name").toString());
                        mContentValues.put(TableAmount.amountCatColumn.realfees.toString(), feesJSONObject.get("fee").toString());
                        mContentValues.put(TableAmount.amountCatColumn.discount.toString(), feesJSONObject.get("discount").toString());
                        mContentValues.put(TableAmount.amountCatColumn.price_last.toString(),feesJSONObject.get("final").toString());
                        mContentValues.put(TableAmount.amountCatColumn.mode_id.toString(), modeJsonObject.get("id").toString());
                        mContentValues.put(TableAmount.amountCatColumn.status.toString(), "1");

                        DatabaseController.insertUpdateData(mContentValues, TableAmount.amount, TableAmount.amountCatColumn.id.toString(), String.valueOf(idOne));
                    }

                }
            }

        } catch (Exception e) {
            AppUtils.showErrorMessage(this.tvHeaderText, String.valueOf(e), this.mActivity);
        }

        AppUtils.hideDialog();

        getModeData(sectionId);
    }

    private void getRefundApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("getRefundApi", AppUrls.paymentRefund);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("order_id", AppSettings.getString(AppSettings.orderId));

            json.put(AppConstants.result, json_data);

            Log.v("getRefundApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.paymentRefund)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseRefunddata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorBody());
                        Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorCode());
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseRefunddata(JSONObject response) {

        Log.d("response ", response.toString());
        AppUtils.hideDialog();

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(tvHeaderText, String.valueOf(jsonObject.getString("res_msg")), mActivity);

                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    getProfileApi();
                } else {
                    AppUtils.showErrorMessage(tvHeaderText, getString(R.string.errorInternet), mActivity);
                }

            }
            else
            {
                AppUtils.showErrorMessage(tvHeaderText, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(e), mActivity);
        }
    }

    private void getProfileApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("getProfileApi", AppUrls.getUserDetail);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_id", AppSettings.getString(AppSettings.userId));

            json.put(AppConstants.result, json_data);

            Log.v("getProfileApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.getUserDetail)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseProfiledata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseProfiledata(JSONObject response) {

        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                JSONObject userObject = jsonObject.getJSONObject("user_detail");

                AppSettings.putString(AppSettings.name,userObject.getString("username"));
                AppSettings.putString(AppSettings.email,userObject.getString("email_id"));
                AppSettings.putString(AppSettings.gender,userObject.getString("gender"));
                AppSettings.putString(AppSettings.mobile,userObject.getString("phone_no"));
                AppSettings.putString(AppSettings.wallet,userObject.getString("wallet_amount"));
                AppSettings.putString(AppSettings.referralId,userObject.getString("referel_id"));

                if(!AppSettings.getString(AppSettings.wallet).equals(""))
                {
                    wallet = Float.parseFloat(AppSettings.getString(AppSettings.wallet));
                }

                if(wallet<=0)
                {
                    check=0;
                    ivCheck.setImageResource(R.drawable.ic_check_box_unselected);
                    rrUseWallet.setVisibility(View.GONE);
                }
                else
                {
                    check=1;
                    ivCheck.setImageResource(R.drawable.ic_check_box_selected);
                    rrUseWallet.setVisibility(View.VISIBLE);
                }

                tvUseWallet.setText("Use RS "+AppSettings.getString(AppSettings.wallet));
            }
            else
            {
                AppUtils.showErrorMessage(tvHeaderText, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
    }

    //code
    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (3) :

                if(AppSettings.getString(AppSettings.status).equalsIgnoreCase("1"))
                {
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                        changeStatusApi("2");
                    } else {
                        AppUtils.showErrorMessage(tvHeaderText, getString(R.string.errorInternet), mActivity);
                    }
                }
                else
                {
                    //AppUtils.showErrorMessage(tvRecharge, "Payment Failed", mActivity);

                    if(AppSettings.getString(AppSettings.paymentType).equalsIgnoreCase("1"))
                    {
                        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                            getRefundApi();
                        } else {
                            AppUtils.showErrorMessage(tvHeaderText, getString(R.string.errorInternet), mActivity);
                        }
                    }
                }

        }
    }

}

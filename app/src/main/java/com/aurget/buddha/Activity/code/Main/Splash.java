package com.aurget.buddha.Activity.code.Main;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;

import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Database.DatabaseController;
import com.aurget.buddha.Activity.code.Database.TableCategory;
import com.aurget.buddha.Activity.code.Database.TableSubCategory;
import com.aurget.buddha.Activity.code.Fragment.DashBoardFragment;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import code.utils.AppUrls;

public class Splash extends BaseActivity {

    TextView tvLogo;
    ProgressDialog progressDialog;
    PackageInfo info;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        this.tvLogo = findViewById(R.id.textView);

        try {
            info = getPackageManager().getPackageInfo("com.aiogrocerymart.aiogrocerymart",PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                Log.e("Hash key", something);
                System.out.println("Hash key" + something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
      /*  PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.aiogrocery.mart", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                Log.e("Hash key", something);
                System.out.println("Hash key" + something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }

        PackageInfo packageInfo = null;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
*/


        if (!DatabaseController.isDataExit(TableCategory.category)) {
            AppSettings.putString(AppSettings.categoryTime, "");
        }

        if (SimpleHTTPConnection.isNetworkAvailable(this.mActivity)) {
            GetCatListApi();
        } else {
            AppUtils.showErrorMessage(this.tvLogo, getString(R.string.error_connection_message), this.mActivity);
        }

    }

    private void GetCatListApi() {
        AppUtils.showRequestDialog(mActivity);
        Log.v("GetCatListApi", AppUrls.getCategory);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            //json_data.put("timestamp", AppSettings.getString(AppSettings.categoryTime));
            json_data.put("timestamp", "");
            json.put(AppConstants.result, json_data);
            Log.v("GetCatListApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.getCategory)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvLogo, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvLogo, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }
    private void parseJsondata(JSONObject response) {
        Log.d("response ", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                JSONArray categoryArray = jsonObject.getJSONArray(TableCategory.category);
                for (int i = 0; i < categoryArray.length(); i++) {
                    ContentValues mContentValues = new ContentValues();
                    JSONObject categoryobject = categoryArray.getJSONObject(i);
                    mContentValues.put(TableCategory.catColumn.category_id.toString(), categoryobject.get("category_id").toString());
                    mContentValues.put(TableCategory.catColumn.category_name.toString(), categoryobject.get("category_name").toString());
                    mContentValues.put(TableCategory.catColumn.featured_category.toString(), categoryobject.get("featured_category").toString());
                    mContentValues.put(TableCategory.catColumn.app_icon.toString(), categoryobject.get("app_icon").toString());
                    mContentValues.put(TableCategory.catColumn.status.toString(), "1");
                    mContentValues.put(TableCategory.catColumn.json.toString(), categoryobject.toString());
                    DatabaseController.insertUpdateData(mContentValues, TableCategory.category, TableCategory.catColumn.category_id.toString(), categoryobject.get("category_id").toString());
                    JSONArray subcategoryArray = categoryobject.getJSONArray(TableSubCategory.sub_category);
                    for (int j = 0; j < subcategoryArray.length(); j++) {
                        mContentValues = new ContentValues();
                        JSONObject subcategoryobject = subcategoryArray.getJSONObject(j);
                        mContentValues.put(TableSubCategory.subCatColumn.category_id.toString(), subcategoryobject.get("category_id").toString());
                        mContentValues.put(TableSubCategory.subCatColumn.sub_category_id.toString(), subcategoryobject.get("sub_category_id").toString());
                        mContentValues.put(TableSubCategory.subCatColumn.sub_category_name.toString(), subcategoryobject.get("sub_category_name").toString());
                        mContentValues.put(TableSubCategory.subCatColumn.icon.toString(), subcategoryobject.get("app_icon").toString());
                        mContentValues.put(TableSubCategory.subCatColumn.status.toString(), "1");
                        mContentValues.put(TableSubCategory.subCatColumn.json.toString(), subcategoryobject.toString());
                        DatabaseController.insertUpdateData(mContentValues, TableSubCategory.sub_category, TableSubCategory.subCatColumn.sub_category_id.toString(), subcategoryobject.get("sub_category_id").toString());
                    }
                }
            }
            AppSettings.putString(AppSettings.categoryTime, jsonObject.getString("sync_time"));
        } catch (Exception e) {
            AppUtils.showErrorMessage(this.tvLogo, String.valueOf(e), this.mActivity);
        }

        AppUtils.hideDialog();

        if (SimpleHTTPConnection.isNetworkAvailable(this.mActivity)) {
            getSettingApi();
        } else {
            AppUtils.showErrorMessage(this.tvLogo, getString(R.string.error_connection_message), this.mActivity);
        }
    }

    private void getSettingApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("getProfileApi", AppUrls.settings);

        AndroidNetworking.get(AppUrls.settings)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseProfiledata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvLogo, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvLogo, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseProfiledata(JSONObject response) {
        Log.d("response ", response.toString());
        AppUtils.hideDialog();
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                JSONObject userObject = jsonObject.getJSONObject("data");
                AppSettings.putString(AppSettings.min_order_bal, userObject.getString("min_order_bal"));
                AppSettings.putString(AppSettings.shippingAmount, userObject.getString("shipping _amount"));
            } else {
                AppUtils.showErrorMessage(tvLogo, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvLogo, String.valueOf(e), mActivity);
        }
        AppSettings.putString(AppSettings.cartCount, "0");
        startActivity(new Intent(getBaseContext(), DashBoardFragment.class));
        finish();
    }
}

package com.aurget.buddha.Activity.code.volly;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.aurget.buddha.Activity.code.utils.AppUtils;

public abstract class AbstrctClss {

    public  abstract void responce(String s);
    public  abstract void onErrorResponsee(String s);

    Activity activity;
    String url;
    String method;
    int methodInt;

    public AbstrctClss(Activity activity, final String url,String method,boolean bool)
    {
        this.activity = activity;
        this.url = url;
        this.method = method;

        AppUtils.showRequestDialog(activity);

        System.out.println("AbstrctClssURL: "+url);

        if (method.equals("p"))
        {
            methodInt = Request.Method.POST;
        }
        else
        {
            methodInt = Request.Method.GET;
        }
        final Request.Priority mPriority = Request.Priority.HIGH;

        StringRequest stringRequest = new StringRequest(methodInt,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try
                        {
                            System.out.println("AbstrctClssRes: "+response);
                            responce(response);
                        } catch (Exception e) {
                            e.printStackTrace();
                            System.out.println("AbstrctClssRes"+e.getMessage());
                        }
                        AppUtils.hideDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        onErrorResponsee(error.getMessage());
                        System.out.println("onErrorResponse"+error.getMessage());
                        AppUtils.hideDialog();
                    }
                })
        {
            @Override
            public Priority getPriority() {
                return mPriority;
            }

        };

        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(18000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
       /* AppController.getInstance().addToRequestQueue(stringRequest);
        stringRequest.getCacheEntry().*/
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);
        requestQueue.getCache().clear();
    }
}

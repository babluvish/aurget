package com.aurget.buddha.Activity;

import android.content.Context;
import android.content.Intent;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.aurget.buddha.Activity.categories.SubCategoryDetailsActivity;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Admin on 8/27/2019.
 */

public class SliderAdapter extends PagerAdapter {

    private Context context;
    private List<String> color;
    private List<String> colorName;
    private int i = 0;

    public SliderAdapter(Context context, List<String> color, List<String> colorName,int i) {
        this.context = context;
        this.color = color;
        this.colorName = colorName;
        this.i = i;
    }

    @Override
    public int getCount() {
        return color.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_slider, null);

       // TextView textView = (TextView) view.findViewById(R.id.textView);
        ImageView img =  view.findViewById(R.id.img);
        Picasso.with(context).load(color.get(position)).into(img);
        //img.setImageResource(color.get(position));

        //LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);

        //textView.setText(colorName.get(position));
       // linearLayout.setBackgroundColor(color.get(position));

        img.setOnClickListener(view1 -> {

            if (i==1) {
                Intent intent = new Intent(context, SubCategoryDetailsActivity.class);
                intent.putExtra("banner_id", colorName.get(position));
                intent.putExtra("category_id", colorName.get(position));
                intent.putExtra("subcategory_id", colorName.get(position));
                context.startActivity(intent);
            }
            else
            {
                Main2Activity.ed2.performClick();
                /*Intent intent = new Intent(context, DashBoardFragment.class);
                context.startActivity(intent);*/
            }
        });

        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}
package com.aurget.buddha.Activity.code.School;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.code.Common.EndScrollListener;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Recharge.RechargeDetailActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import code.utils.AppUrls;

public class SchoolHistoryActivity extends BaseActivity implements View.OnClickListener  {

    Typeface typeface;

    HistoryAdapter historyAdapter;
    ArrayList<HashMap<String, String>> transList = new ArrayList();
    RecyclerView recyclerView;

    //ImageView
    ImageView ivCart,ivMenu,ivSearch;

    //TextView
    TextView tvHeaderText;

    GridLayoutManager mGridLayoutManager;

    boolean loadMore=true;

    int offset=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rec_history);

        findId();
    }

    private void findId() {

        typeface = Typeface.createFromAsset(mActivity.getAssets(), "centurygothic.otf");

        //Recycleview
        recyclerView =   findViewById(R.id.recyclerView);

        //ImageView
        ivCart =   findViewById(R.id.ivCart);
        ivMenu =   findViewById(R.id.iv_menu);
        ivSearch =   findViewById(R.id.searchmain);

        //TextView
        tvHeaderText    =   findViewById(R.id.tvHeaderText);

        ivCart.setVisibility(View.INVISIBLE);
        ivSearch.setVisibility(View.INVISIBLE);

        ivMenu.setImageResource(R.drawable.ic_back);

        tvHeaderText.setText(getString(R.string.fees_payment_history));

        mGridLayoutManager = new GridLayoutManager(mActivity, 1);
        recyclerView.setLayoutManager(mGridLayoutManager);

        ivCart.setOnClickListener(this);
        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);

        transList.clear();
        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getHistoryListApi();
        } else {
            AppUtils.showErrorMessage(tvHeaderText, getString(R.string.errorInternet), mActivity);
        }

        recyclerView.addOnScrollListener(new EndScrollListener(mGridLayoutManager) {
            @Override
            public void onScrolledToEnd() {
                Log.e("Position", "Last item reached");
                if (loadMore) {
                    callNewPage();
                }
            }
        });
    }

    //callNewPage
    protected void callNewPage() {

        offset=offset+20;
        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getHistoryListApi();
        } else {
            AppUtils.showErrorMessage(tvHeaderText, getString(R.string.errorInternet), mActivity);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.iv_menu:
                finish();
                return;
        }
    }

    private void getHistoryListApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("getHistoryListApi", AppUrls.getSchoolTransactionHistory);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json_data.put("type","5");
            json_data.put("offset",offset);
            json.put(AppConstants.result, json_data);
            Log.v("getHistoryListApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.getSchoolTransactionHistory)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseJsondata(JSONObject response) {

        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                JSONArray transactionArray = jsonObject.getJSONArray("transaction_list");

                if(transactionArray.length()>8)
                {
                    loadMore=true;
                }
                else
                {
                    loadMore=false;
                }

                for (int i = 0; i < transactionArray.length(); i++) {
                    JSONObject productobject = transactionArray.getJSONObject(i);
                    HashMap<String, String> prodList = new HashMap();

                    prodList.put("id", productobject.getString("id"));
                    prodList.put("user_id", productobject.getString("user_id"));
                    prodList.put("transaction_id", productobject.getString("transaction_id"));
                    prodList.put("fee_amount", productobject.getString("fee_amount"));
                    prodList.put("class", productobject.getString("class"));
                    prodList.put("batch_no", productobject.getString("batch_no"));
                    prodList.put("roll_no", productobject.getString("roll_no"));
                    prodList.put("fee_mode", productobject.getString("fee_mode"));
                    prodList.put("student_name", productobject.getString("student_name"));
                    prodList.put("father_name", productobject.getString("father_name"));
                    prodList.put("school", productobject.getString("school"));
                    prodList.put("branch_name", productobject.getString("branch_name"));
                    prodList.put("wallet_used", productobject.getString("wallet_used"));
                    prodList.put("wallet_used_amount", productobject.getString("wallet_used_amount"));
                    prodList.put("payment_used", productobject.getString("payment_used"));
                    prodList.put("payment_amount", productobject.getString("payment_amount"));
                    prodList.put("status", productobject.getString("status"));
                    prodList.put("add_date", productobject.getString("add_date"));

                    transList.add(prodList);
                }

            }
            else
            {
                loadMore=false;
                AppUtils.showErrorMessage(tvHeaderText, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvHeaderText, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();

        historyAdapter = new HistoryAdapter(transList);
        recyclerView.setAdapter(historyAdapter);
        recyclerView.setNestedScrollingEnabled(true);
    }

    private class HistoryAdapter extends RecyclerView.Adapter<FavNameHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

        public HistoryAdapter(ArrayList<HashMap<String, String>> favList) {
            data = favList;
        }

        public FavNameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new FavNameHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_recharge, parent, false));
        }

        public void onBindViewHolder(FavNameHolder holder, final int position) {

           /* holder.tvName.setTypeface(typeface);
            holder.tvDate.setTypeface(typeface);
            holder.tvPrice.setTypeface(typeface);*/

            holder.tvPrice.setText("RS "+data.get(position).get("fee_amount"));

            if(data.get(position).get("status").equalsIgnoreCase("1"))
            {
                holder.tvPrice.setTextColor(Color.parseColor("#eeffaa00"));
            }
            else  if(data.get(position).get("status").equalsIgnoreCase("2"))
            {
                holder.tvPrice.setTextColor(Color.parseColor("#4CAF50"));
            }
            else  if(data.get(position).get("status").equalsIgnoreCase("3"))
            {
                holder.tvPrice.setTextColor(Color.parseColor("#ee383e"));
            }
            else  if(data.get(position).get("status").equalsIgnoreCase("4"))
            {
                holder.tvPrice.setTextColor(Color.parseColor("#4CAF50"));
            }

            holder.tvName.setText("Fees paid of "+data.get(position).get("student_name"));
            holder.tvDate.setText("Father Name: "+data.get(position).get("father_name")
                    +"\nClass: "+data.get(position).get("class")
                    +"\nBatch No.: "+data.get(position).get("batch_no")
                    +"\nRoll No.: "+data.get(position).get("roll_no")
                    +"\nSchool Name: "+data.get(position).get("school")
                    +"\nBranch Name: "+data.get(position).get("branch_name")
                    +"\nMode of Payment: "+data.get(position).get("fee_mode")
                    +"\nPayment Date: "+AppUtils.getDateCurrentTimeZone(Long.parseLong(data.get(position).get("add_date"))));


            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String desc= "Student Name: "+data.get(position).get("student_name")
                            +" \nFather Name: "+data.get(position).get("father_name")
                            +"\nClass: "+data.get(position).get("class")
                            +"\nBatch No.: "+data.get(position).get("batch_no")
                            +"\nRoll No.: "+data.get(position).get("roll_no")
                            +"\nSchool Name: "+data.get(position).get("school")
                            +"\nBranch Name: "+data.get(position).get("branch_name")
                            +"\nFees for Month: "+data.get(position).get("fee_mode");

                    Intent mIntent = new Intent(mActivity, RechargeDetailActivity.class);
                    mIntent.putExtra("id", data.get(position).get("id"));
                    mIntent.putExtra("desc",desc);
                    mIntent.putExtra("txn_no",data.get(position).get("transaction_id"));
                    mIntent.putExtra("recharge_amount",data.get(position).get("fee_amount"));
                    mIntent.putExtra("payment_amount",data.get(position).get("payment_amount"));
                    mIntent.putExtra("wallet_used_amount",data.get(position).get("wallet_used_amount"));
                    mIntent.putExtra("status",data.get(position).get("status"));
                    mIntent.putExtra("add_date",AppUtils.getDateCurrentTimeZone(Long.parseLong(data.get(position).get("add_date"))));
                    startActivity(mIntent);

                }
            });


        }

        public int getItemCount() {
            return data.size();
        }
    }

    private static String getMonth(String date) throws ParseException {
        Date d = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        String monthName = new SimpleDateFormat("MMMM").format(cal.getTime());
        return monthName;
    }

    private class FavNameHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView tvDate;
        TextView tvName;
        TextView tvPrice;

        public FavNameHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cardview);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvPrice = (TextView) itemView.findViewById(R.id.tvPrice);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
        }
    }

}

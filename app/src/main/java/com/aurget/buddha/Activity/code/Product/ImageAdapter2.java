package com.aurget.buddha.Activity.code.Product;

import androidx.viewpager.widget.PagerAdapter;

import android.content.Context;


import android.graphics.RectF;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.R;
import com.imagezoom.ImageAttacher;
import com.squareup.picasso.Picasso;

public abstract class ImageAdapter2  extends PagerAdapter {
    private LayoutInflater inflater;
    private Context context;
    public View.OnClickListener viewClickListener;
    String product_id;
    int img_count;

    public ImageAdapter2(Context context,String product_id,int img_count) {
        this.context = context;
        this.product_id = product_id;
        this.img_count = img_count;
        inflater = LayoutInflater.from(context);
        viewClickListener = v -> viewClick(v, String.valueOf(v.getTag()));
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return img_count;
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.layout_pager, view, false);
        ImageView myImage = (ImageView) myImageLayout.findViewById(R.id.imageView40);
        //myImage.setImageResource(data.get(position));
        final int i = position+1;

        Picasso.with(context).load(InterfaceClass.imgPth2+product_id+"_"+i+"_thumb.jpg").into(myImage);
            usingSimpleImage(myImage);

        view.addView(myImageLayout, 0);

        myImageLayout.setTag(InterfaceClass.imgPth2+product_id+"_"+i+"_thumb.jpg");
        myImageLayout.setOnClickListener(viewClickListener);

        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    protected abstract void viewClick(View view, String str);


    public void usingSimpleImage(ImageView imageView) {
        ImageAttacher mAttacher = new ImageAttacher(imageView);
        ImageAttacher.MAX_ZOOM = 4.0f; // times the current Size
        ImageAttacher.MIN_ZOOM = 1.0f; // Half the current Size
        MatrixChangeListener mMaListener = new MatrixChangeListener();
        mAttacher.setOnMatrixChangeListener(mMaListener);
        PhotoTapListener mPhotoTap = new PhotoTapListener();
        mAttacher.setOnPhotoTapListener(mPhotoTap);
    }

    private class PhotoTapListener implements ImageAttacher.OnPhotoTapListener {

        @Override
        public void onPhotoTap(View view, float x, float y) {
        }
    }

    private class MatrixChangeListener implements ImageAttacher.OnMatrixChangedListener {
        @Override
        public void onMatrixChanged(RectF rect) {

        }
    }

}


package com.aurget.buddha.Activity.RajorPayment;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.Utils.CustomPreference;
import com.aurget.buddha.Activity.Utils.ToolBar;
import com.aurget.buddha.R;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import static com.aurget.buddha.Activity.Commonhelper.changeTabsFont;

public class CheckoutActivity extends AppCompatActivity implements PaymentResultListener {

    private TabLayout tabLayoutHeader;
    private Button buttonConfirmOrder;
    private EditText editTextPayment;
    private String amunt = "";

    Activity activity = CheckoutActivity.this;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        findViews();
        listeners();

        if (getIntent().hasExtra("BuyNow")) {
            findViewById(R.id.scrollViewID).setVisibility(View.GONE);
            editTextPayment.setText(getIntent().getStringExtra("totlamount"));
            startPayment();
        }

        findViewById(R.id.bkImg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void findViews() {
        buttonConfirmOrder = findViewById(R.id.buttonConfirmOrder);
        editTextPayment = findViewById(R.id.editTextPayment);
        tabLayoutHeader = findViewById(R.id.tabLayoutHeader);
        tabLayoutHeader.setTabGravity(tabLayoutHeader.GRAVITY_FILL);
        tabLayoutHeader.addTab(tabLayoutHeader.newTab().setText("₹100"));
        tabLayoutHeader.addTab(tabLayoutHeader.newTab().setText("₹500"));
        tabLayoutHeader.addTab(tabLayoutHeader.newTab().setText("₹1000"));
        tabLayoutHeader.addTab(tabLayoutHeader.newTab().setText("₹2000"));
        changeTabsFont(CheckoutActivity.this,tabLayoutHeader);
        ToolBar.title(activity).setText("Payment");
    }

    public void listeners() {
        buttonConfirmOrder.setOnClickListener(view -> {
            if (editTextPayment.getText().toString().equals("")) {
                Commonhelper.showToastLong(CheckoutActivity.this, "Please fill payment");
                return;
            }
            startPayment();
        });

        ToolBar.backBtn(activity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        tabLayoutHeader.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                editTextPayment.setText(tab.getText().toString().replace("₹", ""));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

                editTextPayment.setText(tab.getText().toString().replace("₹", ""));
            }
        });

    }


    public void startPayment() {
        /**
         * You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;
        try {
            Checkout co = new Checkout();
            JSONObject options = new JSONObject();
            options.put("name", "AurGet");
            options.put("description", "");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://cdn.razorpay.com/logos/FUDfedL67L3Rb1_medium.png");
            options.put("currency", "INR");
            //options.put("order_id", "order_9A33XWu170gUtm");

            String payment = editTextPayment.getText().toString();
            double total = Double.parseDouble(payment);
            total = total * 100;
            options.put("amount", total);

            JSONObject preFill = new JSONObject();
            preFill.put("email", CustomPreference.readString(activity,CustomPreference.emailID,""));
            preFill.put("contact", CustomPreference.readString(activity,CustomPreference.mobileNO,""));
            options.put("prefill", preFill);
            co.open(activity, options);
        } catch (Exception e) {
            Commonhelper.showToastLong(activity, "Error in payment: " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        Intent intent = new Intent();
        intent.putExtra("res", "success");
        intent.putExtra("mount", editTextPayment.getText().toString());
        setResult(100, intent);
        finish();
    }

    @Override
    public void onPaymentError(int code, String response) {
        try {
            Intent intent = new Intent();
            intent.putExtra("res", response);
            setResult(100, intent);
            finish();
        } catch (Exception e) {
            Log.e("OnPaymentError", "Exception in onPaymentError", e);
            Intent intent = new Intent();
            intent.putExtra("res", e.getMessage());
            setResult(100, intent);
            finish();
        }
    }
}




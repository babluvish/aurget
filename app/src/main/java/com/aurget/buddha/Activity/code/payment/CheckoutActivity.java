/*
package code.payment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aurgetmart.mega.R;
import com.google.android.material.tabs.TabLayout;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import code.utils.AppUtils;
import code.view.BaseActivity;

public class CheckoutActivity  extends BaseActivity implements PaymentResultListener
{
    private String amunt = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        startPayment();
    }

    public void startPayment() {

        amunt = getIntent().getStringExtra("amount");
        final Activity activity = this;

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", "Aurget");
            options.put("description", "Demoing Charges");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "http://aurget.com/uploads/others/favicon.png");
            options.put("currency", "INR");
            //options.put("order_id", "order_9A33XWu170gUtm");

            String payment = amunt;
            double total = Double.parseDouble(payment);
            total = total * 100;
            options.put("amount", total);

            JSONObject preFill = new JSONObject();
            preFill.put("email", getIntent().getStringExtra("email"));
            preFill.put("contact", getIntent().getStringExtra("phone"));
            options.put("prefill", preFill);
            co.open(activity, options);
        } catch (Exception e) {
            AppUtils.showToastSort(activity, "Error in payment: " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        Intent intent = new Intent();
        intent.putExtra("res", "success");
        intent.putExtra("mount", amunt);
        setResult(100, intent);
        finish();
    }

    @Override
    public void onPaymentError(int code, String response) {
        try {
            Intent intent = new Intent();
            intent.putExtra("res", response);
            setResult(100, intent);
            finish();
        } catch (Exception e) {
            Log.e("OnPaymentError", "Exception in onPaymentError", e);
            Intent intent = new Intent();
            intent.putExtra("res", e.getMessage());
            setResult(100, intent);
            finish();
        }
    }
}
*/

package com.aurget.buddha.Activity.code.School;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aurget.buddha.Activity.code.School.SchoolHistoryActivity;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;

public class SchoolMainActivity extends BaseActivity implements View.OnClickListener {

    //ImageView
    ImageView ivCart,ivMenu,ivSearch;

    RelativeLayout rrRecharge, rrPayHistory;

    //TextView
    TextView tvHeaderText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_main);

        findId();
    }

    private void findId() {

        //ImageView
        ivCart =  findViewById(R.id.ivCart);
        ivMenu =  findViewById(R.id.iv_menu);
        ivSearch =  findViewById(R.id.searchmain);

        //TextView
        tvHeaderText    =  findViewById(R.id.tvHeaderText);

        //RelativeLayout
        rrRecharge = findViewById(R.id.rrRecharge);
        rrPayHistory =findViewById(R.id.rrHistory);

        ivCart.setVisibility(View.INVISIBLE);
        ivSearch.setVisibility(View.INVISIBLE);

        ivMenu.setImageResource(R.drawable.ic_back);

        tvHeaderText.setText(getString(R.string.school_fees));

        ivCart.setOnClickListener(this);
        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        rrRecharge.setOnClickListener(this);
        rrPayHistory.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.iv_menu:
                finish();
                return;

            case R.id.rrRecharge:
                startActivity(new Intent(getBaseContext(), SchoolFeesActivity.class));
                return;

            case R.id.rrHistory:
                startActivity(new Intent(getBaseContext(), SchoolHistoryActivity.class));
                return;
        }
    }
}

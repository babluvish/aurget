package com.aurget.buddha.Activity.code.Database;

public class TableClass {
    static final String SQL_CREATE_CLASSROOM = ("CREATE TABLE classroom (" +
            classroomColumn.class_id + " VARCHAR,"
            + classroomColumn.class_name + " VARCHAR,"
            + classroomColumn.branch_id + " VARCHAR,"
            + classroomColumn.status + " VARCHAR" + ")");
    public static final String classroom = "classroom";

    public enum classroomColumn {
        class_id,
        class_name,
        branch_id,
        status
    }
}

package com.aurget.buddha.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.aurget.buddha.Activity.code.Database.DatabaseController;
import com.aurget.buddha.Activity.code.Product.ProductDetailsActivity;
import com.google.android.gms.maps.model.Dash;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.viewpager.widget.ViewPager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;

import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.AddtoCart.AddCartListActivity;
import com.aurget.buddha.Activity.Outlet.Outlet_details;
import com.aurget.buddha.Activity.RajorPayment.CheckoutActivity;
import com.aurget.buddha.Activity.SolderManagment.AddSolderActivity;
import com.aurget.buddha.Activity.SolderManagment.MyProfileList;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.Utils.CustomPreference;
import com.aurget.buddha.Activity.Utils.SliderTimer;
import com.aurget.buddha.Activity.YourBusiness.BusinessReport;
import com.aurget.buddha.Activity.YourBusiness.YourBusinessTeam;
import com.aurget.buddha.Activity.YourOrder.YourOrder;
import com.aurget.buddha.Activity.YourReturn.YourReturn;
import com.aurget.buddha.Activity.about.AboutActivity;
import com.aurget.buddha.Activity.categories.Categories;
import com.aurget.buddha.Activity.categories.SubCategoryDetails2;
import com.aurget.buddha.Activity.categories.SubCategoryDetailsActivity;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Fragment.DashBoardFragment;
import com.aurget.buddha.Activity.code.service.GPSTracker;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import static com.aurget.buddha.Activity.Commonhelper.changeTabsFont;
import static com.aurget.buddha.Activity.Commonhelper.discountAmt;
import static com.aurget.buddha.Activity.Commonhelper.getScreenWidth;
import static com.aurget.buddha.Activity.Commonhelper.openBrowser;
import static com.aurget.buddha.Activity.Commonhelper.roudOff;
import static com.aurget.buddha.Activity.Commonhelper.shareApp;
import static com.aurget.buddha.Activity.InterfaceClass.token_key2;
import static com.aurget.buddha.Activity.Utils.Constnt.appVersion;
import static com.aurget.buddha.Activity.Utils.Constnt.playStroremessage;
import static com.aurget.buddha.Activity.Utils.Constnt.recentViewJAl;
import static com.aurget.buddha.Activity.code.Database.AppSettings.dob;
import static com.aurget.buddha.Activity.code.Database.AppSettings.mainWallet;
import static java.lang.Double.parseDouble;


public class Main2Activity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnTouchListener,
        ViewTreeObserver.OnScrollChangedListener, SwipeRefreshLayout.OnRefreshListener, LocationListener {

    ViewPager viewPager, viewPager2, viewPager4;
    TabLayout indicator, indicator2, tabLayoutHeader;

    List<String> color;
    List<String> colorName;
    RecyclerView recyclerView, recycleView2, recycleView3, recycleView4, recycleView5, recycleView6, recycleView7;
    private LinearLayoutManager horizontalLayoutManager;
    private ArrayList<JSONObject> jsonObjectal = new ArrayList<>();
    ImageView imgHeader;
    public static ImageView imageView;
    float divideLine = 2.5f;
    String Res;
    TextView crtCountTv, nameTv2;
    static TextView ed2;

    AppBarLayout tol, tol2;
    ScrollView scrollView;
    RelativeLayout recentViewRl, latesstFeatrueRl;
    SwipeRefreshLayout mSwipeRefreshLayout;
    EditText searchEt;

    private int[] tabIcons = {
            R.drawable.your_order,
            R.drawable.growths,
            R.drawable.wallet_cshbck,
            R.drawable.user,
    };

    private void setupTabIcons() {
        if (Constnt.hideMLM) {
            tabLayoutHeader.getTabAt(0).setIcon(tabIcons[0]);
            tabLayoutHeader.getTabAt(1).setIcon(tabIcons[1]);
            tabLayoutHeader.getTabAt(2).setIcon(tabIcons[2]);
            tabLayoutHeader.getTabAt(3).setIcon(tabIcons[3]);
        } else {
            tabLayoutHeader.getTabAt(0).setIcon(tabIcons[0]);
            tabLayoutHeader.getTabAt(1).setIcon(tabIcons[1]);
            tabLayoutHeader.getTabAt(2).setIcon(tabIcons[2]);
        }
    }

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 2;
    private LocationManager locationManager;
    private String provider;
    private Location loc;

    private static final int REQUEST_CODE_PERMISSION = 2;
    String mPermission = Manifest.permission.ACCESS_FINE_LOCATION;

    // GPSTracker class
    GPSTracker gps;

    private String mLastUpdateTime;

    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

    private static final int REQUEST_CHECK_SETTINGS = 100;
    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;

    // boolean flag to toggle the ui
    private Boolean mRequestingLocationUpdates;
    private static final String TAG = "google";

    private void init() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

                updateLocationUI(1);
            }
        };

        mRequestingLocationUpdates = false;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    private void restoreValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("is_requesting_updates")) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean("is_requesting_updates");
            }

            if (savedInstanceState.containsKey("last_known_location")) {
                mCurrentLocation = savedInstanceState.getParcelable("last_known_location");
            }

            if (savedInstanceState.containsKey("last_updated_on")) {
                mLastUpdateTime = savedInstanceState.getString("last_updated_on");
            }
        }

        updateLocationUI(1);
    }

    private void updateLocationUI(int i) {
        /*if (mCurrentLocation != null)
        {
            Log.d("cccc", "Lat: " + mCurrentLocation.getLatitude() + ", " + "Lng: " + mCurrentLocation.getLongitude());
            Log.d("ssss", mLastUpdateTime);

            if (i==0) {
                getAddress(Main2Activity.this, mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            }
        }*/

        if (i == 0) {
            try {
                getAddress(Main2Activity.this, mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            } catch (Exception e) {
                ed2.performClick();
            }
        }
    }

    public String getAddress(Context ctx, double latitude, double longitude) {
        StringBuilder result = new StringBuilder();
        try {
            Log.e("tag---", String.valueOf(latitude));
            Log.e("tag---", String.valueOf(longitude));
            Geocoder geocoder = new Geocoder(ctx, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                String locality = address.getLocality();
                String city = address.getCountryName();
                String region_code = address.getCountryCode();
                String zipcode = address.getPostalCode();
                double lat = address.getLatitude();
                double lon = address.getLongitude();
                result.append(locality + " ");
                result.append(city + " " + region_code + " ");
                result.append(zipcode);
                Log.e("tag---", address.toString());

                check_delivery_location(zipcode);

            }
        } catch (Exception e) {
            Log.e("tag", "errororororor");
            ed2.performClick();
        }
        return result.toString();
    }

    void check_delivery_location(String pinCpde) {
        new AbstrctClss(Main2Activity.this, ConstntApi.check_delivery_location(Main2Activity.this, pinCpde), "p", true) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s);
                    String status = jsonArray.optJSONObject(0).optString("Status");
                    String sms = jsonArray.optJSONObject(0).optString("message");
                    String password = jsonArray.optJSONObject(0).optString("password");

                    if (status.equalsIgnoreCase("false")) {
                        Commonhelper.showToastLong(Main2Activity.this, sms);
                        stopLocationUpdates();
                        new Handler().postDelayed(() -> {
                            LoginApi(password);
                        }, 0);
                    } else {
                        LoginApi(password);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    public void stopLocationUpdates() {
        // Removing location updates
        mFusedLocationClient
                .removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, task -> Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT));
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            getLocation();
        }
    }

    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull
            int[] grantResults) {
        if (requestCode == 123) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLocation();
            } else {
                Toast.makeText(this, "Location permission not granted, " + "restart the app if you want the", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mRequestingLocationUpdates) {
            // pausing location updates
            stopLocationUpdates();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void startLocationUpdates(int i) {
        //Commonhelper.showToastLong(mActivity,""+i);

        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, locationSettingsResponse -> {
                    Log.i(TAG, "All location settings are satisfied.");
                    //Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();
                    //noinspection MissingPermission
                    mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                    if (i == 0) {
                        updateLocationUI(0);
                    }
                })
                .addOnFailureListener(this, e -> {
                    int statusCode = ((ApiException) e).getStatusCode();
                    switch (statusCode) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " + "location settings ");
                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the
                                // result in onActivityResult().
                                ResolvableApiException rae = (ResolvableApiException) e;
                                rae.startResolutionForResult(Main2Activity.this, REQUEST_CHECK_SETTINGS);

                                //startLocationUpdates(i);
                                if (i == 0) {
                                    ed2.performClick();
                                }

                            } catch (IntentSender.SendIntentException sie) {
                                Log.i(TAG, "PendingIntent unable to execute request.");
                                if (i == 0) {
                                    ed2.performClick();
                                }
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            String errorMessage = "Location settings are inadequate, and cannot be " + "fixed here. Fix in Settings.";
                            Log.e(TAG, errorMessage);
                            Toast.makeText(Main2Activity.this, errorMessage, Toast.LENGTH_LONG).show();
                    }
                    if (i == 0) {
                        updateLocationUI(0);
                    }
                });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("is_requesting_updates", mRequestingLocationUpdates);
        outState.putParcelable("last_known_location", mCurrentLocation);
        outState.putString("last_updated_on", mLastUpdateTime);
    }

    private void LoginApi(String password) {
        Dialog dialog = Commonhelper.loadDialog(Main2Activity.this);
        Log.v("LoginApi", code.utils.AppUrls.Login);
        String url = "";
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            url = code.utils.AppUrls.Login;
            json_data.put("phone_no", CustomPreference.readString(Main2Activity.this, CustomPreference.mobileNO, ""));
            json_data.put("password", password);
            json_data.put("googleid", "");
            json_data.put("fb_id", "");
            json_data.put("user_type_login", "1");
            json_data.put("fcm_id", "");
            json_data.put("device_id", AppUtils.getDeviceID(getApplicationContext()));
            json.put(AppConstants.result, json_data);
            Log.v("LoginApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(url)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata(response, dialog);
                    }

                    @Override
                    public void onError(ANError error) {
                        dialog.dismiss();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            Commonhelper.showToastLong(Main2Activity.this, String.valueOf(error.getErrorCode()));
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            Commonhelper.showToastLong(Main2Activity.this, String.valueOf(error.getErrorDetail()));
                        }
                    }
                });
    }

    private void parseJsondata(JSONObject response, Dialog dialog)
    {
        Log.d("response", response.toString());
        dialog.dismiss();
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppSettings.putString(AppSettings.userId, jsonObject.getString("id"));
                AppSettings.putString(AppSettings.name, jsonObject.getString("username"));
                AppSettings.putString(AppSettings.email, jsonObject.getString("email_id"));
                AppSettings.putString(AppSettings.gender, jsonObject.getString("gender"));
                AppSettings.putString(dob, jsonObject.getString("dob"));
                AppSettings.putString(AppSettings.mobile, jsonObject.getString("phone_no"));
                AppSettings.putString(AppSettings.usertype, jsonObject.getString("user_type_login"));
                AppSettings.putString(AppSettings.user_pic, jsonObject.getString("profile_pic"));

                if (isSharedGrocery)
                {
                    Constnt.prodcutID = arl.get(0);
                    Intent intent = new Intent(mActivity, DashBoardFragment.class);
                    startActivity(intent);
                    isSharedGrocery = false;
                    //Commonhelper.sop("product_id=" + arl.get(0)+"="+arl.get(1)+"="+arl.get(2));
                }
                else
                {
                    startActivity(new Intent(getBaseContext(), DashBoardFragment.class));
                }
            } else {
                Commonhelper.showToastLong(Main2Activity.this, jsonObject.getString("res_msg"));
            }
        } catch (Exception e) {
            Commonhelper.showToastLong(Main2Activity.this, String.valueOf(e));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        // Initialize the Mobile Ads SDK
        /*MobileAds.initialize(this, getString(R.string.admob_app_id));
        // Find Banner ad
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        // Display Banner ad
        mAdView.loadAd(adRequest);
        */
        init();

        restoreValuesFromBundle(savedInstanceState);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        // drawer.addDrawerListener(toolbar);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        findViewById(R.id.bkImg).setOnClickListener(view -> {
            DrawerLayout drawer18 = findViewById(R.id.drawer_layout);
            drawer18.openDrawer(GravityCompat.START);
        });

        findViewById(R.id.imghmb1).setOnClickListener(view -> {
            if (Integer.parseInt(crtCountTv.getText().toString().trim().replace("+", "")) < 1) {
                Intent intent = new Intent(Main2Activity.this, EmptyCartActivity.class);
                startActivity(intent);
            } else {
                Intent intent = new Intent(Main2Activity.this, AddCartListActivity.class);
                intent.putExtra("res", crtDetils2);
                startActivity(intent);
            }
        });

        if (!Constnt.hideMLM) {
            findViewById(R.id.outlet).setVisibility(View.GONE);
            findViewById(R.id.yrBusinss).setVisibility(View.GONE);
        }

        imgHeader = findViewById(R.id.imgHeader);
        //letestFetureBigImg = findViewById(R.id.letestFetureBigImg);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        indicator = (TabLayout) findViewById(R.id.indicator);

        searchEt = findViewById(R.id.searchEt);
        viewPager2 = (ViewPager) findViewById(R.id.viewPager2);
        viewPager4 = (ViewPager) findViewById(R.id.viewPager4);
        indicator2 = (TabLayout) findViewById(R.id.indicator2);
        recentViewRl = findViewById(R.id.recentViewRl);
        latesstFeatrueRl = findViewById(R.id.latesstFeatrueRl);
        mSwipeRefreshLayout = findViewById(R.id.swipeToRefresh);
        nameTv2 = findViewById(R.id.nameTv2);
        ed2 = findViewById(R.id.ed2);
        imageView = findViewById(R.id.imageView);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        // tol2 = findViewById(R.id.tol2);
        //tol = findViewById(R.id.tol);

        tabLayoutHeader = (TabLayout) findViewById(R.id.tabLayoutHeader);
        crtCountTv = findViewById(R.id.crtCountTv);

        recyclerView = findViewById(R.id.recycleView);
        recycleView2 = findViewById(R.id.recycleView2);
        recycleView3 = findViewById(R.id.recycleView3);
        recycleView4 = findViewById(R.id.recycleView4);
        recycleView5 = findViewById(R.id.recycleView5);
        recycleView6 = findViewById(R.id.recycleView6);
        recycleView7 = findViewById(R.id.recycleView7);

        scrollView = findViewById(R.id.scrollView);
        scrollView.setOnTouchListener(this);
        scrollView.getViewTreeObserver().addOnScrollChangedListener(this);
        color = new ArrayList<>();

        //imgHeader.setBackgroundResource(R.drawable.logo_big);

//        color.add(R.drawable.ic_add_shopping_cart_black_24dp);
//        color.add(R.drawable.ic_menu_camera);
//        color.add(R.drawable.ic_menu_send);

        colorName = new ArrayList<>();
        colorName.add("RED");
        colorName.add("GREEN");
        colorName.add("BLUE");


//        viewPager.setAdapter(new SliderAdapter(this, color, colorName));
//        indicator.setupWithViewPager(viewPager, true);

        timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(Main2Activity.this, viewPager, color), SliderTimer.delay, SliderTimer.period);
        timer.scheduleAtFixedRate(new SliderTimer(Main2Activity.this, viewPager2, color), SliderTimer.delay, SliderTimer.period);
        timer.scheduleAtFixedRate(new SliderTimer(Main2Activity.this, viewPager4, color), SliderTimer.delay, SliderTimer.period);

        horizontalLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(horizontalLayoutManager);
        recyclerView.setAdapter(new MyRecyclerAdapter(null));

        GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recycleView2.setLayoutManager(manager);

        LinearLayoutManager horizontalLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycleView3.setLayoutManager(horizontalLayoutManager2);
        recycleView3.setAdapter(new MyRecyclerAdapterTodayDeal(null));

        LinearLayoutManager horizontalLayoutManager3 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycleView4.setLayoutManager(horizontalLayoutManager3);
        recycleView4.setAdapter(new MyRecyclerAdapterFastMoving(null));

        LinearLayoutManager horizontalLayoutManager4 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycleView5.setLayoutManager(horizontalLayoutManager4);
        recycleView5.setAdapter(new MyRecyclerAdapterLatestArrivel(null));

        LinearLayoutManager horizontalLayoutManager5 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycleView6.setLayoutManager(horizontalLayoutManager5);
        recycleView6.setAdapter(new MyRecyclerAdapterLatestFeatureProduct(null));

        LinearLayoutManager horizontalLayoutManager6 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycleView7.setLayoutManager(horizontalLayoutManager6);
        recycleView7.setAdapter(new MyRecyclerAdapterRecentllyView(null));

        tabLayoutHeader.addTab(tabLayoutHeader.newTab().setText(getResources().getString(R.string.yourOrder)));
        tabLayoutHeader.addTab(tabLayoutHeader.newTab().setText(getResources().getString(R.string.yourBusiness)));
        tabLayoutHeader.addTab(tabLayoutHeader.newTab().setText(getResources().getString(R.string.wallet_cashback)));
        tabLayoutHeader.addTab(tabLayoutHeader.newTab().setText(getResources().getString(R.string.yourPrpfile)));

        tabLayoutHeader.setTabGravity(tabLayoutHeader.GRAVITY_FILL);
        nameTv2.setText(CustomPreference.readString(Main2Activity.this, CustomPreference.userName, ""));

        setupTabIcons();
        changeTabsFont(Main2Activity.this, tabLayoutHeader);

        /*ShowcaseConfig showcaseConfig = new ShowcaseConfig(this);
        showcaseConfig.setDelay(500);
        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this, "1");
        sequence.setConfig(showcaseConfig);
        sequence.addSequenceItem(((ViewGroup) tabLayoutHeader.getChildAt(0)).getChildAt(0), "About " + getResources().getString(R.string.yourOrder), "Next");
        sequence.addSequenceItem(((ViewGroup) tabLayoutHeader.getChildAt(0)).getChildAt(1), "About " + getResources().getString(R.string.yourReturn), "Next");
        sequence.addSequenceItem(((ViewGroup) tabLayoutHeader.getChildAt(0)).getChildAt(2), "About " + getResources().getString(R.string.yourBusiness), "Finish");*/
        //sequence.show();

        new Handler().postDelayed(() -> {
            hitPi();
            hitPi2();
            hitPi3();
            fastMovingapi();
            latestArrival();
            letestFeturePi();
            recentViewPi();
            deal_product();
            fetch_bank_details();
        }, 1000);


        Intent intent_ = getIntent();

        if (intent_ != null && intent_.getData() != null)
        {
            dashboardApi(intent_.getData().toString());
        }

        /*viewPager.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                Log.d("sadasd", "move");
                timer.cancel();
            } else {
                Log.d("sadasd", "move+++");
            }
            return false;
        });*/

        tabLayoutHeader.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    Intent intent = new Intent(Main2Activity.this, YourOrder.class);
                    startActivity(intent);
                } else if (tab.getPosition() == 1) {
                    yourBusinessDialog();
                } else if (tab.getPosition() == 2) {
                    add_money_to_walletdilog();
                } else if (tab.getPosition() == 3) {
                    Intent intent = new Intent(Main2Activity.this, MyProfileList.class);
                    startActivityForResult(intent, 100);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    Intent intent = new Intent(Main2Activity.this, YourOrder.class);
                    startActivity(intent);
                } else if (tab.getPosition() == 1) {
                    yourBusinessDialog();
                } else if (tab.getPosition() == 2) {
                    add_money_to_walletdilog();
                } else if (tab.getPosition() == 3) {
                    Intent intent = new Intent(Main2Activity.this, MyProfileList.class);
                    startActivityForResult(intent, 100);
                }
            }
        });

        searchEt.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                searchEt.setCursorVisible(true);
            }
        });

        searchEt.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                //searchData(searchEt.getText().toString());
                Intent intent = new Intent(Main2Activity.this, SubCategoryDetailsActivity.class);
                intent.putExtra("search", searchEt.getText().toString());
                startActivity(intent);
                return true;
            }
            return false;
        });

        findViewById(R.id.rel).setOnClickListener(view -> {
//            Intent intent = new Intent(Main2Activity.this, DashBoardFragment.class);
//            startActivity(intent);
        });

        recentViewRl.setOnClickListener(view -> {
            Intent intent = new Intent(Main2Activity.this, RecentViewListActivity.class);
            intent.putExtra("pos", "recentView");
            startActivity(intent);
        });

        latesstFeatrueRl.setOnClickListener(view -> {
            Intent intent = new Intent(Main2Activity.this, RecentViewListActivity.class);
            intent.putExtra("pos", "latesstFeatrue");
            startActivity(intent);
        });

        findViewById(R.id.fastMovingRl).setOnClickListener(view -> {
            Intent intent = new Intent(Main2Activity.this, RecentViewListActivity.class);
            intent.putExtra("pos", "fastMoving");
            startActivity(intent);
        });

        findViewById(R.id.saleonAurget).setOnClickListener(view -> openBrowser(Main2Activity.this, "http://www.aurget.com/home/vendor_logup/registration"));

        findViewById(R.id.viewAllTodayDealTv).setOnClickListener(view -> {
            Intent intent = new Intent(Main2Activity.this, RecentViewListActivity.class);
            intent.putExtra("pos", "todayDeal");
            startActivity(intent);
        });

        findViewById(R.id.latestArrivalTv).setOnClickListener(view -> {
            Intent intent = new Intent(Main2Activity.this, RecentViewListActivity.class);
            intent.putExtra("pos", "latest_arrival");
            startActivity(intent);
        });

        findViewById(R.id.outlet).setOnClickListener(view -> {
            Intent intent = new Intent(Main2Activity.this, Outlet_details.class);
            intent.putExtra("pos", "outlet");
            startActivity(intent);
        });

        findViewById(R.id.order).setOnClickListener(view -> {

            Intent intent = new Intent(Main2Activity.this, YourOrder.class);
            startActivity(intent);

            DrawerLayout drawer1 = findViewById(R.id.drawer_layout);
            drawer1.closeDrawer(GravityCompat.START);
        });

        findViewById(R.id.Return).setOnClickListener(view -> {
            Intent intent = new Intent(Main2Activity.this, YourReturn.class);
            startActivity(intent);

            DrawerLayout drawer12 = findViewById(R.id.drawer_layout);
            drawer12.closeDrawer(GravityCompat.START);
        });

        findViewById(R.id.yrBusinss).setOnClickListener(view -> {
            yourBusinessDialog();

            DrawerLayout drawer13 = findViewById(R.id.drawer_layout);
            drawer13.closeDrawer(GravityCompat.START);
        });

        findViewById(R.id.yourProfileRl).setOnClickListener(view -> {
            Intent intent = new Intent(Main2Activity.this, MyProfileList.class);
            startActivityForResult(intent, 100);

            DrawerLayout drawer14 = findViewById(R.id.drawer_layout);
            drawer14.closeDrawer(GravityCompat.START);
        });

        findViewById(R.id.customerService).setOnClickListener(view -> {
            Intent intent = new Intent(Main2Activity.this, SuportActivity.class);
            startActivity(intent);
            DrawerLayout drawer15 = findViewById(R.id.drawer_layout);
            drawer15.closeDrawer(GravityCompat.START);
        });

        findViewById(R.id.inviteBusiness).setOnClickListener(view -> {
            shareApp(Main2Activity.this, Constnt.shareImageFile, Constnt.share_content);
            /*DrawerLayout drawer16 = findViewById(R.id.drawer_layout);
            drawer16.closeDrawer(GravityCompat.START);*/
        });

        findViewById(R.id.businessPlanRl).setOnClickListener(view -> {
            Commonhelper.openBrowser(Main2Activity.this, "http://aurget.com/home/page/AurGet_Business_Plan");
            DrawerLayout drawer16 = findViewById(R.id.drawer_layout);
            drawer16.closeDrawer(GravityCompat.START);
        });

        findViewById(R.id.AboutAurGet).setOnClickListener(view -> {
            Dialog dialog = new Dialog(Main2Activity.this);
            dialog.setContentView(R.layout.about);
            RelativeLayout SaleonAurGet = dialog.findViewById(R.id.SaleonAurGet);
            SaleonAurGet.setVisibility(View.GONE);
            RelativeLayout AboutAurGet = dialog.findViewById(R.id.AboutAurGet);
            AboutAurGet.setVisibility(View.GONE);
            RelativeLayout policy = dialog.findViewById(R.id.policy);
            RelativeLayout AurGetTerms = dialog.findViewById(R.id.AurGetTerms);
            ImageView img3 = dialog.findViewById(R.id.img3);
            ImageView img4 = dialog.findViewById(R.id.img4);

            img3.setImageResource(0);
            img3.setBackgroundResource(R.drawable.turm);

            img4.setImageResource(0);
            img4.setBackgroundResource(R.drawable.privacy_policy);

            AurGetTerms.setOnClickListener(view12 -> {

                Intent intent = new Intent(Main2Activity.this, AboutActivity.class);
                intent.putExtra("pos", "1");
                startActivity(intent);
            });

            policy.setOnClickListener(view1 -> {

                Intent intent = new Intent(Main2Activity.this, AboutActivity.class);
                intent.putExtra("pos", "2");
                startActivity(intent);
            });

            dialog.show();

            DrawerLayout drawer17 = findViewById(R.id.drawer_layout);
            drawer17.closeDrawer(GravityCompat.START);
        });

        findViewById(R.id.logout).setOnClickListener(view -> {
            showDialog();
            //showAlertDialogBox("Logout","Do you want to logout?");
            DrawerLayout drawer19 = findViewById(R.id.drawer_layout);
            drawer19.closeDrawer(GravityCompat.START);
        });

        ed2.setOnClickListener(v -> {
            Dialog dialog = Commonhelper.loadDialog(mActivity);
            Dexter.withActivity(this)
                    .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    .withListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted(PermissionGrantedResponse response) {
                            //Commonhelper.showToastLong(mActivity,"1234");
                            mRequestingLocationUpdates = true;
                            startLocationUpdates(0);
                            dialog.dismiss();
                        }

                        @Override
                        public void onPermissionDenied(PermissionDeniedResponse response) {
                            if (response.isPermanentlyDenied()) {
                                // open device settings when the permission is
                                // denied permanently
                                //progressDialog.dismiss();
                                //openSettings();
                                dialog.dismiss();
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(com.karumi.dexter.listener.PermissionRequest permission, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    }).check();
        });

        allowPermission();
    }

    private void allowPermission() {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        //mRequestingLocationUpdates = true;
                        startLocationUpdates(1);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(com.karumi.dexter.listener.PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    public void showDialog() {
        Button yes, no;
        final Dialog dialoglog = new Dialog(Main2Activity.this);
        dialoglog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialoglog.setContentView(R.layout.logout_dialog);
        Window window = dialoglog.getWindow();
        window.setLayout(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
        dialoglog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        yes = (Button) dialoglog.findViewById(R.id.yes);
        no = (Button) dialoglog.findViewById(R.id.no);
        dialoglog.show();

        yes.setOnClickListener(view -> {
            try {
                CustomPreference.removeAll(Main2Activity.this);
                Intent intent = new Intent(Main2Activity.this, Login.class);
                startActivity(intent);
                dialoglog.dismiss();
                finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
            dialoglog.dismiss();
        });

        no.setOnClickListener(view -> dialoglog.dismiss());
    }

    public void playStoreDialog() {
        Button yes, no;
        final Dialog dialoglog = new Dialog(Main2Activity.this);
        dialoglog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialoglog.setContentView(R.layout.playstore_sms);
        Window window = dialoglog.getWindow();
        window.setLayout(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
        dialoglog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialoglog.setCanceledOnTouchOutside(false);
        yes = (Button) dialoglog.findViewById(R.id.yes);
        no = (Button) dialoglog.findViewById(R.id.no);
        TextView sms = dialoglog.findViewById(R.id.smsTv);
        CheckBox checkBox = dialoglog.findViewById(R.id.checkbox);
        checkBox.setVisibility(View.GONE);
        dialoglog.show();

        sms.setText(playStroremessage);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    CustomPreference.writeString(Main2Activity.this, CustomPreference.playStroeCancel, "true");
                } else {
                    CustomPreference.writeString(Main2Activity.this, CustomPreference.playStroeCancel, "false");
                }
            }
        });

        yes.setOnClickListener(view -> {
            try {
                Commonhelper.openPlayStore(mActivity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            dialoglog.dismiss();
        });

        no.setOnClickListener(view -> dialoglog.dismiss());
    }

    private void add_money_to_walletdilog() {
        Dialog dialog = new Dialog(Main2Activity.this);
        dialog.setContentView(R.layout.add_money_to_wallet);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        final TextView amountTv = dialog.findViewById(R.id.Wallet_BalanceTv);
        final TextView walletAmtVt = dialog.findViewById(R.id.walletAmtVt);
        Button doneBtn = dialog.findViewById(R.id.doneBtn);

        doneBtn.setOnClickListener(view -> {
            Intent intent = new Intent(Main2Activity.this, CheckoutActivity.class);
            startActivityForResult(intent, 100);
        });

        dialog.show();

        new AbstrctClss(Main2Activity.this, ConstntApi.wallet_ballanceUrl(Main2Activity.this), "p", true) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s);
                    String walletAmt = jsonArray.optJSONObject(0).optString("Wallet_Balance");
                    //wlletTv.setText("₹" + walletAmt);
                    amountTv.setText("₹" + walletAmt);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };

        new AbstrctClss(Main2Activity.this, ConstntApi.unapprove_balance(Main2Activity.this), "p", true) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s);
                    String walletAmt = jsonArray.optJSONObject(0).optString("unapprove_balance");
                    //wlletTv.setText("₹" + walletAmt);
                    walletAmtVt.setText("₹" + walletAmt);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    void deal_product() {
        recycleView3.setAdapter(new MyRecyclerAdapterTodayDeal(Constnt.deal_product));
        /*new AbstrctClss(Main2Activity.this, ConstntApi.deal_product(Main2Activity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    Log.d("deal_product", s);
                    ArrayList<JSONObject> jsonObjects = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray(s);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjects.add(jsonArray.optJSONObject(i));
                    }
                    recycleView3.setAdapter(new MyRecyclerAdapterTodayDeal(jsonObjects));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Log.d("bobobo", data.getStringExtra("res"));
            if (data.hasExtra("res")) {
                Commonhelper.showToastLong(Main2Activity.this, data.getStringExtra("res"));

                if (data.getStringExtra("res").equals("success")) {
                    add_user_balance(CustomPreference.readString(Main2Activity.this, CustomPreference.e_id, ""), data.getStringExtra("mount"));
                } else {
                    Commonhelper.showToastLong(Main2Activity.this, data.getStringExtra("res"));
                }

                wallet_transaction(CustomPreference.readString(Main2Activity.this, CustomPreference.e_id, ""), data.getStringExtra("mount"), data.getStringExtra("res"));

                Commonhelper.sop("res" + data.getStringExtra("res"));
                Commonhelper.sop("mount" + data.getStringExtra("mount"));
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
        return true;
    }

    private void yourBusinessDialog() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.yr_business);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        RelativeLayout bankRl = dialog.findViewById(R.id.bankRl);
        RelativeLayout adhrcrdRl = dialog.findViewById(R.id.adhrcrdRl);
        RelativeLayout panCrdRl = dialog.findViewById(R.id.panCrdRl);
        final LinearLayout childLL = dialog.findViewById(R.id.childLL);

        adhrcrdRl.setOnClickListener(view -> {
            Intent intent = new Intent(Main2Activity.this, AddSolderActivity.class);
            startActivity(intent);
        });

        panCrdRl.setOnClickListener(view -> {

            Intent intent = new Intent(Main2Activity.this, YourBusinessTeam.class);
            startActivity(intent);
        });

        bankRl.setOnClickListener(view -> {

            Intent intent = new Intent(Main2Activity.this, BusinessReport.class);
            startActivity(intent);

        });

        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        /*if (mRequestingLocationUpdates && checkPermissions()) {
            startLocationUpdates();
        }
        updateLocationUI();*/

        crtDetils();

        //String amount = "10";
        //String login_id = CustomPreference.readString(Main2Activity.this, CustomPreference.e_id, "");
        //Log.d("wallet", InterfaceClass.ipAddress4 + "add_user_balance?amount=" + amount + "&login_id=" + login_id + "&token_key_auth=" + token_key2);
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    public void showAlertDialogBox(String title, String msg) {
        final Dialog dialog = new Dialog(Main2Activity.this);
        dialog.setContentView(R.layout.logout_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        final Button noLL = dialog.findViewById(R.id.no);
        final Button yesLl = dialog.findViewById(R.id.yes);

        noLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        yesLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomPreference.removeAll(Main2Activity.this);
                Intent intent = new Intent(Main2Activity.this, Login.class);
                startActivity(intent);
                dialog.dismiss();
                finish();
            }
        });

        dialog.show();

    }

    //https://api.postalpincode.in/pincode/273014
    private void fetch_bank_details() {
        try {
            JSONArray jsonArray = Constnt.get_profile;
            //Picasso.with(Main2Activity.this).load(InterfaceClass.ipAddress3+jsonArray.optJSONObject(0).optString("image_path")).into(imageView);
            Constnt.image_path = jsonArray.optJSONObject(0).optString("image_path");
            String shareImage = jsonArray.optJSONObject(0).optString("image_share");
            Constnt.share_content = jsonArray.optJSONObject(0).optString("share_content");
            Constnt.appVersion = jsonArray.optJSONObject(0).optString("version");
            Constnt.playStroremessage = jsonArray.optJSONObject(0).optString("message");
            try {
                //Bitmap image = getBitmapFromURL(shareImage);
                //Constnt.shareImageFile = bitmapconvert(image, mActivity);
                StrictMode.ThreadPolicy policy1 = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy1);

                new GetImageFromUrl().execute(shareImage);
                Commonhelper.sop("profile" + shareImage);
                //Commonhelper.sop("profile" + Constnt.shareImageFile.getCanonicalPath());
            } catch (Exception e) {
                Commonhelper.sop("profile" + "erroror");
            }
            if (TextUtils.isEmpty(Constnt.image_path) || Constnt.image_path.isEmpty() || Constnt.image_path == null || Constnt.image_path.equals("null")) {
                //do stuff
            } else {
                Commonhelper.picasso_(Main2Activity.this, Constnt.image_path,
                        imageView, R.drawable.ic_account_circle_black_24dp);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*new AbstrctClss(mActivity, ConstntApi.get_profile(mActivity), "g", false) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s);
                    //Picasso.with(Main2Activity.this).load(InterfaceClass.ipAddress3+jsonArray.optJSONObject(0).optString("image_path")).into(imageView);
                    Constnt.image_path = jsonArray.optJSONObject(0).optString("image_path");
                    String shareImage = jsonArray.optJSONObject(0).optString("image_share");
                    Constnt.share_content = jsonArray.optJSONObject(0).optString("share_content");
                    Constnt.appVersion = jsonArray.optJSONObject(0).optString("version");
                    Constnt.playStroremessage = jsonArray.optJSONObject(0).optString("message");
                    try {
                        //Bitmap image = getBitmapFromURL(shareImage);
                        //Constnt.shareImageFile = bitmapconvert(image, mActivity);
                        StrictMode.ThreadPolicy policy1 = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                        StrictMode.setThreadPolicy(policy1);

                        new GetImageFromUrl().execute(shareImage);
                        Commonhelper.sop("profile" + shareImage);
                        //Commonhelper.sop("profile" + Constnt.shareImageFile.getCanonicalPath());
                    } catch (Exception e) {
                        Commonhelper.sop("profile" + "erroror");
                    }
                    if (TextUtils.isEmpty(Constnt.image_path) || Constnt.image_path.isEmpty() || Constnt.image_path == null || Constnt.image_path.equals("null")) {
                        //do stuff
                    } else {
                        Commonhelper.picasso_(Main2Activity.this, Constnt.image_path,
                                imageView, R.drawable.ic_account_circle_black_24dp);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };*/
    }

    private void hitPi() {
        recyclerView.setAdapter(new MyRecyclerAdapter(Constnt.all_category));
        /*new AbstrctClss(Main2Activity.this, ConstntApi.all_category(Main2Activity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    Res = s;
                    Log.d("hitPi", Res);
                    JSONArray jsonArray = new JSONArray(Res);
                    jsonObjectal.clear();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectal.add(jsonArray.optJSONObject(i));
                    }

                    recyclerView.setAdapter(new MyRecyclerAdapter(jsonObjectal));

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                hitPi();
            }
        };*/
    }

    private void hitPi2() {
        try {
            JSONArray jsonArray = Constnt.get_sliders;
            colorName.clear();
            List<String> stringsGrocery = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                String res = jsonArray.optJSONObject(i).optString("slides_id");
                String category_id = jsonArray.optJSONObject(i).optString("category_id");
                String slider_image = jsonArray.optJSONObject(i).optString("slider_image");
                String grocery_image = jsonArray.optJSONObject(i).optString("grocery_image");
                //color.add("https://aurget.com/uploads/slides_image/slides_" + res + ".jpg");
                color.add(slider_image);
                colorName.add(category_id);
                stringsGrocery.add(grocery_image);
            }

            viewPager.setAdapter(new SliderAdapter(Main2Activity.this, color, colorName, 1));
            viewPager2.setAdapter(new SliderAdapter(Main2Activity.this, color, colorName, 1));
            viewPager4.setAdapter(new SliderAdapter(Main2Activity.this, stringsGrocery, colorName, 2));

            //indicator.setupWithViewPager(viewPager, true);
            //indicator2.setupWithViewPager(viewPager2, true);

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("dsd", "sssss");
        }

        /*new AbstrctClss(Main2Activity.this, ConstntApi.all_Slide_bnner(Main2Activity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    Log.d("dsdss", Res);

                    JSONArray jsonArray = new JSONArray(Res);
                    colorName.clear();

                    List<String> stringsGrocery = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        String res = jsonArray.optJSONObject(i).optString("slides_id");
                        String category_id = jsonArray.optJSONObject(i).optString("category_id");
                        String slider_image = jsonArray.optJSONObject(i).optString("slider_image");
                        String grocery_image = jsonArray.optJSONObject(i).optString("grocery_image");
                        //color.add("https://aurget.com/uploads/slides_image/slides_" + res + ".jpg");
                        color.add(slider_image);
                        colorName.add(category_id);
                        stringsGrocery.add(grocery_image);
                    }

                    viewPager.setAdapter(new SliderAdapter(Main2Activity.this, color, colorName, 1));
                    viewPager2.setAdapter(new SliderAdapter(Main2Activity.this, color, colorName, 1));
                    viewPager4.setAdapter(new SliderAdapter(Main2Activity.this, stringsGrocery, colorName, 2));

                    //indicator.setupWithViewPager(viewPager, true);
                    //indicator2.setupWithViewPager(viewPager2, true);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                hitPi2();
            }
        };*/
    }

    private void hitPi3() {
        recycleView2.setAdapter(new MyRecyclerAdapterHomeBanner(Constnt.get_banners));
        /*new AbstrctClss(Main2Activity.this, ConstntApi.get_banners(Main2Activity.this), "p", true) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    Log.d("hitPi3", Res);

                    JSONArray jsonArray = new JSONArray(Res);

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }

                    recycleView2.setAdapter(new MyRecyclerAdapterHomeBanner(jsonObjectArrayList));

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("hitPi3", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                hitPi2();
            }
        };*/
    }

    private void fastMovingapi()
    {
        recycleView4.setAdapter(new MyRecyclerAdapterFastMoving(Constnt.product_list_set_most_view));

        if (Constnt.product_list_set_most_view.size()==0)
        {
            dashboardApi("");
        }

        //dashboardApi("");

        /*new AbstrctClss(Main2Activity.this, ConstntApi.fastMovingapi(Main2Activity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    Log.d("fastMovingapi", Res);

                    JSONArray jsonArray = new JSONArray(Res);

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }

                    recycleView4.setAdapter(new MyRecyclerAdapterFastMoving(jsonObjectArrayList));

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                fastMovingapi();
            }
        };*/
    }

    private void latestArrival() {

        recycleView5.setAdapter(new MyRecyclerAdapterLatestArrivel(Constnt.product_list_latest));

        /*new AbstrctClss(Main2Activity.this, ConstntApi.latestArrival(Main2Activity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    Log.d("latestArrival", Res);

                    JSONArray jsonArray = new JSONArray(Res);

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }

                    recycleView5.setAdapter(new MyRecyclerAdapterLatestArrivel(Constnt.product_list_latest));

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                latestArrival();
            }
        };*/
    }

    private void letestFeturePi() {

        recycleView6.setAdapter(new MyRecyclerAdapterLatestFeatureProduct(Constnt.product_list_set));

        /*new AbstrctClss(Main2Activity.this, ConstntApi.letestFeture(Main2Activity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    Log.d("letestFeturePi", Res);

                    JSONArray jsonArray = new JSONArray(Res);

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }

                    recycleView6.setAdapter(new MyRecyclerAdapterLatestFeatureProduct(jsonObjectArrayList));

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                letestFeturePi();
            }
        };*/
    }

    private void recentViewPi() {
        recycleView7.setAdapter(new MyRecyclerAdapterRecentllyView(recentViewJAl));
        /*new AbstrctClss(Main2Activity.this, ConstntApi.recentViewPi(Main2Activity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    recentViewJAl.clear();
                    String Res = s;
                    JSONArray jsonArray = new JSONArray(Res);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        recentViewJAl.add(jsonArray.getJSONObject(i));
                    }

                    recycleView7.setAdapter(new MyRecyclerAdapterRecentllyView(recentViewJAl));
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                recentViewPi();
            }
        };*/
    }

    private void crtDetils() {
        new AbstrctClss(Main2Activity.this, ConstntApi.get_cart_details(Main2Activity.this), "g", false) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    try {
                        crtDetils2 = Res;
                        Log.d("crtDetils", crtDetils2);
                        JSONArray jsonArray = new JSONArray(crtDetils2);
                        if (jsonArray.length() > 9)
                            crtCountTv.setText("9+");
                        else crtCountTv.setText("" + jsonArray.length());
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("letestFeturePi", "sssss");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    private void add_user_balance(String login_id, String amount) {
        //http://aurget.com/api/add_user_balance?amount=100&login_id=13&token_key_auth=thygfd45678
        // http://aurget.com/api/add_user_balance?amount=100&login_id=13&token_key_auth=thygfd45678

        final Dialog dialog = Commonhelper.loadDialog(Main2Activity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, InterfaceClass.ipAddress4 + "add_user_balance?amount=" + amount + "&login_id=" + login_id + "&token_key_auth=" + token_key2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            crtDetils = response;
                            Log.d("add_user_balance", crtDetils);
                            JSONArray jsonArray = new JSONArray(crtDetils);
                            //Commonhelper.showToastLong(Main2Activity.this,jsonArray.optJSONObject(0).optString("Status"));

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("add_user_balance", "sssss");
                        }
                        dialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("add_user_balance", "recentViewPi");
                        dialog.dismiss();
                    }
                }) {
//            @Override
//            protected Map getParams()
//            {
//                Map params = new HashMap();
//                params.put("username", "");
//                params.put("email", "abc@androidhive.info");
//                params.put("password", "password123");
//
//                return params;
//            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(Main2Activity.this);
        requestQueue.add(stringRequest);

    }

    private void wallet_transaction(String login_id, String amount, final String status) {
        //http://aurget.com/app/wallet_transaction.php?token_key_auth=thygfd45678&user_id=26&amount=2&method=PU&status=due&payment_details=1
        final Dialog dialog = Commonhelper.loadDialog(Main2Activity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, InterfaceClass.ipAddress3 + "wallet_transaction.php?token_key_auth=" + InterfaceClass.token_key2 + "&user_id=" + login_id + "&amount=" + amount + "&method=PU&status=" + status + "&payment_details=1",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            crtDetils = response;
                            Log.d("wallet_transaction", crtDetils);
                            JSONArray jsonArray = new JSONArray(crtDetils);
                            if (jsonArray.optJSONObject(0).optString("Status").equalsIgnoreCase("true"))
                                Commonhelper.showToastLong(Main2Activity.this, "Amount added in wallet " + status);
                            else
                                Commonhelper.showToastLong(Main2Activity.this, "Amount added in wallet failed");

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("add_user_balance", "sssss");
                        }
                        dialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("add_user_balance", "recentViewPi");
                        dialog.dismiss();
                    }
                }) {
//            @Override
//            protected Map getParams()
//            {
//                Map params = new HashMap();
//                params.put("username", "");
//                params.put("email", "abc@androidhive.info");
//                params.put("password", "password123");
//
//                return params;
//            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(Main2Activity.this);
        requestQueue.add(stringRequest);

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        mSwipeRefreshLayout.setRefreshing(false);
        mSwipeRefreshLayout.setEnabled(false);
        //Toast.makeText(getBaseContext(),"Touch bottom reached",Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public void onScrollChanged() {
        View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
        topDetector = scrollView.getScrollY();
        bottomDetector = view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY());
        if (bottomDetector == 0) {
            mSwipeRefreshLayout.setRefreshing(false);
            mSwipeRefreshLayout.setEnabled(false);
            //Toast.makeText(getBaseContext(),"Scroll View bottom reached",Toast.LENGTH_SHORT).show();
        }
        if (topDetector <= 0) {
            mSwipeRefreshLayout.setRefreshing(false);
            mSwipeRefreshLayout.setEnabled(true);
            //Toast.makeText(getBaseContext(),"Scroll View top reached",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(() -> {
            if (topDetector <= 0) {
                dashboardApi("");
            }
        }, 1000);
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapter(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_list, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            try {
                holder.productNAmeTv.setText(jsonObjectal.get(position).optString("category_name"));

//                Picasso.with(Main2Activity.this).load(InterfaceClass.imgPthCtegory + jsonObjectal.get(position).optString("category_id") + ".jpg").networkPolicy(NetworkPolicy.NO_CACHE)
//                        .memoryPolicy(MemoryPolicy.NO_CACHE)
//                        .into(holder.img);

                Commonhelper.picasso(Main2Activity.this, jsonObjectal.get(position).optString("category_id"), holder.img);

                //Picasso.with(Main2Activity.this).load(InterfaceClass.imgPthCtegory + jsonObjectal.get(position).optString("category_id") + ".jpg").into(holder.img);
                //Picasso.with(Main2Activity.this).load(jsonObjectal.get(position).optString("category_id")).into(holder.img);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            try {
                return jsonObjectal.size();
            } catch (Exception e) {
                return 10;
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView productNAmeTv;
            private ImageView img;


            public ViewHolder(View itemView) {
                super(itemView);
                productNAmeTv = itemView.findViewById(R.id.text);
                img = itemView.findViewById(R.id.img);

                img.setOnClickListener(view -> {
                    Intent intent = new Intent(Main2Activity.this, Categories.class);
                    intent.putExtra("category_id_img", InterfaceClass.imgPth + jsonObjectal.get(getAdapterPosition()).optString("category_id"));
                    intent.putExtra("category_id", jsonObjectal.get(getAdapterPosition()).optString("category_id"));
                    //intent.putExtra("jsonObjectal", Constnt.all_category);
                    startActivity(intent);
                });
            }
        }
    }


    public class MyRecyclerAdapterHomeBanner extends RecyclerView.Adapter<MyRecyclerAdapterHomeBanner.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterHomeBanner(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
            v.getLayoutParams().height = (int) (getScreenWidth(Main2Activity.this) / 4); /// THIS LINE WILL DIVIDE OUR VIEW INTO NUMBERS OF PARTS
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            /*Picasso.with(Main2Activity.this).load(InterfaceClass.imgPth3 + jsonObjectal.get(position).optString("banner_id") + ".jpg")
                    .into(holder.img);*/

            Commonhelper.picasso_(Main2Activity.this, InterfaceClass.imgPth3 + jsonObjectal.get(position).optString("banner_id") + ".jpg", holder.img);
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView productNAmeTv;
            private ImageView img;

            public ViewHolder(View itemView) {
                super(itemView);
                img = itemView.findViewById(R.id.img);

                itemView.setOnClickListener(view -> {
                    Intent intent = new Intent(Main2Activity.this, SubCategoryDetailsActivity.class);
                    intent.putExtra("banner_id", jsonObjectal.get(getAdapterPosition()).optString("banner_id"));
                    intent.putExtra("subcategory_id", jsonObjectal.get(getAdapterPosition()).optString("sub_category"));
                    startActivity(intent);
                });
            }
        }
    }

    public class MyRecyclerAdapterTodayDeal extends RecyclerView.Adapter<MyRecyclerAdapterTodayDeal.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterTodayDeal(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.today_deal_item, parent, false);
            v.getLayoutParams().width = (int) (getScreenWidth(Main2Activity.this) / divideLine); /// THIS LINE WILL DIVIDE OUR VIEW INTO NUMBERS OF PARTS
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            try {
                holder.productNmeTv.setText(jsonObjectal.get(position).optString("title"));
                holder.ipTv.setText("IP " + jsonObjectal.get(position).optString("ip"));
                double disAmt = discountAmt(parseDouble(jsonObjectal.get(position).optString("sale_price")), parseDouble(jsonObjectal.get(position).optString("discount")));
                long value = roudOff(disAmt);
                holder.priceTv.setText("₹" + parseDouble(jsonObjectal.get(position).optString("sale_price")));
                int i = Integer.parseInt(jsonObjectal.get(position).optString("product_sub_id"));
                //Picasso.with(Main2Activity.this).load(InterfaceClass.imgPth2+"product_image/product_239_1_thumb.jpg").into(holder.img);
                Commonhelper.sop("deal==" + InterfaceClass.imgPth2 + jsonObjectal.get(position).optString("product_id") + "_" + i + "_thumb.jpg");
                Commonhelper.picasso_(Main2Activity.this, InterfaceClass.imgPth2 + jsonObjectal.get(position).optString("product_id") + "_" + i + "_thumb.jpg", holder.img);
                //Picasso.with(Main2Activity.this).load(InterfaceClass.imgPth2 + jsonObjectal.get(position).optString("product_id") + "_" + 1 + "_thumb.jpg").into(holder.img);
            } catch (Exception e) {
            }
        }

        @Override
        public int getItemCount() {
            try {
                return jsonObjectal.size();
            } catch (Exception e) {
                return 10;
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView productNmeTv, ipTv, priceTv;
            private ImageView img;

            public ViewHolder(View itemView) {
                super(itemView);
                productNmeTv = itemView.findViewById(R.id.prodNmeTv);
                ipTv = itemView.findViewById(R.id.ipTv);
                priceTv = itemView.findViewById(R.id.priceTv);
                img = itemView.findViewById(R.id.img);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(Main2Activity.this, SubCategoryDetails2.class);
                        intent.putExtra("res", jsonObjectal.get(getAdapterPosition()).toString());
                        int pos = getAdapterPosition();
                        intent.putExtra("imgCount", jsonObjectal.get(getAdapterPosition()).optString("num_of_imgs"));
                        intent.putExtra("product_id", jsonObjectal.get(getAdapterPosition()).optString("product_id"));
                        intent.putExtra("image", InterfaceClass.imgPth2 + jsonObjectal.get(getAdapterPosition()).optString("product_id") + "_" + 1 + "_thumb.jpg");
                        startActivity(intent);

//                        int pos = getAdapterPosition()+1;
//                        Intent intent = new Intent(Main2Activity.this, SubCategoryDetails2.class);
//                        intent.putExtra("res",jsonObjectal.get(getAdapterPosition()).toString());
//                        intent.putExtra("image",InterfaceClass.imgPth2+jsonObjectal.get(getAdapterPosition()).optString("product_id")+"_"+pos+"_thumb.jpg");
//                        startActivity(intent);
                    }
                });
            }
        }
    }

    public class MyRecyclerAdapterFastMoving extends RecyclerView.Adapter<MyRecyclerAdapterFastMoving.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterFastMoving(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.today_deal_item, parent, false);
            v.getLayoutParams().width = (int) (getScreenWidth(Main2Activity.this) / divideLine); /// THIS LINE WILL DIVIDE OUR VIEW INTO NUMBERS OF PARTS
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            try {
                holder.productNmeTv.setText(jsonObjectal.get(position).optString("title"));
                holder.ipTv.setText("IP " + jsonObjectal.get(position).optString("ip"));
                double disAmt = discountAmt(parseDouble(jsonObjectal.get(position).optString("sale_price")), parseDouble(jsonObjectal.get(position).optString("discount")));
                long value = roudOff(disAmt);
                holder.priceTv.setText("₹" + parseDouble(jsonObjectal.get(position).optString("sale_price")));
                int i = Integer.parseInt(jsonObjectal.get(position).optString("product_sub_id"));
                //Picasso.with(Main2Activity.this).load(InterfaceClass.imgPth2 + jsonObjectal.get(position).optString("product_id") + "_" + 1 + "_thumb.jpg").into(holder.img);
                Commonhelper.picasso_(Main2Activity.this, InterfaceClass.imgPth2 + jsonObjectal.get(position).optString("product_id") + "_" + i + "_thumb.jpg", holder.img);
            } catch (Exception e) {

            }
        }

        @Override
        public int getItemCount() {
            try {
                return jsonObjectal.size();
            } catch (Exception e) {
                return 10;
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView productNmeTv, ipTv, priceTv;
            private ImageView img;

            public ViewHolder(View itemView) {
                super(itemView);
                productNmeTv = itemView.findViewById(R.id.prodNmeTv);
                ipTv = itemView.findViewById(R.id.ipTv);
                priceTv = itemView.findViewById(R.id.priceTv);
                img = itemView.findViewById(R.id.img);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(Main2Activity.this, SubCategoryDetails2.class);
                        intent.putExtra("res", jsonObjectal.get(getAdapterPosition()).toString());
                        int pos = getAdapterPosition() + 1;
                        intent.putExtra("imgCount", jsonObjectal.get(getAdapterPosition()).optString("num_of_imgs"));
                        intent.putExtra("product_id", jsonObjectal.get(getAdapterPosition()).optString("product_id"));
                        intent.putExtra("image", InterfaceClass.imgPth2 + jsonObjectal.get(getAdapterPosition()).optString("product_id") + "_" + 1 + "_thumb.jpg");
                        startActivity(intent);

//                        int pos = getAdapterPosition()+1;
//                        Intent intent = new Intent(Main2Activity.this, SubCategoryDetails2.class);
//                        intent.putExtra("res",jsonObjectal.get(getAdapterPosition()).toString());
//                        intent.putExtra("image",InterfaceClass.imgPth2+jsonObjectal.get(getAdapterPosition()).optString("product_id")+"_"+pos+"_thumb.jpg");
//                        startActivity(intent);
                    }
                });
            }
        }
    }

    public class MyRecyclerAdapterLatestArrivel extends RecyclerView.Adapter<MyRecyclerAdapterLatestArrivel.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterLatestArrivel(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.today_deal_item, parent, false);
            v.getLayoutParams().width = (int) (getScreenWidth(Main2Activity.this) / divideLine); /// THIS LINE WILL DIVIDE OUR VIEW INTO NUMBERS OF PARTS
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            try {
                holder.productNmeTv.setText(jsonObjectal.get(position).optString("title"));
                holder.ipTv.setText("IP " + jsonObjectal.get(position).optString("ip"));
                double disAmt = discountAmt(parseDouble(jsonObjectal.get(position).optString("sale_price")), parseDouble(jsonObjectal.get(position).optString("discount")));
                long value = roudOff(disAmt);
                holder.priceTv.setText("₹" + parseDouble(jsonObjectal.get(position).optString("sale_price")));
                int i = Integer.parseInt(jsonObjectal.get(position).optString("product_sub_id"));
                //Picasso.with(Main2Activity.this).load(InterfaceClass.imgPth2 + jsonObjectal.get(position).optString("product_id") + "_" + 1 + "_thumb.jpg").into(holder.img);
                Commonhelper.picasso_(Main2Activity.this, InterfaceClass.imgPth2 + jsonObjectal.get(position).optString("product_id") + "_" + i + "_thumb.jpg", holder.img);
            } catch (Exception e) {
            }
        }

        @Override
        public int getItemCount() {
            try {
                return jsonObjectal.size();
            } catch (Exception e) {
                return 10;
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView productNmeTv, ipTv, priceTv;
            private ImageView img;

            public ViewHolder(View itemView) {
                super(itemView);
                productNmeTv = itemView.findViewById(R.id.prodNmeTv);
                ipTv = itemView.findViewById(R.id.ipTv);
                priceTv = itemView.findViewById(R.id.priceTv);
                img = itemView.findViewById(R.id.img);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(Main2Activity.this, SubCategoryDetails2.class);
                        intent.putExtra("res", jsonObjectal.get(getAdapterPosition()).toString());
                        int pos = getAdapterPosition() + 1;
                        intent.putExtra("imgCount", jsonObjectal.get(getAdapterPosition()).optString("num_of_imgs"));
                        intent.putExtra("product_id", jsonObjectal.get(getAdapterPosition()).optString("product_id"));
                        intent.putExtra("image", InterfaceClass.imgPth2 + jsonObjectal.get(getAdapterPosition()).optString("product_id") + "_" + 1 + "_thumb.jpg");
                        startActivity(intent);

//                        int pos = getAdapterPosition()+1;
//                        Intent intent = new Intent(Main2Activity.this, SubCategoryDetails2.class);
//                        intent.putExtra("res",jsonObjectal.get(getAdapterPosition()).toString());
//                        intent.putExtra("image",InterfaceClass.imgPth2+jsonObjectal.get(getAdapterPosition()).optString("product_id")+"_"+pos+"_thumb.jpg");
//                        startActivity(intent);
                    }
                });
            }
        }
    }

    public class MyRecyclerAdapterLatestFeatureProduct extends RecyclerView.Adapter<MyRecyclerAdapterLatestFeatureProduct.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterLatestFeatureProduct(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.latest_feature, parent, false);
            v.getLayoutParams().width = (int) (getScreenWidth(Main2Activity.this) / divideLine); /// THIS LINE WILL DIVIDE OUR VIEW INTO NUMBERS OF PARTS
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            try {
                holder.productNmeTv.setText(jsonObjectal.get(position).optString("title"));
                holder.ipTv.setText("IP " + jsonObjectal.get(position).optString("ip"));
                int i = Integer.parseInt(jsonObjectal.get(position).optString("product_sub_id"));

                double disAmt = discountAmt(parseDouble(jsonObjectal.get(position).optString("sale_price")), parseDouble(jsonObjectal.get(position).optString("discount")));
                long value = roudOff(disAmt);
                holder.priceTv.setText("₹" + parseDouble(jsonObjectal.get(position).optString("sale_price")));

//            if (jsonObjectal.size()==i)
//            {
//               // Picasso.with(Main2Activity.this).load(InterfaceClass.imgPth2+jsonObjectal.get(position).optString("product_id")+"_"+i+"_thumb.jpg").placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(letestFetureBigImg);
//            }
//            else
//            {
                //Picasso.with(Main2Activity.this).load(InterfaceClass.imgPth2 + jsonObjectal.get(position).optString("product_id") + "_" + 1 + "_thumb.jpg").into(holder.img);
                Commonhelper.picasso_(Main2Activity.this, InterfaceClass.imgPth2 + jsonObjectal.get(position).optString("product_id") + "_" + i + "_thumb.jpg", holder.img);
                // }
            } catch (Exception e) {

            }
        }

        @Override
        public int getItemCount() {
            try {
                return jsonObjectal.size();
            } catch (Exception e) {
                return 10;
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView productNmeTv, ipTv, priceTv;
            private ImageView img;

            public ViewHolder(View itemView) {
                super(itemView);

                productNmeTv = itemView.findViewById(R.id.productNmeTv);
                ipTv = itemView.findViewById(R.id.ipTv);
                priceTv = itemView.findViewById(R.id.priceTv);
                img = itemView.findViewById(R.id.img);

                itemView.setOnClickListener(view -> {
                    Intent intent = new Intent(Main2Activity.this, SubCategoryDetails2.class);
                    intent.putExtra("res", jsonObjectal.get(getAdapterPosition()).toString());
                    int pos = getAdapterPosition() + 1;
                    intent.putExtra("imgCount", jsonObjectal.get(getAdapterPosition()).optString("num_of_imgs"));
                    intent.putExtra("product_id", jsonObjectal.get(getAdapterPosition()).optString("product_id"));
                    intent.putExtra("image", InterfaceClass.imgPth2 + jsonObjectal.get(getAdapterPosition()).optString("product_id") + "_" + 1 + "_thumb.jpg");
                    startActivity(intent);
//                        Intent intent = new Intent(Main2Activity.this, SubCategoryDetails2.class);
//                        intent.putExtra("res",jsonObjectal.get(getAdapterPosition()).toString());
//                        int pos = getAdapterPosition()+1;
//                        intent.putExtra("image",InterfaceClass.imgPth2+jsonObjectal.get(getAdapterPosition()).optString("product_id")+"_"+pos+"_thumb.jpg");
//                        startActivity(intent);
                });
            }
        }
    }

    public class MyRecyclerAdapterRecentllyView extends RecyclerView.Adapter<MyRecyclerAdapterRecentllyView.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterRecentllyView(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.today_deal_item, parent, false);
            v.getLayoutParams().width = (int) (getScreenWidth(Main2Activity.this) / divideLine); /// THIS LINE WILL DIVIDE OUR VIEW INTO NUMBERS OF PARTS
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            try {
                holder.prodNmeTv.setText(jsonObjectal.get(position).optString("title"));
                holder.ipTv.setText("IP " + jsonObjectal.get(position).optString("ip"));
                double disAmt = discountAmt(parseDouble(jsonObjectal.get(position).optString("sale_price")), parseDouble(jsonObjectal.get(position).optString("discount")));
                long value = roudOff(disAmt);
                holder.priceTv.setText("₹" + parseDouble(jsonObjectal.get(position).optString("sale_price")));
                int i = Integer.parseInt(jsonObjectal.get(position).optString("product_sub_id"));
                //Picasso.with(Main2Activity.this).load(InterfaceClass.imgPth2 + jsonObjectal.get(position).optString("product_id") + "_" + 1 + "_thumb.jpg").into(holder.img);
                Commonhelper.picasso_(Main2Activity.this, InterfaceClass.imgPth2 + jsonObjectal.get(position).optString("product_id") + "_" + i + "_thumb.jpg", holder.img);
            } catch (Exception e) {

            }
        }

        @Override
        public int getItemCount() {
            try {
                return jsonObjectal.size();
            } catch (Exception e) {
                return 10;
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView prodNmeTv, ipTv, priceTv;
            private ImageView img;

            public ViewHolder(View itemView) {
                super(itemView);
                prodNmeTv = itemView.findViewById(R.id.prodNmeTv);
                ipTv = itemView.findViewById(R.id.ipTv);
                priceTv = itemView.findViewById(R.id.priceTv);
                img = itemView.findViewById(R.id.img);

                itemView.setOnClickListener(view -> {
                    Intent intent = new Intent(Main2Activity.this, SubCategoryDetails2.class);
                    intent.putExtra("res", jsonObjectal.get(getAdapterPosition()).toString());
                    int pos = getAdapterPosition() + 1;
                    intent.putExtra("imgCount", jsonObjectal.get(getAdapterPosition()).optString("num_of_imgs"));
                    intent.putExtra("product_id", jsonObjectal.get(getAdapterPosition()).optString("product_id"));
                    intent.putExtra("image", InterfaceClass.imgPth2 + jsonObjectal.get(getAdapterPosition()).optString("product_id") + "_" + 1 + "_thumb.jpg");
                    startActivity(intent);
                });
            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    public class GetImageFromUrl extends AsyncTask<String, Void, Bitmap> {
        Dialog dialog = Commonhelper.loadDialog(mActivity);
        Bitmap bitmap = null;

        public GetImageFromUrl() {
        }

        @Override
        protected Bitmap doInBackground(String... url) {
            String stringUrl = url[0];
            bitmap = null;
            InputStream inputStream;
            try {
                inputStream = new java.net.URL(stringUrl).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            try {
                bitmapconvert(bitmap);
                String versionName = null;
                versionName = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0).versionName;
                Commonhelper.sop("versionName" + versionName);
                Commonhelper.sop("versionName" + appVersion);
                if (!appVersion.equals(versionName)) {
                    String isTrue = CustomPreference.readString(Main2Activity.this, CustomPreference.playStroeCancel, "");
                    if (!isTrue.equalsIgnoreCase("true")) {
                        playStoreDialog();
                    }
                }
            } catch (Exception e) {

            }

            dialog.dismiss();
        }
    }


    private void dashboardApi(String str)
    {
        //Commonhelper.showToastLong(mActivity,str);
        Commonhelper.sop("showToastLong="+str);
        new AbstrctClss(mActivity, ConstntApi.DashboardApi(mActivity), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    JSONObject jsonArray = new JSONObject(s);
                    JSONArray product_list_latest  = jsonArray.optJSONArray("product_list_latest");
                    JSONArray product_list_set_most_view  = jsonArray.optJSONArray("product_list_set_most_view");
                    JSONArray product_list_set_recent = jsonArray.optJSONArray("product_list_set_recent");

                    JSONArray get_banners = jsonArray.optJSONArray("get_banners");
                    JSONArray allcategory = jsonArray.optJSONArray("allcategory");
                    JSONArray deal_product = jsonArray.optJSONArray("deal_product");
                    JSONArray product_list_set = jsonArray.optJSONArray("product_list_set");
                    JSONArray product_list_set_recent_all = jsonArray.optJSONArray("product_list_set_recent_all");

                    Constnt.product_list_latest.clear();
                    Constnt.product_list_set_most_view.clear();
                    Constnt.recentViewJAl.clear();
                    Constnt.get_banners.clear();
                    Constnt.deal_product.clear();
                    Constnt.all_category.clear();
                    Constnt.product_list_set.clear();
                    Constnt.product_list_set_recent_all.clear();
                    Constnt.get_sliders=null;
                    Constnt.get_profile=null;

                    Constnt.get_profile  = jsonArray.optJSONArray("get_profile");
                    Constnt.get_sliders = jsonArray.optJSONArray("get_sliders");

                    for (int i = 0; i < product_list_latest.length(); i++) {
                        Constnt.product_list_latest.add(product_list_latest.getJSONObject(i));
                    }

                    for (int i = 0; i < product_list_set_most_view.length(); i++) {
                        Constnt.product_list_set_most_view.add(product_list_set_most_view.getJSONObject(i));
                    }

                    for (int i = 0; i < product_list_set_recent.length(); i++) {
                        Constnt.recentViewJAl.add(product_list_set_recent.getJSONObject(i));
                    }
                    for (int i = 0; i < get_banners.length(); i++) {
                        Constnt.get_banners.add(get_banners.getJSONObject(i));
                    }

                    for (int i = 0; i < allcategory.length(); i++) {
                        Constnt.all_category.add(allcategory.getJSONObject(i));
                    }
                    for (int i = 0; i < deal_product.length(); i++) {
                        Constnt.deal_product.add(deal_product.getJSONObject(i));
                    }

                    for (int i = 0; i < product_list_set.length(); i++) {
                        Constnt.product_list_set.add(product_list_set.getJSONObject(i));
                    }

                    for (int i = 0; i < product_list_set_recent_all.length(); i++) {
                        Constnt.product_list_set_recent_all.add(product_list_set_recent_all.getJSONObject(i));
                    }

                    hitPi();
                    hitPi2();
                    hitPi3();
                    fastMovingapi();
                    latestArrival();
                    letestFeturePi();
                    recentViewPi();
                    deal_product();
                    fetch_bank_details();

                    if (!str.equals(""))
                    {
                        if(str.contains("&come_from"))
                        {
                            Commonhelper.sop("product_id" + extractInt(str));
                            String allVal = extractInt(str);
                            String[] allarr  = TextUtils.split(allVal,",");
                            arl = new ArrayList<String>(Arrays.asList(allarr));

                            /*for (String ele:arl)
                            {
                                Commonhelper.sop("product_id=" + ele);
                            }*/

                            isSharedGrocery = true;
                            ed2.performClick();


                            //app://myhost/help?productid=&come_from=3&user_id=743
                        }
                        else {
                            String separator = "=";
                            int sepPos = str.indexOf(separator);
                            String prodID = str.substring(sepPos + separator.length());
                            System.out.println("Substring after separator = " + prodID);

                            new AbstrctClss(Main2Activity.this, "http://aurget.com/DashboardApi/share_product_details?product_id=" + prodID, "g", true) {
                                @Override
                                public void responce(String s) {
                                    JSONArray jsonArray = null;
                                    try {
                                        jsonArray = new JSONArray(s);

                                        Intent intent = new Intent(Main2Activity.this, SubCategoryDetails2.class);
                                        intent.putExtra("res", jsonArray.optJSONObject(0).toString());
                                        intent.putExtra("imgCount", jsonArray.optJSONObject(0).optString("num_of_imgs"));
                                        intent.putExtra("product_id", jsonArray.optJSONObject(0).optString("product_id"));
                                        intent.putExtra("image", InterfaceClass.imgPth2 + jsonArray.optJSONObject(0).optString("product_id") + "_" + 1 + "_thumb.jpg");
                                        startActivity(intent);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onErrorResponsee(String s) {
                                    Commonhelper.sop("errororo" + s);
                                }
                            };
                        }

                    }

                    else
                    {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                Commonhelper.showToastLong(mActivity,"Please check net connection");
            }
        };
    }

    static String extractInt(String str)
    {
        // Replacing every non-digit number 
        // with a space(" ") 
        str = str.replaceAll("[^\\d]", " ");

        // Remove extra spaces from the beginning 
        // and the ending of the string 
        str = str.trim();

        // Replace all the consecutive white 
        // spaces with a single space 
        str = str.replaceAll(" +", ",");

        if (str.equals(""))
            return "-1";

        return str;
    }

    void bitmapconvert(Bitmap bitmap) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    File file = new File(getExternalCacheDir(), "share.png");
                    FileOutputStream fOut = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.flush();
                    fOut.close();
                    file.setReadable(true, false);
                    Constnt.shareImageFile = file;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static String crtDetils, crtDetils2;
    int topDetector = 0;
    int bottomDetector = 0;
    Timer timer;
    TimerTask timerTask;
    final Handler handler = new Handler();
    private  boolean isSharedGrocery = false;
    ArrayList<String> arl;
}

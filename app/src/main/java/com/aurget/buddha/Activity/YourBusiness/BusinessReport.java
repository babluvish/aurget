package com.aurget.buddha.Activity.YourBusiness;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.Utils.SearchDate;
import com.aurget.buddha.Activity.Utils.SearchMonth;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.aurget.buddha.Activity.Commonhelper.monthInWord;
import static com.aurget.buddha.Activity.Commonhelper.placeValue;

public class BusinessReport extends AppCompatActivity {

    private RecyclerView direct_incentiveRV, differential_incentiveRV, total_incentiveRV,
            royalityIncentiveRV, aurgetClubRV, walletCashRv, DirectIPRV, diffrencialIPRV, business_report_Rv, rvHeader;


    TextView dateRnge, mothTv, allTv, titleTv, direct_incentiveTv, differential_incentiveTv, derrerencialIP,
            directIPTv, royalityIncentiveTv, walletCashTv, totalIP, total_incentiveTv;

    CardView direct_incentiveLL;
    CardView differential_incentiveLl;
    CardView total_incentiveLl;
    CardView royalityIncentiveCV;
    CardView aurgetClubCv;
    CardView walletCashCV;
    CardView directIPCV, derrerencialIPCV, totalIPCV;
    LinearLayout ll8, brLl, Ll1;
    ScrollView scroll;
    int pos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_report);

        direct_incentiveRV = findViewById(R.id.direct_incentiveRV);
        direct_incentiveTv = findViewById(R.id.direct_incentiveTv);
        differential_incentiveTv = findViewById(R.id.differential_incentiveTv);
        differential_incentiveRV = findViewById(R.id.differential_incentiveRV);
        royalityIncentiveTv = findViewById(R.id.royalityIncentiveTv);
        derrerencialIP = findViewById(R.id.derrerencialIP);
        directIPTv = findViewById(R.id.directIPTv);
        total_incentiveRV = findViewById(R.id.total_incentiveRV);
        royalityIncentiveRV = findViewById(R.id.royalityIncentiveRV);
        aurgetClubRV = findViewById(R.id.aurgetClubRV);
        walletCashRv = findViewById(R.id.walletCashRv);
        DirectIPRV = findViewById(R.id.DirectIPRV);
        diffrencialIPRV = findViewById(R.id.diffrencialIPRV);
        //scroll = findViewById(R.id.scroll);
        walletCashTv = findViewById(R.id.walletCashTv);
        totalIP = findViewById(R.id.totalIP);
        total_incentiveTv = findViewById(R.id.total_incentiveTv);
        Ll1 = findViewById(R.id.Ll1);

        dateRnge = findViewById(R.id.dateRnge);
        mothTv = findViewById(R.id.mothTv);
        allTv = findViewById(R.id.allTv);
        titleTv = findViewById(R.id.titleTv);

        direct_incentiveLL = findViewById(R.id.direct_incentiveLL);
        differential_incentiveLl = findViewById(R.id.differential_incentiveLl);
        total_incentiveLl = findViewById(R.id.total_incentiveLl);
        royalityIncentiveCV = findViewById(R.id.royalityIncentiveCV);
        aurgetClubCv = findViewById(R.id.aurgetClubCv);
        walletCashCV = findViewById(R.id.walletCashCV);
        directIPCV = findViewById(R.id.directIPCV);
        derrerencialIPCV = findViewById(R.id.derrerencialIPCV);
        totalIPCV = findViewById(R.id.totalIPCV);
        rvHeader = findViewById(R.id.rvHeader);

        ll8 = findViewById(R.id.ll8);
        brLl = findViewById(R.id.brLl);


//        // Wait until my scrollView is ready
//        scroll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                // Ready, move up
//                scroll.fullScroll(View.FOCUS_UP);
//            }
//        });

        titleTv.setText(R.string.direct_incentive_item_detild);

        final LinearLayoutManager horizontalLayoutManager_ = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvHeader.setLayoutManager(horizontalLayoutManager_);
        rvHeader.setItemAnimator(new DefaultItemAnimator());

        final LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        direct_incentiveRV.setLayoutManager(horizontalLayoutManager);
        direct_incentiveRV.setItemAnimator(new DefaultItemAnimator());

        final LinearLayoutManager horizontalLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        differential_incentiveRV.setLayoutManager(horizontalLayoutManager2);
        differential_incentiveRV.setItemAnimator(new DefaultItemAnimator());

        final LinearLayoutManager horizontalLayoutManager3 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        total_incentiveRV.setLayoutManager(horizontalLayoutManager3);
        total_incentiveRV.setItemAnimator(new DefaultItemAnimator());

        final LinearLayoutManager horizontalLayoutManager4 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        royalityIncentiveRV.setLayoutManager(horizontalLayoutManager4);
        royalityIncentiveRV.setItemAnimator(new DefaultItemAnimator());

        final LinearLayoutManager horizontalLayoutManager5 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        aurgetClubRV.setLayoutManager(horizontalLayoutManager5);
        aurgetClubRV.setItemAnimator(new DefaultItemAnimator());

        final LinearLayoutManager horizontalLayoutManager6 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        walletCashRv.setLayoutManager(horizontalLayoutManager6);
        walletCashRv.setItemAnimator(new DefaultItemAnimator());

        final LinearLayoutManager horizontalLayoutManager7 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        DirectIPRV.setLayoutManager(horizontalLayoutManager7);
        DirectIPRV.setItemAnimator(new DefaultItemAnimator());

        final LinearLayoutManager horizontalLayoutManager8 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        diffrencialIPRV.setLayoutManager(horizontalLayoutManager8);
        diffrencialIPRV.setItemAnimator(new DefaultItemAnimator());

        callApiHeader();
        // callApiHeader();

        direct_incentiveLL.setOnClickListener(view -> {
            titleTv.setText(R.string.direct_incentive_item_detild);
            differential_incentiveRV.setVisibility(View.GONE);
            direct_incentiveRV.setVisibility(View.VISIBLE);
            total_incentiveRV.setVisibility(View.GONE);
            royalityIncentiveRV.setVisibility(View.GONE);
            aurgetClubRV.setVisibility(View.GONE);
            walletCashRv.setVisibility(View.GONE);
            DirectIPRV.setVisibility(View.GONE);
            ll8.setVisibility(View.GONE);
            diffrencialIPRV.setVisibility(View.GONE);
            findViewById(R.id.searchEt).setVisibility(View.GONE);

            direct_incentiveLL.setCardBackgroundColor(getResources().getColor(R.color.bluee));
            differential_incentiveLl.setCardBackgroundColor(getResources().getColor(R.color.white));
            total_incentiveLl.setCardBackgroundColor(getResources().getColor(R.color.white));
            royalityIncentiveCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            walletCashCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            directIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            derrerencialIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            totalIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));

            direct_incentive();
        });

        differential_incentiveLl.setOnClickListener(view -> {
            titleTv.setText(R.string.differential_incentive_Statement);
            differential_incentiveRV.setVisibility(View.VISIBLE);
            direct_incentiveRV.setVisibility(View.GONE);
            total_incentiveRV.setVisibility(View.GONE);
            royalityIncentiveRV.setVisibility(View.GONE);
            aurgetClubRV.setVisibility(View.GONE);
            walletCashRv.setVisibility(View.GONE);
            DirectIPRV.setVisibility(View.GONE);
            ll8.setVisibility(View.GONE);
            diffrencialIPRV.setVisibility(View.GONE);
            findViewById(R.id.searchEt).setVisibility(View.GONE);

            direct_incentiveLL.setCardBackgroundColor(getResources().getColor(R.color.white));
            differential_incentiveLl.setCardBackgroundColor(getResources().getColor(R.color.bluee));
            total_incentiveLl.setCardBackgroundColor(getResources().getColor(R.color.white));
            royalityIncentiveCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            walletCashCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            directIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            derrerencialIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            totalIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));

            deffirencial_incentive_list();
        });

        total_incentiveLl.setOnClickListener(view -> {

            titleTv.setText(R.string.totl_incentive_Statement);
            total_incentiveRV.setVisibility(View.VISIBLE);
            direct_incentiveRV.setVisibility(View.GONE);
            differential_incentiveRV.setVisibility(View.GONE);
            royalityIncentiveRV.setVisibility(View.GONE);
            aurgetClubRV.setVisibility(View.GONE);
            walletCashRv.setVisibility(View.GONE);
            DirectIPRV.setVisibility(View.GONE);
            ll8.setVisibility(View.GONE);
            diffrencialIPRV.setVisibility(View.GONE);
            findViewById(R.id.searchEt).setVisibility(View.GONE);
            pos = 1;

            totalIncentive();

            direct_incentiveLL.setCardBackgroundColor(getResources().getColor(R.color.white));
            differential_incentiveLl.setCardBackgroundColor(getResources().getColor(R.color.white));
            total_incentiveLl.setCardBackgroundColor(getResources().getColor(R.color.bluee));
            royalityIncentiveCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            walletCashCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            directIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            derrerencialIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            totalIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));

        });

        royalityIncentiveCV.setOnClickListener(view -> {
            titleTv.setText(R.string.roylityIncentive);
            total_incentiveRV.setVisibility(View.GONE);
            direct_incentiveRV.setVisibility(View.GONE);
            differential_incentiveRV.setVisibility(View.GONE);
            royalityIncentiveRV.setVisibility(View.VISIBLE);
            aurgetClubRV.setVisibility(View.GONE);
            walletCashRv.setVisibility(View.GONE);
            ll8.setVisibility(View.GONE);
            DirectIPRV.setVisibility(View.GONE);
            diffrencialIPRV.setVisibility(View.GONE);
            findViewById(R.id.searchEt).setVisibility(View.GONE);

            direct_incentiveLL.setCardBackgroundColor(getResources().getColor(R.color.white));
            differential_incentiveLl.setCardBackgroundColor(getResources().getColor(R.color.white));
            total_incentiveLl.setCardBackgroundColor(getResources().getColor(R.color.white));
            royalityIncentiveCV.setCardBackgroundColor(getResources().getColor(R.color.bluee));
            walletCashCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            directIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            derrerencialIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            totalIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            royalty_list();
        });

        aurgetClubCv.setOnClickListener(view -> {
  /*          titleTv.setText(R.string.aurget_club);
            total_incentiveRV.setVisibility(View.GONE);
            direct_incentiveRV.setVisibility(View.GONE);
            differential_incentiveRV.setVisibility(View.GONE);
            royalityIncentiveRV.setVisibility(View.GONE);
            aurgetClubRV.setVisibility(View.VISIBLE);
            walletCashRv.setVisibility(View.GONE);
            ll8.setVisibility(View.VISIBLE);
            DirectIPRV.setVisibility(View.GONE);
            diffrencialIPRV.setVisibility(View.GONE);
            findViewById(R.id.searchEt).setVisibility(View.GONE);
            aurgetClubRV.setAdapter(new MyRecyclerAdapterAurgetClub(null));*/
            Commonhelper.showToastLong(BusinessReport.this, "Coming soon");
        });

        walletCashCV.setOnClickListener(view -> {
            titleTv.setText(R.string.wallet_cashback);
            total_incentiveRV.setVisibility(View.GONE);
            direct_incentiveRV.setVisibility(View.GONE);
            differential_incentiveRV.setVisibility(View.GONE);
            royalityIncentiveRV.setVisibility(View.GONE);
            aurgetClubRV.setVisibility(View.VISIBLE);
            walletCashRv.setVisibility(View.GONE);
            ll8.setVisibility(View.GONE);
            walletCashRv.setVisibility(View.VISIBLE);
            DirectIPRV.setVisibility(View.GONE);
            diffrencialIPRV.setVisibility(View.GONE);
            findViewById(R.id.searchEt).setVisibility(View.GONE);

            direct_incentiveLL.setCardBackgroundColor(getResources().getColor(R.color.white));
            differential_incentiveLl.setCardBackgroundColor(getResources().getColor(R.color.white));
            total_incentiveLl.setCardBackgroundColor(getResources().getColor(R.color.white));
            royalityIncentiveCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            walletCashCV.setCardBackgroundColor(getResources().getColor(R.color.bluee));
            directIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            derrerencialIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            totalIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));

            walletCashRv.setAdapter(new MyRecyclerAdapterWalletCashBack(null));
        });

        directIPCV.setOnClickListener(view -> {
            titleTv.setText(R.string.direct_ip);
            total_incentiveRV.setVisibility(View.GONE);
            direct_incentiveRV.setVisibility(View.GONE);
            differential_incentiveRV.setVisibility(View.GONE);
            royalityIncentiveRV.setVisibility(View.GONE);
            aurgetClubRV.setVisibility(View.VISIBLE);
            walletCashRv.setVisibility(View.GONE);
            ll8.setVisibility(View.GONE);
            walletCashRv.setVisibility(View.GONE);
            DirectIPRV.setVisibility(View.VISIBLE);
            diffrencialIPRV.setVisibility(View.GONE);
            findViewById(R.id.searchEt).setVisibility(View.VISIBLE);

            direct_incentiveLL.setCardBackgroundColor(getResources().getColor(R.color.white));
            differential_incentiveLl.setCardBackgroundColor(getResources().getColor(R.color.white));
            total_incentiveLl.setCardBackgroundColor(getResources().getColor(R.color.white));
            royalityIncentiveCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            walletCashCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            directIPCV.setCardBackgroundColor(getResources().getColor(R.color.bluee));
            derrerencialIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            totalIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));

            direct_ip();
        });

        derrerencialIPCV.setOnClickListener(view -> {
            titleTv.setText(R.string.differential_ip);
            total_incentiveRV.setVisibility(View.GONE);
            direct_incentiveRV.setVisibility(View.GONE);
            differential_incentiveRV.setVisibility(View.GONE);
            royalityIncentiveRV.setVisibility(View.GONE);
            aurgetClubRV.setVisibility(View.VISIBLE);
            walletCashRv.setVisibility(View.GONE);
            ll8.setVisibility(View.GONE);
            walletCashRv.setVisibility(View.GONE);
            diffrencialIPRV.setVisibility(View.VISIBLE);
            findViewById(R.id.searchEt).setVisibility(View.VISIBLE);

            direct_incentiveLL.setCardBackgroundColor(getResources().getColor(R.color.white));
            differential_incentiveLl.setCardBackgroundColor(getResources().getColor(R.color.white));
            total_incentiveLl.setCardBackgroundColor(getResources().getColor(R.color.white));
            royalityIncentiveCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            walletCashCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            directIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            derrerencialIPCV.setCardBackgroundColor(getResources().getColor(R.color.bluee));
            totalIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));

            deffirencial_ip_list();
        });

        totalIPCV.setOnClickListener(view -> {
            titleTv.setText(R.string.total_ip);
            total_incentiveRV.setVisibility(View.VISIBLE);
            direct_incentiveRV.setVisibility(View.GONE);
            differential_incentiveRV.setVisibility(View.GONE);
            royalityIncentiveRV.setVisibility(View.GONE);
            aurgetClubRV.setVisibility(View.GONE);
            walletCashRv.setVisibility(View.GONE);
            DirectIPRV.setVisibility(View.GONE);
            ll8.setVisibility(View.GONE);
            diffrencialIPRV.setVisibility(View.GONE);
            findViewById(R.id.searchEt).setVisibility(View.GONE);

            direct_incentiveLL.setCardBackgroundColor(getResources().getColor(R.color.white));
            differential_incentiveLl.setCardBackgroundColor(getResources().getColor(R.color.white));
            total_incentiveLl.setCardBackgroundColor(getResources().getColor(R.color.white));
            royalityIncentiveCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            walletCashCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            directIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            derrerencialIPCV.setCardBackgroundColor(getResources().getColor(R.color.white));
            totalIPCV.setCardBackgroundColor(getResources().getColor(R.color.bluee));

            pos = 2;
            totalIncentive();
        });

        /*======================================================================================*/
        dateRnge.setOnClickListener(view -> {
            dateRnge.setBackgroundResource(R.drawable.ovel_shape);
            allTv.setBackgroundResource(R.drawable.ovel_shap3);
            mothTv.setBackgroundResource(R.drawable.ovel_shap3);

            new SearchDate(BusinessReport.this) {
                @Override
                public String fromDtToDt(String frmDate, String toDt) {
                    return null;
                }
            }.dateRangeDialog();

        });


        allTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dateRnge.setBackgroundResource(R.drawable.ovel_shap3);
                allTv.setBackgroundResource(R.drawable.ovel_shape);
                mothTv.setBackgroundResource(R.drawable.ovel_shap3);
            }
        });

        mothTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mothTv.setBackgroundResource(R.drawable.ovel_shape);
                allTv.setBackgroundResource(R.drawable.ovel_shap3);
                dateRnge.setBackgroundResource(R.drawable.ovel_shap3);

                new SearchMonth(BusinessReport.this) {
                    @Override
                    public String fromDt(String month__, String year) {
                        return null;
                    }
                }.monthNameDialog();

            }
        });

        direct_incentiveRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    // Scrolling up
                    Log.i("ress", "UP");
                    brLl.setVisibility(View.GONE);
                } else {
                    // Scrolling down
                    Log.i("ress", "down");
                    brLl.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    // Do something
                    Log.i("ress", "SCROLL_STATE_FLING");
                } else if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    // Do something
                    Log.i("ress", "SCROLL_STATE_TOUCH_SCROLL");
                } else {
                    // Do something
                    //Log.i("ress","SCROLL_STATE_TOUCH_SCROLL NO");
                }
            }
        });

        detils();
    }

    private void royalty_list() {
        new AbstrctClss(BusinessReport.this, ConstntApi.royalty_list(BusinessReport.this), "g", true) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s);

                    if (jsonArray.length() < 1) {
                        Commonhelper.showToastLong(BusinessReport.this, Constnt.notFound);
                        return;
                    }

                    ArrayList<JSONObject> jsonObjectal = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectal.add(jsonArray.optJSONObject(i));
                    }

                    royalityIncentiveRV.setVisibility(View.VISIBLE);
                    royalityIncentiveRV.setAdapter(new MyRecyclerAdapterRoyalityIncentive(jsonObjectal));
                } catch (Exception e) {

                }
            }

            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }

    private void deffirencial_ip_list() {

        new AbstrctClss(BusinessReport.this, ConstntApi.deffirencial_ip_list(BusinessReport.this), "g", true) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s);

                    if (jsonArray.length() < 1) {
                        Commonhelper.showToastLong(BusinessReport.this, Constnt.notFound);
                        return;
                    }

                    ArrayList<JSONObject> jsonObjectal = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectal.add(jsonArray.optJSONObject(i));
                    }
                    diffrencialIPRV.setVisibility(View.VISIBLE);
                    diffrencialIPRV.setAdapter(new MyRecyclerAdapterDifferencialIP(jsonObjectal));
                } catch (Exception e) {

                }
            }

            @Override
            public void onErrorResponsee(String s) {

            }
        };

    }

    private void direct_ip() {
        new AbstrctClss(BusinessReport.this, ConstntApi.direct_ip(BusinessReport.this), "g", true) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s);
                    ArrayList<JSONObject> jsonObjectal = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectal.add(jsonArray.optJSONObject(i));
                    }
                    DirectIPRV.setVisibility(View.VISIBLE);
                    DirectIPRV.setAdapter(new MyRecyclerAdapterDirectIP(jsonObjectal));
                } catch (Exception e) {

                }
            }

            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }


    private void totalIncentive() {
        String url;

        if (pos == 1) {
            url = ConstntApi.total_incentives(BusinessReport.this);
        } else {
            url = ConstntApi.total_ip_list(BusinessReport.this);
        }

        new AbstrctClss(BusinessReport.this, url, "g", true) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s);
                    ArrayList<JSONObject> jsonObjectal = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectal.add(jsonArray.optJSONObject(i));
                    }

                    total_incentiveRV.setVisibility(View.VISIBLE);
                    total_incentiveRV.setAdapter(new MyRecyclerAdapterTotlIncentive(jsonObjectal));

                } catch (Exception e) {
                }
            }

            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }


    private void detils() {

        new AbstrctClss(BusinessReport.this, ConstntApi.get_all_insentive(BusinessReport.this), "g", true) {
            @Override
            public void responce(String s) {

                JSONArray jsonArray = null;

                try {

                    ArrayList<JSONObject> jsonObjects = new ArrayList<>();
                    jsonArray = new JSONArray(s);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    //rvHeader.setAdapter(new MyRecyclerAdapterHeader(jsonObjects));

                    try {
                        direct_incentiveTv.setText(getResources().getString(R.string.rs) +
                                Commonhelper.roundOff(Double.valueOf(jsonObject.optString("direct_insentive")), placeValue));
                    } catch (Exception e) {
                        direct_incentiveTv.setText(getResources().getString(R.string.rs) + "0");
                    }

                    try {
                        differential_incentiveTv.setText(
                                getResources().getString(R.string.rs) +
                                        Commonhelper.roundOff(Double.valueOf(jsonObject.optString("difference_incentive")), placeValue));
                    } catch (Exception e) {
                        differential_incentiveTv.setText(getResources().getString(R.string.rs) + "0");
                    }

                    try {

                        derrerencialIP.setText(
                                getResources().getString(R.string.rs) +
                                        Commonhelper.roundOff(Double.valueOf(jsonObject.optString("differential_ip"))));

                        //derrerencialIP.setText(jsonObject.optString("differential_ip"));
                    } catch (Exception e) {
                        derrerencialIP.setText("0");
                    }

                    try {
                        directIPTv.setText(jsonObject.optString("direct_ip"));
                    } catch (Exception e) {
                        directIPTv.setText("0");
                    }

                    try {
                        royalityIncentiveTv.setText(
                                getResources().getString(R.string.rs) +
                                        Commonhelper.roundOff(Double.valueOf(jsonObject.optString("royality")), placeValue));
                        //royalityIncentiveTv.setText(getResources().getString(R.string.rs)+jsonObject.optString("royality"));
                    } catch (Exception e) {
                        royalityIncentiveTv.setText(getResources().getString(R.string.rs) + "0");
                    }

                    try {
                        walletCashTv.setText(
                                getResources().getString(R.string.rs) +
                                        Commonhelper.roundOff(Double.valueOf(jsonObject.optString("wallet_balance")), placeValue));
                        //walletCashTv.setText(getResources().getString(R.string.rs)+jsonObject.optString("wallet_balance"));
                    } catch (Exception e) {
                        walletCashTv.setText("0");
                    }
                    try {
                        totalIP.setText(jsonObject.optString("total_ip"));
                    } catch (Exception e) {
                        totalIP.setText("0");
                    }

                    try {
                        total_incentiveTv.setText(
                                getResources().getString(R.string.rs) +
                                        Commonhelper.roundOff(Double.valueOf(jsonObject.optString("total_insentive")), placeValue));

                        //total_incentiveTv.setText(getResources().getString(R.string.rs)+jsonObject.optString("total_insentive"));
                    } catch (Exception e) {
                        total_incentiveTv.setText(getResources().getString(R.string.rs) + "0");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }

    private void direct_incentive() {
        new AbstrctClss(BusinessReport.this, ConstntApi.direct_incentive(BusinessReport.this), "g", true) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s);
                    if (jsonArray.length() == 0) {
                        Commonhelper.showToastLong(BusinessReport.this, Constnt.notFound);
                        return;
                    }

                    ArrayList<JSONObject> jsonObjectal = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectal.add(jsonArray.optJSONObject(i));
                    }

                    direct_incentiveRV.setVisibility(View.VISIBLE);
                    direct_incentiveRV.setAdapter(new MyRecyclerAdapterDirectIncentive(jsonObjectal));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }

    void callApiHeader() {
        new AbstrctClss(BusinessReport.this, ConstntApi.businessReport(BusinessReport.this), "p", true) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s);

                    ArrayList<JSONObject> jsonObjectal = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectal.add(jsonObjectal.get(i));
                    }

                    business_report_Rv.setAdapter(new BusinessDetailsAdapter(jsonObjectal));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    void deffirencial_incentive_list() {

        new AbstrctClss(BusinessReport.this, ConstntApi.deffirencial_incentive_list(BusinessReport.this), "g", true) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(s);
                    if (jsonArray.length() < 1) {
                        Commonhelper.showToastLong(BusinessReport.this, Constnt.notFound);
                        return;
                    }

                    ArrayList<JSONObject> jsonObjectal = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectal.add(jsonArray.optJSONObject(i));
                    }

                    differential_incentiveRV.setAdapter(new MyRecyclerAdapterDifferencilIncentive(jsonObjectal));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }

    public class BusinessDetailsAdapter extends RecyclerView.Adapter<BusinessDetailsAdapter.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public BusinessDetailsAdapter(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public BusinessDetailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.business_report_list, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(BusinessDetailsAdapter.ViewHolder holder, int position) {

            //holder.nameTv.setText();
            // holder.priceTv.setText(jsonObjectal.get(position).optString(""));
        }

        @Override
        public int getItemCount() {
            //return jsonObjectal.size();
            return 10;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView nameTv, priceTv;

            public ViewHolder(View itemView) {
                super(itemView);

                nameTv = itemView.findViewById(R.id.nameTv);
                priceTv = itemView.findViewById(R.id.priceTv);

            }
        }
    }

    public class MyRecyclerAdapterDirectIncentive extends RecyclerView.Adapter<MyRecyclerAdapterDirectIncentive.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterDirectIncentive(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapterDirectIncentive.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.direct_incentive_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapterDirectIncentive.ViewHolder holder, int position) {
            holder.earnIncentiv.setText("Earn Incentive " +getResources().getString(R.string.rs)+ jsonObjectal.get(position).optString("earn_incentive"));
            holder.placedON.setText("" + jsonObjectal.get(position).optString("placed_on"));
            holder.orderId.setText("" + jsonObjectal.get(position).optString("order_id"));
            holder.priceTv.setText("" + jsonObjectal.get(position).optString("price"));
            holder.qtyTv.setText("" + jsonObjectal.get(position).optString("qtr"));
            holder.prodNmeTv.setText("" + jsonObjectal.get(position).optString("product_name"));
            holder.ipTv.setText("" + jsonObjectal.get(position).optString("product_ip"));
            Picasso.with(BusinessReport.this).load(jsonObjectal.get(position).optString("product_image")).into(holder.img);
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView orderId, placedON, priceTv, qtyTv, prodNmeTv, ipTv, earnIncentiv;
            ImageView img;

            public ViewHolder(View itemView) {
                super(itemView);
                orderId = itemView.findViewById(R.id.orderId);
                placedON = itemView.findViewById(R.id.placedON);
                priceTv = itemView.findViewById(R.id.priceTv);
                qtyTv = itemView.findViewById(R.id.qtyTv);
                prodNmeTv = itemView.findViewById(R.id.prodNmeTv);
                img = itemView.findViewById(R.id.img);
                ipTv = itemView.findViewById(R.id.ipTv);
                earnIncentiv = itemView.findViewById(R.id.earnIncentiv);
            }
        }
    }

    public class MyRecyclerAdapterDifferencilIncentive extends RecyclerView.Adapter<MyRecyclerAdapterDifferencilIncentive.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterDifferencilIncentive(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapterDifferencilIncentive.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.differentil_incentive, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapterDifferencilIncentive.ViewHolder holder, int position) {
            holder.nameTv.setText(jsonObjectal.get(position).optString("solder_name"));
            holder.mobileNOTv.setText(jsonObjectal.get(position).optString("mobile"));
            holder.differential_incentiveTv.setText(getResources().getString(R.string.rs) + jsonObjectal.get(position).optString("differencial_incentive"));
            holder.differential_ipTv.setText(jsonObjectal.get(position).optString("differencial_ip"));
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView nameTv, mobileNOTv, differential_incentiveTv, differential_ipTv;

            public ViewHolder(View itemView) {
                super(itemView);
                nameTv = itemView.findViewById(R.id.nameTv);
                mobileNOTv = itemView.findViewById(R.id.mobileNOTv);
                differential_incentiveTv = itemView.findViewById(R.id.differential_incentiveTv);
                differential_ipTv = itemView.findViewById(R.id.differential_ipTv);
            }
        }
    }

    public class MyRecyclerAdapterTotlIncentive extends RecyclerView.Adapter<MyRecyclerAdapterTotlIncentive.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterTotlIncentive(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapterTotlIncentive.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.total_incentive_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapterTotlIncentive.ViewHolder holder, int position) {
            String monthYear[] = jsonObjectal.get(position).optString("month_year").split("-");
            holder.yearMonth.setText(monthInWord((monthYear[1])) + " " + monthYear[0]);
            if (pos == 1)
                holder.incentiv.setText(getResources().getString(R.string.rs) + jsonObjectal.get(position).optString("incentive"));
            else if (pos == 2)
                holder.incentiv.setText(Commonhelper.roundOff(Double.valueOf(jsonObjectal.get(position).optString("ip"))));
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView yearMonth, incentiv;

            public ViewHolder(View itemView) {
                super(itemView);
                yearMonth = itemView.findViewById(R.id.yearMonth);
                incentiv = itemView.findViewById(R.id.incentiv);
            }
        }
    }

    public class MyRecyclerAdapterRoyalityIncentive extends RecyclerView.Adapter<MyRecyclerAdapterRoyalityIncentive.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterRoyalityIncentive(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapterRoyalityIncentive.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.royality_incentive_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapterRoyalityIncentive.ViewHolder holder, int position) {

            holder.royalityIncentiveTv.setText(getResources().getString(R.string.rs) + jsonObjectal.get(position).optString("royal_incentive"));
            holder.differencial_ip.setText(jsonObjectal.get(position).optString("deffirential_ip"));
            holder.solder_name.setText(jsonObjectal.get(position).optString("solder_name"));
            holder.phoneNO.setText(jsonObjectal.get(position).optString("mobile"));
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView solder_name, phoneNO, royalityIncentiveTv, differencial_ip;

            public ViewHolder(View itemView) {
                super(itemView);
                solder_name = itemView.findViewById(R.id.solder_name);
                phoneNO = itemView.findViewById(R.id.phoneNO);
                royalityIncentiveTv = itemView.findViewById(R.id.royalityIncentiveTv);
                differencial_ip = itemView.findViewById(R.id.differencial_ip);
            }
        }
    }

    public class MyRecyclerAdapterAurgetClub extends RecyclerView.Adapter<MyRecyclerAdapterAurgetClub.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterAurgetClub(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapterAurgetClub.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.aurget_club_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapterAurgetClub.ViewHolder holder, int position) {

        }

        @Override
        public int getItemCount() {
            return 10;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public ViewHolder(View itemView) {
                super(itemView);


            }
        }
    }

    public class MyRecyclerAdapterWalletCashBack extends RecyclerView.Adapter<MyRecyclerAdapterWalletCashBack.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterWalletCashBack(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapterWalletCashBack.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.wallet_cash_back, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapterWalletCashBack.ViewHolder holder, int position) {

        }

        @Override
        public int getItemCount() {
            return 10;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public ViewHolder(View itemView) {
                super(itemView);
            }
        }
    }

    public class MyRecyclerAdapterDirectIP extends RecyclerView.Adapter<MyRecyclerAdapterDirectIP.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterDirectIP(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapterDirectIP.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.direct_ip_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapterDirectIP.ViewHolder holder, int position) {
            try {
                holder.earnedIP.setText("Earned IP \n" + jsonObjectal.get(position).optString("earned_ip"));
                holder.prodNmeTv.setText("" + jsonObjectal.get(position).optString("product_name"));
                holder.priceTv.setText("" + jsonObjectal.get(position).optString("price"));
                holder.qtyTv.setText("" + jsonObjectal.get(position).optString("qtr"));
                holder.orderId.setText("" + jsonObjectal.get(position).optString("order_id"));
                holder.placedON.setText("" + jsonObjectal.get(position).optString("placed_on"));
                Picasso.with(BusinessReport.this).load(jsonObjectal.get(position).optString("product_image")).into(holder.img);
            } catch (Exception e) {

            }
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            ImageView img;
            TextView earnedIP, prodNmeTv, priceTv, qtyTv, orderId, placedON;

            public ViewHolder(View itemView) {
                super(itemView);
                earnedIP = itemView.findViewById(R.id.earnedIP);
                prodNmeTv = itemView.findViewById(R.id.prodNmeTv);
                priceTv = itemView.findViewById(R.id.priceTv);
                qtyTv = itemView.findViewById(R.id.qtyTv);
                orderId = itemView.findViewById(R.id.orderId);
                placedON = itemView.findViewById(R.id.placedON);
                img = itemView.findViewById(R.id.img);
            }
        }
    }

    public class MyRecyclerAdapterDifferencialIP extends RecyclerView.Adapter<MyRecyclerAdapterDifferencialIP.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterDifferencialIP(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapterDifferencialIP.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.differencial_ip_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapterDifferencialIP.ViewHolder holder, int position) {
            holder.differencial_ip.setText("Differencial IP: " + jsonObjectal.get(position).optString("differencial_ip"));
            holder.order_placed.setText("Order Placed On: " + jsonObjectal.get(position).optString("order_placed"));
            holder.solder_name.setText(jsonObjectal.get(position).optString("solder_name"));
            holder.mobile.setText(jsonObjectal.get(position).optString("mobile"));
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView solder_name, mobile, order_placed, differencial_ip;

            public ViewHolder(View itemView) {
                super(itemView);
                solder_name = itemView.findViewById(R.id.solder_name);
                mobile = itemView.findViewById(R.id.mobile);
                order_placed = itemView.findViewById(R.id.order_placed);
                differencial_ip = itemView.findViewById(R.id.differencial_ip);
            }
        }
    }

    public class MyRecyclerAdapterHeader extends RecyclerView.Adapter<MyRecyclerAdapterHeader.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterHeader(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapterHeader.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.bussiness_report_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapterHeader.ViewHolder holder, int position) {
            holder.nameTv.setText("Differencial IP: " + jsonObjectal.get(position).optString("differencial_ip"));
            holder.valueTv.setText("Order Placed On: " + jsonObjectal.get(position).optString("order_placed"));
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView nameTv, valueTv;

            public ViewHolder(View itemView) {
                super(itemView);
                nameTv = itemView.findViewById(R.id.solder_name);
                valueTv = itemView.findViewById(R.id.mobile);
            }
        }
    }
}

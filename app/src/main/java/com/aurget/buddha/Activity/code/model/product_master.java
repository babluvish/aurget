package code.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class product_master implements Serializable {

    String id;
    String category_master_id;
    String sub_category_master_id;
    String product_name;
    String image;
    String price;
    String final_price;
    String product_discount_type;
    String product_discount_amount;
    String quantity;
    String weight;

    public List<code.model.sub_product> getSub_product() {
        return sub_product;
    }

    public void setSub_product(List<code.model.sub_product> sub_product) {
        this.sub_product = sub_product;
    }
    List<sub_product> sub_product = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory_master_id() {
        return category_master_id;
    }

    public void setCategory_master_id(String category_master_id) {
        this.category_master_id = category_master_id;
    }

    public String getSub_category_master_id() {
        return sub_category_master_id;
    }

    public void setSub_category_master_id(String sub_category_master_id) {
        this.sub_category_master_id = sub_category_master_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getFinal_price() {
        return final_price;
    }

    public void setFinal_price(String final_price) {
        this.final_price = final_price;
    }

    public String getProduct_discount_type() {
        return product_discount_type;
    }

    public void setProduct_discount_type(String product_discount_type) {
        this.product_discount_type = product_discount_type;
    }

    public String getProduct_discount_amount() {
        return product_discount_amount;
    }

    public void setProduct_discount_amount(String product_discount_amount) {
        this.product_discount_amount = product_discount_amount;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getUnit_name() {
        return unit_name;
    }

    public void setUnit_name(String unit_name) {
        this.unit_name = unit_name;
    }

    String unit_name;

    public static List<product_master> createJsonInList(String str) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<product_master>>() {
        }.getType();
        return gson.fromJson(str, type);
    }
}

package com.aurget.buddha.Activity;

import android.Manifest;
import android.content.ContentUris;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aurget.buddha.Activity.about.Chat.SendMessageActivity;
import com.aurget.buddha.R;

import static com.aurget.buddha.Activity.Commonhelper.openWhatsApp;
import static com.aurget.buddha.Activity.Commonhelper.phoneCall;

public class SuportActivity extends AppCompatActivity {

    LinearLayout cllPhone;
    LinearLayout whtspp;
    LinearLayout fb;
    TextView chatTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suport);

        cllPhone = findViewById(R.id.cllPhone);
        whtspp = findViewById(R.id.whtspp);
        fb = findViewById(R.id.fb);
        chatTv = findViewById(R.id.chatTv);


        cllPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onCallBtnClick();
            }
        });

        chatTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(SuportActivity.this, SendMessageActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.email).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent send = new Intent(Intent.ACTION_SENDTO);
                String uriText = "mailto:" + Uri.encode("support@aurget.com") +
                        "?subject=" + Uri.encode("Aurget Support") +
                        "&body=" + Uri.encode("");
                Uri uri = Uri.parse(uriText);

                send.setData(uri);

                startActivity(Intent.createChooser(send, "Send Email..."));
            }
        });

        whtspp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                String number = "9662101102";
//                String url = "https://api.whatsapp.com/send?phone="+number;
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                startActivity(i);
                openWhatsApp(SuportActivity.this);
            }
        });

        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isPackageExisted("com.facebook.orca")) {
                    Uri uri = Uri.parse("fb-messenger://user/");
                    uri = ContentUris.withAppendedId(uri, Long.parseLong("496279094177711"));
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } else {
                    Toast.makeText(SuportActivity.this, "Please install facebook messenger", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public boolean isPackageExisted(String targetPackage) {
        PackageManager pm = getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(targetPackage, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }

    private void onCallBtnClick() {
        if (Build.VERSION.SDK_INT < 23) {
            phoneCall(SuportActivity.this);
        } else {

            if (ActivityCompat.checkSelfPermission(SuportActivity.this,
                    Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                phoneCall(SuportActivity.this);
            } else {
                final String[] PERMISSIONS_STORAGE = {Manifest.permission.CALL_PHONE};
                //Asking request Permissions
                ActivityCompat.requestPermissions(SuportActivity.this, PERMISSIONS_STORAGE, 9);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        boolean permissionGranted = false;
        switch (requestCode) {
            case 9:
                permissionGranted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (permissionGranted) {
            phoneCall(SuportActivity.this);
        } else {
            Toast.makeText(SuportActivity.this, "You don't assign permission.", Toast.LENGTH_SHORT).show();
        }
    }

}

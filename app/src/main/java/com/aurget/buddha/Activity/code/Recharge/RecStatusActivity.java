package com.aurget.buddha.Activity.code.Recharge;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.R;

import org.json.JSONException;
import org.json.JSONObject;

import code.utils.AppUrls;

public class RecStatusActivity extends BaseActivity implements View.OnClickListener {

    //ImageView
    ImageView ivLoading,ivCart,ivMenu,ivSearch,ivHome;

    //TextView
    TextView tvDetail,tvTxnId,tvTxnIdFailed;

    //LinearLayout
    LinearLayout llPending,llSuccess,llFailure;

    //RotateAnimation
    RotateAnimation animRotate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rec_status);

        //ImageView
        ivCart =     findViewById(R.id.ivCart);
        ivMenu =     findViewById(R.id.iv_menu);
        ivSearch =   findViewById(R.id.searchmain);
        ivHome =     findViewById(R.id.ivHomeBottom);
        ivLoading = findViewById(R.id.imageView16);

        //TextView
        tvDetail = findViewById(R.id.textView26);
        tvTxnId =  findViewById(R.id.textView28);
        tvTxnIdFailed = findViewById(R.id.textView30);

        //LinearLayout
        llPending = findViewById(R.id.llPending);
        llSuccess = findViewById(R.id.llSuccess);
        llFailure = findViewById(R.id.llFailed);

        ivCart.setOnClickListener(this);
        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);

        ivCart.setVisibility(View.INVISIBLE);
        ivSearch.setVisibility(View.INVISIBLE);

        tvDetail.setText(AppSettings.getString(AppSettings.operatorName)+" - "+AppSettings.getString(AppSettings.circleName)
                +"\n"+AppSettings.getString(AppSettings.transId)
                +"\n"+AppSettings.getString(AppSettings.recMobile)
                +"\nRS"+AppSettings.getString(AppSettings.recAmount));

        tvTxnId.setText(AppSettings.getString(AppSettings.transId));
        tvTxnIdFailed.setText(AppSettings.getString(AppSettings.transId));

        ivMenu.setImageResource(R.drawable.ic_back);

        AnimationSet animSet = new AnimationSet(true);
        animSet.setInterpolator(new DecelerateInterpolator());
        animSet.setFillAfter(true);
        animSet.setFillEnabled(true);

        animRotate = new RotateAnimation(0.0f, -90.0f, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);

        animRotate.setDuration(1500);
        animRotate.setFillAfter(true);
        animSet.addAnimation(animRotate);

        ivLoading.startAnimation(animSet);

        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {

            rechargeStatusApi();

        } else {
            AppUtils.showErrorMessage(tvTxnId, getString(R.string.errorInternet), mActivity);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.iv_menu:
                finish();
                return;
        }
    }

    private void rechargeStatusApi() {

        //AppUtils.showRequestDialog(mActivity);

        Log.v("rechargeStatusApi", AppUrls.rechargeStatus);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {

            json_data.put("txn_id", AppSettings.getString(AppSettings.transId));

            json.put(AppConstants.result, json_data);

            Log.v("rechargeStatusApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.rechargeStatus)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        //AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvTxnId, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvTxnId, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedata(JSONObject response) {

        Log.d("response ", response.toString());

        //AppUtils.hideDialog();

        try {

            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);

            if (jsonObject.getString("res_code").equals("1")) {

                llPending.setVisibility(View.VISIBLE);
                llSuccess.setVisibility(View.GONE);
                llFailure.setVisibility(View.GONE);

                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {

                    rechargeStatusApi();

                } else {
                    AppUtils.showErrorMessage(tvTxnId, getString(R.string.errorInternet), mActivity);
                }
            }
            else if (jsonObject.getString("res_code").equals("2")) {

                llSuccess.setVisibility(View.VISIBLE);
                llPending.setVisibility(View.GONE);
                llFailure.setVisibility(View.GONE);

                finish();
            }
            else
            {
                llSuccess.setVisibility(View.GONE);
                llPending.setVisibility(View.GONE);
                llFailure.setVisibility(View.VISIBLE);
                AppUtils.showErrorMessage(tvTxnId, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }

        } catch (Exception e) {
            AppUtils.showErrorMessage(tvTxnId, String.valueOf(e), mActivity);
        }

    }

}

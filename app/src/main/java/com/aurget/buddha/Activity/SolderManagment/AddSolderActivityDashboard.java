package com.aurget.buddha.Activity.SolderManagment;

import android.app.Dialog;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONObject;

public class AddSolderActivityDashboard extends AppCompatActivity {

    LinearLayout previous,previous2;
    LinearLayout next,next2;
    LinearLayout sponsorLL;
    LinearLayout memberLL;
    LinearLayout bankDetailsLl;

    TextView titleTv,nextBtn,sponserNameTv;
    EditText dobEt, firstNameTv, sponserNameEt, NameEt, lastNameEt, e_mail_addressET, mobile_noEt, sponsor_registered_mobileET;
    ImageView tickImg;

    public void verify_otp()
    {
        Dialog dialog = new Dialog(AddSolderActivityDashboard.this);
        dialog.setContentView(R.layout.verify_otp);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        final EditText strtdt = dialog.findViewById(R.id.otpEt);
        final LinearLayout viewBtn = dialog.findViewById(R.id.next2);


        viewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //sendOtp(mobilNoEt.getText().toString());
            }
        });


        dialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_solder_dashboard);

        titleTv = findViewById(R.id.titleTv);
        dobEt = findViewById(R.id.dobEt);
        previous = findViewById(R.id.previous);
        previous2 = findViewById(R.id.previous2);
        next = findViewById(R.id.next);
        next2 = findViewById(R.id.next2);
        nextBtn = findViewById(R.id.nextBtn);
        sponsorLL = findViewById(R.id.sponsorLL);
        memberLL = findViewById(R.id.memberLL);
        bankDetailsLl = findViewById(R.id.bankDetailsLl);

        sponserNameEt = findViewById(R.id.sponserNameEt);
        NameEt = findViewById(R.id.NameEt);
        lastNameEt = findViewById(R.id.lastNameEt);
        e_mail_addressET = findViewById(R.id.e_mail_addressET);
        mobile_noEt = findViewById(R.id.mobile_noEt);
        sponsor_registered_mobileET = findViewById(R.id.sponsor_registered_mobileET);

        tickImg = findViewById(R.id.tickImg);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            previous.setBackgroundTintList(getResources().getColorStateList(R.color.blueLight));
        }

        previous2.setVisibility(View.GONE);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                count++;

                Log.i("count++", String.valueOf(count));

                if (count==1)
                {
                    titleTv.setText(getResources().getString(R.string.solderDetails));
                    next.setEnabled(true);
                    nextBtn.setText(R.string.submit);

                    previous.setEnabled(true);

                    previous2.setVisibility(View.VISIBLE);
                    previous.setVisibility(View.GONE);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        next.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                        previous.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                    }

                    memberLL.setVisibility(View.VISIBLE);
                    sponsorLL.setVisibility(View.GONE);
                    bankDetailsLl.setVisibility(View.GONE);


                    nextBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final Dialog dialog = new Dialog(AddSolderActivityDashboard.this);
                            dialog.setContentView(R.layout.success_popoup);
                            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                            TextView smsTv = dialog.findViewById(R.id.smsTv);
                            Button okBtn = dialog.findViewById(R.id.okBtn);

                            smsTv.setText("Congratulations! You are Successfully! \n Registered With Aurget");

                            okBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    Commonhelper.dismiss();
                                }
                            });
                            dialog.show();
                        }
                    });
                }
            }
        });


        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                count--;
                Log.i("count++", String.valueOf(count));

                if (count==1) {

                    next.setEnabled(true);
                    nextBtn.setText(getString(R.string.continu));
                    //tickImg.setImageResource(R.drawable.ic_arrow_forward_black_24dp);
                    previous.setEnabled(true);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        next.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                        previous.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                    }

                    memberLL.setVisibility(View.VISIBLE);
                    sponsorLL.setVisibility(View.GONE);
                    bankDetailsLl.setVisibility(View.GONE);

                    previous.setVisibility(View.VISIBLE);
                    previous2.setVisibility(View.GONE);
                }

                else
                {
                    previous.setVisibility(View.VISIBLE);
                    previous2.setVisibility(View.GONE);

                    titleTv.setText(getResources().getString(R.string.solder_registration));
                    nextBtn.setText(getString(R.string.continu));
                    //tickImg.setImageResource(R.drawable.ic_arrow_forward_black_24dp);
                    next.setEnabled(true);
                    previous.setEnabled(false);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        next.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
                        previous.setBackgroundTintList(getResources().getColorStateList(R.color.blueLight));
                    }

                    bankDetailsLl.setVisibility(View.GONE);
                    memberLL.setVisibility(View.GONE);
                    sponsorLL.setVisibility(View.VISIBLE);
                }
            }
        });

        previous2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                previous.setVisibility(View.VISIBLE);
                previous2.setVisibility(View.GONE);
                previous.performClick();
            }
        });

        next2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                verify_otp();
                    //hitPi();
            }
        });

        sponsor_registered_mobileET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (sponsor_registered_mobileET.getText().toString().length()==10)
                getSponsorNameByMobileNo(sponsor_registered_mobileET.getText().toString());
            }
        });

    }

    private void hitPi()
    {
        final Dialog dialog = Commonhelper.loadDialog(AddSolderActivityDashboard.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, InterfaceClass.after_login_registration+"username="+NameEt.getText().toString()
                +"&email="+e_mail_addressET.getText().toString()
                +"&phone="+mobile_noEt.getText().toString()
                +"&phone"+lastNameEt.getText().toString()
                +"&sponsor="+sponsor_registered_mobileET.getText().toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String Res = response;
                            Log.d("dsd",Res);

                            JSONArray jsonArray = new JSONArray(Res);

                            if (jsonArray.optJSONObject(0).optString("Status").equalsIgnoreCase("true"))
                            {
                                Commonhelper.showToastLong(AddSolderActivityDashboard.this,"Congratulations! you are registered with aurget successfully!");
                                finish();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("dsd","sssss");
                        }
                        dialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        dialog.dismiss();
                        Commonhelper.showToastLong(AddSolderActivityDashboard.this,getString(R.string.serverError));

                    }
                }) {
        };

        RequestQueue requestQueue = Volley.newRequestQueue(AddSolderActivityDashboard.this);
        requestQueue.add(stringRequest);

    }

    private void getSponsorNameByMobileNo(String pnoneNo)
    {
        //http://aurget.com/budhaa/User/sponsor_name?contact_no=9662101102

        new AbstrctClss(AddSolderActivityDashboard.this, ConstntApi.sponsor_name(pnoneNo), "p", true) {
            @Override
            public void responce(String s) {

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(s);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String sponsor_name = jsonObject.optString("sponsor_name");
                    sponserNameEt.setText(sponsor_name);
                } catch (Exception e) {
                    e.printStackTrace();
                    sponserNameEt.setText("N/A");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                sponserNameEt.setText("N/A");
            }
        };

    }

    int count = 0;
}

package com.aurget.buddha.Activity.code.Account;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.Product.CartListActivity;
import com.aurget.buddha.Activity.code.Search.SearchActivity;
import com.aurget.buddha.R;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class UserAgreementActivity extends AppCompatActivity implements View.OnClickListener  {

    //ImageView
    ImageView ivCart,ivMenu,ivSearch,ivHome;

    //TextView
    TextView tvCount,tvMain,tvAgreement;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_agreement);


        //TextView
        tvCount =      findViewById(R.id.tvCount);
        tvMain =       findViewById(R.id.tvHeaderText);
        tvAgreement =  findViewById(R.id.tvAgreement);

        //ImageView
        ivCart =    findViewById(R.id.ivCart);
        ivMenu =    findViewById(R.id.iv_menu);
        ivSearch =  findViewById(R.id.searchmain);
        ivHome =    findViewById(R.id.ivHomeBottom);

        ivCart.setOnClickListener(this);
        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);

        ivMenu.setImageResource(R.drawable.ic_back);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            tvAgreement.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        }


        if (PoliciesActivity.status==1) {
            tvMain.setText("Buyer Protection Policies");

            tvAgreement.setText(R.string.terms_and_privacy_policy);
        }
        else if (PoliciesActivity.status==2)
        {  tvMain.setText("Cancellation Policy");
            tvAgreement.setText(R.string.cancellationPolicy);
        } else if (PoliciesActivity.status==3)
        {
            tvMain.setText("Return/Replacement Policy");
            tvAgreement.setText(R.string.return_policy);
        }
        else if (PoliciesActivity.status==4)
        {    tvMain.setText("Refund Policy");
            tvAgreement.setText(R.string.refund);
        }
        else if (PoliciesActivity.status==5)
        {
            tvMain.setText("Shipping Policy");
            tvAgreement.setText(R.string.shipping);
        }
        else if (PoliciesActivity.status==6)
        {   tvMain.setText("User Agreement");
          //  tvAgreement.setText(R.string.user_agreement);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.iv_menu:
                finish();
                return;
            case R.id.searchmain:
                startActivity(new Intent(getBaseContext(), SearchActivity.class));
                return;

            case R.id.ivCart:

                if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else if (AppSettings.getString(AppSettings.verified).contains("0")) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else {
                    startActivity(new Intent(getBaseContext(), CartListActivity.class));
                }
                return;
        }
    }
}

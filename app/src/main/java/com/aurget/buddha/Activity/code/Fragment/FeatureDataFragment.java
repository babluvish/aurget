package com.aurget.buddha.Activity.code.Fragment;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.code.Adapter.MyPagerAdapter;
import com.aurget.buddha.Activity.code.Category.CategoryActivity;
import com.aurget.buddha.Activity.code.Category.SubCategoryActivity;
import com.aurget.buddha.Activity.code.Common.CustomLoader;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Dashboard.Slider.CardPagerAdapter;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Database.DatabaseController;
import com.aurget.buddha.Activity.code.Database.TableFavourite;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.Product.ProductDetailsActivity;
import com.aurget.buddha.Activity.code.Product.ProductListActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseFragment;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import code.Common.Config;
import code.model.product_master;
import code.model.sub_product;
import code.utils.AppUrls;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeatureDataFragment extends BaseFragment implements View.OnClickListener {
    public static int VB_PAGES;
    public static int VB_LOOPS = 1000;
    public static int VB_FIRST_PAGE;
    public static String regId = "";
    // ViewPager currentpage
    private static int currentPage = 0;
    // ViewPager no offpage
    private static int NUM_PAGES = 0;
    View rootView;
    ArrayList<HashMap<String, String>> CategoryList = new ArrayList();
    ArrayList<HashMap<String, String>> FeaturedBannerList = new ArrayList();
    ArrayList<HashMap<String, String>> OfferList = new ArrayList();
    ArrayList<ArrayList<HashMap<String, String>>> ProducttList = new ArrayList();
    ArrayList<HashMap<String, String>> TopBannerList = new ArrayList();
    ArrayList<HashMap<String, String>> FeaturedProductList = new ArrayList();
    ArrayList<HashMap<String, String>> hashMapArrayList = new ArrayList<>();
    ArrayList<HashMap<String, String>> subProductListArrayList = new ArrayList();
    ArrayList<HashMap<String, String>> datalist = new ArrayList();
    ArrayList spinnerName = new ArrayList();
    ArrayList spinnerId = new ArrayList();
    boolean loadMore = true;
    RecyclerView recyclerView;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    GridLayoutManager layoutManager;
    ImageView ivCat1, ivCat2, ivCat3, ivCat4, ivDeal1, ivDeal2, ivDeal3, ivDeal4, ivDrop, ivMenu, ivSearch, ivCart, ivLogo;
    LinearLayout llCat1, llCat2, llCat3, llCat4, llDealOfDay, llMore, llOffers, llHome, llCategory, llFavourites, llProfile, llNotification;
    TextView tvHome, tvCategory, tvFavourites, tvProfile, tvNotification, tvCount;
    ImageView ivHome, ivCategory, ivFavourites, ivProfile, ivNotification;
    CardPagerAdapter mCardAdapter;
    MyPagerAdapter adapter;
    ViewPager mViewPager;
    OfferAdapter offerAdapter;
    RelativeLayout rlDeal1, rlDeal2, rlDeal3, rlDeal4, rrRecharge, rrProfile, rrCategory, rrSchool, rrShare, rrInsurance, rrSearch;
    ScrollView scrollView;
    String offset = "0";
    Timer timer;
    TextView tvCat1, tvCat2, tvCat3, tvCat4, tvClock, tvDName1, tvDName2, tvDName3, tvDName4, tvDPrice1, tvDPrice2, tvDPrice3,
            tvDPrice4, tvLogin, tvUserName, tvMain, tvViewDOD;
    View view;
    Typeface typeface;
    int FIRST_PAGE;
    int PAGES;
    private Handler handler;
    private boolean loading = true;
    private Runnable runnable;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private LinearLayout dotsLayout;
    private ImageView[] dots;
    SwipeRefreshLayout swipeRefreshLayout;
    CustomLoader loader;
    int pagecount = 0;

    String Subid;
    public FeatureDataFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate( R.layout.fragment_feature_data, container, false );
        init();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                loader.show();
                loader.setCancelable(false);
                loader.setCanceledOnTouchOutside(false);
                offset="0";
                FeaturedProductList.clear();
                datalist.clear();
                GetDashboardListApi();

            }
        });

        return rootView;
    }

    private void init() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals( Config.REGISTRATION_COMPLETE )) {
                    //FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId();

                    AppSettings.putString( AppSettings.fcmId, regId );

                } else if (intent.getAction().equals( Config.PUSH_NOTIFICATION )) {

                    String message = intent.getStringExtra( "message" );
                    Log.v( "msg", message );
                }
            }
        };
//Todo... commentLine
       /* if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d( "Key: ", key + " Value: " + value);
            }
        }
*/
        //TextView
        loader               =new CustomLoader(getContext(), android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        tvMain = rootView.findViewById( R.id.tvHeaderText );
        //ViewPager
        mViewPager = rootView.findViewById( R.id.myviewpager );
        //LinearLayout
        llMore = rootView.findViewById( R.id.llMore );
        llDealOfDay = rootView.findViewById( R.id.llDealOfDay );
        llOffers = rootView.findViewById( R.id.llOffers );
        llCat1 = rootView.findViewById( R.id.llCat1 );
        llCat2 = rootView.findViewById( R.id.llCat2 );
        llCat3 = rootView.findViewById( R.id.llCat3 );
        llCat4 = rootView.findViewById( R.id.llCat4 );
        swipeRefreshLayout=rootView.findViewById(R.id.swiperefresh);
        //ScrollView
        scrollView = rootView.findViewById( R.id.scroll_side_menu );

        //View
        view = rootView.findViewById( R.id.view );
        //RelativeLayout
        rlDeal1 = rootView.findViewById( R.id.rlD1 );
        rlDeal2 = rootView.findViewById( R.id.rlD2 );
        rlDeal3 = rootView.findViewById( R.id.rlD3 );
        rlDeal4 = rootView.findViewById( R.id.rlD4 );

        rrRecharge = rootView.findViewById( R.id.rr_recharge );
        rrProfile = rootView.findViewById( R.id.rr_profile );
        rrCategory = rootView.findViewById( R.id.rr_category );
        rrSchool = rootView.findViewById( R.id.rrSchool );
        rrShare = rootView.findViewById( R.id.rrShare );
        rrInsurance = rootView.findViewById( R.id.rrInsurance );
        rrSearch = rootView.findViewById( R.id.rlSearch );

        //ImageView
        ivMenu = rootView.findViewById( R.id.iv_menu );
        ivDrop = rootView.findViewById( R.id.imageView3 );
        ivSearch = rootView.findViewById( R.id.searchmain );
        ivDeal1 = rootView.findViewById( R.id.ivProduct );
        ivDeal2 = rootView.findViewById( R.id.ivD2 );
        ivDeal3 = rootView.findViewById( R.id.ivD3 );
        ivDeal4 = rootView.findViewById( R.id.ivD4 );
        ivCat1 = rootView.findViewById( R.id.ivCat1 );
        ivCat2 = rootView.findViewById( R.id.ivCat2 );
        ivCat3 = rootView.findViewById( R.id.ivCat3 );
        ivCat4 = rootView.findViewById( R.id.ivCat4 );
        ivHome = rootView.findViewById( R.id.ivHomeBottom );
        ivCategory = rootView.findViewById( R.id.ivCategoryBottom );
        ivFavourites = rootView.findViewById( R.id.ivFavouritesBottom );
        ivProfile = rootView.findViewById( R.id.ic_profile );
        ivNotification = rootView.findViewById( R.id.ic_notification );
        ivCart = rootView.findViewById( R.id.ivCart );
        ivLogo = rootView.findViewById( R.id.iv_logo );

        //TextView
        tvDName1 = rootView.findViewById( R.id.tvProductName );
        tvDName2 = rootView.findViewById( R.id.tvD2 );
        tvDName3 = rootView.findViewById( R.id.tvD3 );
        tvDName4 = rootView.findViewById( R.id.tvD4 );
        tvDPrice1 = rootView.findViewById( R.id.tvFPrice );
        tvDPrice2 = rootView.findViewById( R.id.tvDPrice2 );
        tvDPrice3 = rootView.findViewById( R.id.tvDPrice3 );
        tvDPrice4 = rootView.findViewById( R.id.tvDPrice4 );
        tvCat1 = rootView.findViewById( R.id.tvCat1 );
        tvCat2 = rootView.findViewById( R.id.tvCat2 );
        tvCat3 = rootView.findViewById( R.id.tvCat3 );
        tvCat4 = rootView.findViewById( R.id.tvCat4 );
        tvClock = rootView.findViewById( R.id.tvClock );
        tvViewDOD = rootView.findViewById( R.id.tvViewDOD );

        tvHome = rootView.findViewById( R.id.tvHomeBottom );
        tvCategory = rootView.findViewById( R.id.tvCategoryBottom );
        tvFavourites = rootView.findViewById( R.id.tvFavouritesBottom );
        tvProfile = rootView.findViewById( R.id.tvProfileBottom );
        tvNotification = rootView.findViewById( R.id.tvNotificationBottom );
        tvCount = rootView.findViewById( R.id.tvCount );
        tvLogin = rootView.findViewById( R.id.tv_login );
        tvUserName = rootView.findViewById( R.id.tvUserName );

        //LinearLayout fro bottom views
        llHome = rootView.findViewById( R.id.llHome );
        llCategory = rootView.findViewById( R.id.llCategory );
        llFavourites = rootView.findViewById( R.id.llFavourites );
        llProfile = rootView.findViewById( R.id.llProfile );
        llNotification = rootView.findViewById( R.id.llNotification );

        rrSearch.setVisibility( View.VISIBLE );
        //Recyclerview
        recyclerView = rootView.findViewById( R.id.recyclerview );

        dotsLayout = rootView.findViewById( R.id.layoutDots );
        // recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        layoutManager = new GridLayoutManager( mActivity, 2 );
        recyclerView.setLayoutManager( layoutManager );

        recyclerView.addOnScrollListener( new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    Log.v( "ksqbmbq", "1" );
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        Log.v( "ksqbmbq", "2" );

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            GetDashboardListApi();
                            loading = false;
                            Log.v( "ksqbmbq", "3" );
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        } );

        //SetOnClickListener

        Onclick();

        ivLogo.setVisibility( View.VISIBLE );
        ivMenu.setVisibility( View.GONE );

        typeface = Typeface.createFromAsset( mActivity.getAssets(), "centurygothic.otf" );
        try {
            int count = Integer.parseInt( AppSettings.getString( AppSettings.cartCount ) );

            if (count > 0) {
                tvCount.setVisibility( View.VISIBLE );
                tvCount.setText( String.valueOf( count ) );
            } else {
                tvCount.setVisibility( View.GONE );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
            GetDashboardListApi();
            getCartListApi();
            sendFCMApi();
        } else {
            AppUtils.showErrorMessage( tvMain, getString( R.string.errorInternet ), mActivity );
        }

        getCatData();
    }

    public void getCatData() {

        int j = 0;

        CategoryList.clear();
        CategoryList.addAll( DatabaseController.getFeaturedCategoryData() );
        // CategoryList.addAll(DatabaseController.getSubCategoryData(TableSubCategory.sub_category, TableSubCategory.subCatColumn.category_id.toString(), "4"));
        for (int i = 0; i < CategoryList.size(); i++) {
            if (j == 0) {
                j = j + 1;
                llCat1.setVisibility( View.VISIBLE );
                llCat1.setTag( ((HashMap) CategoryList.get( i )).get( "category_id" ) );
                Log.v( "vegetables", (String) ((HashMap) CategoryList.get( i )).get( "category_name" ) );
                tvCat1.setText( AppUtils.split( (String) ((HashMap) CategoryList.get( i )).get( "category_name" ) ) );
                if (((String) ((HashMap) CategoryList.get( i )).get( "app_icon" )).isEmpty()) {
                    Picasso.with( mActivity ).load( R.mipmap.ic_launcher ).into( ivCat1 );
                } else {
                    Picasso.with( mActivity ).load( (String) ((HashMap) CategoryList.get( i )).get( "app_icon" ) ).into( ivCat1 );
                }
            } else if (j == 1) {
                j = j + 1;
                llCat2.setVisibility( View.VISIBLE );
                llCat2.setTag( ((HashMap) CategoryList.get( i )).get( "category_id" ) );
                tvCat2.setText( AppUtils.split( (String) ((HashMap) CategoryList.get( i )).get( "category_name" ) ) );
                if (((String) ((HashMap) CategoryList.get( i )).get( "app_icon" )).isEmpty()) {
                    Picasso.with( mActivity ).load( R.mipmap.ic_launcher ).into( ivCat2 );
                } else {
                    Picasso.with( mActivity ).load( (String) ((HashMap) CategoryList.get( i )).get( "app_icon" ) ).into( ivCat2 );
                }

            } else if (j == 2) {
                j = j + 1;
                llCat3.setVisibility( View.VISIBLE );
                llCat3.setTag( ((HashMap) CategoryList.get( i )).get( "category_id" ) );
                tvCat3.setText( AppUtils.split( (String) ((HashMap) CategoryList.get( i )).get( "category_name" ) ) );
                if (((String) ((HashMap) CategoryList.get( i )).get( "app_icon" )).isEmpty()) {
                    Picasso.with( mActivity ).load( R.mipmap.ic_launcher ).into( ivCat3 );
                } else {
                    Picasso.with( mActivity ).load( (String) ((HashMap) CategoryList.get( i )).get( "app_icon" ) ).into( ivCat3 );
                }

            } else if (j == 3) {
                j = j + 1;
                llCat4.setVisibility( View.VISIBLE );
                llCat4.setTag( ((HashMap) CategoryList.get( i )).get( "category_id" ) );
                tvCat4.setText( AppUtils.split( (String) ((HashMap) CategoryList.get( i )).get( "category_name" ) ) );
                if (((String) ((HashMap) CategoryList.get( i )).get( "app_icon" )).isEmpty()) {
                    Picasso.with( mActivity ).load( R.mipmap.ic_launcher ).into( ivCat4 );
                } else {
                    Picasso.with( mActivity ).load( (String) ((HashMap) CategoryList.get( i )).get( "app_icon" ) ).into( ivCat4 );
                }

            }
        }
    }

    private void Onclick() {
        llHome.setOnClickListener( this );
        llCategory.setOnClickListener( this );
        llFavourites.setOnClickListener( this );
        llProfile.setOnClickListener( this );
        llNotification.setOnClickListener( this );


        ivMenu.setOnClickListener( this );
        ivSearch.setOnClickListener( this );
        llMore.setOnClickListener( this );
        rlDeal1.setOnClickListener( this );
        rlDeal2.setOnClickListener( this );
        rlDeal3.setOnClickListener( this );
        rlDeal4.setOnClickListener( this );
        tvViewDOD.setOnClickListener( this );
        llCat1.setOnClickListener( this );
        llCat2.setOnClickListener( this );
        llCat3.setOnClickListener( this );
        llCat4.setOnClickListener( this );
        ivCart.setOnClickListener( this );
        rrRecharge.setOnClickListener( this );
        rrProfile.setOnClickListener( this );
        rrCategory.setOnClickListener( this );
        rrSchool.setOnClickListener( this );
        rrShare.setOnClickListener( this );
        rrInsurance.setOnClickListener( this );
    }

    private void addBottomDots( int currentPage) {
        // dotscount = viewPagerAdapter.getCount();
        dots = new ImageView[TopBannerList.size()];
        //  dots[currentPage].setImageResource(R.drawable.ic_circle_white);
        dotsLayout.removeAllViews();
        for (int i = 0; i < TopBannerList.size(); i++) {
            dots[i] = new ImageView(getContext());

            dots[i].setImageResource(R.drawable.ic_circle_white);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(20,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(5, 0, 5, 0);

            dotsLayout.addView(dots[i],params);



        }
        if (TopBannerList.size() > 0)
            dots[currentPage].setImageResource(R.drawable.ic_circle_blue);

    }

    private void displayFirebaseRegId() {
        /*SharedPreferences pref = getApplicationContext().getSharedPreferences( Config.SHARED_PREF, 0 );
        regId = pref.getString( "regId", null );
        Log.e( "Firebase reg id: ", regId );
        if (!TextUtils.isEmpty( regId )) {            // txtRegId.setText("Firebase Reg Id: " + regId);
            Log.v( "firebaseid", "Firebase Reg Id: " + regId );
        } else {
            Log.v( "not_received", "Firebase Reg Id is not received yet!" );
        }*/
    }

    private void sendFCMApi() {
        Log.v( "sendFCMApi", AppUrls.notificationMaster );

        AppUtils.showRequestDialog( mActivity );
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put( "device_id", AppUtils.getDeviceID( mActivity ) );
            json_data.put( "fcm_id", AppSettings.getString( AppSettings.fcmId ) );
            json.put( AppConstants.result, json_data );

            Log.v( "sendFCMApi", json.toString() );

        } catch (JSONException e) {
            e.printStackTrace();
            Log.v( "printStack", String.valueOf( e ) );
        }

        AndroidNetworking.post( AppUrls.notificationMaster )
                .addJSONObjectBody( json )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseSendFCMdata( response );
                    }

                    @Override
                    public void onError(ANError error) {

                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage( tvMain, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            AppUtils.showErrorMessage( tvMain, String.valueOf( error.getErrorDetail() ), mActivity );
                        }
                    }
                } );
    }

    private void parseSendFCMdata(JSONObject response) {

        Log.d( "response ", response.toString() );
        AppUtils.hideDialog();
        /*  AppSettings.putString(AppSettings.fcmSend,"0");*/

        try {
            JSONObject jsonObject = response.getJSONObject( AppConstants.responseTag );
            if (jsonObject.getString( "res_code" ).equals( "1" )) {

                /* AppSettings.putString(AppSettings.fcmSend,"1");*/

            } else {
                AppUtils.showErrorMessage( tvMain, String.valueOf( jsonObject.getString( "res_msg" ) ), mActivity );
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage( tvMain, String.valueOf( e ), mActivity );
        }
    }

    private void GetDashboardListApi() {
        Log.v( "GetDashboardListApi", AppUrls.FeaturedProduct );
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put( "offset", offset );
            if(AppSettings.getString(AppSettings.userId) .isEmpty()){
                json_data.put( "user_id","0");
            }else{
                json_data.put( "user_id",AppSettings.getString(AppSettings.userId) );
            }
            json.put( AppConstants.result, json_data );
            Log.v( "featuredApi", json.toString() );

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post( AppUrls.FeaturedProduct )
                .addJSONObjectBody( json )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata( response );
                    }
                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage( tvCat4, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            AppUtils.showErrorMessage( tvCat4, String.valueOf( error.getErrorDetail() ), mActivity );
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            //Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        }
                    }
                } );
    }

   private void parseJsondata(JSONObject response) {
       Log.d( "response ", response.toString() );
       loader.cancel();
       TopBannerList.clear();
       FeaturedBannerList.clear();
       OfferList.clear();
       try {
           JSONObject jsonObject = response.getJSONObject( AppConstants.responseTag );
           if (jsonObject.getString( "res_code" ).equals( "1" )) {

               offset = jsonObject.getString( "offset" );
               int i;
               JSONArray bannerArray = jsonObject.getJSONArray( "banner" );
               for (i = 0; i < bannerArray.length(); i++) {
                   JSONObject bannerArrayJSONObject = bannerArray.getJSONObject( i );
                   HashMap<String, String> bannerlist = new HashMap();
                   bannerlist.put( "banner_id", bannerArrayJSONObject.getString( "banner_id" ) );
                   bannerlist.put( "banner_on", bannerArrayJSONObject.getString( "banner_on" ) );
                   bannerlist.put( "id", bannerArrayJSONObject.getString( "id" ) );
                   bannerlist.put( "featured_banner", bannerArrayJSONObject.getString( "featured_banner" ) );
                   bannerlist.put( "banner_image", bannerArrayJSONObject.getString( "banner_image" ) );
                   if (bannerArrayJSONObject.getString( "featured_banner" ).equals( "1" )) {
                       TopBannerList.add( bannerlist );
                   } else {
                       FeaturedBannerList.add( bannerlist );
                   }
               }
               String listInString = jsonObject.getString( "features_product" );
               List<product_master> productList = product_master.createJsonInList( listInString );
               JSONArray productArray = jsonObject.getJSONArray( "features_product" );
                   for (i = 0; i < productArray.length(); i++) {
                       JSONObject productArrayJSONObject = productArray.getJSONObject(i);
                       if(productArrayJSONObject.has("banner_image")) {
                           HashMap<String, String> sliderBanner = new HashMap();
                           sliderBanner.put( "banner_image", productArrayJSONObject.getString( "banner_image" ) );
                           subProductListArrayList.add(sliderBanner);
                       }else {
                           HashMap<String, String> productlist = new HashMap();
                           productlist.put("id", productArrayJSONObject.getString("id"));
                           productlist.put("category_id", productArrayJSONObject.getString("category_master_id"));
                           productlist.put("sub_category_master_id", productArrayJSONObject.getString("sub_category_master_id"));
                           productlist.put("product_name", productArrayJSONObject.getString("product_name"));
                           productlist.put("product_image", productArrayJSONObject.getString("image"));
                           productlist.put("price", productArrayJSONObject.getString("price"));
                           productlist.put("final_price", productArrayJSONObject.getString("final_price"));
                           productlist.put("weight", productArrayJSONObject.getString("weight"));
                           productlist.put("unit_name", productArrayJSONObject.getString("unit_name"));
                           productlist.put("product_discount_type", productArrayJSONObject.getString("product_discount_type"));
                           productlist.put("product_discount_amount", productArrayJSONObject.getString("product_discount_amount"));
                           productlist.put("sub_product", productArrayJSONObject.getString("sub_product"));
                           if (productArrayJSONObject.has("sub_product")) {
                               JSONArray newsliderarray = productArrayJSONObject.getJSONArray("sub_product");
                               for (int j = 0; j < newsliderarray.length(); j++) {
                                   JSONObject sliderObject = newsliderarray.getJSONObject(j);
                                   HashMap<String, String> sliderMap = new HashMap<>();
                                   if (j == 0) {
                                       productlist.put("SpinerValue", sliderObject.get("sub_weight").toString() + sliderObject.get("unit_name").toString());
                                   }
                                   sliderMap.put("sub_id", sliderObject.get("sub_id").toString());
                                   sliderMap.put("dis_type", sliderObject.get("dis_type").toString());
                                   sliderMap.put("per_amount", sliderObject.get("per_amount").toString());
                                   sliderMap.put("sub_weight", sliderObject.get("sub_weight").toString());
                                   sliderMap.put("unit_name", sliderObject.get("unit_name").toString());
                                   sliderMap.put("actual_p", sliderObject.get("actual_p").toString());
                                   sliderMap.put("price", sliderObject.get("price").toString());
                                   sliderMap.put("parentId", productArrayJSONObject.getString("id"));
                                   sliderMap.put("cart_quantity", sliderObject.getString("cart_quantity"));
                                   datalist.add(sliderMap);
                                   Log.v("dshf", sliderObject.get("dis_type").toString());
                               }
                           }
                           FeaturedProductList.add(productlist);

                       }


                   }

               //  Adapter adapter = new Adapter( productList );
               offerAdapter = new OfferAdapter( FeaturedProductList, datalist );
               recyclerView.setAdapter( offerAdapter );
               recyclerView.setNestedScrollingEnabled( false );
           } else {
               AppUtils.showErrorMessage( tvMain, String.valueOf( jsonObject.getString( "res_msg" ) ), mActivity );
           }
       } catch (Exception e) {
           AppUtils.showErrorMessage( tvMain, String.valueOf( e ), mActivity );
       }
       AppUtils.hideDialog();
       topBannerAdapter();
       if (TopBannerList.size() > 0) {
           TimerTask timerTask = new MyTimerTask();
           timer = new Timer();
           timer.schedule( timerTask, 3000, 3000 );
       }
       AppSettings.putString( AppSettings.cartCount, "0" );
       if (!AppSettings.getString( AppSettings.userId ).isEmpty()) {
           if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
               getCartListApi();
           } else {
               AppUtils.showErrorMessage( tvMain, getString( R.string.errorInternet ), mActivity );
           }
       } else {
           if (AppSettings.getString( AppSettings.fcmSend ).equals( "0" ) || AppSettings.getString( AppSettings.fcmSend ).isEmpty()) {
               if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                   sendFCMApi();
               } else {
                   AppUtils.showErrorMessage( tvMain, getString( R.string.errorInternet ), mActivity );
               }
           }
       }
   }
    private void getCartListApi() {
        // AppUtils.showRequestDialog(mActivity);
        Log.v( "getCartListApi", AppUrls.getCart );
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put( "user_master_id", AppSettings.getString( AppSettings.userId ) );
            json.put( AppConstants.result, json_data );
            Log.v( "getCartListApi", json.toString() );

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post( AppUrls.getCart )
                .addJSONObjectBody( json )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        parsecarttdata( response );
                    }
                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage( tvMain, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            AppUtils.showErrorMessage( tvMain, String.valueOf( error.getErrorDetail() ), mActivity );
                        }
                    }
                } );
    }
    private void parsecarttdata(JSONObject response) {
        AppSettings.putString( AppSettings.cartCount, "0");
        Log.d( "response ", response.toString() );
        try {
            JSONObject jsonObject = response.getJSONObject( AppConstants.responseTag );
            if (jsonObject.getString( "res_code" ).equals( "1" )) {
                JSONArray productArray = jsonObject.getJSONArray( "cart_product" );

                AppSettings.putString( AppSettings.cartCount, String.valueOf( productArray.length() ) );
                if (productArray.length() > 0) {
                    tvCount.setVisibility( View.VISIBLE );
                    tvCount.setText( String.valueOf( productArray.length() ) );
                    ((DashBoardFragment) getActivity()).tvCount.setText(String.valueOf( productArray.length() ));
                    ((DashBoardFragment) getActivity()).tvCount.setVisibility(View.VISIBLE);
                    Log.v("jjdj",String.valueOf( productArray.length() ));
                } else {
                    tvCount.setVisibility( View.GONE );
                }
            } else {
                tvCount.setVisibility( View.GONE );
                AppSettings.putString( AppSettings.cartCount, "0");
            }
        } catch (Exception e) {
            tvCount.setVisibility( View.GONE );
            AppSettings.putString( AppSettings.cartCount, "0" );
        }
        AppUtils.hideDialog();
        /*if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getProfileApi();
        } else {
            AppUtils.showErrorMessage(tvMain, getString(R.string.errorInternet), mActivity);
        }*/
    }

    private void getProfileApi() {
        AppUtils.showRequestDialog( mActivity );
        Log.v( "getProfileApi", AppUrls.getUserDetail );
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put( "user_id", AppSettings.getString( AppSettings.userId ) );
            json.put( AppConstants.result, json_data );
            Log.v( "getProfileApi", json.toString() );

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post( AppUrls.getUserDetail )
                .addJSONObjectBody( json )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseProfiledata( response );
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage( tvMain, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            AppUtils.showErrorMessage( tvMain, String.valueOf( error.getErrorDetail() ), mActivity );
                        }
                    }
                } );
    }

    private void parseProfiledata(JSONObject response) {
        Log.d( "response ", response.toString() );
        try {
            JSONObject jsonObject = response.getJSONObject( AppConstants.responseTag );
            if (jsonObject.getString( "res_code" ).equals( "1" )) {

                JSONObject userObject = jsonObject.getJSONObject( "user_detail" );
                AppSettings.putString( AppSettings.name, userObject.getString( "username" ) );
                AppSettings.putString( AppSettings.email, userObject.getString( "email_id" ) );
                AppSettings.putString( AppSettings.gender, userObject.getString( "gender" ) );
                AppSettings.putString( AppSettings.mobile, userObject.getString( "phone_no" ) );
                AppSettings.putString( AppSettings.wallet, userObject.getString( "wallet_amount" ) );
                AppSettings.putString( AppSettings.referralId, userObject.getString( "referel_id" ) );

            } else {
                AppUtils.showErrorMessage( tvMain, String.valueOf( jsonObject.getString( "res_msg" ) ), mActivity );
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage( tvMain, String.valueOf( e ), mActivity );
        }
        AppUtils.hideDialog();

        onResume();

        if (AppSettings.getString( AppSettings.fcmSend ).equals( "0" ) || AppSettings.getString( AppSettings.fcmSend ).isEmpty()) {
            if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                //sendFCMApi();
            } else {
                AppUtils.showErrorMessage( tvMain, getString( R.string.errorInternet ), mActivity );
            }
        }
    }

    public void topBannerAdapter() {
        currentPage++;
        NUM_PAGES = TopBannerList.size();
        VB_PAGES = TopBannerList.size();
        VB_FIRST_PAGE = VB_PAGES / 2;

        // adapter=new MyPagerAdapter(getApplicationContext(),getSupportFragmentManager(),TopBannerList);

        /*mCardAdapter = new CardPagerAdapter( getApplicationContext(), TopBannerList ) {
            protected void onCategoryClick(View v, String position) {
                loginCheck( position);
               *//* if (TopBannerList.get( Integer.parseInt( position ) ).get( "banner_on" ).equals( "1" )) {
                    Intent mIntent = new Intent( mActivity, ProductListActivity.class );
                    mIntent.putExtra( AppConstants.subCategoryId, TopBannerList.get( Integer.parseInt( position ) ).get( "banner_id" ) );
                    mIntent.putExtra( AppConstants.from, "1" );
                    startActivity( mIntent );
                } else if (TopBannerList.get( Integer.parseInt( position ) ).get( "banner_on" ).equals( "2" )) {
                    Intent mIntent = new Intent( mActivity, ProductDetailsActivity.class );
                    mIntent.putExtra( AppConstants.productId, TopBannerList.get( Integer.parseInt( position ) ).get( "banner_id" ) );
                    mIntent.putExtra( AppConstants.from, "1" );
                    startActivity( mIntent );
                }*//*
            }
        };*/
        mViewPager.setAdapter( mCardAdapter );
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);
                Log.v("postiopn123", String.valueOf(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mViewPager.setPageTransformer( false, adapter );
        mViewPager.setCurrentItem( currentPage, true );
        mViewPager.setOffscreenPageLimit( 3 );
        mViewPager.setClipToPadding( false );
        mViewPager.setPadding( 15, 5, 15, 5 );
        mViewPager.setPageMargin(15);
       // int margin = getResources().getDimensionPixelSize( R.dimen._10sdp );
       // mViewPager.setPageMargin( margin );
        // mViewPager.setElevation(10f);

        mViewPager.setPageTransformer( false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                int pageWidth = mViewPager.getMeasuredWidth() -
                        mViewPager.getPaddingLeft() - mViewPager.getPaddingRight();
                int pageHeight = mViewPager.getHeight();
                int paddingLeft = mViewPager.getPaddingLeft();
                float transformPos = (float) (page.getLeft() -
                        (mViewPager.getScrollX() + paddingLeft)) / pageWidth;
                int max = pageHeight / 10;
                if (transformPos < -1) {
                    page.setScaleY( 0.8f );
                } else if (transformPos <= 1) {

                    page.setScaleY( 1f );
                } else {
                    page.setScaleY( 0.8f );
                }
            }
        } );
    }

    private void addFavApi(String productId) {
        AppUtils.showRequestDialog( mActivity );
        Log.v( "addFavApi", AppUrls.wishList );
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put( "user_master_id", AppSettings.getString( AppSettings.userId ) );
            json_data.put( "product_master_id", productId );

            json.put( AppConstants.result, json_data );

            Log.v( "addFavApi", json.toString() );

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post( AppUrls.wishList )
                .addJSONObjectBody( json )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseFavdata( response );
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage( tvMain, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            AppUtils.showErrorMessage( tvMain, String.valueOf( error.getErrorDetail() ), mActivity );
                        }
                    }
                } );
    }

    private void parseFavdata(JSONObject response) {
        Log.d( "response ", response.toString() );

        DatabaseController.removeTable( TableFavourite.favourite );

        try {
            JSONObject jsonObject = response.getJSONObject( AppConstants.responseTag );
            if (jsonObject.getString( "res_code" ).equals( "1" )) {
                if (jsonObject.getString( "status" ).equals( "1" )) {
                    AppUtils.showErrorMessage( tvMain, "Product Added to Favourites", mActivity );
                } else if (jsonObject.getString( "status" ).equals( "2" )) {
                    AppUtils.showErrorMessage( tvMain, "Product Removed from Favourites", mActivity );
                }
                JSONArray wishArray = jsonObject.getJSONArray( "wish_list_product" );

                for (int i = 0; i < wishArray.length(); i++) {
                    JSONObject productobject = wishArray.getJSONObject( i );
                    ContentValues mContentValues = new ContentValues();
                    mContentValues.put( TableFavourite.favouriteColumn.productId.toString(), productobject.getString( "product_id" ) );
                    DatabaseController.insertData( mContentValues, TableFavourite.favourite );
                }
            } else {
                AppUtils.showErrorMessage( tvMain, "Product Removed from Favourites", mActivity );
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage( tvMain, String.valueOf( e ), mActivity );
        }
        AppUtils.hideDialog();
        setAdapter();
    }

    public void setAdapter() {
        llOffers.removeAllViews();

        for (int i = 0; i < OfferList.size(); i++) {

            LayoutInflater inflater2 = null;
            //inflater2 = (LayoutInflater) getApplicationContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            View mLinearView = inflater2.inflate( R.layout.inflate_offer, null );
            LinearLayout llSponsor = mLinearView.findViewById( R.id.llSponsor );
            TextView tvOfferName = mLinearView.findViewById( R.id.tvOfferName );
            final TextView tvViewAll = mLinearView.findViewById( R.id.textView5 );
            RecyclerView productRecyclerView = mLinearView.findViewById( R.id.productRecyclerView );
            ImageView ivSponsorPic = mLinearView.findViewById( R.id.imageView2 );
            productRecyclerView.setLayoutManager( new GridLayoutManager( mActivity, 2 ) );
            if (FeaturedBannerList.size() > i) {
                llSponsor.setVisibility( View.VISIBLE );
                try {
                    if (!(((HashMap) FeaturedBannerList.get( i )).get( "banner_image" ) == null || ((String) ((HashMap) FeaturedBannerList.get( i )).get( "banner_image" )).isEmpty())) {
                        Picasso.with( mActivity ).load( (String) ((HashMap) FeaturedBannerList.get( i )).get( "banner_image" ) ).into( ivSponsorPic );
                    }
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
            } else {
                llSponsor.setVisibility( View.GONE );
            }
            tvViewAll.setTag( ((HashMap) OfferList.get( i )).get( "sub_category_id" ) );

            tvOfferName.setText( (CharSequence) ((HashMap) OfferList.get( i )).get( "offer_name" ) );

            if (OfferList.size() > 3) {
                tvViewAll.setVisibility( View.VISIBLE );
            } else {
                tvViewAll.setVisibility( View.GONE );
            }

            tvViewAll.setOnClickListener( new View.OnClickListener() {
                public void onClick(View v) {
                    String id = String.valueOf( tvViewAll.getTag() );
                    Intent mIntent = new Intent( mActivity, ProductListActivity.class );
                    mIntent.putExtra( AppConstants.subCategoryId, id );
                    mIntent.putExtra( AppConstants.from, "3" );
                    startActivity( mIntent );
                }
            } );

               /* offerAdapter = new OfferAdapter((ArrayList) ProducttList.get(i));
                layoutManager = new GridLayoutManager(mActivity,2);
                productRecyclerView.setLayoutManager(layoutManager);
                productRecyclerView.setAdapter(offerAdapter);
                productRecyclerView.setNestedScrollingEnabled(true);
                llOffers.addView(mLinearView);*/
        }
    }

    @Override
    public void onClick(View view) {
        Intent mIntent;
        switch (view.getId()) {
            case R.id.llCat1:
           /*    mIntent = new Intent(mActivity, SubCategoryActivity.class);
              //  mIntent.putExtra(AppConstants.subCategoryId, (String) ((HashMap) CategoryList.get(0)).get("sub_category_id"));
              //  mIntent.putExtra(AppConstants.from, "4");
                startActivity(mIntent);*/
                mIntent = new Intent( mActivity, SubCategoryActivity.class );
                SubCategoryActivity.categoryId = llCat1.getTag().toString();
                startActivity( mIntent );
                return;
            case R.id.llCat2:
              /*  mIntent = new Intent(mActivity, SubCategoryActivity.class);
               // mIntent.putExtra(AppConstants.subCategoryId, (String) ((HashMap) CategoryList.get(1)).get("sub_category_id"));
               // mIntent.putExtra(AppConstants.from, "4");
                startActivity(mIntent);*/
                mIntent = new Intent( mActivity, SubCategoryActivity.class );
                SubCategoryActivity.categoryId = llCat2.getTag().toString();
                startActivity( mIntent );
                break;
            case R.id.llCat3:
                mIntent = new Intent( mActivity, SubCategoryActivity.class );
                SubCategoryActivity.categoryId = llCat3.getTag().toString();
                startActivity( mIntent );
                break;
            case R.id.llCat4:
                mIntent = new Intent( mActivity, SubCategoryActivity.class );
                SubCategoryActivity.categoryId = llCat4.getTag().toString();
                startActivity( mIntent );
                break;
            case R.id.llMore:
                startActivity( new Intent( mActivity, CategoryActivity.class ) );
                break;
        }
    }

   /* public void UpdateSize(final String units, View v, final String cartId, final TextView spVal, TextView price, TextView finalprice) {
        Context wrapper = new ContextThemeWrapper( mActivity, R.style.PopupMenuOverlapAnchor );
        PopupMenu popup = new PopupMenu( wrapper, v );

        HashMap<String, String> hashMap = new HashMap<>();
        popup.getMenu().clear();
        try {

            for (int k = 0; k < datalist.size(); k++) {
                if (cartId.equalsIgnoreCase( datalist.get( k ).get( "parentId" ) )) {
                    popup.getMenu().add( datalist.get( k ).get( "sub_weight" ) + datalist.get( k ).get( "unit_name" ) );
                    hashMap.put( "aprice", datalist.get( k ).get( "actual_p" ) );
                    hashMap.put( "price", datalist.get( k ).get( "price" ) );
                    hashMapArrayList.add( hashMap );

                    //price.setText( "RS" + " " + datalist.get( k ).get( "actual_p" ) );
                    // finalprice.setText( "RS" + " " + datalist.get( k ).get( "price" ) );
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate( R.menu.menu_main, popup.getMenu() );

        popup.show();


        popup.setOnMenuItemClickListener( new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                Log.v( "MenuSelected", item.getTitle().toString() );
                Log.v( "MenuSelected", item.getTitle().toString() );

                spVal.setText( item.getTitle().toString() );

                return true;

            }
        } );
    }
*/

    public void UpdateSize(final String units,final String quantity, View v, final String cartId, final TextView spVal, final TextView price, final TextView finalprice, final TextView tvDiscount, final LinearLayout llCartCountBtn, final LinearLayout llCartBtn) {
        Context wrapper = new ContextThemeWrapper( mActivity, R.style.PopupMenuOverlapAnchor );
        PopupMenu popup = new PopupMenu( wrapper, v );
        HashMap<String, String> hashMap = new HashMap<>();
        popup.getMenu().clear();
        try {
            for (int k = 0; k < datalist.size(); k++) {
                if (cartId.equalsIgnoreCase( datalist.get( k ).get( "parentId" ) )) {
                    popup.getMenu().add(k,k,k, datalist.get( k ).get( "sub_weight" ) + datalist.get( k ).get( "unit_name" ) );
                    hashMap.put( "sub_id", datalist.get( k ).get( "sub_id" ) );
                    hashMap.put( "aprice", datalist.get( k ).get( "actual_p" ) );
                    hashMap.put( "price", datalist.get( k ).get( "price" ) );
                    hashMap.put( "per_amount", datalist.get( k ).get( "per_amount" ) );
                    hashMap.put( "dis_type", datalist.get( k ).get( "dis_type" ) );
                    hashMap.put( "cart_quantity", datalist.get( k ).get( "cart_quantity" ) );
                    hashMapArrayList.add( hashMap );
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate( R.menu.menu_main, popup.getMenu() );
        popup.show();
        llCartBtn.setVisibility(View.VISIBLE);
        llCartCountBtn.setVisibility(View.GONE);

        popup.setOnMenuItemClickListener( new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final MenuItem item) {

                int id = item.getItemId();
                Log.v( "MenuitemIdSelected", String.valueOf(id));
                llCartBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        llCartBtn.setVisibility(View.GONE);
                        llCartCountBtn.setVisibility(View.VISIBLE);
                        AddToCartApi(datalist.get(item.getOrder()).get( "sub_id" ));
                    }
                });

                finalprice.setText("RS "+datalist.get(item.getOrder()).get("actual_p"));
                /*spVal.setText( item.getTitle().toString() );*/
                spVal.setText(item.getTitle().toString());

                if(datalist.get(item.getOrder()).get("per_amount").equals("0.00")){
                    tvDiscount.setVisibility(View.INVISIBLE);
                    price.setText("RS "+datalist.get(item.getOrder()).get("price"));
                }
                else{
                    tvDiscount.setVisibility(View.VISIBLE);
                    tvDiscount.setText("RS "+ datalist.get(item.getOrder()).get("per_amount") +" off");
                }
                if(datalist.get(item.getOrder()).get("dis_type").equals("123")){
                    price.setVisibility(View.GONE);

                }
                else{
                    price.setVisibility(View.VISIBLE);
                    price.setText("RS "+datalist.get(item.getOrder()).get("price"));
                }
                //ToDo..
                Subid= datalist.get(item.getOrder()).get("sub_id").toString();
                Log.v("subid",Subid);
                return true;
            }
        } );
    }

    private void AddToCartApi(String productId) {
        AppUtils.showRequestDialog( mActivity );

        Log.v( "AddToCartApi", AppUrls.addToCart );

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put( "product_id", productId );
            json_data.put( "user_master_id", AppSettings.getString( AppSettings.userId ) );
            json.put( AppConstants.result, json_data );
            Log.v( "AddToCartApi", json.toString() );
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post( AppUrls.addToCart )
                .addJSONObjectBody( json )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        parseCartdata( response );
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            //  AppUtils.showErrorMessage(tvDeatils, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            //  AppUtils.showErrorMessage(tvDeatils, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                } );
    }

    private void parseCartdata(JSONObject response) {

        Log.d( "response ", response.toString() );

        try {
            JSONObject jsonObject = response.getJSONObject( AppConstants.responseTag );
            if (jsonObject.getString( "res_code" ).equals( "1" )) {
                getCartListApi();
                AppUtils.showErrorMessage( tvCat1, String.valueOf( jsonObject.getString( "res_msg" ) ), this.mActivity );
                //  startActivity(new Intent(getBaseContext(), CartListActivity.class));
            } else {
                // startActivity(new Intent(mActivity, CartListActivity.class));
                AppUtils.showErrorMessage( tvCat1, String.valueOf( jsonObject.getString( "res_msg" ) ), this.mActivity );
            }
        } catch (Exception e) {
            // AppUtils.showErrorMessage(tvDeatils, String.valueOf(e), this.mActivity);
        }
        AppUtils.hideDialog();
    }
    private void UpdateCartApi(String quantity, String ProductId) {

        Log.v( "AddToCartApi", AppUrls.updateCart );

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put( "product_id", ProductId );
            json_data.put( "user_master_id", AppSettings.getString( AppSettings.userId ) );
            json_data.put( "quantity", quantity );
            json.put( AppConstants.result, json_data );
            Log.v( "AddToCartApi", json.toString() );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post( AppUrls.updateCart )
                .addJSONObjectBody( json )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedata( response );
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage( recyclerView, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            AppUtils.showErrorMessage( recyclerView, String.valueOf( error.getErrorDetail() ), mActivity );
                        }
                    }
                } );
    }

    private void parsedata(JSONObject response) {
        Log.d( "response", response.toString() );
        try {
            JSONObject jsonObject = response.getJSONObject( AppConstants.responseTag );
            if (jsonObject.getString( "res_code" ).equals( "1" )) {
                AppUtils.showErrorMessage( recyclerView, String.valueOf( jsonObject.getString( "res_msg") ), mActivity );
                // cartList.clear();
                if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                    getCartListApi();
                } else {
                    AppUtils.showErrorMessage( recyclerView, getString( R.string.errorInternet ), mActivity );
                }
            } else {
                AppUtils.hideDialog();

                AppUtils.showErrorMessage( recyclerView, String.valueOf( jsonObject.getString( "res_msg" ) ), mActivity );


            }
        } catch (Exception e) {
            AppUtils.hideDialog();
            AppUtils.showErrorMessage( recyclerView, String.valueOf( e ), mActivity );
        }
    }

    private void DeleteCartApi(String cartId) {
        AppUtils.showRequestDialog( mActivity );
        Log.v( "AddToCartApi", AppUrls.deleteFromCart );
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put( "cart_id",cartId);
            json_data.put( "user_master_id", AppSettings.getString( AppSettings.userId ) );
            json.put( AppConstants.result, json_data );
            Log.v( "AddToCartApi", json.toString() );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post( AppUrls.deleteFromCart )
                .addJSONObjectBody( json )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedeletedata( response );
                    }
                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage( recyclerView, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            AppUtils.showErrorMessage( recyclerView, String.valueOf( error.getErrorDetail() ), mActivity );
                        }
                    }
                } );
    }

    private void parsedeletedata(JSONObject response) {
        Log.d( "response ", response.toString() );
        try {
            JSONObject jsonObject = response.getJSONObject( AppConstants.responseTag );
            if (jsonObject.getString( "res_code" ).equals( "1" )) {
                AppUtils.showErrorMessage( recyclerView, String.valueOf( jsonObject.getString( "res_msg" ) ), mActivity );
                AppUtils.hideDialog();
                //cartList.clear();
                if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                    getCartListApi();
                } else {
                    AppUtils.showErrorMessage( recyclerView, getString( R.string.errorInternet ), mActivity );
                }
            } else {
                AppUtils.hideDialog();
                AppUtils.showErrorMessage( recyclerView, String.valueOf( jsonObject.getString( "res_msg" ) ), mActivity );
            }
        } catch (Exception e) {
            AppUtils.hideDialog();
            AppUtils.showErrorMessage( recyclerView, String.valueOf( e ), mActivity );
        }

    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            getActivity().runOnUiThread( new Runnable() {

                @Override
                public void run() {

                    if (currentPage == NUM_PAGES) {

                        currentPage = 1;
                    }
                    mViewPager.setCurrentItem( currentPage++, true );
                    // mViewPager.setCurrentItem((mViewPager.getCurrentItem() + 1) % TopBannerList.size());

                    // mViewPager.post(new MyTimerTask());
                }
            } );
        }

    }

    public class OfferAdapter extends RecyclerView.Adapter<OfferHolder> {
        public ArrayList<String> arrlistJobName = new ArrayList<String>();
        ArrayList<HashMap<String, String>> data = new ArrayList();
        ArrayList<HashMap<String, String>> data2 = new ArrayList();
        ArrayList<HashMap<String, String>> sliderMapArray = new ArrayList<HashMap<String, String>>();
        List<product_master> gff;
        int count ;

        public OfferAdapter(ArrayList<HashMap<String, String>> favList, ArrayList<HashMap<String, String>> subProductListArrayList) {

            data = favList;
            data2 = subProductListArrayList;
        }
        @Override
        public OfferHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from( parent.getContext() ).inflate( R.layout.inflate_product, parent, false );
            return new OfferHolder( itemView );
        }

        public void onBindViewHolder(final OfferHolder holder, final int position) {
            if(!data2.get( position ).get( "cart_quantity" ).equalsIgnoreCase("")){
                count= Integer.parseInt(data2.get( position ).get( "cart_quantity" ));
            }else{
                count=1;
            }
            holder.tvName.setText( (CharSequence) ((HashMap) data.get( position )).get( "product_name" ) );
            if (((HashMap) data.get( position )).get( "product_discount_type" ).equals( "1" )) {
                holder.tvPrice.setVisibility( View.VISIBLE );
                holder.tvDiscount.setText( "RS" + " " + ((HashMap) data.get( position )).get( "product_discount_amount" ) + "  /-off" );
            } else if (((HashMap) data.get( position )).get( "product_discount_type" ).equals( "2" )) {
                holder.tvPrice.setVisibility( View.VISIBLE );
                holder.tvDiscount.setText( "RS" + " " + ((HashMap) data.get( position )).get( "product_discount_amount" ) + "% off" );
            }  else if (data.get( position ).get( "product_discount_type" ).equals( "123" )) {
                // holder.tvDiscount.setText("");
                holder.tvDiscount.setVisibility( View.INVISIBLE );
                holder.tvPrice.setVisibility( View.GONE );
            }
            if (((String) ((HashMap) data.get( position )).get( "final_price" )).equalsIgnoreCase( (String) ((HashMap) data.get( position )).get( "price" ) )) {
                holder.tvFPrice.setText( "RS" + " " + ((HashMap) data.get( position )).get( "final_price" ) );
                holder.tvDiscount.setText( "" );
                holder.tvDiscount.setVisibility( View.INVISIBLE );
                holder.tvPrice.setText( "" );
            } else {
                holder.tvFPrice.setText( "RS" + " " + ((HashMap) data.get( position )).get( "final_price" ) );
                holder.tvPrice.setText( "RS" + " " + ((HashMap) data.get( position )).get( "price" ) );
                //  holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.ANTI_ALIAS_FLAG);
                holder.tvPrice.setPaintFlags( holder.tvPrice.getPaintFlags() | 16 );
            }
            try {
                if (((HashMap) data.get( position )).get( "product_image" ) != null && !((String) ((HashMap) data.get( position )).get( "product_image" )).isEmpty()) {
                    Picasso.with( mActivity ).load( (String) ((HashMap) data.get( position )).get( "product_image" ) ).into( holder.ivPic );
                }
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }
            if (DatabaseController.checkRecordExist( TableFavourite.favourite, String.valueOf( TableFavourite.favouriteColumn.productId ), data.get( position ).get( "id" ) )) {
                holder.icWishlist.setImageResource( R.drawable.ic_heart_red );
            } else {
                holder.icWishlist.setImageResource( R.drawable.ic_heart_grey );
            }
            holder.icWishlist.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!AppSettings.getString( AppSettings.userId ).isEmpty()) {
                        if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                            addFavApi( data.get( position ).get( "id" ) );
                        } else {
                            AppUtils.showErrorMessage( tvMain, getString( R.string.errorInternet ), mActivity );
                        }
                    } else {
                        AppUtils.showErrorMessage( tvMain, "Kindly login first", mActivity );
                    }
                }
            } );

            holder.itemView.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent mIntent = new Intent( mActivity, ProductDetailsActivity.class );
                    mIntent.putExtra( AppConstants.productId, data.get( position ).get( "id" ) );
                    mIntent.putExtra( AppConstants.from, "3" );
                    mIntent.putExtra( AppConstants.CartQuantityValue, "6" );
                    startActivity( mIntent );
                }
            } );



            holder.llCartCountBtn.setVisibility( View.GONE );
            holder.llCartBtn.setVisibility( View.VISIBLE );
            holder.llCartBtn.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (AppSettings.getString( AppSettings.userId ).isEmpty()) {
                        startActivity( new Intent( mActivity, LoginActivity.class ) );
                    } else {
                        holder.llCartCountBtn.setVisibility( View.VISIBLE );
                        holder.llCartBtn.setVisibility( View.GONE );
                        AddToCartApi( data.get( position ).get( "id" ) );
                    }
                }
            } );


            holder.tvSpinner.setText( data.get( position ).get( "SpinerValue" ) );
            Log.v( "ndn", data.get( position ).get( "SpinerValue" ) );

            holder.tvSpinner.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UpdateSize( data.get( position ).get( "sub_weight" ),data.get( position ).get( "cart_quantity" ), view, data.get( position ).get( "id" ), holder.tvSpinner, holder.tvPrice, holder.tvFPrice ,holder.tvDiscount, holder.llCartCountBtn, holder.llCartBtn);
                    if (AppSettings.getString( AppSettings.userId ).isEmpty()) {
                        //  startActivity( new Intent( mActivity, LoginActivity.class ) );
                    } else {
                        count=  (Integer.parseInt( holder.tvQuantityCount.getText().toString() )) ;
                        holder.tvQuantityCount.setText(String.valueOf(count));
                        Log.v("CountValue",String.valueOf(count));
                       /* holder.llCartCountBtn.setVisibility( View.GONE );
                        holder. llCartBtn.   setVisibility( View.VISIBLE );*/
                    }    }
            } );
            Log.v( "datasub",data2.toString() );
            Log.v( "data2",data2.toString() );
            holder.tvQuantityCount.setText( String.valueOf(count) );

          /*  if (!data2.get( position ).get( "cart_quantity" ).equalsIgnoreCase( "" )) {
                holder.llCartCountBtn.setVisibility( View.VISIBLE );
                holder.llCartBtn.setVisibility( View.GONE );

                AppSettings.putString(AppSettings.Quantity,data2.get(position).get("quantity"));
            } else {
                holder.llCartCountBtn.setVisibility( View.GONE );
                holder.llCartBtn.setVisibility( View.VISIBLE );
            }*/
            AppSettings.putString(AppSettings.subiid,data2.get( position ).get( "sub_id" ));
            holder.imageView3.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    count = (Integer.parseInt( holder.tvQuantityCount.getText().toString() )) - 1;
                    if (count <= 1) {
                        Vibrator vibe = (Vibrator) mActivity.getSystemService( Context.VIBRATOR_SERVICE );
                        vibe.vibrate( 100 );
                        holder.llCartCountBtn.setVisibility( View.GONE );
                        holder.llCartBtn.setVisibility( View.VISIBLE );
                        String dd="714";
                        DeleteCartApi(dd);
                    } else if (count > 1) {
                        // if (count > 1) {
                        // count = count - 1;
                        // ((DashBoardFragment) getActivity()).tvCount.setText(Integer.toString(count));
                        Vibrator vibe = (Vibrator) mActivity.getSystemService( Context.VIBRATOR_SERVICE );
                        vibe.vibrate( 100 );
                        if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                            UpdateCartApi( String.valueOf( count ), data2.get( position ).get( "sub_id" ) );
                            holder.tvQuantityCount.setText( (count) + "" );
                        } else {
                            AppUtils.showErrorMessage( recyclerView, getString( R.string.errorInternet ), mActivity );
                        }
                    } /*else if (count == 1) {
                            DeleteCartApi( count );
                            AppUtils.showErrorMessage( recyclerView, getString( R.string.minusQuantityError ), mActivity );
                        }*/

                }
                  /*  int quant = Integer.parseInt( data2.get( position ).get( "quantity" ) );
                    if (quant > 1) {
                        quant = quant - 1;
                        Vibrator vibe = (Vibrator) mActivity.getSystemService( Context.VIBRATOR_SERVICE );
                        vibe.vibrate( 100 );
                        if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                            UpdateCartApi( String.valueOf( quant ), data2.get( position ).get( "sub_id" ), position );
                        } else {
                            AppUtils.showErrorMessage( recyclerView, getString( R.string.errorInternet ), mActivity );
                        }
                    } else {
                        DeleteCartApi( data2.get( position ).get( "sub_id" ) );
                        AppUtils.showErrorMessage( recyclerView, getString( R.string.minusQuantityError ), mActivity );
                    }*/

                   /* int quant = (Integer.parseInt( holder.tvQuantityCount.getText().toString() ));
                    if (quant > 1) {
                        quant = quant - 1;
                        Vibrator vibe = (Vibrator) mActivity.getSystemService( Context.VIBRATOR_SERVICE );
                        vibe.vibrate( 100 );

                        if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                            UpdateCartApi( String.valueOf( quant ), data.get( position ).get( "product_id" ) );
                        } else {
                            AppUtils.showErrorMessage( recyclerView, getString( R.string.errorInternet ), mActivity );
                        }
                    } else {
                      //  DeleteCartApi( data.get( position ).get( "cart_id" ) );
                        DeleteCartApi(quant );
                        AppUtils.showErrorMessage( recyclerView, getString( R.string.minusQuantityError ), mActivity );
                    }

*/

            } );
            holder.imageView2.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    count = (Integer.parseInt( holder.tvQuantityCount.getText().toString() )) + 1;
                    if (count < 10) {
                        // ((DashBoardFragment) getActivity()).tvCount.setText(Integer.toString(count));
                        AppSettings.putString(AppSettings.cartValue,tvCount.getText().toString());
                        //count = count + 1;
                        Vibrator vibe = (Vibrator) mActivity.getSystemService( Context.VIBRATOR_SERVICE );
                        vibe.vibrate( 100 );
                        if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                            UpdateCartApi( String.valueOf( count ), data2.get( position ).get( "sub_id" ) );
                            holder.tvQuantityCount.setText( (count) + "" );
                        } else {
                            AppUtils.showErrorMessage( recyclerView, getString( R.string.errorInternet ), mActivity );
                        }
                    } else {

                        AppUtils.showErrorMessage( recyclerView, getString( R.string.addQuantityError ), mActivity );
                    }
                }
            } );
        }

        public int getItemCount() {
            return data.size();
        }
    }

    private class OfferHolder extends RecyclerView.ViewHolder {
        ImageView ivPic, icWishlist, imageView2, imageView3;
        LinearLayout linearLayout, llCartBtn, llCartCountBtn;
        TextView tvDiscount;
        TextView tvFPrice;
        TextView tvName;
        TextView tvPrice;
        TextView tvSpinner, tvQuantityCount;
        Spinner spinners;

        RelativeLayout btnSpinner;

        public OfferHolder(View itemView) {
            super( itemView );
            linearLayout = itemView.findViewById( R.id.layout_item );
            tvQuantityCount = itemView.findViewById( R.id.tvQuantityCount );
            spinners = itemView.findViewById( R.id.spinners );
            ivPic = itemView.findViewById( R.id.ivProduct );
            icWishlist = itemView.findViewById( R.id.ic_wishlist );
            llCartCountBtn = itemView.findViewById( R.id.llCartCountBtn );
            tvName = itemView.findViewById( R.id.tvProductName );
            tvSpinner = itemView.findViewById( R.id.tvSpinner );

            llCartBtn = itemView.findViewById( R.id.llCartBtn );
            tvFPrice = itemView.findViewById( R.id.tvFPrice );
            tvPrice = itemView.findViewById( R.id.tvPrice );
            tvDiscount = itemView.findViewById( R.id.tvDiscount );
            btnSpinner = itemView.findViewById( R.id.btnSpinner );

            imageView2 = itemView.findViewById( R.id.imageView2 );
            imageView3 = itemView.findViewById( R.id.imageView3 );

        }
    }

    public class Adapter extends RecyclerView.Adapter<Adapter.Holder> {
        List<product_master> data = new ArrayList();

        public Adapter(List<product_master> featuredProductList) {
            data = featuredProductList;
        }

        @NonNull
        @Override
        public Adapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new Adapter.Holder( LayoutInflater.from( parent.getContext() ).inflate( R.layout.inflate_product, parent, false ) );
        }

        @Override
        public void onBindViewHolder(@NonNull final Adapter.Holder holder, final int position) {
            holder.tvName.setText( data.get( position ).getProduct_name() );

            /* if (((HashMap) data.get( position )).get( "product_discount_type" ).equals( "1" )) {
                holder.tvDiscount.setText( "RS" + " " + ((HashMap) data.get( position )).get( "product_discount_amount" ) + " /-off" );
            } else if (((HashMap) data.get( position )).get( "product_discount_type" ).equals( "2" )) {
                holder.tvDiscount.setText( "RS" + " " + ((HashMap) data.get( position )).get( "product_discount_amount" ) + "% off" );
            } else {
                // holder.tvDiscount.setText("");
                holder.tvDiscount.setVisibility( View.INVISIBLE );
                holder.tvPrice.setVisibility( View.INVISIBLE );
            }*/
            if (data.get( position ).getProduct_discount_type().equals( "1" )) {
                holder.tvPrice.setPaintFlags( holder.tvPrice.getPaintFlags() | 16 );
                holder.tvPrice.setVisibility( View.VISIBLE );
                holder.tvDiscount.setText( "RS" + " " + data.get( position ).getProduct_discount_amount() + "/-off" );
            } else if (data.get( position ).getProduct_discount_type().equals( "2" )) {
                holder.tvPrice.setPaintFlags( holder.tvPrice.getPaintFlags() | 16 );
                holder.tvPrice.setVisibility( View.VISIBLE );
                holder.tvDiscount.setText( "RS" + " " + data.get( position ).getProduct_discount_amount() + "% off" );
            }else if (data.get( position ).getProduct_discount_type().equals( "123" ))  {
                // holder.tvDiscount.setText("");
                holder.tvDiscount.setVisibility( View.INVISIBLE );
                holder.tvPrice.setVisibility( View.GONE );
            }
            if (data.get( position ).getFinal_price().equals( data.get( position ).getPrice() )) {
                holder.tvFPrice.setText( "RS" + " " + data.get( position ).getFinal_price() );
                holder.tvDiscount.setText( "" );
                holder.tvDiscount.setVisibility( View.INVISIBLE );
                holder.tvPrice.setText( "" );
            } else {
                holder.tvFPrice.setText( "RS" + " " + data.get( position ).getFinal_price() );
                holder.tvPrice.setText( "RS" + " " + data.get( position ).getPrice() );

                //  holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.ANTI_ALIAS_FLAG);
                holder.tvPrice.setPaintFlags( holder.tvPrice.getPaintFlags() | 16 );
            }
            try {
                if (data.get( position ).getImage() != null && !data.get( position ).getImage().isEmpty()) {
                    Picasso.with( mActivity ).load( data.get( position ).getImage() ).into( holder.ivPic );
                }
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }
            if (DatabaseController.checkRecordExist( TableFavourite.favourite, String.valueOf( TableFavourite.favouriteColumn.productId ), data.get( position ).getId() )) {
                holder.icWishlist.setImageResource( R.drawable.ic_heart_red );
            } else {
                holder.icWishlist.setImageResource( R.drawable.ic_heart_grey );
            }

           /* data2.clear();
            for(int k=0;k<subProductListArrayList.size();k++){
                if(data.get(position).get("id").equalsIgnoreCase(subProductListArrayList.get(k).get("parentId"))){
                    HashMap<String, String> sliderMap = new HashMap<>();
                    sliderMap.put("sub_id", subProductListArrayList.get(k).get("sub_id"));
                    sliderMap.put("dis_type", subProductListArrayList.get(k).get("dis_type"));
                    sliderMap.put("per_amount", subProductListArrayList.get(k).get("per_amount"));
                    sliderMap.put("sub_weight", subProductListArrayList.get(k).get("sub_weight"));
                    sliderMap.put("unit_name", subProductListArrayList.get(k).get("unit_name"));
                    sliderMap.put("price", subProductListArrayList.get(k).get("price"));
                    data2.add(sliderMap);


                }
            }*/


         /*   holder.spinners.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    String   strSubstationId = arrlistJobName.get( i );

                    String  subStationPosition = String.valueOf( adapterView.getSelectedItem() );
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
*/
            holder.icWishlist.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!AppSettings.getString( AppSettings.userId ).isEmpty()) {
                        if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                            addFavApi( data.get( position ).getId() );
                        } else {
                            AppUtils.showErrorMessage( tvMain, getString( R.string.errorInternet ), mActivity );
                        }
                    } else {
                        AppUtils.showErrorMessage( tvMain, "Kindly login first", mActivity );
                    }

                }
            } );

            holder.linearLayout.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   /* Intent mIntent = new Intent(mActivity, ProductDetailsActivity.class);
                    mIntent.putExtra(AppConstants.productId, data.get(position).getId());
                    mIntent.putExtra(AppConstants.from, "3");
                    mActivity.startActivity(mIntent);*/
                }
            } );

            holder.llCartCountBtn.setVisibility( View.GONE );
            holder.llCartBtn.setVisibility( View.VISIBLE );
            holder.llCartBtn.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (AppSettings.getString( AppSettings.userId ).isEmpty()) {
                        startActivity( new Intent( mActivity, LoginActivity.class ) );
                    } else {
                        holder.llCartCountBtn.setVisibility( View.VISIBLE );
                        holder.llCartBtn.setVisibility( View.GONE );
                        AddToCartApi( data.get( position ).getId() );
                    }

                }
            } );
            // holder.tvSpinner.setText(  );

            holder.tvSpinner.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //    UpdateSize( data.get( position ).get( "sub_weight" ), view, data.get( position ).get( "id" ), holder.tvSpinner );
                }
            } );

            // Log.v( "datasub", data2.toString() );


            //  Log.v( "data2", data2.toString() );


            holder.imageView2.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            } );

            holder.imageView3.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            } );
            if (data.get( position ).getSub_product()!= null && data.get( position ).getSub_product().size() > 0) {
                final List<sub_product> priceDataList = data.get( position ).getSub_product();
                final ArrayList<String> aa = new ArrayList<>();
                //   final String[] arrQuantity = new String[priceDataList.size()];
                String[] arrSubId = new String[priceDataList.size()];
                HashMap<String, String> hasmapForQuantity = new HashMap<>();
                for (int j = 0; j < priceDataList.size(); j++) {
                    //arrQuantity[j] = priceDataList.get( j ).getSub_weight() + " " + priceDataList.get( j ).getUnit_name();
                    // arrSubId[j] = priceDataList.get( j ).getSub_id();
                    // hasmapForQuantity.put( priceDataList.get( j ).getUnit_name(), priceDataList.get( j ).getSub_id() );
                    aa.add( priceDataList.get( j ).getSub_weight() + " " + priceDataList.get( j ).getUnit_name() );
                }

                // ArrayAdapter<String> adapter = new ArrayAdapter<String>( mActivity, R.layout.spinner_layout, R.id.tvName, arrQuantity );
                //  holder.spinners.setAdapter( adapter);

                getActivity().runOnUiThread(() -> holder.spinners.setAdapter( new ArrayAdapter<String>( mActivity, android.R.layout.simple_spinner_item, aa ) ));

                holder.spinners.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        for (int j = 0; j < priceDataList.size(); j++) {
                            if (holder.spinners.getSelectedItem().toString().trim().equalsIgnoreCase( priceDataList.get( j ).getSub_weight() + " " + priceDataList.get( j ).getUnit_name() )) {
                                Toast.makeText( mActivity, priceDataList.get( j ).getSub_weight() + " " + priceDataList.get( j ).getUnit_name(), Toast.LENGTH_SHORT ).show();
                            }
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                } );
            }

        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class Holder extends RecyclerView.ViewHolder {
            ImageView ivPic, icWishlist, imageView2, imageView3;
            LinearLayout linearLayout, llCartBtn, llCartCountBtn;
            TextView tvDiscount;
            TextView tvFPrice;
            TextView tvName;
            TextView tvPrice;
            TextView tvSpinner;
            Spinner spinners;

            RelativeLayout btnSpinner;

            public Holder(@NonNull View itemView) {
                super( itemView );
                linearLayout = itemView.findViewById( R.id.layout_item );
                spinners = itemView.findViewById( R.id.spinners );
                ivPic = itemView.findViewById( R.id.ivProduct );
                icWishlist = itemView.findViewById( R.id.ic_wishlist );
                llCartCountBtn = itemView.findViewById( R.id.llCartCountBtn );
                tvName = itemView.findViewById( R.id.tvProductName );
                tvSpinner = itemView.findViewById( R.id.tvSpinner );

                llCartBtn = itemView.findViewById( R.id.llCartBtn );
                tvFPrice = itemView.findViewById( R.id.tvFPrice );
                tvPrice = itemView.findViewById( R.id.tvPrice );
                tvDiscount = itemView.findViewById( R.id.tvDiscount );
                btnSpinner = itemView.findViewById( R.id.btnSpinner );

                imageView2 = itemView.findViewById( R.id.imageView2 );
                imageView3 = itemView.findViewById( R.id.imageView3 );
            }
        }
    }

    public void loginCheck(String position) {

        final Dialog dialog = new Dialog(mActivity,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.bannerpopup);
        Window window = dialog.getWindow();
        dialog.setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();
        ImageView ivBanner=(ImageView)dialog.findViewById(R.id.ivBanner);
        Picasso.with(dialog.getContext()).load(TopBannerList.get( Integer.parseInt( position ) ).get( "banner_image" )).into(ivBanner);
        Log.v("StringData", String.valueOf(TopBannerList));
        dialog.show();
    }
}

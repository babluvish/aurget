package com.aurget.buddha.Activity.AddtoCart;

import android.content.Intent;
import android.graphics.Paint;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.aurget.buddha.Activity.BuyNowCart;
import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.EmptyCartActivity;
import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.Activity.Sqlite.DBHelper;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AddCartListActivity extends AppCompatActivity {

    private DBHelper mydb;
    RecyclerView addtoCartRv;
    ImageView imghmb1;
    TextView titleTv, continues, amountTv, totIPTv;
    RatingBar rting;
    public static ArrayList<JSONObject> jsonObjectal = new ArrayList<>();
    ArrayList<JSONObject> qtyJobj = new ArrayList<>();

    private MyRecyclerAdapter myRecyclerAdapter = new MyRecyclerAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cart_list);

        mydb = new DBHelper(this);

        addtoCartRv = findViewById(R.id.addtoCartRv);
        addtoCartRv.setVisibility(View.VISIBLE);
        imghmb1 = findViewById(R.id.right_icon);
        titleTv = findViewById(R.id.title);
        continues = findViewById(R.id.continues);
        amountTv = findViewById(R.id.amountTv);
        totIPTv = findViewById(R.id.totIPTv);

        imghmb1.setVisibility(View.VISIBLE);
        imghmb1.setImageResource(0);
        imghmb1.setBackgroundResource(R.drawable.saveforlatter);

        titleTv.setText("Add to Cart List");

        mydb.getAllCotacts();

        LinearLayoutManager horizontalLayoutManager5 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        addtoCartRv.setLayoutManager(horizontalLayoutManager5);


        try {
            crtDetils();
        } catch (Exception e) {
            e.printStackTrace();
        }

        imghmb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mydb.numberOfRows() < 1) {
                    Commonhelper.showToastLong(AddCartListActivity.this, "Please save item first");
                    return;
                }

                Intent intent = new Intent(AddCartListActivity.this, SAveforLattrerActivity.class);
                startActivity(intent);
                finish();
            }
        });

        continues.setOnClickListener(v -> {
            Intent intent = new Intent(AddCartListActivity.this, BuyNowCart.class);
            intent.putExtra("totAmt", amountTv.getText().toString());
            intent.putExtra("totIPs", totIPTv.getText().toString());
            intent.putExtra("res", jsonObjectal.toString());
            startActivity(intent);
        });

        findViewById(R.id.bkImg).setOnClickListener(v -> finish());

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {
            /*Log.d("ssss", jsonObjectal.toString());
            addtoCartRv.setAdapter(myRecyclerAdapter);
            myRecyclerAdapter.notifyDataSetChanged();
            calcTotAmt();*/
        }
    }

    class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

        public MyRecyclerAdapter() {

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_to_crt_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            try {
                if (jsonObjectal.get(position).optString("move_to_cart").equals("1")) {
                    holder.priceTv.setText("₹" + jsonObjectal.get(position).optString("sale_price"));
                    holder.priceTv2.setText("₹" + jsonObjectal.get(position).optString("cut_amount"));
                    holder.ipTv.setText("IP: " + Commonhelper.roundOff(Double.valueOf(jsonObjectal.get(position).optString("ip"))));
                    holder.productNmeTv.setText("" + jsonObjectal.get(position).optString("title"));
                    holder.qtyEt.setText("" + jsonObjectal.get(position).optString("qtr"));

                    if (jsonObjectal.get(position).optString("product_size").equalsIgnoreCase("null") ||
                            TextUtils.isEmpty(jsonObjectal.get(position).optString("product_size")))
                    {
                        holder.sizeCv.setVisibility(View.GONE);
                        holder.sizeTv2.setVisibility(View.GONE);
                    }
                    else {
                        holder.sizeTv.setText("" + jsonObjectal.get(position).optString("product_size"));
                    }

                    if (jsonObjectal.get(position).optString("product_size").equalsIgnoreCase("null") ||
                            TextUtils.isEmpty(jsonObjectal.get(position).optString("product_size")))
                    {
                        holder.sizeCv.setVisibility(View.GONE);
                        holder.sizeTv2.setVisibility(View.GONE);
                    }
                    else {
                        holder.sizeTv.setText("" + jsonObjectal.get(position).optString("product_size"));
                    }

                    if (Integer.parseInt(jsonObjectal.get(position).optString("discount")) > 0) {

                        Commonhelper.isDiscountRate(jsonObjectal, position, holder.percentOffTv);

                    /*if (jsonObjectal.get(position).optString("discount_type").equals("percent")) {
                        holder.percentOffTv.setText("" + jsonObjectal.get(position).optString("discount") + "% Discount");
                    }
                    else
                    {
                        holder.percentOffTv.setText("₹" + jsonObjectal.get(position).optString("discount") + " Discount");
                    }*/
                        //priceTv2.setText("₹" + roudOff(camt));
                        holder.priceTv2.setText("₹" + jsonObjectal.get(position).optString("cut_amount"));
                        //priceTv.setText("₹" + amt);
                        holder.priceTv.setText("₹" + jsonObjectal.get(position).optString("sale_price"));
                    } else {
                        //priceTv.setText("₹" + roudOff(sp));
                        holder.priceTv.setText("₹" + jsonObjectal.get(position).optString("sale_price"));
                        holder.percentOffTv.setVisibility(View.GONE);
                        holder.priceTv2.setVisibility(View.GONE);
                    }

                    //rting.setRating(Float.parseFloat(jsonObjectal.get(position).optString("rating_total")));
                    //holder.rtingTv.setText(""+Float.parseFloat(jsonObjectal.get(position).optString("rating_total")));
                    Picasso.with(AddCartListActivity.this).load(InterfaceClass.imgPth2 + jsonObjectal.get(position).optString("product_id") + "_" + 1 + "_thumb.jpg").into(holder.img);

                    if (jsonObjectal.get(position).optString("product_color").equalsIgnoreCase("null") ||
                            TextUtils.isEmpty(jsonObjectal.get(position).optString("product_color")))
                    {
                        holder.imageTv.setVisibility(View.GONE);
                        holder.imgCv.setVisibility(View.GONE);
                    }
                    else {
                        Picasso.with(AddCartListActivity.this).load(jsonObjectal.get(position).optString("product_color")).into(holder.colorimg);
                    }
                    holder.crt.setVisibility(View.VISIBLE);

                } else {
                    holder.crt.setVisibility(View.GONE);
                }

            } catch (Exception e) {
            }
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView save_for_latterBtn, deleteBtn, minusTv, plusTv, amountTv, sizeTv,sizeTv2;
            TextView priceTv, priceTv2, percentOffTv, ipTv, productNmeTv, soldTv, rtingTv,imageTv;

            private ImageView img, colorimg;
            private CardView crt,sizeCv,imgCv;
            EditText qtyEt;

            public ViewHolder(final View itemView) {
                super(itemView);
                save_for_latterBtn = itemView.findViewById(R.id.save_for_latterBtn);
                deleteBtn = itemView.findViewById(R.id.deleteBtn);
                crt = itemView.findViewById(R.id.crt);
                imgCv = itemView.findViewById(R.id.imgCv);
                imageTv = itemView.findViewById(R.id.imageTv);
                qtyEt = itemView.findViewById(R.id.qtyEt);
                img = itemView.findViewById(R.id.img);
                colorimg = itemView.findViewById(R.id.colorimg);
                sizeTv = itemView.findViewById(R.id.sizeTv);
                sizeTv2 = itemView.findViewById(R.id.sizeTv2);
                sizeCv = itemView.findViewById(R.id.sizeCv);

                rtingTv = itemView.findViewById(R.id.rtingTv);
                priceTv = itemView.findViewById(R.id.priceTv);
                priceTv2 = itemView.findViewById(R.id.priceTv2);
                priceTv2.setPaintFlags(priceTv2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                percentOffTv = itemView.findViewById(R.id.percentOffTv);
                ipTv = itemView.findViewById(R.id.ipTv);
                productNmeTv = itemView.findViewById(R.id.productNmeTv);
                soldTv = itemView.findViewById(R.id.soldTv);
                rting = itemView.findViewById(R.id.rting);
                minusTv = itemView.findViewById(R.id.minusTv);
                plusTv = itemView.findViewById(R.id.plusTv);
                amountTv = itemView.findViewById(R.id.amountTv);

                deleteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        delete_cart(jsonObjectal.get(getAdapterPosition()).optString("product_id"));

                        if (mydb.isExist(jsonObjectal.get(getAdapterPosition()).optString("cart_id"))) {
                            mydb.deleteContact(Integer.valueOf(jsonObjectal.get(getAdapterPosition()).optString("cart_id")));
                        }

                        jsonObjectal.remove(getAdapterPosition());
                        notifyDataSetChanged();
                        calcTotAmt();
                    }
                });

                save_for_latterBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            jsonObjectal.get(getAdapterPosition()).put("move_to_cart", "0");

                            if (!mydb.isExist(jsonObjectal.get(getAdapterPosition()).optString("cart_id"))) {
                                mydb.insertContact(jsonObjectal.get(getAdapterPosition()).optString("cart_id"));
                            }

                            notifyDataSetChanged();
                            calcTotAmt();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                minusTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (qtyEt.getText().toString().equals("1")) {
                            return;
                        } else {
                            int qty = Integer.parseInt(qtyEt.getText().toString()) - 1;
                            try {
                                jsonObjectal.get(getAdapterPosition()).put("qtr", "" + qty);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            qtyEt.setText("" + qty);
                            calcTotAmt();
                        }
                    }
                });

                plusTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int qty = Integer.parseInt(qtyEt.getText().toString()) + 1;
                        try {
                            jsonObjectal.get(getAdapterPosition()).put("qtr", "" + qty);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        qtyEt.setText("" + qty);
                        calcTotAmt();
                    }
                });
            }
        }
    }

    private void calcTotAmt() {

        double amt = 0.0, totAmt = 0.0, totIPs = 0.0;

        int qty = 1;
        totAmtAl.clear();
        totIPAl.clear();

        for (int i = 0; i < jsonObjectal.size(); i++) {

            totAmtAl.add(0.0);
            totIPAl.add(0.0);

            try {
                if (jsonObjectal.get(i).optString("move_to_cart").equalsIgnoreCase("1")) {
                    amt = Double.parseDouble(jsonObjectal.get(i).optString("sale_price"));
                    qty = Integer.parseInt(jsonObjectal.get(i).optString("qtr"));
                    Double totIP = Double.parseDouble(jsonObjectal.get(i).optString("ip"));

                    totAmtAl.set(i, amt * qty);
                    totIPAl.set(i, totIP * qty);
                }

            } catch (Exception e) {

            }
        }

        for (int i = 0; i < totAmtAl.size(); i++) {
            totAmt = totAmt + totAmtAl.get(i);
            totIPs = totIPs + totIPAl.get(i);
        }

        amountTv.setText(" ₹" + Commonhelper.roundOff(totAmt));
        totIPTv.setText(" | IP:" + Commonhelper.roundOff(totIPs));

        if (totAmt < 1) {
            Intent intent = new Intent(AddCartListActivity.this, EmptyCartActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void crtDetils() {
        new AbstrctClss(AddCartListActivity.this, ConstntApi.get_cart_details(AddCartListActivity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    try {
                        JSONArray jsonArray = new JSONArray(s);
                        Log.d("jsonArray", jsonArray.toString());
                        jsonObjectal.clear();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            if (mydb.isExist(jsonArray.optJSONObject(i).optString("cart_id"))) {
                                jsonArray.optJSONObject(i).put("move_to_cart", "0");
                            }
                            jsonObjectal.add(jsonArray.optJSONObject(i));
                            qtyJobj.add(null);
                        }

                        addtoCartRv.setAdapter(myRecyclerAdapter);
                        calcTotAmt();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("letestFeturePi", "sssss");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    private void delete_cart(final String productID) {

        new AbstrctClss(AddCartListActivity.this, ConstntApi.delete_cart(AddCartListActivity.this, productID), "p", true) {
            @Override
            public void responce(String s) {

                try {
                    String Res = s;
                    Log.d("dsdss", Res);
                    JSONArray jsonArray = new JSONArray(Res);
                    Commonhelper.showToastLong(AddCartListActivity.this, jsonArray.optJSONObject(0).optString("Status"));
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }

    ArrayList<Double> totAmtAl = new ArrayList<>();
    ArrayList<Double> totIPAl = new ArrayList<>();
    int count = 0;
}

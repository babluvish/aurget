package com.aurget.buddha.Activity.categories;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import com.aurget.buddha.Activity.Main2Activity;
import com.google.android.material.tabs.TabLayout;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.aurget.buddha.Activity.Adapter.SliderAdapter;
import com.aurget.buddha.Activity.AddtoCart.AddCartListActivity;
import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.EmptyCartActivity;
import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.Activity.Rate.AllRateList;
import com.aurget.buddha.Activity.Rate.RateActivity;
import com.aurget.buddha.Activity.RecentViewListActivity;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.Utils.CustomPreference;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import static com.aurget.buddha.Activity.Commonhelper.discountAmt;
import static com.aurget.buddha.Activity.Commonhelper.getScreenWidth;
import static com.aurget.buddha.Activity.Commonhelper.openBrowser;
import static com.aurget.buddha.Activity.Commonhelper.roudOff;
import static com.aurget.buddha.Activity.Main2Activity.crtDetils2;
import static java.lang.Double.parseDouble;

public class SubCategoryDetails2 extends BaseActivity {

    RecyclerView recycleViewImg, recentViewRv, relaredProdRv, colorRv, sizeRv, rateRv;
    TextView addtocrtTv, addtocrtTv2, buyNowTv, buyNowTv2, priceTv, priceTv2, percentOffTv, ipTv, productNmeTv, soldTv, rtingTv, selectShippingAddreddTv;

    TextView lTv, xlTv, xxlTv, sTv, xl3Tv, mTv, sizeTv, colorTv, viewDetailsDis, crtCountTv, ratenowTv;
    ScrollView scrollView;
    LinearLayout cartBuyBtn, colorContaner, sizeContaner;
    LinearLayout cartBuyBtn2;
    RelativeLayout deliveryRl, recentViewRl, releatedFeaturedRl;
    RatingBar rting;
    String res;
    JSONObject jsonObject;
    boolean isSizeSelected = false;
    ImageView img, img5;
    int imgCount = 0;
    String product_id = "", paymentMethod = "", description = "";
    WebView webview;
    ViewPager viewPager, viewPager2;
    TabLayout indicator, indicator2, tabLayoutHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        setContentView(R.layout.activity_sub_category_details2);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        indicator = (TabLayout) findViewById(R.id.indicator);
        img5 = findViewById(R.id.img5);
        webview = findViewById(R.id.webview);

        recycleViewImg = findViewById(R.id.recycleViewImg);
        rateRv = findViewById(R.id.rateRv);
        colorRv = findViewById(R.id.colorRv);
        addtocrtTv = findViewById(R.id.addtocrtTv);
        buyNowTv = findViewById(R.id.buyNowTv);
        scrollView = findViewById(R.id.scrollView);
        cartBuyBtn = findViewById(R.id.cartBuyBtn);
        cartBuyBtn2 = findViewById(R.id.cartBuyBtn2);
        addtocrtTv2 = findViewById(R.id.addtocrtTv2);
        buyNowTv2 = findViewById(R.id.buyNowTv2);
        deliveryRl = findViewById(R.id.deliveryRl);
        rtingTv = findViewById(R.id.rtingTv);
        priceTv = findViewById(R.id.priceTv);
        priceTv2 = findViewById(R.id.priceTv2);
        sizeRv = findViewById(R.id.sizeRv);
        priceTv2.setPaintFlags(priceTv2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        percentOffTv = findViewById(R.id.percentOffTv);
        ipTv = findViewById(R.id.ipTv);
        productNmeTv = findViewById(R.id.productNmeTv);
        soldTv = findViewById(R.id.soldTv);
        rting = findViewById(R.id.rting);
        recentViewRv = findViewById(R.id.recentViewRv);
        relaredProdRv = findViewById(R.id.relaredProdRv);
        img = findViewById(R.id.img);
        sizeTv = findViewById(R.id.sizeTv);
        colorTv = findViewById(R.id.colorTv);
        ratenowTv = findViewById(R.id.ratenowTv);
        viewDetailsDis = findViewById(R.id.viewDetailsDis);

        colorContaner = findViewById(R.id.colorContaner);
        sizeContaner = findViewById(R.id.sizeContaner);

        recentViewRl = findViewById(R.id.recentViewRl);
        crtCountTv = findViewById(R.id.crtCountTv);
        //findViewById(R.id.searchEt).setVisibility(View.GONE);
        selectShippingAddreddTv = findViewById(R.id.selectShippingAddreddTv);

        findViewById(R.id.bkImg).setVisibility(View.GONE);
        findViewById(R.id.bkImg2).setVisibility(View.VISIBLE);

        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycleViewImg.setLayoutManager(manager);

        LinearLayoutManager managerCol = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        colorRv.setLayoutManager(managerCol);

        LinearLayoutManager managerSize = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        sizeRv.setLayoutManager(managerSize);

        LinearLayoutManager horizontalLayoutManager6 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recentViewRv.setLayoutManager(horizontalLayoutManager6);

        LinearLayoutManager horizontalLayoutManager7 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        relaredProdRv.setLayoutManager(horizontalLayoutManager7);

        LinearLayoutManager horizontalLayoutManager8 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rateRv.setLayoutManager(horizontalLayoutManager8);

        if (getIntent().hasExtra("res")) {
            res = getIntent().getStringExtra("res");

            if (getIntent().hasExtra("imgCount"))
            {
                imgCount = Integer.parseInt(getIntent().getStringExtra("imgCount"));
                //Commonhelper.showToastLong(SubCategoryDetails2.this,""+imgCount);
                product_id = getIntent().getStringExtra("product_id");
                viewPager.setAdapter(new SliderAdapter(SubCategoryDetails2.this, product_id, imgCount));

                AppConstants.imageList.clear();

                shareImage = InterfaceClass.imgPth2 + product_id + "_" + 1 + ".jpg";

                for (int i = 0; i < imgCount; i++) {
                    int count = i + 1;
                    HashMap<String, String> prodList1 = new HashMap();
                    prodList1.put("id", "" + count);
                    prodList1.put("name", InterfaceClass.imgPth2 + product_id + "_" + count + ".jpg");
                    AppConstants.imageList.add(prodList1);
                }

                Commonhelper.sop("AppConstants.imageList" + AppConstants.imageList.toString());

                if (imgCount == 1) {
                    indicator.setVisibility(View.GONE);
                }

                indicator.setupWithViewPager(viewPager, true);
                //recycleViewImg.setAdapter(new MyRecyclerAdapterVendor(null));

                new Handler().postDelayed(() -> {
                    crtDetils();
                    colorSizeApi(product_id);
                    //hitPiadd_size(product_id);
                }, 1000);
            }
            try {
                jsonObject = new JSONObject(res);
                description = jsonObject.optString("description");
                double sp = parseDouble(jsonObject.optString("sale_price"));
                double dis = parseDouble(jsonObject.optString("discount"));

                double camt = 0.0;
                try {
                    camt = parseDouble(jsonObject.optString("cut_amount"));
                } catch (NumberFormatException e) {
                    camt = 0.0;
                }
                double gt = discountAmt(sp, dis);

                long amt = roudOff(gt);

                ipTv.setText("IP: " + jsonObject.optString("ip"));
                productNmeTv.setText("" + jsonObject.optString("title"));

                try {
                    rting.setRating(Float.parseFloat(jsonObject.optString("rating_total")));
                    rtingTv.setText("" + Float.parseFloat(jsonObject.optString("rating_total")));
                } catch (Exception e) {

                }

                if (Integer.parseInt(jsonObject.optString("discount")) > 0) {
                    Commonhelper.isDiscountRate(jsonObject, percentOffTv);

//                    if (jsonObject.optString("discount_type").equals("percent")) {
//                        percentOffTv.setText("" + jsonObject.optString("discount") + "% Discount");
//                    }
//                    else
//                    {
//                        percentOffTv.setText("₹" + jsonObject.optString("discount") + " Discount");
//                    }
                    //priceTv2.setText("₹" + roudOff(camt));
                    priceTv2.setText("₹" + camt);
                    //priceTv.setText("₹" + amt);
                    priceTv.setText("₹" + sp);
                } else {
                    //priceTv.setText("₹" + roudOff(sp));
                    priceTv.setText("₹" + sp);
                    percentOffTv.setVisibility(View.GONE);
                    priceTv2.setVisibility(View.GONE);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Commonhelper.sop("resssss_" + res);
        }

        recentViewRl.setOnClickListener(view -> {
            Intent intent = new Intent(SubCategoryDetails2.this, RecentViewListActivity.class);
            startActivity(intent);
        });

        findViewById(R.id.reletedRr).setOnClickListener(view -> {
            Intent intent = new Intent(SubCategoryDetails2.this, RecentViewListActivity.class);
            intent.putExtra("pos", "reletedProduct");
            startActivity(intent);
        });

        findViewById(R.id.relativeLayout2).setOnClickListener(view -> openBrowser(SubCategoryDetails2.this, Constnt.returnPolicy));

        viewDetailsDis.setOnClickListener(view -> {

            if (viewDetailsDis.getText().toString().equals("View Details")) {
                viewDetailsDis.setText("Close");
                webview.setVisibility(View.VISIBLE);
            } else {
                viewDetailsDis.setText("View Details");
                webview.setVisibility(View.GONE);
                return;
            }

            final String mimeType = "text/html";
            final String encoding = "UTF-8";
            String html = description;
            Log.d("html", html);
            //webview.setNestedScrollingEnabled(true);
            webview.requestFocus();
//                webview.getSettings().setLoadWithOverviewMode(true);
//                webview.getSettings().setUseWideViewPort(true);
//                webview.getSettings().setSupportZoom(true);
//                webview.getSettings().setBuiltInZoomControls(true);
            webview.getSettings().setJavaScriptEnabled(true);
            webview.loadDataWithBaseURL("", html, mimeType, encoding, "");

//                Intent intent = new Intent(SubCategoryDetails2.this, WebviewActivity.class);
//                intent.putExtra("html",description);
//                startActivity(intent);
        });

        viewDetailsDis.performClick();

        addtocrtTv.setOnClickListener(view -> {

            if (lastSelectedPositionCol == -1 && colorContaner.getVisibility() == View.VISIBLE) {
                Commonhelper.showToastLong(SubCategoryDetails2.this, "Please select color image");
                return;
            }

            if (lastSelectedPosition == -1 && sizeContaner.getVisibility() == View.VISIBLE) {
                Commonhelper.showToastLong(SubCategoryDetails2.this, "Please select size");
                return;
            }

//                Intent intent = new Intent(SubCategoryDetails2.this, AddTOCart.class);
//                startActivity(intent);
            hitPiadd_cart(CustomPreference.readString(SubCategoryDetails2.this, "e_id", ""), jsonObject.optString("product_id"));
        });

        addtocrtTv2.setOnClickListener(view -> {

            if (lastSelectedPositionCol == -1 && colorContaner.getVisibility() == View.VISIBLE) {
                Commonhelper.showToastLong(SubCategoryDetails2.this, "Please select color image");
                return;
            }

            if (lastSelectedPosition == -1 && sizeContaner.getVisibility() == View.VISIBLE) {
                Commonhelper.showToastLong(SubCategoryDetails2.this, "Please select size");
                return;
            }

            hitPiadd_cart(CustomPreference.readString(SubCategoryDetails2.this, "e_id", ""), jsonObject.optString("product_id"));
//                Intent intent = new Intent(SubCategoryDetails2.this, AddTOCart.class);
//                startActivity(intent);
        });

        selectShippingAddreddTv.setOnClickListener(view -> {

//                Intent intent = new Intent(SubCategoryDetails2.this, Shipping_ddress_list_Activity.class);
//                intent.putExtra("SubCategoryDetails2", "true");
//                startActivityForResult(intent, 100);
        });

        buyNowTv.setOnClickListener(view -> {

            if (lastSelectedPositionCol == -1 && colorContaner.getVisibility() == View.VISIBLE) {
                Commonhelper.showToastLong(SubCategoryDetails2.this, "Please select color image");
                scrollView.post(() -> scrollView.fullScroll(View.FOCUS_UP));
                return;
            }

            if (lastSelectedPosition == -1 && sizeContaner.getVisibility() == View.VISIBLE) {
                Commonhelper.showToastLong(SubCategoryDetails2.this, "Please select size");
                scrollView.post(() -> scrollView.fullScroll(View.FOCUS_UP));
                return;
            }

            try {
                Intent intent = new Intent(SubCategoryDetails2.this, BuyNow.class);
                intent.putExtra("product_id", jsonObject.optString("product_id"));
                intent.putExtra("img", getIntent().getStringExtra("image"));
                intent.putExtra("size", size);
                intent.putExtra("color", color);
                startActivityForResult(intent, 100);
            } catch (NullPointerException e) {
                Commonhelper.showToastLong(SubCategoryDetails2.this, "ID not found");
            }
        });

        buyNowTv2.setOnClickListener(view -> {

            if (lastSelectedPositionCol == -1 && colorContaner.getVisibility() == View.VISIBLE) {
                Commonhelper.showToastLong(SubCategoryDetails2.this, "Please select color image");
                scrollView.post(() -> scrollView.fullScroll(View.FOCUS_UP));
                return;
            }

            if (lastSelectedPosition == -1 && sizeContaner.getVisibility() == View.VISIBLE) {
                Commonhelper.showToastLong(SubCategoryDetails2.this, "Please select size");
                scrollView.post(() -> scrollView.fullScroll(View.FOCUS_UP));
                return;
            }

            try {
                Intent intent = new Intent(SubCategoryDetails2.this, BuyNow.class);
                intent.putExtra("product_id", jsonObject.optString("product_id"));
                intent.putExtra("img", getIntent().getStringExtra("image"));
                intent.putExtra("size", size);
                intent.putExtra("color", color);
                startActivityForResult(intent, 100);
            } catch (NullPointerException e) {
                Commonhelper.showToastLong(SubCategoryDetails2.this, "ID not found");
            }
        });

        deliveryRl.setOnClickListener(view -> {
            Intent intent = new Intent(SubCategoryDetails2.this, ProductDelivryDetilsActivity.class);
            startActivity(intent);
        });

        findViewById(R.id.viewMoreTv).setOnClickListener(view -> {
            Intent intent = new Intent(SubCategoryDetails2.this, AllRateList.class);
            intent.putExtra("RateRes", RateRes);
            startActivity(intent);
        });

        ratenowTv.setOnClickListener(view -> {
            Intent intent = new Intent(SubCategoryDetails2.this, RateActivity.class);
            intent.putExtra("product_id", product_id);
            intent.putExtra("product_name", product_id);
            intent.putExtra("product_img", product_id);
            intent.putExtra("productName", productNmeTv.getText().toString());
            startActivity(intent);
        });

        scrollView.getViewTreeObserver().addOnScrollChangedListener(() -> {
            View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
            int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
            System.out.println("google " + diff);

            if (diff < 778) {
                cartBuyBtn2.setVisibility(View.GONE);
                cartBuyBtn.setVisibility(View.VISIBLE);
            } else {
                cartBuyBtn2.setVisibility(View.VISIBLE);
                cartBuyBtn.setVisibility(View.GONE);
            }
        });



        findViewById(R.id.bkImg2).setOnClickListener(view -> finish());

        findViewById(R.id.imghmb1).setOnClickListener(view -> {
            if (Integer.parseInt(crtCountTv.getText().toString().trim().replace("+","")) < 1) {
                Intent intent = new Intent(mActivity, EmptyCartActivity.class);
                startActivity(intent);
                return;
            }
            Intent intent = new Intent(mActivity, AddCartListActivity.class);
            intent.putExtra("res", crtDetils2);
            startActivity(intent);
        });

        findViewById(R.id.share).setOnClickListener(view -> {
            //shareProdDetails();
            Commonhelper.shareApps(mActivity,null, productNmeTv.getText().toString()+"\n"+"http://aurget.com/app/share_link.php?productID="+product_id);
        });


        final EditText searchEt = findViewById(R.id.searchEt);

        searchEt.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus)
            {
                searchEt.setCursorVisible(true);
            }
        });

        searchEt.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                Intent intent = new Intent(mActivity, SubCategoryDetailsActivity.class);
                intent.putExtra("search", searchEt.getText().toString());
                startActivity(intent);
                return true;
            }
            return false;
        });


        recentViewPi();
        reletedProductAPi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Handler().postDelayed(() -> {
            rateListAPi(product_id);
        }, 1000);
    }

    private void crtDetils() {
        new AbstrctClss(SubCategoryDetails2.this, ConstntApi.get_cart_details(SubCategoryDetails2.this), "g", false) {
            @Override
            public void responce(String s) {
                try {
                    JSONArray jsonArray = new JSONArray(s);
                    crtDetils2 = s;
                    if (jsonArray.length() > 9)
                        crtCountTv.setText("9+");
                    else crtCountTv.setText("" + jsonArray.length());
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("letestFeturePi", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }

    private void colorSizeApi(String product_id) {
        new AbstrctClss(SubCategoryDetails2.this, ConstntApi.color_image(SubCategoryDetails2.this,product_id), "g", false) {
            @Override
            public void responce(String s) {
                try {

                    String Res = s;
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArraySize  = jsonObject.optJSONArray("size");
                    JSONArray jsonArrayColor  = jsonObject.optJSONArray("color");
                    /*for size*/
                    if (Res.length() == 0 || jsonArraySize.length() == 0) {
                        sizeContaner.setVisibility(View.GONE);
                    }
                    else {
                        ArrayList arrayList = new ArrayList();
                        if (jsonArraySize.length() == 0) {
                            sizeContaner.setVisibility(View.GONE);
                            return;
                        }
                        sizeTv.setText(jsonArraySize.optJSONObject(0).optString("title"));

                        for (int i = 0; i < jsonArraySize.optJSONObject(0).optJSONArray("size").length(); i++) {
                            arrayList.add(jsonArraySize.optJSONObject(0).optJSONArray("size").get(i));
                            //Log.d("arrayList", arrayList.toString());
                        }

                        sizeRv.setAdapter(new SizeAdapter(arrayList));
                        sizeContaner.setVisibility(View.VISIBLE);
                    }

                    /*for color*/
                    if (Res.length() == 0) {
                        colorContaner.setVisibility(View.GONE);
                        return;
                    }

                    Log.d("hitPiadd_color", Res);

                    if (jsonArrayColor.length() == 0) {
                        colorContaner.setVisibility(View.GONE);
                        return;
                    }

                    if (jsonArrayColor.optJSONObject(0).optString("num_of_img").equalsIgnoreCase("null")) {
                        colorContaner.setVisibility(View.GONE);
                        return;
                    }

                    int num_of_img = Integer.parseInt(jsonArrayColor.optJSONObject(0).optString("num_of_img"));
                    colorRv.setAdapter(new colorAdapter(num_of_img));

                    colorContaner.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("letestFeturePi", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }

    private void recentViewPi() {
        recentViewRv.setAdapter(new MyRecyclerAdapterRecentllyView(Constnt.recentViewJAl));
    }

    private void reletedProductAPi()
    {
        relaredProdRv.setAdapter(new MyRecyclerAdapterRecentllyView(Constnt.product_list_set_recent_all));

        /*new AbstrctClss(mActivity, ConstntApi.recentViewPi_(mActivity), "g", false) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    JSONArray jsonArray = new JSONArray(Res);
                    ArrayList<JSONObject> relatedProdAl = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        relatedProdAl.add(jsonArray.getJSONObject(i));
                    }
                    relaredProdRv.setAdapter(new MyRecyclerAdapterRecentllyView(relatedProdAl));

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };*/
    }

    private void rateListAPi(String product_id) {
        new AbstrctClss(SubCategoryDetails2.this, ConstntApi.rating_list(SubCategoryDetails2.this, product_id), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    RateRes = s;
                    JSONArray jsonArray = new JSONArray(RateRes);
                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();
                    if (jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                        }
                        rateRv.setAdapter(new MyRecyclerAdapterRate(jsonObjectArrayList));
                    }
                    scrollView.post(() -> scrollView.fullScroll(View.FOCUS_UP));
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                    scrollView.post(() -> scrollView.fullScroll(View.FOCUS_UP));
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                scrollView.post(() -> scrollView.fullScroll(View.FOCUS_UP));
            }
        };
    }

    public class MyRecyclerAdapterRecentllyView extends RecyclerView.Adapter<MyRecyclerAdapterRecentllyView.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterRecentllyView(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.today_deal_item, parent, false);
            v.getLayoutParams().width = (int) (getScreenWidth(SubCategoryDetails2.this) / divideLine); /// THIS LINE WILL DIVIDE OUR VIEW INTO NUMBERS OF PARTS
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            holder.prodNmeTv.setText(jsonObjectal.get(position).optString("title"));
            holder.ipTv.setText("IP " + jsonObjectal.get(position).optString("ip"));
            //double disAmt = discountAmt(parseDouble(jsonObjectal.get(position).optString("sale_price")),parseDouble(jsonObjectal.get(position).optString("discount")));
            //long value = roudOff(disAmt);
            holder.priceTv.setText("₹" + jsonObjectal.get(position).optString("sale_price"));
            int i = Integer.parseInt(jsonObjectal.get(position).optString("product_sub_id"));
            Picasso.with(SubCategoryDetails2.this).load(InterfaceClass.imgPth2 + jsonObjectal.get(position).optString("product_id") + "_" + i + "_thumb.jpg").into(holder.img);
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView prodNmeTv, ipTv, priceTv;
            private ImageView img;

            public ViewHolder(View itemView) {
                super(itemView);

                prodNmeTv = itemView.findViewById(R.id.prodNmeTv);
                ipTv = itemView.findViewById(R.id.ipTv);
                priceTv = itemView.findViewById(R.id.priceTv);
                img = itemView.findViewById(R.id.img);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(SubCategoryDetails2.this, SubCategoryDetails2.class);
                        intent.putExtra("res", jsonObjectal.get(getAdapterPosition()).toString());
                        int pos = getAdapterPosition() + 1;
                        intent.putExtra("imgCount", jsonObjectal.get(getAdapterPosition()).optString("num_of_imgs"));
                        intent.putExtra("product_id", jsonObjectal.get(getAdapterPosition()).optString("product_id"));
                        intent.putExtra("image", InterfaceClass.imgPth2 + jsonObjectal.get(getAdapterPosition()).optString("product_id") + "_" + 1 + "_thumb.jpg");
                        startActivity(intent);
                        finish();
                    }
                });
            }
        }
    }

    public class MyRecyclerAdapterRate extends RecyclerView.Adapter<MyRecyclerAdapterRate.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterRate(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rate_item2, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {

            try {
                int i = position + 1;
                //Picasso.with(SubCategoryDetails2.this).load(InterfaceClass.imgPth2+product_id+"_"+i+"_thumb.jpg").into(holder.imageView);
                holder.userNameTv.setText(jsonObjectal.get(position).optString("user_name"));
                //holder.rate.setRating(Float.parseFloat(jsonObjectal.get(position).optString("rating")));
                holder.commentTv.setText(jsonObjectal.get(position).optString("comment"));
                holder.createdOnTv.setText("Created on: " + jsonObjectal.get(position).optString("created_at"));

                if (jsonObjectal.get(position).optString("rating").equals("1")) {
                    holder.horribleImg.setColorFilter(ContextCompat.getColor(SubCategoryDetails2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                } else if (jsonObjectal.get(position).optString("rating").equals("2")) {
                    holder.horribleImg.setColorFilter(ContextCompat.getColor(SubCategoryDetails2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                    holder.statImg2.setColorFilter(ContextCompat.getColor(SubCategoryDetails2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                } else if (jsonObjectal.get(position).optString("rating").equals("3")) {
                    holder.horribleImg.setColorFilter(ContextCompat.getColor(SubCategoryDetails2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                    holder.statImg2.setColorFilter(ContextCompat.getColor(SubCategoryDetails2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                    holder.statImg3.setColorFilter(ContextCompat.getColor(SubCategoryDetails2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                } else if (jsonObjectal.get(position).optString("rating").equals("4")) {
                    holder.horribleImg.setColorFilter(ContextCompat.getColor(SubCategoryDetails2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                    holder.statImg2.setColorFilter(ContextCompat.getColor(SubCategoryDetails2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                    holder.statImg3.setColorFilter(ContextCompat.getColor(SubCategoryDetails2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                    holder.statImg4.setColorFilter(ContextCompat.getColor(SubCategoryDetails2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                } else if (jsonObjectal.get(position).optString("rating").equals("5")) {
                    holder.horribleImg.setColorFilter(ContextCompat.getColor(SubCategoryDetails2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                    holder.statImg2.setColorFilter(ContextCompat.getColor(SubCategoryDetails2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                    holder.statImg3.setColorFilter(ContextCompat.getColor(SubCategoryDetails2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                    holder.statImg4.setColorFilter(ContextCompat.getColor(SubCategoryDetails2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                    holder.statImg5.setColorFilter(ContextCompat.getColor(SubCategoryDetails2.this, R.color.greencol), PorterDuff.Mode.SRC_IN);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            if (jsonObjectal.size() >= 4) {
                return 4;
            } else {
                return jsonObjectal.size();
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView userNameTv, commentTv, createdOnTv;
            ImageView statImg2, statImg3, statImg4, statImg5, horribleImg;

            public ViewHolder(View itemView) {
                super(itemView);
                horribleImg = itemView.findViewById(R.id.horribleImg);
                statImg2 = itemView.findViewById(R.id.statImg2);
                statImg3 = itemView.findViewById(R.id.statImg3);
                statImg4 = itemView.findViewById(R.id.statImg4);
                statImg5 = itemView.findViewById(R.id.statImg5);
                userNameTv = itemView.findViewById(R.id.userNameTv);
                commentTv = itemView.findViewById(R.id.commentTv);
                createdOnTv = itemView.findViewById(R.id.createdOnTv);
            }
        }
    }

    public class SizeAdapter extends RecyclerView.Adapter<SizeAdapter.ViewHolder> {

        private ArrayList<String> jsonObjectal;

        public SizeAdapter(ArrayList<String> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
            lastSelectedPosition = -1;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_size_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {

            //   Log.d("imgkk",getIntent().getStringExtra("image"));
            holder.imageView.setText(jsonObjectal.get(position));

            holder.imageView.setBackgroundResource(R.color.white);

            if (lastSelectedPosition == position) {
                holder.imageView.setBackgroundResource(R.drawable.line6);
            }

            holder.imageView.setOnClickListener(v -> {
                size = holder.imageView.getText().toString();
                int previousItem = lastSelectedPosition;
                lastSelectedPosition = position;

                notifyItemChanged(previousItem);
                notifyItemChanged(position);

                Commonhelper.sop("lastSelectedPosition" + lastSelectedPosition);
            });
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView imageView;

            public ViewHolder(View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.mTv);
            }
        }
    }

    public class colorAdapter extends RecyclerView.Adapter<colorAdapter.ViewHolder> {

        private int num_img;

        public colorAdapter(int num_img) {
            this.num_img = num_img;
            lastSelectedPositionCol = -1;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_color_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {

            //   Log.d("imgkk",getIntent().getStringExtra("image"));
            int i = position + 1;
            //Picasso.with(SubCategoryDetails2.this).load(getIntent().getStringExtra("image")).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(holder.imageView);
            String imgPath = InterfaceClass.colorPath + product_id + "_" + i + "_thumb.jpg";
            Commonhelper.sop("imgPath="+imgPath);
            Picasso.with(SubCategoryDetails2.this).load(imgPath).into(holder.imageView);

            holder.rl.setBackgroundResource(R.color.gray);

            if (lastSelectedPositionCol == position)
                holder.rl.setBackgroundResource(R.drawable.line5);

            holder.itemView.setOnClickListener(view -> {
                int previousItem = lastSelectedPositionCol;
                lastSelectedPositionCol = position;
                notifyItemChanged(previousItem);
                notifyItemChanged(position);
                int i2 = position+1;
                color = InterfaceClass.colorPath + product_id + "_" + i2 + "_thumb.jpg";
                Commonhelper.sop("color"+color);
            });
        }

        @Override
        public int getItemCount() {
            return num_img;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            ImageView imageView, tickImg;
            LinearLayout rl;
            RelativeLayout rl1;

            public ViewHolder(View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.img);
                tickImg = itemView.findViewById(R.id.tickImg);
                rl = itemView.findViewById(R.id.rl);
                rl1 = itemView.findViewById(R.id.rl1);
            }
        }
    }

    private void hitPiadd_cart(String loginID, String productID) {
        final Dialog dialog = Commonhelper.loadDialog(SubCategoryDetails2.this);
        String req = InterfaceClass.ipAddress3 + "add_cart.php?cart_count=10&login_id=" + loginID + "&product_id=" + productID + "&product_size=" + size + "&product_color=" + color;
        Log.d("hitPiadd_cart", req);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, req,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String Res = response;
                            Log.d("hitPiadd_cart", Res);
                            JSONArray jsonArray = new JSONArray(Res);

                            if (jsonArray.optJSONObject(0).optString("Status").equalsIgnoreCase("true")) {
                                Commonhelper.showToastLong(SubCategoryDetails2.this, "Item added successfully");
                                crtDetils();
                            } else {
                                Commonhelper.showToastLong(SubCategoryDetails2.this, Constnt.notFound);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("dsd", "sssss");
                        }

                        dialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("onErrorResponse", "hitPi2");
                        dialog.dismiss();
                    }
                }) {
//            @Override
//            protected Map getParams()
//            {
//                Map params = new HashMap();
//                params.put("username", "");
//                params.put("email", "abc@androidhive.info");
//                params.put("password", "password123");
//
//                return params;
//            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(SubCategoryDetails2.this);
        requestQueue.add(stringRequest);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void shareProdDetails()
    {
         try {
            StrictMode.ThreadPolicy policy1 = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy1);
             Commonhelper.sop("profile" + ""+shareImage);
            new GetImageFromUrl().execute(shareImage);
            } catch (Exception e) {
             Commonhelper.sop("profile" + "erroror");
         }
    }

    public class GetImageFromUrl extends AsyncTask<String, Void, Bitmap> {
        Dialog dialog = Commonhelper.loadDialog(mActivity);
        Bitmap bitmap = null;

        public GetImageFromUrl() {
        }

        @Override
        protected Bitmap doInBackground(String... url) {
            String stringUrl = url[0];
            bitmap = null;
            InputStream inputStream;
            try {
                inputStream = new java.net.URL(stringUrl).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            try {
                bitmapconvert(bitmap);
            } catch (Exception e) {

            }

            dialog.dismiss();
        }
    }

    void bitmapconvert(Bitmap bitmap) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    File file = new File(getExternalCacheDir(), "share.png");
                    FileOutputStream fOut = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.flush();
                    fOut.close();
                    file.setReadable(true, false);
                    Commonhelper.shareApps(mActivity,file, productNmeTv.getText().toString()+"\n"+"http://aurget.com/app/share_link.php?productID="+product_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    private String address_id = "", color = "", size = "",shareImage="",prodDetls="";
    float divideLine = 2.5f;
    int lastSelectedPosition =-1;
    int lastSelectedPositionCol = -1;
    private String RateRes = "";
}


package com.aurget.buddha.Activity.code.Insurance;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import code.utils.AppUrls;


public class InsuranceActivity extends BaseActivity implements View.OnClickListener {

    //ImageView
    ImageView ivCart,ivMenu,ivSearch,ivHome,ivCheck,ivMobile;

    //TextView
    TextView tvHeaderText,tvSubmit,tvUseWallet;
    TextView tvRech,tvDate;

    //ArrayList
    public static ArrayList<String> operator_List = new ArrayList<String>();
    public static ArrayList<String> operator_Code = new ArrayList<String>();

    //ArrayList
    public static ArrayList<String> circle_List = new ArrayList<String>();
    public static ArrayList<String> circle_Code = new ArrayList<String>();

    //Calendar
    private Calendar myCalendar = Calendar.getInstance();

    //Spinner
    Spinner spinner_operator,spinner_circle;

    String operatorId="",circle_code="";

    View v1;

    //EditText
    EditText edMobile,edAmount;

    //Typeface
    Typeface typeface;

    //RelativeLayout
    RelativeLayout rrUseWallet;

    //ImageView
    ImageView ivCal;

    float wallet=0;
    int check=0;

    // Declare
    static final int PICK_CONTACT=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insurance);

        initialise();
    }

    private void initialise() {

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        operator_List.clear(); operator_Code.clear();
        operator_List.add("Select Operator"); operator_Code.add("Select Insurance Operator");
        operator_List.add("ICICI Prudential Life Insurance_CP");operator_Code.add("ILI");
        operator_List.add("Tata AIA Life Insurance_CP");operator_Code.add("TLI");

        circle_List.clear();circle_Code.clear();
        circle_List.add("Select Your Region"); circle_Code.add("Select Your Region");
        circle_List.add("Andhra Pradesh"); circle_Code.add("1");
        circle_List.add("Assam"); circle_Code.add("2");
        circle_List.add("Bihar"); circle_Code.add("3");
        circle_List.add("Chennai"); circle_Code.add("4");
        circle_List.add("Delhi"); circle_Code.add("5");
        circle_List.add("Gujarat"); circle_Code.add("6");
        circle_List.add("Haryana"); circle_Code.add("7");
        circle_List.add("Himachal Pradesh"); circle_Code.add("8");
        circle_List.add("Jammu & Kashmir"); circle_Code.add("9");
        circle_List.add("Karnataka"); circle_Code.add("10");
        circle_List.add("Kerala"); circle_Code.add("11");
        circle_List.add("Kolkata"); circle_Code.add("12");
        circle_List.add("Maharashtra & Goa (except Mumbai)"); circle_Code.add("13");
        circle_List.add("Madhya Pradesh & Chhattisgarh"); circle_Code.add("14");
        circle_List.add("Mumbai"); circle_Code.add("15");
        circle_List.add("North East"); circle_Code.add("16");
        circle_List.add("Orissa"); circle_Code.add("17");
        circle_List.add("Punjab"); circle_Code.add("18");
        circle_List.add("Rajasthan"); circle_Code.add("19");
        circle_List.add("Tamil Nadu"); circle_Code.add("20");
        circle_List.add("Uttar Pradesh - East"); circle_Code.add("21");
        circle_List.add("Uttar Pradesh - West"); circle_Code.add("22");
        circle_List.add("West Bengal"); circle_Code.add("23");
        circle_List.add("Jharkhand"); circle_Code.add("24");

        //Typeface
        typeface   = Typeface.createFromAsset(mActivity.getAssets(),"centurygothic.otf");

        //RelativeLayout
        rrUseWallet= (RelativeLayout) findViewById(R.id.rrUseWallet);

        //ImageView
        ivCart =  findViewById(R.id.ivCart);
        ivMenu =  findViewById(R.id.iv_menu);
        ivSearch =  findViewById(R.id.searchmain);
        ivHome =  findViewById(R.id.ivHomeBottom);
        ivCheck =  findViewById(R.id.ivWalletCheck);
        ivMobile =  findViewById(R.id.imageView20);

        //EditText
        edMobile =  findViewById(R.id.editText3);
        edAmount =  findViewById(R.id.editText4);

        //TextView
        tvHeaderText    =  findViewById(R.id.tvHeaderText);
        tvDate=  findViewById(R.id.tvDate);
        tvSubmit=  findViewById(R.id.textView27);
        tvUseWallet= findViewById(R.id.tvWalletCheck);
        tvRech=  findViewById(R.id.tvh2r);

        //ImageView
        ivCal =  findViewById(R.id.imageView19);

        //View
        v1 =findViewById(R.id.v1);

        //Spinner
        spinner_operator = findViewById(R.id.spinner2);
        spinner_circle =   findViewById(R.id.spinner3);

        ivCart.setOnClickListener(this);
        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        ivMobile.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        rrUseWallet.setOnClickListener(this);
        ivCal.setOnClickListener(this);

        ivCart.setVisibility(View.INVISIBLE);
        ivSearch.setVisibility(View.INVISIBLE);

        ivMenu.setImageResource(R.drawable.ic_back);
        tvHeaderText.setText(getString(R.string.insurance_pay));

        if(!AppSettings.getString(AppSettings.wallet).equals(""))
        {
            wallet = Float.parseFloat(AppSettings.getString(AppSettings.wallet));
        }

        if(wallet<=0)
        {
            check=0;
            ivCheck.setImageResource(R.drawable.ic_check_box_unselected);
            rrUseWallet.setVisibility(View.GONE);
        }
        else
        {
            check=1;
            ivCheck.setImageResource(R.drawable.ic_check_box_selected);
            rrUseWallet.setVisibility(View.VISIBLE);
        }

        tvUseWallet.setText("Use RS "+AppSettings.getString(AppSettings.wallet));

        spinner_operator.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_img_text, operator_List));
        spinner_operator.setSelection(0);

        spinner_circle.setAdapter(new adapter_spinner_new(getApplicationContext(), R.layout.spinner_textview_rel, circle_List));
        spinner_circle.setSelection(0);

        spinner_operator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                if(position==0)
                {
                    operatorId = "";
                }
                else
                {
                    AppSettings.putString(AppSettings.operatorName,operator_List.get(position));
                    operatorId = operator_Code.get(position);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        spinner_circle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub

                if(position==0)
                {
                    circle_code = "";
                }
                else
                {
                    AppSettings.putString(AppSettings.circleName,circle_List.get(position));
                    circle_code = circle_Code.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.iv_menu:
                finish();
                return;

            case R.id.imageView19:

                new DatePickerDialog(InsuranceActivity.this, d, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();

                return;

            case R.id.rrUseWallet:

                if(check==0)
                {
                    check=1;
                    ivCheck.setImageResource(R.drawable.ic_check_box_selected);
                }
                else
                {
                    check=0;
                    ivCheck.setImageResource(R.drawable.ic_check_box_unselected);
                }

                return;

            case R.id.textView27:

                AppUtils.hideSoftKeyboard(mActivity);

               /* int amount = 0;
                try {
                    amount = Integer.parseInt(edAmount.getText().toString().trim());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                if(operatorId.equals(""))
                {
                    AppUtils.showErrorMessage(tvHeaderText,getString(R.string.errorrechargeoperator),mActivity);
                }
                else  if(circle_code.equals(""))
                {
                    AppUtils.showErrorMessage(tvHeaderText,getString(R.string.errorCircle),mActivity);
                }
                else if(edMobile.getText().toString().isEmpty())
                {
                    AppUtils.showErrorMessage(tvHeaderText,getString(R.string.errorMobileNumber),mActivity);
                }
                else if(amount<10||amount>1000)
                {
                    AppUtils.showErrorMessage(tvHeaderText,getString(R.string.errorAmount),mActivity);
                }
                else if(tvDate.getText().toString().isEmpty())
                {
                    AppUtils.showErrorMessage(tvDate,getString(R.string.errorDOB),mActivity);
                }
                else if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {

                    startInsuranceApi();

                } else {
                    AppUtils.showErrorMessage(tvHeaderText, getString(R.string.errorInternet), mActivity);
                }*/

                return;

        }
    }

    public class adapter_spinner extends ArrayAdapter<String> {

        ArrayList<String> data;

        public adapter_spinner(Context context, int textViewResourceId, ArrayList <String> arraySpinner_time) {

            super(context, textViewResourceId, arraySpinner_time);

            this.data = arraySpinner_time;

        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row=inflater.inflate(R.layout.spinner_img_text, parent, false);
            ImageView imageView=(ImageView) row.findViewById(R.id.imageView22);
            TextView label=(TextView)row.findViewById(R.id.tv_spinner_name);

            imageView.setVisibility(View.VISIBLE);
            if(position==0)
            {
                imageView.setVisibility(View.GONE);
            }
            else if(position==1)
            {
                imageView.setBackgroundResource(R.drawable.icici);
            }
            else if(position==2)
            {
                imageView.setBackgroundResource(R.drawable.tata);
            }

            label.setText(data.get(position).toString());

            //label.setTypeface(typeface);

            return row;
        }
    }

    public class adapter_spinner_new extends ArrayAdapter<String> {

        ArrayList<String> data;

        public adapter_spinner_new(Context context, int textViewResourceId, ArrayList <String> arraySpinner_time) {

            super(context, textViewResourceId, arraySpinner_time);

            this.data = arraySpinner_time;

        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row=inflater.inflate(R.layout.spinner_textview_rel, parent, false);
            TextView label=(TextView)row.findViewById(R.id.tv_spinner_name);

            label.setText(data.get(position).toString());

            //label.setTypeface(typeface);

            return row;
        }
    }

    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            myCalendar.set(year, monthOfYear, dayOfMonth);
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy"/*, new Locale("en_US")*/);
            String formattedDate = df.format(myCalendar.getTime());

            tvDate.setText(formattedDate);
        }
    };

    private void startInsuranceApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("startInsuranceApi", AppUrls.startInsurance);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {

            json_data.put("uuid", AppSettings.getString(AppSettings.userId));
            json_data.put("insurance_amt", edAmount.getText().toString().trim());
            json_data.put("policy_number",edMobile.getText().toString().trim());
            json_data.put("service_provider", operatorId);
            json_data.put("circle_code", circle_code);
            json_data.put("wallet", check);
            json_data.put("dob", tvDate.getText().toString().trim());

            json.put(AppConstants.result, json_data);

            Log.v("startInsuranceApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.startInsurance)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(edAmount, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(edAmount, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedata(JSONObject response) {

        Log.d("response ", response.toString());
        AppUtils.hideDialog();

        try {

            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);

            if (jsonObject.getString("res_code").equals("1")) {

                JSONObject object = jsonObject.getJSONObject("transaction_detail");

                AppSettings.putString(AppSettings.transId,object.getString("txn_id"));
                AppSettings.putString(AppSettings.recAmount,edAmount.getText().toString().trim());
                AppSettings.putString(AppSettings.amount,object.getString("amount"));
                AppSettings.putString(AppSettings.wallet,object.getString("wallet_amount"));
                AppSettings.putString(AppSettings.recMobile,edMobile.getText().toString().trim());

                if(object.getString("payment_req").equals("1"))
                {
                    AppUtils.showErrorMessage(tvHeaderText, getString(R.string.paymentRequired), mActivity);
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {

                        insuranceApi();

                    } else {
                        AppUtils.showErrorMessage(tvHeaderText, getString(R.string.errorInternet), mActivity);
                    }
                }
                else
                {
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {

                        insuranceApi();

                    } else {
                        AppUtils.showErrorMessage(tvHeaderText, getString(R.string.errorInternet), mActivity);
                    }
                }

                /*if(object.getString("amount").contains("-"))
                {
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {

                        insuranceApi();

                    } else {
                        AppUtils.showErrorMessage(tvHeaderText, getString(R.string.errorInternet), mActivity);
                    }
                }
                else {
                    AppUtils.showErrorMessage(tvHeaderText, getString(R.string.errorInternet), mActivity);
                }*/

                /*if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {

                    insuranceApi();

                } else {
                    AppUtils.showErrorMessage(tvHeaderText, getString(R.string.errorInternet), mActivity);
                }*/

            }
            else
            {
                AppUtils.showErrorMessage(edMobile, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }

        } catch (Exception e) {
            AppUtils.showErrorMessage(edAmount, String.valueOf(e), mActivity);
        }

    }

    private void insuranceApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("insuranceApi", AppUrls.insuranceApi);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {

            json_data.put("txn_id", AppSettings.getString(AppSettings.transId));

            json.put(AppConstants.result, json_data);

            Log.v("insuranceApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.insuranceApi)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseRechagreResponse(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();

                        //startActivity(new Intent(getBaseContext(), RecStatusActivity.class));
                        finish();

                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(edAmount, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(edAmount, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseRechagreResponse(JSONObject response) {

        Log.d("response ", response.toString());

        try {

            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);

            if (jsonObject.getString("res_code").equals("1")) {

                //startActivity(new Intent(getBaseContext(), RecStatusActivity.class));
                finish();
            }
            else
            {
                AppUtils.showErrorMessage(edMobile, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(edAmount, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
    }
}

package com.aurget.buddha.Activity.code.Insurance;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;

public class InsuranceMainActivity extends BaseActivity implements View.OnClickListener {

    //ImageView
    ImageView ivCart;
    ImageView ivMenu;
    ImageView ivSearch;

    RelativeLayout rrRecharge,rrRecHistory;

    //TextView
    TextView tvHeaderText,tvH1,tvH2,tvH3,tvH4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge_main);

        findId();
    }

    private void findId() {

        //ImageView
        ivCart =    findViewById(R.id.ivCart);
        ivMenu =    findViewById(R.id.iv_menu);
        ivSearch =  findViewById(R.id.searchmain);

        //TextView
        tvHeaderText =  findViewById(R.id.tvHeaderText);
        tvH1=           findViewById(R.id.textView2);
        tvH2=           findViewById(R.id.tvH2);
        tvH3=           findViewById(R.id.tv2);
        tvH4=           findViewById(R.id.tvH4);

       //RelativeLayout
        rrRecharge =   findViewById(R.id.rrRecharge);
        rrRecHistory = findViewById(R.id.rrHistory);

        ivCart.setVisibility(View.INVISIBLE);
        ivSearch.setVisibility(View.INVISIBLE);

        ivMenu.setImageResource(R.drawable.ic_back);

        tvHeaderText.setText(getString(R.string.insurance_policy));
        tvH1.setText(getString(R.string.insurance));
        tvH2.setText(getString(R.string.insurance_pay));
        tvH3.setText(getString(R.string.insurance_history));
        tvH4.setText(getString(R.string.insurance_all));

        ivCart.setOnClickListener(this);
        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        rrRecharge.setOnClickListener(this);
        rrRecHistory.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.iv_menu:
                finish();
                return;

            case R.id.rrRecharge:
                startActivity(new Intent(getBaseContext(), InsuranceActivity.class));
                return;

            case R.id.rrHistory:
                //startActivity(new Intent(getBaseContext(), TransHistoryActivity.class));
                return;
        }
    }
}

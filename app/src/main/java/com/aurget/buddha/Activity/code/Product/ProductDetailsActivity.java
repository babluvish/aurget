package com.aurget.buddha.Activity.code.Product;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.os.Vibrator;
import androidx.annotation.RequiresApi;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.categories.SubCategoryDetails2;
import com.aurget.buddha.Activity.code.Adapter.MyPagerAdapter;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Dashboard.Slider.CardPagerAdapter;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Database.DatabaseController;
import com.aurget.buddha.Activity.code.Database.TableFavourite;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.Search.SearchActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import code.utils.AppUrls;
import me.relex.circleindicator.CircleIndicator;


public class ProductDetailsActivity extends BaseActivity implements View.OnClickListener {

    //ArrayList
    public static ArrayList<String> prodId = new ArrayList<String>();
    public static ArrayList<String> prodWeight = new ArrayList<String>();
    public static ArrayList<String> unitName = new ArrayList<String>();
    ArrayList<HashMap<String, String>> cartList = new ArrayList<HashMap<String, String>>();
    TextView tvProductName, textViewAddToCart, textViewBuyNow, tvDiscount;
    TextView tvFPrice, tvPrice, tvDeatils, tvUnit;
    ImageView ivProductImage, ivMenu, ivSearch, ivCart, ivHeart;
    TextView tvCount;
    //LinearLayout
    LinearLayout llFavourite, llCartBtn, llCartCountBtn, llDropdown;
    ImageAdapter imageAdapter;
    ViewPager mPager;
    CircleIndicator circle;
    MyPagerAdapter adapter;
    public static int VB_FIRST_PAGE;
    TextView tvQuantityCount;
    RecyclerView recyclerview;
    AppCompatSpinner spinner;
    private ImageView[] dots;
    private LinearLayout dotsLayout;
    CardPagerAdapter mCardAdapter;
    ArrayList<HashMap<String, String>> productList = new ArrayList();
    ArrayList<HashMap<String, String>> pagerllist = new ArrayList();

    String productId = "";
    private int selectedPosition = 0;
    int count;
    ImageView imageView2, ivNegative, imageView;
    int productQuantity;
    String cartId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);
        findById();
        getCartListApi();

    /*    Commonhelper.sop("dataaa"+getIntent().getStringExtra("data"));
        Commonhelper.sop("dataaa2"+getIntent().getStringExtra("data3"));*/
    }

    private void findById()
    {
        //LinearLayout
        llFavourite = findViewById(R.id.layout_action3);
        llCartBtn = findViewById(R.id.llCartBtn);
        llCartCountBtn = findViewById(R.id.llCartCountBtn);
        // llCartCountBtn.setVisibility(View.GONE);
        // llCartBtn.setVisibility(View.VISIBLE);
        //TextView
        tvProductName = findViewById(R.id.tvProductName);
        textViewAddToCart = findViewById(R.id.text_action_bottom1);
        textViewBuyNow = findViewById(R.id.text_action_bottom2);
        tvFPrice = findViewById(R.id.tvFPrice);
        tvPrice = findViewById(R.id.tvPrice);
        tvDiscount = findViewById(R.id.tvDiscount);
        tvDeatils = findViewById(R.id.tvDeatils);
        tvUnit = findViewById(R.id.tvUnit);
        tvCount = findViewById(R.id.tvCount);
        tvQuantityCount = findViewById(R.id.tvQuantityCount);
        //Recyclerview
        recyclerview = findViewById(R.id.recyclerview);
        spinner = findViewById(R.id.spinners);
        //ImageView
        llDropdown = findViewById(R.id.llDropdown);
        ivSearch = findViewById(R.id.searchmain);
        ivMenu = findViewById(R.id.iv_menu);
        ivProductImage = findViewById(R.id.image1);
        ivCart = findViewById(R.id.ivCart);
        ivHeart = findViewById(R.id.ivHeart);
        imageView2 = findViewById(R.id.imageView2);
        ivNegative = findViewById(R.id.ivNegative);
        imageView = findViewById(R.id.imageView);
        ivMenu.setOnClickListener(this);
        llCartBtn.setOnClickListener(this);

        ivSearch.setOnClickListener(this);
        textViewAddToCart.setOnClickListener(this);
        textViewBuyNow.setOnClickListener(this);
        ivCart.setOnClickListener(this);
        llFavourite.setOnClickListener(this);
        imageView2.setOnClickListener(this);
        ivNegative.setOnClickListener(this);
        imageView.setOnClickListener(this);

        tvQuantityCount.setText("1");
        ivProductImage.setVisibility(View.GONE);
        ivProductImage.setOnClickListener(this);
        ivMenu.setImageResource(R.drawable.ic_back);
        mPager = findViewById(R.id.pager);
        circle = findViewById(R.id.circle);
        dotsLayout = findViewById(R.id.layoutDots);
        addBottomDots(0);
        llDropdown.setOnClickListener(this);

        mPager.setPageTransformer(false, adapter);
        mPager.setCurrentItem(currentPage, true);
        mPager.setOffscreenPageLimit(3);
        mPager.setClipToPadding(false);
        mPager.setPadding(5, 5, 5, 0);

        int margin = getResources().getDimensionPixelSize(R.dimen._10sdp);
        mPager.setPageMargin(margin);
        //mPager.setAdapter( mCardAdapter );
        mPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                int pageWidth = mPager.getMeasuredWidth() -
                        mPager.getPaddingLeft() - mPager.getPaddingRight();
                int pageHeight = mPager.getHeight();
                int paddingLeft = mPager.getPaddingLeft();
                float transformPos = (float) (page.getLeft() -
                        (mPager.getScrollX() + paddingLeft)) / pageWidth;
                int max = pageHeight / 10;
                if (transformPos < -1) {
                    page.setScaleY(0.8f);
                } else if (transformPos <= 1) {

                    page.setScaleY(1f);
                } else {
                    page.setScaleY(0.8f);
                }
            }
        });

        if (AppConstants.imageList.size() > 0) {
            TimerTask timerTask = new MyTimerTask();
            timer = new Timer();
            timer.schedule(timerTask, 3000, 3000);
        }
        currentPage++;
        NUM_PAGES = AppConstants.imageList.size();
        VB_PAGES = AppConstants.imageList.size();
        VB_FIRST_PAGE = VB_PAGES / 2;


        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                getProductDetailApi();
        } else {
            AppUtils.showErrorMessage(tvProductName, getString(R.string.errorInternet), mActivity);
        }
/*
        if (DatabaseController.checkRecordExist( TableFavourite.favourite, String.valueOf( TableFavourite.favouriteColumn.productId ), productId )) {
            ivHeart.setImageResource( R.drawable.ic_heart_red );
        } else {
            ivHeart.setImageResource( R.drawable.ic_heart_grey );
        }
*/


        findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                //shareProdDetails();
                String path =  "http://aurget.com/app/share_link.php?productID="+getIntent().getStringExtra(AppConstants.productId)+"&come_from="+
                        getIntent().getStringExtra(AppConstants.from)+"&user_id="+AppSettings.getString(AppSettings.userId);

                Commonhelper.shareApps(mActivity, null,tvProductName.getText().toString()+"\n"+path);
            }
        });


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                productId = prodId.get(position);
                //  setValues( position);
                if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                    //  startActivity( new Intent( mActivity, LoginActivity.class ) );
                } else {
                    int c = 0;
                    for (int i = 0; i < cartList.size(); i++)
                    {
                        if (productId.equalsIgnoreCase(cartList.get(i).get("product_id")))
                        {
                            Log.v("CartListCheck", "sId " + cartList.get(i).get("product_id"));
                            tvQuantityCount.setText(cartList.get(i).get("quantity"));
                            //Toast.makeText(ProductDetailsActivity.this, "Hello", Toast.LENGTH_SHORT).show();
                            llCartBtn.setVisibility(View.GONE);
                            llCartCountBtn.setVisibility(View.VISIBLE);
                            tvFPrice.setText(getString(R.string.rs) + cartList.get(i).get("final_price"));
                            c++;
                        }
                    }
                   /* if(c==0){
                        llCartBtn.setVisibility(View.VISIBLE);
                        llCartCountBtn.setVisibility(View.GONE);
                    }*/

                    // count=  (Integer.parseInt( holder.tvQuantityCount.getText().toString() )) ;
                    /*tvQuantityCount.setText(String.valueOf(count));*/
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void getProductDetailApi()
    {
        AppUtils.showRequestDialog(mActivity);
        Log.v("getProductDetailApi", AppUrls.getProductDetail);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try
        {
            json_data.put("product_id", getIntent().getStringExtra(AppConstants.productId));
            json_data.put("come_from", getIntent().getStringExtra(AppConstants.from));
            json_data.put("user_id", AppSettings.getString(AppSettings.userId));
            json.put(AppConstants.result, json_data);
            Log.v("getProductDetailApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.getProductDetail)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvProductName, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvProductName, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void addBottomDots(final int currentPage) {
        dots = new ImageView[AppConstants.imageList.size()];
        dotsLayout.removeAllViews();

        for (int i = 0; i < AppConstants.imageList.size(); i++) {
            dots[i] = new ImageView(getApplicationContext());
            dots[i].setImageResource(R.drawable.ic_circle_white);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(20,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(5, 0, 5, 0);
            dotsLayout.addView(dots[i], params);

        }
        if (AppConstants.imageList.size() > 0)
            dots[currentPage].setImageResource(R.drawable.ic_circle_blue);
    }

    private void parseJsondata(JSONObject response) {
        Log.d("response ", response.toString());
        productList.clear();
        prodId.clear();
        prodWeight.clear();
        AppConstants.imageList.clear();
        try
        {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);

            if (jsonObject.getString("res_code").equals("1")) {

                tvProductName.setText(jsonObject.getString("product_name"));
                productId = jsonObject.getString("id");

                if (jsonObject.getString("product_discount_type").equals("1")) {
                    tvPrice.setVisibility(View.VISIBLE);
                    tvPrice.setPaintFlags(tvPrice.getPaintFlags() | 16);
                    tvDiscount.setVisibility(View.VISIBLE);
                    tvDiscount.setText(getString(R.string.rs) + " " + jsonObject.getString("product_discount_amount") + " /-off");
                } else if (jsonObject.getString("product_discount_type").equals("2")) {
                    tvPrice.setVisibility(View.VISIBLE);
                    tvPrice.setPaintFlags(tvPrice.getPaintFlags() | 16);
                    tvDiscount.setVisibility(View.VISIBLE);
                    tvDiscount.setText(jsonObject.getString("product_discount_amount") + "% off");
                } else {
                    tvDiscount.setText("");
                    tvDiscount.setVisibility(View.INVISIBLE);
                    tvPrice.setVisibility(View.INVISIBLE);
                }
                if (jsonObject.getString("price").equalsIgnoreCase(jsonObject.getString("final_price"))) {
                    tvFPrice.setText(getString(R.string.rs) + " " + jsonObject.getString("final_price"));
                    tvDiscount.setText("");
                    tvDiscount.setVisibility(View.INVISIBLE);
                    tvPrice.setText("");
                } else {
                    tvFPrice.setText(getString(R.string.rs) + " " + jsonObject.getString("final_price"));
                    tvPrice.setText(getString(R.string.rs) + " " + jsonObject.getString("price"));
                    tvPrice.setPaintFlags(tvPrice.getPaintFlags() | 16);
                }

                tvDeatils.setText("\u2022 " + jsonObject.getString("description"));
                tvUnit.setText(jsonObject.getString("weight_litr") + " " + jsonObject.getString("unit"));

                HashMap<String, String> prodList = new HashMap();

                prodList.put("id", jsonObject.getString("id"));
                prodList.put("product_discount_type", jsonObject.getString("product_discount_type"));
                prodList.put("product_discount_amount", jsonObject.getString("product_discount_amount"));
                prodList.put("price", jsonObject.getString("price"));
                prodList.put("final_price", jsonObject.getString("final_price"));
                prodList.put("weight_litr", jsonObject.getString("weight_litr"));
                prodList.put("unit", jsonObject.getString("unit"));
                prodList.put("quantity", jsonObject.getString("quantity"));

                productQuantity = Integer.parseInt(jsonObject.getString("quantity"));

                prodId.add(jsonObject.getString("id"));
                prodWeight.add(jsonObject.getString("weight_litr") + " " + jsonObject.getString("unit"));

                productList.add(prodList);
                JSONArray imageArray = jsonObject.getJSONArray("images");

                for (int i = 0; i < imageArray.length(); i++) {
                    JSONObject imageobject = imageArray.getJSONObject(i);
                    HashMap<String, String> prodList1 = new HashMap();
                    prodList1.put("id", imageobject.getString("id"));
                    prodList1.put("name", imageobject.getString("name"));
                    AppConstants.imageList.add(prodList1);
                    AppConstants.imageListtemp.add(prodList1);
                    pagerllist.add(prodList1);
                }

                JSONArray subProduct = jsonObject.getJSONArray("sub_product");

                for (int i = 0; i < subProduct.length(); i++) {
                    JSONObject jsonObject1 = subProduct.getJSONObject(i);
                    cartId = jsonObject1.getString("cartId");
                }

                JSONArray productArrayList = jsonObject.getJSONArray("product_list");
                for (int i = 0; i < productArrayList.length(); i++) {
                    JSONObject productobject = productArrayList.getJSONObject(i);
                    HashMap<String, String> prodList2 = new HashMap();
                    prodList2.put("id", productobject.getString("id"));
                    prodList2.put("product_discount_type", productobject.getString("product_discount_type"));
                    prodList2.put("product_discount_amount", productobject.getString("product_discount_amount"));
                    prodList2.put("price", productobject.getString("price"));
                    prodList2.put("final_price", productobject.getString("final_price"));
                    prodList2.put("weight_litr", productobject.getString("weight_litr"));
                    prodList2.put("unit", productobject.getString("unit"));
                    prodList2.put("quantity", productobject.getString("quantity"));
                    prodId.add(productobject.getString("id"));
                    prodWeight.add(productobject.getString("weight_litr") + " " + productobject.getString("unit"));
                    productList.add(prodList2);
                }
                // spinner.setAdapter( new ArrayAdapter<String>( mActivity, android.R.layout.simple_spinner_dropdown_item, prodWeight ) );
                //  ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,prodWeight);
                //  adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //  spinner.setAdapter(adapter);

                spinner.setAdapter(new adapter_spinner(mActivity, R.layout.spinner_layout, prodWeight));

                if (pagerllist.size()==1)
                {
                    circle.setVisibility(View.GONE);
                }

                mPager.setAdapter(new ImageAdapter(mActivity, pagerllist, 0) {
                    @Override
                    protected void viewClick(View view, String str) {
                        GalleryActivity.url = str;
                        GalleryActivity.apiId = getIntent().getStringExtra(AppConstants.productId);
                        GalleryActivity.apiComefrom = getIntent().getStringExtra(AppConstants.from);
                        Log.v("parmatma", String.valueOf(pagerllist));

                        Intent intent = new Intent(getBaseContext(), GalleryActivity.class);
                        intent.putExtra("images", pagerllist.toString());
                        intent.putExtra("grocery", pagerllist.toString());
                        startActivity(intent);
                        /*Intent i=new Intent(mActivity,GalleryActivity.class);
                        i.putExtra("GalleryPager",AppConstants.imageList);
                        startActivity(i);*/
                    }
                });
                circle.setViewPager(mPager);
                final float density = getResources().getDisplayMetrics().density;

                final Handler handler = new Handler();
                final Runnable Update = new Runnable() {
                    public void run() {
                        if (currentPage == pagerllist.size()) {
                            currentPage = 0;
                        }
                        mPager.setCurrentItem(currentPage++, true);
                    }
                };
                Timer swipeTimer = new Timer();
                swipeTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(Update);
                    }
                }, 5000, 3000);
                circle.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                    @Override

                    public void onPageScrolled(int i, float v, int i1) {

                    }

                    @Override

                    public void onPageSelected(int i) {

                    }

                    @Override
                    public void onPageScrollStateChanged(int i) {

                    }
                });
            } else {
                AppUtils.showErrorMessage(tvProductName, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvProductName, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();

        // LinearLayoutManager Linear = new LinearLayoutManager( this);
        // recyclerview.setLayoutManager( Linear );
        //  ProductAdapter productAdapter = new ProductAdapter( prodWeight );
        //  recyclerview.setAdapter( productAdapter );

        //  spinner.setAdapter( new adapter_spinner( getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, prodWeight ) );
        //  spinner.setSelection( 0 );

        //setValues( 0 );
    }

    public void setValues(int position) {
        if (productList.get(position).get("product_discount_type").equals("1")) {
            tvDiscount.setVisibility(View.VISIBLE);
            tvPrice.setVisibility(View.VISIBLE);
            tvPrice.setPaintFlags(tvPrice.getPaintFlags() | 16);
            tvDiscount.setText(getString(R.string.rs) + " " + productList.get(position).get("product_discount_amount") + " off");
        } else if (productList.get(position).get("product_discount_type").equals("2")) {
            tvPrice.setVisibility(View.VISIBLE);
            tvDiscount.setVisibility(View.VISIBLE);
            tvPrice.setPaintFlags(tvPrice.getPaintFlags() | 16);
            tvDiscount.setText(productList.get(position).get("product_discount_amount") + "% off");
        } else if (productList.get(position).get("product_discount_type").equals("123")) {
            tvDiscount.setText("");
            tvDiscount.setVisibility(View.INVISIBLE);
            tvPrice.setVisibility(View.GONE);
        }
        if (productList.get(position).get("price").equalsIgnoreCase(productList.get(position).get("final_price"))) {
            tvFPrice.setText(getString(R.string.rs) + " " + productList.get(position).get("final_price"));
            tvDiscount.setText("");
            tvPrice.setText("");
        } else {
            tvFPrice.setText(getString(R.string.rs) + " " + productList.get(position).get("final_price"));
            tvPrice.setText(getString(R.string.rs) + " " + productList.get(position).get("price") + "");
            tvPrice.setPaintFlags(tvPrice.getPaintFlags() | 16);
        }
        tvUnit.setText(productList.get(position).get("weight_litr") + " " + productList.get(position).get("unit"));
    }

    private class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(() -> {
                if (currentPage == NUM_PAGES) {
                    currentPage = 1;
                }
                mPager.setCurrentItem(currentPage++, true);
                // mViewPager.setCurrentItem((mViewPager.getCurrentItem() + 1) % TopBannerList.size());
                // mViewPager.post(new MyTimerTask());
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivNegative:

                count = (Integer.parseInt(tvQuantityCount.getText().toString())) - 1;
                if (count == 0) {
                    llCartCountBtn.setVisibility(View.GONE);
                    llCartBtn.setVisibility(View.VISIBLE);
                    DeleteCartApi(cartId);
                } else if (count > 0) {
                    // if (count > 1) {
                    // count = count - 1;

                    //tvCount.setText(Integer.toString(count));

                    Vibrator vibe = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                        UpdateCartApi(String.valueOf(count), AppSettings.getString(AppSettings.subiid));
                        tvQuantityCount.setText((count) + "");
                    } else {
                        AppUtils.showErrorMessage(tvDeatils, getString(R.string.errorInternet), mActivity);
                    }
                }
                break;
            case R.id.text_action_bottom1:
                if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else if (AppSettings.getString(AppSettings.verified).equals("0")) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    AddToCartApi();
                } else {
                    AppUtils.showErrorMessage(tvDeatils, getString(R.string.error_connection_message), mActivity);
                }
                return;
            case R.id.llDropdown:
                // spinner.setAdapter( new ArrayAdapter<String>( mActivity, R.layout.spinner_layout, prodWeight ) );
                break;
            case R.id.imageView2:
                count = (Integer.parseInt(tvQuantityCount.getText().toString())) + 1;
                if (count <= productQuantity) {
                    //count = count + 1;
                    //   tvCount.setText(Integer.toString(count));
                    Vibrator vibe = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(100);
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                        UpdateCartApi(String.valueOf(count), AppSettings.getString(AppSettings.subiid));
                        tvQuantityCount.setText((count) + "");
                    } else {
                        AppUtils.showErrorMessage(tvDeatils, getString(R.string.errorInternet), mActivity);
                    }
                } else {
                    AppUtils.showErrorMessage(tvDeatils, getString(R.string.addQuantityError) + " " + productQuantity, mActivity);
                }
                break;

            case R.id.text_action_bottom2:
                if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else if (AppSettings.getString(AppSettings.verified).equals("0")) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    AddToCartApi();
                } else {
                    AppUtils.showErrorMessage(tvDeatils, getString(R.string.error_connection_message), mActivity);
                }
                return;

            case R.id.iv_menu:
                finish();
                return;
            case R.id.llCartBtn:
                if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                    startActivity(new Intent(mActivity, LoginActivity.class));
                } else {
                   /* llCartCountBtn.setVisibility(View.VISIBLE);
                    llCartBtn.setVisibility(View.GONE);*/
                    AddToCartApi();
                }
                return;


            case R.id.image1:
                //finish();
                return;

            case R.id.searchmain:
                startActivity(new Intent(getBaseContext(), SearchActivity.class));
                return;

            case R.id.layout_action3:
                if (!AppSettings.getString(AppSettings.userId).isEmpty()) {
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                        addFavApi(getIntent().getStringExtra(AppConstants.productId));
                    } else {
                        AppUtils.showErrorMessage(tvCount, getString(R.string.errorInternet), mActivity);
                    }
                } else {
                    AppUtils.showErrorMessage(tvCount, "Kindly login first", mActivity);
                }
                return;

            case R.id.ivCart:
                if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else if (AppSettings.getString(AppSettings.verified).contains("0")) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else {
                    startActivity(new Intent(getBaseContext(), CartListActivity.class));
                }
                return;
            default:

                return;
        }
    }

    private void AddToCartApi() {
        AppUtils.showRequestDialog(mActivity);
        Log.v("AddToCartApi", AppUrls.addToCart);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("product_id", productId);
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json.put(AppConstants.result, json_data);
            Log.v("AddToCartApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.addToCart)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvDeatils, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvDeatils, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedata(JSONObject response) {

        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(tvDeatils, String.valueOf(jsonObject.getString("res_msg")), mActivity);
                llCartCountBtn.setVisibility(View.VISIBLE);
                llCartBtn.setVisibility(View.GONE);
                cartId = jsonObject.getString("cartId");
                getCartListApi();

                // startActivity( new Intent( getBaseContext(), CartListActivity.class ) );
            } else {
                startActivity(new Intent(getBaseContext(), CartListActivity.class));
                AppUtils.showErrorMessage(tvDeatils, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvDeatils, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
    }


    private void UpdateCartApi(String quantity, String ProductIds) {
        Log.v("AddToCartApi", AppUrls.updateCart);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("product_id", productId);
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json_data.put("quantity", quantity);
            json.put(AppConstants.result, json_data);
            Log.v("AddToCartApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.updateCart)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Updateparsedata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvDeatils, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvDeatils, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void Updateparsedata(JSONObject response) {
        Log.d("response ", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(tvDeatils, String.valueOf(jsonObject.getString("res_msg")), mActivity);

                // cartList.clear();
                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    getCartListApi();
                } else {
                    AppUtils.showErrorMessage(tvDeatils, getString(R.string.errorInternet), mActivity);
                }
            } else {
                AppUtils.hideDialog();
                AppUtils.showErrorMessage(tvDeatils, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.hideDialog();
            AppUtils.showErrorMessage(tvDeatils, String.valueOf(e), mActivity);
        }

    }

    private void getCartListApi() {
        AppUtils.showRequestDialog(mActivity);
        Log.v("getCartListApi", AppUrls.getCart);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));

            json.put(AppConstants.result, json_data);

            Log.v("getCartListApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.getCart)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsecartlistdata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvDeatils, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvDeatils, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsecartlistdata(JSONObject response) {
        cartList.clear();
        AppSettings.putString(AppSettings.cartCount, "0");
        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                JSONArray productArray = jsonObject.getJSONArray("cart_product");
                AppSettings.putString(AppSettings.cartCount, String.valueOf(productArray.length()));
                if (productArray.length() > 0) {
                    tvCount.setVisibility(View.VISIBLE);
                    tvCount.setText(String.valueOf(productArray.length()));
                    for (int i = 0; i < productArray.length(); i++) {
                        JSONObject cartJson = productArray.getJSONObject(i);
                        HashMap<String, String> hashMap = new HashMap();
                        hashMap.put("outofstock", cartJson.getString("outofstock"));
                        hashMap.put("product_id", cartJson.getString("product_id"));
                        hashMap.put("product_name", cartJson.getString("product_name"));
                        hashMap.put("price", cartJson.getString("price"));
                        hashMap.put("product_image", cartJson.getString("product_image"));
                        hashMap.put("quantity", cartJson.getString("quantity"));
                        hashMap.put("discount_applied_in", cartJson.getString("discount_applied_in"));
                        hashMap.put("final_price", cartJson.getString("final_price"));
                        hashMap.put("product_discount_type", cartJson.getString("product_discount_type"));
                        hashMap.put("product_discount_amount", cartJson.getString("product_discount_amount"));
                        cartList.add(hashMap);
                    }
                } else {
                    tvCount.setVisibility(View.GONE);
                }
            } else {
                tvCount.setVisibility(View.GONE);
                AppSettings.putString(AppSettings.cartCount, "0");
            }
        } catch (Exception e) {
            tvCount.setVisibility(View.GONE);
            AppSettings.putString(AppSettings.cartCount, "0");
        }
        AppUtils.hideDialog();

      /*  if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
            getProfileApi();
        } else {
            AppUtils.showErrorMessage( tvDeatils, getString( R.string.errorInternet ), mActivity );
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();

        int count = Integer.parseInt(AppSettings.getString(AppSettings.cartCount));

        if (count > 0) {
            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(String.valueOf(count));
        } else {
            tvCount.setVisibility(View.GONE);
        }

        if (DatabaseController.checkRecordExist(TableFavourite.favourite, String.valueOf(TableFavourite.favouriteColumn.productId), getIntent().getStringExtra(AppConstants.productId))) {
            ivHeart.setImageResource(R.drawable.ic_heart_red);
        } else {
            ivHeart.setImageResource(R.drawable.ic_heart_grey);
        }
    }

    private void addFavApi(String productId) {

        AppUtils.showRequestDialog(mActivity);

        Log.v("addFavApi", AppUrls.wishList);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json_data.put("product_master_id", productId);

            json.put(AppConstants.result, json_data);

            Log.v("addFavApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.wishList)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseFavdata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvCount, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvCount, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseFavdata(JSONObject response) {

        Log.d("response ", response.toString());

        DatabaseController.removeTable(TableFavourite.favourite);

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                if (jsonObject.getString("status").equals("1")) {
                    AppUtils.showErrorMessage(tvCount, "Product Added to Favourites", mActivity);
                } else if (jsonObject.getString("status").equals("2")) {
                    AppUtils.showErrorMessage(tvCount, "Product Removed from Favourites", mActivity);
                }

                JSONArray wishArray = jsonObject.getJSONArray("wish_list_product");

                for (int i = 0; i < wishArray.length(); i++) {

                    JSONObject productobject = wishArray.getJSONObject(i);

                    ContentValues mContentValues = new ContentValues();

                    mContentValues.put(TableFavourite.favouriteColumn.productId.toString(), productobject.getString("product_id"));

                    DatabaseController.insertData(mContentValues, TableFavourite.favourite);
                }

            } else {
                AppUtils.showErrorMessage(tvCount, "Product Removed from Favourites", mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvCount, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();

        if (DatabaseController.checkRecordExist(TableFavourite.favourite, String.valueOf(TableFavourite.favouriteColumn.productId), getIntent().getStringExtra(AppConstants.productId))) {
            ivHeart.setImageResource(R.drawable.ic_heart_red);
        } else {
            ivHeart.setImageResource(R.drawable.ic_heart_grey);
        }
    }


    public class adapter_spinner extends ArrayAdapter<String> {

        ArrayList<String> data;

        public adapter_spinner(Context context, int textViewResourceId, ArrayList<String> arraySpinner_time) {

            super(context, textViewResourceId, arraySpinner_time);

            this.data = arraySpinner_time;

        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.spinner_textview_rel, parent, false);
            TextView label = row.findViewById(R.id.tv_spinner_name);

            label.setText(data.get(position));

            return row;
        }
    }


    private class ProductAdapter extends RecyclerView.Adapter<FavNameHolder> {
        ArrayList<String> data = new ArrayList<>();

        public ProductAdapter(ArrayList<String> favList) {
            data = favList;
        }

        public FavNameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new FavNameHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.product_weight, parent, false));
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        public void onBindViewHolder(final FavNameHolder holder, final int position) {
            holder.tvWeight.setText(data.get(position));
            holder.rlWeight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    holder.rlWeight.setBackground(getResources().getDrawable(R.drawable.rectangle_corner_selected));
                    productId = prodId.get(position);

                    setValues(position);
                    selectedPosition = position;
                    notifyDataSetChanged();
                    //status=1;
                }
            });


            if (selectedPosition == position) {
                holder.rlWeight.setBackground(getResources().getDrawable(R.drawable.rectangle_corner_selected));
            } else {
                holder.rlWeight.setBackground(getResources().getDrawable(R.drawable.rectangle_corner_unselected));
            }
        }

        public int getItemCount() {
            return data.size();
        }
    }

    private class FavNameHolder extends RecyclerView.ViewHolder {

        RelativeLayout rlWeight;
        TextView tvWeight;


        public FavNameHolder(View itemView) {
            super(itemView);
            rlWeight = itemView.findViewById(R.id.rlWeight);
            tvWeight = itemView.findViewById(R.id.tvWeight);

        }
    }

    private void DeleteCartApi(String cartId) {
        AppUtils.showRequestDialog(mActivity);
        Log.v("AddToCartApi", AppUrls.deleteFromCart);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("cart_id", cartId);
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json.put(AppConstants.result, json_data);
            Log.v("AddToCartApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.deleteFromCart)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedeletedata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvCount, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvCount, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedeletedata(JSONObject response) {
        Log.d("response ", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(tvCount, String.valueOf(jsonObject.getString("res_msg")), mActivity);
                AppUtils.hideDialog();
                //cartList.clear();
                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    getCartListApi();
                } else {
                    AppUtils.showErrorMessage(tvCount, getString(R.string.errorInternet), mActivity);
                }
            } else {
                AppUtils.hideDialog();
                AppUtils.showErrorMessage(tvCount, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.hideDialog();
            AppUtils.showErrorMessage(tvCount, String.valueOf(e), mActivity);
        }
    }


    private void shareProdDetails()
    {
        try {
            StrictMode.ThreadPolicy policy1 = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy1);
            Commonhelper.sop("profile" + ""+pagerllist.get(0).get("name"));
            new GetImageFromUrl().execute(pagerllist.get(0).get("name"));
        } catch (Exception e) {
            Commonhelper.sop("profile" + "erroror");
        }
    }

    public class GetImageFromUrl extends AsyncTask<String, Void, Bitmap> {
        Dialog dialog = Commonhelper.loadDialog(mActivity);
        Bitmap bitmap = null;

        public GetImageFromUrl() {
        }

        @Override
        protected Bitmap doInBackground(String... url) {
            String stringUrl = url[0];
            bitmap = null;
            InputStream inputStream;
            try {
                inputStream = new java.net.URL(stringUrl).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            try {
                bitmapconvert(bitmap);
            } catch (Exception e) {

            }

            dialog.dismiss();
        }
    }

    void bitmapconvert(Bitmap bitmap) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    File file = new File(getExternalCacheDir(), "share.png");
                    FileOutputStream fOut = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.flush();
                    fOut.close();
                    file.setReadable(true, false);

                    String path =  "http://aurget.com/app/share_link.php?productID="+getIntent().getStringExtra(AppConstants.productId)+"&come_from="+
                            getIntent().getStringExtra(AppConstants.from)+"&user_id="+AppSettings.getString(AppSettings.userId);

                    Commonhelper.shareApps(mActivity, null,tvDeatils.getText().toString()+"\n"+path);
                  //  Commonhelper.shareApps(mActivity,file, productNmeTv.getText().toString()+"\n"+"http://aurget.com/app/share_link.php?productID="+product_id);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static int currentPage = 0;
    public static int NUM_PAGES = 0;
    public static Timer timer;
    public static int VB_PAGES;
    private String shareImage="";

}


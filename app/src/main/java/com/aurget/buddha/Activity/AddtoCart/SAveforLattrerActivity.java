package com.aurget.buddha.Activity.AddtoCart;

import android.content.Intent;
import android.graphics.Paint;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.Activity.Sqlite.DBHelper;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SAveforLattrerActivity extends AppCompatActivity {

    RecyclerView addtoCartRv;
    TextView titleTv, save_for_latterCount;
    ArrayList<JSONObject> jsonObjectal_ = new ArrayList<>();
    private DBHelper mydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_savefor_lattrer);
        mydb = new DBHelper(this);
        addtoCartRv = findViewById(R.id.addtoCartRv);
        addtoCartRv.setVisibility(View.VISIBLE);
        titleTv = findViewById(R.id.title);
        save_for_latterCount = findViewById(R.id.save_for_latterCount);
        titleTv.setText("Save for Letter List");

        LinearLayoutManager horizontalLayoutManager5 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        addtoCartRv.setLayoutManager(horizontalLayoutManager5);

        jsonObjectal_.clear();
        jsonObjectal_ = AddCartListActivity.jsonObjectal;
        addtoCartRv.setAdapter(new MyRecyclerAdapter());

        findViewById(R.id.bkImg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SAveforLattrerActivity.this, AddCartListActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

        public MyRecyclerAdapter() {
        }

        @Override
        public MyRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_to_crt_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapter.ViewHolder holder, int position) {
            try {
                if (mydb.isExist(jsonObjectal_.get(position).optString("cart_id"))) {
                    //if (jsonObjectal_.get(position).optString("move_to_cart").equals("1")) {
                    holder.priceTv.setText("₹" + jsonObjectal_.get(position).optString("sale_price"));
                    holder.priceTv2.setText("₹" + jsonObjectal_.get(position).optString("cut_amount"));
                    holder.ipTv.setText("Product IP: " + jsonObjectal_.get(position).optString("ip"));
                    holder.productNmeTv.setText("Product Name: " + jsonObjectal_.get(position).optString("title"));
                    holder.qtyEt.setText("" + jsonObjectal_.get(position).optString("qtr"));
                    holder.sizeTv.setText("" + jsonObjectal_.get(position).optString("product_size"));

                    if (Integer.parseInt(jsonObjectal_.get(position).optString("discount")) > 0) {

                        Commonhelper.isDiscountRate(jsonObjectal_, position, holder.percentOffTv);

                    /*if (jsonObjectal_.get(position).optString("discount_type").equals("percent")) {
                        holder.percentOffTv.setText("" + jsonObjectal_.get(position).optString("discount") + "% Discount");
                    }
                    else
                    {
                        holder.percentOffTv.setText("₹" + jsonObjectal_.get(position).optString("discount") + " Discount");
                    }*/
                        //priceTv2.setText("₹" + roudOff(camt));
                        holder.priceTv2.setText("₹" + jsonObjectal_.get(position).optString("cut_amount"));
                        //priceTv.setText("₹" + amt);
                        holder.priceTv.setText("₹" + jsonObjectal_.get(position).optString("sale_price"));
                    } else {
                        //priceTv.setText("₹" + roudOff(sp));
                        holder.priceTv.setText("₹" + jsonObjectal_.get(position).optString("sale_price"));
                        holder.percentOffTv.setVisibility(View.GONE);
                        holder.priceTv2.setVisibility(View.GONE);
                    }

                    //rting.setRating(Float.parseFloat(jsonObjectal_.get(position).optString("rating_total")));
                    //holder.rtingTv.setText(""+Float.parseFloat(jsonObjectal_.get(position).optString("rating_total")));

                    int i = position + 1;
                    save_for_latterCount.setText("Save for Letter (" + i + ")");

                    Picasso.with(SAveforLattrerActivity.this).load(InterfaceClass.imgPth2 + jsonObjectal_.get(position).optString("product_id") + "_" + 1 + "_thumb.jpg").into(holder.img);
                    Picasso.with(SAveforLattrerActivity.this).load(jsonObjectal_.get(position).optString("product_color")).into(holder.colorimg);
                    holder.crt.setVisibility(View.VISIBLE);
                } else {
                    holder.crt.setVisibility(View.GONE);
                }

            } catch (Exception e) {
            }
        }

        @Override
        public int getItemCount() {
            return jsonObjectal_.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView save_for_latterBtn, deleteBtn, minusTv, plusTv, amountTv, sizeTv;
            TextView priceTv, priceTv2, percentOffTv, ipTv, productNmeTv, soldTv, rtingTv;

            private ImageView img, colorimg;
            private CardView crt;
            EditText qtyEt;

            public ViewHolder(final View itemView) {
                super(itemView);

                save_for_latterBtn = itemView.findViewById(R.id.save_for_latterBtn);
                save_for_latterBtn.setText("Move to Cart");
                deleteBtn = itemView.findViewById(R.id.deleteBtn);
                crt = itemView.findViewById(R.id.crt);
                qtyEt = itemView.findViewById(R.id.qtyEt);
                img = itemView.findViewById(R.id.img);
                colorimg = itemView.findViewById(R.id.colorimg);
                sizeTv = itemView.findViewById(R.id.sizeTv);
                itemView.findViewById(R.id.qtyLl).setVisibility(View.GONE);

                rtingTv = itemView.findViewById(R.id.rtingTv);
                priceTv = itemView.findViewById(R.id.priceTv);
                priceTv2 = itemView.findViewById(R.id.priceTv2);
                priceTv2.setPaintFlags(priceTv2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                percentOffTv = itemView.findViewById(R.id.percentOffTv);
                ipTv = itemView.findViewById(R.id.ipTv);
                productNmeTv = itemView.findViewById(R.id.productNmeTv);
                soldTv = itemView.findViewById(R.id.soldTv);
                //rting = itemView.findViewById(R.id.rting);
                minusTv = itemView.findViewById(R.id.minusTv);
                plusTv = itemView.findViewById(R.id.plusTv);
                amountTv = itemView.findViewById(R.id.amountTv);

                deleteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        delete_cart(jsonObjectal_.get(getAdapterPosition()).optString("product_id"));

                        if (mydb.isExist(jsonObjectal_.get(getAdapterPosition()).optString("cart_id"))) {
                            mydb.deleteContact(Integer.valueOf(jsonObjectal_.get(getAdapterPosition()).optString("cart_id")));
                        }
                        if (mydb.numberOfRows() < 1) {
                            Intent intent = new Intent(SAveforLattrerActivity.this, AddCartListActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        jsonObjectal_.remove(getAdapterPosition());
                        notifyDataSetChanged();
                    }
                });

                save_for_latterBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            jsonObjectal_.get(getAdapterPosition()).put("move_to_cart", "1");
                            if (mydb.isExist(jsonObjectal_.get(getAdapterPosition()).optString("cart_id"))) {
                                mydb.deleteContact(Integer.valueOf(jsonObjectal_.get(getAdapterPosition()).optString("cart_id")));
                            }

                            if (mydb.numberOfRows() < 1) {
                                Intent intent = new Intent(SAveforLattrerActivity.this, AddCartListActivity.class);
                                intent.putExtra("SAveforLattrerActivity","SAveforLattrerActivity");
                                startActivity(intent);
                                finish();
                            }
                            notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    }

/*
    public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

        public MyRecyclerAdapter() {
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.save_for_latter_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            if (mydb.isExist(jsonObjectal_.get(position).optString("cart_id"))) {
                holder.crt.setVisibility(View.VISIBLE);
            } else {
                holder.crt.setVisibility(View.GONE);
            }
        }

        @Override
        public int getItemCount() {
            return jsonObjectal_.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView move_to_cartBtb, deleteBtn, priceTv;
            private ImageView img;
            CardView crt;

            public ViewHolder(final View itemView) {
                super(itemView);

                move_to_cartBtb = itemView.findViewById(R.id.move_to_cartBtb);
                deleteBtn = itemView.findViewById(R.id.deleteBtn);
                crt = itemView.findViewById(R.id.crt);

                move_to_cartBtb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            jsonObjectal_.get(getAdapterPosition()).put("move_to_cart", "1");
                            if (mydb.isExist(jsonObjectal_.get(getAdapterPosition()).optString("cart_id"))) {
                                mydb.deleteContact(Integer.valueOf(jsonObjectal_.get(getAdapterPosition()).optString("cart_id")));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        notifyDataSetChanged();
                    }
                });

                deleteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        delete_cart(jsonObjectal_.get(getAdapterPosition()).optString("product_id"));

                        if (mydb.isExist(jsonObjectal_.get(getAdapterPosition()).optString("cart_id"))) {
                            mydb.deleteContact(Integer.valueOf(jsonObjectal_.get(getAdapterPosition()).optString("cart_id")));
                        }
                        jsonObjectal_.remove(getAdapterPosition());
                        notifyDataSetChanged();
                    }
                });
            }
        }
    }
*/

    private void delete_cart(String productID) {

        new AbstrctClss(SAveforLattrerActivity.this, ConstntApi.delete_cart(SAveforLattrerActivity.this, productID), "p", true) {
            @Override
            public void responce(String s) {

                try {
                    String Res = s;
                    Log.d("dsdss", Res);
                    JSONArray jsonArray = new JSONArray(Res);
                    Commonhelper.showToastLong(SAveforLattrerActivity.this, jsonArray.optJSONObject(0).optString("Status"));
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SAveforLattrerActivity.this, AddCartListActivity.class);
        startActivity(intent);
    }
}

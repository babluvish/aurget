package com.aurget.buddha.Activity.Utils;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.R;

import java.util.ArrayList;
import java.util.Calendar;

public abstract class SearchDate {

    public abstract String fromDtToDt(String frmDate, String toDt);

    Activity activity;

    protected SearchDate(Activity activity) {
        this.activity = activity;
    }

//    public void dateRangeDialog(TextView titleTv)
//    {
//        Dialog dialog = new Dialog(activity);
//        dialog.setContentView(R.layout.date_rnge);
//        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//
//        final TextView strtdt = dialog.findViewById(R.id.strtdt);
//        final TextView endDte = dialog.findViewById(R.id.endDte);
//        final LinearLayout rdoBtn = dialog.findViewById(R.id.rdoBtn);
//        final LinearLayout rdoBtn2 = dialog.findViewById(R.id.rdoBtn2);
//        final Button viewBtn = dialog.findViewById(R.id.viewBtn);
//
//        if (titleTv.getText().toString().equalsIgnoreCase(activity.getString(R.string.direct_incentive_item_detild))) {
//            rdoBtn.setVisibility(View.VISIBLE);
//            rdoBtn2.setVisibility(View.GONE);
//        } else if (titleTv.getText().toString().equalsIgnoreCase(activity.getString(R.string.differential_incentive_Statement))) {
//            rdoBtn.setVisibility(View.VISIBLE);
//            rdoBtn2.setVisibility(View.VISIBLE);
//        } else if (titleTv.getText().toString().equalsIgnoreCase(activity.getString(R.string.totl_incentive_Statement))) {
//            rdoBtn.setVisibility(View.VISIBLE);
//            rdoBtn2.setVisibility(View.VISIBLE);
//        } else {
//            rdoBtn.setVisibility(View.VISIBLE);
//            rdoBtn2.setVisibility(View.VISIBLE);
//        }
//
//        viewBtn.setText("GO");
//
//        strtdt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                piecker(strtdt);
//                fromDt(strtdt.getText().toString());
//            }
//        });
//
//        endDte.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                piecker(endDte);
//                toDt(endDte.getText().toString());
//            }
//        });
//
//        viewBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
//
//        dialog.show();
//    }

    public void dateRangeDialog() {
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.date_rnge);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        final TextView strtdt = dialog.findViewById(R.id.strtdt);
        final TextView endDte = dialog.findViewById(R.id.endDte);
        final LinearLayout rdoBtn = dialog.findViewById(R.id.rdoBtn);
        final LinearLayout rdoBtn2 = dialog.findViewById(R.id.rdoBtn2);
        final Button viewBtn = dialog.findViewById(R.id.viewBtn);

        viewBtn.setText("GO");

        strtdt.setOnClickListener(view -> piecker(strtdt));
        endDte.setOnClickListener(view -> piecker(endDte));

        viewBtn.setOnClickListener(view -> {
            if (strtdt.getText().toString().length() == 0) {
                Commonhelper.showToastLong(activity, "Please select start date");
            } else if (endDte.getText().toString().length() == 0) {
                Commonhelper.showToastLong(activity, "Please select end date");
            } else {
                fromDtToDt(strtdt.getText().toString(), endDte.getText().toString());
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    private DatePickerDialog piecker(final TextView editText) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        editText.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        //editText.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);

        if (!datePickerDialog.isShowing())
            datePickerDialog.show();

        return datePickerDialog;
    }

    public DatePickerDialog piecker(final EditText editText) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        editText.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

        if (!datePickerDialog.isShowing())
            datePickerDialog.show();

        return datePickerDialog;
    }

    public void monthNameDialog() {
        Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.month_list);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        RecyclerView recycleViewmonth = dialog.findViewById(R.id.recycleViewmonth);
        final RadioButton rdo1 = dialog.findViewById(R.id.rdo1);
        final RadioButton rdo2 = dialog.findViewById(R.id.rdo2);

        rdo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                rdo2.setChecked(false);
            }
        });

        rdo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                rdo1.setChecked(false);
            }
        });

        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recycleViewmonth.setLayoutManager(horizontalLayoutManager);
        recycleViewmonth.setItemAnimator(new DefaultItemAnimator());


        ArrayList<String> jsonObjects = new ArrayList<>();

        jsonObjects.add("January");
        jsonObjects.add("Fabruary");
        jsonObjects.add("March");
        jsonObjects.add("April");
        jsonObjects.add("May");
        jsonObjects.add("June");
        jsonObjects.add("July");
        jsonObjects.add("August");
        jsonObjects.add("September");
        jsonObjects.add("October");
        jsonObjects.add("November");
        jsonObjects.add("December");

        recycleViewmonth.setAdapter(new MyRecyclerAdapterMonth(jsonObjects));
        dialog.show();

    }

    public static class MyRecyclerAdapterMonth extends RecyclerView.Adapter<MyRecyclerAdapterMonth.ViewHolder> {

        private ArrayList<String> jsonObjectal;
        int lastSelectedPosition = -1;

        public MyRecyclerAdapterMonth(ArrayList<String> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapterMonth.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.month_list_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapterMonth.ViewHolder holder, int position) {

            holder.monthTv.setText(jsonObjectal.get(position));

            holder.rdo.setChecked(lastSelectedPosition == position);
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView monthTv;
            RadioButton rdo;

            public ViewHolder(View itemView) {
                super(itemView);

                monthTv = itemView.findViewById(R.id.monthTv);
                rdo = itemView.findViewById(R.id.rdo);


                rdo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        lastSelectedPosition = getAdapterPosition();
                        notifyDataSetChanged();
                    }
                });

            }
        }
    }

}

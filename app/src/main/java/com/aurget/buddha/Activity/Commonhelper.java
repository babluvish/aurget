package com.aurget.buddha.Activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.provider.ContactsContract;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Base64;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aurget.buddha.Activity.Utils.CustomPreference;
import com.aurget.buddha.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;


public class Commonhelper {


    public static int READ_CONTACT = 1;

    public static final String spname = "sp";
    public static String finalStr;
    public static String placeValue = "0.00";

    public Context _ctx;
    private SharedPreferences Shpref;
    private SharedPreferences.Editor editShpref;

    public Commonhelper(Context context) {
        this._ctx = context;
        Shpref = _ctx.getSharedPreferences(spname,
                _ctx.MODE_PRIVATE);
        editShpref = Shpref.edit();
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static boolean compareImage(ImageView imageButton, AppCompatActivity activity, int id) {
        Drawable drawable = imageButton.getDrawable();

        if (drawable.getConstantState().equals(activity.getResources().getDrawable(id).getConstantState())) {
            return true;
        }

        return false;
    }

    public static boolean isNull(String str) {
        return str == null ? true : false;
    }

    public static String generateRandomNo(Activity activity) {
        String id = String.format("%04d", new Random().nextInt(10000));
        return id;
    }

    public static int generateRandomNoPwd(Activity activity) {
        int aNumber = 0;
        aNumber = (int) ((Math.random() * 3000000) + 20000000);
        // Toast.makeText(activity, "Referal code is=" + aNumber, Toast.LENGTH_LONG).show();
        return aNumber;
    }

    public static String monthInWord(String month) {
        /*Calendar cal=Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
        int monthnum=month-1;
        cal.set(Calendar.MONTH,monthnum);
        String month_name = month_date.format(cal.getTime());*/

        if (month.equals("1") || month.equals("01")) {
            return "January";
        } else if (month.equals("2") || month.equals("02")) {
            return "Fabruary";
        } else if (month.equals("3") || month.equals("03")) {
            return "March";
        } else if (month.equals("4") || month.equals("04")) {
            return "April";
        } else if (month.equals("5") || month.equals("05")) {
            return "May";
        } else if (month.equals("6") || month.equals("06")) {
            return "June";
        } else if (month.equals("7") || month.equals("07")) {
            return "July";
        } else if (month.equals("8") || month.equals("08")) {
            return "August";
        } else if (month.equals("9") || month.equals("09")) {
            return "September";
        } else if (month.equals("10")) {
            return "October";
        } else if (month.equals("11")) {
            return "November";
        } else {
            return "December";
        }
    }

    public static void hyperLink(TextView textView, String txt, String link) {
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        String text = "<a href='" + link + "'>" + txt + "</a>";
        textView.setText(Html.fromHtml(text));
    }

    public void showSoftKeyboard(final EditText editText) {
        try {
            editText.requestFocus();
            editText.postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            InputMethodManager keyboard = (InputMethodManager) _ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
                            keyboard.showSoftInput(editText, 0);
                        }
                    }
                    , 200);
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String showDateTime(String str) {
        Calendar mcurrentDate = Calendar.getInstance();
        String s;

        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH) + 1;
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        if (str.equals("dt")) {
            SimpleDateFormat sdf = new SimpleDateFormat("h:mm a");
            String currentTime = sdf.format(new Date());
            s = +mYear + ":" + mMonth + ":" + mDay + "," + currentTime;
        } else {
            s = +mDay + "/" + mMonth + "/" + mYear + "";
        }

        return s;
    }

    public static void notificationShow(Context _ctx1, Class context, int icon, String title, String text) {
        //Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Uri soundUri = Uri.parse("android.resource://" + _ctx1.getPackageName() + "/raw/ofo");
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(_ctx1)
                        .setSmallIcon(icon)
                        .setContentTitle(title)
                        .setContentText(text).setSound(soundUri);

        Intent notificationIntent = new Intent(_ctx1, context);
        PendingIntent contentIntent = PendingIntent.getActivity(_ctx1, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        // Add as notification
        NotificationManager manager = (NotificationManager) _ctx1.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }


    public static String showTimeStampDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String currentDateandTime = sdf.format(new Date());
        return currentDateandTime;
    }

    public static String showTime() {
        SimpleDateFormat sdf1 = new SimpleDateFormat("h:mm a");
        String currentTime1 = sdf1.format(new Date());
        return currentTime1;
    }

    public static void showToastShort(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showToastLong(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void sop(String msg) {
        System.out.println(msg);
    }


//    public void shwTsnackBarErro(String sms,Activity activity)
//    {
//        TSnackbar snackbar = TSnackbar.make(activity.findViewById(android.R.id.content), sms, TSnackbar.LENGTH_LONG);
//        snackbar.setActionTextColor(Color.WHITE);
//       // snackbar.setIconLeft(img, 24); //Size in dp - 24 is great!
//        snackbar.setIconRight(R.drawable.error_icon, 24); //Resize to bigger dp
//        //snackbar.setIconPadding(8);
//        snackbar.setMaxWidth(3000); //if you want fullsize on tablets
//        View snackbarView = snackbar.getView();
//        snackbarView.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
//        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
//        textView.setTextColor(Color.BLACK);
//        snackbar.show();
//    }

    //public void shwTsnackBarSuccess(String sms,Activity activity)
//    {
//        TSnackbar snackbar = TSnackbar.make(activity.findViewById(android.R.id.content), sms, TSnackbar.LENGTH_LONG);
//        snackbar.setActionTextColor(Color.WHITE);
//       // snackbar.setIconLeft(img, 24); //Size in dp - 24 is great!
//        snackbar.setIconRight(R.drawable.ic_check_black_24dp, 24); //Resize to bigger dp
//        //snackbar.setIconPadding(8);
//        snackbar.setMaxWidth(3000); //if you want fullsize on tablets
//        View snackbarView = snackbar.getView();
//        snackbarView.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
//        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
//        textView.setTextColor(Color.BLACK);
//        snackbar.show();
//    }

    public void shwTsnackBarSuccess(RelativeLayout relativeLayout, String sms) {
        Snackbar snackbar = Snackbar.make(relativeLayout, sms, Snackbar.LENGTH_LONG);

//        final Snackbar snackbar = Snackbar
//                .make(relativeLayout, sms, Snackbar.LENGTH_LONG)
//                .setAction("OK", new View.OnClickListener()
//                {
//                    @Override
//                    public void onClick(View view)
//                    {
//
//                    }
//                });

        // Changing message text color

        snackbar.setActionTextColor(Color.BLACK);

        // Changing snackbar background color
        ViewGroup group = (ViewGroup) snackbar.getView();
        group.setBackgroundColor(ContextCompat.getColor(_ctx, R.color.colorPrimary));

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(Color.BLACK);

        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) sbView.getLayoutParams();
        params.gravity = Gravity.CENTER;
        params.setMargins(10, 10, 10, 10);
        sbView.setLayoutParams(params);
        snackbar.show();
    }


    public void showMesseage(String str) {
        Toast.makeText(_ctx, str, Toast.LENGTH_SHORT).show();
    }

    public void showMesseage(String str, EditText editText) {
        Toast.makeText(_ctx, str, Toast.LENGTH_SHORT).show();
        editText.requestFocus();
    }

    public boolean setSharedPreferences(String obj, String val) {
        try {
            editShpref.putString(obj, val);
            editShpref.commit();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public String getSharedPreferences(String obj, String defval) {
        String a = Shpref.getString(obj, defval);
        return a;

    }

    public boolean ClearSharedPreference() {
        try {
            editShpref.clear();
            editShpref.commit();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public final void callintent(Context context, Class newc) {
        Intent i = new Intent(context, newc);
        context.startActivity(i);
    }

    public boolean isNullOrEmpaty(String param) {
        if (isNull(param) || param.trim().length() == 0) {
            return true;
        }
        return false;
    }

    public JSONArray CnvrtToJsonArray(String stringjson) {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(stringjson);
        } catch (JSONException e) {
            jsonArray = null;
        }
        return jsonArray;
    }


    public static double discountAmt(double sp, double dis) {
        double totamt = sp * dis / 100;
        double gt = sp - totamt;
        return gt;
    }

    public static long roudOff(double val) {
        return Math.round(val);
    }


    public boolean isStoragePermissionGranted(Activity _activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (_activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                //     Log.v(TAG, "Permission is granted");
                return true;
            } else {

                //Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(_activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            // Log.v(TAG,"Permission is granted");
            return true;
        }
    }

    public static void countDown(final TextView Str) {
        new CountDownTimer(60000, 1000) {
            public void onTick(long millisUntilFinished) {
                Str.setText("Please wait... " + millisUntilFinished / 1000 + " " + "second");
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                Str.setText("Resend OTP");
            }
        }.start();
    }


    public boolean showAlertDialogBox(String title, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(_ctx);
        builder.setTitle(title);
        builder.setMessage(msg)
                .setPositiveButton("Send now", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        b = true;
                    }
                }).setNegativeButton("Not now", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                b = false;
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

        return b;
    }

    public void alertDialogBoxShow(String title, String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(_ctx);
        builder.setTitle(title);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static int returnParsedJsonObject(String result) {

        JSONObject resultObject = null;

        int returnedResult = 0;

        try {

            resultObject = new JSONObject(result);

            returnedResult = resultObject.getInt("success");

        } catch (JSONException e) {

            e.printStackTrace();

        }

        return returnedResult;

    }

    public static StringBuilder inputStreamToString(InputStream is) {

        String rLine = "";

        StringBuilder answer = new StringBuilder();

        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        try {

            while ((rLine = br.readLine()) != null) {

                answer.append(rLine);

            }

        } catch (IOException e) {

// TODO Auto-generated catch block

            e.printStackTrace();

        }

        return answer;

    }


    public boolean checkNetworka() {
        ConnectivityManager cm = (ConnectivityManager) _ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm != null) {
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            return isConnected;
        }
        return false;
    }


    public void getContact(ListView listView) {

        String phoneNumber = null;
        strings = new ArrayList<String>();

        Cursor phones = _ctx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);

        while (phones.moveToNext()) {
            //String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            //Toast.makeText(getApplicationContext(),name+" "+phoneNumber, Toast.LENGTH_LONG).show();
            strings.add(phoneNumber);
        }

        ArrayAdapter arrayAdapter = new ArrayAdapter(_ctx, android.R.layout.simple_list_item_1, android.R.id.text1, strings);
        listView.setAdapter(arrayAdapter);
        phones.close();
    }


    /*========================getStringImage=================================*/
    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }


    public static void progressDialog(Activity activity) {
        if (progressDialog != null) {

        } else {
            progressDialog = new ProgressDialog(activity);
        }

        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);

        try {
            progressDialog.show();
        } catch (WindowManager.BadTokenException ex) {
            ex.printStackTrace();
        }
    }

    public static Dialog loadDialog(Activity activity) {
        Dialog dialog = null;
        try {
            dialog = new Dialog(activity);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            dialog.setContentView(R.layout.avl_loader_layout);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            //Commonhelper.showToastLong(activity,"google baba");
        }
        return dialog;
    }

//    public static void loadDialog(Activity activity)
//    {
//        progressDialog(activity);
//    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.findViewById(android.R.id.content);
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static String loginID(Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences("loginSp", 0);
        String loginId = sharedPreferences.getString("loginId", "");
        return loginId;
    }


    @TargetApi(Build.VERSION_CODES.M)
    public byte[] profileImageToConvertIntoByte(Bitmap b) {

        ByteArrayOutputStream bos = null;

        if (b != null) {

            bos = new ByteArrayOutputStream();
            b.compress(Bitmap.CompressFormat.PNG, 0, bos);
        }

        return bos.toByteArray();
    }

    public static byte[] convertImageUriToByte(Uri uri, Activity activity) {
        byte[] data = null;
        try {
            ContentResolver cr = activity.getContentResolver();
            InputStream inputStream = cr.openInputStream(uri);
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            data = baos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return data;
    }


    public void callPhone(String phoneNO) {
        if (ActivityCompat.checkSelfPermission(_ctx, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phoneNO));//change the number
        _ctx.startActivity(callIntent);
    }

    public void sendSmsPhone(String phoneN, String sms) {
        Uri sms_uri = Uri.parse("smsto:" + phoneN);
        Intent sms_intent = new Intent(Intent.ACTION_SENDTO, sms_uri);
        sms_intent.putExtra("sms_body", sms);
        _ctx.startActivity(sms_intent);
    }

    public static void shareLink(Activity activity, String shareLink) {
        try {
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareLink);
            activity.startActivity(Intent.createChooser(sharingIntent, "Share  the application via"));
        } catch (Exception e) {
            showToastShort(activity, "Not Found");
        }
    }


    public static void dismiss() {
        if (dialog != null)
            dialog.dismiss();
    }


    public static int getScreenWidth(Activity activity) {

        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public static String fileExe(Uri uri) {
        String fileExt = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
        return "." + fileExt;
    }

    public static String bitMap(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return imageString;
    }

    public static void phoneCall(Activity activity) {
        if (ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:9662101102"));
            activity.startActivity(callIntent);
        } else {
            Toast.makeText(activity, "You don't assign permission.", Toast.LENGTH_SHORT).show();
        }
    }

    public static void phoneCall(Activity activity, String no) {
        if (ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + no));
            activity.startActivity(callIntent);
        } else {
            Toast.makeText(activity, "You don't assign permission.", Toast.LENGTH_SHORT).show();
        }
    }

    public static void openBrowser(Activity activity, String link) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        activity.startActivity(browserIntent);
    }

    public static void changeTabsFont(Activity activity, TabLayout tableLayout) {
        /*ViewGroup childTabLayout = (ViewGroup) tableLayout.getChildAt(0);
        for (int i = 0; i < childTabLayout.getChildCount(); i++) {
            ViewGroup viewTab = (ViewGroup) childTabLayout.getChildAt(i);
            for (int j = 0; j < viewTab.getChildCount(); j++) {
                View tabTextView = viewTab.getChildAt(j);
                if (tabTextView instanceof TextView) {
                    Typeface typeface = Typeface.createFromAsset(activity.getAssets(), Constnt.font);
                    ((TextView) tabTextView).setTypeface(typeface);
                    ((TextView) tabTextView).setTextSize(TypedValue.COMPLEX_UNIT_DIP, activity.getResources().getDimension(R.dimen.sp15));
                }
            }
        }*/
    }

    public static void openWhatsApp(Activity activity, String no) {
        String contact = "+91 " + no; // use country code with your phone number
        String url = "https://api.whatsapp.com/send?phone=" + contact;
        try {
            PackageManager pm = activity.getPackageManager();
            pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            activity.startActivity(i);
            //Toast.makeText(activity, ""+contact+"="+PackageManager.GET_ACTIVITIES, Toast.LENGTH_SHORT).show();
        } catch (PackageManager.NameNotFoundException e) {
            try {
                PackageManager pm = activity.getPackageManager();
                pm.getPackageInfo("com.whatsapp.w4b", PackageManager.GET_ACTIVITIES);
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                activity.startActivity(i);
            } catch (Exception e1) {
                Toast.makeText(activity, "Whatsapp app not installed in your phone", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static void openWhatsApp(Activity activity) {
        String contact = "+91 9662101102"; // use country code with your phone number
        String url = "https://api.whatsapp.com/send?phone=" + contact;
        try {
            PackageManager pm = activity.getPackageManager();
            pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            activity.startActivity(i);
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(activity, "Whatsapp app not installed in your phone", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public static void openWhatsApp_(Activity activity, String link) {
        //String contact = "+91 9662101102"; // use country code with your phone number
        String url = link;
        try {
            PackageManager pm = activity.getPackageManager();
            pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            activity.startActivity(i);
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(activity, "Whatsapp app not installed in your phone", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public static void isDiscountRate(JSONObject jsonObject, TextView textView) {
        if (jsonObject.optString("discount_type").equals("percent")) {
            textView.setText("" + jsonObject.optString("discount") + "% Discount");
        } else {
            textView.setText("₹" + jsonObject.optString("discount") + " Discount");
        }
    }

    public static void isDiscountRate(ArrayList<JSONObject> jsonObjectal, int position, TextView textView) {
        if (jsonObjectal.get(position).optString("discount_type").equals("percent")) {
            textView.setText("" + jsonObjectal.get(position).optString("discount") + "% Discount");
        } else {
            textView.setText("₹" + jsonObjectal.get(position).optString("discount") + " Discount");
        }
    }

    public static String roundOff(Double dbl) {
        DecimalFormat precision = new DecimalFormat("0.000");
        return precision.format(dbl);
    }

    public static String roundOff_(Double dbl) {
        DecimalFormat precision = new DecimalFormat("0.00");
        return precision.format(dbl);
    }

    public static String roundOff(Double dbl, String place) {
        DecimalFormat precision = new DecimalFormat(place);
        return precision.format(dbl);
    }

    public static void picasso(Activity _ctx, String place, ImageView imageView) {
        Picasso.with(_ctx).load(InterfaceClass.imgPthCtegory+place+".jpg").into(imageView);
        Commonhelper.sop("picasso" + InterfaceClass.imgPthCtegory + place + ".jpg");
        /*Picasso.with(_ctx).load(InterfaceClass.imgPthCtegory + place + ".jpg").networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(imageView);*/
    }

    public static void picasso2(Activity _ctx, String place, ImageView imageView) {
        Picasso.with(_ctx).load(place).into(imageView);
        /*Picasso.with(_ctx).load(place).networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(imageView);*/
    }

    public static void picasso_(Activity _ctx, String place, ImageView imageView) {
        Picasso.with(_ctx).load(place)
                .into(imageView);
    }

    public static void picasso_(Activity _ctx, String place, ImageView imageView, int placeholder) {
        Picasso.with(_ctx).load(place).placeholder(placeholder)
                .into(imageView);
    }

    public static Bitmap getBitmapFromURL(String src) {
        StrictMode.ThreadPolicy policy1 = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy1);
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    public static void openPlayStore(Activity _ctx) {
        final String appPackageName = _ctx.getPackageName(); // getPackageName() from Context or Activity object
        try {
            //_ctx.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            _ctx.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            _ctx.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public static void shareAndEarn(Activity activity, int pos, String imageLink, String contant) {
        JSONObject jsonObject = null;
        String link = null;
        String sponsor_id = CustomPreference.readString(activity, CustomPreference.e_id, "");
        String mobNo = CustomPreference.readString(activity, CustomPreference.mobileNO, "");
        String link_, link__;

        link_ = contant;
        link = link_ + "\n" + "http://aurget.com/invite/cmF6/" + sponsor_id + "/" + mobNo;
        link__ = "http://aurget.com/invite/cmF6/" + sponsor_id + "/" + mobNo;


        dialog = Commonhelper.loadDialog(activity);

        if (pos == 1) {
            //shareApp(activity, "Welcome to AurGet \\n https://play.google.com/store/apps/details?id=com.aurget.buddha");
            Commonhelper.sop("immmgg" + imageLink);
            try {
                url = new URL(imageLink);
                //url = new URL("http://aurget.com/uploads/product_image/product_1_1_thumb.jpg");
                try {
                    image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    shareBitmap(image, link, activity);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else {

            String imageLink2 = null, name = null;
            try {
                jsonObject = new JSONObject(imageLink);
                imageLink2 = jsonObject.optString("img");
                name = jsonObject.optString("name");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //shareApp(activity, link);
            try {
                Commonhelper.sop("immmgg" + imageLink2);
                url = new URL(imageLink2);
                image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                shareBitmap(image, name + "\n" + link__, activity);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void shareApp(Activity _ctx, File file, String contant) {
        String sponsor_id = CustomPreference.readString(_ctx, CustomPreference.e_id, "");
        String mobNo = CustomPreference.readString(_ctx, CustomPreference.mobileNO, "");
        String shareText = contant + "\n" + "http://aurget.com/invite/cmF6/" + sponsor_id + "/" + mobNo;
        final Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Intent.EXTRA_TEXT, shareText);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            intent.putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(_ctx, "com.aurget.provider", file));
        } else {
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        }

        intent.setType("image/png");
        _ctx.startActivity(Intent.createChooser(intent, "Share image via"));
    }

    public static void shareApps(Activity _ctx, File file, String contant) {
        String sponsor_id = CustomPreference.readString(_ctx, CustomPreference.e_id, "");
        String mobNo = CustomPreference.readString(_ctx, CustomPreference.mobileNO, "");
        String shareText = contant;
        final Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Intent.EXTRA_TEXT, shareText);

        /*if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            intent.putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(_ctx, "com.aurget.provider", file));
        } else {
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        }*/

        //intent.setType("image/png");
        intent.setType("text/plain");
        _ctx.startActivity(Intent.createChooser(intent, "Share image via"));
    }

    public static void shareBitmap(Bitmap bitmap, String shareText, Activity _ctx) {
        new Thread(() -> _ctx.runOnUiThread(() -> {
            try {
                File file = new File(_ctx.getExternalCacheDir(), "share.png");
                FileOutputStream fOut = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                fOut.flush();
                fOut.close();
                file.setReadable(true, false);

                final Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Intent.EXTRA_TEXT, shareText);
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                    intent.putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(_ctx, "com.aurget.provider", file));
                } else {
                    intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
                }

                intent.setType("image/png");
                _ctx.startActivity(Intent.createChooser(intent, "Share image via"));
                dialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        })).start();
    }

    public static File bitmapconvert(Bitmap bitmap, Activity _ctx) {
        new Thread(() -> _ctx.runOnUiThread(() -> {
            try {
                file = new File(_ctx.getExternalCacheDir(), "share.png");
                FileOutputStream fOut = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                fOut.flush();
                fOut.close();
                file.setReadable(true, false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        })).start();
        return file;
    }

    static public ProgressDialog progressDialog;
    protected ArrayList strings;
    protected boolean b = false;
    static protected Dialog dialog;

    static File file = null;
    static Bitmap image;
    static URL url = null;
}

package com.aurget.buddha.Activity.Utils;

import android.app.Activity;
import androidx.viewpager.widget.ViewPager;

import java.util.List;
import java.util.TimerTask;

public class SliderTimer extends TimerTask {

    public static final int delay = 200;
    public static final int period = 2000;

    private Activity activity;
    private ViewPager viewPager;
    private List<String> color;

    public SliderTimer(Activity activity, ViewPager viewPager,List<String> color)
    {
        this.activity = activity;
        this.viewPager =viewPager;
        this.color = color;
    }

    @Override
    public void run() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (viewPager.getCurrentItem() < color.size() - 1) {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                } else {
                    viewPager.setCurrentItem(0);
                }
            }
        });
    }
}

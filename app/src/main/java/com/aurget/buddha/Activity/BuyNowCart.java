package com.aurget.buddha.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.aurget.buddha.Activity.MyProfile.Shipping_ddress_list_Activity;
import com.aurget.buddha.Activity.RajorPayment.CheckoutActivity;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.Utils.CustomPreference;
import com.aurget.buddha.Activity.categories.BuyNow;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.aurget.buddha.Activity.Commonhelper.getScreenWidth;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;

public class BuyNowCart extends AppCompatActivity {

    RecyclerView recyclerView;
    TextView titleTv, buyNowTv2, subTOtTv, shippingchrgTv, totlamountTv, wlletTv, plusTv;
    TextView priceTv;
    TextView priceTv2;
    TextView percentOffTv, totalTv;
    TextView ipTv;
    TextView productNmeTv, addressTv;
    TextView soldTv, wallet_BalanceTv;
    TextView rtingTv, totlIPTv;
    TextView minusTv, saveWallet;

    LinearLayout pymentMethodLl, shippingLL;
    String paymentMethod = "", walletAmt = "";
    EditText qtyEt;
    CardView shippingAddressCv;
    ImageView imageView1, imageView2;

    Activity activity = this;
    Switch wlletSwitch;
    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_now_cart);

        titleTv = findViewById(R.id.title);
        addressTv = findViewById(R.id.addressTv);
        pymentMethodLl = findViewById(R.id.pymentMethodLl);
        shippingLL = findViewById(R.id.shippingLL);
        buyNowTv2 = findViewById(R.id.buyNowTv2);
        shippingAddressCv = findViewById(R.id.shippingAddressCv);
        plusTv = findViewById(R.id.plusTv);
        minusTv = findViewById(R.id.minusTv);
        saveWallet = findViewById(R.id.saveWallet);
        totlIPTv = findViewById(R.id.totlIPTv);

        //rtingTv = findViewById(R.id.rtingTv);
        // priceTv = findViewById(R.id.priceTv);
        //priceTv2 = findViewById(R.id.priceTv2);
        //priceTv2.setPaintFlags(priceTv2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        //percentOffTv = findViewById(R.id.percentOffTv);
        //ipTv = findViewById(R.id.ipTv);
        //productNmeTv = findViewById(R.id.productNmeTv);
        subTOtTv = findViewById(R.id.subTOtTv);
        // soldTv = findViewById(R.id.soldTv);
        wlletTv = findViewById(R.id.wlletTv);
        qtyEt = findViewById(R.id.qtyEt);
//        imageView1 = findViewById(R.id.img);
//        imageView2 = findViewById(R.id.img2);

        subTOtTv = findViewById(R.id.subTOtTv);
        shippingchrgTv = findViewById(R.id.shippingchrgTv);
        totlamountTv = findViewById(R.id.totlamountTv);
        totalTv = findViewById(R.id.totalTv);
        wallet_BalanceTv = findViewById(R.id.Wallet_BalanceTv);
        wlletSwitch = findViewById(R.id.wlletSwitch);

        //findViewById(R.id.totalLB).setVisibility(View.GONE);

        titleTv.setText("Buy Now");

        recyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager horizontalLayoutManager5 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManager5);

        try {
            String res = getIntent().getStringExtra("res");
            JSONArray jsonArray = new JSONArray(res);
            jsonObjectArrayList.clear();
            prodIds.clear();
            jsonArrayQtr = new ArrayList();

            for (int i = 0; i < jsonArray.length(); i++) {
                if (jsonArray.optJSONObject(i).optString("move_to_cart").equals("1")) {
                    jsonObjectArrayList.add(jsonArray.optJSONObject(i));
                    prodIds.add(jsonArray.optJSONObject(i).optString("product_id"));
                }
            }

            Commonhelper.sop("rrr" + jsonObjectArrayList.toString());

            recyclerView.setAdapter(new MyRecyclerAdapter());
            //subTOtTv.setText("₹"+subTotal);
            //totalTv.setText("₹"+subTotal);
            //totlamountTv.setText(""+subTotal);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        totAmt = getIntent().getStringExtra("totAmt").replace("₹", "");
        totIP = getIntent().getStringExtra("totIPs");

        new AbstrctClss(BuyNowCart.this, ConstntApi.wallet_ballanceUrl(BuyNowCart.this), "p", true) {
            @Override
            public void responce(String s) {

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(s);
                    walletAmt = jsonArray.optJSONObject(0).optString("Wallet_Balance");
                    //wlletTv.setText("₹" + walletAmt);
                    wallet_BalanceTv.setText(walletAmt);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };

        wlletSwitch.setChecked(false);

        wlletSwitch.setOnCheckedChangeListener((compoundButton, b) -> {

            if (parseDouble(walletAmt)<1)
            {
                wlletSwitch.setChecked(false);
                Commonhelper.showToastLong(BuyNowCart.this, Constnt.insufficientwalletbalance);
                return;
            }

            if (parseDouble(totlamountTv.getText().toString()) < 499 && wlletSwitch.isChecked()) {
                wlletSwitch.setChecked(false);
                Commonhelper.showToastLong(BuyNowCart.this, Constnt.pleaseorderminimum₹499forusecashback);
                return;
            }

            if (b) {
                if (parseDouble(walletAmt) > parseDouble(totlamountTv.getText().toString())) {
                    totalAmt = Double.parseDouble(totlamountTv.getText().toString());
                    totlamountTv.setText("0.00");
                    totalTv.setText("₹0.00");
                    wlletTv.setText("₹" + Commonhelper.roundOff_(Double.valueOf(totalAmt)));
                    double wallet3 = parseDouble(walletAmt) - totalAmt;
                    wallet_BalanceTv.setText("" + Commonhelper.roundOff_(Double.valueOf(wallet3)));
                    deductAmt = totalAmt;
                    saveWallet.setText("You will save ₹" + totalAmt + " using ₹+" + totalAmt + " Wallet/Cashback");
                } else {
                    deductAmt = parseDouble(walletAmt);
                    totalAmt = Double.parseDouble(totlamountTv.getText().toString());
                    double totAmot = totalAmt - parseDouble(walletAmt);
                    totlamountTv.setText("" + Commonhelper.roundOff_(totAmot));
                    totalTv.setText("₹" + Commonhelper.roundOff_(totAmot));
                    wlletTv.setText("₹" + Commonhelper.roundOff_(Double.valueOf(walletAmt)));
                    wallet_BalanceTv.setText("0.00");
                    saveWallet.setText("You will save ₹" + walletAmt + " using ₹" + walletAmt + " Wallet/Cashback");
                }
            } else {
                deductAmt = 0.0;
                saveWallet.setText("You will save ₹0 using ₹0 Wallet/Cashback");
                wallet_BalanceTv.setText("" + Commonhelper.roundOff_(Double.valueOf(walletAmt)));
                wlletTv.setText("₹0.00");
                totlamountTv.setText("" + Commonhelper.roundOff_(Double.valueOf(totalAmt)));
                totalTv.setText("₹" + Commonhelper.roundOff_(Double.valueOf(totalAmt)));
            }
        });

        buyNowTv2.setOnClickListener(view -> {

            if (addressTv.getText().toString().trim().length() == 0) {
                Commonhelper.showToastLong(BuyNowCart.this, getString(R.string.select_shipping_address));
                return;
            }

            if (parseDouble(totlamountTv.getText().toString().trim()) < 1) {
                final Dialog dialog = Commonhelper.loadDialog(BuyNowCart.this);
                new Handler().postDelayed(() -> {
                    insert_invoice_details();
                    Commonhelper.dismiss();
                    dialog.dismiss();
                }, 2000);

                return;
            }

            paymentMethod = "";

            final Dialog dialog = new Dialog(BuyNowCart.this);
            dialog.setContentView(R.layout.pyment_method);
            dialog.setCanceledOnTouchOutside(false);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            final RadioButton payOnlineRbn = dialog.findViewById(R.id.payOnlineRbn);
            final RadioButton codRbn = dialog.findViewById(R.id.codRbn);
            final RadioButton walletRbn = dialog.findViewById(R.id.walletRbn);
            final Button doneBtn = dialog.findViewById(R.id.doneBtn);
            final TextView Wallet_BalanceTv = dialog.findViewById(R.id.Wallet_BalanceTv);
            final TextView payTv = dialog.findViewById(R.id.payTv);
            final TextView cashDelCharge = dialog.findViewById(R.id.cashDelCharge);
            Wallet_BalanceTv.setText(wlletTv.getText().toString());

            if (cashDelChrg > 0) {
                //shippingLL.setVisibility(View.GONE);
                shippingchrgTv.setText("₹" + cashDelChrg);
                shippingchrgTv.setTextColor(getResources().getColor(R.color.blackCol));
            } else {
                shippingLL.setVisibility(View.VISIBLE);
                shippingchrgTv.setText("Free");
                shippingchrgTv.setTextColor(getResources().getColor(R.color.greencol));
            }

            if (cod_charge > 0) {
                //shippingLL.setVisibility(View.GONE);
                cashDelCharge.setText("" + cod_charge);
                cashDelCharge.setTextColor(getResources().getColor(R.color.blackCol));
            } else {
                cashDelCharge.setText("Free");
                cashDelCharge.setTextColor(getResources().getColor(R.color.greencol));
            }

            payOnlineRbn.setOnCheckedChangeListener((compoundButton, b) -> {
                if (b) {
                    codRbn.setChecked(false);
                    payOnlineRbn.setChecked(true);
                    walletRbn.setChecked(false);
                    paymentMethod = "card";
                    payTv.setText("" + totlamountTv.getText().toString());
                    shippingLL.setVisibility(View.VISIBLE);
                    /*shippingchrgTv.setText("Free");
                    shippingchrgTv.setTextColor(getResources().getColor(R.color.greencol));*/
                }
            });

            codRbn.setOnCheckedChangeListener((compoundButton, b) -> {
                if (b) {
                    payOnlineRbn.setChecked(false);
                    codRbn.setChecked(true);
                    walletRbn.setChecked(false);
                    paymentMethod = "cod";
                    shippingLL.setVisibility(View.VISIBLE);

                    if (cashDelChrg > 0) {
                        shippingchrgTv.setText("₹" + cashDelChrg);
                        shippingchrgTv.setTextColor(getResources().getColor(R.color.blackCol));
                    } else {
                        shippingchrgTv.setText("Free");
                        shippingchrgTv.setTextColor(getResources().getColor(R.color.greencol));
                    }

                    Double totamt = parseDouble(totlamountTv.getText().toString()) + cod_charge;
                    payTv.setText("" + totamt);
                }
            });

            walletRbn.setOnCheckedChangeListener((compoundButton, b) -> {
                if (b) {
                    payOnlineRbn.setChecked(false);
                    codRbn.setChecked(false);
                    walletRbn.setChecked(true);
                    paymentMethod = "wallet";
                    shippingLL.setVisibility(View.GONE);

                    double totaAmout = parseDouble(totlamountTv.getText().toString()) - parseDouble(walletAmt);

                    payTv.setText("" + totaAmout);
                }
            });

            doneBtn.setOnClickListener(view1 -> {

                if (paymentMethod.equals("")) {
                    Commonhelper.showToastLong(BuyNowCart.this, getString(R.string.selectPaymentType));
                    return;
                }

                if (paymentMethod.equals("cod") || parseDouble(payTv.getText().toString().trim()) < 1) {
                    final Dialog dialog1 = Commonhelper.loadDialog(BuyNowCart.this);

                    new Handler().postDelayed(() -> {
                        if (address_id.equals("")) {
                            Commonhelper.showToastLong(BuyNowCart.this, "Select Address");
                            dialog1.dismiss();
                        } else {
                            //ArrayList<JSONObject> jsonObjectArrayList_ = new ArrayList<>();
                            insert_invoice_details();
                            //Commonhelper.showToastLong(BuyNow.this,"Payment has been successfully");
                            Commonhelper.dismiss();
                            dialog1.dismiss();
                           /* Intent intent = new Intent(BuyNow.this, Main2Activity.class);
                            startActivity(intent);
                            ActivityCompat.finishAffinity(BuyNow.this);*/
                        }
                    }, 2000);

                    return;
                }

                Intent intent = new Intent(BuyNowCart.this, CheckoutActivity.class);
                intent.putExtra("BuyNow", "true");
                intent.putExtra("totlamount", payTv.getText().toString());
                startActivityForResult(intent, 100);
                dialog.dismiss();
            });

            payTv.setText("" + totlamountTv.getText().toString());
            dialog.show();

        });

        findViewById(R.id.bkImg).setOnClickListener(v -> finish());

        shippingAddressCv.setOnClickListener(view -> {
            Intent intent = new Intent(BuyNowCart.this, Shipping_ddress_list_Activity.class);
            intent.putExtra("SubCategoryDetails2", "true");
            startActivityForResult(intent, 101);
        });

        detils();
    }

    private String product_details() {
        ArrayList arrayListCol = new ArrayList();
        ArrayList arrayListSize = new ArrayList();
        ArrayList arrayListID = new ArrayList();
        JSONObject jsonObject = new JSONObject();

        for (int i = 0; i < jsonObjectArrayList.size(); i++) {
            arrayListCol.add(jsonObjectArrayList.get(i).optString("product_color"));
            arrayListSize.add(jsonObjectArrayList.get(i).optString("product_size"));
            arrayListID.add(jsonObjectArrayList.get(i).optString("product_id"));
            jsonArrayQtr.add(jsonObjectArrayList.get(i).optString("qtr"));
        }

        try {
            jsonObject.put("color", arrayListCol.toString());
            jsonObject.put("size", arrayListSize.toString());
            jsonObject.put("product_id", arrayListID.toString());
            //jsonObjectArrayList_.add(jsonObject);
            //jsonArray.add(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    private void insert_invoice_details() {
        String loginId = CustomPreference.readString(activity, CustomPreference.e_id, "");
        String res = InterfaceClass.ipAddress4 + "insert_invoice_details?product_details=" + product_details() + "&shipping_address=" + address_id + "&tax=1&per_tax=1" +
                "&grand_total=" + totlamountTv.getText().toString() + "&login_id=" + loginId + "&payment_type=" + paymentMethod + "&product_id=1&deductAmt=" + deductAmt
                + "&qtr=" + jsonArrayQtr.toString() + "&wallet_amount=" + wlletTv.getText().toString().replace("₹", "");

        Log.d("resss", res);

        final Dialog dialog = Commonhelper.loadDialog(BuyNowCart.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, res,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String Res = response;
                            Log.d("dsdss", Res);
                            //JSONArray jsonArray = new JSONArray(Res);
                            //String status = jsonArray.optJSONObject(0).optString("Statusa");
                            if (response.contains("control")) {

                                Commonhelper.showToastLong(BuyNowCart.this, getResources().getString(R.string.order_submitteed));

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent(BuyNowCart.this, Main2Activity.class);
                                        startActivity(intent);
                                        ActivityCompat.finishAffinity(BuyNowCart.this);
                                    }
                                }, 1000);
                                //paymentContent("success");
                                //paymentSuccessAPI();
                                //sendOtp(CustomPreference.readString(BuyNowCart.this, CustomPreference.mobileNO, ""));
                            } else {
                                Commonhelper.showToastLong(BuyNowCart.this, "Failed");
                                //paymentContent("fail");
                                //paymentSuccessAPI();
                                //sendOtp(CustomPreference.readString(BuyNowCart.this, CustomPreference.mobileNO, ""));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("dsd", "sssss");
                        }
                        dialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("onErrorResponse", "hitPi2");
                        dialog.dismiss();
                    }
                }) {
//            @Override
//            protected Map getParams()
//            {
//                Map params = new HashMap();
//                params.put("username", "");
//                params.put("email", "abc@androidhive.info");
//                params.put("password", "password123");
//
//                return params;
//            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(BuyNowCart.this);
        requestQueue.add(stringRequest);

    }

  /*  private void paymentSuccessAPI() {

        String login_id = CustomPreference.readString(BuyNowCart.this, CustomPreference.e_id, "");

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("product_id", product_id);
            jsonObject.put("shiping_address_id", address_id);
            jsonObject.put("quantity", qtyEt.getText().toString());
            jsonObject.put("payment_type", paymentMethod);
            jsonObject.put("amount", totlamountTv.getText().toString());
            jsonObject.put("ip", ipTv.getText().toString());
            jsonObject.put("login_id", login_id);
            jsonObject.put("size", sizeTv.getText().toString());
            jsonObject.put("color", color);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //http://aurget.com/app/sale_details.php?sale_code=1&buyer=1&product_details=1&shipping_address=1&gst=1&gst_per=1&shipping_charge=10&payment_type=razerpay&payment_status=1&payment_details=135353535&grand_total=120&sale_datetime=1/1/2020&delivary_datetime=1/1/2020&delivery_status=pending

        new AbstrctClss(BuyNowCart.this, ConstntApi.paymentSuccessAPI(jsonObject), "p", true) {
            @Override
            public void responce(String s) {

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(s);

                    if (jsonArray.optJSONObject(0).optString("Status").equalsIgnoreCase("true")) {
                        //Commonhelper.showToastLong(BuyNow.this, jsonArray.optJSONObject(0).optString("Message"));

                        Commonhelper.showToastLong(BuyNowCart.this, getResources().getString(R.string.order_submitteed));

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(BuyNowCart.this, Main2Activity.class);
                                startActivity(intent);
                                ActivityCompat.finishAffinity(BuyNowCart.this);
                            }
                        },3000);

                    } else {
                        Commonhelper.showToastLong(BuyNowCart.this, jsonArray.optJSONObject(0).optString("Message"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }*/

    public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

        public MyRecyclerAdapter() {
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item, parent, false);
            //v.getLayoutParams().width = (int) (getScreenWidth(BuyNowCart.this) / 1.1f);
            v.getLayoutParams().width = (int) (getScreenWidth(BuyNowCart.this));
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            try {
                holder.prodNmeTv.setText(jsonObjectArrayList.get(position).optString("title"));

                if (jsonObjectArrayList.get(position).optString("product_size").trim().equals("")) {
                    holder.sizeCv.setVisibility(View.GONE);
                    holder.sizeTv2.setVisibility(View.GONE);
                } else {
                    holder.sizeCv.setVisibility(View.VISIBLE);
                    holder.sizeTv2.setVisibility(View.VISIBLE);
                    holder.sizeTv.setText(jsonObjectArrayList.get(position).optString("product_size"));
                }

                holder.qtyTv.setText("Quantity: " + jsonObjectArrayList.get(position).optString("qtr"));
                holder.ipTv.setText("IP: " + jsonObjectArrayList.get(position).optString("ip"));

                if (parseDouble(jsonObjectArrayList.get(position).optString("discount")) > 0) {
                    /*holder.percentOffTv.setText("₹" + jsonObjectArrayList.get(position).optString("sale_price") + " ₹" + "" +
                            jsonObjectArrayList.get(position).optString("discount"));*/
                    Commonhelper.isDiscountRate(jsonObjectArrayList, position, holder.percentOffTv);
                } else {
                    holder.percentOffTv.setText("₹" + jsonObjectArrayList.get(position).optString("sale_price"));
                }

                if (jsonObjectArrayList.get(position).optString("product_color").trim().length()>0)
                {
                    holder.imageTv.setVisibility(View.VISIBLE);
                    holder.imgCv.setVisibility(View.VISIBLE);

                    Picasso.with(BuyNowCart.this).load(jsonObjectArrayList.get(position).optString("product_color"))
                            .into(holder.colorimg);
                }
                else
                {
                    holder.imageTv.setVisibility(View.GONE);
                    holder.imgCv.setVisibility(View.GONE);
                }

                Commonhelper.sop("imggg"+InterfaceClass.imgPth2 + jsonObjectArrayList.get(position).optString("product_id") + "_" + 1 + "_thumb.jpg");
                Picasso.with(BuyNowCart.this).load(InterfaceClass.imgPth2 + jsonObjectArrayList.get(position).optString("product_id") + "_" + 1 + "_thumb.jpg")
                        .into(holder.img);

            } catch (Exception e) {
            }
        }

        @Override
        public int getItemCount() {
            return jsonObjectArrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView prodNmeTv, sizeTv, qtyTv, percentOffTv, ipTv, sizeTv2,imageTv;
            private ImageView img, colorimg;
            private CardView sizeCv,imgCv;
            EditText qtyEt;

            public ViewHolder(final View itemView) {
                super(itemView);

                prodNmeTv = itemView.findViewById(R.id.prodNmeTv);
                sizeTv = itemView.findViewById(R.id.sizeTv);
                qtyTv = itemView.findViewById(R.id.qtyTv);
                percentOffTv = itemView.findViewById(R.id.percentOffTv);
                ipTv = itemView.findViewById(R.id.ipTv);
                img = itemView.findViewById(R.id.img2);
                colorimg = itemView.findViewById(R.id.colorimg);
                sizeCv = itemView.findViewById(R.id.sizeCv);
                sizeTv2 = itemView.findViewById(R.id.sizeTv2);
                imgCv = itemView.findViewById(R.id.imgCv);
                imageTv = itemView.findViewById(R.id.imageTv);
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            Log.i("onActivityResult", data.getStringExtra("res"));
            String res = data.getStringExtra("res");
            if (res.equals("success")) {
                insert_invoice_details();
                //Commonhelper.showToastLong(BuyNowCart.this, getResources().getString(R.string.order_submitteed));
            } else {
                try {
                    JSONObject jsonObject = new JSONObject(res);
                    Commonhelper.showToastLong(BuyNowCart.this, jsonObject.optJSONObject("error").optString("description"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    Commonhelper.showToastLong(BuyNowCart.this, "Payment cancelled");
                }
            }
            //paymentSuccessAPI();
        } else if (requestCode == 101) {
            /*try {
                if (data.hasExtra("address_id")) {
                    address_id = data.getStringExtra("address_id");
                    addressTv.setText(data.getStringExtra("name") + "\n" + data.getStringExtra("address")
                            + "\n" + data.getStringExtra("city") + " " + data.getStringExtra("pin_code"));
                }
            } catch (Exception e) {

            }*/
        }
    }

    private void detils() {
        new AbstrctClss(BuyNowCart.this, ConstntApi.product_single_buy(prodIds.toString()), "p", true) {
            @Override
            public void responce(String s) {
                try {
                    JSONArray jsonArray = new JSONArray(s);

                    cod_charge = Double.parseDouble(jsonArray.optJSONObject(0).optString("cod_charge"));

                    for (int i = 0; i < jsonArray.length(); i++) {
                        cashDelChrg = cashDelChrg + Double.parseDouble(jsonArray.optJSONObject(i).optString("shipping_cost"));
                    }

                    //shippingchrgTv.setText("₹" + cashDelChrg);

                    if (cashDelChrg > 0) {
                        //shippingLL.setVisibility(View.GONE);
                        shippingchrgTv.setText("₹" + cashDelChrg);
                    } else {
                        shippingLL.setVisibility(View.VISIBLE);
                        shippingchrgTv.setText("Free");
                        shippingchrgTv.setTextColor(getResources().getColor(R.color.greencol));
                    }

                    subTOtTv.setText(Commonhelper.roundOff_(Double.valueOf(totAmt)));
                    double totAmt_ = Double.parseDouble(totAmt) + cashDelChrg;
                    totalTv.setText("" + Commonhelper.roundOff_(totAmt_));
                    totlamountTv.setText(""+Commonhelper.roundOff_(totAmt_));
                    totlIPTv.setText(totIP);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {

            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        //String name = CustomPreference.readString(BuyNowCart.this,CustomPreference.shippingName,"");
        String address = CustomPreference.readString(BuyNowCart.this, CustomPreference.shippingAddress, "");
        address_id = CustomPreference.readString(BuyNowCart.this, CustomPreference.getShippingAddressID, "");

        if (!address_id.equals(""))
            addressTv.setText(address);
    }

    private String address_id = "";
    private String product_id = "";
    private String price = "";
    private Double countPrice = 0.0;
    private double totalAmt = 0;
    private double subTotal = 0;
    private double deductAmt = 0;
    private double cashDelChrg = 0.0;
    private double cod_charge = 0.0;
    private ArrayList<String> prodIds = new ArrayList<>();
    String totAmt;
    String totIP;

    private ArrayList jsonArrayQtr = null;
}

package com.aurget.buddha.Activity.code.Main;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ParseException;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aurget.buddha.Activity.code.Account.AccountActivity;
import com.aurget.buddha.Activity.code.Account.FavouriteListActivity;
import com.aurget.buddha.Activity.code.Category.CategoryActivity;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Fragment.DashBoardFragment;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.Activity.code.volly.AbstrctClss;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import code.volly.ConstntApi;

public class NotificationActivity extends BaseActivity implements View.OnClickListener {

    Typeface typeface;

    PushAdapter pushAdapter;
    ArrayList<HashMap<String, String>> transList = new ArrayList();
    RecyclerView recyclerView;

    //ImageView
    ImageView ivCart, ivMenu, ivSearch, ivHome, ivCategory, ivFavourites, ivProfile, ivNotification;

    //LinearLayout
    LinearLayout llHome, llCategory, llFavourites, llProfile;
    TextView tvHome, tvProfile, tvCategory, tvFavourites, tvNotification,
            tvHeaderText;

    GridLayoutManager mGridLayoutManager;

    RelativeLayout rrNoData;

    ArrayList<HashMap<String, String>> PushList = new ArrayList();


    public static String date, date1, notify_time, notify_time1, timein24, month, date_time;

    //Integer
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        findId();
    }

    private void findId() {
        //typeface = Typeface.createFromAsset(mActivity.getAssets(), "centurygothic.otf");

        //Recycleview
        recyclerView = findViewById(R.id.recyclerView);

        //ImageView
        ivCart = findViewById(R.id.ivCart);
        ivMenu = findViewById(R.id.iv_menu);
        ivSearch = findViewById(R.id.searchmain);

        //TextView
        tvHeaderText = findViewById(R.id.tvHeaderText);
        tvHome = findViewById(R.id.tvHomeBottom);
        tvCategory = findViewById(R.id.tvCategoryBottom);
        tvFavourites = findViewById(R.id.tvFavouritesBottom);
        tvProfile = findViewById(R.id.tvProfileBottom);
        tvNotification = findViewById(R.id.tvNotificationBottom);

        ivHome = findViewById(R.id.ivHomeBottom);
        ivCategory = findViewById(R.id.ivCategoryBottom);
        ivFavourites = findViewById(R.id.ivFavouritesBottom);
        ivProfile = findViewById(R.id.ic_profile);
        ivNotification = findViewById(R.id.ic_notification);


        rrNoData = findViewById(R.id.rrNoData);

        //LinearLayout fro bottom views
        llHome = findViewById(R.id.llHome);
        llCategory = findViewById(R.id.llCategory);
        llFavourites = findViewById(R.id.llFavourites);
        llProfile = findViewById(R.id.llProfile);


        llHome.setOnClickListener(this);
        llCategory.setOnClickListener(this);
        llFavourites.setOnClickListener(this);
        llProfile.setOnClickListener(this);

        ivCart.setVisibility(View.INVISIBLE);
        ivSearch.setVisibility(View.INVISIBLE);

        ivMenu.setImageResource(R.drawable.ic_back);

        tvHeaderText.setText(getString(R.string.updates));

        mGridLayoutManager = new GridLayoutManager(mActivity, 1);
        recyclerView.setLayoutManager(mGridLayoutManager);

        ivCart.setOnClickListener(this);
        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);

        new AbstrctClss(mActivity, ConstntApi.updateProfileApi(), "p", true) {
            @Override
            public void responce(String s) {
                AppUtils.print("sss" + s);
                try {
                    JSONArray jsonArray = new JSONArray(s);
                    getPush(jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                AppUtils.print("ssss" + s);
            }
        };

    }

    private void getPush(JSONArray jsonArray) {


        PushList.clear();

        for (int i = 0; i < jsonArray.length(); i++) {

            HashMap<String, String> hashlist = new HashMap();
            hashlist.put("title", jsonArray.optJSONObject(i).optString("title"));
            hashlist.put("push_msg", jsonArray.optJSONObject(i).optString("push_msg"));
            hashlist.put("image", jsonArray.optJSONObject(i).optString("image"));
            PushList.add(hashlist);
            //PushList.addAll(DatabaseController.getPushData());
        }

        if (PushList.size() <= 0) {
            rrNoData.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            rrNoData.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }

        pushAdapter = new PushAdapter(PushList);
        recyclerView.setAdapter(pushAdapter);
        recyclerView.setNestedScrollingEnabled(true);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.iv_menu:
                finish();
                return;

            case R.id.llHome:

                tvHome.setTextColor(Color.parseColor("#155b72"));
                tvCategory.setTextColor(Color.parseColor("#9d9b9b"));
                tvFavourites.setTextColor(Color.parseColor("#9d9b9b"));
                tvProfile.setTextColor(Color.parseColor("#9d9b9b"));
                ivHome.setImageResource(R.drawable.ic_home_darkgreen);
                ivCategory.setImageResource(R.drawable.menu);
                ivFavourites.setImageResource(R.drawable.ic_heart_grey);
                ivProfile.setImageResource(R.drawable.ic_notifications_grey);

                startActivity(new Intent(getBaseContext(), DashBoardFragment.class));

                return;

            case R.id.llCategory:

                tvHome.setTextColor(Color.parseColor("#9d9b9b"));
                tvCategory.setTextColor(Color.parseColor("#155b72"));
                tvFavourites.setTextColor(Color.parseColor("#9d9b9b"));
                tvProfile.setTextColor(Color.parseColor("#9d9b9b"));
                ivHome.setImageResource(R.drawable.ic_home_grey);
                ivCategory.setImageResource(R.drawable.menu);
                ivFavourites.setImageResource(R.drawable.ic_heart_grey);
                ivProfile.setImageResource(R.drawable.ic_notifications_grey);

                startActivity(new Intent(getBaseContext(), CategoryActivity.class));

                return;

            case R.id.llFavourites:

                if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else if (AppSettings.getString(AppSettings.verified).contains("0")) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else {

                    tvHome.setTextColor(Color.parseColor("#9d9b9b"));
                    tvCategory.setTextColor(Color.parseColor("#9d9b9b"));
                    tvFavourites.setTextColor(Color.parseColor("#155b72"));
                    tvProfile.setTextColor(Color.parseColor("#9d9b9b"));
                    ivHome.setImageResource(R.drawable.ic_home_grey);
                    ivCategory.setImageResource(R.drawable.menu);
                    ivFavourites.setImageResource(R.drawable.ic_heart_darkgreen);
                    ivProfile.setImageResource(R.drawable.ic_notifications_grey);

                    startActivity(new Intent(getBaseContext(), FavouriteListActivity.class));
                }

                return;

            case R.id.llProfile:

                tvHome.setTextColor(Color.parseColor("#9d9b9b"));
                tvCategory.setTextColor(Color.parseColor("#9d9b9b"));
                tvFavourites.setTextColor(Color.parseColor("#9d9b9b"));
                tvNotification.setTextColor(Color.parseColor("#9d9b9b"));
                tvProfile.setTextColor(Color.parseColor("#155b72"));
                ivHome.setImageResource(R.drawable.ic_home_grey);
                ivCategory.setImageResource(R.drawable.menu);
                ivFavourites.setImageResource(R.drawable.ic_heart_grey);
                ivProfile.setImageResource(R.drawable.ic_profile_darkgreen);
                ivNotification.setImageResource(R.drawable.ic_notifications_grey);

                if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else if (AppSettings.getString(AppSettings.verified).contains("0")) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else {
                    startActivity(new Intent(getBaseContext(), AccountActivity.class));
                }

        }
    }

    private class PushAdapter extends RecyclerView.Adapter<FavNameHolder> {

        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

        public PushAdapter(ArrayList<HashMap<String, String>> favList) {
            data = favList;
        }

        public FavNameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new FavNameHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_push, parent, false));
        }

        public void onBindViewHolder(FavNameHolder holder, final int position) {
            //largeIcon
//            long timestamp = Long.parseLong(data.get(position).get("add_date")) * 1000L;
//            notify_time=getTime(timestamp);
//
//
//            try {
//                timein24=getTime24hr(notify_time);
//
//            } catch (java.text.ParseException e) {
//                e.printStackTrace();
//            }
//
//            date = getDate(timestamp);
//
//            try {
//                month=getMonth(date);
//            } catch (java.text.ParseException e) {
//                e.printStackTrace();
//            }
//
//            //getting date
//            String[] separated = date.split("/");
//            separated[0]= separated[0].trim();//month
//            separated[1]= separated[1].trim();//date
//            separated[2]= separated[2].trim();//year
//
//            date_time = month+" "+separated[1]+", "+separated[2]+" at " +timein24;

            holder.tvName.setText(data.get(position).get("title"));
            //holder.tvDate.setText(getTimeAgo(Long.parseLong(data.get(position).get("add_date"))));
            holder.tvDesc.setText(data.get(position).get("push_msg"));

            if (!data.get(position).get("image").isEmpty()) {
                Picasso.with(mActivity).load(data.get(position).get("image")).into(holder.ivIcon);
            }
        }

        public int getItemCount() {
            return data.size();
        }
    }

    private class FavNameHolder extends RecyclerView.ViewHolder {
        ImageView ivIcon;
        TextView tvDate;
        TextView tvName;
        TextView tvDesc;
        RelativeLayout rrView;

        public FavNameHolder(View itemView) {
            super(itemView);
            ivIcon = itemView.findViewById(R.id.imageView21);
            tvName = itemView.findViewById(R.id.tvName);
            tvDesc = itemView.findViewById(R.id.tvDesc);
            tvDate = itemView.findViewById(R.id.tvDate);
            rrView = itemView.findViewById(R.id.rrView);
        }
    }


    private String getTime(long timeStamp) {

        try {
            DateFormat sdf = new SimpleDateFormat("hh:mm a");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        } catch (Exception ex) {
            return "xx";
        }
    }


    private String getDate(long timeStamp) {

        try {
            DateFormat sdf = new SimpleDateFormat("MMMM/dd/yyyy");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        } catch (Exception ex) {
            return "xx";
        }
    }


    private String getTime24hr(String time) throws java.text.ParseException {
        String t = "";
        SimpleDateFormat date12Format = new SimpleDateFormat("hh:mm a");

        SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm");
        t = date24Format.format(date12Format.parse(time));
        Log.e("t", t);
        return t;

    }


    private static String getMonth(String date) throws ParseException, java.text.ParseException {
        Date d = new SimpleDateFormat("MMMM/dd/yyyy", Locale.ENGLISH).parse(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        String monthName = new SimpleDateFormat("MMMM").format(cal.getTime());
        String month_name = monthName.substring(0, 3);
        return month_name;
    }


    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;


        }
        // long now = getCurrentTime(ctx);
        long now = System.currentTimeMillis();

        if (time > now || time <= 0) {
            return null;
        }
        // TODO: localize
        final long diff = now - time;


        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            //mins = diff / MINUTE_MILLIS ;
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            if ((diff / HOUR_MILLIS) == 1) {
                return "an hour ago";
            } else {
                return diff / HOUR_MILLIS + " hours ago";
            }
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {


            return date_time;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        tvHome.setTextColor(Color.parseColor("#9d9b9b"));
        tvCategory.setTextColor(Color.parseColor("#9d9b9b"));
        tvFavourites.setTextColor(Color.parseColor("#9d9b9b"));
        tvNotification.setTextColor(Color.parseColor("#155b72"));
        tvProfile.setTextColor(Color.parseColor("#9d9b9b"));
        ivHome.setImageResource(R.drawable.ic_home_grey);
        ivCategory.setImageResource(R.drawable.menu);
        ivFavourites.setImageResource(R.drawable.ic_heart_grey);
        ivNotification.setImageResource(R.drawable.ic_notifications_green);
        ivProfile.setImageResource(R.drawable.ic_profile_grey);

    }
}

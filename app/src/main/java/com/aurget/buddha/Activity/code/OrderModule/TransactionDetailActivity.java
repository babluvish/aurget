package com.aurget.buddha.Activity.code.OrderModule;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;

public class TransactionDetailActivity extends BaseActivity implements View.OnClickListener {

    //ImageView
    ImageView ivCart,ivMenu,ivSearch;

    //TextView
    TextView tvHeaderText, tvOrderId,tvDate,tvDetail,tvAmount,tvStatus,tvWallet,tvPayment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge_detail);

        initialise();
    }

    private void initialise() {

        //ImageView
        ivCart =    findViewById(R.id.ivCart);
        ivMenu =    findViewById(R.id.iv_menu);
        ivSearch =  findViewById(R.id.searchmain);

        ivCart.setOnClickListener(this);
        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);

        ivCart.setVisibility(View.INVISIBLE);
        ivSearch.setVisibility(View.INVISIBLE);

        ivMenu.setImageResource(R.drawable.ic_back);

        //TextView
        tvHeaderText    =  findViewById(R.id.tvHeaderText);
        tvOrderId=         findViewById(R.id.textView23);
        tvDate=            findViewById(R.id.textView24);
        tvDetail=          findViewById(R.id.textView27);
        tvAmount=          findViewById(R.id.textView28);
        tvStatus=          findViewById(R.id.tvStatusValue);
        tvWallet=          findViewById(R.id.textView32);
        tvPayment=         findViewById(R.id.textView34);

        //1=>pending .2=> success 3=>failed 4=> refund

        if(getIntent().getStringExtra("status").equalsIgnoreCase("1"))
        {
            tvStatus.setText("Pending");
            tvStatus.setTextColor(Color.parseColor("#eeffaa00"));
        }
        else  if(getIntent().getStringExtra("status").equalsIgnoreCase("2"))
        {
            tvStatus.setText("Success");
            tvStatus.setTextColor(Color.parseColor("#4CAF50"));
        }
        else  if(getIntent().getStringExtra("status").equalsIgnoreCase("3"))
        {
            tvStatus.setText("Failed");
            tvStatus.setTextColor(Color.parseColor("#ee383e"));
        }
        else  if(getIntent().getStringExtra("status").equalsIgnoreCase("4"))
        {
            tvStatus.setText("Refund");
            tvStatus.setTextColor(Color.parseColor("#4CAF50"));
        }

        if(getIntent().getStringExtra("wallet_used_amount").equalsIgnoreCase("Null")||getIntent().getStringExtra("wallet_used_amount").equalsIgnoreCase("0"))
        {
            tvWallet.setText("RS 0");
        }
        else
        {
            tvWallet.setText("RS "+getIntent().getStringExtra("wallet_used_amount"));
        }

        if(getIntent().getStringExtra("payment_amount").equalsIgnoreCase("Null")||getIntent().getStringExtra("payment_amount").equalsIgnoreCase("0"))
        {
            tvPayment.setText("RS 0");
        }
        else
        {
            tvPayment.setText("RS "+getIntent().getStringExtra("payment_amount"));
        }

        tvHeaderText.setText(getString(R.string.invoice));
        tvOrderId.setText(getIntent().getStringExtra("txn_no"));
        tvDate.setText(getIntent().getStringExtra("add_date"));
        tvDetail.setText("Order Placed");
        tvAmount.setText("RS "+getIntent().getStringExtra("recharge_amount"));
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.iv_menu:

                finish();

                return;
        }
    }
}

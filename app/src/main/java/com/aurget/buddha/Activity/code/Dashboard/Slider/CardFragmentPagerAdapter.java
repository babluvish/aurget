package com.aurget.buddha.Activity.code.Dashboard.Slider;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.cardview.widget.CardView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class CardFragmentPagerAdapter extends FragmentStatePagerAdapter implements CardAdapter {
    private float mBaseElevation;
    private List<CardFragment> mFragments = new ArrayList();

    public CardFragmentPagerAdapter(FragmentManager fm, float baseElevation) {
        super(fm);
        this.mBaseElevation = baseElevation;
        for (int i = 0; i < 5; i++) {
            addCardFragment(new CardFragment());
        }
    }

    public float getBaseElevation() {
        return this.mBaseElevation;
    }

    public CardView getCardViewAt(int position) {
        return ((CardFragment) this.mFragments.get(position)).getCardView();
    }

    public int getCount() {
        return this.mFragments.size();
    }

    public Fragment getItem(int position) {
        return (Fragment) this.mFragments.get(position);
    }

    public Object instantiateItem(ViewGroup container, int position) {
        Object fragment = super.instantiateItem(container, position);
        this.mFragments.set(position, (CardFragment) fragment);
        return fragment;
    }

    public void addCardFragment(CardFragment fragment) {
        this.mFragments.add(fragment);
    }
}

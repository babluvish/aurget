package com.aurget.buddha.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aurget.buddha.Activity.Model.SelectUser;
import com.aurget.buddha.Activity.Utils.CustomPreference;
import com.aurget.buddha.Activity.categories.SubCategoryDetailsActivity;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SearchData extends AppCompatActivity {

    ImageView bkImg;
    EditText searchEt;
    ListView recyclerView ;
    TextView txt1;
    List<SelectUser> selectUsers =  new ArrayList();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_data);
        bkImg = findViewById(R.id.bkImg2);
        recyclerView = findViewById(R.id.recycleView);
        searchEt = findViewById(R.id.searchEt);
        txt1 = findViewById(R.id.txt1);


        bkImg.setVisibility(View.VISIBLE);
        findViewById(R.id.bkImg).setVisibility(View.GONE);


//        findViewById(R.id.crtContner).setVisibility(View.GONE);

        String ssss = CustomPreference.readString(SearchData.this, CustomPreference.serch,"");

        if (!ssss.equals("")) {
            try
            {
                JSONArray jsonArray = new JSONArray(ssss);
                for (int i = 0; i < jsonArray.length(); i++) {
                    SelectUser selectUser = new SelectUser();
                    selectUser.setName(jsonArray.optJSONObject(i).optString("nme"));
                    selectUsers.add(selectUser);
                }
                recyclerView.setAdapter(new ContactListAdapter(selectUsers, SearchData.this));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

       bkImg.setOnClickListener(view -> finish());

        searchEt.setOnClickListener(view -> {

           String s = CustomPreference.readString(SearchData.this, CustomPreference.serch,"");

           if (s.length()==0)
           {
               try {
                   JSONArray jsonArray = new JSONArray();
                   JSONObject jsonObject = new JSONObject();
                   jsonObject.put("nme",txt1.getText().toString());
                   jsonArray.put(jsonObject);
                   CustomPreference.writeString(SearchData.this,CustomPreference.serch,jsonArray.toString());
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }
           else
           {
               try {
                   JSONArray jsonArray = new JSONArray(s);
                   JSONArray jsonArray2 = new JSONArray();
                   for (int i = 0; i < jsonArray.length(); i++) {
                       JSONObject jsonObject = new JSONObject();
                       jsonObject.put("nme",jsonArray.optJSONObject(i).optString("nme"));
                       jsonArray2.put(jsonObject);
                   }
                   JSONObject jsonObject = new JSONObject();
                   jsonObject.put("nme",txt1.getText().toString());
                   jsonArray2.put(jsonObject);
                   CustomPreference.writeString(SearchData.this,CustomPreference.serch,jsonArray2.toString());
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }

           finish();
       });

     searchEt.addTextChangedListener(new TextWatcher() {
         @Override
         public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
         }

         @Override
         public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
         }

         @Override
         public void afterTextChanged(Editable editable) {
             txt1.setText(searchEt.getText().toString());
         }
     });

        searchEt.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                //searchData(searchEt.getText().toString());
                Intent intent  = new Intent(SearchData.this, SubCategoryDetailsActivity.class);
                intent.putExtra("search",searchEt.getText().toString());
                startActivity(intent);
                return true;
            }
            return false;
        });

    }
}


  class ContactListAdapter extends BaseAdapter {

    public List<SelectUser> _data;
    Context _c;
    ViewHolder v;
    int a = 0;
    private ArrayList<SelectUser> arraylist;


    public ContactListAdapter(List<SelectUser> selectUsers, Context context) {
        _data = selectUsers;
        _c = context;
        this.arraylist = new ArrayList<SelectUser>();
        this.arraylist.addAll(_data);
    }

    @Override
    public int getCount() {
        return _data.size();
    }

    @Override
    public Object getItem(int i) {
        return _data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        View view = convertView;
        if (view == null) {
            LayoutInflater li = (LayoutInflater) _c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.view, null);
            Log.e("Inside", "here--------------------------- In view1");
        } else {
            view = convertView;
            Log.e("Inside", "here--------------------------- In view2");
        }
        v = new ViewHolder();

        v.title = (TextView) view.findViewById(R.id.txt1);

        //v.countTv = (TextView) view.findViewById(R.id.countTv);
        // v.imageView = (ImageView) view.findViewById(R.id.pic);

        data = (SelectUser) _data.get(position);
        v.title.setText(data.getName());

        view.setTag(data);

        v.title.setText(_data.get(position).getName());
        return view;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        _data.clear();
        if (charText.length() == 0) {
            _data.addAll(arraylist);
        } else {
            for (SelectUser wp : arraylist) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    _data.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView title, phone;
    }

    SelectUser data=null;
}


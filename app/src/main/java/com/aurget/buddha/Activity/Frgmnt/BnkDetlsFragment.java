package com.aurget.buddha.Activity.Frgmnt;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.ImageCropper.CropImage;
import com.aurget.buddha.Activity.ImageCropper.CropImageView;
import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.Activity.MyProfile.BankAccountEdit;
import com.aurget.buddha.Activity.Utils.CustomPreference;
import com.aurget.buddha.R;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

public class BnkDetlsFragment extends Fragment {

    private SharedPreferences Shpref;
    private SharedPreferences.Editor editShpref;

    public static RelativeLayout passbookRl,aadharRl,adharRl,pankPicRl,panRl,panRl2;
    public static ImageView passbookimg,aadharcardImg,aadharcardImg1,aadharcardImgRemove2,imgpssbook,panImg;
    public static CardView aadharcardCV;
    public static EditText brnchneEt,ifcscodeEt,accountNoEt,bankNameEt,adharEt,panEt;
    public static LinearLayout aadharcardLl,frontAdharCard,backAdharcard,previous,editIcon;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_bnk_detls, container, false);

        passbookRl = view.findViewById(R.id.passbookRl);

        passbookimg = view.findViewById(R.id.passbookimg);
        previous = view.findViewById(R.id.previous);
        panEt = view.findViewById(R.id.panEt);
        imgpssbook = view.findViewById(R.id.imgpssbook);
        aadharcardImg = view.findViewById(R.id.aadharcardImg);
        aadharcardImg1 = view.findViewById(R.id.aadharcardImg1);
        aadharcardImgRemove2 = view.findViewById(R.id.aadharcardImgRemove2);
        aadharRl = view.findViewById(R.id.aadharRl);
        adharRl = view.findViewById(R.id.adharRl);
        frontAdharCard = view.findViewById(R.id.frontAdharCard);

        brnchneEt = view.findViewById(R.id.brnchneEt);
        adharEt = view.findViewById(R.id.adharEt);
        bankNameEt = view.findViewById(R.id.bankNameEt);
        accountNoEt = view.findViewById(R.id.accountNoEt);
        ifcscodeEt = view.findViewById(R.id.ifcscodeEt);
        aadharcardLl = view.findViewById(R.id.aadharcardLl);
        //aadharcardCV = view.findViewById(R.id.aadharcardCV);
        backAdharcard = view.findViewById(R.id.backAdharcard);
        panRl = view.findViewById(R.id.panRl);
        panRl2 = view.findViewById(R.id.panRl2);
        panImg = view.findViewById(R.id.panImg);

        Shpref = getActivity().getSharedPreferences("sp", MODE_PRIVATE);
        editShpref = Shpref.edit();

        passbookRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                editShpref.putInt("num",1);
                editShpref.apply();

                popupUploadImage();
            }
        });

        frontAdharCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editShpref.putInt("num",2);
                editShpref.apply();

                popupUploadImage();
            }
        });

        aadharcardImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frontAdharCard.performClick();
            }
        });

        backAdharcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editShpref.putInt("num",3);
                editShpref.apply();

                popupUploadImage();
            }
        });

        aadharcardImg1.setOnClickListener(view13 -> backAdharcard.performClick());

        editIcon.setOnClickListener(view12 -> {

                if (count%2==0)
                {
                    Commonhelper.sop("counntt"+1);
                }
                else
                {
                    Commonhelper.sop("counntt"+2);
                }
        });

        view.findViewById(R.id.adharRl).setOnClickListener(view1 -> {

            final Dialog dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.aadhar_fornt_back);

            TextView FrontScreenTv = dialog.findViewById(R.id.FrontScreenTv);
            TextView BackScreenTv = dialog.findViewById(R.id.BackScreenTv);

            dialog.findViewById(R.id.byMonthTv).setVisibility(View.GONE);

            FrontScreenTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view1) {

                    editShpref.putInt("num",2);
                    editShpref.apply();

                    popupUploadImage();
                    dialog.dismiss();

                }
            });

            BackScreenTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view1) {

                    editShpref.putInt("num",3);
                    editShpref.apply();

                    popupUploadImage();
                    dialog.dismiss();

                }
            });

            dialog.show();

        });

        panRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                editShpref.putInt("num",4);
                editShpref.apply();

                popupUploadImage();
            }
        });


        adharEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus)
                {
                    if (adharEt.getText().toString().length()!=12)
                    {
                        Commonhelper.showToastLong(getActivity(),"Not A Valid Adhar Number.");
                    }
                }
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AsyncDataClass asyncRequestObject = new AsyncDataClass();
                asyncRequestObject.execute(InterfaceClass.ipAddress3+"update_bank_details.php",
                        CustomPreference.readString(getActivity(), CustomPreference.e_id, ""),
                        BnkDetlsFragment.bankNameEt.getText().toString(),
                        BnkDetlsFragment.accountNoEt.getText().toString(),
                        BnkDetlsFragment.brnchneEt.getText().toString(),
                        BnkDetlsFragment.ifcscodeEt.getText().toString(),
                        CustomPreference.readString(getActivity(), CustomPreference.e_id, ""),
                        BnkDetlsFragment.adharEt.getText().toString(),

                        ((BankAccountEdit)getActivity()).adharCrdImgPth,

                        "_",
                        "_",
                        BnkDetlsFragment.panEt.getText().toString(),
                        ((BankAccountEdit)getActivity()).panImgPth,
                        ((BankAccountEdit)getActivity()).adharCrdImgext,
                        ((BankAccountEdit)getActivity()).panImgext,
                        ((BankAccountEdit)getActivity()).adharCrdImgBackPth,
                        ((BankAccountEdit)getActivity()).adharCrdImgBackExt,
                        ((BankAccountEdit)getActivity()).passbookImgPath,
                        ((BankAccountEdit)getActivity()).passbookImgExt
                );
            }
        });

        //fetch_bank_details();

        return  view;
    }

    private class AsyncDataClass extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            HttpParams httpParameters = new BasicHttpParams();
//
//            HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
//
//            HttpConnectionParams.setSoTimeout(httpParameters, 5000);

            HttpClient httpClient = new DefaultHttpClient(httpParameters);

            HttpPost httpPost = new HttpPost(params[0]);

            String jsonResult = "";

            try {

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("mem_reg_id", params[1]));
                nameValuePairs.add(new BasicNameValuePair("bank_name", params[2]));
                nameValuePairs.add(new BasicNameValuePair("account_no", params[3]));
                nameValuePairs.add(new BasicNameValuePair("branch", params[4]));
                nameValuePairs.add(new BasicNameValuePair("ifsc", params[5]));
                nameValuePairs.add(new BasicNameValuePair("login_id", params[6]));
                nameValuePairs.add(new BasicNameValuePair("aadhar_number", params[7]));
                nameValuePairs.add(new BasicNameValuePair("adhar_image", params[8]));
                nameValuePairs.add(new BasicNameValuePair("dob", params[9]));
                nameValuePairs.add(new BasicNameValuePair("gender", params[10]));
                nameValuePairs.add(new BasicNameValuePair("pan_number", params[11]));
                nameValuePairs.add(new BasicNameValuePair("pan_image", params[12]));
                nameValuePairs.add(new BasicNameValuePair("aadhar_exension", params[13]));
                nameValuePairs.add(new BasicNameValuePair("pan_exension", params[14]));
                nameValuePairs.add(new BasicNameValuePair("aadhar_back", params[15]));
                nameValuePairs.add(new BasicNameValuePair("adhar_back_extension", params[16]));
                nameValuePairs.add(new BasicNameValuePair("pass_book", params[17]));
                nameValuePairs.add(new BasicNameValuePair("pass_extension", params[18]));


                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = httpClient.execute(httpPost);

                jsonResult = Commonhelper.inputStreamToString(response.getEntity().getContent()).toString();

                System.out.println("Returned Json object " + jsonResult.toString());

            } catch (ClientProtocolException e) {

                e.printStackTrace();

            } catch (IOException e) {

                e.printStackTrace();
            }
            return jsonResult;
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            dialog = Commonhelper.loadDialog(getActivity());
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            System.out.println("Resulted Value: " + result);

            try {
                JSONArray jsonArray = new JSONArray(result);
                if(jsonArray.optJSONObject(0).optString("Status").equalsIgnoreCase("true"))
                    Toast.makeText(getActivity(), "Bank Details have been successfully Submitted", Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            dialog.dismiss();

        }
    }

    private void popupUploadImage() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setActivityTitle("Crop image")
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setCropMenuCropButtonTitle("Done")
                .setRequestedSize(1000, 1000).setScaleType(CropImageView.ScaleType.FIT_CENTER)
                .setCropMenuCropButtonIcon(R.drawable.ic_fullscreen_black_24dp)
                .start(getActivity());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult cropResult = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                int i = Shpref.getInt("num",0);
                Log.i("vvv",""+i);
                Log.i("vvv-",""+cropResult.getUri());

                if (i==1) {
                    passbookRl.setVisibility(View.GONE);
                    pankPicRl.setVisibility(View.VISIBLE);
                    imgpssbook.setImageURI(cropResult.getUri());
                }
                else if(i==2) {
                    aadharRl.setVisibility(View.VISIBLE);
                    aadharcardImg.setImageURI(cropResult.getUri());
                }
                else if(i==3) {
                    aadharRl.setVisibility(View.VISIBLE);
                    aadharcardImg1.setImageURI(cropResult.getUri());
                    aadharcardImg1.setVisibility(View.VISIBLE);
                    aadharcardImgRemove2.setVisibility(View.VISIBLE);
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(getActivity(), "Cropping failed: " + cropResult.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    Dialog dialog;
    int count=0;
}

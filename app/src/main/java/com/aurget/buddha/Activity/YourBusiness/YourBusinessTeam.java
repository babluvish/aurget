package com.aurget.buddha.Activity.YourBusiness;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.Main2Activity;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.Utils.SearchDate;
import com.aurget.buddha.Activity.Utils.SearchMonth;
import com.aurget.buddha.Activity.categories.SubCategoryDetailsActivity;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

import static com.aurget.buddha.Activity.Commonhelper.openWhatsApp;
import static com.aurget.buddha.Activity.Commonhelper.phoneCall;

public class YourBusinessTeam extends BaseActivity {

    RecyclerView recycleView;
    TextView dateRnge, mothTv, allTv, allTeamTv;
    EditText searchBt;
    FloatingActionButton floatBt;
    LinearLayoutManager horizontalLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_business_team);

        dateRnge = findViewById(R.id.dateRnge);
        mothTv = findViewById(R.id.mothTv);
        allTv = findViewById(R.id.allTv);
        allTeamTv = findViewById(R.id.allTeamTv);
        searchBt = findViewById(R.id.searchBt);
        floatBt = findViewById(R.id.floatBt);
        floatBt.setVisibility(View.GONE);
        recycleView = findViewById(R.id.recycleView);

        horizontalLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recycleView.setLayoutManager(horizontalLayoutManager);
        recycleView.setItemAnimator(new DefaultItemAnimator());

        businessTeam();


        searchBt.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                searchBt.setCursorVisible(true);
            }
        });

        searchBt.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                businessTeamSearch(searchBt.getText().toString());
                return true;
            }
            return false;
        });


        mothTv.setOnClickListener(view -> new SearchMonth(YourBusinessTeam.this) {
            @Override
            public String fromDt(String month__, String year) {
                return null;
            }
        }.monthNameDialog());

        allTv.setOnClickListener(view -> {

            dateRnge.setBackgroundResource(R.drawable.ovel_shap3);
            allTv.setBackgroundResource(R.drawable.ovel_shape);
            mothTv.setBackgroundResource(R.drawable.ovel_shap3);

        });

        dateRnge.setOnClickListener(view -> new SearchDate(YourBusinessTeam.this) {
            @Override
            public String fromDtToDt(String frmDate, String toDt) {
                return null;
            }
        }.dateRangeDialog());


        recycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE){
                    int position = getCurrentItem();

                    if (position<10)
                    {
                        floatBt.setVisibility(View.GONE);
                    }
                    else
                    {
                        floatBt.setVisibility(View.VISIBLE);
                    }
                    //Commonhelper.showToastLong(mActivity,""+position);
                    //onPageChanged(position);
                }
            }
        });

        floatBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recycleView.post(new Runnable() {
                    @Override
                    public void run() {

                        count++;

                        if(count%2==1)
                        {
                            recycleView.scrollToPosition(horizontalLayoutManager.getItemCount()-1);
                        }
                        else
                        {
                            recycleView.scrollToPosition(1);
                        }

                    }
                });
            }
        });
    }

    private int getCurrentItem(){
        return ((LinearLayoutManager)recycleView.getLayoutManager())
                .findFirstVisibleItemPosition();
    }

    void businessTeam() {
        new AbstrctClss(YourBusinessTeam.this, ConstntApi.business_team(YourBusinessTeam.this), "g", true) {

            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    JSONArray jsonArray = new JSONArray(Res);
                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }
                    recycleView.setAdapter(new MyRecyclerAdapter(jsonObjectArrayList));

                } catch (Exception e) {
                }
            }

            @Override
            public void onErrorResponsee(String s) {

            }
        };

    }

    void businessTeamSearch(String keyword) {
        new AbstrctClss(YourBusinessTeam.this, ConstntApi.business_team_search(YourBusinessTeam.this, keyword), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    JSONArray jsonArray = new JSONArray(Res);

                    if (jsonArray.length() > 0) {
                        ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                        }
                        recycleView.setAdapter(new MyRecyclerAdapter(jsonObjectArrayList));
                        recycleView.scrollToPosition(jsonObjectArrayList.size() - 1);
                    } else {
                        Commonhelper.showToastLong(mActivity, Constnt.notFound);
                    }
                } catch (Exception e) {
                    Commonhelper.showToastLong(mActivity, Constnt.notFound);
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                Commonhelper.showToastLong(mActivity, Constnt.notFound);
            }
        };

    }

    public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapter(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_your_business_team_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapter.ViewHolder holder, int position) {
            holder.dojTv.setText("Since " + jsonObjectal.get(position).optString("doj"));
            holder.mobileNOTv.setText(jsonObjectal.get(position).optString("mobile"));
            holder.solderNmeTv.setText(jsonObjectal.get(position).optString("solder_name"));
            holder.lbleTv.setText(jsonObjectal.get(position).optString("level_name"));
            holder.district.setText(jsonObjectal.get(position).optString("dist"));
            holder.state.setText(jsonObjectal.get(position).optString("state"));
            holder.country.setText(jsonObjectal.get(position).optString("country"));
            holder.pinCode.setText(jsonObjectal.get(position).optString("pincode"));
            holder.ipTv.setText("[" + Commonhelper.roundOff(Double.valueOf(jsonObjectal.get(position).optString("ip"))) + " IP]");
            allTeamTv.setText("All Team " + jsonObjectal.get(position).optString("total_team"));
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView solderNmeTv, lbleTv, mobileNOTv, dojTv, ipTv, district, state, country, pinCode;
            private ImageView imgCall;
            LinearLayout callLl;

            public ViewHolder(View itemView) {
                super(itemView);

                callLl = itemView.findViewById(R.id.callLl);
                solderNmeTv = itemView.findViewById(R.id.solderNmeTv);
                dojTv = itemView.findViewById(R.id.dojTv);
                mobileNOTv = itemView.findViewById(R.id.mobileNOTv);
                lbleTv = itemView.findViewById(R.id.lbleTv);
                ipTv = itemView.findViewById(R.id.ipTv);
                imgCall = itemView.findViewById(R.id.imgCall);
                district = itemView.findViewById(R.id.district);
                state = itemView.findViewById(R.id.state);
                country = itemView.findViewById(R.id.country);
                pinCode = itemView.findViewById(R.id.pinCode);

                itemView.setOnClickListener(view -> {
                    final SimpleTooltip tooltip = new SimpleTooltip.Builder(YourBusinessTeam.this)
                            .anchorView(imgCall)
                            //.text(R.string.differential_ip)
                            .gravity(Gravity.START)
                            .dismissOnOutsideTouch(true)
                            .dismissOnInsideTouch(false)
                            .modal(true).overlayWindowBackgroundColor(getResources().getColor(android.R.color.transparent))
                            .transparentOverlay(true).showArrow(false)
                            //.animated(true)
                            //.animationDuration(2000)
                            //.animationPadding(SimpleTooltipUtils.pxFromDp(50))
                            .contentView(R.layout.contact_layout)
                            .focusable(true)
                            .build();

                    ImageView FrontScreen = tooltip.findViewById(R.id.cllPhone);
                    ImageView BackScreen = tooltip.findViewById(R.id.whtasapp);

                    FrontScreen.setOnClickListener(view12 -> {
                        tooltip.dismiss();
                        phoneCall(YourBusinessTeam.this, mobileNOTv.getText().toString());
                    });

                    BackScreen.setOnClickListener(view1 -> {
                        openWhatsApp(YourBusinessTeam.this, mobileNOTv.getText().toString());
                        tooltip.dismiss();
                    });

                    tooltip.show();
                    //DialogContct();
                });
            }
        }
    }

    int count = 0;
}

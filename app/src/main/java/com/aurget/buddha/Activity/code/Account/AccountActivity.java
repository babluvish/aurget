package com.aurget.buddha.Activity.code.Account;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Database.DatabaseController;
import com.aurget.buddha.Activity.code.Database.TableFavourite;
import com.aurget.buddha.Activity.code.Fragment.DashBoardFragment;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.OrderModule.ChooseAddressListActivity;
import com.aurget.buddha.Activity.code.Search.SearchActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.BuildConfig;
import com.aurget.buddha.R;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import code.utils.AppUrls;


public class AccountActivity extends BaseActivity implements View.OnClickListener,GoogleApiClient.OnConnectionFailedListener{
    //ImageView
    ImageView ivCart,ivMenu,ivSearch,ivHome,ivCategory,ivFavourites,ivProfile,ivProfilePic;

    //LinearLayout
    LinearLayout llHome,llCategory,llFavourites,llProfile;

    //TextView
    TextView tvCount,tvEdit,tvHome,tvMain,tvCategory,tvFavourites,tvProfile,tvUserName,tvWallet,tvLogin,tvSignup;

    RelativeLayout rrOrders,rrAddress,rrWallet,rrPolicies,rrAgreement,rlProfile,rrShare,rrLogout,rlLogin,rlSignup,rrChangePassword;


    //CallbackManager callbackManager;

    private static final int RC_SIGN_IN = 9001;

    private GoogleApiClient mGoogleApiClient;

    public static GoogleSignInOptions gso;

    private boolean signedInUser;

    String facebook_id, name, email;

    String social="";

    ArrayList<String> genderList;

    private boolean mIntentInProgress;

    String gender="";

    private ConnectionResult mConnectionResult;

    EditText etDob;

    EditText edName;
    EditText edEmail;
    EditText edMobile;
    ImageView ivMen;
    ImageView ivWomen;
    RelativeLayout rrMen;
    RelativeLayout rrWomen;


    private SimpleDateFormat dateFormatter;
    private DatePickerDialog fromDatePickerDialog;

    private static String[] PERMISSIONS = { Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static final int READ_EXTERNAL_STORAGE= 1;
    private static final int WRITE_EXTERNAL_STORAGE= 1;

    private static final int SELECT_PICTURE = 1;
    public Uri fileUri;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final String IMAGE_DIRECTORY_NAME = "Hello Camera";
    String picturePath = "", filename = "", ext = "";
    public static final int MEDIA_TYPE_VIDEO = 2;
    // public static Bitmap bitmap, b1;
    String encodedString="";
    Uri picUri;
    String setPic = "", ImageName = "";
    Boolean validate = false;
    public static Bitmap bitmap;
    //ProgressBar
    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        initialise();

//        findViewById(R.id.share).setOnClickListener(v -> shareApp(mActivity));
    }

    public static void shareApp(Context context)
    {
        final String appPackageName = context.getPackageName();
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "AurGet Grocery Mart ने किराना ,सब्जी और फल की होम डिलीवरी देने के लिए AurGet Grocery ऐप्प को लॉन्च किया है। कृपया ऐप्प को डाऊनलोड करें। AurGet Grocery को सेवा का अवसर दे।"+"\n"+"https://play.google.com/store/apps/details?id=com.aurgetmart.mega");
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }
    private void initialise() {

        //TextView
        tvCount =  findViewById(R.id.tvCount);
        tvHome =   findViewById(R.id.tvHomeBottom);
        tvCategory =    findViewById(R.id.tvCategoryBottom);
        tvFavourites =  findViewById(R.id.tvFavouritesBottom);
        tvProfile =  findViewById(R.id.tvProfileBottom);
        tvMain =  findViewById(R.id.tvHeaderText);
        tvUserName =  findViewById(R.id.tvUserName);
        tvWallet =  findViewById(R.id.tv1);
        tvLogin =  findViewById(R.id.tvlogin);
        tvSignup =  findViewById(R.id.tvSignup);

        //ImageView
        ivCart =  findViewById(R.id.ivCart);
        ivMenu =  findViewById(R.id.iv_menu);
        ivSearch =  findViewById(R.id.searchmain);
        ivHome =    findViewById(R.id.ivHomeBottom);
        ivCategory =  findViewById(R.id.ivCategoryBottom);
        ivFavourites =  findViewById(R.id.ivFavouritesBottom);
        ivProfile =  findViewById(R.id.ic_profile);
        ivProfilePic =  findViewById(R.id.imageView4);

  /*      //LinearLayout fro bottom views
        llHome = (LinearLayout) findViewById(R.id.llHome);
        llCategory = (LinearLayout) findViewById(R.id.llCategory);
        llFavourites = (LinearLayout) findViewById(R.id.llFavourites);
        llProfile = (LinearLayout) findViewById(R.id.llProfile);*/

        //RelativeLayout
        rrOrders =  findViewById(R.id.rrOrders);
        rrAddress =  findViewById(R.id.rrAddress);
        rrWallet =    findViewById(R.id.rrWallet);
        rrPolicies =  findViewById(R.id.rrPolicies);
        rrShare =       findViewById(R.id.rrShare);
        rlLogin =           findViewById(R.id.rlLogin);
        rlSignup =          findViewById(R.id.rlSignUp);

        ivCart.setOnClickListener(this);
        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
//        tvEdit.setOnClickListener(this);
        rrOrders.setOnClickListener(this);
        rrAddress.setOnClickListener(this);
        rrWallet.setOnClickListener(this);
        rrPolicies.setOnClickListener(this);
//        rrChangePassword.setOnClickListener(this);
//        ivProfilePic.setOnClickListener(this);


        rrShare.setOnClickListener(this);
//        rrLogout.setOnClickListener(this);
        rlLogin.setOnClickListener(this);
        rlSignup.setOnClickListener(this);


        ivMenu.setImageResource(R.drawable.ic_back);
        tvMain.setText("My Account");
        tvWallet.setText("RS "+ AppSettings.getString(AppSettings.wallet));

        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getProfileApi();
        } else {
            AppUtils.showErrorMessage(tvCount, getString(R.string.errorInternet), mActivity);
        }
        if (AppSettings.getString(AppSettings.usertype).equals("1"))
        {
            //              rrChangePassword.setVisibility(View.VISIBLE);
        }else
        {
//                rrChangePassword.setVisibility(View.GONE);
        }
        try {
            if(AppSettings.getString(AppSettings.social).equals("1")) {
                Picasso.with(mActivity).load(AppSettings.getString(AppSettings.profilePic)).placeholder((int) R.mipmap.ic_launcher).into(ivProfilePic);
            }else {
                Picasso.with(mActivity).load(AppSettings.getString(AppSettings.user_pic)).placeholder((int) R.mipmap.ic_launcher).into(ivProfilePic);
            }
            /*Picasso.with(getApplicationContext()).load(AppSettings.getString(AppSettings.user_pic)).into(ivProfilePic, new ImageLoadedCallback(progressBar) {
                @Override
                public void onSuccess() {
                    if (progressBar != null) {
                        progressBar.setVisibility(View.GONE);
                    }
                }
            });*/
        }
        catch (Exception e)
        {
        }
    }

    private static final int REQUEST_PERMISSIONS = 1;
    public void getPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                ||ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                ||ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
        )
        {
            ActivityCompat.requestPermissions(AccountActivity.this, PERMISSIONS, REQUEST_PERMISSIONS);

        } else {

            ActivityCompat.requestPermissions(AccountActivity.this, PERMISSIONS, REQUEST_PERMISSIONS);
        }

    }


    private class ImageLoadedCallback implements Callback {
        ProgressBar progressBar;

        public  ImageLoadedCallback(ProgressBar progBar){
            progressBar = progBar;
        }

        @Override
        public void onSuccess() {
        }

        @Override
        public void onError() {

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == REQUEST_PERMISSIONS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // showCameraGalleryDialog();

            } else {
                //Toast.makeText(Drawer_Activity.this, "Permission denied", Toast.LENGTH_SHORT).show();
            }
        }
        else  if (requestCode == READ_EXTRENAL_MEDIA_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 &&grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {
            }
        }
        else   if (requestCode == READ_EXTERNAL_STORAGE) {

        }
        else    if (requestCode == WRITE_EXTERNAL_STORAGE) {


        }

        else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private static final int READ_EXTRENAL_MEDIA_PERMISSIONS_REQUEST = 1;


    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }




    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.iv_menu:
                finish();
                break;
            case R.id.imageView4:
                if (!hasPermissions(this,PERMISSIONS))
                {
                    getPermission();
                }
                else
                {
                    showCameraGalleryDialog();
                }
                break;

            case R.id.searchmain:
                startActivity(new Intent(getBaseContext(), SearchActivity.class));
                return;

            case R.id.ivCart:

                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                }
                else
                {
                    //startActivity(new Intent(getBaseContext(), CartListActivity.class));
                    startActivity(new Intent(getBaseContext(), ChooseAddressListActivity.class));
                }

                return;

            case R.id.rrOrders:
                startActivity(new Intent(getBaseContext(), OrderListActivity.class));
                return;

            case R.id.rrOffer:
                //startActivity(new Intent(getBaseContext(), CartListActivity.class));
                return;

            case R.id.rrWallet:
                startActivity(new Intent(getBaseContext(), WalletHistoryActivity.class));
                return;

            case R.id.rrPolicies:
                startActivity(new Intent(getBaseContext(), PoliciesActivity.class));
                return;
            case R.id.rrAgreement:
                startActivity(new Intent(getBaseContext(), UserAgreementActivity.class));
                return;

            case R.id.rrAddress:
                //startActivity(new Intent(getBaseContext(), AddressListActivity.class));
                startActivity(new Intent(getBaseContext(), ChooseAddressListActivity.class));
                return;

            case R.id.rlLogin:
                AlertLoginPopUp();
                return;

            case R.id.rlSignUp:
                AlertSignUpPopUp();
                return;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        int count = Integer.parseInt(AppSettings.getString(AppSettings.cartCount));

        if(count>0)
        {
            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(String.valueOf(count));
        }
        else
        {
            tvCount.setVisibility(View.GONE);
        }

        if(AppSettings.getString(AppSettings.name).equals(""))
        {
            try {
                if (AppSettings.getString(AppSettings.social).equals("1")) {
                    Picasso.with(mActivity).load(AppSettings.getString(AppSettings.profilePic)).placeholder((int) R.mipmap.ic_launcher).into(ivProfilePic);
                } else if (AppSettings.getString(AppSettings.social).equals("2")) {

                } else {
                    Picasso.with(mActivity).load(AppSettings.getString(AppSettings.user_pic)).placeholder((int) R.mipmap.ic_launcher).into(ivProfilePic);

                }
            }catch(IllegalArgumentException ex){

            }
            if(AppSettings.getString(AppSettings.social).equals("1")) {
                //tvUserName.setText(AppSettings.getString(AppSettings.googleName));
            }
            else if(AppSettings.getString(AppSettings.social).equals("2")) {
                //tvUserName.setText(AppSettings.getString(AppSettings.fbname));
            }
            else{
//                tvUserName.setText(AppSettings.getString(AppSettings.name));
            }
        }
        else
        {
//            tvUserName.setText(getString(R.string.hi));
        }
    }

    //=====================================================AlertPopUp===========================================//
    //AlertPopUp
    private void AlertPopUp() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        final Dialog dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alertprofile);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();

        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        //TextView
        TextView tvYes     = dialog.findViewById(R.id.tvOk);
        TextView tvCancel  = dialog.findViewById(R.id.tvcancel);

        edName                   = dialog.findViewById(R.id.edName);
        edEmail                  = dialog.findViewById(R.id.edEmail);
        edMobile                = dialog.findViewById(R.id.edMobile);
        etDob                                  = dialog.findViewById(R.id.etDob);

        ivMen                   =dialog.findViewById(R.id.imageView11);
        ivWomen                 =dialog.findViewById(R.id.imageView12);

        rrMen    =   dialog.findViewById(R.id.rrMen);
        rrWomen  = dialog.findViewById(R.id.rrWomen);

        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        setDateTimeField();

       /* edEmail.setText(AppSettings.getString(AppSettings.email));
        edName.setText(AppSettings.getString(AppSettings.name));
        edMobile.setText(AppSettings.getString(AppSettings.mobile));
        etDob.setText(AppSettings.getString(AppSettings.dob));
        */


        if(AppSettings.getString(AppSettings.gender).equals("1"))
        {
            ivMen.setImageResource(R.drawable.ic_check_box_unselected);
            ivWomen.setImageResource(R.drawable.ic_check_box_selected);
            gender="1";
            //women==1
            //men==0
        }
        else
        {
            ivMen.setImageResource(R.drawable.ic_check_box_selected);
            ivWomen.setImageResource(R.drawable.ic_check_box_unselected);
            gender="0";
        }

        dialog.show();

        rrMen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ivMen.setImageResource(R.drawable.ic_check_box_selected);
                ivWomen.setImageResource(R.drawable.ic_check_box_unselected);
                gender="0";
            }
        });

        rrWomen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivMen.setImageResource(R.drawable.ic_check_box_unselected);
                ivWomen.setImageResource(R.drawable.ic_check_box_selected);
                gender="1";
            }
        });


        if(AppSettings.getString(AppSettings.social).equals("2")) {
            edName.setText(AppSettings.getString(AppSettings.fbname));
            edMobile.setText("");
            etDob.setText("");


        }else if(AppSettings.getString(AppSettings.social).equals("1")){
            edEmail.setText(AppSettings.getString(AppSettings.googleEmail));
            edName.setText(AppSettings.getString(AppSettings.googleName));
            edMobile.setText(AppSettings.getString(AppSettings.mobile));
            etDob.setText("");
        }else{
            edEmail.setText(AppSettings.getString(AppSettings.email));
            edName.setText(AppSettings.getString(AppSettings.name));
            edMobile.setText(AppSettings.getString(AppSettings.mobile));
            etDob.setText(AppSettings.getString(AppSettings.dob));
        }
        tvYes.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(edName.getText().toString().isEmpty())
                {
                    AppUtils.showErrorMessage(edName,getString(R.string.errorName),mActivity);
                }
                else if(gender.equals(""))
                {
                    AppUtils.showErrorMessage(edName,getString(R.string.errorGender),mActivity);
                }
                else if(edMobile.getText().toString().trim().length()>12)
                {
                    AppUtils.showErrorMessage(edName,getString(R.string.errorProperMobileNumber),mActivity);
                }
                else if(edMobile.getText().toString().trim().length()<9)
                {
                    AppUtils.showErrorMessage(edName,getString(R.string.errorProperMobileNumber),mActivity);
                }
                else  if(edEmail.getText().toString().isEmpty())
                {
                    AppUtils.showErrorMessage(edName,getString(R.string.errorMail),mActivity);
                }
                else  if(!AppUtils.isEmailValid(edEmail.getText().toString()))
                {
                    AppUtils.showErrorMessage(edName,getString(R.string.errorProperMail),mActivity);
                }
                else
                {
                    dialog.dismiss();
                    /*Holding data from  user input side*/
                    AppSettings.putString(AppSettings.name,edName.getText().toString().trim());
                    AppSettings.putString(AppSettings.email,edEmail.getText().toString().trim());
                    AppSettings.putString(AppSettings.gender,gender);
                    AppSettings.putString(AppSettings.dob,etDob.getText().toString().trim());
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                        updateApi(edName.getText().toString().trim(),gender,edEmail.getText().toString().trim(),etDob.getText().toString(),edMobile.getText().toString());
                    } else {
                        AppUtils.showErrorMessage(tvCount, getString(R.string.errorInternet), mActivity);
                    }
                }

            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        etDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fromDatePickerDialog.show();
            }
        });

    }


    private void setDateTimeField() {
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                etDob.setText(dateFormatter.format(newDate.getTime()));
            }
        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
    }

    private void AlertLoginPopUp() {

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        final Dialog dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_login2);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.show();

        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        try
        {   Googleplus();
        }
        catch (Exception e)
        {

        }
        //Calling Facebook
        //facebook();

        ImageView  ivCross;
        final EditText etEmail;
        final EditText etPassword;

        Button btnLogin;

        LinearLayout llFacebook,llGoogle;

        ivCross      =dialog.findViewById(R.id.ivCross);
        etEmail      =dialog.findViewById(R.id.etEmail);
        etPassword   =dialog.findViewById(R.id.etPassword);

        btnLogin     =dialog.findViewById(R.id.btnLogin);

        llFacebook   =dialog.findViewById(R.id.llFacebook);
        llGoogle     =dialog.findViewById(R.id.llGoogle);


        ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etEmail.getText().toString().isEmpty())
                {
                    Toast.makeText(AccountActivity.this, "Please enter your email", Toast.LENGTH_SHORT).show();
                }
                else if (etPassword.getText().toString().isEmpty())
                {
                    Toast.makeText(AccountActivity.this, "Please enter your password", Toast.LENGTH_SHORT).show();
                }
                else if (etPassword.getText().toString().length()<6)
                {
                    Toast.makeText(AccountActivity.this, "Please enter your minimum 6 digit paasword", Toast.LENGTH_SHORT).show();
                }
                // dialog.dismiss();
            }
        });

        llGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                signIn();
            }
        });

        llFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //LoginManager.getInstance().logInWithReadPermissions(AccountActivity.this, Arrays.asList("public_profile", "user_friends", "email"));
            }
        });



        //TextView



        dialog.show();


    }

    private void AlertSignUpPopUp() {

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        final Dialog dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.signup);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.show();

        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        //Calling Google
        try
        {   Googleplus();
        }
        catch (Exception e)
        {

        }


        //Calling Facebook
        //facebook();


        genderList=new ArrayList<>();

        genderList.add("Select Gender");
        genderList.add("Male");
        genderList.add("Female");

        //TextView

        Spinner spinnerGender=dialog.findViewById(R.id.SpinnerGender);

        spinnerGender.setAdapter(new spinnerAdapter(getApplicationContext(), R.layout.spinner_layout, (ArrayList<String>) genderList));


        final ImageView  ivCross,ivCheck;
        final EditText etEmail;
        final EditText etPassword,etName,etRetypePassword;

        Button btnSignup;

        LinearLayout llFacebook,llGoogle;

        ivCross      =dialog.findViewById(R.id.ivCross);
        ivCheck      =dialog.findViewById(R.id.ivCheck);
        etEmail      =dialog.findViewById(R.id.etEmail);
        etPassword   =dialog.findViewById(R.id.etPassword);
        etRetypePassword   =dialog.findViewById(R.id.etReenterPassword);
        etName   =dialog.findViewById(R.id.etName);

        btnSignup     =dialog.findViewById(R.id.btnSignUp);

        llFacebook   =dialog.findViewById(R.id.llFacebook);
        llGoogle     =dialog.findViewById(R.id.llGoogle);


        ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etEmail.getText().toString().isEmpty())
                {
                    Toast.makeText(AccountActivity.this, "Please enter your email", Toast.LENGTH_SHORT).show();
                }
                else if (!AppUtils.isEmailValid(etEmail.getText().toString()))
                {
                    Toast.makeText(AccountActivity.this, "Please enter valid email id ", Toast.LENGTH_SHORT).show();
                }
                else if (etName.getText().toString().isEmpty())
                {
                    Toast.makeText(AccountActivity.this, "Please enter your name ", Toast.LENGTH_SHORT).show();
                }
                else if (etPassword.getText().toString().isEmpty())
                {
                    Toast.makeText(AccountActivity.this, "Please enter your password", Toast.LENGTH_SHORT).show();
                }
                else if (etPassword.getText().toString().length()<6)
                {
                    Toast.makeText(AccountActivity.this, "Please enter your minimum 6 digit paasword", Toast.LENGTH_SHORT).show();
                }
                else if (etRetypePassword.getText().toString().isEmpty())
                {
                    Toast.makeText(AccountActivity.this, "Please enter  re-enter your password", Toast.LENGTH_SHORT).show();
                }
                else if (etRetypePassword.getText().toString().length()<6)
                {
                    Toast.makeText(AccountActivity.this, "Please enter  re-enter your minimum 6 digit password", Toast.LENGTH_SHORT).show();
                }

            }
        });


        llGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

        llFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //LoginManager.getInstance().logInWithReadPermissions(AccountActivity.this, Arrays.asList("public_profile", "user_friends", "email"));
            }
        });


        ivCheck.setOnClickListener(new View.OnClickListener() {
            int status=0;
            @Override
            public void onClick(View view) {
                if (status==0)
                {
                    ivCheck.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_box_selected));
                    status=1;
                }
                else
                {
                    ivCheck.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_box_unselected));
                    status=0;
                }

            }
        });


        dialog.show();


    }


    private void AlertLogoutPopUp() {

        final Dialog dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alertyesno);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();

        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        //TextView
        TextView tvYes                          = dialog.findViewById(R.id.tvOk);
        TextView tvCancel                       = dialog.findViewById(R.id.tvcancel);
        TextView tvReason                       = dialog.findViewById(R.id.textView22);



        tvReason.setText("Are you sure you want to logout \n of this app?");

        dialog.show();

        tvYes.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                dialog.dismiss();

                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    LogoutApi();
                } else {
                    AppUtils.showErrorMessage(tvCount, getString(R.string.errorInternet), mActivity);
                }


                onResume();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }


    //=====================================================Google Plus==================================================//
    public void Googleplus() {
        //GooglePlus
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();



        /*mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
*/
        mGoogleApiClient.connect();
        super.onStart();


    }

    private void signIn() {


        /*Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
*/
        if (!mGoogleApiClient.isConnecting()) {
            signedInUser = true;

        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (!connectionResult.hasResolution()) {

            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this, 0).show();

            return;

        }

        if (!mIntentInProgress) {

            // store mConnectionResult

            mConnectionResult = connectionResult;

            if (signedInUser) {


            }

        }
    }

    //===========================Facebook=============================//
    /*public void facebook() {

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {


            @Override
            public void onSuccess(LoginResult loginResult) {
                //loader.show();
                facebookSuccess(loginResult);


            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }


        });


    }*/

    /*public void facebookSuccess(LoginResult loginResult) {
        AccessToken accessToken = loginResult.getAccessToken();
        com.facebook.Profile profile = com.facebook.Profile.getCurrentProfile();

        if (profile != null) {
            facebook_id = profile.getId();
            name = profile.getName();

        }
        // Facebook Email address
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        Log.v("LoginActivity Response ", response.toString());

                        try {
                         *//*   pref.set(AppConstants.user_name, object.getString("name"));
                            social=object.getString("id");
                            pref.set(AppConstants.user_type,"1");

                            pref.commit();
                            if (Utils.isNetworkConnectedMainThred(getApplicationContext())) {
                                loader.show();
                                loader.setCancelable(false);
                                loader.setCanceledOnTouchOutside(false);
                                Hit_Login_Api("2");

                            } else {
                                Toast.makeText(getApplicationContext(), R.string.no_internet, Toast.LENGTH_SHORT).show();
                            }*//*

                            // FormDialog();


                            LoginManager.getInstance().logOut();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });


        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name");
        request.setParameters(parameters);
        request.executeAsync();
    }*/


    //==========================================+++++++API================================================//


    private void updateApi(String name,String gender,String email,String dob,String mobile) {

        AppUtils.showRequestDialog(mActivity);
        Log.v("updateApi", AppUrls.updateUserProfile);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("id", AppSettings.getString(AppSettings.userId));
            json_data.put("username", name);
            json_data.put("email_id", email);
            json_data.put("gender", gender);
            json_data.put("phone_no", mobile);
            json_data.put("dob", dob);
            if(!AppSettings.getString(AppSettings.social).equals("")) {
                json_data.put("profile_pic", "");
            }else{
                json_data.put("profile_pic",encodedString);
            }
            json.put(AppConstants.result, json_data);
            Log.v("updateApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.updateUserProfile)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        parsedata(response);
                    }
                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvCount, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvCount, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedata(JSONObject response) {
        Log.d("response ", response.toString());
        DatabaseController.removeTable(TableFavourite.favourite);

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(tvCount, String.valueOf(jsonObject.getString("res_msg")), mActivity);
                if(AppSettings.getString(AppSettings.social).equals("1")) {
                    tvUserName.setText(AppSettings.getString(AppSettings.googleName));
                }
                else if(AppSettings.getString(AppSettings.social).equals("2")) {
                    tvUserName.setText(AppSettings.getString(AppSettings.fbname));
                }
                else{
                    tvUserName.setText(AppSettings.getString(AppSettings.name));
                }

                edEmail.setText(AppSettings.getString(AppSettings.email));
                edName.setText(AppSettings.getString(AppSettings.name));
                edMobile.setText(AppSettings.getString(AppSettings.mobile));
                etDob.setText(AppSettings.getString(AppSettings.dob));

            }
            else
            {
                AppUtils.showErrorMessage(tvCount, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvCount, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();

    }



    private void updateProfileApi() {


        //http://aurget.in/mart/demo/api/Sends/update_profile?id=9&profile_image=&extensions=png
        AppUtils.showRequestDialog(mActivity);
        Log.v("updateApi", AppUrls.updateUserProfile);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        /*try {
            json_data.put("id", AppSettings.getString(AppSettings.userId));
            json_data.put("username",    AppSettings.getString(AppSettings.name));
            json_data.put("email_id",    AppSettings.getString(AppSettings.email));
            json_data.put("gender",      AppSettings.getString(AppSettings.gender));
            json_data.put("phone_no",    AppSettings.getString(AppSettings.mobile));
            json_data.put("profile_pic", encodedString);
            json_data.put("dob",         AppSettings.getString(AppSettings.dob));
            json.put(AppConstants.result, json_data);
            Log.v("dhfg", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        try {
            json_data.put("id", AppSettings.getString(AppSettings.userId));
            json_data.put("profile_image", "sssss");
            json_data.put("extensions","jpg");
            json.put(AppConstants.result, json_data);
            Log.v("dhfg", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AsyncDataClass asyncRequestObject = new AsyncDataClass();
        asyncRequestObject.execute(AppUrls.update_profile,
                AppSettings.getString(AppSettings.userId),
                encodedString,
                "jpg"
        );

     /*   new AbstrctClss(mActivity, ConstntApi.updateProfileApi(mActivity, json_data), "p", true)
        {
            @Override
            public void responce(String s) {
                AppUtils.showErrorMessage(tvCount, s , mActivity);
            }

            @Override
            public void onErrorResponsee(String s) {
                AppUtils.showErrorMessage(tvCount, s , mActivity);
            }
        };*/


      /*  AndroidNetworking.post("http://aurget.in/mart/demo/api/Sends/update_profile")
                .addJSONObjectBody(json_data)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedataProfile(response);
                    }
                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvCount, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvCount, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });*/
    }

    private void parsedataProfile(JSONObject response) {
        Log.d("response ", response.toString());
        DatabaseController.removeTable(TableFavourite.favourite);
        try {
            //JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            //if (response.getString("Status").equals("1")) {
            AppUtils.showErrorMessage(tvCount, String.valueOf(response.getString("message")), mActivity);

            if(AppSettings.getString(AppSettings.social).equals("1")) {
                tvUserName.setText(AppSettings.getString(AppSettings.googleName));
            }
            else if(AppSettings.getString(AppSettings.social).equals("2")) {
                tvUserName.setText(AppSettings.getString(AppSettings.fbname));
            }
            else{
                tvUserName.setText(AppSettings.getString(AppSettings.name));
            }
            //}
            /*else
            {*/
            //   AppUtils.showErrorMessage(tvCount, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            //}
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvCount, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();

    }

    private void getProfileApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("getProfileApi", AppUrls.getUserDetail);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("user_id", AppSettings.getString(AppSettings.userId));
            json.put(AppConstants.result, json_data);
            Log.v("getProfileApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.getUserDetail)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseProfiledata(response);
                    }
                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseProfiledata(JSONObject response) {

        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                JSONObject userObject = jsonObject.getJSONObject("user_detail");
                // Toast.makeText(this, userObject.getString("username"), Toast.LENGTH_LONG).show();
                AppSettings.putString(AppSettings.name,userObject.getString("username"));
                AppSettings.putString(AppSettings.email,userObject.getString("email_id"));
                AppSettings.putString(AppSettings.gender,userObject.getString("gender"));
                AppSettings.putString(AppSettings.mobile,userObject.getString("phone_no"));
                AppSettings.putString(AppSettings.wallet,userObject.getString("wallet_amount"));
                AppSettings.putString(AppSettings.dob,userObject.getString("dob"));
                AppSettings.putString(AppSettings.user_pic,userObject.getString("profile_pic"));
                AppSettings.putString(AppSettings.referralId,userObject.getString("referel_id"));
                tvWallet.setText("RS "+ AppSettings.getString(AppSettings.wallet));
            }
            else
            {
                AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
    }

    private void LogoutApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("getProfileApi", AppUrls.Logout);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_id", AppSettings.getString(AppSettings.userId));
            json_data.put("device_id", AppUtils.getDeviceID(getApplicationContext()));
            json.put(AppConstants.result, json_data);

            Log.v("getProfileApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.Logout)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseLogout(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }
    private void parseLogout(JSONObject response) {
        Log.d("response ", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                Toast.makeText(this, jsonObject.getString("res_msg"), Toast.LENGTH_SHORT).show();
                AppSettings.putString(AppSettings.mobile,"");
                AppSettings.putString(AppSettings.verified,"");
                AppSettings.putString(AppSettings.userId,"");
                AppSettings.putString(AppSettings.cartCount,"0");


                //userprofile data
                AppSettings.putString(AppSettings.name,"");
                AppSettings.putString(AppSettings.email,"");
                AppSettings.putString(AppSettings.gender,"");
                AppSettings.putString(AppSettings.mobile,"");
                AppSettings.putString(AppSettings.wallet,"");
                AppSettings.putString(AppSettings.dob,"");
                AppSettings.putString(AppSettings.user_pic,"");
                AppSettings.putString(AppSettings.referralId,"");
                AppSettings.putString(AppSettings.profilePic,"");



                AppSettings.putString(AppSettings.googleName,"");
                AppSettings.putString(AppSettings.googleEmail,"");
                AppSettings.putString(AppSettings.googledob,"");
                AppSettings.putString(AppSettings.googleId,"");


                AppSettings.putString(AppSettings.fbname,"");
                AppSettings.putString(AppSettings.fbId,"");

                startActivity(new Intent(this, DashBoardFragment.class));
            }
            else
            {
                AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
    }


    //==============================Spinner Adapter==========================================================//
    public class spinnerAdapter extends ArrayAdapter<String> {

        ArrayList<String> data;

        public spinnerAdapter(Context context, int textViewResourceId, ArrayList<String> arraySpinner_time) {
            super(context, textViewResourceId, arraySpinner_time);
            this.data = arraySpinner_time;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.spinner_layout, parent, false);
            TextView label =  row.findViewById(R.id.tvName);
            //   label.setTypeface(typeface);
            label.setText(data.get(position));

            return row;
        }
    }


    //===============================================Gallery==========================================//
    public void showCameraGalleryDialog() {

        final Dialog dialog = new Dialog(this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(true);

        dialog.setContentView(R.layout.camera_gallery_popup);

        dialog.show();

        RelativeLayout rrCancel =  dialog.findViewById(R.id.rr_cancel);
        LinearLayout llCamera =  dialog.findViewById(R.id.ll_camera);
        LinearLayout llGallery =  dialog.findViewById(R.id.ll_gallery);
        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImage();
                dialog.dismiss();
            }
        });


        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(photoPickerIntent, SELECT_PICTURE);

                dialog.dismiss();
            }
        });

        rrCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    //============================Code for Camera and Gallary===================================//

    private void captureImage() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            fileUri = FileProvider.getUriForFile(mActivity, BuildConfig.APPLICATION_ID + ".provider", getOutputMediaFile(MEDIA_TYPE_IMAGE));
            AppSettings.putString(AppSettings.image_path, String.valueOf(fileUri));
            Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            it.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(it, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
        } else {
            // create Intent to take a picture and return control to the calling application
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE); // create a file to save the image
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name
            // start the image capture Intent
            startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
        }

       /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);*/
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }


    public static int getImageOrientation(String imagePath) {
        int rotate = 0;
        try {
            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(
                    imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rotate;
    }

    //method to convert string into base64
    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }

    public void setPictures(Bitmap b, String setPic, String base64) {

        //profile.setImageBitmap(b);
    }

    public static String getFileType(String path) {


        String fileType = null;
        fileType = path.substring(path.indexOf('.', path.lastIndexOf('/')) + 1).toLowerCase();
        return fileType;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (fileUri == null) {

            fileUri = Uri.parse(AppSettings.getString(AppSettings.image_path));
            picturePath = fileUri.getPath();

        } else {
            if (!fileUri.equals(""))
                picturePath = fileUri.getPath();
        }
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//
//            if (result.isSuccess()) {
//
//                GoogleSignInAccount acct = result.getSignInAccount();
//
//                social=acct.getId();
//
//                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
//                        new ResultCallback<Status>() {
//                            @Override
//                            public void onResult(Status status) {
//                                mGoogleApiClient.disconnect();
//                            }
//                        });
            //}
        }

        else if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE)
        {
            if (resultCode == RESULT_OK) {
                try {
                    picturePath = fileUri.getPath().toString();
                }
                catch (Exception e)
                {
                    Log.v("hello1234",e.getMessage());
                }
                filename = picturePath.substring(picturePath.lastIndexOf("/") + 1);
                Log.d("filename_camera", filename);
                String selectedImagePath = picturePath;
                Uri uri = Uri.parse(picturePath);
                ivProfilePic.setImageURI(uri);
                ext = "jpg";
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 500;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bitmap = BitmapFactory.decodeFile(selectedImagePath, options);
                Matrix matrix = new Matrix();
                matrix.postRotate(getImageOrientation(picturePath));
                Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                ByteArrayOutputStream bao = new ByteArrayOutputStream();
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                byte[] ba = bao.toByteArray();

                encodedString=getEncoded64ImageStringFromBitmap(bitmap);
                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    updateProfileApi();
                } else {
                    AppUtils.showErrorMessage(tvCount, getString(R.string.errorInternet), mActivity);
                }
            }
        } else if (requestCode == SELECT_PICTURE) {
            if (data != null) {
                Uri contentURI = data.getData();
                //get the Uri for the captured image
                picUri = data.getData();

                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(contentURI, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                filename = picturePath.substring(picturePath.lastIndexOf("/") + 1);
                ivProfilePic.setImageURI(picUri);
                Log.d("path", picturePath);
                System.out.println("Image Path : " + picturePath);
                cursor.close();
                filename = picturePath.substring(picturePath.lastIndexOf("/") + 1);

                ext = getFileType(picturePath);

                String selectedImagePath = picturePath;

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 500;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bitmap = BitmapFactory.decodeFile(selectedImagePath, options);

                /*Matrix matrix = new Matrix();
                matrix.postRotate(getImageOrientation(picturePath));
                Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                ByteArrayOutputStream bao = new ByteArrayOutputStream();
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                byte[] ba = bao.toByteArray();*/

                try {
                    Bitmap panImgPthBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), picUri);
                    //passbookImgExt = fileExe(picUri);
                    encodedString = bitMap(panImgPthBitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //encodedString = getEncoded64ImageStringFromBitmap(bitmap);
                System.out.println(""+encodedString);

                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    updateProfileApi();
                } else {
                    AppUtils.showErrorMessage(tvCount, getString(R.string.errorInternet), mActivity);
                }

                //Facebook

                try {
                    //callbackManager.onActivityResult(requestCode, resultCode, data);
                }
                catch (Exception e)
                {

                }
            }
        }
    }

    public static String fileExe(Uri uri) {
        String fileExt = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
        return "." + fileExt;
    }

    public static String bitMap(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return imageString;
    }

    private class AsyncDataClass extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            HttpParams httpParameters = new BasicHttpParams();
//
//            HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
//
//            HttpConnectionParams.setSoTimeout(httpParameters, 5000);

            HttpClient httpClient = new DefaultHttpClient(httpParameters);

            HttpPost httpPost = new HttpPost(params[0]);

            String jsonResult = "";

            try {

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("id", params[1]));
                nameValuePairs.add(new BasicNameValuePair("profile_image", params[2]));
                nameValuePairs.add(new BasicNameValuePair("extensions", params[3]));

                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = httpClient.execute(httpPost);

                jsonResult = inputStreamToString(response.getEntity().getContent()).toString();

                System.out.println("Returned Json object " + jsonResult.toString());

            } catch (ClientProtocolException e) {

                e.printStackTrace();

            } catch (IOException e) {

                e.printStackTrace();
            }
            return jsonResult;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            System.out.println("Resulted Value: " + result);
            try
            {
                AppUtils.showErrorMessage(tvCount, new JSONObject(result).optString("message"), mActivity);
                AppUtils.hideDialog();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static StringBuilder inputStreamToString(InputStream is) {

        String rLine = "";

        StringBuilder answer = new StringBuilder();

        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        try {

            while ((rLine = br.readLine()) != null) {

                answer.append(rLine);

            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return answer;
    }
}

package code.utility;

public class Constants {
	public static final String PARAMETER_SEP = "&";
	public static final String PARAMETER_EQUALS = "=";
	public static final String TRANS_URL = "https://secure.ccavenue.com/transaction/initTrans";
	public static final String REDIRECT_URL = "https://mrnmrsekart.com/paymentGateway/ccavResponseHandler.php";
	public static final String CANCEL_URL = "https://mrnmrsekart.com/paymentGateway/ccavResponseHandler.php";
	public static final String RSA_KEY_URL = "https://mrnmrsekart.com/paymentGateway/GetRSA.php";
	public static final String ACCESS_CODE = "AVNB75FA68BO55BNOB";
	public static final String MERCHANT_ID = "159185";
	public static final String MYJSON = "";

}
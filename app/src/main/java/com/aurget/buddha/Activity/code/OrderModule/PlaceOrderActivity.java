package com.aurget.buddha.Activity.code.OrderModule;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;

import com.aurget.buddha.Activity.BuyNowCart;
import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.RajorPayment.CheckoutActivity;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.categories.BuyNow;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import androidx.core.app.ActivityCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.code.Account.AccountActivity;
import com.aurget.buddha.Activity.code.Account.OrderDetailActivity;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Fragment.DashBoardFragment;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.Activity.code.volly.AbstrctClss;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import code.utils.AppUrls;
import code.volly.ConstntApi;

import static java.lang.Double.parseDouble;

public class PlaceOrderActivity extends BaseActivity implements View.OnClickListener {


    /*PayUmoneySdkInitializer.PaymentParam.Builder builder = new PayUmoneySdkInitializer.PaymentParam.Builder();
    //declare paymentParam object
    PayUmoneySdkInitializer.PaymentParam paymentParam = null;*/

    String TAG = "StartPaymentActivity", txnid = "", amount = "", phone = "",
            prodname = "", firstname = "", email = "",
            merchantId = "", merchantkey = "";  //   first test key only.

    String merchantHash = "";
    String p_type = "";


    Context context;
    ArrayList<HashMap<String, String>> cartList = new ArrayList();
    ArrayList<String> slot = new ArrayList<>();

    //ImageView header
    ImageView ivMenu, ivSearch, ivCart, ivUseWallet, ivCredit, ivNet, ivCOD;

    //RelativeLayout
    RelativeLayout rrCredit, rrNet, rrCOD, rrWallet,llCoupanAvailability;

    //TextView
    TextView tvMain, tvCount, tvAddress1, tvAddress2, tvMobile, tvChange,
            tvApplyCoupon, tvPrice, tvTotal, tvDiscount, tvPrices, tvProceed,
            tvShipAmount, tvWallet, tvDiscountAmount, tvValue, textViewCoupanPrice;

    //EditText
    EditText edCoupon, etDob, etAdditionalNotes;

    LinearLayout  llValue;
    Spinner spinnerSlot;
    float y;
    String finalPrice = "", id;

    TextView tvBtnRemove, tvBtnApply, tvCoupanAvailablity;
    int credit = 0, net = 0, cod = 0;
    int coupaDiscountAmount = 0;
    float minAmount = 0;
    //BottomSheetDialog
    BottomSheetDialog bottomSheetDialog;
    float wallet = 0;
    RelativeLayout rrCoupanAmount;
    int check = 0, payment = 1;
    LinearLayoutManager linearLayoutManager;
    RecyclerView rcvList;
    LinearLayout llPayment;

    private SimpleDateFormat dateFormatter;
    private DatePickerDialog fromDatePickerDialog;
    ArrayList<HashMap<String, String>> coupanList = new ArrayList();

    RelativeLayout rlCod, rlOnline;
    ImageView ivCodcheck, ivCodUncheck, ivOnlineCheck, ivOnlineUncheck;

    RadioButton rbCod, rbOnline;

    TextView  subTOtTv, shippingchrgTv, wlletTv, priceBottomTv;
    TextView  totalTv,walletDeductAmount;
    TextView  totlIPTv;
    TextView soldTv, wallet_BalanceTv;
    TextView  saveWallet;
    String  walletAmt = "";
    EditText qtyEt;
    ImageView imageView1, imageView2, colorImg;
    Switch wlletSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_coupon_page);
        initialise();

        new com.aurget.buddha.Activity.Utils.AbstrctClss(mActivity, com.aurget.buddha.Activity.Utils.ConstntApi.grocery_wallet(mActivity), "p", true) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s);
                    walletAmt = jsonArray.optJSONObject(0).optString("Wallet_Balance");
                    //wlletTv.setText("₹" + walletAmt);
                    minWalAmt = Double.parseDouble(jsonArray.optJSONObject(0).optString("apply_min_amount"));
                    Message = jsonArray.optJSONObject(0).optString("Message");
                    wallet_BalanceTv.setText(walletAmt);
                    Commonhelper.sop("minWalAmt"+minWalAmt);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };

        wlletSwitch.setChecked(false);
        wlletSwitch.setOnCheckedChangeListener((compoundButton, b) -> {

            if (parseDouble(walletAmt) < 1) {
                wlletSwitch.setChecked(false);
                Commonhelper.showToastLong(mActivity, Constnt.insufficientwalletbalance);
                return;
            }

            try {
                if (totamt2 < minWalAmt && wlletSwitch.isChecked()) {
                    wlletSwitch.setChecked(false);
                    Commonhelper.showToastLong(mActivity, Message);
                    return;
                }
                if (b) {
                    if (parseDouble(walletAmt) > totamt2) {
                        totalAmt = totamt2;
                        tvTotal.setText("₹0.00");
                        //  wlletTv.setText("₹" + Commonhelper.roundOff_(Double.valueOf(totalAmt)));
                        double wallet3 = parseDouble(walletAmt) - totalAmt;
                        deductAmt = totalAmt;
                        wallet_BalanceTv.setText("" + Commonhelper.roundOff_(Double.valueOf(wallet3)));
                        saveWallet.setText("You will save ₹" + totalAmt + " using ₹+" + totalAmt + " Wallet/Cashback");
                        walletDeductAmount.setText("₹"+deductAmt);
                    } else {
                        totalAmt = totamt2;
                        double totAmot = totalAmt - parseDouble(walletAmt);
                        deductAmt = Double.parseDouble(walletAmt);
                        walletDeductAmount.setText("₹"+deductAmt);
                       // tvTotal.setText("" + Commonhelper.roundOff_(totAmot));
                        tvTotal.setText("₹" + Commonhelper.roundOff_(totAmot));
                        // wlletTv.setText("₹" + Commonhelper.roundOff_(Double.valueOf(walletAmt)));
                        wallet_BalanceTv.setText("0.00");
                        saveWallet.setText("You will save ₹" + walletAmt + " using ₹" + walletAmt + " Wallet/Cashback");
                    }
                } else {
                    saveWallet.setText("You will save ₹0 using ₹0 Wallet/Cashback");
                    wallet_BalanceTv.setText("" + Commonhelper.roundOff_(Double.valueOf(walletAmt)));
                    //wlletTv.setText("₹0.00");
                    deductAmt = 0.00;
                    walletDeductAmount.setText("₹"+deductAmt);
                    tvTotal.setText("₹" + totamt2);
                }
            } catch (Exception e) {
                Commonhelper.sop("Exception" + e.getMessage());
            }
        });

    }

    private Double calcTotAmt(String qty) {
        Double aDouble = 0.0;
        try {
            aDouble = parseDouble(qty) * totPrice;
            String ouble = Commonhelper.roundOff(aDouble);
            return Double.parseDouble(ouble);
        } catch (NumberFormatException e) {
            return 0.0;
        }
    }

    private void initialise() {
        //ImageView
        ivMenu = findViewById(R.id.iv_menu);
        walletDeductAmount = findViewById(R.id.walletDeductAmount);
        ivSearch = findViewById(R.id.searchmain);
        ivCart = findViewById(R.id.ivCart);
        ivUseWallet = findViewById(R.id.ivWalletCheck);
        ivCredit = findViewById(R.id.imageView6);
        ivNet = findViewById(R.id.imageView7);
        ivCOD = findViewById(R.id.imageView8);
        ivCodcheck = findViewById(R.id.ivCodcheck);
        ivCodUncheck = findViewById(R.id.ivCodUncheck);
        ivOnlineCheck = findViewById(R.id.ivOnlineCheck);
        ivOnlineUncheck = findViewById(R.id.ivOnlineUncheck);

        //RelativeLayout
        rlCod = findViewById(R.id.rlCod);
        rlOnline = findViewById(R.id.rlOnline);

        // RadioButton
        rbCod = findViewById(R.id.rbCod);
        rbOnline = findViewById(R.id.rbOnline);

        //TextView
        tvBtnRemove = findViewById(R.id.tvBtnRemove);
        tvBtnApply = findViewById(R.id.tvBtnApply);
        tvCoupanAvailablity = findViewById(R.id.tvCoupanMessage);
        tvCount = findViewById(R.id.tvCount);
        tvMain = findViewById(R.id.tvHeaderText);
        tvProceed = findViewById(R.id.tvOrder);
        tvAddress1 = findViewById(R.id.textView4);
        tvAddress2 = findViewById(R.id.textView3);
        tvMobile = findViewById(R.id.textView5);
        tvChange = findViewById(R.id.textView6);
        tvApplyCoupon = findViewById(R.id.textView17);
        tvPrice = findViewById(R.id.textView12);
        tvTotal = findViewById(R.id.textView14);
        tvDiscount = findViewById(R.id.textView16);
        tvPrices = findViewById(R.id.textView11);
        tvShipAmount = findViewById(R.id.tvShipAmount);
        tvWallet = findViewById(R.id.tv2);
        tvDiscountAmount = findViewById(R.id.tvDiscountAmount);

        llPayment = findViewById(R.id.llPayment);

        //EditText
        edCoupon = findViewById(R.id.edName);
        etDob = findViewById(R.id.etDob);
        etAdditionalNotes = findViewById(R.id.etAdditionalNotes);
        tvValue = findViewById(R.id.tvvalue);
        textViewCoupanPrice = findViewById(R.id.textViewCoupanPrice);

        rrCredit = findViewById(R.id.rrCredit);
        rrNet = findViewById(R.id.rrNet);
        rrCOD = findViewById(R.id.rrCOD);
        rrWallet = findViewById(R.id.rrWallet);
        rrCoupanAmount = findViewById(R.id.rrCoupanAmount);

        //LinearLayout
        llCoupanAvailability = findViewById(R.id.llCoupanAvailability);
        llValue = findViewById(R.id.llValue);

        ivMenu.setImageResource(R.drawable.ic_back);


        slot = new ArrayList<>();
        slot.add("Select a delivery slot");
        slot.add("8.00 AM-10.00 AM");
        slot.add("10.00 AM-1.00 PM");
        slot.add("3.00 PM-5.00 PM");
        slot.add("6.00 PM-7.30 PM");

        llCoupanAvailability.setOnClickListener(this);
        spinnerSlot = findViewById(R.id.spinnerSlot);


        subTOtTv = findViewById(R.id.subTOtTv);
        soldTv = findViewById(R.id.soldTv);
        wlletTv = findViewById(R.id.wlletTv);
        qtyEt = findViewById(R.id.qtyEt);
        imageView1 = findViewById(R.id.img);
        imageView2 = findViewById(R.id.img2);
        colorImg = findViewById(R.id.colorImg);

        subTOtTv = findViewById(R.id.subTOtTv);
        shippingchrgTv = findViewById(R.id.shippingchrgTv);
        totalTv = findViewById(R.id.totalTv);
        wallet_BalanceTv = findViewById(R.id.Wallet_BalanceTv);
        wlletSwitch = findViewById(R.id.wlletSwitch);
        priceBottomTv = findViewById(R.id.priceBottomTv);
        totlIPTv = findViewById(R.id.totlIPTv);
        saveWallet = findViewById(R.id.saveWallet);

        walletDeductAmount.setText(getString(R.string.rs)+"0.00");

        spinnerSlot.setAdapter(new spinnerAdapter(getApplicationContext(), R.layout.spinner_layout, (ArrayList<String>) slot));

        rbCod.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                p_type = "1";
            }
        });

        //rbOnline.setVisibility(View.GONE);

        rbOnline.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                p_type = "2";
            }
        });

        tvMain.setText("Place Order");
        ivMenu.setOnClickListener(this);
        tvChange.setOnClickListener(this);
        tvProceed.setOnClickListener(this);
        tvApplyCoupon.setOnClickListener(this);
        rrCredit.setOnClickListener(this);
        rrNet.setOnClickListener(this);
        rrWallet.setOnClickListener(this);
        rrCOD.setOnClickListener(this);
        etDob.setOnClickListener(this);
        tvBtnRemove.setOnClickListener(this);
        tvBtnApply.setOnClickListener(this);
        rlCod.setOnClickListener(this);
        rlOnline.setOnClickListener(this);

        tvBtnApply.setVisibility(View.VISIBLE);
        tvBtnRemove.setVisibility(View.GONE);

        ivCart.setVisibility(View.GONE);
        ivSearch.setVisibility(View.GONE);
        tvCount.setVisibility(View.GONE);

        rrCoupanAmount.setVisibility(View.GONE);
        tvAddress1.setText(AppConstants.addressNewList.get(0).get("name"));

        AppSettings.putString(AppSettings.discount, "0");
        tvWallet.setText(getString(R.string.rs) + AppSettings.getString(AppSettings.wallet));

        String address = AppConstants.addressNewList.get(0).get("area_streat_address") + ", "
                + AppConstants.addressNewList.get(0).get("locality") + ", "
                + AppConstants.addressNewList.get(0).get("district_city_town") + ", "
                + AppConstants.addressNewList.get(0).get("landmark_optional") + ","
                + AppConstants.addressNewList.get(0).get("stateName") + " - "
                + AppConstants.addressNewList.get(0).get("pincode");

        tvAddress2.setText(address);
        tvMobile.setText(AppConstants.addressNewList.get(0).get("phone_no"));
        tvDiscountAmount.setText(getString(R.string.rs) + " 0.00");

        if (Integer.parseInt(AppSettings.getString(AppSettings.cartCount)) < 2) {
            tvPrice.setText("Price (" + AppSettings.getString(AppSettings.cartCount) + " Product)");
        } else {
            tvPrice.setText("Price (" + AppSettings.getString(AppSettings.cartCount) + " Products)");
        }

        if (!AppSettings.getString(AppSettings.wallet).equals("")) {
            wallet = Float.parseFloat(AppSettings.getString(AppSettings.wallet));
        }

        if (wallet <= 1) {
            check = 0;
            ivUseWallet.setImageResource(R.drawable.ic_check_box_unselected);
            rrWallet.setVisibility(View.GONE);
            credit = 0;
            cod = 1;
            net = 0;
            payment = 1;
            AppSettings.putInt(AppSettings.paymentType, 1);
            ivCOD.setImageResource(R.drawable.ic_wallet);
            ivNet.setImageResource(R.drawable.ic_radio_button_unchecked);
            ivCredit.setImageResource(R.drawable.ic_radio_button_unchecked);
            llPayment.setVisibility(View.VISIBLE);
        } else {
            check = 1;
            ivUseWallet.setImageResource(R.drawable.ic_check_box_selected);
            rrWallet.setVisibility(View.VISIBLE);
            credit = 0;
            cod = 0;
            net = 0;
            payment = 3;
            AppSettings.putInt(AppSettings.paymentType, 3);
            ivCOD.setImageResource(R.drawable.ic_radio_button_unchecked);
            ivNet.setImageResource(R.drawable.ic_radio_button_unchecked);
            ivCredit.setImageResource(R.drawable.ic_radio_button_unchecked);
            llPayment.setVisibility(View.GONE);
        }

        tvPrices.setText(getString(R.string.rs) + AppSettings.getString(AppSettings.totalAmount));
        //  Toast.makeText(this, tvShipAmount.getText().toString().trim()+""+AppSettings.getString(AppSettings.totalAmount), Toast.LENGTH_LONG).show();
        float x = Float.parseFloat(AppSettings.getString(AppSettings.totalAmount));
        //  float y=x+10;
        // Toast.makeText(this, ""+y, Toast.LENGTH_LONG).show();
        totamt2 = x;
        tvTotal.setText(getString(R.string.rs) + x);
        tvApplyCoupon.setText("Apply Coupon");

        AppSettings.putString(AppSettings.discount, "0");
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        setDateTimeField();
        cartList.clear();
        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getCartListApi();
        } else {
            AppUtils.showErrorMessage(tvTotal, getString(R.string.errorInternet), mActivity);
        }
        wallet = 1;
        cod = 1;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_menu:
                finish();
                return;

            case R.id.llCoupanAvailability:
                myCoupanApi();
                alertCoupanBox();

                break;

            case R.id.tvBtnApply:
                break;
            case R.id.tvBtnRemove:
                rrCoupanAmount.setVisibility(View.GONE);
                tvBtnApply.setVisibility(View.VISIBLE);
                tvValue.setText("");

                tvTotal.setText(getString(R.string.rs) + AppSettings.getString(AppSettings.mainWallet));
                tvValue.setHint("Enter a Coupan Code");
                tvValue.setHintTextColor(Color.BLACK);
                break;

            case R.id.etDob:
                fromDatePickerDialog.show();
                return;

            case R.id.tvOrder:
                if (etDob.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please select your delivery date", Toast.LENGTH_SHORT).show();
                } else if (spinnerSlot.getSelectedItem().equals("Select a delivery slot")) {
                    Toast.makeText(this, "Please select your delivery slot", Toast.LENGTH_SHORT).show();
                } else if (rbCod.isChecked() == false && rbOnline.isChecked() == false) {
                    Toast.makeText(this, "Please select Payment options", Toast.LENGTH_SHORT).show();
                } else if (AppSettings.getString(AppSettings.mobile).isEmpty()) {
                    Toast.makeText(this, "Please update your profile", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(mActivity, AccountActivity.class));
                } else {
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                        if (rbCod.isChecked()) {
                            placeOrderApi();
                        } else {
                            startpay();
                        }
                    } else {
                        AppUtils.showErrorMessage(tvTotal, getString(R.string.errorInternet), mActivity);
                    }
                }
                return;

            case R.id.textView6:

                finish();

                return;

            case R.id.rrCOD:

                payment = 1;
                AppSettings.putInt(AppSettings.paymentType, 1);
                ivCOD.setImageResource(R.drawable.ic_wallet);
                ivNet.setImageResource(R.drawable.ic_radio_button_unchecked);
                ivCredit.setImageResource(R.drawable.ic_radio_button_unchecked);

                return;

            case R.id.rrNet:

                payment = 2;
                AppSettings.putInt(AppSettings.paymentType, 2);
                ivNet.setImageResource(R.drawable.ic_radio_button_checked);
                ivCOD.setImageResource(R.drawable.ic_radio_button_unchecked);
                ivCredit.setImageResource(R.drawable.ic_radio_button_unchecked);

                return;

            case R.id.rrCredit:

                /*if(credit==0)
                {
                    payment=1;
                    ivCredit.setImageResource(R.drawable.ic_radio_button_checked);
                    ivNet.setImageResource(R.drawable.ic_radio_button_unchecked);
                    ivCOD.setImageResource(R.drawable.ic_radio_button_unchecked);
                }*/

                break;

            case R.id.rrWallet:

                if (check == 1) {
                    check = 0;
                    ivUseWallet.setImageResource(R.drawable.ic_check_box_unselected);
                    payment = 1;
                    AppSettings.putInt(AppSettings.paymentType, 1);
                    ivCOD.setImageResource(R.drawable.ic_wallet);
                    ivNet.setImageResource(R.drawable.ic_radio_button_unchecked);
                    ivCredit.setImageResource(R.drawable.ic_radio_button_unchecked);
                    llPayment.setVisibility(View.VISIBLE);
                } else {
                    check = 1;
                    ivUseWallet.setImageResource(R.drawable.ic_check_box_selected);
                    llPayment.setVisibility(View.GONE);
                    payment = 3;
                    AppSettings.putInt(AppSettings.paymentType, 3);
                    ivCOD.setImageResource(R.drawable.ic_radio_button_unchecked);
                    ivNet.setImageResource(R.drawable.ic_radio_button_unchecked);
                    ivCredit.setImageResource(R.drawable.ic_radio_button_unchecked);
                }

                return;

            case R.id.textView17:

                if (tvApplyCoupon.getText().toString().trim().equals("Apply Coupon")) {
                    if (edCoupon.getText().toString().isEmpty()) {
                        AppUtils.showErrorMessage(tvMain, "Enter Coupon Code", mActivity);
                    } else if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                        // applyCouponApi();
                    } else {
                        AppUtils.showErrorMessage(tvMain, getString(R.string.errorInternet), mActivity);
                    }
                } else {
                    AppUtils.showErrorMessage(tvMain, "Coupon Code Removed", mActivity);
                    AppSettings.putString(AppSettings.finalPrice, AppSettings.getString(AppSettings.price));
                    AppSettings.putString(AppSettings.couponId, "");
                    tvApplyCoupon.setText("Apply Coupon");
                    edCoupon.setText("");
                    float s = Float.parseFloat(AppSettings.getString(AppSettings.totalAmount));
                    //float y=s+10;
                    tvTotal.setText(getString(R.string.rs) + s);
                }

                return;

            case R.id.rlCod:
                if (ivCodcheck.getVisibility() == View.VISIBLE) {
                    ivCodcheck.setVisibility(View.GONE);
                    ivCodUncheck.setVisibility(View.VISIBLE);
                } else {
                    ivCodcheck.setVisibility(View.VISIBLE);
                    ivCodUncheck.setVisibility(View.GONE);
                }
                return;

            case R.id.rlOnline:
                if (ivOnlineCheck.getVisibility() == View.VISIBLE) {
                    ivOnlineCheck.setVisibility(View.GONE);
                    ivOnlineUncheck.setVisibility(View.VISIBLE);
                } else {
                    ivOnlineCheck.setVisibility(View.VISIBLE);
                    ivOnlineUncheck.setVisibility(View.GONE);
                }
                return;

        }

    }

    private void setDateTimeField() {
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, (view, year, monthOfYear, dayOfMonth) -> {
            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);
            etDob.setText(dateFormatter.format(newDate.getTime()));
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        // fromDatePickerDialog.getDatePicker().setMaxDate(newDate.getTimeInMillis());
        fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        //  fromDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
    }


    //~Shailaja Tripathi Code
    private void myCoupanApi() {

        AppUtils.showRequestDialog(mActivity);
        Log.v("GetmyCoupanApi", AppUrls.myCoupon);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json.put(AppConstants.result, json_data);
            Log.v("GetCoupanApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.myCoupon)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsemyCoupanJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsemyCoupanJsondata(JSONObject response) {
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                id = jsonObject.getString("id");

                tvCoupanAvailablity.setText("You have " + id + " Coupon Available");

                JSONArray coupanArray = jsonObject.getJSONArray("data");

                if (coupanArray.equals("")) {
                    llCoupanAvailability.setVisibility(View.VISIBLE);
                    llValue.setVisibility(View.VISIBLE);
                }
                coupanList.clear();
                int CouponId = 0;

                for (int i = 0; i < coupanArray.length(); i++) {

                    JSONObject coupanobject = coupanArray.getJSONObject(i);
                    HashMap<String, String> addList = new HashMap();
                    addList.put("coupon_id", coupanobject.getString("coupon_id"));
                    addList.put("coupon_code", coupanobject.getString("coupon_code"));
                    addList.put("title", coupanobject.getString("title"));
                    addList.put("payment_factor", coupanobject.getString("payment_factor"));
                    addList.put("coupon_category", coupanobject.getString("coupon_category"));
                    addList.put("couponType_discount", coupanobject.getString("couponType_discount"));
                    addList.put("discount_value", coupanobject.getString("discount_value"));
                    addList.put("min_amount", coupanobject.getString("min_amount"));
                    addList.put("start_date", coupanobject.getString("start_date"));
                    addList.put("end_date", coupanobject.getString("end_date"));
                    coupanList.add(addList);
                    CouponId = Integer.parseInt(coupanobject.getString("coupon_id"));
                    coupaDiscountAmount = Integer.parseInt(coupanobject.getString("discount_value"));
                    minAmount = Float.parseFloat(coupanobject.getString("min_amount"));
                }
                AppSettings.putString(AppSettings.couponId, String.valueOf(CouponId));
                AppSettings.putString(AppSettings.coupaDiscountAmount, String.valueOf(coupaDiscountAmount));
                TypeAdapter typeAdapter = new TypeAdapter(coupanList);
                rcvList.setAdapter(typeAdapter);
                rcvList.setNestedScrollingEnabled(true);
            } else {
                AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }

        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
    }


    //oldApi
    /*private void applyCouponApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("applyCouponApi", AppUrls.applyCouponCode);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("final_price", AppSettings.getString(AppSettings.price));
            json_data.put("coupon_code", edCoupon.getText().toString().trim());

            json.put(AppConstants.result, json_data);

            Log.v("applyCouponApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.applyCouponCode)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                         AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedata(JSONObject response) {

        Log.d("textView14 ", response.toString());

        AppSettings.putString(AppSettings.couponId,"");

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);

            if (jsonObject.getString("res_code").equals("1")) {
                tvApplyCoupon.setText("Cancel Coupon");
                tvTotal.setText("RS "+jsonObject.getString("final_price"));
             //   AppSettings.putString(AppSettings.couponId,jsonObject.getString("coupon_id"));

                AppSettings.putString(AppSettings.finalPrice,jsonObject.getString("final_price"));
                AppSettings.putString(AppSettings.discount,jsonObject.getString("discount"));

                float min = Float.parseFloat(AppSettings.getString(AppSettings.min_order_bal));
                float amount = Float.parseFloat(jsonObject.getString("final_price"));

                if(min>amount)
                {
                  //  float total = Float.parseFloat(String.valueOf(amount+ Float.parseFloat(AppSettings.getString(AppSettings.shippingAmount))));
                    float total = Float.parseFloat(String.valueOf(amount+ 10));
                    AppSettings.putString(AppSettings.finalPrice, String.valueOf(total));
                    tvShipAmount.setText("RS 10.00");
                }
                else
                {
                    tvShipAmount.setText("RS 10.00");
                }

                tvPrices.setText("RS "+AppSettings.getString(AppSettings.totalAmount));
                tvTotal.setText("RS "+y);
                tvDiscountAmount.setText("RS "+jsonObject.getString("discount"));

            }
            else
            {
                AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
         AppUtils.hideDialog();

    }*/
   /* private void placeOrderApi() {
        AppUtils.showRequestDialog(mActivity);
        Log.v("placeOrderApi", AppUrls.placeOrder);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
          //  json_data.put("address_master_id", AppConstants.addressNewList.get(0).get("id"));
            json_data.put("name", AppConstants.addressNewList.get(0).get("name"));
            json_data.put("phone_no", AppConstants.addressNewList.get(0).get("phone_no"));
            json_data.put("pincode", AppConstants.addressNewList.get(0).get("pincode"));
            json_data.put("locality", AppConstants.addressNewList.get(0).get("locality"));
            json_data.put("area_streat_address", AppConstants.addressNewList.get(0).get("area_streat_address"));
            json_data.put("district_city_town", AppConstants.addressNewList.get(0).get("district_city_town"));
            json_data.put("state_master_id", AppConstants.addressNewList.get(0).get("stateName"));
            json_data.put("landmark_optional", AppConstants.addressNewList.get(0).get("landmark_optional"));
            json_data.put("alternative_phone_optional", AppConstants.addressNewList.get(0).get("landmark_optional"));


            json_data.put("coupon_code_id", AppSettings.getString(AppSettings.couponId));
            json_data.put("payment_type", payment);
            json_data.put("wallet_applied", check);
            json_data.put("final_price", AppSettings.getString(AppSettings.price));
        //    json_data.put("discount",AppSettings.getString(AppSettings.discount));
           // json_data.put("total_amount", AppSettings.getString(AppSettings.price));
            json_data.put("delivery_time", etDob.getText().toString().trim());
            json_data.put("slot_selection", spinnerSlot.getSelectedItemPosition());
            json_data.put("additional_note",etAdditionalNotes.getText().toString() );

            for (int i=0;i<cartList.size();i++)
            {
                JSONObject jsonObject = new JSONObject();

                jsonObject.put("product_id", cartList.get(i).get("product_id"));
                jsonObject.put("discount_type", cartList.get(i).get("product_discount_type"));
                jsonObject.put("price", cartList.get(i).get("price"));
                jsonObject.put("discount_amount", cartList.get(i).get("product_discount_amount"));
                jsonObject.put("discount_applied_in", cartList.get(i).get("discount_applied_in"));
                jsonObject.put("final_price", cartList.get(i).get("final_price"));
                jsonObject.put("quantity", cartList.get(i).get("quantity"));

                jsonArray.put(jsonObject);
            }

            json_data.put("products",jsonArray);

            json.put(AppConstants.result, json_data);

            Log.v("placeOrderApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.placeOrder)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata(response);
                    }
                    @Override
                    public void onError(ANError error) {
                         AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }*/
//PlaceOrderApi

    private void placeOrderApi() {
        AppUtils.showRequestDialog(mActivity);
        Log.v("placeOrderApi", AppUrls.placeOrder1);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        String type = "";

        if (rbCod.isChecked()) {
            type = "1";
        } else {
            type = "2";
        }
        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json_data.put("address_master_id", AppConstants.addressNewList.get(0).get("id"));
            json_data.put("coupon_code_id", AppSettings.getString(AppSettings.couponId));
            json_data.put("payment_type", type);
            json_data.put("wallet_applied", "");
            json_data.put("final_price", "");
            json_data.put("delivery_time", etDob.getText().toString().trim());
            json_data.put("slot_selection", spinnerSlot.getSelectedItem().toString());
            json_data.put("additional_note", "");
            json_data.put("discount", "");
            json_data.put("total_amount", "");
            json_data.put("Wallet_amount", deductAmt);
            for (int i = 0; i < cartList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("product_master_id", cartList.get(i).get("product_id"));
                jsonObject.put("discount_type", cartList.get(i).get("product_discount_type"));
                jsonObject.put("price", cartList.get(i).get("price"));
                jsonObject.put("discount_amount", cartList.get(i).get("product_discount_amount"));
                jsonObject.put("discount_applied_in", cartList.get(i).get("discount_applied_in"));
                jsonObject.put("final_price", cartList.get(i).get("final_price"));
                jsonObject.put("quantity", cartList.get(i).get("quantity"));
                jsonArray.put(jsonObject);
            }
            json_data.put("products", jsonArray);
            json.put(AppConstants.result, json_data);
            Log.v("placeOrderApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.placeOrder1)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondate(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseJsondate(JSONObject response) {
        Log.v("Paymrnt", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            AppUtils.hideDialog();
            String mesg = jsonObject.getString("res_msg");
            if (jsonObject.getString("res_code").equals("1")) {

                if (rbCod.isChecked()) {
                    AppUtils.showErrorMessage(tvMain, mesg, mActivity);
                    Toast.makeText(this, mesg, Toast.LENGTH_LONG).show();
                }

                String ordID = jsonObject.getString("ordID");
                String order_id = jsonObject.getString("order_id");

                if (jsonObject.has("payUdetails")) {
                    JSONObject payUdetails = jsonObject.getJSONObject("payUdetails");
                    merchantkey = payUdetails.getString("MERCHANT_KEY");
                    merchantId = payUdetails.getString("MERCHANT_ID");
                    String SALT = payUdetails.getString("SALT");
                    merchantHash = payUdetails.getString("hash");
                    txnid = payUdetails.getString("txnid");
                }
                if (jsonObject.has("hasDetail")) {
                    JSONObject hasDetail = jsonObject.getJSONObject("hasDetail");
                    firstname = hasDetail.getString("firstname");
                    amount = hasDetail.getString("amount");
                    email = hasDetail.getString("email");
                    phone = hasDetail.getString("phone");
                    prodname = hasDetail.getString("productinfo");
                }
                if (rbCod.isChecked()) {
                    //  Toast.makeText(this, "hello", Toast.LENGTH_SHORT).show();
                    Intent mIntent = new Intent(mActivity, DashBoardFragment.class);
                    mIntent.putExtra(AppConstants.orderId, order_id);
                    mIntent.putExtra(AppConstants.from, "0");
                    ActivityCompat.finishAffinity(mActivity);
                    startActivity(mIntent);
                } else {
                    Toast.makeText(this, mesg, Toast.LENGTH_LONG).show();
                    Intent mIntent = new Intent(mActivity, DashBoardFragment.class);
                    mIntent.putExtra(AppConstants.orderId, order_id);
                    mIntent.putExtra(AppConstants.from, "0");
                    ActivityCompat.finishAffinity(mActivity);
                    startActivity(mIntent);

                    //startpay();
                    /*Intent intent  = new Intent(getBaseContext(), CheckoutActivity.class);
                    intent.putExtra("amount",amount);
                    intent.putExtra("phone",phone);
                    intent.putExtra("email",email);
                    startActivity(intent);*/
                }

//                 startActivity(new Intent(getBaseContext(), DashBoardFragment.class));
            } else {
                Toast.makeText(this, mesg, Toast.LENGTH_LONG).show();
                startActivity(new Intent(getBaseContext(), DashBoardFragment.class));
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    /*private void parseJsondata(JSONObject response) {

        Log.d("response ", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if(jsonObject.getString("res_code").equals("1")){
                Toast.makeText(this, ""+jsonObject.getString("res_msg"), Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this, ""+jsonObject.getString("res_msg"), Toast.LENGTH_LONG).show();
            }

           *//* if (jsonObject.getString("res_code").equals("1"))
           {
                AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);

                *//**//*Intent mIntent = new Intent(mActivity, OrderDetailActivity.class);
                mIntent.putExtra(AppConstants.orderId, jsonObject.getString("order_id"));
                mIntent.putExtra(AppConstants.from, "0");
                startActivity(mIntent);
                finishAffinity();*//**//*

                if(payment==2)
                {
                    AppSettings.putString(AppSettings.orderId, jsonObject.getString("order_id"));

                    float finalPrice = Float.parseFloat(AppSettings.getString(AppSettings.finalPrice));

                   // tvAddress1.setText(AppConstants.addressNewList.get(0).get("name"));

                    AppSettings.putString(AppSettings.discount, "0");
                    tvWallet.setText("RS "+AppSettings.getString(AppSettings.wallet));

                  *//**//*  Intent intent = new Intent(mActivity,WebViewActivity.class);
                    intent.putExtra(AvenuesParams.ACCESS_CODE, Constants.ACCESS_CODE);
                    intent.putExtra(AvenuesParams.MERCHANT_ID, Constants.MERCHANT_ID);
                    intent.putExtra(AvenuesParams.BILLING_NAME, AppConstants.addressNewList.get(0).get("name"));
                    intent.putExtra(AvenuesParams.BILLING_ADDRESS, AppConstants.addressNewList.get(0).get("area_streat_address"));
                    intent.putExtra(AvenuesParams.BILLING_CITY, AppConstants.addressNewList.get(0).get("district_city_town"));
                    intent.putExtra(AvenuesParams.BILLING_STATE, "Uttar Pradesh");
                    intent.putExtra(AvenuesParams.BILLING_ZIP, AppConstants.addressNewList.get(0).get("pincode"));
                    intent.putExtra(AvenuesParams.ORDER_ID,  jsonObject.getString("order_id"));
                    intent.putExtra(AvenuesParams.CURRENCY, "INR");
                    intent.putExtra(AvenuesParams.AMOUNT, String.valueOf(finalPrice));
                    //intent.putExtra(AvenuesParams.AMOUNT, "1");
                    intent.putExtra(AvenuesParams.BILLING_COUNTRY, "India");

                    intent.putExtra(AvenuesParams.BILLING_TEL, AppSettings.getString(AppSettings.mobile));
                    //intent.putExtra(AvenuesParams.BILLING_EMAIL, AppSettings.getString(AppSettings.email));
                    intent.putExtra(AvenuesParams.BILLING_EMAIL, "test@gmail.com");
                    intent.putExtra(AvenuesParams.REDIRECT_URL, Constants.REDIRECT_URL);
                    intent.putExtra(AvenuesParams.CANCEL_URL, Constants.CANCEL_URL);
                    intent.putExtra(AvenuesParams.RSA_KEY_URL, Constants.RSA_KEY_URL);

                    startActivityForResult(intent, 1);*//**//*

                }
                else if(payment==3)
                {
                    float amount=0;
                    float finalPrice = Float.parseFloat(AppSettings.getString(AppSettings.finalPrice));
                    float wallet = Float.parseFloat(AppSettings.getString(AppSettings.wallet));

                    if(wallet<finalPrice)
                    {
                        amount = finalPrice-wallet;
                    }
                    else if(wallet>=finalPrice)
                    {
                        amount = 0;
                    }

                   *//**//* float price = Float.parseFloat(AppSettings.getString(AppSettings.price));
                    float finalPrice = Float.parseFloat(AppSettings.getString(AppSettings.finalPrice));
                    float wallet = Float.parseFloat(AppSettings.getString(AppSettings.wallet));*//**//*

                    if(amount==0)
                    {
                        Intent mIntent = new Intent(mActivity, OrderDetailActivity.class);
                        mIntent.putExtra(AppConstants.orderId, jsonObject.getString("order_id"));
                        mIntent.putExtra(AppConstants.from, "0");
                        startActivity(mIntent);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            finishAffinity();
                        }
                    }
                    else
                    {

                        //Toast.makeText(this, "Pay "+finalPrice+" via Online Payment", Toast.LENGTH_SHORT).show();
                        AppSettings.putString(AppSettings.orderId, jsonObject.getString("order_id"));

                      *//**//*  Intent intent = new Intent(mActivity,WebViewActivity.class);
                        intent.putExtra(AvenuesParams.ACCESS_CODE, Constants.ACCESS_CODE);
                        intent.putExtra(AvenuesParams.MERCHANT_ID, Constants.MERCHANT_ID);
                        intent.putExtra(AvenuesParams.BILLING_NAME, AppConstants.addressNewList.get(0).get("name"));
                        intent.putExtra(AvenuesParams.BILLING_ADDRESS, AppConstants.addressNewList.get(0).get("area_streat_address"));
                        intent.putExtra(AvenuesParams.BILLING_CITY, AppConstants.addressNewList.get(0).get("district_city_town"));
                        intent.putExtra(AvenuesParams.BILLING_STATE, "Uttar Pradesh");
                        intent.putExtra(AvenuesParams.BILLING_ZIP, AppConstants.addressNewList.get(0).get("pincode"));
                        intent.putExtra(AvenuesParams.ORDER_ID,  jsonObject.getString("order_id"));
                        intent.putExtra(AvenuesParams.CURRENCY, "INR");
                        intent.putExtra(AvenuesParams.AMOUNT, String.valueOf(amount));
                        //intent.putExtra(AvenuesParams.AMOUNT, "1");
                        intent.putExtra(AvenuesParams.BILLING_COUNTRY, "India");

                        intent.putExtra(AvenuesParams.BILLING_TEL, AppSettings.getString(AppSettings.mobile));
                        //intent.putExtra(AvenuesParams.BILLING_EMAIL, AppSettings.getString(AppSettings.email));
                        intent.putExtra(AvenuesParams.BILLING_EMAIL, "test@gmail.com");
                        intent.putExtra(AvenuesParams.REDIRECT_URL, Constants.REDIRECT_URL);
                        intent.putExtra(AvenuesParams.CANCEL_URL, Constants.CANCEL_URL);
                        intent.putExtra(AvenuesParams.RSA_KEY_URL, Constants.RSA_KEY_URL);

                        startActivityForResult(intent, 1);*//**//*
                    }
                }
                else
                {
                    Intent mIntent = new Intent(mActivity, OrderDetailActivity.class);
                    mIntent.putExtra(AppConstants.orderId, jsonObject.getString("order_id"));
                    mIntent.putExtra(AppConstants.from, "0");
                    startActivity(mIntent);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        finishAffinity();
                    }
                }
            }
            else
            {
                AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }*//*
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
         AppUtils.hideDialog();

    }*/

    private void getCartListApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("getCartListApi", AppUrls.getCart);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));

            json.put(AppConstants.result, json_data);

            Log.v("getCartListApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.getCart)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseCartdata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseCartdata(JSONObject response) {

        Log.d("response ", response.toString());

        finalPrice = "";
        AppSettings.putString(AppSettings.cartCount, "0");
        AppSettings.putString(AppSettings.price, String.valueOf(finalPrice));
        AppSettings.putString(AppSettings.finalPrice, String.valueOf(finalPrice));

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                JSONArray productArray = jsonObject.getJSONArray("cart_product");

                AppSettings.putString(AppSettings.cartCount, String.valueOf(productArray.length()));

                for (int i = 0; i < productArray.length(); i++) {

                    JSONObject productobject = productArray.getJSONObject(i);

                    HashMap<String, String> prodList = new HashMap();

                    prodList.put("cart_id", productobject.getString("cart_id"));
                    prodList.put("product_id", productobject.getString("product_id"));
                    prodList.put("product_name", productobject.getString("product_name"));
                    prodList.put("final_price", productobject.getString("final_price"));
                    prodList.put("price", productobject.getString("price"));
                    prodList.put("product_discount_amount", productobject.getString("product_discount_amount"));
                    prodList.put("product_discount_type", productobject.getString("product_discount_type"));
                    prodList.put("product_image", productobject.getString("product_image"));
                    prodList.put("quantity", productobject.getString("quantity"));
                    prodList.put("discount_applied_in", productobject.getString("discount_applied_in"));

                    cartList.add(prodList);

                    //  int price = Integer.parseInt(productobject.getString("final_price"));
                    //   int quantity = Integer.parseInt(productobject.getString("quantity"));

                    // int amount = price*quantity;

                    //  finalPrice = finalPrice+amount;

                    float price = Float.parseFloat(productobject.getString("final_price"));
                    int quantity = Integer.parseInt(productobject.getString("quantity"));
                    shipping_amount = productobject.getString("shipping_amount");

                    float amount = price * quantity;
                    //Double finalPrice1 = Double.valueOf(amount+0.00);
                    //finalPrice=roundTwoDecimals(finalPrice1);
                    finalPrice = String.format("%.2f", amount);
                }

                float min = Float.parseFloat(AppSettings.getString(AppSettings.min_order_bal));
                float amount = Float.parseFloat(String.valueOf(finalPrice));

                AppSettings.putString(AppSettings.price, String.valueOf(finalPrice));
                AppSettings.putString(AppSettings.finalPrice, String.valueOf(finalPrice));

                if (min > amount) {
                    //float total = Float.parseFloat(String.valueOf(amount+ Float.parseFloat(AppSettings.getString(AppSettings.shippingAmount))));
//                    float total = amount+10;
//                    AppSettings.putString(AppSettings.finalPrice, String.format("%.2f",total));
                    tvShipAmount.setText(shipping_amount);
                } else {
                    tvShipAmount.setText(getString(R.string.rs) + shipping_amount);
                }
                tvPrices.setText(getString(R.string.rs) + AppSettings.getString(AppSettings.totalAmount));
                float x = Float.parseFloat(AppSettings.getString(AppSettings.totalAmount));
                // float y=x+10;
                AppSettings.putString(AppSettings.mainWallet, String.valueOf(y));

                float totamt = x + Float.parseFloat(shipping_amount);
                totamt2 = totamt;
                tvTotal.setText(getString(R.string.rs) + totamt);
                AppSettings.putString(AppSettings.priceamount, String.valueOf(y));
            } else {
                AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();

        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getProfileApi();
        } else {
            AppUtils.showErrorMessage(tvTotal, getString(R.string.errorInternet), mActivity);
        }

    }

    private void getProfileApi() {

        AppUtils.showRequestDialog(mActivity);
        Log.v("getProfileApi", AppUrls.getUserDetail);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_id", AppSettings.getString(AppSettings.userId));

            json.put(AppConstants.result, json_data);

            Log.v("getProfileApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.getUserDetail)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        parseProfiledata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void afterSuccessPAyment() {

        JSONObject json_data = new JSONObject();
        try {
            json_data.put("phone", phone);
            json_data.put("order_id", amount);
            json_data.put("username", firstname);
            json_data.put("amount", amount);
            Log.v("after_payment", json_data.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        new AbstrctClss(mActivity, ConstntApi.after_payment(mActivity, json_data), "p", true) {
            @Override
            public void responce(String s) {
                try {
                    try {
                        Log.d("jsonArray", s);
                        /*JSONArray jsonObject = new JSONArray(s);
                        if (jsonObject.optJSONObject(0).optString("Status").equalsIgnoreCase("true"))
                        {
                            AppUtils.showToastSort(mActivity,jsonObject.optJSONObject(0).optString("message"));
                        } else {
                            AppUtils.showToastSort(mActivity,jsonObject.optJSONObject(0).optString("message"));
                        }*/
                        Intent mIntent = new Intent(mActivity, DashBoardFragment.class);
                        ActivityCompat.finishAffinity(mActivity);
                        startActivity(mIntent);

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("letestFeturePi", "sssss");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                Log.d("dsd", "ssssssssssss");
            }
        };
    }

    private void parseProfiledata(JSONObject response) {

        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                JSONObject userObject = jsonObject.getJSONObject("user_detail");

                AppSettings.putString(AppSettings.name, userObject.getString("username"));
                AppSettings.putString(AppSettings.email, userObject.getString("email_id"));
                AppSettings.putString(AppSettings.gender, userObject.getString("gender"));
                AppSettings.putString(AppSettings.mobile, userObject.getString("phone_no"));
                AppSettings.putString(AppSettings.wallet, userObject.getString("wallet_amount"));
                AppSettings.putString(AppSettings.referralId, userObject.getString("referel_id"));

                tvWallet.setText(AppSettings.getString(AppSettings.wallet));

                if (!AppSettings.getString(AppSettings.wallet).equals("")) {
                    wallet = Float.parseFloat(AppSettings.getString(AppSettings.wallet));
                }

                if (wallet <= 1) {
                    check = 0;
                    ivUseWallet.setImageResource(R.drawable.ic_check_box_unselected);
                    rrWallet.setVisibility(View.GONE);
                    credit = 0;
                    cod = 1;
                    net = 0;
                    payment = 1;
                    AppSettings.putInt(AppSettings.paymentType, payment);
                    ivCOD.setImageResource(R.drawable.ic_wallet);
                    ivNet.setImageResource(R.drawable.ic_radio_button_unchecked);
                    ivCredit.setImageResource(R.drawable.ic_radio_button_unchecked);
                    llPayment.setVisibility(View.VISIBLE);
                } else {
                    check = 1;
                    ivUseWallet.setImageResource(R.drawable.ic_check_box_selected);
                    rrWallet.setVisibility(View.VISIBLE);
                    credit = 0;
                    cod = 0;
                    net = 0;
                    payment = 3;
                    AppSettings.putInt(AppSettings.paymentType, payment);
                    ivCOD.setImageResource(R.drawable.ic_radio_button_unchecked);
                    ivNet.setImageResource(R.drawable.ic_radio_button_unchecked);
                    ivCredit.setImageResource(R.drawable.ic_radio_button_unchecked);
                    llPayment.setVisibility(View.GONE);
                }
            } else {
                AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
    }

    public void startpay() {

        Intent intent = new Intent(mActivity, CheckoutActivity.class);
        intent.putExtra("BuyNow", "true");
        intent.putExtra("totlamount", tvTotal.getText().toString().replace(getString(R.string.rs), ""));
        startActivityForResult(intent, 100);
        /*builder.setAmount(amount)                          // Payment amount
                .setTxnId(txnid)                     // Transaction ID
                .setPhone(phone)                   // User Phone number
                .setProductName(prodname)                   // Product Name or description
                .setFirstName(firstname)                              // User First name
                .setEmail(email)              // User Email ID
                .setsUrl("https://www.payumoney.com/mobileapp/payumoney/success.php")     // Success URL (surl)
                .setfUrl("https://www.payumoney.com/mobileapp/payumoney/failure.php")     //Failure URL (furl)
                .setUdf1("")
                .setUdf2("")
                .setUdf3("")
                .setUdf4("")
                .setUdf5("")
                .setUdf6("")
                .setUdf7("")
                .setUdf8("")
                .setUdf9("")
                .setUdf10("")
                .setIsDebug(false)                              // Integration environment - true (Debug)/ false(Production)
                .setKey(merchantkey)                        // Merchant key
                .setMerchantId(merchantId);
        try {
            paymentParam = builder.build();
            // generateHashFromServer(paymentParam );
            paymentParam.setMerchantHash(merchantHash);
            // Invoke the following function to open the checkout page.
            // PayUmoneyFlowManager.startPayUMoneyFlow(paymentParam, StartPaymentActivity.this,-1, true);
            PayUmoneyFlowManager.startPayUMoneyFlow(paymentParam, PlaceOrderActivity.this, R.style.AppTheme_default, false);
        } catch (Exception e) {
            Log.e(TAG, " error s " + e.toString());
        }*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        payment = AppSettings.getInt(AppSettings.paymentType);

        if (requestCode == 1) {
            if (AppSettings.getString(AppSettings.status).equalsIgnoreCase("1")) {
                Intent mIntent = new Intent(mActivity, OrderDetailActivity.class);
                mIntent.putExtra(AppConstants.orderId, AppSettings.getString(AppSettings.orderId));
                mIntent.putExtra(AppConstants.from, "0");
                startActivity(mIntent);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    finishAffinity();
                }
            } else {
                //AppUtils.showErrorMessage(tvMain, "Payment Failed", mActivity);
                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    getRefundApi();
                } else {
                    AppUtils.showErrorMessage(tvTotal, getString(R.string.errorInternet), mActivity);
                }
            }
        } else if (requestCode == 100) {
            Log.i("onActivityResult", data.getStringExtra("res"));
            String res = data.getStringExtra("res");

            if (res.equals("success")) {

                placeOrderApi();

                if (AppSettings.getString(AppSettings.status).equalsIgnoreCase("1")) {
                    Intent mIntent = new Intent(mActivity, OrderDetailActivity.class);
                    mIntent.putExtra(AppConstants.orderId, AppSettings.getString(AppSettings.orderId));
                    mIntent.putExtra(AppConstants.from, "0");
                    startActivity(mIntent);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        finishAffinity();
                    }
                } else {
                    //AppUtils.showErrorMessage(tvMain, "Payment Failed", mActivity);
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                        getRefundApi();
                    } else {
                        AppUtils.showErrorMessage(tvTotal, getString(R.string.errorInternet), mActivity);
                    }
                }
            } else {
                try {
                    JSONObject jsonObject = new JSONObject(res);
                    Commonhelper.showToastLong(mActivity, jsonObject.optJSONObject("error").optString("description"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    Commonhelper.showToastLong(mActivity, "Payment cancelled");
                }
            }
        }

        /*if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT && resultCode == RESULT_OK && data != null) {
            TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager.INTENT_EXTRA_TRANSACTION_RESPONSE);
            if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
                if (transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
                    Toast.makeText(context, "Payment Successfull", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(context, ""+transactionResponse.payuResponse, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Payment Failed", Toast.LENGTH_SHORT).show();
                }

                afterSuccessPAyment();
                // Response from Payumoney
                String payuResponse = transactionResponse.getPayuResponse();
                // Response from SURl and FURL
                String merchantResponse = transactionResponse.getTransactionDetails();

            } *//* else if (resultModel != null && resultModel.getError() != null) {
                Log.d(TAG, "Error response : " + resultModel.getError().getTransactionResponse());
            } else {
                Log.d(TAG, "Both objects are null!");
            }*//*
        }*/
    }

    //old Code
    private void getRefundApi() {
        AppUtils.showRequestDialog(mActivity);
        Log.v("getRefundApi", AppUrls.paymentRefund);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("order_id", AppSettings.getString(AppSettings.orderId));
            json.put(AppConstants.result, json_data);
            Log.v("getRefundApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.paymentRefund)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            parseRefunddata(response);
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorBody());
                        Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorCode());
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            // AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            //AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void parseRefunddata(JSONObject response) {
        Log.d("response ", response.toString());
        AppUtils.hideDialog();

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);

                Intent mIntent = new Intent(mActivity, OrderDetailActivity.class);
                mIntent.putExtra(AppConstants.orderId, AppSettings.getString(AppSettings.orderId));
                mIntent.putExtra(AppConstants.from, "0");
                startActivity(mIntent);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    finishAffinity();
                }

            } else {
                if (AppSettings.getInt(AppSettings.paymentType) == 2) {
                    Intent mIntent = new Intent(mActivity, OrderDetailActivity.class);
                    mIntent.putExtra(AppConstants.orderId, AppSettings.getString(AppSettings.orderId));
                    mIntent.putExtra(AppConstants.from, "0");
                    startActivity(mIntent);
                    finishAffinity();
                } else {
                    AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);
                }
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
    }


    //==============================Spinner Adapter==========================================================//
    public class spinnerAdapter extends ArrayAdapter<String> {

        ArrayList<String> data;

        public spinnerAdapter(Context context, int textViewResourceId, ArrayList<String> arraySpinner_time) {

            super(context, textViewResourceId, arraySpinner_time);

            this.data = arraySpinner_time;

        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View row = inflater.inflate(R.layout.spinner_layout, parent, false);
            TextView label = (TextView) row.findViewById(R.id.tvName);
            //   label.setTypeface(typeface);
            label.setText(data.get(position));

            return row;
        }
    }

    //~Shailaja Tripathi Code
    private void alertCoupanBox() {

        bottomSheetDialog = new BottomSheetDialog(mActivity);
        bottomSheetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        bottomSheetDialog.setCancelable(true);

        bottomSheetDialog.setContentView(R.layout.coupanalert);
        Window window = bottomSheetDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        bottomSheetDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        bottomSheetDialog.show();

        ImageView ivCancel = bottomSheetDialog.findViewById(R.id.ivCancel);
        rcvList = bottomSheetDialog.findViewById(R.id.rcvList);
        linearLayoutManager = new LinearLayoutManager(mActivity, RecyclerView.VERTICAL, false);
        rcvList.setLayoutManager(linearLayoutManager);
        rcvList.setHasFixedSize(true);
        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.cancel();
            }
        });
      /*  final TextView tvOkay=bottomSheetDialog.findViewById(R.id.tvOkay);
        ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        tvOkay.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {


            }
        });*/

    }

    //~Shailaja Tripathi Code
    private class TypeAdapter extends RecyclerView.Adapter<TypeAdapter.MyViewHolder> {

        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();
        float pricedValue = 0;

        public TypeAdapter(ArrayList<HashMap<String, String>> favList) {
            data = favList;
        }

        @Override
        public TypeAdapter.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflatetype, null);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(TypeAdapter.MyViewHolder holder, final int position) {

            final String id = data.get(position).get("coupon_id");
            data.get(position).get("coupon_code");
            data.get(position).get("title");
            data.get(position).get("payment_factor");
            data.get(position).get("coupon_category");
            data.get(position).get("couponType_discount");


            data.get(position).get("start_date");
            data.get(position).get("end_date");


            /* float x= Float.parseFloat(AppSettings.getString(AppSettings.coupaDiscountAmount));

                Log.v("valuex", String.valueOf(x));
                Log.v("valuey", String.valueOf(y));
                if(y>x){
               }else{
                   Toast.makeText(this, "Your Total price is Lower than coupon Price", Toast.LENGTH_SHORT).show();
               }*/


            holder.tvCoupan.setText(data.get(position).get("title"));
            holder.tvStartDate.setText(data.get(position).get("end_date"));
            holder.tvEndDate.setText(data.get(position).get("end_date"));
            holder.tvCoupanCode.setText(data.get(position).get("coupon_code"));

            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String amount = tvTotal.getText().toString();

                    tvValue.setText(data.get(position).get("coupon_code"));
                    bottomSheetDialog.dismiss();
                    tvBtnApply.setVisibility(View.GONE);
                    tvBtnRemove.setVisibility(View.VISIBLE);

                    float discountValue = Float.parseFloat(data.get(position).get("discount_value"));
                    float totalAmount = Float.parseFloat(AppSettings.getString(AppSettings.totalAmount));
                    pricedValue = totalAmount - discountValue;

                    AppSettings.putString(AppSettings.priceValue, String.valueOf(pricedValue));

                    float p = Float.parseFloat(AppSettings.getString(AppSettings.priceamount));
                    String b = String.valueOf(pricedValue - p);
                    tvTotal.setText(b);

                    rrCoupanAmount.setVisibility(View.VISIBLE);
                    float minimumAmount = Float.parseFloat(data.get(position).get("min_amount"));
                    y = Float.parseFloat(AppSettings.getString(AppSettings.totalAmount));
                    float dd = Float.parseFloat(data.get(position).get("discount_value"));
                    float prrice = Float.parseFloat(AppSettings.getString(AppSettings.totalAmount));
                    float mainWalletprice = Float.parseFloat(AppSettings.getString(AppSettings.mainWallet));

                    if (y >= minimumAmount) {
                        float takep = mainWalletprice - dd;
                        textViewCoupanPrice.setText(getString(R.string.rs) + dd);
                        tvTotal.setText(getString(R.string.rs) + takep);
                    } else {
                        tvTotal.setText(amount);
                        rrCoupanAmount.setVisibility(View.GONE);
                        Toast.makeText(PlaceOrderActivity.this, "coupon amount is not applicable", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tvCoupan, tvStartDate, tvEndDate, tvCoupanCode;

            CardView cardView;

            public MyViewHolder(View itemView) {
                super(itemView);
                tvCoupan = itemView.findViewById(R.id.tvCoupan);
                tvStartDate = itemView.findViewById(R.id.tvStartDate);
                tvEndDate = itemView.findViewById(R.id.tvEndDate);
                tvCoupanCode = itemView.findViewById(R.id.tvCoupanCode);
                cardView = itemView.findViewById(R.id.cardView);
            }
        }
    }

    String shipping_amount = "0.0",Message="";
    double minWalAmt = 0.0;
    double totalAmt = 0, deductAmt = 0.0;
    double  totPrice = 0.0;
    double totamt2=0;
}

package com.aurget.buddha.Activity;

import android.app.Dialog;
import android.graphics.Paint;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;



import com.aurget.buddha.R;


public class Registration extends AppCompatActivity  {

    private TextView termAndConTv;
    private Dialog dialog;
    private Button registerBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        termAndConTv = findViewById(R.id.termAndConTv);
        registerBtn = findViewById(R.id.registerBtn);
        termAndConTv.setPaintFlags(termAndConTv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog  = Commonhelper.loadDialog(Registration.this);
                StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://aurget.com/Api/registration",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    String Res = response;
                                    Log.d("dsd",Res);
                                    Toast.makeText(Registration.this,"Registered Successfully",Toast.LENGTH_LONG).show();
                                    finish();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.d("dsd","sssss");
                                }
                                dialog.dismiss();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                               // Log.i("reer",error.getMessage());
                                Toast.makeText(Registration.this,"Registered Successfully",Toast.LENGTH_LONG).show();
                                finish();
                            }
                        }) {
//            @Override
//            protected Map getParams()
//            {
//                Map params = new HashMap();
//                params.put("username", "");
//                params.put("email", "abc@androidhive.info");
//                params.put("password", "password123");
//
//                return params;
//            }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(Registration.this);
                requestQueue.add(stringRequest);

            }
        });
    }
}

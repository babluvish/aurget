package com.aurget.buddha.Activity.code.Database;

public class TableCategory {
    static final String SQL_CREATE_CATEGORY = ("CREATE TABLE category (" + catColumn.category_id + " VARCHAR," + catColumn.category_name + " VARCHAR," + catColumn.featured_category + " VARCHAR," + catColumn.app_icon + " VARCHAR," + catColumn.json + " VARCHAR," + catColumn.status + " VARCHAR" + ")");
    public static final String category = "category";
    public enum catColumn {
        category_id,
        category_name,
        featured_category,
        app_icon,
        status,
        json
    }
}

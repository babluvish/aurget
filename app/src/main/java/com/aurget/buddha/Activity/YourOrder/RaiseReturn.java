package com.aurget.buddha.Activity.YourOrder;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.provider.MediaStore;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.ImageCropper.CropImage;
import com.aurget.buddha.Activity.ImageCropper.CropImageView;
import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.Activity.Main2Activity;
import com.aurget.buddha.Activity.MyProfile.Shipping_ddress_list_Activity;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.Utils.CustomPreference;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.aurget.buddha.Activity.Commonhelper.bitMap;
import static com.aurget.buddha.Activity.Commonhelper.fileExe;

public class RaiseReturn extends BaseActivity {

    private LinearLayout ll;
    private LinearLayout l2;
    private LinearLayout l3;
    private LinearLayout previousLL;
    private LinearLayout nextLl, uploadImgRl1, uploadImgRl2, uploadImgRl3, uploadImgRl4;
    private TextView nextBtn, reasonforreturnTv, plus1, plus2, plus3, plus4, addressTv;
    private int count = 1;
    ImageView img1, img2, img3, img4;
    RecyclerView rv, recycleView;
    RadioButton refundRb, replaceRb;
    Button changes;
    EditText commentEt;

    private void address(String orderID) {
        new AbstrctClss(RaiseReturn.this, ConstntApi.shipping_address(RaiseReturn.this, orderID), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    Log.d("deal_product", s);
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.optJSONArray("shipping_address");
                    addressTv.setText("" + jsonArray.optJSONObject(0).optString("firstname")
                            + "\n" + jsonArray.optJSONObject(0).optString("lastname")
                            + "\n" + jsonArray.optJSONObject(0).optString("address1")
                            + "\n" + jsonArray.optJSONObject(0).optString("address2")
                            + "\n" + jsonArray.optJSONObject(0).optString("zip")
                            + "\n" + jsonArray.optJSONObject(0).optString("email")
                            + "\n" + jsonArray.optJSONObject(0).optString("phone"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_raise_return);
        ll = findViewById(R.id.ll);
        l2 = findViewById(R.id.ll2);
        l3 = findViewById(R.id.ll3);

        plus1 = findViewById(R.id.plus1);
        plus2 = findViewById(R.id.plus2);
        plus3 = findViewById(R.id.plus3);
        plus4 = findViewById(R.id.plus4);
        refundRb = findViewById(R.id.refundRb);
        replaceRb = findViewById(R.id.replaceRb);

        uploadImgRl1 = findViewById(R.id.uploadImgRl1);
        uploadImgRl2 = findViewById(R.id.uploadImgRl2);
        uploadImgRl3 = findViewById(R.id.uploadImgRl3);
        uploadImgRl4 = findViewById(R.id.uploadImgRl4);

        img1 = findViewById(R.id.img1);
        img2 = findViewById(R.id.img2);
        img3 = findViewById(R.id.img3);
        img4 = findViewById(R.id.img4);

        addressTv = findViewById(R.id.addressTv);
        commentEt = findViewById(R.id.commentEt);

        nextBtn = findViewById(R.id.nextBtn);
        reasonforreturnTv = findViewById(R.id.reasonforreturnTv);

        l2.setVisibility(View.GONE);
        l3.setVisibility(View.GONE);

        previousLL = findViewById(R.id.previousLL);
        nextLl = findViewById(R.id.nextLl);

        changes = findViewById(R.id.changes);

        recycleView = findViewById(R.id.recycleView);

        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(RaiseReturn.this, LinearLayoutManager.VERTICAL, false);
        recycleView.setLayoutManager(horizontalLayoutManager);
        recycleView.setItemAnimator(new DefaultItemAnimator());

        order_list(getIntent().getStringExtra("orderID"));
        address(getIntent().getStringExtra("orderID"));

        return_reason();

        reasonforreturnTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(RaiseReturn.this);
                dialog.setContentView(R.layout.reson_for_retun_rbtn);
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                rv = dialog.findViewById(R.id.rv);
                LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(RaiseReturn.this, LinearLayoutManager.VERTICAL, false);
                rv.setLayoutManager(horizontalLayoutManager);
                rv.setItemAnimator(new DefaultItemAnimator());

                ArrayList<JSONObject> jsonObjects = new ArrayList<>();

                for (int i = 0; i < jsonObject.length(); i++) {
                    jsonObjects.add(jsonObject.optJSONObject(i));
                }

                rv.setAdapter(new MyRecyclerAdapterMonth(jsonObjects));

                dialog.show();

                dialog.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });

        nextLl.setOnClickListener(view -> {

            if (nextBtn.getText().toString().equalsIgnoreCase(getString(R.string.submit_request)))
            {
                JSONObject jsonObject = new JSONObject();
                String returnType = "";

                if (refundRb.isChecked())
                {
                    returnType = "2";
                }
                else
                {
                    returnType = "1";
                }

                try {
                    jsonObject.put("1",getIntent().getStringExtra("orderID"));
                    jsonObject.put("2",resionID);
                    jsonObject.put("3",jsonArray_.toString());
                    jsonObject.put("4",returnType);
                    jsonObject.put("5",commentEt.getText().toString());
                    jsonObject.put("6","1");
                    return_submit(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return;
            }

            count++;

            if (count == 2) {
                ll.setVisibility(View.GONE);
                l2.setVisibility(View.VISIBLE);
                l3.setVisibility(View.GONE);

                nextBtn.setText(getString(R.string.next));
            }

            if (count == 3) {
                ll.setVisibility(View.GONE);
                l2.setVisibility(View.GONE);
                l3.setVisibility(View.VISIBLE);

                nextBtn.setText(getString(R.string.submit_request));

            }

        });

        previousLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                count--;

                if (count == 1) {
                    ll.setVisibility(View.VISIBLE);
                    l2.setVisibility(View.GONE);
                    l3.setVisibility(View.GONE);
                    nextBtn.setText(getString(R.string.next));
                }

                if (count == 2) {
                    ll.setVisibility(View.GONE);
                    l2.setVisibility(View.VISIBLE);
                    l3.setVisibility(View.GONE);
                    nextBtn.setText(getString(R.string.next));
                }
            }
        });


        uploadImgRl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = 1;
                popupUploadImage();
            }
        });

        uploadImgRl2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = 2;
                popupUploadImage();
            }
        });

        uploadImgRl3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = 3;
                popupUploadImage();
            }
        });

        uploadImgRl4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = 4;
                popupUploadImage();
            }
        });

        changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RaiseReturn.this, Shipping_ddress_list_Activity.class);
                intent.putExtra("RaiseReturn", "true");
                startActivityForResult(intent, 101);
            }
        });

        replaceRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    refundRb.setChecked(false);
            }
        });

        refundRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    replaceRb.setChecked(false);
            }
        });

    }

    private void popupUploadImage() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setActivityTitle("Crop image")
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setCropMenuCropButtonTitle("Done")
                .setRequestedSize(1000, 1000).setScaleType(CropImageView.ScaleType.FIT_CENTER)
                .setCropMenuCropButtonIcon(R.drawable.ic_fullscreen_black_24dp)
                .start(RaiseReturn.this);
    }


    private void return_reason() {
        new AbstrctClss(RaiseReturn.this, ConstntApi.return_reason(RaiseReturn.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    Log.d("deal_product", s);
                    jsonObject = new JSONArray(s);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    private void return_submit(JSONObject jsonObject) {
        new AbstrctClss(RaiseReturn.this, ConstntApi.product_return(RaiseReturn.this,jsonObject), "p", true) {
            @Override
            public void responce(String s) {
                try {
                    Log.d("return_submit", s);
                    JSONArray jsonArray2 = new JSONArray(s);
                    Commonhelper.showToastLong(RaiseReturn.this,jsonArray2.optJSONObject(0).optString("message"));
                    finish();
                    startActivity(new Intent(RaiseReturn.this, Main2Activity.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }


    public class MyRecyclerAdapterMonth extends RecyclerView.Adapter<MyRecyclerAdapterMonth.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;
        int lastSelectedPosition = -1;

        public MyRecyclerAdapterMonth(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapterMonth.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.return_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapterMonth.ViewHolder holder, int position) {
            holder.monthTv.setText(jsonObjectal.get(position).optString("reason_type"));
            holder.rdo.setChecked(lastSelectedPosition == position);
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView monthTv, monthTv2;
            RadioButton rdo;

            public ViewHolder(View itemView) {
                super(itemView);

                monthTv = itemView.findViewById(R.id.monthTv);
                rdo = itemView.findViewById(R.id.rdo);

                rdo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        resionID = jsonObjectal.get(getAdapterPosition()).optString("id");
                        reasonforreturnTv.setText(monthTv.getText().toString());
                        lastSelectedPosition = getAdapterPosition();
                        notifyDataSetChanged();
                    }
                });
            }
        }
    }


    void order_list(String orderId)
    {
        new AbstrctClss(RaiseReturn.this, ConstntApi.order_details(RaiseReturn.this, orderId), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    Log.d("deal_product", s);
                    JSONObject jsonObject = new JSONObject(s);

                    ArrayList<JSONObject> jsonObjects = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray();
                    jsonArray = jsonObject.optJSONArray("data");

                    if (jsonArray.length() < 1) {
                        Commonhelper.showToastLong(RaiseReturn.this, Constnt.notFound);
                        return;
                    }

                    for (int i = 0; i < jsonArray.length(); i++)
                    {
                        jsonObjects.add(jsonArray.optJSONObject(i));
                        jsonArray_.put("0");
                    }
                    recycleView.setAdapter(new MyRecyclerAdapter(jsonObjects));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapter(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item3, parent, false);
            return new MyRecyclerAdapter.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapter.ViewHolder holder, int position) {
            try {
                holder.ipTv.setText("IP:" + jsonObjectal.get(position).optString("ip"));
                holder.prodNmeTv.setText("" + jsonObjectal.get(position).optString("product_name"));
                holder.qtyTv.setText("Quantity:" + jsonObjectal.get(position).optString("qty"));
                holder.priceTv.setText("" + getString(R.string.rs) + jsonObjectal.get(position).optString("price"));
                holder.orderId.setText("Order ID:" + jsonObjectal.get(position).optString("order_id"));
                Picasso.with(RaiseReturn.this).load(jsonObjectal.get(position).optString("image")).into(holder.img);
                holder.orderId.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView prodNmeTv, totaAmtTv, ipTv, qtyTv, priceTv, orderId;
            ImageView img;
            CheckBox checkbox;

            public ViewHolder(View itemView) {
                super(itemView);
                prodNmeTv = itemView.findViewById(R.id.prodNmeTv);
                ipTv = itemView.findViewById(R.id.ipTv);
                qtyTv = itemView.findViewById(R.id.qtyTv);
                priceTv = itemView.findViewById(R.id.priceTv);
                orderId = itemView.findViewById(R.id.orderId);
                img = itemView.findViewById(R.id.img);
                checkbox = itemView.findViewById(R.id.checkbox);
                checkbox.setVisibility(View.VISIBLE);

                checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                        if (isChecked)
                        {
                            try {
                                jsonArray_.put(getAdapterPosition(),jsonObjectal.get(getAdapterPosition()).optString("image"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        else
                        {
                            try {
                                jsonArray_.put(getAdapterPosition(),"0");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 101) {
            try {
                if (data.hasExtra("address_id")) {
                    address_id = data.getStringExtra("address_id");
                    addressTv.setText(data.getStringExtra("name") + "\n" + data.getStringExtra("address")
                            + "\n" + data.getStringExtra("city") + " " + data.getStringExtra("pin_code"));
                }
            } catch (Exception e) {
            }
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult cropResult = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                Log.i("vvv", "" + i);
                Log.i("vvv-", "" + cropResult.getUri());

                if (i == 1) {
                    uploadImgRl2.setVisibility(View.VISIBLE);
                    Picasso.with(RaiseReturn.this).load(cropResult.getUri()).into(img1);
                    plus1.setVisibility(View.GONE);
                    Bitmap panImgPthBitmap = null;
                    try {
                        panImgPthBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), cropResult.getUri());
                        String passbookImgExt = fileExe(cropResult.getUri());
                        String passbookImgPath = bitMap(panImgPthBitmap);

                        AsyncDataClass asyncRequestObject = new AsyncDataClass();
                        asyncRequestObject.execute(InterfaceClass.ipAddress3 + "return_product_img.php",
                                CustomPreference.readString(RaiseReturn.this, CustomPreference.e_id, ""),
                                getIntent().getStringExtra("orderID"),
                                passbookImgPath,
                                passbookImgExt
                        );
                        //Toast.makeText(BAnkActivity.this, "" + passbookImgExt, Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (i == 2) {
                    uploadImgRl3.setVisibility(View.VISIBLE);
                    plus2.setVisibility(View.GONE);
                    Picasso.with(RaiseReturn.this).load(cropResult.getUri()).into(img2);
                    Bitmap panImgPthBitmap = null;
                    try {
                        panImgPthBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), cropResult.getUri());
                        String passbookImgExt = fileExe(cropResult.getUri());
                        String passbookImgPath = bitMap(panImgPthBitmap);

                        AsyncDataClass asyncRequestObject = new AsyncDataClass();
                        asyncRequestObject.execute(InterfaceClass.ipAddress3 + "return_product_img.php",
                                CustomPreference.readString(RaiseReturn.this, CustomPreference.e_id, ""),
                                getIntent().getStringExtra("orderID"),
                                passbookImgPath,
                                passbookImgExt
                        );
                        //Toast.makeText(BAnkActivity.this, "" + passbookImgExt, Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (i == 3) {
                    uploadImgRl4.setVisibility(View.VISIBLE);
                    plus3.setVisibility(View.GONE);
                    Picasso.with(RaiseReturn.this).load(cropResult.getUri()).into(img3);
                    Bitmap panImgPthBitmap = null;
                    try {
                        panImgPthBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), cropResult.getUri());
                        String passbookImgExt = fileExe(cropResult.getUri());
                        String passbookImgPath = bitMap(panImgPthBitmap);

                        AsyncDataClass asyncRequestObject = new AsyncDataClass();
                        asyncRequestObject.execute(InterfaceClass.ipAddress3 + "return_product_img.php",
                                CustomPreference.readString(RaiseReturn.this, CustomPreference.e_id, ""),
                                getIntent().getStringExtra("orderID"),
                                passbookImgPath,
                                passbookImgExt
                        );
                        //Toast.makeText(BAnkActivity.this, "" + passbookImgExt, Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (i == 4) {
                    Picasso.with(RaiseReturn.this).load(cropResult.getUri()).into(img4);
                    plus4.setVisibility(View.GONE);
                    Bitmap panImgPthBitmap = null;
                    try {
                        panImgPthBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), cropResult.getUri());
                        String passbookImgExt = fileExe(cropResult.getUri());
                        String passbookImgPath = bitMap(panImgPthBitmap);

                        AsyncDataClass asyncRequestObject = new AsyncDataClass();
                        asyncRequestObject.execute(InterfaceClass.ipAddress3 + "return_product_img.php",
                                CustomPreference.readString(RaiseReturn.this, CustomPreference.e_id, ""),
                                getIntent().getStringExtra("orderID"),
                                passbookImgPath,
                                passbookImgExt
                        );
                        //Toast.makeText(BAnkActivity.this, "" + passbookImgExt, Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Commonhelper.showToastLong(RaiseReturn.this, "Cropping failed: " + cropResult.getError());
            }
        }
    }

    private class AsyncDataClass extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            HttpParams httpParameters = new BasicHttpParams();
            HttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpPost httpPost = new HttpPost(params[0]);

            String jsonResult = "";

            try {

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                nameValuePairs.add(new BasicNameValuePair("e_id", params[1]));
                nameValuePairs.add(new BasicNameValuePair("order_id", params[2]));
                nameValuePairs.add(new BasicNameValuePair("product_image", params[3]));
                nameValuePairs.add(new BasicNameValuePair("extensions", params[4]));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = httpClient.execute(httpPost);

                jsonResult = Commonhelper.inputStreamToString(response.getEntity().getContent()).toString();

                System.out.println("Returned Json object " + nameValuePairs.toString());

            } catch (ClientProtocolException e) {

                e.printStackTrace();

            } catch (IOException e) {

                e.printStackTrace();
            }
            return jsonResult;
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            dialog = Commonhelper.loadDialog(RaiseReturn.this);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            System.out.println("Resulted Value: " + result);

            try {
                JSONArray jsonArray = new JSONArray(result);
                if (jsonArray.optJSONObject(0).optString("Status").equalsIgnoreCase("true"))
                    Toast.makeText(RaiseReturn.this, jsonArray.optJSONObject(0).optString("message"), Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(RaiseReturn.this, jsonArray.optJSONObject(0).optString("message"), Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            dialog.dismiss();
        }
    }

    JSONArray jsonObject;
    String address_id="";
    String resionID = "";
    int i;
    JSONArray jsonArray_ = new JSONArray();

    Dialog dialog;
}

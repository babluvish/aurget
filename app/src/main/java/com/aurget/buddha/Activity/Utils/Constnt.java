package com.aurget.buddha.Activity.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

public class Constnt {
    public static final String  font  = "fonts/Poppins-Medium.otf";
    public static String notFound = "Not Found";
    public static String insufficientwalletbalance = "You do not have a wallet balance";
    public static String pleaseorderminimum₹499forusecashback = "Please order minimum ₹499 for use cashback";
    public static String pleaseorderminimum₹1499forusecashback = "Please order minimum ₹1499 for use cashback";
    public static String invaildOTP = "Invalid OTP";
    public static String oTPsentsuccessfully = "OTP sent successfully";
    public static String notaregisterednumber = "Not a registered number";
    public static String mobileNoalreadyregistered = "Mobile No. already registered";
    public static String returnPolicy = "http://www.aurget.com/home/page/Return";
    public static boolean hideMLM = true;
    public static File shareImageFile;
    public static String image_path;
    public static String share_content;
    public static String appVersion;
    public static String playStroremessage;
    public static String prodcutID;

    public static ArrayList<JSONObject> recentViewJAl = new ArrayList<>();
    public static ArrayList<JSONObject> product_list_latest = new ArrayList<>();
    public static ArrayList<JSONObject> all_category = new ArrayList<>();
    public static JSONArray get_sliders = new JSONArray();
    public static JSONArray get_profile = new JSONArray();
    public static ArrayList<JSONObject> get_banners = new ArrayList<>();
    public static ArrayList<JSONObject> product_list_set_most_view = new ArrayList<>();
    public static ArrayList<JSONObject> product_list_set = new ArrayList<>();
    public static ArrayList<JSONObject> deal_product = new ArrayList<>();
    public static ArrayList<JSONObject> product_list_set_recent_all = new ArrayList<>();
}

package com.aurget.buddha.Activity.MyProfile;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.Utils.CustomPreference;
import com.aurget.buddha.R;

import org.json.JSONArray;

import java.util.Calendar;

public class Shipping_ddress extends AppCompatActivity {

    EditText mobile_no;
    EditText nameTv;
    EditText genderEt;
    EditText pinCodeEt;
    EditText districtEt;
    EditText flat_house_no_floor_buildingEt;
    EditText colony_street_localityEt;
    EditText cityEt, date_of_birthEt;
    EditText stateEt;
    EditText countryEt;
    Button add;
    RadioButton male, female, custome;

    String gender = "male";

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shipping_ddress);

        nameTv = findViewById(R.id.nameTv);
        genderEt = findViewById(R.id.genderEt);
        pinCodeEt = findViewById(R.id.pinCodeEt);
        flat_house_no_floor_buildingEt = findViewById(R.id.flat_house_no_floor_buildingEt);
        colony_street_localityEt = findViewById(R.id.colony_street_localityEt);
        cityEt = findViewById(R.id.cityEt);
        stateEt = findViewById(R.id.stateEt);
        countryEt = findViewById(R.id.countryEt);
        mobile_no = findViewById(R.id.mobile_no);
        districtEt = findViewById(R.id.districtEt);

        add = findViewById(R.id.add);
        male = findViewById(R.id.male);
        female = findViewById(R.id.female);
        custome = findViewById(R.id.custome);

        if (getIntent().hasExtra("pos")) {
            add.setText("Edit");

            nameTv.setText(getIntent().getStringExtra("name"));
            colony_street_localityEt.setText(getIntent().getStringExtra("colony_street"));
            cityEt.setText(getIntent().getStringExtra("city"));
            stateEt.setText(getIntent().getStringExtra("state"));
            pinCodeEt.setText(getIntent().getStringExtra("pin_code"));
            countryEt.setText(getIntent().getStringExtra("country"));
            mobile_no.setText(getIntent().getStringExtra("mobile"));
            flat_house_no_floor_buildingEt.setText(getIntent().getStringExtra("address"));
        }
        else
        {
            nameTv.setText(CustomPreference.readString(Shipping_ddress.this,CustomPreference.userName,""));
            mobile_no.setText(CustomPreference.readString(Shipping_ddress.this,CustomPreference.mobileNO,""));
        }

        male.setOnClickListener(view -> {
            gender = "male";
            genderEt.setVisibility(View.GONE);
        });

        female.setOnClickListener(view -> {
            gender = "female";
            genderEt.setVisibility(View.GONE);
        });

        custome.setOnClickListener(view -> genderEt.setVisibility(View.VISIBLE));

        add.setOnClickListener(view -> {
            if (add.getText().toString().equals("Edit"))
            {
                if (validation())
                    hitPIEdit();
            } else {
                if (validation())
                    hitPI();
            }
        });

        pinCodeEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void afterTextChanged(Editable editable) {
                if (pinCodeEt.getText().toString().length() == 6) {
                    hitPIPinCode();
                }
            }
        });

    }

    boolean validation() {
        boolean bool = false;
        if (nameTv.getText().toString().length() == 0) {
            Commonhelper.showToastLong(Shipping_ddress.this, "Enter full name.");
            bool = false;
        } else if (mobile_no.getText().toString().length() == 0) {
            Commonhelper.showToastLong(Shipping_ddress.this, "Enter mobile No.");
            bool = false;
        } else if (genderEt.getVisibility() == View.VISIBLE && genderEt.getText().toString().length() == 0) {
            Commonhelper.showToastLong(Shipping_ddress.this, "Enter gender.");
            bool = false;
        } else if (pinCodeEt.getText().toString().length() == 0) {
            Commonhelper.showToastLong(Shipping_ddress.this, "Enter PIN code.");
            bool = false;
        } else if (cityEt.getText().toString().length() == 0) {
            Commonhelper.showToastLong(Shipping_ddress.this, "Enter city.");
            bool = false;
        } else if (stateEt.getText().toString().length() == 0) {
            Commonhelper.showToastLong(Shipping_ddress.this, "Enter state.");
            bool = false;
        } else if (countryEt.getText().toString().length() == 0) {
            Commonhelper.showToastLong(Shipping_ddress.this, "Enter state.");
            bool = false;
        } else {
            bool = true;
        }

        return bool;
    }

    void hitPIEdit()
    {
        String e_id = CustomPreference.readString(Shipping_ddress.this, CustomPreference.e_id, "");
        String mlm_reg_id = CustomPreference.readString(Shipping_ddress.this, CustomPreference.e_id, "");
        String address_id = getIntent().getStringExtra("address_id");

        final Dialog dialog = Commonhelper.loadDialog(Shipping_ddress.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://aurget.com/api/shipping_address?name="+nameTv.getText().toString()
                +"&mobile=" +mobile_no.getText().toString() +
                "&pin_code=" +pinCodeEt.getText().toString() +
                "&address=" +flat_house_no_floor_buildingEt.getText().toString()
                +"&flate_number=1&colony_street="
                +colony_street_localityEt.getText().toString()+"&land_mark=1&login_id=" + mlm_reg_id
                +"&city="+cityEt.getText().toString()
                +"&state="+stateEt.getText().toString()
                +"&address_id="+address_id
                +"&country="+countryEt.getText().toString() +
                "&e_id="+e_id,
                response -> {
                    try {
                        String Res = response;
                        JSONArray jsonArray = new JSONArray(Res);
                        if (jsonArray.optJSONObject(0).optString("Status").equalsIgnoreCase("true")) {
                            Log.d("dsd", Res);
                            Toast.makeText(Shipping_ddress.this, "Address edited  Successfully", Toast.LENGTH_LONG).show();
                            finish();
                        } else {
                            Toast.makeText(Shipping_ddress.this, "Invalid Login Credentials", Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("dsd", "sssss");
                    }
                    dialog.dismiss();
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        finish();
                    }
                }) {
        };

        RequestQueue requestQueue = Volley.newRequestQueue(Shipping_ddress.this);
        requestQueue.add(stringRequest);
    }

    void hitPIPinCode() {
        final Dialog dialog = Commonhelper.loadDialog(Shipping_ddress.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://api.postalpincode.in/pincode/" + pinCodeEt.getText().toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String Res = response;
                            JSONArray jsonArray = new JSONArray(Res);
                            Log.d("dsd", Res);
                            if (jsonArray.optJSONObject(0).optString("Status").equalsIgnoreCase("Success")) {
                                Log.d("dsd", Res);
                                String city = jsonArray.optJSONObject(0).optJSONArray("PostOffice").optJSONObject(0).optString("Block");
                                String State = jsonArray.optJSONObject(0).optJSONArray("PostOffice").optJSONObject(0).optString("State");
                                String Country = jsonArray.optJSONObject(0).optJSONArray("PostOffice").optJSONObject(0).optString("Country");
                                String District = jsonArray.optJSONObject(0).optJSONArray("PostOffice").optJSONObject(0).optString("District");

                                cityEt.setText(city);
                                stateEt.setText(State);
                                countryEt.setText(Country);
                                districtEt.setText(District);

                            } else {
                                Toast.makeText(Shipping_ddress.this, "Invalid Pincode Credentials", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("dsd", "sssss");
                        }
                        dialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Log.i("reer",error.getMessage());
                        finish();
                    }
                }) {
        };

        RequestQueue requestQueue = Volley.newRequestQueue(Shipping_ddress.this);
        requestQueue.add(stringRequest);
    }

    private void piecker() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        date_of_birthEt.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    void hitPI() {
        String e_id = CustomPreference.readString(Shipping_ddress.this, CustomPreference.e_id, "");
        final Dialog dialog = Commonhelper.loadDialog(Shipping_ddress.this);

        String req = "http://aurget.com/api/shipping_address?name=" + nameTv.getText().toString()
                + "&mobile=" + mobile_no.getText().toString()
                + "&pin_code=" + pinCodeEt.getText().toString()
                + "&address=" + flat_house_no_floor_buildingEt.getText().toString()
                + "&flate_number=" + flat_house_no_floor_buildingEt.getText().toString()
                + "&colony_street=" + colony_street_localityEt.getText().toString()
                + "&land_mark=1"
                + "&login_id=" + e_id
                + "&city=" + cityEt.getText().toString()
                + "&state=" + stateEt.getText().toString()
                + "&country=" + countryEt.getText().toString();

        Commonhelper.sop("ress" + req);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, req,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String Res = response;
                            JSONArray jsonArray = new JSONArray(Res);
                            Log.d("dsd", Res);
                            if (jsonArray.optJSONObject(0).optString("Status").equalsIgnoreCase("true")) {
                                Toast.makeText(Shipping_ddress.this, "Address added successfully", Toast.LENGTH_LONG).show();
                                finish();
                            } else {
                                Toast.makeText(Shipping_ddress.this, "Some error occured", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("dsd", "sssss");
                        }
                        dialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        finish();
                    }
                }) {
//            @Override
//            protected Map getParams()
//            {
//                Map params = new HashMap();
//                params.put("mobile", mob);
//                params.put("password", pwd);
//                //params.put("mobile", "9768178608");
//                //params.put("password", "123");
//                return params;
//            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(Shipping_ddress.this);
        requestQueue.add(stringRequest);
    }
}

package com.aurget.buddha.Activity.Utils;

import android.app.Activity;
import android.util.JsonReader;

import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.Activity.categories.SubCategoryDetails2;

import org.json.JSONObject;

import java.util.Random;

public class ConstntApi {

    public static final String wallet_ballanceUrl(Activity activity) {
        return InterfaceClass.ipAddress3 + "wallet_ballance.php?login_id=" + CustomPreference.readString(activity, CustomPreference.e_id, "");
    }

    //http://aurget.com/app/grocery_wallet.php?mobile=9235720166
    public static final String grocery_wallet(Activity activity)
    {
        //return InterfaceClass.ipAddress3 + "grocery_wallet.php?mobile=" + CustomPreference.readString(activity, CustomPreference.mobileNO, "");
        return InterfaceClass.ipAddress3 + "grocery_wallet.php?mobile=9235720166";
    }

    public static final String check_delivery_location(Activity activity,String pinCode) {
        return"http://aurget.in/mart/demo/api/Check_delivery_location?pincode=" + pinCode+"&mobile="+CustomPreference.readString(activity, CustomPreference.mobileNO, "");
    }

    public static final String unapprove_balance(Activity activity) {
        //http://aurget.com/app/unapprove_balance.php?e_id=1
        return InterfaceClass.ipAddress3 + "unapprove_balance.php?e_id=" + CustomPreference.readString(activity, CustomPreference.e_id, "");
    }

    public static final String get_address(Activity activity) {
        return InterfaceClass.ipAddress2 + "get_address?login_id=" + CustomPreference.readString(activity, CustomPreference.e_id, "");
    }

    public static final String delete_shipping_address(Activity activity, String address_id) {
        //http://aurget.com/api/delete_shipping_address?address_id=46
        return InterfaceClass.ipAddress4 + "delete_shipping_address?address_id=" + address_id;
    }

    public static final String smsApi(Activity activity, String mob, String randNO, String sms) {
        return "http://www.smsalert.co.in/api/mverify.json?apikey=5e733f4630b82&sender=AURGET&mobileno='" + mob + "'&template=" + randNO + " " + sms;
    }

    public static final String send_otp_login(Activity activity, String mob) {
        return InterfaceClass.ipAddress3 + "send_otp_loginsss.php?mobile_number=" + mob;
    }

    public static final String direct_incentive(Activity activity) {
        //http://aurget.com/api/direct_incentive?mobile=9721682022
        return InterfaceClass.ipAddress4 + "direct_incentive?mobile=" + CustomPreference.readString(activity, CustomPreference.mobileNO, "");
    }

    public static final String get_all_insentive(Activity activity) {
        //http://aurget.com/business/User/get_all_insentive?contact_no=9662101102
        //return InterfaceClass.ipAddress+"business/User/get_all_insentive?contact_no=9662101102";
        return InterfaceClass.ipAddress7 + "get_all_insentive?contact_no=" + CustomPreference.readString(activity, CustomPreference.mobileNO, "");
    }

    public static final String total_incentives(Activity activity) {
        //http://aurget.com/business/api/total_incentives?mobile=9662101102
        return InterfaceClass.ipAddress6 + "total_incentives?mobile=" + CustomPreference.readString(activity, CustomPreference.mobileNO, "");
    }

    public static final String royalty_list(Activity activity) {
        //http://aurget.com/business/Api/royalty_list?eid=1
        return InterfaceClass.ipAddress6 + "royalty_list?eid=" + CustomPreference.readString(activity, CustomPreference.e_id, "");
    }

    public static final String solder_details(Activity activity) {
        //http://aurget.com/business/api/solder_details?mobile=9662101102
        return InterfaceClass.ipAddress6 + "solder_details?mobile=" + CustomPreference.readString(activity, CustomPreference.mobileNO, "");
    }

    public static final String direct_ip(Activity activity) {
        //http://aurget.com/api/direct_ip?mobile=9662101102
        return InterfaceClass.ipAddress4 + "direct_ip?mobile=" + CustomPreference.readString(activity, CustomPreference.mobileNO, "");
    }

    public static final String update_solder_details(Activity activity, JSONObject jsonObject) {
        //http://aurget.com/api/update_solder_details?mobile=1515154785&fname=bob&lname=vish&email=kshafiq542@gmail.com&gender=male&dob=03-07-1992
        return InterfaceClass.ipAddress4 + "update_solder_details?mobile=" + CustomPreference.readString(activity, CustomPreference.mobileNO, "") +
                "&fname=" + jsonObject.optString("2") +
                "&lname=" + jsonObject.optString("3") +
                "&email=" + jsonObject.optString("4") +
                "&gender=" + jsonObject.optString("5") +
                "&dob=" + jsonObject.optString("6");
    }

    public static final String deffirencial_ip_list(Activity activity) {
        //http://aurget.com/business/api/deffirencial_ip_list?eid=1
        return InterfaceClass.ipAddress6 + "deffirencial_ip_list?eid=" + CustomPreference.readString(activity, CustomPreference.e_id, "");
    }

    public static final String business_team(Activity activity) {
        //http://aurget.com/business/User/get_all_insentive?contact_no=9662101102
        //return InterfaceClass.ipAddress+"business/User/get_all_insentive?contact_no=9662101102";
        return InterfaceClass.ipAddress6 + "business_team?eid=" + CustomPreference.readString(activity, CustomPreference.e_id, "");
    }

    public static final String business_team_search(Activity activity,String keyword) {
        //http://aurget.com/business/api/business_team?eid=1&search_key=9235720166
        return InterfaceClass.ipAddress6 + "business_team?eid=" + CustomPreference.readString(activity, CustomPreference.e_id, "")+"&search_key="+keyword;
    }

    public static final String condition_new(Activity activity) {   //http://aurget.com/api/sorting?sort=condition_new
        return InterfaceClass.ipAddress4 + "sorting?sort=condition_new";
    }

    public static final String condition_old(Activity activity) {   //http://aurget.com/api/sorting?sort=condition_old
        return InterfaceClass.ipAddress4 + "sorting?sort=condition_old";
    }

    public static final String price_low(Activity activity) {   //http://aurget.com/api/sorting?sort=price_low
        return InterfaceClass.ipAddress4 + "sorting?sort=price_low";
    }

    public static final String price_high(Activity activity) {   //http://aurget.com/api/sorting?sort=price_high
        return InterfaceClass.ipAddress4 + "sorting?sort=price_high";
    }

    public static final String get_cart_details(Activity activity) {   //http://aurget.com/api/sorting?sort=price_high
        //http://aurget.com/api/get_cart_details?login_id=1
        return InterfaceClass.ipAddress4 + "get_cart_details?login_id=" + CustomPreference.readString(activity, CustomPreference.e_id, "");
    }

    public static final String product(Activity activity,String url) {
        return InterfaceClass.ipAddress3 + url;
    }

    public static final String solder_level(Activity activity) {
        http://aurget.com/business/api/solder_level/1
        return InterfaceClass.ipAddress6 + "solder_level/"+CustomPreference.readString(activity, CustomPreference.e_id, "");
    }

    public static final String color_image(Activity activity,String prodID) {
        http://aurget.com/api/color_image?product_id=3
        return InterfaceClass.ipAddress4 + "color_image?product_id="+prodID;
    }

    public static final String data_filter(Activity activity,String id) {   //http://aurget.com/api/data_filter/29
        return InterfaceClass.ipAddress4 + "data_filter/"+id;
    }

    public static final String data_filterrate(Activity activity,String id) {   //http://aurget.com/api/data_filterrate/29
        return InterfaceClass.ipAddress4 + "data_filterrate/"+id;
    }

    public static final String all_Slide_bnner(Activity activity) {   //http://aurget.com/api/sorting?sort=price_high
        return InterfaceClass.all_Slide_bnner;
    }

    public static final String all_category(Activity activity) {   //http://aurget.com/api/sorting?sort=price_high
        return InterfaceClass.all_category;
    }

    public static final String price_range(Activity activity, String from_price, String to_price) {
        //http://aurget.com/api/price_range?from_price=100&to_price=500
        return InterfaceClass.ipAddress4 + "price_range?from_price=" + from_price + "&to_price=" + to_price;
    }

    public static final String outletDetals(Activity activity) {
        //http://aurget.com/app/order_details.php?customer_id=26
        //http://aurget.com/api/outlet_shopping?mobile=9662101102
        return InterfaceClass.ipAddress4 + "outlet_shopping?mobile=" + CustomPreference.readString(activity, CustomPreference.mobileNO, "");
    }

    public static final String deal_product(Activity activity) {
        return InterfaceClass.ipAddress4 + "deal_product";
    }

    public static final String search(Activity activity,String search) {
        //search
        return InterfaceClass.ipAddress4 + "search/"+search;
    }

    public static final String order_list(Activity activity) { //http://aurget.com/api/order_list?login_id=26
        return InterfaceClass.ipAddress4 + "order_list?login_id=" + CustomPreference.readString(activity, CustomPreference.e_id, "");
    }

    public static final String order_details(Activity activity, String orderID) { //http://aurget.com/api/order_list?login_id=26
        return InterfaceClass.ipAddress + "Apitwo/order_list_details?order_id=" + orderID;
    }

    public static final String shipping_address(Activity activity, String orderID) { //http://aurget.com/api/order_list?login_id=26
        return InterfaceClass.ipAddress + "Apitwo/shipping_address?order_id=" + orderID;
    }

    public static final String cancel_order(Activity activity, String orderID) {
        // ttp://aurget.com/api/cancel_order?order_id=11&e_id=1
        return InterfaceClass.ipAddress4 + "cancel_order?order_id=" + orderID + "&e_id=" + CustomPreference.readString(activity, CustomPreference.e_id, "");
    }

    public static final String return_reason(Activity activity) {
        // ttp://aurget.com/api/cancel_order?order_id=11&e_id=1
        return InterfaceClass.ipAddress4 + "return_reason";
    }

    //http://aurget.com/apitwo/return_order_list?login_id=1
    public static final String return_order_list(Activity activity) {
        return InterfaceClass.ipAddress+"apitwo/return_order_list?login_id="+CustomPreference.readString(activity, CustomPreference.e_id, "");
    }

    public static final String return_list_details(Activity activity,String orderID) {
        //http://aurget.com/apitwo/return_list_details?order_id=5
        return InterfaceClass.ipAddress+"apitwo/return_list_details?order_id="+orderID;
    }

    public static final String product_return(Activity activity, JSONObject jsonObject) {
        //http://aurget.com/app/product_return.php?e_id=1&order_id=11&reason=1&return_product_id=1,2&return_type=1&product_comment=ertghjk&addres_id=1
        return InterfaceClass.ipAddress3 + "product_return.php?e_id=" + CustomPreference.readString(activity, CustomPreference.e_id, "")
                + "&order_id=" + jsonObject.optString("1")
                + "&reason=" + jsonObject.optString("2")
                + "&return_product_id=" + jsonObject.optString("3")
                + "&return_type=" + jsonObject.optString("4")
                + "&product_comment=" + jsonObject.optString("5")
                + "&addres_id=" + jsonObject.optString("6");
    }

    public static final String insert_rating(Activity activity, JSONObject jsonObject) {
        //http://aurget.com/apitwo/insert_rating?e_id=1&product_id=1&product_type=1&rating=5.00&comment=test
        return InterfaceClass.ipAddress+"apitwo/insert_rating?e_id="+ CustomPreference.readString(activity, CustomPreference.e_id, "")
                + "&product_id=" + jsonObject.optString("1")
                + "&product_type=" + jsonObject.optString("2")
                + "&rating=" + jsonObject.optString("3")
                + "&comment=" + jsonObject.optString("4");
    }

    public static final String order_list_by_date(Activity activity, String from_date, String to_date) {
        return InterfaceClass.ipAddress4 + "order_list_by_date?login_id=" + CustomPreference.readString(activity, CustomPreference.e_id, "") +
                "&from_date=" + from_date + "&to_date=" + to_date;
    }

    public static final String delete_cart(Activity activity, String productID) {
        return InterfaceClass.ipAddress3 + "delete_cart.php?product_id=" + productID + "&login_id=" + CustomPreference.readString(activity, CustomPreference.e_id, "");
    }

    public static final String get_banners(Activity activity) {
        return InterfaceClass.get_banners;
    }


    public static final String fastMovingapi(Activity activity) {
        return InterfaceClass.ipAddress4 + "product_list_set_most_view";
    }

    public static final String latestArrival(Activity activity) {
        return InterfaceClass.ipAddress4 + "product_list_latest";
    }

    public static final String letestFeture(Activity activity) {
        return InterfaceClass.ipAddress4 + "product_list_set";
    }

    public static final String recentViewPi(Activity activity) {
        return InterfaceClass.ipAddress4 + "product_list_set_recent";
    }

    public static final String recentViewPi_(Activity activity) {
        return InterfaceClass.ipAddress4 + "product_list_set_recent_all";
    }

    public static final String rating_list(Activity activity,String product_id) {
        //http://aurget.com/api/rating_list?product_id=3
        return InterfaceClass.ipAddress4 +"rating_list?product_id="+product_id;
    }

    public static final String order_list_by_month(Activity activity, String year_month) {
        //http://aurget.com/api/order_list_by_month?login_id=26&year_month=2020-05
        return InterfaceClass.ipAddress4 + "order_list_by_month?login_id=" + CustomPreference.readString(activity, CustomPreference.e_id, "")
                + "&year_month=" + year_month;
    }

    public static final String get_direct_insentive(Activity activity) {
        //http://aurget.com/budhaa/User/get_direct_insentive?contact_no=9662101102
        return InterfaceClass.ipAddress5 + "get_direct_insentive?contact_no=" + CustomPreference.readString(activity, CustomPreference.mobileNO, "");
    }

    public static final String sponsor_name(String contact_no) {
        //http://aurget.com/business/User/sponsor_name?contact_no=9662101102
        return InterfaceClass.ipAddress + "business/User/sponsor_name?contact_no=" + contact_no;
    }

    public static final String verify_otp(String contact_no, String otp) {
        //http://aurget.com/api/verify_otp?mobile=9235720166&otp=9952
        return InterfaceClass.ipAddress4 + "verify_otp?mobile=" + contact_no + "&otp=" + otp;
    }

    public static final String beforLogin(Activity activity,String url) {

        //http://aurget.com/api/registration?username=ff&surname=test&email=sk@gmail.com&phone=8299012718&pos=pos&gender=1&sponsor=966210110;

        return url;
    }

    public static final String send_otp(String contact_no) {
        //http://aurget.com/app/send_otp.php?mobile_number=9235720162
        //http://aurget.com/business/User/sponsor_name?contact_no=9662101102
        return InterfaceClass.ipAddress3 + "send_otp.php?mobile_number=" + contact_no;
    }

    public static final String sms_content(String status) {
        http:
//aurget.com/app/sms_content.php?status=success
        return InterfaceClass.ipAddress3 + "sms_content.php?status=" + status;
    }

    public static final String businessReport(Activity activity) {
        return InterfaceClass.ipAddress3 + "product_single_buy.php?product_id=" + CustomPreference.readString(activity, CustomPreference.mobileNO, "");
    }

    public static final String total_ip_list(Activity activity) {
        //http://aurget.com/business/api/total_ip_list?mobile=9662101102
        return InterfaceClass.ipAddress6 + "total_ip_list?mobile=" + CustomPreference.readString(activity, CustomPreference.mobileNO, "");
    }

    public static final String deffirencial_incentive_list(Activity activity) {
        return InterfaceClass.ipAddress + "business/Api/deffirencial_incentive_list?eid=" + CustomPreference.readString(activity, CustomPreference.e_id, "");
    }

    public static final String product_single_buy(String product_id) {
        return InterfaceClass.ipAddress3 + "product_single_buy.php?product_id=" + product_id;
    }

    public static final String fetch_bank_details(Activity activity) {
        //http://aurget.com/Api/fetch_bank_details?memberId=1
        return InterfaceClass.ipAddress3 + "get_bank_details.php?mem_id=" + CustomPreference.readString(activity, CustomPreference.e_id, "");
    }

    public static final String add_bank_details(Activity activity, JSONObject jsonObject) {
        //http://aurget.com/Api/fetch_bank_details?memberId=1
        return InterfaceClass.ipAddress3 + "add_bank_details.php?memberId=" + jsonObject.optString("1")
                + "&bank_name=" + jsonObject.optString("2")
                + "&account_no=" + jsonObject.optString("3")
                + "&branch=" + jsonObject.optString("4")
                + "&ifsc=" + jsonObject.optString("5")
                + "&login_id=" + jsonObject.optString("6")
                + "&aadhar_number=" + jsonObject.optString("7")
                + "&pan_number=" + jsonObject.optString("8");
    }

    public static final String get_profile(Activity activity) {
        //http://aurget.com/api/get_profile?e_id=1
        return InterfaceClass.ipAddress4 + "get_profile?e_id=" + CustomPreference.readString(activity, CustomPreference.e_id, "");
    }

    public static final String DashboardApi(Activity activity) {
        //http://aurget.com/DashboardApi/DashboardApi?e_id=1&token_key=ytyure45789
        return InterfaceClass.ipAddress + "DashboardApi/DashboardApi?e_id=" + CustomPreference.readString(activity, CustomPreference.e_id, "")+"&token_key=ytyure45789";
    }

    public static final String get_bank_details(Activity activity) {
        //http://aurget.com/app/get_bank_details.php?mem_id=17
        return InterfaceClass.ipAddress3 + "get_bank_details.php?mem_id=" + CustomPreference.readString(activity, CustomPreference.e_id, "");
    }

    public static final String paymentSuccessAPI(JSONObject jsonObject) {
        return InterfaceClass.ipAddress3 + "order_list.php?product_id=" + jsonObject.optString("product_id")
                + "&shiping_address_id=" + jsonObject.optString("shiping_address_id")
                + "&quantity=" + jsonObject.optString("quantity")
                + "&payment_type=" + jsonObject.optString("payment_type")
                + "&amount=" + jsonObject.optString("amount")
                + "&ip=" + jsonObject.optString("ip")
                + "&login_id=" + jsonObject.optString("login_id")
                + "&size=" + jsonObject.optString("size")
                + "&color=" + jsonObject.optString("color");
    }
}

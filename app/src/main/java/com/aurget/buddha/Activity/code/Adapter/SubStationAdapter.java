package com.aurget.buddha.Activity.code.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.aurget.buddha.R;

import java.util.ArrayList;

public class SubStationAdapter extends ArrayAdapter<String>  {

    ArrayList<String> data;
    ArrayList<String> dataa;

    public SubStationAdapter(Context context, int resource,  ArrayList<String> arrlistSubstation,ArrayList<String> arrlistSubstationID) {

        super(context, resource, arrlistSubstation);

        this.data = arrlistSubstation;
        this.dataa = arrlistSubstationID;

    }
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        return getCustomView(position, convertView, parent);

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View row = inflater.inflate( R.layout.inflatesubstation, parent, false);
        TextView tvSubstationName = (TextView) row.findViewById(R.id.tvSubstationName);

        tvSubstationName.setText(data.get(position).toString());
        Log.v("arrdata", data.get(position));


        return row;
    }
}

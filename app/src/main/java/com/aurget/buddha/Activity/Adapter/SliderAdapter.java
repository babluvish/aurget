package com.aurget.buddha.Activity.Adapter;

import android.content.Context;
import android.content.Intent;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.aurget.buddha.Activity.InterfaceClass;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Product.GalleryActivity;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SliderAdapter extends PagerAdapter {

    private Context context;
    private String product_id;
    private int img_count;
    private List<String> color;
    private List<String> colorName;

    public SliderAdapter(Context context, String product_id,int img_count) {
        this.context = context;
        this.product_id = product_id;
        this.img_count = img_count;
    }

    @Override
    public int getCount() {
        return img_count;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.img_list_item2, null);
        // TextView textView = (TextView) view.findViewById(R.id.textView)
        final int i = position+1;
        final ImageView img =  view.findViewById(R.id.img);
        final ProgressBar progressBar =  view.findViewById(R.id.loader);
        //img.setVisibility(View.VISIBLE);
        img.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            Log.d("sdsf",InterfaceClass.imgPth2+product_id+"_"+i+"_thumb.jpg");
            Picasso.with(context).load(InterfaceClass.imgPth2+product_id+"_"+i+".jpg").into(img, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                    img.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }
                @Override
                public void onError() {

                }
            });

        });

        //img.setImageResource(color.get(position));
        //LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);
        //textView.setText(colorName.get(position));
        //linearLayout.setBackgroundColor(color.get(position));

        img.setOnClickListener(view1 -> {
            //Intent intent = new Intent(context, ZoomImge.class);
            Intent intent = new Intent(context, GalleryActivity.class);
            intent.putExtra("imgCount",img_count);
            intent.putExtra("product_id",product_id);
            context.startActivity(intent);

        });

        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}

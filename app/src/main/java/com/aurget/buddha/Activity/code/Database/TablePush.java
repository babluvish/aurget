package com.aurget.buddha.Activity.code.Database;

public class TablePush {
    static final String SQL_CREATE_PUSH = ("CREATE TABLE push (" +
            pushColumn.message + " VARCHAR," +
            pushColumn.largeIcon + " VARCHAR," +
            pushColumn.title + " VARCHAR," +
            pushColumn.id + " VARCHAR," +
            pushColumn.add_date + " VARCHAR," +
            pushColumn.type + " VARCHAR," +
            pushColumn.datetime + " VARCHAR" + ")");

    public static final String push = "push";

    public enum pushColumn {
        message,
        largeIcon,
        title,
        datetime,
        add_date,
        type,
        id
    }
}

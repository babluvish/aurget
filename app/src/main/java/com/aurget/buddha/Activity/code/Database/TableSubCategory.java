package com.aurget.buddha.Activity.code.Database;

public class TableSubCategory {
    static final String SQL_CREATE_SUB_CATEGORY = ("CREATE TABLE sub_category (" + subCatColumn.category_id + " VARCHAR," + subCatColumn.icon + " VARCHAR," + subCatColumn.sub_category_id + " VARCHAR," + subCatColumn.sub_category_name + " VARCHAR," + subCatColumn.json + " VARCHAR," + subCatColumn.status + " VARCHAR" + ")");
    public static final String sub_category = "sub_category";

    public enum subCatColumn {
        category_id,
        sub_category_id,
        sub_category_name,
        status,
        icon,
        json
    }
}

package com.aurget.buddha.Activity.code.Dashboard.Slider;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aurget.buddha.R;


public class CardFragment extends Fragment {
    private CardView mCardView;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_adapter, container, false);
        this.mCardView = (CardView) view.findViewById(R.id.cardView);
        this.mCardView.setMaxCardElevation(this.mCardView.getCardElevation() * 8.0f);
        return view;
    }

    public CardView getCardView() {
        return this.mCardView;
    }
}

package com.aurget.buddha.Activity.code.Dashboard.Slider;

import android.content.Context;

import androidx.viewpager.widget.PagerAdapter;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.aurget.buddha.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class CardPagerAdapter extends PagerAdapter implements CardAdapter {
    public OnClickListener clickListener;
    Context context;
    ArrayList<HashMap<String, String>> data;
    private float mBaseElevation;
    private List<String> mData = new ArrayList();
    private List<CardView> mViews = new ArrayList();

    private class ImageLoadedCallback implements Callback {
        ProgressBar progressBar;

        public ImageLoadedCallback(ProgressBar progBar) {
            this.progressBar = progBar;
        }

        public void onSuccess() {
        }

        public void onError() {
        }
    }

    protected abstract void onCategoryClick(View view, String str);

    public CardPagerAdapter(Context context, ArrayList<HashMap<String, String>> banner_list) {
        this.context = context;
        this.data = banner_list;
        for (int i = 0; i < this.data.size(); i++) {
            this.mData.add("");
            this.mViews.add(null);
        }
        clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCategoryClick(v, String.valueOf(v.getTag()));
            }
        };
    }

    public float getBaseElevation() {
        return this.mBaseElevation;
    }

    public CardView getCardViewAt(int position) {
        return (CardView) this.mViews.get(position);
    }

    public int getCount() {
        return this.data.size();
    }

    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.adapter, container, false);
        container.addView(view);
        CardView cardView = (CardView) view.findViewById(R.id.cardView);
        ImageView iv = (ImageView) view.findViewById(R.id.iv);
        if (this.mBaseElevation == 0.0f) {
            this.mBaseElevation = cardView.getCardElevation();
        }
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar2);
        int size = (int) Math.ceil(Math.sqrt(786432.0d));
        if (((String) ((HashMap) this.data.get(position)).get("banner_image")).equalsIgnoreCase("")) {
            Picasso.with(this.context).load((int) R.mipmap.ic_launcher).resize(815,315).into(iv);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            Picasso.with(this.context).load((String) ((HashMap) this.data.get(position)).get("banner_image")).resize(815,315).into(iv);
        }
        view.setTag(position);
        view.setOnClickListener(clickListener);
        cardView.setMaxCardElevation(this.mBaseElevation * ((float) this.data.size()));
        this.mViews.set(position, cardView);
        return view;
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        this.mViews.set(position, null);
    }

}

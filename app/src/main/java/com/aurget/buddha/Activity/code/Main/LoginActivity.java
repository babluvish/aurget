package com.aurget.buddha.Activity.code.Main;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.code.Common.RoundedImageView;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Fragment.DashBoardFragment;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.Activity.code.volly.AbstrctClss;
import com.aurget.buddha.R;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import code.utils.AppUrls;
import code.volly.ConstntApi;

import static com.aurget.buddha.Activity.code.Database.AppSettings.dob;
import static java.lang.Integer.parseInt;

public class LoginActivity extends BaseActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    //Edittext
    EditText etPassword, etEmail, etForgotEmail;
    //Button
    Button btnLogin, loginOTP;
    //LinearLayout
    LinearLayout llFacebook, llGoogle, llSignUp, lin;
    //RelativeLayout
    RelativeLayout rlBack;
    //Textview
    TextView tvSignUp, tvForgotPassword;
    //private ProfileTracker mProfileTracker;
    //Imageview
    RoundedImageView ivLogo;

    private GoogleApiClient mGoogleApiClient;
    public static GoogleSignInOptions gso;
    private boolean signedInUser;
    String facebook_id, name, email, google_id;
    String social = "";
    private boolean mIntentInProgress;
    private ConnectionResult mConnectionResult;
    private static final String TAG = "Customlog";
    private SignInButton signInButton;
    private GoogleApiClient googleApiClient;
    private static final int RC_SIGN_IN = 1;
    String idToken;

    private static final String TAGS = "FacebookLogin";
    private static final int RC_SIGN_IN_FB = 12345;
    //    CallbackManager mCallbackManager;
//    CallbackManager callbackManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        //Edittext
        etPassword = findViewById(R.id.etPassword);
        etEmail = findViewById(R.id.etEmail);

        //Button
        btnLogin = findViewById(R.id.btnLogin);
        loginOTP = findViewById(R.id.loginOTP);

        //TextView
        tvSignUp = findViewById(R.id.tvSignUp);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);

        //LinearLayout
        llFacebook = findViewById(R.id.llFacebook);
        llGoogle = findViewById(R.id.llGoogle);
        llSignUp = findViewById(R.id.llSignUp);
        //signInButton = findViewById(R.id.sign_in_button);

        //RelativeLayout
        rlBack = findViewById(R.id.rlBack);

        //Imageview
        ivLogo = findViewById(R.id.ivLogo);

        try {
            Googleplus();
        } catch (Exception e) {
            Log.e("jsdbk", String.valueOf(e));
        }
        //Calling Facebook
        //facebook();
        //SetonClickListener
        etPassword.setOnClickListener(this);
        llFacebook.setOnClickListener(this);
        //signInButton.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        // tvSignUp.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
        ivLogo.setOnClickListener(this);
        llSignUp.setOnClickListener(this);
        loginOTP.setOnClickListener(this);
        loginOTP.setClickable(true);
    }

    //check vaildation for password
    public boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
                finish();
                break;

            case R.id.loginOTP:
                //dilog()
                if (etEmail.getText().toString().trim().length() == 0) {
                    AppUtils.showErrorMessage(etEmail, getString(R.string.errorMobileNumber), this.mActivity);
                    etEmail.requestFocus();
                    return;
                }
                loginOTP.setClickable(false);
                mobileNO(etEmail.getText().toString().trim());
                break;

            case R.id.ivLogo:
                finish();
                break;
            case R.id.tvForgotPassword:
                AlertLogoutPopUp();
              /*  if (SimpleHTTPConnection.isNetworkAvailable(this.mActivity))
                {
                    ForgotApi();
                }
                else
                {
                    AppUtils.showErrorMessage(etEmail, getString(R.string.error_connection_message), this.mActivity);
                }*/
                break;
            case R.id.btnLogin:
                if (etEmail.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please enter your mobile no.", Toast.LENGTH_SHORT).show();
                } else if (etEmail.getText().toString().trim().length() != 10) {
                    Toast.makeText(this, "Please enter your valid mobile no.", Toast.LENGTH_SHORT).show();
                } else if (etPassword.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please enter your password", Toast.LENGTH_SHORT).show();
                }
              /*  else if (etPassword.getText().toString().length()<7)
                {
                    Toast.makeText(this, "Please enter minimum 7 digit password", Toast.LENGTH_SHORT).show();
                }*/
              /*  else if (!isValidPassword(etPassword.getText().toString().trim()))
                {
                    Toast.makeText(this, "Please enter valid password", Toast.LENGTH_SHORT).show();
                }*/
                else {
                    AppUtils.hideSoftKeyboard(mActivity);
                    if (SimpleHTTPConnection.isNetworkAvailable(this.mActivity)) {
                        LoginApi("1");
                    } else {
                        AppUtils.showErrorMessage(etEmail, getString(R.string.error_connection_message), this.mActivity);
                    }
                }
                break;
            case R.id.llFacebook:
                //LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email", ""));
                break;

/*            case R.id.sign_in_button:
//                Intent i=new Intent(LoginActivity.this,Customlog.class);
//                startActivity(i);
                signIn();
                break;*/

            case R.id.llSignUp:
                Intent j = new Intent(this, SignUpActivity.class);
                startActivity(j);
                break;
        }

    }

    //========================================AlertPopup===================================================//
    private void AlertLogoutPopUp() {
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_forgot);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();

        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        ImageView ivCross = dialog.findViewById(R.id.ivCross);

        etForgotEmail = dialog.findViewById(R.id.etEmail);

        final TextView tvOkay = dialog.findViewById(R.id.tvOkay);

        ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvOkay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (etForgotEmail.getText().toString().isEmpty()) {
                    Toast.makeText(dialog.getContext(), "Please enter your email Id", Toast.LENGTH_SHORT).show();
                } else if (!AppUtils.isEmailValid(etForgotEmail.getText().toString())) {
                    Toast.makeText(dialog.getContext(), "Please enter your valid email id", Toast.LENGTH_SHORT).show();
                } else {
                    dialog.dismiss();
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                        ForgotApi();
                    } else {
                        AppUtils.showErrorMessage(tvOkay, getString(R.string.errorInternet), mActivity);
                    }
                }

                onResume();
            }
        });
    }

    //=====================================================Google Plus==================================================//
    public void Googleplus() {
//        firebaseAuth = com.google.firebase.auth.FirebaseAuth.getInstance();
//        //this is where we start the Auth state Listener to listen for whether the user is signed in or not
//        authStateListener = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                // Get signedIn user
//                FirebaseUser user = firebaseAuth.getCurrentUser();
//
//                //if user is signed in, we call a helper method to save the user details to Firebase
//                if (user != null) {
//                    // User is signed in
//                    // you could place other firebase code
//                    //logic to save the user details to Firebase
//                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
//                } else {
//                    // User is signed out
//                    Log.d(TAG, "onAuthStateChanged:signed_out");
//                }
//            }
//        };

//        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestIdToken(getString(R.string.web_client_id))//you can also use R.string.default_web_client_id
//                .requestEmail()
//                .build();
//        googleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(this, this)
//                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                .build();


    }


    private void signIn() {
//        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
//        startActivityForResult(intent, RC_SIGN_IN);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Facebook
//        callbackManager.onActivityResult(requestCode, resultCode, data);

        //mCallbackManager.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            handleSignInResult(result);

        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (!connectionResult.hasResolution()) {

            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this, 0).show();

            return;
        }
        if (!mIntentInProgress) {
            // store mConnectionResult
            mConnectionResult = connectionResult;

            if (signedInUser) {


            }

        }
    }

    //===========================Facebook=============================//
    /*public void facebook() {
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        // Initialize Facebook Login button
        mCallbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = findViewById(R.id.login_button);
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken(), loginResult);
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {

            }
        });


    }*/

    void dilog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.login3);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mobilNoEt = dialog.findViewById(R.id.mobilNoEt);
        one = dialog.findViewById(R.id.one);
        two = dialog.findViewById(R.id.two);
        three = dialog.findViewById(R.id.three);
        four = dialog.findViewById(R.id.four);

        lin = dialog.findViewById(R.id.lin);
        resendOtp = dialog.findViewById(R.id.resendOtp);
        resendOtp.setVisibility(View.VISIBLE);
        mobilNoEt.setVisibility(View.GONE);
        lin.setVisibility(View.VISIBLE);

        Button loginBtn = dialog.findViewById(R.id.loginBtn);

        one.requestFocus();
        AppUtils.countDown(resendOtp);
        //loginBtn.setText("Get OTP");

        /*one.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (one.getText().length() == 1) {
                    two.requestFocus();
                } else if (one.getText().length() == 0) {
                    one.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });*/
        two.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (two.getText().length() == 1) {
                    three.requestFocus();
                } else if (two.getText().length() == 0) {
                    one.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        three.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (three.getText().length() == 1) {
                    four.requestFocus();
                } else if (three.getText().length() == 0) {
                    two.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        four.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (four.getText().length() == 1) {
                    four.requestFocus();
                } else if (four.getText().length() == 0) {
                    three.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        resendOtp.setOnClickListener(view -> {

            if (resendOtp.getText().toString().equals("Resend OTP")) {
                mobileNO(etEmail.getText().toString().trim());
            }
            //sendOtp(mobEt.getText().toString());
            //new GetWeatherTask().execute(ConstntApi.send_otp_login(Login.this,mobEt.getText().toString()));
            //otpLogin(mobEt.getText().toString(),mobilNoEt.getText().toString());
        });


        loginBtn.setOnClickListener(view -> {

            if (lin.getVisibility() == View.VISIBLE && istrue) {
                LoginApi("3");
                return;
            }

            if (one.getText().toString().trim().length() == 0) {
                AppUtils.showErrorMessage(etEmail, getResources().getString(R.string.errorOTP), mActivity);
                return;
            }

            if (otp == parseInt(one.getText().toString().trim())) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                intent.putExtra("mobile", etEmail.getText().toString().trim());
                startActivity(intent);
            } else {
                AppUtils.showErrorMessage(etEmail, getResources().getString(R.string.invalid_otp), mActivity);
            }
            //mobileNO(etEmail.getText().toString().trim());
        });

        dialog.show();
    }


    //======================================API==========================================================//
    private void mobileNO(String type) {
        new AbstrctClss(mActivity, ConstntApi.mobilesendOtp(mActivity, type), "p", true) {
            @Override
            public void responce(String s) {
                try {
                    try {
                        Log.d("jsonArray", s);
                        JSONObject jsonObject = new JSONObject(s);
                        if (jsonObject.optString("status").equalsIgnoreCase("success")) {
                            //resendOtp.setVisibility(View.VISIBLE);
                            //mobilNoEt.setVisibility(View.GONE);
//                            one.setText("");
//                            two.setText("");
//                            three.setText("");
//                            four.setText("");
//                            one.requestFocus();
                            istrue = true;
                        } else {
                            //resendOtp.setVisibility(View.VISIBLE);
                            //mobilNoEt.setVisibility(View.GONE);
//                            one.setText("");
//                            two.setText("");
//                            three.setText("");
//                            four.setText("");
                            //one.requestFocus();

                            otp = jsonObject.optInt("otp");

                            istrue = false;
                            //AppUtils.showToastSort(mActivity,jsonObject.optString("message"));
                            //AppUtils.showToastSort(etEmail, jsonObject.optString("message"), mActivity);
                        }

                        dilog();
                        loginOTP.setClickable(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("letestFeturePi", "sssss");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd", "sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                Log.d("dsd", "ssssssssssss");
            }
        };
    }

    private void LoginApi(String type) {
        AppUtils.showRequestDialog(mActivity);
        Log.v("LoginApi", AppUrls.Login);
        String url = "";
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            if (type.equals("1")) {
                url = AppUrls.Login;
                json_data.put("phone_no", etEmail.getText().toString());
                json_data.put("password", etPassword.getText().toString());
                json_data.put("googleid", "");
                json_data.put("fb_id", "");
                json_data.put("user_type_login", type);
                json_data.put("fcm_id", "");
                json_data.put("device_id", AppUtils.getDeviceID(getApplicationContext()));
            } else {
                url = AppUrls.Login2;
                json_data.put("phone_no", etEmail.getText().toString());
                //json_data.put("otp", one.getText().toString().trim() + two.getText().toString().trim() + three.getText().toString().trim() + four.getText().toString().trim());
                json_data.put("otp", one.getText().toString().trim());
                json_data.put("user_type_login", type);
                json_data.put("device_id", AppUtils.getDeviceID(getApplicationContext()));

                /*if (social.equals("1")) {
                    json_data.put("email_id", email);
                    json_data.put("password", "");
                    json_data.put("googleid", google_id);
                    json_data.put("fb_id", "");
                    json_data.put("user_type_login", type);
                    json_data.put("fcm_id", AppSettings.getString(AppSettings.fcmId));
                    json_data.put("device_id", AppUtils.getDeviceID(getApplicationContext()));
                } else {
                    json_data.put("email_id", "");
                    json_data.put("password", "");
                    json_data.put("googleid", "");
                    json_data.put("fb_id", facebook_id);
                    json_data.put("user_type_login", type);
                    json_data.put("fcm_id", AppSettings.getString(AppSettings.fcmId));
                    json_data.put("device_id", AppUtils.getDeviceID(getApplicationContext()));
                }*/
            }
            json.put(AppConstants.result, json_data);
            Log.v("LoginApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(url)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(etEmail, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(etEmail, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }
    private void parseJsondata(JSONObject response) {
        Log.d("response ", response.toString());
        AppUtils.hideDialog();
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppSettings.putString(AppSettings.userId, jsonObject.getString("id"));
                AppSettings.putString(AppSettings.name, jsonObject.getString("username"));
                AppSettings.putString(AppSettings.email, jsonObject.getString("email_id"));
                AppSettings.putString(AppSettings.gender, jsonObject.getString("gender"));
                AppSettings.putString(dob, jsonObject.getString("dob"));
                AppSettings.putString(AppSettings.mobile, jsonObject.getString("phone_no"));
                AppSettings.putString(AppSettings.usertype, jsonObject.getString("user_type_login"));
                AppSettings.putString(AppSettings.user_pic, jsonObject.getString("profile_pic"));

                if (istrue)
                    startActivity(new Intent(getBaseContext(), DashBoardFragment.class));
                else
                    startActivity(new Intent(getBaseContext(), SignUpActivity.class));
                Log.v("12398948954", "jfjjjjgjjgh");
            } else {
                AppUtils.showErrorMessage(etEmail, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(etEmail, String.valueOf(e), mActivity);
        }
    }

    private void ForgotApi() {
        AppUtils.showRequestDialog(mActivity);
        Log.v("LoginApi", AppUrls.ForgetPassword);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("email_id", etForgotEmail.getText().toString());
            json.put(AppConstants.result, json_data);
            Log.v("LoginApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.ForgetPassword)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsonForgot(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(etEmail, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(etEmail, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseJsonForgot(JSONObject response) {
        Log.d("response ", response.toString());
        AppUtils.hideDialog();
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                Toast.makeText(this, jsonObject.getString("res_msg"), Toast.LENGTH_LONG).show();
                //  startActivity(new Intent(getBaseContext(), DashboardActivity.class));
                //  Log.v("12398948954","jfjjjjgjjgh");
            } else {
                AppUtils.showErrorMessage(etEmail, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(etEmail, String.valueOf(e), mActivity);
        }

    }

    /*private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            name = acct.getDisplayName();
            email = acct.getEmail();
            google_id = acct.getId();
            String personPhoto = String.valueOf(acct.getPhotoUrl());
            AppSettings.putString(AppSettings.profilePic, personPhoto);

            social = "1";
            Log.v("gName", name);
            Log.v("gemail", email);
            Log.v("gdob", dob);
            Log.v("ggoogle_id", google_id);
            Log.v("gsocial", social);

            AppSettings.putString(AppSettings.googleName, name);
            AppSettings.putString(AppSettings.googleEmail, email);
            AppSettings.putString(AppSettings.googledob, "");
            AppSettings.putString(AppSettings.googleId, google_id);
            AppSettings.putString(AppSettings.social, "1");

            if (SimpleHTTPConnection.isNetworkAvailable(this.mActivity)) {
                LoginApi("2");
            } else {
                AppUtils.showErrorMessage(etEmail, getString(R.string.error_connection_message), this.mActivity);
            }

        } else {
            System.out.print(result.getStatus());
        }


//        if(result.isSuccess()){
//            GoogleSignInAccount account = result.getSignInAccount();
//            idToken = account.getIdToken();
//            name = account.getDisplayName();
//            email = account.getEmail();
//            Toast.makeText(LoginActivity.this, email, Toast.LENGTH_SHORT).show();
//            AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
//            firebaseAuthWithGoogle(credential);
//        }else{
//            Log.e(TAG, "Login Unsuccessful. "+result);
//            Toast.makeText(this, "Login Unsuccessful", Toast.LENGTH_SHORT).show();
//        }
    }
    private void firebaseAuthWithGoogle(AuthCredential credential) {

        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        if (task.isSuccessful()) {
                            Toast.makeText(LoginActivity.this, "Login successful", Toast.LENGTH_SHORT).show();
                            gotoProfile();
                        } else {
                            Log.w(TAG, "signInWithCredential" + task.getException().getMessage());
                            task.getException().printStackTrace();
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }*/

    private void gotoProfile() {

    }

    @Override
    protected void onStart() {
        super.onStart();
        /*try {
            if (authStateListener != null) {
                FirebaseAuth.getInstance().signOut();
            }
            firebaseAuth.addAuthStateListener(authStateListener);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser != null) {
                Log.d(TAG, "Currently Signed in: " + currentUser.getEmail());
                Toast.makeText(LoginActivity.this, "Currently Logged in: " + currentUser.getEmail(), Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

    @Override
    protected void onStop() {
        super.onStop();
//        if (authStateListener != null) {
//            firebaseAuth.removeAuthStateListener(authStateListener);
//        }
    }

    /*private void handleFacebookAccessToken(AccessToken token, LoginResult l) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, UI will update with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            String name = user.getDisplayName();
                            facebook_id = user.getUid();

                            String profilePic = user.getPhotoUrl().toString();
                            AppSettings.putString(AppSettings.profilePic, String.valueOf(profilePic));
                            AppSettings.putString(AppSettings.fbname, name);
                            AppSettings.putString(AppSettings.fbId, facebook_id);

                            social = "2";

                            AppSettings.putString(AppSettings.social, "2");
                            if (SimpleHTTPConnection.isNetworkAvailable(LoginActivity.this)) {
                                LoginApi("2");
                            } else {
                                AppUtils.showErrorMessage(etEmail, getString(R.string.error_connection_message), LoginActivity.this);
                            }

                            LoginManager.getInstance().logOut();
                            Toast.makeText(LoginActivity.this, user.getDisplayName(), Toast.LENGTH_SHORT).show();
                        } else {
                            // If sign-in fails, a message will display to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }*/

//    public void facebookSuccess(LoginResult loginResult) {
//
//        GraphRequest request = GraphRequest.newMeRequest(
//                loginResult.getAccessToken(),
//                new GraphRequest.GraphJSONObjectCallback() {
//                    @Override
//                    public void onCompleted(
//                            JSONObject object,
//                            GraphResponse response) {
//                        Log.v("LoginActivity Response ", response.toString());
//                        try {
//                            Profile profile = Profile.getCurrentProfile();
//                            String id = profile.getId();
//                            String link = profile.getLinkUri().toString();
//                            Log.i("Link", link);
//                            if (Profile.getCurrentProfile() != null) {
//                                Log.i("Login", "ProfilePic" + Profile.getCurrentProfile().getProfilePictureUri(200, 200));
//                            }
//                            name = object.getString("name");
//                            facebook_id = object.getString("id");
//                            int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.com_facebook_profilepictureview_preset_size_large);
//                            // Uri profilePictureUri= Profile.getCurrentProfile().getProfilePictureUri(dimensionPixelSize , dimensionPixelSize);
//
//                            if (Profile.getCurrentProfile() != null) {
//                                Uri profilePictureUri = ImageRequest.getProfilePictureUri(Profile.getCurrentProfile().getId(), dimensionPixelSize, dimensionPixelSize);
//                                AppSettings.putString(profilePic, String.valueOf(profilePictureUri));
//                            }
//
//                            /*String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";*/
//                            AppSettings.putString(AppSettings.fbname, name);
//                            AppSettings.putString(AppSettings.fbId, facebook_id);
//
//                            social = "2";
//                            AppSettings.putString(AppSettings.social, "2");
//                            if (SimpleHTTPConnection.isNetworkAvailable(LoginActivity.this)) {
//                                LoginApi("2");
//                            } else {
//                                AppUtils.showErrorMessage(etEmail, getString(R.string.error_connection_message), LoginActivity.this);
//                            }
//                            LoginManager.getInstance().logOut();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//
//        Bundle parameters = new Bundle();
//        parameters.putString("fields", "id,name,email,gender, birthday");
//        request.setParameters(parameters);
//        request.executeAsync();
//    }
//com.aurgetmart.mega

    EditText mobilNoEt;
    EditText one;
    EditText two;
    EditText three;
    EditText four;
    TextView resendOtp;
    boolean istrue;
    int otp;
}

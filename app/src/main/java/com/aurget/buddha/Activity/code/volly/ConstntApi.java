package code.volly;

import android.app.Activity;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import code.utils.AppUrls;

public class ConstntApi {

    public static final String mobilesendOtp(Activity activity,String mobile)
    {
        return AppUrls.baseUrl+"Sends?phone="+ mobile;
    }

    public static final String after_payment(Activity activity,JSONObject json_data)
    {
        return AppUrls.after_payment+"phone="+json_data.optString("phone")
                +"&order_id="+json_data.optString("order_id")
                +"&username="+json_data.optString("username")
                +"&amount="+json_data.optString("amount");
    }

    public static final String updateProfileApi(Activity activity,JSONObject json_data)
    {
        return AppUrls.update_profile+"id="+json_data.optString("id")
                +"&profile_image="+json_data.optString("profile_image")
                +"&extensions="+json_data.optString("extensions");
    }
    public static final String updateProfileApi()
    {
        //http://aurget.in/mart/demo/api/Get_notification
        return AppUrls.Get_notification;
    }
}

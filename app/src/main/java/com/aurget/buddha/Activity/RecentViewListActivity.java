package com.aurget.buddha.Activity;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.categories.SubCategoryDetails2;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.aurget.buddha.Activity.Commonhelper.discountAmt;
import static com.aurget.buddha.Activity.Commonhelper.getScreenWidth;
import static com.aurget.buddha.Activity.Commonhelper.roudOff;
import static java.lang.Double.parseDouble;

public class RecentViewListActivity extends AppCompatActivity {

    RecyclerView recycleView;
    Activity activity = this;
    float divideLine = 2.0f;
    TextView title ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_view_list);
        recycleView = findViewById(R.id.recycleView);
        title = findViewById(R.id.title);

        GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recycleView.setLayoutManager(manager);


        if (getIntent().hasExtra("pos"))
        {
              if (getIntent().getStringExtra("pos").equals("recentView"))
              {
                  title.setText("Recent View List");
                  recentViewPi();
              }
              else if (getIntent().getStringExtra("pos").equals("latesstFeatrue"))
              {
                  title.setText("Latest Feature Product");
                  letestFeturePi();
              }
              else if (getIntent().getStringExtra("pos").equals("fastMoving"))
              {
                  title.setText("Fast Moving");
                  fastMovingapi();
              }
              else if (getIntent().getStringExtra("pos").equals("todayDeal"))
              {
                  title.setText(getString(R.string.to_day_s_deal));
                  to_day_s_deal();
              }
              else if (getIntent().getStringExtra("pos").equals("latest_arrival"))
              {
                  title.setText(getString(R.string.latest_arrival));
                  latestArrival();
              }
              else if (getIntent().getStringExtra("pos").equals("reletedProduct"))
              {
                  title.setText(getString(R.string.reletedProducts));
                  reletedProductAPi();
              }
        }
        else {
            title.setText("Recent View List");
            recentViewPi();
        }

        findViewById(R.id.bkImg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void fastMovingapi()
    {
        new AbstrctClss(RecentViewListActivity.this, ConstntApi.fastMovingapi(RecentViewListActivity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    Log.d("fastMovingapi",Res);

                    JSONArray jsonArray = new JSONArray(Res);

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                    for (int i = 0;i<jsonArray.length();i++)
                    {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }
                    recycleView.setAdapter(new MyRecyclerAdapterRecentllyView(jsonObjectArrayList));

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd","sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                fastMovingapi();
            }
        };
    }

    private void to_day_s_deal()
    {
        new AbstrctClss(RecentViewListActivity.this, ConstntApi.deal_product(RecentViewListActivity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    Log.d("to_day_s_deal",Res);

                    JSONArray jsonArray = new JSONArray(Res);

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                    for (int i = 0;i<jsonArray.length();i++)
                    {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }
                    recycleView.setAdapter(new MyRecyclerAdapterRecentllyView(jsonObjectArrayList));

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd","sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                //fastMovingapi();
            }
        };
    }

    private void latestArrival()
    {
        new AbstrctClss(RecentViewListActivity.this, ConstntApi.latestArrival(RecentViewListActivity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    Log.d("latestArrival",Res);

                    JSONArray jsonArray = new JSONArray(Res);

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                    for (int i = 0;i<jsonArray.length();i++)
                    {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }

                    recycleView.setAdapter(new MyRecyclerAdapterRecentllyView(jsonObjectArrayList));

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd","sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
//                latestArrival();
            }
        };
    }

    private void letestFeturePi()
    {
        new AbstrctClss(RecentViewListActivity.this, ConstntApi.letestFeture(RecentViewListActivity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    Log.d("letestFeturePi",Res);

                    JSONArray jsonArray = new JSONArray(Res);

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                    for (int i = 0;i<jsonArray.length();i++)
                    {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }

                    recycleView.setAdapter(new MyRecyclerAdapterRecentllyView(jsonObjectArrayList));

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd","sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                letestFeturePi();
            }
        };
    }

    private void recentViewPi()
    {
        new AbstrctClss(RecentViewListActivity.this, ConstntApi.recentViewPi(RecentViewListActivity.this), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    String Res = s;
                    JSONArray jsonArray = new JSONArray(Res);

                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                    for (int i = 0;i<jsonArray.length();i++)
                    {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }

                    recycleView.setAdapter(new MyRecyclerAdapterRecentllyView(jsonObjectArrayList));

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd","sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
                recentViewPi();
            }
        };
    }

    private void reletedProductAPi()
    {
        new AbstrctClss(RecentViewListActivity.this, ConstntApi.recentViewPi_(RecentViewListActivity.this), "g", true) {
            @Override
            public void responce(String s) {
                try
                {
                    String Res = s;
                    JSONArray jsonArray = new JSONArray(Res);
                    ArrayList<JSONObject> jsonObjectArrayList = new ArrayList<>();

                    for (int i = 0;i<jsonArray.length();i++)
                    {
                        jsonObjectArrayList.add(jsonArray.getJSONObject(i));
                    }
                    recycleView.setAdapter(new MyRecyclerAdapterRecentllyView(jsonObjectArrayList));

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("dsd","sssss");
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    public class MyRecyclerAdapterRecentllyView extends RecyclerView.Adapter<MyRecyclerAdapterRecentllyView.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapterRecentllyView(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.today_deal_item, parent, false);
            v.getLayoutParams().width = (int) (getScreenWidth(activity) / divideLine); /// THIS LINE WILL DIVIDE OUR VIEW INTO NUMBERS OF PARTS
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            holder.prodNmeTv.setText(jsonObjectal.get(position).optString("title"));
            holder.ipTv.setText("IP "+jsonObjectal.get(position).optString("ip"));
            //double disAmt = discountAmt(parseDouble(jsonObjectal.get(position).optString("sale_price")),parseDouble(jsonObjectal.get(position).optString("discount")));
            //long value = roudOff(disAmt);
            holder.priceTv.setText("₹"+jsonObjectal.get(position).optString("sale_price"));
            int i = Integer.parseInt(jsonObjectal.get(position).optString("product_sub_id"));
            Picasso.with(activity).load(InterfaceClass.imgPth2+jsonObjectal.get(position).optString("product_id")+"_"+i+"_thumb.jpg").into(holder.img);
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView prodNmeTv,ipTv,priceTv;
            private ImageView img;

            public ViewHolder(View itemView) {
                super(itemView);

                prodNmeTv = itemView.findViewById(R.id.prodNmeTv);
                ipTv = itemView.findViewById(R.id.ipTv);
                priceTv = itemView.findViewById(R.id.priceTv);
                img = itemView.findViewById(R.id.img);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

//                        Intent intent = new Intent(RecentViewListActivity.this, SubCategoryDetails2.class);
//                        intent.putExtra("res",jsonObjectal.get(getAdapterPosition()).toString());
//                        int pos = getAdapterPosition()+1;
//                        intent.putExtra("image",InterfaceClass.imgPth2+jsonObjectal.get(getAdapterPosition()).optString("product_id")+"_"+pos+"_thumb.jpg");
//                        startActivity(intent);

                        Intent intent = new Intent(RecentViewListActivity.this, SubCategoryDetails2.class);
                        intent.putExtra("res",jsonObjectal.get(getAdapterPosition()).toString());
                        int pos = getAdapterPosition()+1;
                        intent.putExtra("imgCount",jsonObjectal.get(getAdapterPosition()).optString("num_of_imgs"));
                        intent.putExtra("product_id",jsonObjectal.get(getAdapterPosition()).optString("product_id"));
                        intent.putExtra("image",InterfaceClass.imgPth2+jsonObjectal.get(getAdapterPosition()).optString("product_id")+"_"+1+"_thumb.jpg");
                        startActivity(intent);

                    }
                });
            }

        }
    }

}

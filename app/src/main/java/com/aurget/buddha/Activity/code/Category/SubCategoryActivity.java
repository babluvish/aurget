package com.aurget.buddha.Activity.code.Category;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.code.Account.AccountActivity;
import com.aurget.buddha.Activity.code.Account.FavouriteListActivity;
import com.aurget.buddha.Activity.code.Adapter.SubCategoryAdapter;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Database.DatabaseController;
import com.aurget.buddha.Activity.code.Database.TableSubCategory;
import com.aurget.buddha.Activity.code.Fragment.DashBoardFragment;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.Main.NotificationActivity;
import com.aurget.buddha.Activity.code.Product.CartListActivity;
import com.aurget.buddha.Activity.code.Product.ProductDetailsActivity;
import com.aurget.buddha.Activity.code.Product.ProductListActivity;
import com.aurget.buddha.Activity.code.Search.SearchActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import code.utils.AppUrls;

public class SubCategoryActivity extends BaseActivity implements OnClickListener {
    public static String categoryId = "";
    ArrayList<HashMap<String, String>> SubCategoryList = new ArrayList();
    ArrayList<HashMap<String, String>> productList = new ArrayList();
    GridView gridView;
    ImageView ivMenu;
    ImageView ivSearch, ivCart;
    SubCategoryAdapter subCategoryAdapter;
    ProductListAdapter productListAdapter;
    TextView tvMain;
    RelativeLayout rrNoData;

    LinearLayout llHome, llCategory, llFavourites, llProfile, llNotification;

    TextView tvHome, tvCategory, tvFavourites, tvProfile, tvNotification, tvCount;

    ImageView ivHome, ivCategory, ivFavourites, ivProfile, ivNotification;


    RecyclerView recyclerview;
    LinearLayoutManager linearLayoutManager;

    boolean loadMore = true;

    String header="";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_subcategory );
        if(getIntent()!=null){
            header=getIntent().getStringExtra( "category_name" );
        }
        initialise();

    }

    private void initialise() {
        //TextView
        tvMain = findViewById( R.id.tvHeaderText );
        tvHome = findViewById( R.id.tvHomeBottom );
        tvCategory = findViewById( R.id.tvCategoryBottom );
        tvFavourites = findViewById( R.id.tvFavouritesBottom );
        tvProfile = findViewById( R.id.tvProfileBottom );
        tvNotification = findViewById( R.id.tvNotificationBottom );
        tvCount = findViewById( R.id.tvCount );

        ivMenu = findViewById( R.id.iv_menu );
        ivSearch = findViewById( R.id.searchmain );
        ivHome = findViewById( R.id.ivHomeBottom );
        ivCategory = findViewById( R.id.ivCategoryBottom );
        ivFavourites = findViewById( R.id.ivFavouritesBottom );
        ivProfile = findViewById( R.id.ic_profile );
        ivNotification = findViewById( R.id.ic_notification );
        ivCart = findViewById( R.id.ivCart);
        //GridView
        gridView = findViewById( R.id.gridView );

        //Recyclerview
        recyclerview = findViewById( R.id.recyclerview );


        linearLayoutManager = new LinearLayoutManager( mActivity, LinearLayoutManager.HORIZONTAL, false );
        recyclerview.setLayoutManager( linearLayoutManager );

        //recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true));

        //RelativeLayout
        rrNoData = findViewById( R.id.rrNoData );

        gridView.setNumColumns( 2 );

        tvMain.setText(header);

        //LinearLayout fro bottom views
        llHome = findViewById( R.id.llHome );
        llCategory = findViewById( R.id.llCategory );
        llFavourites = findViewById( R.id.llFavourites );
        llProfile = findViewById( R.id.llProfile );
        llNotification = findViewById( R.id.llNotification );

        llHome.setOnClickListener( this );
        llCategory.setOnClickListener( this );
        llFavourites.setOnClickListener( this );
        llProfile.setOnClickListener( this );
        llNotification.setOnClickListener( this );
        ivMenu.setOnClickListener( this );
        ivSearch.setOnClickListener( this );
        ivCart.setOnClickListener( this );
        ivMenu.setImageResource( R.drawable.ic_back );

        Log.v("MyPagePath","SubCategoryList");
        getCatData();

        if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
            getProductListApi();
        } else {
            AppUtils.showErrorMessage( rrNoData, getString( R.string.errorInternet ), mActivity );
        }


    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_menu:
                finish();
                return;
            case R.id.searchmain:
                startActivity( new Intent( getBaseContext(), SearchActivity.class ) );
                return;

            case R.id.llHome:
                tvHome.setTextColor( getResources().getColor( R.color.textcolor ) );
                tvCategory.setTextColor( getResources().getColor( R.color.dark_grey ) );
                tvFavourites.setTextColor( getResources().getColor( R.color.dark_grey ) );
                tvProfile.setTextColor( getResources().getColor( R.color.dark_grey ) );
                tvNotification.setTextColor( getResources().getColor( R.color.dark_grey ) );
                ivHome.setImageResource( R.drawable.ic_home_darkgreen );
                ivCategory.setImageResource( R.drawable.ic_categories_grey );
                ivFavourites.setImageResource( R.drawable.ic_heart_grey );
                ivProfile.setImageResource( R.drawable.ic_profile_grey );
                ivNotification.setImageResource( R.drawable.ic_notifications_grey );

                startActivity( new Intent( getBaseContext(), DashBoardFragment.class ) );

                return;

            case R.id.llCategory:

                tvHome.setTextColor( getResources().getColor( R.color.dark_grey ) );
                tvCategory.setTextColor( getResources().getColor( R.color.textcolor ) );
                tvFavourites.setTextColor( getResources().getColor( R.color.dark_grey ) );
                tvProfile.setTextColor( getResources().getColor( R.color.dark_grey ) );
                tvNotification.setTextColor( getResources().getColor( R.color.dark_grey ) );
                ivHome.setImageResource( R.drawable.ic_home_grey );
                ivCategory.setImageResource( R.drawable.ic_categories_darkgreen );
                ivFavourites.setImageResource( R.drawable.ic_heart_grey );
                ivProfile.setImageResource( R.drawable.ic_profile_grey );
                ivNotification.setImageResource( R.drawable.ic_notifications_grey );
                startActivity( new Intent( getBaseContext(), SubCategoryActivity.class ) );
                return;

            case R.id.llFavourites:
                if (AppSettings.getString( AppSettings.userId ).isEmpty()) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else if (AppSettings.getString( AppSettings.verified ).contains( "0" )) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else {
                    tvHome.setTextColor( getResources().getColor( R.color.dark_grey ) );
                    tvCategory.setTextColor( getResources().getColor( R.color.dark_grey ) );
                    tvFavourites.setTextColor( getResources().getColor( R.color.textcolor ) );
                    tvProfile.setTextColor( getResources().getColor( R.color.dark_grey ) );
                    tvNotification.setTextColor( getResources().getColor( R.color.dark_grey ) );
                    ivHome.setImageResource( R.drawable.ic_home_grey );
                    ivCategory.setImageResource( R.drawable.ic_categories_grey );
                    ivFavourites.setImageResource( R.drawable.ic_heart_darkgreen );
                    ivProfile.setImageResource( R.drawable.ic_profile_grey );
                    ivNotification.setImageResource( R.drawable.ic_notifications_grey );

                    startActivity( new Intent( getBaseContext(), FavouriteListActivity.class ) );
                }
                return;

            case R.id.llProfile:


                tvHome.setTextColor( getResources().getColor( R.color.dark_grey ) );
                tvCategory.setTextColor( getResources().getColor( R.color.dark_grey ) );
                tvFavourites.setTextColor( getResources().getColor( R.color.dark_grey ) );
                tvProfile.setTextColor( getResources().getColor( R.color.textcolor ) );
                tvNotification.setTextColor( getResources().getColor( R.color.dark_grey ) );
                ivHome.setImageResource( R.drawable.ic_home_grey );
                ivCategory.setImageResource( R.drawable.ic_categories_grey );
                ivFavourites.setImageResource( R.drawable.ic_heart_grey );
                ivProfile.setImageResource( R.drawable.ic_profile_darkgreen );
                ivNotification.setImageResource( R.drawable.ic_notifications_grey );

                //  startActivity(new Intent(getBaseContext(), AccountActivity.class));
                startActivity( new Intent( getBaseContext(), AccountActivity.class ) );


                return;


            case R.id.llNotification:
                tvHome.setTextColor( getResources().getColor( R.color.dark_grey ) );
                tvCategory.setTextColor( getResources().getColor( R.color.dark_grey ) );
                tvFavourites.setTextColor( getResources().getColor( R.color.dark_grey ) );
                tvProfile.setTextColor( getResources().getColor( R.color.dark_grey ) );
                tvNotification.setTextColor( getResources().getColor( R.color.textcolor ) );
                ivHome.setImageResource( R.drawable.ic_home_grey );
                ivCategory.setImageResource( R.drawable.ic_categories_grey );
                ivFavourites.setImageResource( R.drawable.ic_heart_grey );
                ivNotification.setImageResource( R.drawable.ic_notifications_green );
                ivProfile.setImageResource( R.drawable.ic_profile_grey );

                startActivity( new Intent( getBaseContext(), NotificationActivity.class ) );

                break;

            case R.id.ivCart:

                if (AppSettings.getString( AppSettings.userId ).isEmpty()) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else if (AppSettings.getString( AppSettings.verified ).contains( "0" )) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else {
                    startActivity( new Intent( getBaseContext(), CartListActivity.class ) );
                }

                return;

            default:
                return;
        }
    }

    public void getCatData() {
        SubCategoryList.clear();

        SubCategoryList.addAll( DatabaseController.getSubCategoryData( TableSubCategory.sub_category, TableSubCategory.subCatColumn.category_id.toString(), categoryId ) );

        Log.v( "hello", categoryId );

        if (SubCategoryList.size() > 0) {
            Log.v( "hello1", String.valueOf( SubCategoryList.size() ) );
            rrNoData.setVisibility( View.GONE );
            recyclerview.setVisibility( View.VISIBLE );
            productListAdapter = new ProductListAdapter( SubCategoryList );
            recyclerview.setAdapter( productListAdapter );
            // recyclerview.setNestedScrollingEnabled(true);
            //  recyclerview.smoothScrollToPosition(0);
            // setSubCatAdapter();
        } else {
            rrNoData.setVisibility( View.VISIBLE );
            recyclerview.setVisibility( View.GONE );
        }
    }

    public void setSubCatAdapter() {
     /*   productList = new ProductListAdapter(SubCategoryList) {
            protected void catClick(View v, String position) {
                Intent mIntent = new Intent(mActivity, ProductListActivity.class);
                mIntent.putExtra(AppConstants.subCategoryId, (String) ((HashMap) SubCategoryList.get(Integer.parseInt(position))).get("sub_category_id"));
                mIntent.putExtra(AppConstants.from, "4");
                startActivity(mIntent);
            }
        };*/
        // gridView.setAdapter(subCategoryAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        tvHome.setTextColor( getResources().getColor( R.color.dark_grey ) );
        tvCategory.setTextColor( getResources().getColor( R.color.textcolor ) );
        tvFavourites.setTextColor( getResources().getColor( R.color.dark_grey ) );
        tvProfile.setTextColor( getResources().getColor( R.color.dark_grey ) );
        tvNotification.setTextColor( getResources().getColor( R.color.dark_grey ) );
        ivHome.setImageResource( R.drawable.ic_home_grey );
        ivCategory.setImageResource( R.drawable.ic_categories_darkgreen );
        ivFavourites.setImageResource( R.drawable.ic_heart_grey );
        ivProfile.setImageResource( R.drawable.ic_profile_grey );
        ivNotification.setImageResource( R.drawable.ic_notifications_grey );

        int count = Integer.parseInt( AppSettings.getString( AppSettings.cartCount ) );

        if (count > 0) {
            tvCount.setVisibility( View.VISIBLE );
            tvCount.setText( String.valueOf( count ) );
        } else {
            tvCount.setVisibility( View.GONE );
        }
    }


    private void getProductListApi() {

        Log.v( "getProductListApi", AppUrls.Getproductbyid );

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put( "id", categoryId );

            json.put( AppConstants.result, json_data );

            Log.v( "getProductListApi", json.toString() );

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post( AppUrls.Getproductbyid )
                .addJSONObjectBody( json )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        parseJsondata( response );

                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage( rrNoData, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            AppUtils.showErrorMessage( rrNoData, String.valueOf( error.getErrorDetail() ), mActivity );
                        }
                    }
                } );
    }


    private void parseJsondata(JSONObject response) {

        Log.d( "response ", response.toString() );

        try {
            JSONObject jsonObject = response.getJSONObject( AppConstants.responseTag );
            if (jsonObject.getString( "res_code" ).equals( "1" )) {

                JSONArray productArray = jsonObject.getJSONArray( "product_details" );

                for (int i = 0; i < productArray.length(); i++) {
                    JSONObject productobject = productArray.getJSONObject( i );
                    HashMap<String, String> prodList = new HashMap();
                    prodList.put( "product_name", productobject.getString( "product_name" ) );
                    prodList.put( "final_price", productobject.getString( "final_price" ) );
                    prodList.put( "product_id", productobject.getString( "product_id" ) );
                    prodList.put( "product_unique_id", productobject.getString( "product_unique_id" ) );
                    prodList.put( "price", productobject.getString( "price" ) );
                    prodList.put( "discount", productobject.getString( "discount" ) );
                    prodList.put( "unit_price", productobject.getString( "unit_price" ) );
                    prodList.put( "image", productobject.getString( "image" ) );
                    prodList.put( "product_discount_type", productobject.getString( "product_discount_type" ) );
                    productList.add( prodList );
                }
                Log.v( "productlist", productList.toString() );
                subCategoryAdapter = new SubCategoryAdapter( getApplicationContext(), productList ) {
                    @Override
                    protected void catClick(View view, String str) {
                        Intent mIntent = new Intent( mActivity, ProductDetailsActivity.class );
                        mIntent.putExtra( AppConstants.productId, productList.get( Integer.parseInt( str ) ).get( "product_id" ) );
                        mIntent.putExtra( AppConstants.from, "4" );
                        startActivity( mIntent );
                    }
                };
                gridView.setAdapter( subCategoryAdapter );
                //  gridView.setNestedScrollingEnabled(true);
                // }
            } else {
                loadMore = false;
                AppUtils.showErrorMessage( rrNoData, String.valueOf( jsonObject.getString( "res_msg" ) ), mActivity );
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage( rrNoData, String.valueOf( e ), mActivity );
        }
        AppUtils.hideDialog();
    }


    //==================================================Adapter================================================================//


    private class ProductListAdapter extends RecyclerView.Adapter<FavNameHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

        public ProductListAdapter(ArrayList<HashMap<String, String>> favList) {
            this.data = favList;
        }

        public FavNameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new FavNameHolder( LayoutInflater.from( parent.getContext() ).inflate( R.layout.subcategoryadapter, parent, false ) );

        }

        public void onBindViewHolder(FavNameHolder holder, final int position) {

            holder.tvSubcategory.setText( ((String) ((HashMap) this.data.get( position )).get( "sub_category_name" )).trim() );
            if (((String) ((HashMap) data.get( position )).get( "icon" )).isEmpty()) {
                Picasso.with( mActivity ).load( R.mipmap.ic_launcher ).into( holder.ivPic );
            } else {
                Picasso.with( mActivity ).load( (String) ((HashMap) data.get( position )).get( "icon" ) ).into( holder.ivPic );
            }

            holder.rr.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent mIntent = new Intent( mActivity, ProductListActivity.class );
                    mIntent.putExtra( AppConstants.subCategoryId, (String) ((HashMap) SubCategoryList.get( position )).get( "sub_category_id" ) );
                    mIntent.putExtra( AppConstants.from, "4" );
                    startActivity( mIntent );
                }
            } );


        }

        public int getItemCount() {
            return data.size();
        }
    }


    private class FavNameHolder extends RecyclerView.ViewHolder {
        ImageView ivPic;
        RelativeLayout rr;
        TextView tvSubcategory;

        public FavNameHolder(View itemView) {
            super( itemView );
            rr = itemView.findViewById( R.id.rr );
            ivPic = itemView.findViewById( R.id.imageView24 );
            tvSubcategory = itemView.findViewById( R.id.textView29 );
        }
    }
}

package com.aurget.buddha.Activity.YourOrder;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.aurget.buddha.Activity.Commonhelper.openBrowser;

public class YourOrderDetils extends BaseActivity {

    private LinearLayout raiseReturnLL;
    private RecyclerView recycleView;
    private TextView grandTotalTv, totalIPTv, shippingAddress, cancel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.your_order_items);

        raiseReturnLL = findViewById(R.id.raiseReturnLL);
        recycleView = findViewById(R.id.rv);
        grandTotalTv = findViewById(R.id.grandTotalTv);
        totalIPTv = findViewById(R.id.totalIPTv);
        shippingAddress = findViewById(R.id.shippingAddress);
        cancel = findViewById(R.id.cancel);

        final LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recycleView.setLayoutManager(horizontalLayoutManager);
        recycleView.setItemAnimator(new DefaultItemAnimator());

        raiseReturnLL.setOnClickListener(view -> {
            Intent intent = new Intent(YourOrderDetils.this, RaiseReturn.class);
            intent.putExtra("orderID", getIntent().getStringExtra("orderID"));
            startActivity(intent);
        });

        findViewById(R.id.hotLine).setOnClickListener(view -> Commonhelper.phoneCall(YourOrderDetils.this));

        findViewById(R.id.returnPolicyTv).setOnClickListener(view -> openBrowser(YourOrderDetils.this, Constnt.returnPolicy));

        cancel.setOnClickListener(view -> cancelOrder(getIntent().getStringExtra("orderID")));

        //recycleView.setAdapter(new MyRecyclerAdapter(null));

        order_list(getIntent().getStringExtra("orderID"));
        address(getIntent().getStringExtra("orderID"));
    }

    private void cancelOrder(String orderID) {
        new AbstrctClss(YourOrderDetils.this, ConstntApi.cancel_order(YourOrderDetils.this, orderID), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    Log.d("deal_product", s);
                    JSONObject jsonObject = new JSONObject(s);

                    if (jsonObject.optString("Status").equalsIgnoreCase("true")) {
                        cancel.setVisibility(View.GONE);
                    }

                    Commonhelper.showToastLong(YourOrderDetils.this, jsonObject.optString("Status"));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    private void address(String orderID) {
        new AbstrctClss(YourOrderDetils.this, ConstntApi.shipping_address(YourOrderDetils.this, orderID), "g", true) {
            @Override
            public void responce(String s) {
                try {
                    Log.d("deal_product", s);
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.optJSONArray("shipping_address");

                    String fname = jsonArray.optJSONObject(0).optString("firstname");
                    String lname = jsonArray.optJSONObject(0).optString("lastname");
                    String address1 = jsonArray.optJSONObject(0).optString("address1");
                    String address2 = jsonArray.optJSONObject(0).optString("address2");
                    String zip = jsonArray.optJSONObject(0).optString("zip");
                    String email = jsonArray.optJSONObject(0).optString("email");
                    String phone = jsonArray.optJSONObject(0).optString("phone");

                    TextView lnameTv = findViewById(R.id.lname);
                    TextView address1Tv = findViewById(R.id.address1);
                    TextView address2Tv = findViewById(R.id.address2);
                    TextView zipTv = findViewById(R.id.zip);
                    TextView emailTv = findViewById(R.id.email);
                    TextView phoneTv = findViewById(R.id.phone);

                    if (!fname.equals("null")) {
                        shippingAddress.setText("" + fname);
                    } else {
                        shippingAddress.setVisibility(View.GONE);
                    }
                    if (!lname.equals("null")) {
                        lnameTv.setText("" + lname);
                    } else {
                        lnameTv.setVisibility(View.GONE);
                    }
                    if (!address1.equals("null")) {
                        address1Tv.setText("" + address1);
                    } else {
                        address1Tv.setVisibility(View.GONE);
                    }
                    if (!address2.equals("null")) {
                        address2Tv.setText("" + address2);
                    } else {
                        address2Tv.setVisibility(View.GONE);
                    }
                    if (!zip.equals("null")) {
                        zipTv.setText("" + zip);
                    } else {
                        zipTv.setVisibility(View.GONE);
                    }
                    if (!email.equals("null")) {
                        emailTv.setText("" + email);
                    } else {
                        emailTv.setVisibility(View.GONE);
                    }
                    if (!phone.equals("null")) {
                        phoneTv.setText("" + phone);
                    } else {
                        phoneTv.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    void order_list(String orderId)
    {
        new AbstrctClss(YourOrderDetils.this, ConstntApi.order_details(YourOrderDetils.this, orderId), "g", true)
        {
            @Override
            public void responce(String s) {
                try {
                    Log.d("deal_product", s);
                    JSONObject jsonObject = new JSONObject(s);

                    ArrayList<JSONObject> jsonObjects = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray();
                    jsonArray = jsonObject.optJSONArray("data");

                    if (jsonArray.length() < 1) {
                        Commonhelper.showToastLong(YourOrderDetils.this, Constnt.notFound);
                        return;
                    }

                    TextView totTv = findViewById(R.id.tot);
                    totTv.setText("Total " + jsonArray.length() + " items");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObjects.add(jsonArray.optJSONObject(i));
                    }

                    recycleView.setAdapter(new MyRecyclerAdapter(jsonObjects));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponsee(String s) {
            }
        };
    }

    public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

        private ArrayList<JSONObject> jsonObjectal;

        public MyRecyclerAdapter(ArrayList<JSONObject> jsonObjectal) {
            this.jsonObjectal = jsonObjectal;
        }

        @Override
        public MyRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item3, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapter.ViewHolder holder, int position) {
            try {
                holder.ipTv.setText("IP:" + jsonObjectal.get(position).optString("ip"));
                holder.prodNmeTv.setText("" + jsonObjectal.get(position).optString("product_name"));
                holder.qtyTv.setText("Quantity:" + jsonObjectal.get(position).optString("qty"));
                holder.priceTv.setText("" + getString(R.string.rs) + jsonObjectal.get(position).optString("price"));
                holder.orderId.setText("Order ID:" + jsonObjectal.get(position).optString("order_id"));
                Picasso.with(YourOrderDetils.this).load(jsonObjectal.get(position).optString("image")).into(holder.img);

                grandTotalTv.setText(getString(R.string.rs) + jsonObjectal.get(position).optString("grand_total"));

                totIp = totIp + Double.parseDouble(jsonObjectal.get(position).optString("ip"));
                totalIPTv.setText("" + Commonhelper.roundOff(totIp));

                holder.orderId.setVisibility(View.GONE);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return jsonObjectal.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView prodNmeTv, totaAmtTv, ipTv, qtyTv, priceTv, orderId;

            ImageView img;

            public ViewHolder(View itemView) {
                super(itemView);

                prodNmeTv = itemView.findViewById(R.id.prodNmeTv);
                ipTv = itemView.findViewById(R.id.ipTv);
                qtyTv = itemView.findViewById(R.id.qtyTv);
                priceTv = itemView.findViewById(R.id.priceTv);
                orderId = itemView.findViewById(R.id.orderId);
                img = itemView.findViewById(R.id.img);
            }
        }
    }

    double totIp;
}

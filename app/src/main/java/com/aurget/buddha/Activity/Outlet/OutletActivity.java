package com.aurget.buddha.Activity.Outlet;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.R;

import org.json.JSONException;
import org.json.JSONObject;

import static com.aurget.buddha.Activity.Commonhelper.openBrowser;

public class OutletActivity extends AppCompatActivity {

    private TextView title, dt, totShipAmt, totIp, saleID, nameTv, qty, subTOtTv, grandTotalTv, ipTv, shippingAmt, walletAmtTt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outlet);

        title = findViewById(R.id.title);
        dt = findViewById(R.id.dt);
        totShipAmt = findViewById(R.id.totShipAmt);
        totIp = findViewById(R.id.totIp);
        saleID = findViewById(R.id.saleID);
        nameTv = findViewById(R.id.nameTv);
        qty = findViewById(R.id.qty);
        subTOtTv = findViewById(R.id.subTOtTv);
        grandTotalTv = findViewById(R.id.grandTotalTv);
        ipTv = findViewById(R.id.ipTv);
        shippingAmt = findViewById(R.id.shippingAmt);
        walletAmtTt = findViewById(R.id.walletAmtTt);

        title.setText(R.string.outlet_shopping);

        try {
            JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("ress"));
            nameTv.setText(jsonObject.optString("product_name"));
            dt.setText(jsonObject.optString("placed_on"));
            saleID.setText(jsonObject.optString("order_id"));
            totIp.setText(jsonObject.optString("ip"));
            totShipAmt.setText("₹"+jsonObject.optString("price"));
            qty.setText("Quantity:"+jsonObject.optString("qtr"));
            subTOtTv.setText("Sub Total: ₹" + jsonObject.optString("price"));
            grandTotalTv.setText("Grand Total: ₹" + jsonObject.optString("price"));
            ipTv.setText("IP:" + jsonObject.optString("ip"));
            shippingAmt.setText("₹" + jsonObject.optString("price"));
            walletAmtTt.setText("₹" + jsonObject.optString("price"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        findViewById(R.id.bkImg).setOnClickListener(view -> finish());
        findViewById(R.id.callTv).setOnClickListener(view -> Commonhelper.phoneCall(OutletActivity.this));
        findViewById(R.id.policy).setOnClickListener(view -> openBrowser(OutletActivity.this,"http://aurget.com/home/page/Outlet_return_policy"));


    }
}

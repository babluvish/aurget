package com.aurget.buddha.Activity.code.Fragment;

import android.content.ContentValues;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.code.Account.AccountActivity;
import com.aurget.buddha.Activity.code.Account.FavouriteListActivity;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Database.DatabaseController;
import com.aurget.buddha.Activity.code.Database.TableFavourite;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.Main.NotificationActivity;
import com.aurget.buddha.Activity.code.Product.CartListActivity;
import com.aurget.buddha.Activity.code.Product.ProductDetailsActivity;
import com.aurget.buddha.Activity.code.Search.SearchActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseFragment;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import code.utils.AppUrls;

public class FeaturedFragment  extends BaseFragment implements View.OnClickListener {

    ArrayList<HashMap<String, String>> FeaturedProductList = new ArrayList();

    RecyclerView recyclerView;
    GridLayoutManager layoutManager;
    ImageView ivMenu,ivSearch,ivCart;

    TypeAdapter typeAdapter;

    TextView tvMain;

    RelativeLayout rrNoData;

    LinearLayout llHome,llCategory,llFavourites,llProfile,llNotification;
    String offset="0";

    TextView tvHome,tvCategory,tvFavourites,tvProfile,tvCount;

    ImageView ivHome,ivCategory,ivFavourites,ivProfile,ivNotification;

    View  view;
    private boolean loading = true;

    int pastVisiblesItems, visibleItemCount, totalItemCount;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.featuredinflate, container, false);


        // initialise();
         if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
           // getFeaturedDataApi();
            } else {
            AppUtils.showErrorMessage(tvMain, getString(R.string.errorInternet), mActivity);
            }

         /* recyclerView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
//                int scrollY = rootScrollView.getScrollY(); // For ScrollView
//                int scrollX = rootScrollView.getScrollX(); // For HorizontalScrollView
//                // DO SOMETHING WITH THE SCROLL COORDINATES
                if (scrollView != null && !paging.equalsIgnoreCase("0")) {
                    if (scrollView.getChildAt(0).getBottom() <= (scrollView.getHeight() + scrollView.getScrollY())) {
                        //scroll view is at bottom
                        simpleProgressBar.setVisibility(View.GONE);
                        progresbar.setVisibility(View.VISIBLE);
                        paging = "0";
                        catalogueList.clear();
                        getCatlogCollection();
                    } else {
                        progresbar.setVisibility(View.GONE);
                    }
                }
            }
        });*/

        return view;
    }

    private void initialise() {
        //TextView
        tvMain =     view.findViewById(R.id.tvHeaderText);
        tvHome =        view.findViewById(R.id.tvHomeBottom);
        tvCategory =    view.findViewById(R.id.tvCategoryBottom);
        tvFavourites =  view.findViewById(R.id.tvFavouritesBottom);
        tvProfile =     view.findViewById(R.id.tvProfileBottom);
        tvCount =       view.findViewById(R.id.tvCount);

        //ImageView
        ivMenu =          view.findViewById(R.id.iv_menu);
        ivSearch =        view.findViewById(R.id.searchmain);
        ivHome =          view.findViewById(R.id.ivHomeBottom);
        ivCategory =      view.findViewById(R.id.ivCategoryBottom);
        ivFavourites =    view.findViewById(R.id.ivFavouritesBottom);
        ivProfile =       view.findViewById(R.id.ic_profile);
        ivCart =          view.findViewById(R.id.ivCart);
        ivNotification =  view.findViewById(R.id.ic_notification);
        //GridView
        recyclerView =   view.findViewById(R.id.recyclerview);
        layoutManager = new GridLayoutManager(mActivity, 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if(dy > 0) //check for scroll down
                {
                    Log.v("ksqbmbq","1");
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        Log.v("ksqbmbq","2");
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            getFeaturedDataApi();
                            Log.v("ksqbmbq","3");
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });
        //RelativeLayout
     //   rrNoData =   view.findViewById(R.id.rrNoData);
        //LinearLayout from bottom views
        llHome =  view. findViewById(R.id.llHome);
        llCategory =  view. findViewById(R.id.llCategory);
        llFavourites =  view. findViewById(R.id.llFavourites);
        llProfile =     view. findViewById(R.id.llProfile);
        llNotification =view. findViewById(R.id.llNotification);

        llHome.setOnClickListener(this);
        llCategory.setOnClickListener(this);
        llFavourites.setOnClickListener(this);
        llProfile.setOnClickListener(this);
        llNotification.setOnClickListener(this);

        tvMain.setText("Categories List");
        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        ivCart.setOnClickListener(this);
        ivMenu.setImageResource(R.drawable.ic_back);
    }

    private void getFeaturedDataApi() {
        Log.v("GetDashboardListApi", AppUrls.FeaturedProduct);
        AppUtils.showRequestDialog(mActivity);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("offset", offset);

            json.put(AppConstants.result, json_data);

            Log.v("featuredApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.FeaturedProduct)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        parseJsondata(response);

                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvCategory, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvCategory, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseJsondata(JSONObject response) {
        Log.d("response ", response.toString());
        AppUtils.hideDialog();
        FeaturedProductList.clear();

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                offset=jsonObject.getString("offset");
                int i;
                JSONArray productArray = jsonObject.getJSONArray("features_product");
                for (i = 0; i < productArray.length(); i++) {
                    JSONObject productArrayJSONObject = productArray.getJSONObject(i);
                    HashMap<String, String> productlist = new HashMap();
                    productlist.put("id", productArrayJSONObject.getString("id"));
                    productlist.put("category_id", productArrayJSONObject.getString("category_master_id"));
                    productlist.put("sub_category_master_id", productArrayJSONObject.getString("sub_category_master_id"));
                    productlist.put("product_name", productArrayJSONObject.getString("product_name"));
                    productlist.put("product_image", productArrayJSONObject.getString("image"));
                    productlist.put("price", productArrayJSONObject.getString("price"));
                    productlist.put("final_price", productArrayJSONObject.getString("final_price"));
                    productlist.put("product_discount_type", productArrayJSONObject.getString("product_discount_type"));
                    productlist.put("product_discount_amount", productArrayJSONObject.getString("product_discount_amount"));
                    FeaturedProductList.add(productlist);
                }
                typeAdapter = new TypeAdapter(FeaturedProductList);
                recyclerView.setAdapter(typeAdapter);

            } else {
                Toast.makeText(mActivity, String.valueOf(jsonObject.getString("res_msg")), Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
    }
  private class TypeAdapter extends RecyclerView.Adapter<TypeAdapter.MyViewHolder>{
      ArrayList<HashMap<String, String>> data = new ArrayList();

      public TypeAdapter(ArrayList<HashMap<String, String>> favList) {
          data = favList;
      }

      @Override
      public TypeAdapter.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
          View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_product, null);
          return new  TypeAdapter.MyViewHolder(view);
      }

      @Override
      public void onBindViewHolder(TypeAdapter.MyViewHolder holder, final int position) {
          holder.tvDiscount.setVisibility(View.VISIBLE);

           /* if (((HashMap) data.get( position )).get( "product_discount_type" ).equals( "1" )) {
                holder.tvDiscount.setText( "RS" + " " + ((HashMap) data.get( position )).get( "product_discount_amount" ) + " /-off" );
            } else if (((HashMap) data.get( position )).get( "product_discount_type" ).equals( "2" )) {
                holder.tvDiscount.setText( "RS" + " " + ((HashMap) data.get( position )).get( "product_discount_amount" ) + "% off" );
            } else {
                // holder.tvDiscount.setText("");
                holder.tvDiscount.setVisibility( View.INVISIBLE );
                holder.tvPrice.setVisibility( View.INVISIBLE );
            }*/

          holder.tvName.setText((CharSequence) ((HashMap) data.get(position)).get("product_name"));
          if (((String) ((HashMap) data.get(position)).get("product_discount_type")).equals("1")) {
              holder.tvDiscount.setText("RS"+" "+ ((String) ((HashMap) data.get(position)).get("product_discount_amount")) + " /-off");
          } else if (((String) ((HashMap) data.get(position)).get("product_discount_type")).equals("2")) {
              holder.tvDiscount.setText(((String) ((HashMap) data.get(position)).get("product_discount_amount")) + "% off");
          } else {
              // holder.tvDiscount.setText("");
              holder.tvDiscount.setVisibility(View.INVISIBLE);
              holder.tvPrice.setVisibility( View.INVISIBLE );
          }
          if (((String) ((HashMap) data.get(position)).get("final_price")).equalsIgnoreCase((String) ((HashMap) data.get(position)).get("price"))) {
              holder.tvFPrice.setText("RS" +" "+ ((String) ((HashMap) data.get(position)).get("final_price")));
              holder.tvDiscount.setText("");
              holder.tvDiscount.setVisibility(View.INVISIBLE);
              holder.tvPrice.setText("");
          } else {
              holder.tvFPrice.setText("RS"+" " + ((String) ((HashMap) data.get(position)).get("final_price")));
              holder.tvPrice.setText("RS"+" "+ ((String) ((HashMap) data.get(position)).get("price")));
              //     holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.ANTI_ALIAS_FLAG);
              holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | 16);
          }
          try {
              if (((HashMap) data.get(position)).get("product_image") != null && !((String) ((HashMap) data.get(position)).get("product_image")).isEmpty()) {
                  Picasso.with(mActivity).load((String) ((HashMap) data.get(position)).get("product_image")).placeholder((int) R.mipmap.ic_launcher).into(holder.ivPic);
              }
          } catch (Resources.NotFoundException e) {
              e.printStackTrace();
          }

          holder.icWishlist.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  if(!AppSettings.getString(AppSettings.userId).isEmpty())
                  {
                      if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                          addFavApi(data.get(position).get("id"));
                      } else {
                          AppUtils.showErrorMessage(tvMain, getString(R.string.errorInternet), mActivity);
                      }
                  }
                  else {
                      AppUtils.showErrorMessage(tvMain, "Kindly login first", mActivity);
                  }

              }
          });

          holder.linearLayout.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  Intent mIntent = new Intent(mActivity, ProductDetailsActivity.class);
                  mIntent.putExtra(AppConstants.productId, data.get(position).get("id"));
                  mIntent.putExtra(AppConstants.from, "3");
                  startActivity(mIntent);

              }
          });

      }
      @Override
      public int getItemCount() {
          return data.size();
      }

      public class MyViewHolder extends RecyclerView.ViewHolder {
          ImageView ivPic, icWishlist;
          LinearLayout linearLayout;
          TextView tvDiscount;
          TextView tvFPrice;
          TextView tvName;
          TextView tvPrice;

          public MyViewHolder( View itemView) {
              super(itemView);
              linearLayout =  itemView.findViewById(R.id.layout_item);
              ivPic =  itemView.findViewById(R.id.ivProduct);
              icWishlist =  itemView.findViewById(R.id.ic_wishlist);
              tvName =  itemView.findViewById(R.id.tvProductName);
              tvFPrice =  itemView.findViewById(R.id.tvFPrice);
              tvPrice =  itemView.findViewById(R.id.tvPrice);
              tvDiscount =  itemView.findViewById(R.id.tvDiscount);

          }
      }
  }


    private void addFavApi(String productId) {

        AppUtils.showRequestDialog(mActivity);

        Log.v("addFavApi", AppUrls.wishList);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json_data.put("product_master_id", productId);

            json.put(AppConstants.result, json_data);

            Log.v("addFavApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.wishList)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        parseFavdata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }
    private void parseFavdata(JSONObject response) {
        Log.d("response ", response.toString());

        DatabaseController.removeTable(TableFavourite.favourite);

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                if(jsonObject.getString("status").equals("1"))
                {
                    AppUtils.showErrorMessage(tvMain, "Product Added to Favourites", mActivity);
                }
                else if(jsonObject.getString("status").equals("2"))
                {
                    AppUtils.showErrorMessage(tvMain, "Product Removed from Favourites", mActivity);
                }

                JSONArray wishArray = jsonObject.getJSONArray("wish_list_product");

                for (int i = 0; i < wishArray.length(); i++) {
                    JSONObject productobject = wishArray.getJSONObject(i);
                    ContentValues mContentValues = new ContentValues();
                    mContentValues.put(TableFavourite.favouriteColumn.productId.toString(), productobject.getString("product_id"));
                    DatabaseController.insertData(mContentValues,TableFavourite.favourite);
                }

            }
            else
            {
                AppUtils.showErrorMessage(tvMain, "Product Removed from Favourites", mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
      //  setAdapter();
    }
   /* public void setAdapter() {
        llOffers.removeAllViews();

        for(int i=0;i<OfferList.size();i++) {

            LayoutInflater inflater2 = null;
            inflater2 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mLinearView = inflater2.inflate(R.layout.inflate_offer, null);

            LinearLayout llSponsor =  mLinearView.findViewById(R.id.llSponsor);
            TextView tvOfferName =  mLinearView.findViewById(R.id.tvOfferName);
            final TextView tvViewAll =  mLinearView.findViewById(R.id.textView5);
            RecyclerView productRecyclerView =  mLinearView.findViewById(R.id.productRecyclerView);
            ImageView ivSponsorPic =  mLinearView.findViewById(R.id.imageView2);
            productRecyclerView.setLayoutManager(new GridLayoutManager(mActivity, 2));
            if (FeaturedBannerList.size() > i) {
                llSponsor.setVisibility(View.VISIBLE);
                try {
                    if (!(((HashMap) FeaturedBannerList.get(i)).get("banner_image") == null || ((String) ((HashMap) FeaturedBannerList.get(i)).get("banner_image")).isEmpty())) {
                        Picasso.with(mActivity).load((String) ((HashMap) FeaturedBannerList.get(i)).get("banner_image")).placeholder((int) R.mipmap.ic_launcher).into(ivSponsorPic);
                    }
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
            } else {
                llSponsor.setVisibility(View.GONE);
            }
            tvViewAll.setTag(((HashMap) OfferList.get(i)).get("sub_category_id"));

            tvOfferName.setText((CharSequence) ((HashMap) OfferList.get(i)).get("offer_name"));

            if (OfferList.size() > 3) {
                tvViewAll.setVisibility(View.VISIBLE);
            } else {
                tvViewAll.setVisibility(View.GONE);
            }

            tvViewAll.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    String id = String.valueOf(tvViewAll.getTag());

                    Intent mIntent = new Intent(mActivity, ProductListActivity.class);
                    mIntent.putExtra(AppConstants.subCategoryId, id);
                    mIntent.putExtra(AppConstants.from, "3");
                    startActivity(mIntent);
                }
            });

               *//* offerAdapter = new OfferAdapter((ArrayList) ProducttList.get(i));
                layoutManager = new GridLayoutManager(mActivity,2);
                productRecyclerView.setLayoutManager(layoutManager);
                productRecyclerView.setAdapter(offerAdapter);
                productRecyclerView.setNestedScrollingEnabled(true);
                llOffers.addView(mLinearView);*//*

        }
    }*/
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.iv_menu:
               //finish
                return;

            case R.id.searchmain:
                startActivity(new Intent(mActivity, SearchActivity.class));

                return;

            case R.id.llHome:

                tvHome.setTextColor(Color.parseColor("#155b72"));
                tvCategory.setTextColor(Color.parseColor("#9d9b9b"));
                tvFavourites.setTextColor(Color.parseColor("#9d9b9b"));
                tvProfile.setTextColor(Color.parseColor("#9d9b9b"));
                ivHome.setImageResource(R.drawable.ic_home_darkgreen);
                ivCategory.setImageResource(R.drawable.ic_categories_grey);
                ivFavourites.setImageResource(R.drawable.ic_heart_grey);
                ivProfile.setImageResource(R.drawable.ic_notifications_grey);

             startActivity(new Intent(mActivity, DashBoardFragment.class));

                return;

            case R.id.llFavourites:

                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(mActivity, LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(mActivity, LoginActivity.class));
                }
                else {

                    tvHome.setTextColor(Color.parseColor("#9d9b9b"));
                    tvCategory.setTextColor(Color.parseColor("#9d9b9b"));
                    tvFavourites.setTextColor(Color.parseColor("#155b72"));
                    tvProfile.setTextColor(Color.parseColor("#9d9b9b"));
                    ivHome.setImageResource(R.drawable.ic_home_grey);
                    ivCategory.setImageResource(R.drawable.ic_categories_grey);
                    ivFavourites.setImageResource(R.drawable.ic_heart_darkgreen);
                    ivProfile.setImageResource(R.drawable.ic_notifications_grey);

                    startActivity(new Intent(mActivity, FavouriteListActivity.class));
                }

                return;

            case R.id.llNotification:
                tvHome.setTextColor(Color.parseColor("#9d9b9b"));
                tvCategory.setTextColor(Color.parseColor("#9d9b9b"));
                tvFavourites.setTextColor(Color.parseColor("#9d9b9b"));
                tvProfile.setTextColor(Color.parseColor("#9d9b9b"));
                //  tvNotification.setTextColor(Color.parseColor("#155b72"));
                ivHome.setImageResource(R.drawable.ic_home_grey);
                ivCategory.setImageResource(R.drawable.ic_categories_grey);
                ivFavourites.setImageResource(R.drawable.ic_heart_grey);
                ivNotification.setImageResource(R.drawable.ic_notifications_green);
                ivProfile.setImageResource(R.drawable.ic_profile_grey);

                startActivity(new Intent(mActivity, NotificationActivity.class));

                break;

            case R.id.llProfile:

                tvHome.setTextColor(Color.parseColor("#9d9b9b"));
                tvCategory.setTextColor(Color.parseColor("#9d9b9b"));
                tvFavourites.setTextColor(Color.parseColor("#9d9b9b"));
                tvProfile.setTextColor(Color.parseColor("#155b72"));
                ivHome.setImageResource(R.drawable.ic_home_grey);
                ivCategory.setImageResource(R.drawable.ic_categories_grey);
                ivFavourites.setImageResource(R.drawable.ic_heart_grey);
                ivProfile.setImageResource(R.drawable.ic_profile_darkgreen);

                startActivity(new Intent(mActivity, AccountActivity.class));

                return;

            case R.id.ivCart:

                if(AppSettings.getString(AppSettings.userId).isEmpty())
                {
                    startActivity(new Intent(mActivity, LoginActivity.class));
                }
                else  if(AppSettings.getString(AppSettings.verified).contains("0"))
                {
                    startActivity(new Intent(mActivity, LoginActivity.class));
                }
                else
                {
                    startActivity(new Intent(mActivity, CartListActivity.class));
                }

                return;

            default:
                return;
        }
    }
}

package com.aurget.buddha.Activity.code.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.aurget.buddha.Activity.code.Product.ProductDetailsActivity;
import com.aurget.buddha.Activity.code.Product.ProductListActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;


public class MyFragment extends Fragment implements View.OnClickListener{

    String Link;

    String banner_on;
    String id;

    public static Fragment newInstance(Context context, int pos, float scale, ArrayList<HashMap<String,String>> imagesGet) {
        Bundle b = new Bundle();




        if(imagesGet.get(pos).containsKey("banner_image"))
        {
           b.putString("featured_banner",imagesGet.get(pos).get("featured_banner"));
           b.putString("banner_image",imagesGet.get(pos).get("banner_image"));
           b.putString("id",imagesGet.get(pos).get("id"));
           b.putString("banner_on",imagesGet.get(pos).get("banner_on"));
           b.putString("banner_id",imagesGet.get(pos).get("banner_id"));
           b.putString("type","1");
        }


        b.putInt("pos", pos);
        b.putFloat("scale", scale);
        return Fragment.instantiate(context, MyFragment.class.getName(), b);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        LinearLayout l = (LinearLayout)
                inflater.inflate(R.layout.mf, container, false);

        int pos = this.getArguments().getInt("pos");

      //  Link=this.getArguments().getString("Link");
        banner_on=this.getArguments().getString("banner_on");
        id=this.getArguments().getString("id");

        ImageView content = (ImageView)l.findViewById(R.id.content);

      //  Log.v("imagesGet",this.getArguments().getString("image"));

        if (this.getArguments().getString("banner_image").equalsIgnoreCase("")) {
            Picasso.with(getContext()).load((int) R.mipmap.ic_launcher).resize(815,315).into(content);
          //  progressBar.setVisibility(View.VISIBLE);
        } else {
            Picasso.with(getContext()).load(this.getArguments().getString("banner_image")).resize(815,315).into(content);
        }

       /* Picasso.with(getContext())
                .load(this.getArguments().getString("image"))
                .error(R.drawable.arrow)      // optional
                // optional
                .into(content);*/

        content.setOnClickListener(this);

        MyLinearLayout root = (MyLinearLayout) l.findViewById(R.id.root);
        float scale = this.getArguments().getFloat("scale");
        root.setScaleBoth(scale);

        return l;
    }

    @Override
    public void onClick(View v) {

        if(banner_on.equals("1"))
        {
            Intent mIntent = new Intent(getContext(), ProductListActivity.class);
            mIntent.putExtra(AppConstants.subCategoryId, id);
            mIntent.putExtra(AppConstants.from, "1");
            startActivity(mIntent);
        }
        else  if(banner_on.equals("2"))
        {
            Intent mIntent = new Intent(getContext(), ProductDetailsActivity.class);
            mIntent.putExtra(AppConstants.productId, id);
            mIntent.putExtra(AppConstants.from, "1");
            startActivity(mIntent);
        }

    }





}

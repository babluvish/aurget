package com.aurget.buddha.Activity.code.Adapter;


import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.View;

import com.aurget.buddha.Activity.code.Dashboard.DashboardActivity;
import com.aurget.buddha.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.aurget.buddha.Activity.code.Adapter.MyFragment.newInstance;


public class MyPagerAdapter extends FragmentPagerAdapter implements ViewPager.PageTransformer {
    public final static float BIG_SCALE = 1.0f;
    public final static float SMALL_SCALE = 0.9f;
    public final static float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;

    private MyLinearLayout cur = null;
    private MyLinearLayout next = null;
    private Context context;
    private FragmentManager fm;
    private float scale;

    int FIRST_PAGE;
    int PAGES;
    int LOOPS=1000;


    ArrayList<HashMap<String,String>> imagesGet;

    public MyPagerAdapter(Context context,FragmentManager fm, ArrayList<HashMap<String, String>> imagesGet) {
        super(fm);
        this.fm = fm;
        this.context = context;
        this.FIRST_PAGE = FIRST_PAGE;
        this.PAGES = PAGES;
        this.imagesGet=imagesGet;
    }



    @Override
    public Fragment getItem(int position) {
        // make the first pager bigger than others
        if (position == DashboardActivity.VB_FIRST_PAGE)
            scale = BIG_SCALE;
        else
            scale = SMALL_SCALE;


        position = position % DashboardActivity.VB_PAGES;


        return newInstance(context, position, scale,imagesGet);
    }

    @Override
    public int getCount() {

       // return PAGES * LOOPS;
        return DashboardActivity.VB_PAGES;
    }

    @Override
    public void transformPage(View page, float position) {
        MyLinearLayout myLinearLayout = (MyLinearLayout) page.findViewById(R.id.root);
        float scale = BIG_SCALE;
        if (position > 0) {
            scale = scale - position * DIFF_SCALE;
        } else {
            scale = scale + position * DIFF_SCALE;
        }
        if (scale < 0) scale = 0;
        myLinearLayout.setScaleBoth(scale);
    }


    @Override
    public float getPageWidth(int position) {
        return .85f;
    }

}

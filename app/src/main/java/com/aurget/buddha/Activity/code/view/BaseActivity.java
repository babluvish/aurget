package com.aurget.buddha.Activity.code.view;

import android.app.Activity;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;

import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.OSettings;

public class BaseActivity extends AppCompatActivity {

    protected static Activity mActivity;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.mActivity = this;
        Log.i("mActivity",mActivity.getClass().getSimpleName());
        OSettings oSettings = new OSettings(this.mActivity);
        SimpleHTTPConnection simpleHTTPConnection = new SimpleHTTPConnection(this.mActivity);
    }
}

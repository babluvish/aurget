package com.aurget.buddha.Activity.Frgmnt;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.aurget.buddha.Activity.Commonhelper;
import com.aurget.buddha.Activity.Utils.AbstrctClss;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.Utils.ConstntApi;
import com.aurget.buddha.Activity.Utils.SearchDate;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class SolderDetilsFragment extends Fragment {

    EditText firstNameEt, lastNameEt, dateofBirth, genderEt, mobile_noEt;
    LinearLayout editLl;
    RadioButton male, female, custome;

    public SolderDetilsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View views = inflater.inflate(R.layout.fragment_solder_detils, container, false);

        firstNameEt = views.findViewById(R.id.firstNameEt);
        lastNameEt = views.findViewById(R.id.lastNameEt);
        dateofBirth = views.findViewById(R.id.dateofBirth);
        genderEt = views.findViewById(R.id.genderEt);
        mobile_noEt = views.findViewById(R.id.mobile_noEt);
        editLl = views.findViewById(R.id.editLl);

        male = views.findViewById(R.id.male);
        female = views.findViewById(R.id.female);
        custome = views.findViewById(R.id.custome);

        solder_details();

        male.setOnClickListener(view -> {
            gender = "male";
        });

        female.setOnClickListener(view -> {
            gender = "female";
        });

        custome.setOnClickListener(view -> {
            gender = "other";
        });

        dateofBirth.setOnFocusChangeListener((view1, b) -> {
            if (b) {
                new SearchDate(getActivity()) {
                    @Override
                    public String fromDtToDt(String frmDate, String toDt) {
                        return null;
                    }
                }.piecker(dateofBirth);
            }
        });

        dateofBirth.setOnClickListener(view12 -> new SearchDate(getActivity()) {
            @Override
            public String fromDtToDt(String frmDate, String toDt) {
                return null;
            }
        }.piecker(dateofBirth));

        editLl.setOnClickListener(view13 -> update_solder_details());

        firstNameEt.setEnabled(true);
        lastNameEt.setEnabled(true);
        dateofBirth.setEnabled(true);
        genderEt.setEnabled(true);

        views.findViewById(R.id.editImg).setOnClickListener(view14 -> {
            if (firstNameEt.isEnabled()) {
                firstNameEt.setEnabled(false);
                lastNameEt.setEnabled(false);
                dateofBirth.setEnabled(false);
                genderEt.setEnabled(false);
            } else {
                firstNameEt.setEnabled(true);
                lastNameEt.setEnabled(true);
                dateofBirth.setEnabled(true);
                genderEt.setEnabled(true);
                firstNameEt.requestFocus(firstNameEt.getText().length() + 1);
            }
        });

        return views;
    }

    private void update_solder_details() {
        JSONObject jsonObject = new JSONObject();
        Commonhelper.sop("gender" + gender);

        try {
            jsonObject.put("2", firstNameEt.getText().toString());
            jsonObject.put("3", lastNameEt.getText().toString());
            jsonObject.put("4", "_");
            jsonObject.put("5", gender);
            jsonObject.put("6", dateofBirth.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new AbstrctClss(getActivity(), ConstntApi.update_solder_details(getActivity(), jsonObject), "g", true) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s);
                    if (jsonArray.length() < 1) {
                        Commonhelper.showToastLong(getContext(), Constnt.notFound);
                        return;
                    }
                    else if (jsonArray.optJSONObject(0).optString("Status").equalsIgnoreCase("true"))
                    {
                        Commonhelper.showToastLong(getContext(), jsonArray.optJSONObject(0).optString("message"));
                    }
                    else {
                        Commonhelper.showToastLong(getContext(), jsonArray.optJSONObject(0).optString("message"));
                    }

                } catch (Exception e) {

                }
            }

            @Override
            public void onErrorResponsee(String s) {
                Commonhelper.showToastLong(getContext(), Constnt.notFound);
            }
        };

    }

    void solder_details() {
        new AbstrctClss(getActivity(), ConstntApi.solder_details(getActivity()), "g", true) {
            @Override
            public void responce(String s) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s);

                    if (jsonArray.length() < 1) {
                        Commonhelper.showToastLong(getContext(), Constnt.notFound);
                        return;
                    }
                    try {
                        firstNameEt.setText(jsonArray.optJSONObject(0).optString("first_name"));
                    } catch (Exception e) {
                    }
                    try {
                        lastNameEt.setText(jsonArray.optJSONObject(0).optString("last_name"));
                    } catch (Exception e) {
                    }
                    try {
                        mobile_noEt.setText(jsonArray.optJSONObject(0).optString("mobile"));
                    } catch (Exception e) {
                    }
                    try {
                        dateofBirth.setText(jsonArray.optJSONObject(0).optString("dob"));
                    } catch (Exception e) {
                    }
                    try {
                        String gender2 = jsonArray.optJSONObject(0).optString("gender");
                        if (gender2.equalsIgnoreCase("male")) {
                            male.setChecked(true);
                        } else if (gender2.equalsIgnoreCase("female")) {
                            female.setChecked(true);
                        } else {
                            custome.setChecked(true);
                        }
                    } catch (Exception e) {
                    }

                } catch (Exception e) {

                }
            }

            @Override
            public void onErrorResponsee(String s) {
                Commonhelper.showToastLong(getContext(), Constnt.notFound);
            }
        };
    }

    String gender;
}

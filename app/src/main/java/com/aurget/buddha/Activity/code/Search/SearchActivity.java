package com.aurget.buddha.Activity.code.Search;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.os.Vibrator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Database.DatabaseController;
import com.aurget.buddha.Activity.code.Database.TableFavourite;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.Product.ProductDetailsActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import code.Adapter.SortAdapter;
import code.utils.AppUrls;


public class SearchActivity extends BaseActivity implements OnClickListener,SearchView.OnQueryTextListener {
    ImageView icBack;

    ProductAdapter productAdapter;
    ArrayList<HashMap<String, String>> productList = new ArrayList();
    ArrayList<HashMap<String, String>> sortsList = new ArrayList();

    ArrayList<HashMap<String, String>> datalist = new ArrayList();
    ArrayList<HashMap<String, String>> hashMapArrayList = new ArrayList<>();

    RecyclerView recyclerView;
    RelativeLayout rrNoData;
    ImageView ivMenu,ivSearch;

    Typeface typeface;
    String offset="0",sortId="";
    GridLayoutManager mGridLayoutManager;

    TextView tvSort,tvFilter;

    SortAdapter sortAdapter;

    boolean loadMore=true;

    EditText edSearch;


    SearchView editsearch;


    int pastVisiblesItems, visibleItemCount, totalItemCount;
    NewProductAdapter newProductAdapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_search);
        initialise();
    }

    private void initialise() {
        //  getWindow().setSoftInputMode(2);
        this.icBack =  findViewById(R.id.iv_menu);
        this.icBack.setOnClickListener(this);
        rrNoData =  findViewById(R.id.rrNoData);

        //ImageView
        ivMenu =  findViewById(R.id.iv_menu);
        ivSearch = findViewById(R.id.searchmain);

        //EditText
        edSearch =  findViewById(R.id.ed_Main);

        //Recycleview
        recyclerView =  findViewById(R.id.productRecyclerView);

//        typeface = Typeface.createFromAsset(mActivity.getAssets(), "centurygothic.otf");

        //TextView
        tvSort =  findViewById(R.id.textView7);
        tvFilter =  findViewById(R.id.textView8);

        ivMenu.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        tvSort.setOnClickListener(this);
        tvFilter.setOnClickListener(this);
        ivMenu.setImageResource(R.drawable.ic_back);

        editsearch =  findViewById(R.id.search);
        editsearch.setOnQueryTextListener(this);

        int searchSrcTextId = getResources().getIdentifier("android:id/search_src_text", null, null);
        EditText searchEditText = editsearch.findViewById(searchSrcTextId);
        searchEditText.setTextColor(Color.BLACK); // set the text color
        searchEditText.setHintTextColor(Color.BLACK); // set the hint color



        mGridLayoutManager = new GridLayoutManager(mActivity, 2);
        recyclerView.setLayoutManager(mGridLayoutManager);

        productList.clear();
        sortsList.clear();

        newProductAdapter = new NewProductAdapter(productList, datalist);
        recyclerView.setAdapter(newProductAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);



        recyclerView.addOnScrollListener( new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = mGridLayoutManager.getChildCount();
                    totalItemCount = mGridLayoutManager.getItemCount();
                    pastVisiblesItems = mGridLayoutManager.findFirstVisibleItemPosition();

                    if (!loadMore) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loadMore = true;
                            //page = page+1;
                            getNewProductApi();
                        }
                    }
                }
            }
        } );


     //   mGridLayoutManager = new GridLayoutManager(mActivity, 2);
     //   recyclerView.setLayoutManager(mGridLayoutManager);

/*
        recyclerView.addOnScrollListener(new EndScrollListener(mGridLayoutManager) {
            @Override
            public void onScrolledToEnd() {
                Log.e("Position", "Last item reached");
                if (loadMore) {
                    callNewPage();
                }
            }
        });
*/
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    //callNewPage
    protected void callNewPage() {

        AppUtils.hideSoftKeyboard(mActivity);

        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getProductListApi();
        } else {
            AppUtils.showErrorMessage(rrNoData, getString(R.string.errorInternet), mActivity);
        }
    }

    private void getProductListApi() {

        AppUtils.showRequestDialog(mActivity);
        Log.v("getProductListApi", AppUrls.getComman);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                json_data.put("user_id", "0");
            } else {
                json_data.put("user_id", AppSettings.getString(AppSettings.userId));
            }

            json_data.put("sub_category_id", "");
            json_data.put("home_product_type", "");
            json_data.put("search_key", editsearch.getQuery());
            json_data.put("category_id", "");
            json_data.put("product_id", "");
            json_data.put("offset", offset);
            json.put(AppConstants.result, json_data);
            Log.v("getProductListApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.getComman)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(rrNoData, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(rrNoData, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseJsondata(JSONObject response) {

        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                offset = jsonObject.getString("offset");

                JSONArray productArray = jsonObject.getJSONArray("products");

                if(productArray.length()>8)
                {
                    loadMore=true;
                }
                else
                {
                    loadMore=false;
                }

                for (int i = 0; i < productArray.length(); i++) {
                    JSONObject productobject = productArray.getJSONObject(i);
                    HashMap<String, String> prodList = new HashMap();
                    prodList.put("product_name", productobject.getString("product_name"));
                    prodList.put("final_price", productobject.getString("final_price"));
                    prodList.put("id", productobject.getString("id"));
                    prodList.put("price", productobject.getString("price"));
                    prodList.put("product_discount_amount", productobject.getString("product_discount_amount"));
                    prodList.put("product_discount_type", productobject.getString("product_discount_type"));
                    prodList.put("product_image", productobject.getString("product_image"));
                    productList.add(prodList);
                }
                if (productList.size() <= 0) {
                    rrNoData.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                } else {
                    rrNoData.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);

                    if(productList.size()<=10)
                    {
                        productAdapter = new ProductAdapter(productList);
                        recyclerView.setAdapter(productAdapter);
                        recyclerView.setNestedScrollingEnabled(true);
                    }
                    else
                    {
                        productAdapter.notifyDataSetChanged();
                    }

                }

                JSONArray sortArray = jsonObject.getJSONArray("sorts");

                for (int i = 0; i < sortArray.length(); i++) {
                    JSONObject sortobject = sortArray.getJSONObject(i);
                    HashMap<String, String> sortList = new HashMap();

                    sortList.put("id",sortobject.getString("id"));
                    sortList.put("sort_name",sortobject.getString("sort_name"));
                    sortList.put("status","0");

                    sortsList.add(sortList);
                }
            }
            else
            {
                loadMore=false;
                AppUtils.showErrorMessage(rrNoData, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(rrNoData, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
    }

    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_menu:
                finish();
                return;

            case R.id.searchmain:

                AppUtils.hideSoftKeyboard(mActivity);


                if(editsearch.getQuery().length()==0)
                {
                    AppUtils.showErrorMessage(rrNoData, getString(R.string.errorsearch), mActivity);
                }

                else
                {
                    productList.clear();
                    sortsList.clear();
                    if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                        getProductListApi();
                    } else {
                        AppUtils.showErrorMessage(rrNoData, getString(R.string.errorInternet), mActivity);
                    }
                }

                return;

            case R.id.textView7:

                if(sortsList.size()>0)
                {
                    AlertPopUp();
                }
                else
                {
                    AppUtils.showErrorMessage(rrNoData, getString(R.string.errorsearch), mActivity);
                }
                return;

            case R.id.textView8:
                //AlertPopUp();
                return;

            default:
                return;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        productList.clear();
        sortsList.clear();
        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            offset="0";
            productList.clear();
            sortsList.clear();
            getNewProductApi();
            //getProductListApi();
        } else {
            AppUtils.showErrorMessage(rrNoData, getString(R.string.errorInternet), mActivity);
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        productList.clear();
        sortsList.clear();

        if (!TextUtils.isEmpty(s) && s.length() >= 3) {
            ivSearch.setVisibility(View.GONE);
            if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                offset="0";
                productList.clear();
                sortsList.clear();
                getNewProductApi();
               // getProductListApi();
            } else {
                AppUtils.showErrorMessage(rrNoData, getString(R.string.errorInternet), mActivity);
            }
            return false;
        }
        else {
            return true;
        }


    }


    private class ProductAdapter extends RecyclerView.Adapter<NameHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();

        public ProductAdapter(ArrayList<HashMap<String, String>> favList) {
            data = favList;
        }

        public NameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new NameHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_product, parent, false));
        }

        public void onBindViewHolder(NameHolder holder, final int position) {

            /*holder.tvName.setTypeface(typeface);
            holder.tvDiscount.setTypeface(typeface);
            holder.tvPrice.setTypeface(typeface);
            holder.tvFPrice.setTypeface(typeface);*/
            /* if (((HashMap) data.get( position )).get( "product_discount_type" ).equals( "1" )) {
                holder.tvDiscount.setText( "RS" + " " + ((HashMap) data.get( position )).get( "product_discount_amount" ) + " /-off" );
            } else if (((HashMap) data.get( position )).get( "product_discount_type" ).equals( "2" )) {
                holder.tvDiscount.setText( "RS" + " " + ((HashMap) data.get( position )).get( "product_discount_amount" ) + "% off" );
            } else {
                // holder.tvDiscount.setText("");
                holder.tvDiscount.setVisibility( View.INVISIBLE );
                holder.tvPrice.setVisibility( View.INVISIBLE );
            }*/


            holder.tvName.setText((CharSequence) ((HashMap) data.get(position)).get("product_name"));
            if (((String) ((HashMap) data.get(position)).get("product_discount_type")).equals("1")) {
                holder.tvDiscount.setText("Flat RS" + ((String) ((HashMap) data.get(position)).get("product_discount_amount")) + "/- off");
            } else if (((String) ((HashMap) data.get(position)).get("product_discount_type")).equals("2")) {
                holder.tvDiscount.setText(((String) ((HashMap) data.get(position)).get("product_discount_amount")) + "% off");
            } else {
                holder.tvDiscount.setText("");
                holder.tvDiscount.setVisibility(View.INVISIBLE);
                holder.tvPrice.setVisibility( View.INVISIBLE );
            }
            if (((String) ((HashMap) data.get(position)).get("final_price")).equalsIgnoreCase((String) ((HashMap) data.get(position)).get("price"))) {
                holder.tvFPrice.setText("RS" + ((String) ((HashMap) data.get(position)).get("final_price")));
                holder.tvDiscount.setText("");
                holder.tvPrice.setText("");
            } else {
                holder.tvFPrice.setText("RS" + ((String) ((HashMap) data.get(position)).get("final_price")));
                holder.tvPrice.setText("RS" + ((String) ((HashMap) data.get(position)).get("price")));
                holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | 16);
            }
            try {
                if (((HashMap) data.get(position)).get("product_image") != null && !((String) ((HashMap) data.get(position)).get("product_image")).isEmpty()) {
                    Picasso.with(mActivity).load((String) ((HashMap) data.get(position)).get("product_image")).placeholder((int) R.mipmap.ic_launcher).into(holder.ivPic);
                }
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }

            if(DatabaseController.checkRecordExist(TableFavourite.favourite, String.valueOf(TableFavourite.favouriteColumn.productId),data.get(position).get("id")))
            {
                holder.icWishlist.setImageResource(R.drawable.ic_heart_red);
            }
            else
            {
                holder.icWishlist.setImageResource(R.drawable.ic_heart_grey);
            }

            holder.icWishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(!AppSettings.getString(AppSettings.userId).isEmpty())
                    {
                        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                            addFavApi(data.get(position).get("id"));
                        } else {
                            AppUtils.showErrorMessage(tvFilter, getString(R.string.errorInternet), mActivity);
                        }
                    }
                    else {
                        AppUtils.showErrorMessage(tvFilter, "Kindly login first", mActivity);
                    }

                }
            });

            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent mIntent = new Intent(mActivity, ProductDetailsActivity.class);
                    mIntent.putExtra(AppConstants.productId, data.get(position).get("id"));
                    mIntent.putExtra(AppConstants.from, "1");
                    startActivity(mIntent);

                }
            });


        }

        public int getItemCount() {
            return data.size();
        }
    }

    private class NameHolder extends RecyclerView.ViewHolder {
        ImageView ivPic, icWishlist;
        LinearLayout linearLayout;
        TextView tvDiscount;
        TextView tvFPrice;
        TextView tvName;
        TextView tvPrice;

        public NameHolder(View itemView) {
            super(itemView);
            linearLayout =  itemView.findViewById(R.id.layout_item);
            ivPic =  itemView.findViewById(R.id.ivProduct);
            icWishlist =  itemView.findViewById(R.id.ic_wishlist);
            tvName =  itemView.findViewById(R.id.tvProductName);
            tvFPrice =  itemView.findViewById(R.id.tvFPrice);
            tvPrice =  itemView.findViewById(R.id.tvPrice);
            tvDiscount =  itemView.findViewById(R.id.tvDiscount);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            productAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addFavApi(String productId) {

        AppUtils.showRequestDialog(mActivity);

        Log.v("addFavApi", AppUrls.wishList);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json_data.put("product_master_id", productId);

            json.put(AppConstants.result, json_data);

            Log.v("addFavApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.wishList)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseFavdata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvFilter, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvFilter, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseFavdata(JSONObject response) {

        Log.d("response ", response.toString());

        DatabaseController.removeTable(TableFavourite.favourite);

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                if(jsonObject.getString("status").equals("1"))
                {
                    AppUtils.showErrorMessage(tvFilter, "Product Added to Wishlist", mActivity);
                }
                else if(jsonObject.getString("status").equals("2"))
                {
                    AppUtils.showErrorMessage(tvFilter, "Product Removed from Wishlist", mActivity);
                }

                JSONArray wishArray = jsonObject.getJSONArray("wish_list_product");

                for (int i = 0; i < wishArray.length(); i++) {

                    JSONObject productobject = wishArray.getJSONObject(i);

                    ContentValues mContentValues = new ContentValues();

                    mContentValues.put(TableFavourite.favouriteColumn.productId.toString(), productobject.getString("product_id"));

                    DatabaseController.insertData(mContentValues,TableFavourite.favourite);
                }

            }
            else
            {
                AppUtils.showErrorMessage(tvFilter, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvFilter, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();

        productAdapter.notifyDataSetChanged();
    }

    //AlertPopUp
    private void AlertPopUp() {

        final Dialog dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alertsort);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();

        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        //TextView
        TextView tvYes                             = dialog.findViewById(R.id.tvOk);
        TextView tvCancel                      = dialog.findViewById(R.id.tvcancel);
        TextView tvAlertMsg                   = dialog.findViewById(R.id.tvAlertMsg);
        ListView listView                          = dialog.findViewById(R.id.listView);

        dialog.show();

        tvYes.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                dialog.dismiss();

                productList.clear();
                sortsList.clear();
                offset="0";
                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    getProductListApi();
                } else {
                    AppUtils.showErrorMessage(rrNoData, getString(R.string.errorInternet), mActivity);
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        sortAdapter = new SortAdapter(mActivity, sortsList) {
            protected void catClick(View v, String position) {

                for(int i=0;i<sortsList.size();i++)
                {
                    HashMap<String, String> sortList = new HashMap();
                    sortList.put("id",sortsList.get(i).get("id"));
                    sortList.put("sort_name",sortsList.get(i).get("sort_name"));
                    sortList.put("status","0");

                    sortsList.set(i,sortList);
                }

                sortId = sortsList.get(Integer.parseInt(position)).get("id");

                HashMap<String, String> sortList = new HashMap();
                sortList.put("id",sortsList.get(Integer.parseInt(position)).get("id"));
                sortList.put("sort_name",sortsList.get(Integer.parseInt(position)).get("sort_name"));
                sortList.put("status","1");

                sortsList.set(Integer.parseInt(position),sortList);

                sortAdapter.notifyDataSetChanged();
            }
        };
        listView.setAdapter(sortAdapter);
    }






    private void getNewProductApi(){
        AppUtils.showRequestDialog(mActivity);
        Log.v("getProductListApi", AppUrls.getComman);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                json_data.put("user_id", "0");
            } else {
                json_data.put("user_id", AppSettings.getString(AppSettings.userId));
            }

            json_data.put("sub_category_id", "");
            json_data.put("home_product_type", "");
            json_data.put("search_key", editsearch.getQuery());
            json_data.put("category_id", "");
            json_data.put("product_id", "");
            json_data.put("offset", offset);
            json.put(AppConstants.result, json_data);
            Log.v("getProductListApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.getComman)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseNewJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(rrNoData, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(rrNoData, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseNewJsondata(JSONObject response) {
        AppUtils.hideDialog();
        Log.d("getProductListApi", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                offset = jsonObject.getString("offset");
                JSONArray productArray = jsonObject.getJSONArray("product_master");


                for (int i = 0; i < productArray.length(); i++) {

                    JSONObject productobject = productArray.getJSONObject(i);

                    HashMap<String, String> prodList = new HashMap();

                    prodList.put("id", productobject.getString("id"));
                    prodList.put("category_master_id", productobject.getString("category_master_id"));
                    prodList.put("sub_category_master_id", productobject.getString("sub_category_master_id"));
                    prodList.put("product_name", productobject.getString("product_name"));
                    prodList.put("image", productobject.getString("image"));
                    prodList.put("price", productobject.getString("price"));
                    prodList.put("final_price", productobject.getString("final_price"));
                    prodList.put("product_discount_type", productobject.getString("product_discount_type"));
                    prodList.put("product_discount_amount", productobject.getString("product_discount_amount"));
                    prodList.put("quantity", productobject.getString("quantity"));
                    prodList.put("weight", productobject.getString("weight"));
                    prodList.put("unit_name", productobject.getString("unit_name"));

                    prodList.put("sub_product", productobject.getString("sub_product"));


                    if (productobject.has("sub_product")) {
                        JSONArray newsliderarray = productobject.getJSONArray("sub_product");
                        for (int j = 0; j < newsliderarray.length(); j++) {
                            JSONObject sliderObject = newsliderarray.getJSONObject(j);
                            HashMap<String, String> sliderMap = new HashMap<>();
                            if (j == 0) {
                                productobject.put("SpinerValue", sliderObject.get("sub_weight").toString() + sliderObject.get("unit_name").toString());
                            }
                            sliderMap.put("sub_id", sliderObject.get("sub_id").toString());
                            sliderMap.put("dis_type", sliderObject.get("dis_type").toString());
                            sliderMap.put("per_amount", sliderObject.get("per_amount").toString());
                            sliderMap.put("sub_weight", sliderObject.get("sub_weight").toString());
                            sliderMap.put("unit_name", sliderObject.get("unit_name").toString());
                            sliderMap.put("actual_p", sliderObject.get("actual_p").toString());
                            sliderMap.put("price", sliderObject.get("price").toString());
                            sliderMap.put("parentId", productobject.getString("id"));
                            sliderMap.put("image", productobject.getString("image"));
                            sliderMap.put("cart_quantity", sliderObject.getString("cart_quantity"));
                            sliderMap.put("cartId", sliderObject.getString("cartId"));
                            datalist.add(sliderMap);
                            Log.v("dshf", sliderObject.get("dis_type").toString());
                        }
                    }
                    productList.add(prodList);
                }
                if (productList.size() <= 0) {
                    rrNoData.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                } else {
                    rrNoData.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
                newProductAdapter.notifyDataSetChanged();
                loadMore = false;

            } else {
                loadMore = false;
                AppUtils.showErrorMessage(rrNoData, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
             AppUtils.showErrorMessage(rrNoData, String.valueOf(e), mActivity);
        }

    }

    private class NewProductAdapter extends RecyclerView.Adapter<FavNameHolder> {
        ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();
        ArrayList<HashMap<String, String>> data2 = new ArrayList<HashMap<String, String>>();
        int count=1;

        public NewProductAdapter(ArrayList<HashMap<String, String>> favList, ArrayList<HashMap<String, String>> subProductListArrayList) {
            data = favList;
            data2 = subProductListArrayList;
        }

        public FavNameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.inflate_product, parent, false );
            FavNameHolder holder = new FavNameHolder( view );
            holder.setIsRecyclable( false );
            return holder;
        }

        public void onBindViewHolder(final FavNameHolder holder, final int position) {

            if (!data2.get(position).get("cart_quantity").equalsIgnoreCase("")) {
                Log.d("Count", "onBindViewHolder: "+data2.get(position).get("cart_quantity"));
                count = Integer.parseInt(data2.get(position).get("cart_quantity"));
                holder.llCartCountBtn.setVisibility(View.VISIBLE);
                holder.llCartBtn.setVisibility(View.GONE);
                holder.tvQuantityCount.setText(count+"");
            } else {
                count = 1;
                holder.llCartCountBtn.setVisibility(View.GONE);
                holder.llCartBtn.setVisibility(View.VISIBLE);
            }


            holder.tvName.setText((CharSequence) ((HashMap) data.get(position)).get("product_name"));
            if (((HashMap) data.get(position)).get("product_discount_type").equals("1")) {
                holder.tvPrice.setVisibility(View.VISIBLE);
                holder.tvDiscount.setText("RS" + " " + ((HashMap) data.get(position)).get("product_discount_amount") + "  /-off");
            } else if (((HashMap) data.get(position)).get("product_discount_type").equals("2")) {
                holder.tvPrice.setVisibility(View.VISIBLE);
                holder.tvDiscount.setText("RS" + " " + ((HashMap) data.get(position)).get("product_discount_amount") + "% off");
            } else if (data.get(position).get("product_discount_type").equals("123")) {
                // holder.tvDiscount.setText("");
                holder.tvDiscount.setVisibility(View.INVISIBLE);
                holder.tvPrice.setVisibility(View.GONE);
            }
            if (((String) ((HashMap) data.get(position)).get("final_price")).equalsIgnoreCase((String) ((HashMap) data.get(position)).get("price"))) {
                holder.tvFPrice.setText("RS" + " " + ((HashMap) data.get(position)).get("final_price"));
                holder.tvDiscount.setText("");
                holder.tvDiscount.setVisibility(View.INVISIBLE);
                holder.tvPrice.setText("");
            } else {
                holder.tvFPrice.setText("RS" + " " + ((HashMap) data.get(position)).get("final_price"));
                holder.tvPrice.setText("RS" + " " + ((HashMap) data.get(position)).get("price"));
                //  holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.ANTI_ALIAS_FLAG);
                holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | 16);
            }
            try {
                if (((HashMap) data.get(position)).get("image") != null && !((String) ((HashMap) data.get(position)).get("image")).isEmpty()) {
                    Picasso.with(mActivity).load((String) ((HashMap) data.get(position)).get("image")).into(holder.ivPic);
                }
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }


            holder.icWishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!AppSettings.getString(AppSettings.userId).isEmpty()) {
                        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                            addFavApi(data.get(position).get("id"));
                        } else {
                            AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                        }
                    } else {
                        AppUtils.showErrorMessage(recyclerView, "Kindly login first", mActivity);
                    }
                }
            });

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent mIntent = new Intent(mActivity, ProductDetailsActivity.class);
                    mIntent.putExtra(AppConstants.productId, data.get(position).get("id"));
                    mIntent.putExtra(AppConstants.from, "3");
                    mIntent.putExtra(AppConstants.CartQuantityValue, "6");
                    startActivity(mIntent);
                }
            });


            holder.llCartBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                        Toast.makeText(mActivity, "Please Login to continue", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(mActivity, LoginActivity.class));
                    } else {
                        holder.llCartCountBtn.setVisibility(View.VISIBLE);
                        holder.llCartBtn.setVisibility(View.GONE);
                        AddToCartApi(data.get(position).get("id"));
                        datalist.get(position).put("cart_quantity","1");
                    }
                }
            });


            holder.tvSpinner.setText(data.get(position).get("weight")+data.get(position).get("unit_name"));

            holder.tvSpinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UpdateSize(data.get(position).get("weight"), data.get(position).get("quantity"), view, data.get(position).get("id"), holder.tvSpinner, holder.tvPrice, holder.tvFPrice, holder.tvDiscount, holder.llCartCountBtn, holder.llCartBtn);
                    if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                        //  startActivity( new Intent( mActivity, LoginActivity.class ) );
                    } else {
                        count = (Integer.parseInt(holder.tvQuantityCount.getText().toString()));
                        holder.tvQuantityCount.setText(String.valueOf(count));
                        Log.v("CountValue", String.valueOf(count));
                       /* holder.llCartCountBtn.setVisibility( View.GONE );
                        holder. llCartBtn.   setVisibility( View.VISIBLE );*/
                    }
                }
            });

            holder.tvQuantityCount.setText(String.valueOf(count));

            holder.tvSpinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UpdateSize(data.get(position).get("weight"), data.get(position).get("quantity"), view, data.get(position).get("id"), holder.tvSpinner, holder.tvPrice, holder.tvFPrice, holder.tvDiscount, holder.llCartCountBtn, holder.llCartBtn);
                    if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                    } else {
                        count = (Integer.parseInt(holder.tvQuantityCount.getText().toString()));
                        holder.tvQuantityCount.setText(String.valueOf(count));
                        Log.v("CountValue", String.valueOf(count));

                    }
                }
            });

            holder.tvQuantityCount.setText(String.valueOf(count));

            AppSettings.putString(AppSettings.subiid, data2.get(position).get("sub_id"));
            holder.imageView3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    count = (Integer.parseInt(holder.tvQuantityCount.getText().toString())) - 1;
                    if (count == 0) {
                        Vibrator vibe = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
                        vibe.vibrate(100);
                        holder.llCartCountBtn.setVisibility(View.GONE);
                        holder.llCartBtn.setVisibility(View.VISIBLE);
                        String dd = "714";
                        DeleteCartApi(data2.get(position).get("cartId"));
                    } else if (count > 0) {

                        Vibrator vibe = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
                        vibe.vibrate(100);
                        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                            UpdateCartApi(String.valueOf(count), data2.get(position).get("sub_id"));
                            holder.tvQuantityCount.setText((count) + "");
                            datalist.get(position).put("cart_quantity",count+"");
                        } else {
                            AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                        }
                    }

                }


            });
            holder.imageView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    count = (Integer.parseInt(holder.tvQuantityCount.getText().toString())) + 1;
                    if (count <= Integer.parseInt(data.get(position).get("quantity"))) {
                        // ((DashBoardFragment) getActivity()).tvCount.setText(Integer.toString(count));
                        //count = count + 1;
                        Vibrator vibe = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
                        vibe.vibrate(100);
                        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                            UpdateCartApi(String.valueOf(count), data2.get(position).get("sub_id"));
                            holder.tvQuantityCount.setText((count) + "");
                            datalist.get(position).put("cart_quantity",count+"");
                        } else {
                            AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                        }
                    } else {

                        AppUtils.showErrorMessage(recyclerView, getString(R.string.addQuantityError)+" "+data.get(position).get("quantity"), mActivity);
                    }
                }
            });
        }

        public int getItemCount() {
            return data.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }
    }
    private class FavNameHolder extends RecyclerView.ViewHolder {
        ImageView ivPic, icWishlist, imageView2, imageView3;
        LinearLayout linearLayout, llCartBtn, llCartCountBtn;
        TextView tvDiscount;
        TextView tvFPrice;
        TextView tvName;
        RelativeLayout btnSpinner;
        TextView tvPrice, tvSpinner, tvQuantityCount;
        Spinner spinners;

        public FavNameHolder(View itemView) {
            super(itemView);
            linearLayout = itemView.findViewById(R.id.layout_item);
            ivPic = itemView.findViewById(R.id.ivProduct);
            icWishlist = itemView.findViewById(R.id.ic_wishlist);
            tvName = itemView.findViewById(R.id.tvProductName);
            tvFPrice = itemView.findViewById(R.id.tvFPrice);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvDiscount = itemView.findViewById(R.id.tvDiscount);
            tvSpinner = itemView.findViewById(R.id.tvSpinner);
            spinners = itemView.findViewById(R.id.spinners);
            tvQuantityCount = itemView.findViewById(R.id.tvQuantityCount);
            llCartCountBtn = itemView.findViewById(R.id.llCartCountBtn);
            llCartBtn = itemView.findViewById(R.id.llCartBtn);
            btnSpinner = itemView.findViewById(R.id.btnSpinner);
            imageView2 = itemView.findViewById(R.id.imageView2);
            imageView3 = itemView.findViewById(R.id.imageView3);
        }
    }

    public void UpdateSize(final String units, final String quantity, View v, final String cartId, final TextView spVal, final TextView price, final TextView finalprice, final TextView tvDiscount, final LinearLayout llCartCountBtn, final LinearLayout llCartBtn) {
        Context wrapper = new ContextThemeWrapper(mActivity, R.style.PopupMenuOverlapAnchor);
        PopupMenu popup = new PopupMenu(wrapper, v);
        HashMap<String, String> hashMap = new HashMap<>();
        popup.getMenu().clear();
        try {
            for (int k = 0; k < datalist.size(); k++) {
                if (cartId.equalsIgnoreCase(datalist.get(k).get("parentId"))) {
                    popup.getMenu().add(k, k, k, datalist.get(k).get("sub_weight") + datalist.get(k).get("unit_name"));
                    hashMap.put("sub_id", datalist.get(k).get("sub_id"));
                    hashMap.put("aprice", datalist.get(k).get("actual_p"));
                    hashMap.put("price", datalist.get(k).get("price"));
                    hashMap.put("per_amount", datalist.get(k).get("per_amount"));
                    hashMap.put("dis_type", datalist.get(k).get("dis_type"));
                    hashMap.put("cart_quantity", datalist.get(k).get("cart_quantity"));
                    hashMapArrayList.add(hashMap);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_main, popup.getMenu());
        popup.show();
        llCartBtn.setVisibility(View.VISIBLE);
        llCartCountBtn.setVisibility(View.GONE);

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final MenuItem item) {

                int id = item.getItemId();
                Log.v("MenuitemIdSelected", String.valueOf(id));
                llCartBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                            Toast.makeText(mActivity, "Please Login to continue", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(mActivity, LoginActivity.class));
                        } else {
                            llCartBtn.setVisibility(View.GONE);
                            llCartCountBtn.setVisibility(View.VISIBLE);
                            AddToCartApi(datalist.get(item.getOrder()).get("sub_id"));
                        }}
                });

                finalprice.setText("RS " + datalist.get(item.getOrder()).get("actual_p"));
                /*spVal.setText( item.getTitle().toString() );*/
                spVal.setText(item.getTitle().toString());

                if (datalist.get(item.getOrder()).get("per_amount").equals("0.00")) {
                    tvDiscount.setVisibility(View.INVISIBLE);
                    price.setText("RS " + datalist.get(item.getOrder()).get("price"));
                } else {
                    tvDiscount.setVisibility(View.VISIBLE);
                    tvDiscount.setText("RS " + datalist.get(item.getOrder()).get("per_amount") + " off");
                }
                if (datalist.get(item.getOrder()).get("dis_type").equals("123")) {
                    price.setVisibility(View.GONE);

                } else {
                    price.setVisibility(View.VISIBLE);
                    price.setText("RS " + datalist.get(item.getOrder()).get("price"));
                }
                //ToDo..
                return true;
            }
        });
    }

    private void AddToCartApi(String productId) {
        AppUtils.showRequestDialog(mActivity);
        Log.v("AddToCartApi", AppUrls.addToCart);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("product_id", productId);
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json.put(AppConstants.result, json_data);
            Log.v("AddToCartApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.addToCart)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseCartdata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            //  AppUtils.showErrorMessage(tvDeatils, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            //  AppUtils.showErrorMessage(tvDeatils, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseCartdata(JSONObject response) {

        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            } else {
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            // AppUtils.showErrorMessage(tvDeatils, String.valueOf(e), this.mActivity);
        }
        AppUtils.hideDialog();
    }

    private void UpdateCartApi(String quantity, String ProductId) {

        Log.v("AddToCartApi", AppUrls.updateCart);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("product_id", ProductId);
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json_data.put("quantity", quantity);
            json.put(AppConstants.result, json_data);
            Log.v("AddToCartApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.updateCart)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedata(JSONObject response) {
        Log.d("response", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
                // cartList.clear();
                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    //getCartListApi();
                } else {
                    AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                }
            } else {
                AppUtils.hideDialog();
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.hideDialog();
            AppUtils.showErrorMessage(recyclerView, String.valueOf(e), mActivity);
        }
    }

    private void DeleteCartApi(String cartId) {
        AppUtils.showRequestDialog(mActivity);
        Log.v("AddToCartApi", AppUrls.deleteFromCart);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("cart_id", cartId);
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json.put(AppConstants.result, json_data);
            Log.v("AddToCartApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.deleteFromCart)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedeletedata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedeletedata(JSONObject response) {
        Log.d("response ", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
                AppUtils.hideDialog();
                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                } else {
                    AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                }
            } else {
                AppUtils.hideDialog();
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.hideDialog();
            AppUtils.showErrorMessage(recyclerView, String.valueOf(e), mActivity);
        }

    }
}

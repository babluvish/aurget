package com.aurget.buddha.Activity.code.Account;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.Product.CartListActivity;
import com.aurget.buddha.Activity.code.Search.SearchActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import code.utils.AppUrls;

public class EditProfileActivity extends BaseActivity implements View.OnClickListener {

    //ImageView header
    ImageView ivMenu, ivSearch, ivCart;

    //TextView
    TextView tvMain, tvCount;

    //Button
    Button btnSave;

    //EditText
    EditText edName, edMobile, edAlternate, edFlat, edLocality, edCity, edPincode, edLandmark;


    Spinner spinnerState;


    ArrayList<HashMap<String, String>> stateList;
    ArrayList<String> Statelist;
    ArrayList<String> StatelistID;

    adapter_spinner adapter_spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_edit_profile );

        initialise();

        if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
            GetStateListApi();
        } else {
            AppUtils.showErrorMessage( edMobile, getString( R.string.error_connection_message ), mActivity );
        }

    }

    private void initialise() {

        AppUtils.hideSoftKeyboard( mActivity );

        //Arraylist
        stateList = new ArrayList<>();
        Statelist = new ArrayList<>();
        StatelistID = new ArrayList<>();


        //Spinner
        spinnerState = findViewById( R.id.spinner );


        //ImageView
        ivMenu = findViewById( R.id.iv_menu );
        ivSearch = findViewById( R.id.searchmain );
        ivCart = findViewById( R.id.ivCart );

        //TextView
        tvCount = findViewById( R.id.tvCount );
        tvMain = findViewById( R.id.tvHeaderText );

        //EditText
        edName = findViewById( R.id.edName );
        edMobile = findViewById( R.id.edMobile );
        edAlternate = findViewById( R.id.edAlternate );
        edFlat = findViewById( R.id.edFlat );
        edLocality = findViewById( R.id.edLocality );
        edCity = findViewById( R.id.edCity );
        edPincode = findViewById( R.id.edPincode );
        edLandmark = findViewById( R.id.edLandmark );

        //Button
        btnSave = findViewById( R.id.btnSave );

        ivMenu.setImageResource( R.drawable.ic_back );

        ivMenu.setOnClickListener( this );
        ivSearch.setOnClickListener( this );
        ivCart.setOnClickListener( this );
        btnSave.setOnClickListener( this );

        if (getIntent().getStringExtra( "From" ).equals( "1" )) {
            tvMain.setText( "Edit Addresses" );

            edName.setText( getIntent().getStringExtra( "Name" ) );
            edMobile.setText( getIntent().getStringExtra( "PhoneNo" ) );
            edPincode.setText( getIntent().getStringExtra( "Pincode" ) );
            edLocality.setText( getIntent().getStringExtra( "Locality" ) );
            edFlat.setText( getIntent().getStringExtra( "Flat" ) );
            edCity.setText( getIntent().getStringExtra( "City" ) );
            edLandmark.setText( getIntent().getStringExtra( "Landmark" ) );
            edAlternate.setText( getIntent().getStringExtra( "Alternate" ) );
        } else {
            tvMain.setText( "Add Addresses" );
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.iv_menu:

                onBackPressed();

                return;

            case R.id.searchmain:
                startActivity( new Intent( getBaseContext(), SearchActivity.class ) );
                return;

            case R.id.ivCart:

                if (AppSettings.getString( AppSettings.userId ).isEmpty()) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else if (AppSettings.getString( AppSettings.verified ).contains( "0" )) {
                    startActivity( new Intent( getBaseContext(), LoginActivity.class ) );
                } else {
                    startActivity( new Intent( getBaseContext(), CartListActivity.class ) );
                }

                return;

            case R.id.btnSave:

                if (edName.getText().toString().isEmpty()) {
                    AppUtils.showErrorMessage( edMobile, getString( R.string.nameError ), mActivity );
                } else if (edMobile.getText().toString().isEmpty()) {
                    AppUtils.showErrorMessage( edMobile, getString( R.string.errorMobileNumber ), mActivity );
                } else if (edMobile.getText().toString().trim().length() < 10) {
                    AppUtils.showErrorMessage( edMobile, getString( R.string.errorProperMobileNumber ), mActivity );
                } else if (edFlat.getText().toString().isEmpty()) {
                    AppUtils.showErrorMessage( edMobile, getString( R.string.flatError ), mActivity );
                } else if (edLocality.getText().toString().isEmpty()) {
                    AppUtils.showErrorMessage( edMobile, getString( R.string.localityError ), mActivity );
                } else if (edCity.getText().toString().isEmpty()) {
                    AppUtils.showErrorMessage( edMobile, getString( R.string.cityError ), mActivity );
                } else if (edPincode.getText().toString().isEmpty()) {
                    AppUtils.showErrorMessage( edMobile, getString( R.string.pincodeError ), mActivity );
                } else if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                    AddEditAddress();
                } else {
                    AppUtils.showErrorMessage( edMobile, getString( R.string.error_connection_message ), mActivity );
                }

                return;
        }
    }


    private void AddEditAddress() {

        AppUtils.showRequestDialog( mActivity );

        Log.v( "AddEditAddress", AppUrls.addNewAddress );

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {

            if (getIntent().getStringExtra( "From" ).equals( "1" )) {
                json_data.put( "address_master_id", getIntent().getStringExtra( "AddressId" ) );
                json_data.put( "add_update_delete", "2" );
            } else {
                json_data.put( "address_master_id", "" );
                json_data.put( "add_update_delete", "1" );
            }

            json_data.put( "user_master_id", AppSettings.getString( AppSettings.userId ) );
            json_data.put( "name", edName.getText().toString().trim() );
            json_data.put( "phone_no", edMobile.getText().toString().trim() );
            json_data.put( "pincode", edPincode.getText().toString().trim() );
            json_data.put( "locality", edLocality.getText().toString().trim() );
            json_data.put( "area_streat_address", edFlat.getText().toString().trim() );
            json_data.put( "district_city_town", edCity.getText().toString().trim() );
            json_data.put( "landmark_optional", edLandmark.getText().toString().trim() );
            json_data.put( "alternative_phone_optional", edAlternate.getText().toString().trim() );
            json_data.put( "state_master_id", StatelistID.get( spinnerState.getSelectedItemPosition() ) );

            json.put( AppConstants.result, json_data );

            Log.v( "AddEditAddress", json.toString() );

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post( AppUrls.addNewAddress )
                .addJSONObjectBody( json )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedeletedata( response );
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage( edAlternate, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            AppUtils.showErrorMessage( edAlternate, String.valueOf( error.getErrorDetail() ), mActivity );
                        }
                    }
                } );
    }

    private void parsedeletedata(JSONObject response) {

        Log.d( "response ", response.toString() );

        try {
            JSONObject jsonObject = response.getJSONObject( AppConstants.responseTag );
            if (jsonObject.getString( "res_code" ).equals( "1" )) {
                AppUtils.showErrorMessage( edAlternate, String.valueOf( jsonObject.getString( "res_msg" ) ), mActivity );
                AppUtils.hideDialog();

                onBackPressed();
            } else {
                AppUtils.hideDialog();
                AppUtils.showErrorMessage( edAlternate, String.valueOf( jsonObject.getString( "res_msg" ) ), mActivity );
            }
        } catch (Exception e) {
            AppUtils.hideDialog();
            AppUtils.showErrorMessage( edAlternate, String.valueOf( e ), mActivity );
        }

    }


    private void GetStateListApi() {

        AppUtils.showRequestDialog( mActivity );

        Log.v( "GetStateListApi", AppUrls.getState );

        AndroidNetworking.get( AppUrls.getState )
                .setPriority( Priority.HIGH )
                .build()
                .getAsJSONObject( new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata( response );
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage( edAlternate, String.valueOf( error.getErrorCode() ), mActivity );
                            Log.d( "onError errorCode ", "onError errorCode : " + error.getErrorCode() );
                            Log.d( "onError errorBody", "onError errorBody : " + error.getErrorBody() );
                            Log.d( "onError errorDetail", "onError errorDetail : " + error.getErrorDetail() );
                        } else {
                            AppUtils.showErrorMessage( edAlternate, String.valueOf( error.getErrorDetail() ), mActivity );
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            //Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        }
                    }
                } );
    }

    private void parseJsondata(JSONObject response) {
        Log.d( "response ", response.toString() );
        stateList.clear();
        StatelistID.clear();
        Statelist.clear();
        Statelist.add( "Select State" );
        StatelistID.add( "Select State" );
        try {
            JSONObject jsonObject = response.getJSONObject( AppConstants.responseTag );
            if (jsonObject.getString( "res_code" ).equals( "1" )) {
                int i;
                JSONArray bannerArray = jsonObject.getJSONArray( "AllStates" );
                for (i = 0; i < bannerArray.length(); i++) {
                    JSONObject bannerArrayJSONObject = bannerArray.getJSONObject( i );
                    HashMap<String, String> list = new HashMap();
                    list.put( "id", bannerArrayJSONObject.getString( "id" ) );
                    list.put( "name", bannerArrayJSONObject.getString( "name" ) );
                    stateList.add( list );
                    Statelist.add( bannerArrayJSONObject.getString( "name" ) );
                    StatelistID.add( bannerArrayJSONObject.getString( "id" ) );
                }


                // spinnerState.setAdapter(new adapter_spinner(getApplicationContext(), R.layout.spinner_textview_rel, Statelist));
                adapter_spinner = new adapter_spinner( getApplicationContext(), R.layout.spinner_textview_rel, Statelist );
                adapter_spinner.notifyDataSetChanged();
                spinnerState.setAdapter( adapter_spinner );

            } else {
                AppUtils.showErrorMessage( tvMain, String.valueOf( jsonObject.getString( "res_msg" ) ), mActivity );
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage( tvMain, String.valueOf( e ), mActivity );
        }

        AppUtils.hideDialog();
    }

    @Override
    protected void onResume() {
        super.onResume();

        int count = Integer.parseInt( AppSettings.getString( AppSettings.cartCount ) );

        if (count > 0) {
            tvCount.setVisibility( View.VISIBLE );
            tvCount.setText( String.valueOf( count ) );
        } else {
            tvCount.setVisibility( View.GONE );
        }
    }


    public class adapter_spinner extends ArrayAdapter<String> {

        ArrayList<String> data;

        public adapter_spinner(Context context, int textViewResourceId, ArrayList<String> arraySpinner_time) {

            super( context, textViewResourceId, arraySpinner_time );

            this.data = arraySpinner_time;

        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView( position, convertView, parent );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView( position, convertView, parent );
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from( parent.getContext() );
            View row = inflater.inflate( R.layout.spinner_textview_rel, parent, false );
            TextView label = row.findViewById( R.id.tv_spinner_name );

            label.setText( data.get( position ) );

            //label.setTypeface(typeface);

            return row;
        }
    }

}

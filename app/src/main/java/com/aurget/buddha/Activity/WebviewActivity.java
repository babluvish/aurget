package com.aurget.buddha.Activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.aurget.buddha.R;

public class WebviewActivity extends AppCompatActivity {

    TextView title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        WebView wv = findViewById(R.id.webview);
        title = findViewById(R.id.title);
        title.setText("Description Details");
        final String mimeType = "text/html";
        final String encoding = "UTF-8";
        String html = getIntent().getStringExtra("html");
        Log.d("html",html);

//        String html = "<br /><br />Read the handouts please for tomorrow.<br /><br /><!--homework help homework" +
//                "help help with homework homework assignments elementary school high school middle school" +
//                "// --><font color='#60c000' size='4'><strong>Please!</strong></font>" +
//                "<img src='http://www.homeworknow.com/hwnow/upload/images/tn_star300.gif'  />";

        wv.loadDataWithBaseURL("", html, mimeType, encoding, "");

        findViewById(R.id.bkImg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}

package com.aurget.buddha.Activity.code.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.aurget.buddha.Activity.Utils.Constnt;
import com.aurget.buddha.Activity.code.Account.AccountActivity;
import com.aurget.buddha.Activity.code.Account.FavouriteListActivity;
import com.aurget.buddha.Activity.code.Adapter.MyPagerAdapter;
import com.aurget.buddha.Activity.code.Category.CategoryActivity;
import com.aurget.buddha.Activity.code.Common.CustomLoader;
import com.aurget.buddha.Activity.code.Common.NotificationUtils;
import com.aurget.buddha.Activity.code.Common.SimpleHTTPConnection;
import com.aurget.buddha.Activity.code.Dashboard.Slider.CardPagerAdapter;
import com.aurget.buddha.Activity.code.Database.AppSettings;
import com.aurget.buddha.Activity.code.Database.DatabaseController;
import com.aurget.buddha.Activity.code.Database.TableCategory;
import com.aurget.buddha.Activity.code.Database.TableFavourite;
import com.aurget.buddha.Activity.code.Database.TableSubCategory;
import com.aurget.buddha.Activity.code.Main.LoginActivity;
import com.aurget.buddha.Activity.code.Main.NotificationActivity;
import com.aurget.buddha.Activity.code.Product.CartListActivity;
import com.aurget.buddha.Activity.code.Product.ProductDetailsActivity;
import com.aurget.buddha.Activity.code.Product.ProductListActivity;
import com.aurget.buddha.Activity.code.Search.SearchActivity;
import com.aurget.buddha.Activity.code.utils.AppConstants;
import com.aurget.buddha.Activity.code.utils.AppUtils;
import com.aurget.buddha.Activity.code.view.BaseActivity;
import com.aurget.buddha.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import code.Common.Config;
import code.model.CartModal;
import code.model.product_master;
import code.utils.AppUrls;

public class DashBoardFragment extends BaseActivity implements View.OnClickListener {

    public static int VB_PAGES;
    public static int VB_FIRST_PAGE;
    public static String regId = "";
    // ViewPager currentpage
    private static int currentPage = 0;
    // ViewPager no offpage
    private static int NUM_PAGES = 0;
    ArrayList<HashMap<String, String>> subProductListArrayList = new ArrayList();
    MyPagerAdapter adapter;
    ArrayList<HashMap<String, String>> cartList = new ArrayList();
    ArrayList<HashMap<String, String>> CategoryList = new ArrayList();
    ArrayList<HashMap<String, String>> FeaturedBannerList = new ArrayList();
    ArrayList<HashMap<String, String>> OfferList = new ArrayList();
    ArrayList<ArrayList<HashMap<String, String>>> ProducttList = new ArrayList();
    ArrayList<HashMap<String, String>> TopBannerList = new ArrayList();
    ArrayList<HashMap<String, String>> FeaturedProductList = new ArrayList();
    ArrayList<HashMap<String, String>> sliderBannerList = new ArrayList();
    ArrayList<HashMap<String, String>> hashMapArrayList = new ArrayList<>();
    OfferAdapter offerAdapter;
    GridLayoutManager layoutManager;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    Timer timer;
    String Subid;
    RecyclerView recyclerView;
    ImageView ivCat1, ivCat2, ivCat3, ivCat4,
            ivDeal1, ivDeal2, ivDeal3, ivDeal4,
            ivDrop, ivMenu, ivSearch, ivCart, ivLogo, ivAll;
    SwipeRefreshLayout swipeRefreshLayout;
    LinearLayout llHome, llCategory,
            llFavourites, llProfile, llNotification;
    TextView tvHome, tvCategory,
            tvFavourites, tvProfile, tvNotification;
    TextView tvCount;
    ImageView ivHome, ivCategory,
            ivFavourites, ivProfile, ivNotification;
    EditText searchEdit;
    TextView tvBestValue;
    ScrollView scrollView;
    TextView tvCat1, tvCat2, tvCat3, tvCat4, tvClock, tvDName1, tvDName2, tvDName3,
            tvDName4, tvDPrice1, tvDPrice2, tvDPrice3,
            tvDPrice4, tvLogin, tvUserName,
            tvMain, tvAll, tvTopFeatures;
    View view;
    RelativeLayout rrSearch, relativeLayoutSearch;
    Typeface typeface;
    String offset = "0", categotytName = "";
    LinearLayout llCat1, llCat2, llCat3, llCat4, llDealOfDay, llMore,
            llOffers;
    ViewPager mViewPager;
    CustomLoader loader;
    String apiUrl;
    CardPagerAdapter mCardAdapter;
    private boolean doubleBackToExitPressedOnce;
    private boolean loading = true;
    //FramLayout
//    FrameLayout flDashboard;
    //FragmentManager
//    FragmentManager fragmentManager;
    private Handler handler;
    private Runnable runnable;
    //Fragments
//    private Fragment fragment;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private LinearLayout dotsLayout;
    private ImageView[] dots;
    //BaseFragment
    private boolean exit = false;

    CartModal cartModal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboardfragment);
        apiUrl = AppUrls.getDashboard;

        if (!DatabaseController.isDataExit(TableCategory.category)) {
            AppSettings.putString(AppSettings.categoryTime, "");
        }

        if (SimpleHTTPConnection.isNetworkAvailable(this.mActivity)) {
            GetCatListApi();
        } else {
            AppUtils.showErrorMessage(this.tvCat4, getString(R.string.error_connection_message), this.mActivity);
        }

        init();

        cartModal = new CartModal();

        swipeRefreshLayout.setOnRefreshListener(() -> {
            swipeRefreshLayout.setRefreshing(false);
            loader.show();
            loader.setCancelable(false);
            loader.setCanceledOnTouchOutside(false);
            offset = "0";
            FeaturedProductList.clear();
            sliderBannerList.clear();
            GetDashboardListApi();
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    Log.v("ksqbmbq", "1");
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        Log.v("ksqbmbq", "2");
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            GetDashboardListApi();
                            loading = false;
                            Log.v("ksqbmbq", "3");
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });

        // displayView(0);
    }

    private void init() {
        mViewPager = findViewById(R.id.myviewpager);
        recyclerView = findViewById(R.id.recyclerview);
        dotsLayout = findViewById(R.id.layoutDots);
        layoutManager = new GridLayoutManager(mActivity, 2);
        swipeRefreshLayout = findViewById(R.id.swiperefresh);

        recyclerView.setLayoutManager(layoutManager);
        loader = new CustomLoader(mActivity, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);

        //getWindow().setSoftInputMode(2);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    displayFirebaseRegId();
                    AppSettings.putString(AppSettings.fcmId, regId);
                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    String message = intent.getStringExtra("message");
                    Log.v("msg", message);
                }
            }
        };


        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d("Key: ", key + " Value: " + value);
            }
        }

        //TextView
        tvMain = findViewById(R.id.tvHeaderText);
        tvTopFeatures = findViewById(R.id.tvTopFeatures);
        //LinearLayout

        //ScrollView
        scrollView = findViewById(R.id.scroll_side_menu);

        //View
        view = findViewById(R.id.view);
        //RelativeLayout
        rrSearch = findViewById(R.id.rlSearch);
        relativeLayoutSearch = findViewById(R.id.relativeLayoutSearch);
        rrSearch.setVisibility(View.VISIBLE);
        //ImageView
        ivMenu = findViewById(R.id.iv_menu);
        ivDrop = findViewById(R.id.imageView3);
        ivSearch = findViewById(R.id.searchmain);
        searchEdit = findViewById(R.id.searchEdit);
        ivDeal1 = findViewById(R.id.ivProduct);
        ivDeal2 = findViewById(R.id.ivD2);
        ivDeal3 = findViewById(R.id.ivD3);
        ivDeal4 = findViewById(R.id.ivD4);
        ivCat1 = findViewById(R.id.ivCat1);
        ivCat2 = findViewById(R.id.ivCat2);
        ivCat3 = findViewById(R.id.ivCat3);
        ivCat4 = findViewById(R.id.ivCat4);
        ivHome = findViewById(R.id.ivHomeBottom);
        tvBestValue = findViewById(R.id.tvBestValue);
        ivCategory = findViewById(R.id.ivCategoryBottom);
        ivFavourites = findViewById(R.id.ivFavouritesBottom);
        ivProfile = findViewById(R.id.ic_profile);
        ivNotification = findViewById(R.id.ic_notification);
        ivCart = findViewById(R.id.ivCart);
        ivLogo = findViewById(R.id.iv_logo);

        //TextView
        tvDName1 = findViewById(R.id.tvProductName);
        tvDName2 = findViewById(R.id.tvD2);
        tvDName3 = findViewById(R.id.tvD3);
        ivAll = findViewById(R.id.ivAll);
        tvDName4 = findViewById(R.id.tvD4);
        tvDPrice1 = findViewById(R.id.tvFPrice);
        tvDPrice2 = findViewById(R.id.tvDPrice2);
        tvDPrice3 = findViewById(R.id.tvDPrice3);
        tvDPrice4 = findViewById(R.id.tvDPrice4);
        tvCat1 = findViewById(R.id.tvCat1);
        tvCat2 = findViewById(R.id.tvCat2);
        tvCat3 = findViewById(R.id.tvCat3);
        tvCat4 = findViewById(R.id.tvCat4);
        tvClock = findViewById(R.id.tvClock);

        llCat1 = findViewById(R.id.llCat1);
        llCat2 = findViewById(R.id.llCat2);
        llCat3 = findViewById(R.id.llCat3);
        llCat4 = findViewById(R.id.llCat4);
        llMore = findViewById(R.id.llMore);

        llCat1.setOnClickListener(this);
        llCat2.setOnClickListener(this);
        llCat3.setOnClickListener(this);
        llCat4.setOnClickListener(this);
        llMore.setOnClickListener(this);


        tvAll = findViewById(R.id.tvAll);
        tvHome = findViewById(R.id.tvHomeBottom);
        tvCategory = findViewById(R.id.tvCategoryBottom);
        tvFavourites = findViewById(R.id.tvFavouritesBottom);
        tvProfile = findViewById(R.id.tvProfileBottom);
        tvNotification = findViewById(R.id.tvNotificationBottom);
        tvCount = findViewById(R.id.tvCount);
        tvLogin = findViewById(R.id.tv_login);
        tvUserName = findViewById(R.id.tvUserName);

        //LinearLayout fro bottom views
        llHome = findViewById(R.id.llHome);
        llCategory = findViewById(R.id.llCategory);
        llFavourites = findViewById(R.id.llFavourites);
        llProfile = findViewById(R.id.llProfile);
        llNotification = findViewById(R.id.llNotification);

        tvBestValue.setTextColor(Color.parseColor("#000000"));
        tvTopFeatures.setTextColor(Color.parseColor("#bfc2c2"));
        tvAll.setTextColor(Color.parseColor("#bfc2c2"));
        onclickListener();

        //searchEdit.setOnClickListener(v -> startActivity(new Intent(mActivity, SearchActivity.class)));
        rrSearch.setOnClickListener(v -> startActivity(new Intent(mActivity, SearchActivity.class)));

        //rrSearch.performClick();
        //searchEdit.performClick();
        //getCatData();

    }

    public static void openWhatsApp(Activity activity) {
        String contact = "+91 9662101102"; // use country code with your phone number
        String url = "https://api.whatsapp.com/send?phone=" + contact;
        try {
            PackageManager pm = activity.getPackageManager();
            pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            activity.startActivity(i);
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(activity, "Whatsapp app not installed in your phone", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public void topBannerAdapter() {

        currentPage++;
        NUM_PAGES = TopBannerList.size();

        VB_PAGES = TopBannerList.size();
        VB_FIRST_PAGE = VB_PAGES / 2;

        // adapter=new MyPagerAdapter(getApplicationContext(),getSupportFragmentManager(),TopBannerList);

        mCardAdapter = new CardPagerAdapter(getApplicationContext(), TopBannerList) {
            protected void onCategoryClick(View v, String position) {
                try {
                    bannerImageDialog(position);
                } catch (WindowManager.BadTokenException ex) {
                    ex.printStackTrace();
                }

                if (TopBannerList.get(Integer.parseInt(position)).get("banner_on").equals("1")) {

                    /*Intent mIntent = new Intent( mActivity, ProductListActivity.class );
                    mIntent.putExtra( AppConstants.subCategoryId, TopBannerList.get( Integer.parseInt( position ) ).get( "banner_id" ) );
                    mIntent.putExtra( AppConstants.from, "1" );
                    startActivity( mIntent );*/
                } else if (TopBannerList.get(Integer.parseInt(position)).get("banner_on").equals("2")) {

                  /*  Intent mIntent = new Intent( mActivity, ProductDetailsActivity.class );
                    mIntent.putExtra( AppConstants.productId, TopBannerList.get( Integer.parseInt( position ) ).get( "banner_id" ) );
                    mIntent.putExtra( AppConstants.from, "1" );
                    startActivity( mIntent );*/
                }
            }
        };
        mViewPager.setAdapter(mCardAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);
                Log.v("postiopn123", String.valueOf(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mViewPager.setPageTransformer(false, adapter);
        mViewPager.setCurrentItem(currentPage, true);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setClipToPadding(false);
        mViewPager.setPadding(15, 15, 15, 15);
        mViewPager.setPageMargin(15);
        //mViewPager.setElevation(10f);
        mViewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                int pageWidth = mViewPager.getMeasuredWidth() -
                        mViewPager.getPaddingLeft() - mViewPager.getPaddingRight();
                int pageHeight = mViewPager.getHeight();
                int paddingLeft = mViewPager.getPaddingLeft();
                float transformPos = (float) (page.getLeft() -
                        (mViewPager.getScrollX() + paddingLeft)) / pageWidth;
                int max = pageHeight / 10;
                if (transformPos < -1) {
                    page.setScaleY(0.8f);
                } else if (transformPos <= 1) {
                    page.setScaleY(1f);
                } else {

                    page.setScaleY(0.8f);
                }
            }
        });
    }

    public void bannerImageDialog(String position) {

        final Dialog dialog = new Dialog(mActivity);
        // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.bannerpopup);
        //  Window window = dialog.getWindow();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        // WindowManager.LayoutParams wlp = window.getAttributes();
        // wlp.gravity = Gravity.CENTER;
        //  wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        //  window.setAttributes(wlp);
        //  window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        //  dialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        ImageView ivBanner = dialog.findViewById(R.id.ivBanner);
        Picasso.with(dialog.getContext()).load(TopBannerList.get(Integer.parseInt(position)).get("banner_image")).into(ivBanner);
        Log.v("StringData", String.valueOf(TopBannerList));
        dialog.show();
    }

    private void addBottomDots(int currentPage) {
        // dotscount = viewPagerAdapter.getCount();
        dots = new ImageView[TopBannerList.size()];
        //  dots[currentPage].setImageResource(R.drawable.ic_circle_white);
        dotsLayout.removeAllViews();
        for (int i = 0; i < TopBannerList.size(); i++) {
            dots[i] = new ImageView(mActivity);

            dots[i].setImageResource(R.drawable.ic_circle_white);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(20,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(5, 0, 5, 0);

            dotsLayout.addView(dots[i], params);


        }
        if (TopBannerList.size() > 0)
            dots[currentPage].setImageResource(R.drawable.ic_circle_blue);

    }

    public void getCatData() {
        int j = 0;

        CategoryList.clear();
        CategoryList.addAll(DatabaseController.getFeaturedCategoryData());
        // CategoryList.addAll(DatabaseController.getSubCategoryData(TableSubCategory.sub_category, TableSubCategory.subCatColumn.category_id.toString(), "4"));
        for (int i = 0; i < CategoryList.size(); i++)
        {
            if (j == 0)
            {
                j = j + 1;
                llCat1.setVisibility(View.VISIBLE);
                llCat1.setTag(((HashMap) CategoryList.get(i)).get("category_id"));
                categotytName = (String) ((HashMap) CategoryList.get(i)).get("category_name");
                Log.v("vegetables", (String) ((HashMap) CategoryList.get(i)).get("category_name"));
                tvCat1.setText(AppUtils.split((String) ((HashMap) CategoryList.get(i)).get("category_name")));

                if (((String) ((HashMap) CategoryList.get(i)).get("app_icon")).isEmpty())
                {
                    Picasso.with(mActivity).load(R.mipmap.ic_launcher).into(ivCat1);
                } else {
                    Picasso.with(mActivity).load((String) ((HashMap) CategoryList.get(i)).get("app_icon")).into(ivCat1);
                }

            } else if (j == 1) {
                j = j + 1;
                llCat2.setVisibility(View.VISIBLE);
                llCat2.setTag(((HashMap) CategoryList.get(i)).get("category_id"));
                tvCat2.setText(AppUtils.split((String) ((HashMap) CategoryList.get(i)).get("category_name")));
                if (((String) ((HashMap) CategoryList.get(i)).get("app_icon")).isEmpty()) {
                    Picasso.with(mActivity).load(R.mipmap.ic_launcher).into(ivCat2);
                } else {
                    Picasso.with(mActivity).load((String) ((HashMap) CategoryList.get(i)).get("app_icon")).into(ivCat2);
                }
            } else if (j == 2) {
                j = j + 1;
                llCat3.setVisibility(View.VISIBLE);
                llCat3.setTag(((HashMap) CategoryList.get(i)).get("category_id"));
                tvCat3.setText(AppUtils.split((String) ((HashMap) CategoryList.get(i)).get("category_name")));
                if (((String) ((HashMap) CategoryList.get(i)).get("app_icon")).isEmpty()) {
                    Picasso.with(mActivity).load(R.mipmap.ic_launcher).into(ivCat3);
                } else {
                    Picasso.with(mActivity).load((String) ((HashMap) CategoryList.get(i)).get("app_icon")).into(ivCat3);
                }

            } else if (j == 3) {
                j = j + 1;
                llCat4.setVisibility(View.VISIBLE);
                llCat4.setTag(((HashMap) CategoryList.get(i)).get("category_id"));
                tvCat4.setText(AppUtils.split((String) ((HashMap) CategoryList.get(i)).get("category_name")));
                if (((String) ((HashMap) CategoryList.get(i)).get("app_icon")).isEmpty()) {
                    Picasso.with(mActivity).load(R.mipmap.ic_launcher).into(ivCat4);
                } else {
                    Picasso.with(mActivity).load((String) ((HashMap) CategoryList.get(i)).get("app_icon")).into(ivCat4);
                }
            }
        }
        //AppUtils.hideSoftKeyboard(DashBoardFragment.this);
        closeKeyboard();
    }

    private void closeKeyboard() {
        // this will give us the view
        // which is currently focus
        // in this layout
    }

    private void GetDashboardListApi() {
        Log.v("GetDashboardListApi", apiUrl);
        AppUtils.hideSoftKeyboard(mActivity);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("offset", offset);
            if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                json_data.put("user_id", "0");
            } else {
                json_data.put("user_id", AppSettings.getString(AppSettings.userId));
            }
            json.put(AppConstants.result, json_data);
            Log.v("GetDashboardListApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(apiUrl)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvCat4, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvCat4, String.valueOf(error.getErrorDetail()), mActivity);
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            //Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        }
                    }
                });
    }

    private void parseJsondata(JSONObject response) {
        Log.d("GetDashboardListApi", response.toString());

        TopBannerList.clear();
        FeaturedBannerList.clear();
        OfferList.clear();
        loader.cancel();

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                offset = jsonObject.getString("offset");
                int i;
                JSONArray bannerArray = jsonObject.getJSONArray("banner");
                for (i = 0; i < bannerArray.length(); i++) {
                    JSONObject bannerArrayJSONObject = bannerArray.getJSONObject(i);
                    HashMap<String, String> bannerlist = new HashMap();
                    bannerlist.put("banner_id", bannerArrayJSONObject.getString("banner_id"));
                    bannerlist.put("banner_on", bannerArrayJSONObject.getString("banner_on"));
                    bannerlist.put("id", bannerArrayJSONObject.getString("id"));
                    bannerlist.put("featured_banner", bannerArrayJSONObject.getString("featured_banner"));
                    bannerlist.put("banner_image", bannerArrayJSONObject.getString("banner_image"));

                    if (bannerArrayJSONObject.getString("featured_banner").equals("1")) {
                        TopBannerList.add(bannerlist);
                        AppConstants.imagesList.add(bannerlist);
                    } else {
                        FeaturedBannerList.add(bannerlist);
                        AppConstants.imagesList.add(bannerlist);
                    }
                }
                String listInString;
                List<product_master> productList;
                JSONArray productArray;
                if (jsonObject.has("features_product")) {
                    productArray = jsonObject.getJSONArray("features_product");
                    listInString = jsonObject.getString("features_product");
                } else {
                    productArray = jsonObject.getJSONArray("product_master");
                    listInString = jsonObject.getString("product_master");

                }
                productList = product_master.createJsonInList(listInString);
                for (i = 0; i < productArray.length(); i++) {
                    JSONObject productArrayJSONObject = productArray.getJSONObject(i);
                    if (productArrayJSONObject.has("banner_image")) {
                        HashMap<String, String> sliderBanner = new HashMap();
                        sliderBanner.put("banner_image", productArrayJSONObject.getString("banner_image"));
                        subProductListArrayList.add(sliderBanner);
                    } else {
                        HashMap<String, String> productlist = new HashMap();
                        productlist.put("id", productArrayJSONObject.getString("id"));
                        productlist.put("category_id", productArrayJSONObject.getString("category_master_id"));
                        productlist.put("sub_category_master_id", productArrayJSONObject.getString("sub_category_master_id"));
                        productlist.put("product_name", productArrayJSONObject.getString("product_name"));
                        productlist.put("product_image", productArrayJSONObject.getString("image"));
                        productlist.put("price", productArrayJSONObject.getString("price"));
                        productlist.put("final_price", productArrayJSONObject.getString("final_price"));
                        productlist.put("weight", productArrayJSONObject.getString("weight"));
                        productlist.put("unit_name", productArrayJSONObject.getString("unit_name"));
                        productlist.put("product_discount_type", productArrayJSONObject.getString("product_discount_type"));
                        productlist.put("quantity", productArrayJSONObject.getString("quantity"));
                        productlist.put("product_discount_amount", productArrayJSONObject.getString("product_discount_amount"));
                        productlist.put("sub_product", productArrayJSONObject.getString("sub_product"));

                        if (productArrayJSONObject.has("sub_product")) {
                            JSONArray newsliderarray = productArrayJSONObject.getJSONArray("sub_product");
                            for (int j = 0; j < newsliderarray.length(); j++) {
                                JSONObject sliderObject = newsliderarray.getJSONObject(j);
                                HashMap<String, String> sliderMap = new HashMap<>();
                                if (j == 0) {
                                    productlist.put("SpinerValue", sliderObject.get("sub_weight").toString() + sliderObject.get("unit_name").toString());
                                    productlist.put("SpinerValueId", sliderObject.get("sub_id").toString());
                                }
                                sliderMap.put("sub_id", sliderObject.get("sub_id").toString());
                                sliderMap.put("dis_type", sliderObject.get("dis_type").toString());
                                sliderMap.put("per_amount", sliderObject.get("per_amount").toString());
                                sliderMap.put("sub_weight", sliderObject.get("sub_weight").toString());
                                sliderMap.put("unit_name", sliderObject.get("unit_name").toString());
                                sliderMap.put("actual_p", sliderObject.get("actual_p").toString());
                                sliderMap.put("price", sliderObject.get("price").toString());
                                sliderMap.put("parentId", productArrayJSONObject.getString("id"));
                                sliderMap.put("cart_quantity", sliderObject.getString("cart_quantity"));
                                sliderMap.put("cartId", sliderObject.getString("cartId"));
                                sliderBannerList.add(sliderMap);
                                Log.v("dshf", sliderObject.get("dis_type").toString());

/*
                                if(!sliderObject.getString("cartId").equalsIgnoreCase("")) {

                                    cartModal.setId(productArrayJSONObject.getString("id"));
                                    cartModal.setCart_id(sliderObject.getString("cartId"));
                                    MyData.myDataArrayList.add(cartModal);
                                }
*/
                            }
                        }
                        FeaturedProductList.add(productlist);

                    }

                }
                //  Adapter adapter = new Adapter( productList );
                offerAdapter = new OfferAdapter(FeaturedProductList, sliderBannerList, subProductListArrayList);
                recyclerView.setAdapter(offerAdapter);
                recyclerView.setNestedScrollingEnabled(false);

                closeKeyboard();

            } else {
                AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();
        topBannerAdapter();
     /*   mViewPager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mActivity, "fhdgfffgf", Toast.LENGTH_SHORT).show();
            }
        });*/
        if (TopBannerList.size() > 0) {
            TimerTask timerTask = new MyTimerTask();
            timer = new Timer();
            timer.schedule(timerTask, 3000, 3000);
        }

        AppSettings.putString(AppSettings.cartCount, "0");
        if (!AppSettings.getString(AppSettings.userId).isEmpty()) {
            if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                getCartListApi();
            } else {
                AppUtils.showErrorMessage(tvMain, getString(R.string.errorInternet), mActivity);
            }
        } else {
            if (AppSettings.getString(AppSettings.fcmSend).equals("0") || AppSettings.getString(AppSettings.fcmSend).isEmpty()) {
                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    sendFCMApi();
                } else {
                    AppUtils.showErrorMessage(tvMain, getString(R.string.errorInternet), mActivity);
                }
            }
        }
    }

    private void onclickListener() {
        llHome.setOnClickListener(this);
        llCategory.setOnClickListener(this);
        llFavourites.setOnClickListener(this);
        llProfile.setOnClickListener(this);
        llNotification.setOnClickListener(this);
        tvBestValue.setOnClickListener(this);
        tvTopFeatures.setOnClickListener(this);
        ivMenu.setOnClickListener(this);
        searchEdit.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        rrSearch.setOnClickListener(this);

        relativeLayoutSearch.setOnClickListener(this);
        ivCart.setOnClickListener(this);
        tvAll.setOnClickListener(this);
        ivAll.setOnClickListener(this);
        ivLogo.setOnClickListener(this);

        ivLogo.setClickable(true);
        ivLogo.setFocusable(true);

        ivLogo.setVisibility(View.VISIBLE);
        ivMenu.setVisibility(View.GONE);

        ivLogo.setImageResource(R.drawable.back_arrow);

        findViewById(R.id.whatsll).setOnClickListener(v -> openWhatsApp(mActivity));

        searchEdit.setOnClickListener(v -> startActivity(new Intent(mActivity, SearchActivity.class)));

        //typeface = Typeface.createFromAsset(mActivity.getAssets(), "centurygothic.otf");

        try {
            int count = Integer.parseInt(AppSettings.getString(AppSettings.cartCount));
            if (count > 0) {
                tvCount.setVisibility(View.VISIBLE);
                tvCount.setText(String.valueOf(count));
                Log.v("value", String.valueOf(count));
            } else {
                tvCount.setVisibility(View.GONE);
            }
        } catch (NumberFormatException e) {

        }
    }

    public void displayView(int position) {
        offset = "0";
        loader.show();
        FeaturedProductList.clear();
        sliderBannerList.clear();
        switch (position) {
            case 0:
                apiUrl = AppUrls.getDashboard;
                GetDashboardListApi();
                break;
            case 1:
                apiUrl = AppUrls.FeaturedProduct;
                GetDashboardListApi();
                break;
        }
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onClick(View v) {
        Intent mIntent;
        switch (v.getId()) {
            case R.id.llCat1:
           /*    mIntent = new Intent(mActivity, SubCategoryActivity.class);
              //  mIntent.putExtra(AppConstants.subCategoryId, (String) ((HashMap) CategoryList.get(0)).get("sub_category_id"));
              //  mIntent.putExtra(AppConstants.from, "4");
                startActivity(mIntent);*/
//                mIntent = new Intent(mActivity, SubCategoryActivity.class);
//                SubCategoryActivity.categoryId = llCat1.getTag().toString();
//                mIntent.putExtra("category_name", tvCat1.getText().toString());
//                startActivity(mIntent);

                ProductListActivity.categoryId = llCat1.getTag().toString();
                startActivity(new Intent(getBaseContext(), ProductListActivity.class).putExtra("category_name", tvCat1.getText().toString()));
                return;
            case R.id.llCat2:
              /*  mIntent = new Intent(mActivity, SubCategoryActivity.class);
               // mIntent.putExtra(AppConstants.subCategoryId, (String) ((HashMap) CategoryList.get(1)).get("sub_category_id"));
               // mIntent.putExtra(AppConstants.from, "4");
                startActivity(mIntent);*/

                // for (int i = 0; i < CategoryList.size(); i++) {
//                mIntent = new Intent(mActivity, SubCategoryActivity.class);
//                SubCategoryActivity.categoryId = llCat2.getTag().toString();
//                mIntent.putExtra("category_name", tvCat2.getText().toString());
//                startActivity(mIntent);


                ProductListActivity.categoryId = llCat2.getTag().toString();
                startActivity(new Intent(getBaseContext(), ProductListActivity.class).putExtra("category_name", tvCat2.getText().toString()));
                // }
                break;
            case R.id.llCat3:
//                mIntent = new Intent(mActivity, SubCategoryActivity.class);
//                SubCategoryActivity.categoryId = llCat3.getTag().toString();
//                mIntent.putExtra("category_name", tvCat3.getText().toString());
//                startActivity(mIntent);

                ProductListActivity.categoryId = llCat3.getTag().toString();
                startActivity(new Intent(getBaseContext(), ProductListActivity.class).putExtra("category_name", tvCat3.getText().toString()));


                break;
            case R.id.llCat4:
//                mIntent = new Intent(mActivity, SubCategoryActivity.class);
//                SubCategoryActivity.categoryId = llCat4.getTag().toString();
//                mIntent.putExtra("category_name", tvCat4.getText().toString());
//                startActivity(mIntent);

                ProductListActivity.categoryId = llCat4.getTag().toString();
                startActivity(new Intent(getBaseContext(), ProductListActivity.class).putExtra("category_name", tvCat4.getText().toString()));

                break;

            case R.id.llMore:
                startActivity(new Intent(mActivity, CategoryActivity.class));
                break;

            case R.id.view:
                ivDrop.setVisibility(View.GONE);
                //scrollView.setVisibility(View.GONE);
                view.setVisibility(View.GONE);
                AppUtils.collapse(scrollView);
                break;

            case R.id.tvBestValue:
                tvBestValue.setTextColor(Color.parseColor("#000000"));
                tvTopFeatures.setTextColor(Color.parseColor("#bfc2c2"));
                tvAll.setTextColor(Color.parseColor("#bfc2c2"));
                displayView(0);
                break;

            case R.id.searchEdit:
                startActivity(new Intent(mActivity, SearchActivity.class));
                break;

            case R.id.tvTopFeatures:
                tvBestValue.setTextColor(Color.parseColor("#bfc2c2"));
                tvAll.setTextColor(Color.parseColor("#bfc2c2"));
                tvTopFeatures.setTextColor(Color.parseColor("#000000"));
                displayView(1);
                break;

            case R.id.tvAll:
                tvBestValue.setTextColor(Color.parseColor("#bfc2c2"));
                tvTopFeatures.setTextColor(Color.parseColor("#bfc2c2"));
                tvAll.setTextColor(Color.parseColor("#000000"));
                startActivity(new Intent(mActivity, CategoryActivity.class));
                return;

            case R.id.iv_menu:
                if (scrollView.getVisibility() == View.VISIBLE) {
                    ivDrop.setVisibility(View.GONE);//scrollView.setVisibility(View.GONE);
                    view.setVisibility(View.GONE);
                    AppUtils.collapse(scrollView);
                    break;
                }
                ivDrop.setVisibility(View.VISIBLE);
                view.setVisibility(View.VISIBLE);
                AppUtils.expand(scrollView);
                break;
            case R.id.searchmain:
/*                if (scrollView.getVisibility() == View.VISIBLE) {
                    ivDrop.setVisibility(View.GONE);
                    view.setVisibility(View.GONE);
                    AppUtils.collapse(scrollView);
                    break;
                }
               aiogrocerymartdev@gmail.com
               groxerymartdev
                */
                startActivity(new Intent(mActivity, SearchActivity.class));
                break;
            case R.id.rlSearch:
                startActivity(new Intent(mActivity, SearchActivity.class));
                break;
            case R.id.ivAll:
                startActivity(new Intent(mActivity, CategoryActivity.class));
                break;
            case R.id.relativeLayoutSearch:
                startActivity(new Intent(getBaseContext(), SearchActivity.class));
                break;

            case R.id.llCategory:
                tvHome.setTextColor(Color.parseColor("#9d9b9b"));
                tvCategory.setTextColor(Color.parseColor("#155b72"));
                tvFavourites.setTextColor(Color.parseColor("#9d9b9b"));
                tvProfile.setTextColor(Color.parseColor("#9d9b9b"));
                tvNotification.setTextColor(Color.parseColor("#9d9b9b"));
                ivHome.setImageResource(R.drawable.ic_home_grey);
                //ivCategory.setImageResource(R.drawable.ic_categories_darkgreen);
                ivFavourites.setImageResource(R.drawable.ic_heart_grey);
                ivNotification.setImageResource(R.drawable.ic_notifications_grey);
                ivProfile.setImageResource(R.drawable.ic_profile_grey);

                startActivity(new Intent(getBaseContext(), CategoryActivity.class));

                break;

            case R.id.llFavourites:

                if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else if (AppSettings.getString(AppSettings.verified).contains("0")) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else {

                    tvHome.setTextColor(Color.parseColor("#9d9b9b"));
                    tvCategory.setTextColor(Color.parseColor("#9d9b9b"));
                    tvFavourites.setTextColor(Color.parseColor("#155b72"));
                    tvProfile.setTextColor(Color.parseColor("#9d9b9b"));
                    tvNotification.setTextColor(Color.parseColor("#9d9b9b"));
                    ivHome.setImageResource(R.drawable.ic_home_grey);
                    //ivCategory.setImageResource(R.drawable.ic_categories_grey);
                    ivFavourites.setImageResource(R.drawable.ic_heart_darkgreen);
                    ivNotification.setImageResource(R.drawable.ic_notifications_grey);
                    ivProfile.setImageResource(R.drawable.ic_profile_grey);
                    startActivity(new Intent(getBaseContext(), FavouriteListActivity.class));
                }

                break;

            case R.id.llNotification:
                tvHome.setTextColor(Color.parseColor("#9d9b9b"));
                tvCategory.setTextColor(Color.parseColor("#9d9b9b"));
                tvFavourites.setTextColor(Color.parseColor("#9d9b9b"));
                tvProfile.setTextColor(Color.parseColor("#9d9b9b"));
                tvNotification.setTextColor(Color.parseColor("#155b72"));
                ivHome.setImageResource(R.drawable.ic_home_grey);
                //ivCategory.setImageResource(R.drawable.ic_categories_grey);
                ivFavourites.setImageResource(R.drawable.ic_heart_grey);
                ivNotification.setImageResource(R.drawable.ic_notifications_green);
                ivProfile.setImageResource(R.drawable.ic_profile_grey);

                startActivity(new Intent(getBaseContext(), NotificationActivity.class));

                break;

            case R.id.llProfile:

                tvHome.setTextColor(Color.parseColor("#9d9b9b"));
                tvCategory.setTextColor(Color.parseColor("#9d9b9b"));
                tvFavourites.setTextColor(Color.parseColor("#9d9b9b"));
                tvNotification.setTextColor(Color.parseColor("#9d9b9b"));
                tvProfile.setTextColor(Color.parseColor("#155b72"));
                ivHome.setImageResource(R.drawable.ic_home_grey);
                //ivCategory.setImageResource(R.drawable.ic_categories_grey);
                ivFavourites.setImageResource(R.drawable.ic_heart_grey);
                ivProfile.setImageResource(R.drawable.ic_profile_darkgreen);
                ivNotification.setImageResource(R.drawable.ic_notifications_grey);

                if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else if (AppSettings.getString(AppSettings.verified).contains("0")) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else {
                    startActivity(new Intent(getBaseContext(), AccountActivity.class));
                }

                break;

            case R.id.rr_profile:
                ivDrop.setVisibility(View.GONE);
                view.setVisibility(View.GONE);
                AppUtils.collapse(scrollView);
                startActivity(new Intent(getBaseContext(), LoginActivity.class));
                if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else if (AppSettings.getString(AppSettings.verified).contains("0")) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else {
                    startActivity(new Intent(getBaseContext(), AccountActivity.class));
                }

                break;


            case R.id.ivCart:

                if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else if (AppSettings.getString(AppSettings.verified).contains("0")) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else {
                    startActivity(new Intent(getBaseContext(), CartListActivity.class));
                }
                break;
            case R.id.iv_logo:
                //openWhatsApp(mActivity);
                finish();
                break;

            case R.id.rr_logout:

                ivDrop.setVisibility(View.GONE);
                view.setVisibility(View.GONE);
                AppUtils.collapse(scrollView);

                if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else if (AppSettings.getString(AppSettings.verified).contains("0")) {
                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                } else {
                    AlertPopUp();
                }

                return;


            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {

//        if (doubleBackToExitPressedOnce) {
//            super.onBackPressed();
//            return;
//        }
        //this.doubleBackToExitPressedOnce = true;
        //Toast.makeText(this, "Press twice to exit", Toast.LENGTH_SHORT).show();

        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        displayView(0);

        try {
            int count = Integer.parseInt(AppSettings.getString(AppSettings.cartCount));
            tvNotification.setTextColor(getResources().getColor(R.color.dark_grey));
            if (AppSettings.getString(AppSettings.userId).isEmpty()) {
            } else {
                //  getCartListApi();

            }
            if (count > 0) {
                tvCount.setVisibility(View.VISIBLE);
                tvCount.setText(String.valueOf(count));
                Log.v("value2", String.valueOf(count));
            } else {
                tvCount.setVisibility(View.GONE);
            }
        } catch (NumberFormatException ex) { // handle your exception
            tvCount.setVisibility(View.GONE);
        }
       /* if(AppSettings.getString(AppSettings.userId).isEmpty())
        {
            tvLogin.setText("LoginActivity");
        }
        else if(AppSettings.getString(AppSettings.verified).equals("1"))
        {
            tvLogin.setText("Logout");
        }
        else
        {
            tvLogin.setText("LoginActivity");
        }*/
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(mActivity).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));
        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(mActivity).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());

        //GetDashboardListApi();
        //onSomeButtonClick();
        //statusCheck();
    }

    @Override
    protected void onPause() {
        //LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    //AlertPopUp
    private void AlertPopUp() {
        final Dialog dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alertyesno);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        //TextView
        TextView tvYes = dialog.findViewById(R.id.tvOk);
        TextView tvCancel = dialog.findViewById(R.id.tvcancel);
        TextView tvReason = dialog.findViewById(R.id.textView22);
        TextView tvAlertMsg = dialog.findViewById(R.id.tvAlertMsg);

        tvAlertMsg.setText("Confirmation");
        tvReason.setText("You will log out from the device. Are you sure?");
        dialog.show();

        tvYes.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                AppSettings.putString(AppSettings.mobile, "");
                AppSettings.putString(AppSettings.verified, "");
                AppSettings.putString(AppSettings.userId, "");
                AppSettings.putString(AppSettings.cartCount, "0");
                //  onResume();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    //==================================API=================================================//
    private void getCartListApi() {
        AppUtils.hideSoftKeyboard(mActivity);
        // AppUtils.showRequestDialog(mActivity);
        Log.v("getCartListApi", AppUrls.getCart);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json.put(AppConstants.result, json_data);
            Log.v("getCartListApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.getCart)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsecarttdata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsecarttdata(JSONObject response) {
        cartList.clear();
        AppSettings.putString(AppSettings.cartCount, "0");
        Log.d("getCartListApi", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                JSONArray productArray = jsonObject.getJSONArray("cart_product");
                AppSettings.putString(AppSettings.cartCount, String.valueOf(productArray.length()));
                cartList.clear();
                if (productArray.length() > 0) {
                    tvCount.setVisibility(View.VISIBLE);
                    tvCount.setText(String.valueOf(productArray.length()));
                    tvCount.setText(String.valueOf(productArray.length()));
                    AppSettings.putString(AppSettings.cartPoint, tvCount.getText().toString());
                    tvCount.setVisibility(View.VISIBLE);
                    Log.v("jjdj", String.valueOf(productArray.length()));
                    for (int i = 0; i < productArray.length(); i++) {
                        JSONObject cartJson = productArray.getJSONObject(i);
                        HashMap<String, String> hashMap = new HashMap();
                        hashMap.put("outofstock", cartJson.getString("outofstock"));
                        hashMap.put("product_id", cartJson.getString("product_id"));
                        hashMap.put("product_name", cartJson.getString("product_name"));
                        hashMap.put("price", cartJson.getString("price"));
                        hashMap.put("product_image", cartJson.getString("product_image"));
                        hashMap.put("quantity", cartJson.getString("quantity"));
                        hashMap.put("discount_applied_in", cartJson.getString("discount_applied_in"));
                        hashMap.put("final_price", cartJson.getString("final_price"));
                        hashMap.put("product_discount_type", cartJson.getString("product_discount_type"));
                        hashMap.put("product_discount_amount", cartJson.getString("product_discount_amount"));
                        cartList.add(hashMap);
                    }

                } else {
                    tvCount.setVisibility(View.GONE);
                }
            } else {
                tvCount.setVisibility(View.GONE);
                AppSettings.putString(AppSettings.cartCount, "0");
            }
        } catch (Exception e) {
            tvCount.setVisibility(View.GONE);
            AppSettings.putString(AppSettings.cartCount, "0");
        }
        AppUtils.hideDialog();
        /*if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
            getProfileApi();
        } else {
            AppUtils.showErrorMessage(tvMain, getString(R.string.errorInternet), mActivity);
        }*/
    }

    private void addFavApi(String productId) {
        AppUtils.showRequestDialog(mActivity);
        Log.v("addFavApi", AppUrls.wishList);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json_data.put("product_master_id", productId);

            json.put(AppConstants.result, json_data);

            Log.v("addFavApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.wishList)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        parseFavdata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseFavdata(JSONObject response) {
        Log.d("response ", response.toString());

        DatabaseController.removeTable(TableFavourite.favourite);

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                if (jsonObject.getString("status").equals("1")) {
                    AppUtils.showErrorMessage(tvMain, "Product Added to Favourites", mActivity);
                } else if (jsonObject.getString("status").equals("2")) {
                    AppUtils.showErrorMessage(tvMain, "Product Removed from Favourites", mActivity);
                }

                JSONArray wishArray = jsonObject.getJSONArray("wish_list_product");

                for (int i = 0; i < wishArray.length(); i++) {

                    JSONObject productobject = wishArray.getJSONObject(i);
                    ContentValues mContentValues = new ContentValues();
                    mContentValues.put(TableFavourite.favouriteColumn.productId.toString(), productobject.getString("product_id"));
                    DatabaseController.insertData(mContentValues, TableFavourite.favourite);
                }

            } else {
                AppUtils.showErrorMessage(tvMain, "Product Removed from Favourites", mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();

    }

    private void getProfileApi() {

        AppUtils.showRequestDialog(mActivity);

        Log.v("getProfileApi", AppUrls.getUserDetail);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("user_id", AppSettings.getString(AppSettings.userId));

            json.put(AppConstants.result, json_data);

            Log.v("getProfileApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.getUserDetail)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseProfiledata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseProfiledata(JSONObject response) {

        Log.d("response ", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                JSONObject userObject = jsonObject.getJSONObject("user_detail");
                AppSettings.putString(AppSettings.name, userObject.getString("username"));
                AppSettings.putString(AppSettings.email, userObject.getString("email_id"));
                AppSettings.putString(AppSettings.gender, userObject.getString("gender"));
                AppSettings.putString(AppSettings.mobile, userObject.getString("phone_no"));
                AppSettings.putString(AppSettings.wallet, userObject.getString("wallet_amount"));
                AppSettings.putString(AppSettings.referralId, userObject.getString("referel_id"));

            } else {
                AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
        AppUtils.hideDialog();

        // onResume();

        if (AppSettings.getString(AppSettings.fcmSend).equals("0") || AppSettings.getString(AppSettings.fcmSend).isEmpty()) {
            if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                //sendFCMApi();
            } else {
                AppUtils.showErrorMessage(tvMain, getString(R.string.errorInternet), mActivity);
            }
        }
    }

    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("regId", null);
        Log.e("Firebase reg id: ", regId);
        if (!TextUtils.isEmpty(regId)) {            // txtRegId.setText("Firebase Reg Id: " + regId);
            Log.v("firebaseid", "Firebase Reg Id: " + regId);
        } else {
            Log.v("not_received", "Firebase Reg Id is not received yet!");
        }
    }

    private void AddToCartApi(String productId, int position) {
        AppUtils.showRequestDialog(mActivity);
        AppUtils.hideSoftKeyboard(mActivity);
        Log.v("AddToCartApi", AppUrls.addToCart);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("product_id", productId);
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json.put(AppConstants.result, json_data);
            Log.v("AddToCartApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.addToCart)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        parseCartdata(response, position);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            //  AppUtils.showErrorMessage(tvDeatils, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            //  AppUtils.showErrorMessage(tvDeatils, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseCartdata(JSONObject response, int position) {

        Log.d("responseBOB", response.toString());

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {

                getCartListApi();
                //recreate();

               /* HashMap<String,String> hm=new HashMap<>();
                hm.put("id", FeaturedProductList.get(position).get("id"));
                hm.put("category_id", FeaturedProductList.get(position).get("category_master_id"));
                hm.put("sub_category_master_id", FeaturedProductList.get(position).get("sub_category_master_id"));
                hm.put("product_name", FeaturedProductList.get(position).get("product_name"));
                hm.put("product_image", FeaturedProductList.get(position).get("image"));
                hm.put("price", FeaturedProductList.get(position).get("price"));
                hm.put("final_price", FeaturedProductList.get(position).get("final_price"));
                hm.put("weight", FeaturedProductList.get(position).get("weight"));
                hm.put("unit_name", FeaturedProductList.get(position).get("unit_name"));
                hm.put("product_discount_type", FeaturedProductList.get(position).get("product_discount_type"));
                hm.put("quantity", FeaturedProductList.get(position).get("quantity"));
                hm.put("product_discount_amount", FeaturedProductList.get(position).get("product_discount_amount"));
                hm.put("sub_product", FeaturedProductList.get(position).get("sub_product"));
             //   JSONArray newsliderarray =new JSONArray(FeaturedProductList.get(position).get("sub_product")) ;
               *//* for (int j = 0; j < newsliderarray.length(); j++) {
                    JSONObject sliderObject = newsliderarray.getJSONObject(j);
                    HashMap<String, String> sliderMap = new HashMap<>();
                    if (j == 0) {
                        hm.put("SpinerValue", sliderBannerList.get(position).get("sub_weight").toString() + sliderObject.get("unit_name").toString());
                        hm.put("SpinerValueId", sliderBannerList.get(position).get("sub_id").toString());
                    }*//*

                hm.put("sub_id", sliderBannerList.get(position).get("sub_id").toString());
                hm.put("dis_type", sliderBannerList.get(position).get("dis_type").toString());
                hm.put("per_amount", sliderBannerList.get(position).get("per_amount").toString());
                hm.put("sub_weight", sliderBannerList.get(position).get("sub_weight").toString());
                hm.put("unit_name", sliderBannerList.get(position).get("unit_name").toString());
                hm.put("actual_p", sliderBannerList.get(position).get("actual_p").toString());
                hm.put("price", sliderBannerList.get(position).get("price").toString());
                hm.put("parentId", FeaturedProductList.get(position).get("id"));
                hm.put("cart_quantity", sliderBannerList.get(position).get("cart_quantity"));
                hm.put("cartId", jsonObject.getString("cartId"));
                sliderBannerList.set(position,hm);
                 //   Log.v("dshf", sliderObject.get("dis_type").toString());

            //    }



              //  hm.put("cartId",jsonObject.getString("cartId"));


                FeaturedProductList.set(position,hm);

                offerAdapter.notifyDataSetChanged();*/


                AppUtils.showErrorMessage(tvCat1, String.valueOf(jsonObject.getString("res_msg")), mActivity);


            } else {
                // startActivity(new Intent(mActivity, CartListActivity.class));
                AppUtils.showErrorMessage(tvCat1, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            // AppUtils.showErrorMessage(tvDeatils, String.valueOf(e), this.mActivity);
        }
        AppUtils.hideDialog();
    }

    private void UpdateCartApi(String quantity, String ProductId) {

        Log.v("AddToCartApi", AppUrls.updateCart);

        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("product_id", ProductId);
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json_data.put("quantity", quantity);
            json.put(AppConstants.result, json_data);
            Log.v("AddToCartApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.updateCart)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedata(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedata(JSONObject response) {
        Log.d("response", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
                // cartList.clear();
                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    getCartListApi();
                } else {
                    AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                }
            } else {
                AppUtils.hideDialog();
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.hideDialog();
            AppUtils.showErrorMessage(recyclerView, String.valueOf(e), mActivity);
        }
    }

    private void DeleteCartApi(String cartId) {
        AppUtils.showRequestDialog(mActivity);
        AppUtils.hideSoftKeyboard(mActivity);
        Log.v("AddToCartApi", AppUrls.deleteFromCart);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();
        try {
            json_data.put("cart_id", cartId);
            json_data.put("user_master_id", AppSettings.getString(AppSettings.userId));
            json.put(AppConstants.result, json_data);
            Log.v("AddToCartApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(AppUrls.deleteFromCart)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parsedeletedata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(recyclerView, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parsedeletedata(JSONObject response) {
        Log.d("response ", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
                AppUtils.hideDialog();
                //cartList.clear();
                if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                    getCartListApi();
                } else {
                    AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                }
            } else {
                AppUtils.hideDialog();
                AppUtils.showErrorMessage(recyclerView, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.hideDialog();
            AppUtils.showErrorMessage(recyclerView, String.valueOf(e), mActivity);
        }

    }


    private void sendFCMApi() {

        Log.v("sendFCMApi", AppUrls.notificationMaster);

        AppUtils.showRequestDialog(mActivity);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            json_data.put("device_id", AppUtils.getDeviceID(mActivity));
            json_data.put("fcm_id", AppSettings.getString(AppSettings.fcmId));
            json.put(AppConstants.result, json_data);

            Log.v("sendFCMApi", json.toString());

        } catch (JSONException e) {
            e.printStackTrace();
            Log.v("printStack", String.valueOf(e));
        }

        AndroidNetworking.post(AppUrls.notificationMaster)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseSendFCMdata(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        // handle error
                        if (error.getErrorCode() != 0) {
                            //AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            //AppUtils.showErrorMessage(tvMain, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseSendFCMdata(JSONObject response) {

        Log.d("response ", response.toString());
        AppUtils.hideDialog();
        /*  AppSettings.putString(AppSettings.fcmSend,"0");*/

        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                /* AppSettings.putString(AppSettings.fcmSend,"1");*/
            } else {
                // AppUtils.showErrorMessage(tvMain, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            //AppUtils.showErrorMessage(tvMain, String.valueOf(e), mActivity);
        }
    }

    public void dropdownList(final String units, final String quantity, View v, final String cartId, final TextView spVal, final TextView price, final TextView finalprice, final TextView tvDiscount, final LinearLayout llCartCountBtn, final LinearLayout llCartBtn, final TextView tvQuantityCount, int position)
    {
        Context wrapper = new ContextThemeWrapper(mActivity, R.style.PopupMenuOverlapAnchor);
        PopupMenu popup = new PopupMenu(wrapper, v);
        HashMap<String, String> hashMap = new HashMap<>();
        popup.getMenu().clear();
        try {
            for (int k = 0; k < sliderBannerList.size(); k++) {
                if (cartId.equalsIgnoreCase(sliderBannerList.get(k).get("parentId"))) {
                    popup.getMenu().add(k, k, k, sliderBannerList.get(k).get("sub_weight") + sliderBannerList.get(k).get("unit_name"));
                    hashMap.put("sub_id", sliderBannerList.get(k).get("sub_id"));
                    hashMap.put("aprice", sliderBannerList.get(k).get("actual_p"));
                    hashMap.put("price", sliderBannerList.get(k).get("price"));
                    hashMap.put("per_amount", sliderBannerList.get(k).get("per_amount"));
                    hashMap.put("dis_type", sliderBannerList.get(k).get("dis_type"));
                    hashMap.put("cart_quantity", sliderBannerList.get(k).get("cart_quantity"));
                    hashMapArrayList.add(hashMap);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_main, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(item -> {
            int id = item.getItemId();
            Log.v("MenuitemIdSelected", String.valueOf(id));
            final String idChoosen = sliderBannerList.get(item.getOrder()).get("sub_id");
            llCartBtn.setVisibility(View.VISIBLE);
            llCartCountBtn.setVisibility(View.GONE);
            Log.v("CartListCheck", cartList.toString());
            Log.v("CartListCheck", "idChoosen " + idChoosen);
            tvQuantityCount.setText("1");
            int c = 0;
            for (int i = 0; i < cartList.size(); i++) {
                if (idChoosen.equalsIgnoreCase(cartList.get(i).get("product_id"))) {
                    Log.v("CartListCheck", "sId " + cartList.get(i).get("product_id"));
                    tvQuantityCount.setText(cartList.get(i).get("quantity"));
                    llCartBtn.setVisibility(View.GONE);
                    llCartCountBtn.setVisibility(View.VISIBLE);
                    c++;
                }
            }
            if (c == 0) {
                tvQuantityCount.setText("1");
            }
            llCartBtn.setOnClickListener(v1 -> {
                if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                    Toast.makeText(mActivity, "Please Login to continue", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(mActivity, LoginActivity.class));
                } else {
                    llCartBtn.setVisibility(View.GONE);
                    llCartCountBtn.setVisibility(View.VISIBLE);
                    AddToCartApi(idChoosen, position);
                }
            });

            finalprice.setText(getString(R.string.rs) + sliderBannerList.get(item.getOrder()).get("actual_p"));
            /*spVal.setText( item.getTitle().toString() );*/
            spVal.setText(item.getTitle().toString());

            if (sliderBannerList.get(item.getOrder()).get("per_amount").equals("0.00")) {
                tvDiscount.setVisibility(View.INVISIBLE);
                price.setText(getString(R.string.rs) + sliderBannerList.get(item.getOrder()).get("price"));
            } else {
                tvDiscount.setVisibility(View.VISIBLE);
                tvDiscount.setText(getString(R.string.rs) + sliderBannerList.get(item.getOrder()).get("per_amount") + " off");
            }
            if (sliderBannerList.get(item.getOrder()).get("dis_type").equals("123")) {
                price.setVisibility(View.GONE);
            } else {
                price.setVisibility(View.VISIBLE);
                price.setText(getString(R.string.rs) + sliderBannerList.get(item.getOrder()).get("price"));
            }
            //ToDo..
            Subid = sliderBannerList.get(item.getOrder()).get("sub_id");
            Log.v("subid", Subid);
            return true;
        });
    }

    public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.OfferHolder> {
        public ArrayList<String> arrlistJobName = new ArrayList<String>();
        ArrayList<HashMap<String, String>> data = new ArrayList();
        ArrayList<HashMap<String, String>> data2 = new ArrayList();
        ArrayList<HashMap<String, String>> sliderMapArray = new ArrayList<HashMap<String, String>>();
        List<product_master> gff;
        int count;

        public OfferAdapter(ArrayList<HashMap<String, String>> FeaturedProductList, ArrayList<HashMap<String, String>> subProductListArrayList, ArrayList<HashMap<String, String>> BannerListArrayList)
        {
            data = FeaturedProductList;
            data2 = subProductListArrayList;
            sliderMapArray = BannerListArrayList;
        }

        @Override
        public OfferHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_product, parent, false);
            return new OfferHolder(itemView);
        }

        public void onBindViewHolder(final OfferHolder holder, final int position) {
            try {
                if (!data2.get(position).get("cart_quantity").equalsIgnoreCase("")) {
                    count = Integer.parseInt(data2.get(position).get("cart_quantity"));
                    holder.llCartCountBtn.setVisibility(View.VISIBLE);
                    holder.llCartBtn.setVisibility(View.GONE);
                    holder.tvQuantityCount.setText(count + "");
                } else {
                    count = 1;
                    holder.llCartCountBtn.setVisibility(View.GONE);
                    holder.llCartBtn.setVisibility(View.VISIBLE);
                }

                holder.tvName.setText((CharSequence) ((HashMap) data.get(position)).get("product_name"));
                if (((HashMap) data.get(position)).get("product_discount_type").equals("1")) {
                    holder.tvPrice.setVisibility(View.VISIBLE);
                    holder.tvDiscount.setText(getString(R.string.rs) + " " + ((HashMap) data.get(position)).get("product_discount_amount") + "  /-off");
                } else if (((HashMap) data.get(position)).get("product_discount_type").equals("2")) {
                    holder.tvPrice.setVisibility(View.VISIBLE);
                    holder.tvDiscount.setText(getString(R.string.rs) + " " + ((HashMap) data.get(position)).get("product_discount_amount") + "% off");
                } else if (data.get(position).get("product_discount_type").equals("123")) {
                    // holder.tvDiscount.setText("");
                    holder.tvDiscount.setVisibility(View.INVISIBLE);
                    holder.tvPrice.setVisibility(View.GONE);
                }
                if (((String) ((HashMap) data.get(position)).get("final_price")).equalsIgnoreCase((String) ((HashMap) data.get(position)).get("price"))) {
                    holder.tvFPrice.setText(getString(R.string.rs) + " " + ((HashMap) data.get(position)).get("final_price"));
                    holder.tvDiscount.setText("");
                    holder.tvDiscount.setVisibility(View.INVISIBLE);
                    holder.tvPrice.setText("");
                } else {
                    holder.tvFPrice.setText(getString(R.string.rs) + " " + ((HashMap) data.get(position)).get("final_price"));
                    holder.tvPrice.setText(getString(R.string.rs) + " " + ((HashMap) data.get(position)).get("price"));
                    //  holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.ANTI_ALIAS_FLAG);
                    holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | 16);
                }
                try {
                    if (((HashMap) data.get(position)).get("product_image") != null && !((String) ((HashMap) data.get(position)).get("product_image")).isEmpty()) {
                        Picasso.with(mActivity).load((String) ((HashMap) data.get(position)).get("product_image")).into(holder.ivPic);
                    }
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                if (DatabaseController.checkRecordExist(TableFavourite.favourite, String.valueOf(TableFavourite.favouriteColumn.productId), data.get(position).get("id"))) {
                    holder.icWishlist.setImageResource(R.drawable.ic_heart_red);
                } else {
                    holder.icWishlist.setImageResource(R.drawable.ic_heart_grey);
                }

                holder.icWishlist.setOnClickListener(view -> {
                    if (!AppSettings.getString(AppSettings.userId).isEmpty()) {
                        if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                            addFavApi(data.get(position).get("id"));
                        } else {
                            AppUtils.showErrorMessage(tvMain, getString(R.string.errorInternet), mActivity);
                        }
                    } else {
                        AppUtils.showErrorMessage(tvMain, "Kindly login first", mActivity);
                    }
                });

                holder.itemView.setOnClickListener(view -> {
                    Intent mIntent = new Intent(mActivity, ProductDetailsActivity.class);
                    mIntent.putExtra(AppConstants.productId, data.get(position).get("id"));
                    mIntent.putExtra(AppConstants.from, "3");
                    mIntent.putExtra(AppConstants.CartQuantityValue, "6");
                    mIntent.putExtra("data", data2.get(position).toString());
                    mIntent.putExtra("data3", sliderBannerList.toString());
                    startActivity(mIntent);
                });


                try
                {
                    if(Constnt.prodcutID.equalsIgnoreCase(data.get(position).get("id")))
                    {
                        Constnt.prodcutID = "";
                        Intent mIntent = new Intent(mActivity, ProductDetailsActivity.class);
                        mIntent.putExtra(AppConstants.productId, data.get(position).get("id"));
                        mIntent.putExtra(AppConstants.from, "3");
                        mIntent.putExtra(AppConstants.CartQuantityValue, "6");
                        mIntent.putExtra("data", data2.get(position).toString());
                        mIntent.putExtra("data3", sliderBannerList.toString());
                        startActivity(mIntent);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                holder.llCartBtn.setOnClickListener(view -> {
                    if (AppSettings.getString(AppSettings.userId).isEmpty()) {
                        Toast.makeText(mActivity, "Please Login to continue", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(mActivity, LoginActivity.class));
                    } else {
                        holder.llCartCountBtn.setVisibility(View.VISIBLE);
                        holder.llCartBtn.setVisibility(View.GONE);
                        AddToCartApi(data.get(position).get("id"), position);
                    }
                });


                holder.tvSpinner.setText(data.get(position).get("SpinerValue"));
                Log.v("ItemProduct", data.get(position).toString());

                for (int i = 0; i < cartList.size(); i++) {
                    if (data.get(position).get("SpinerValueId").equalsIgnoreCase(cartList.get(i).get("product_id"))) {
                        Log.v("CartListCheck", "sId " + cartList.get(i).get("product_id"));
                        holder.tvQuantityCount.setText(cartList.get(i).get("quantity"));
                        holder.llCartBtn.setVisibility(View.GONE);
                        holder.llCartCountBtn.setVisibility(View.VISIBLE);
                    }
                }

                holder.tvSpinner.setOnClickListener(view -> {
                    //this method opens subProduct in dropdown
                    dropdownList(data.get(position).get("sub_weight"), data.get(position).get("cart_quantity"), view, data.get(position).get("id"), holder.tvSpinner, holder.tvPrice, holder.tvFPrice, holder.tvDiscount, holder.llCartCountBtn, holder.llCartBtn, holder.tvQuantityCount, position);

                    count = (Integer.parseInt(holder.tvQuantityCount.getText().toString()));
                    holder.tvQuantityCount.setText(String.valueOf(count));
                    Log.v("CountValue", String.valueOf(count));

                });

                AppSettings.putString(AppSettings.subiid, data2.get(position).get("sub_id"));
                holder.imageView3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        count = (Integer.parseInt(holder.tvQuantityCount.getText().toString())) - 1;
                        if (count == 0) {
                            Vibrator vibe = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
                            vibe.vibrate(100);
                            holder.llCartCountBtn.setVisibility(View.GONE);
                            holder.llCartBtn.setVisibility(View.VISIBLE);
                            String dd = "714";

                            DeleteCartApi(data2.get(position).get("cartId"));
                        } else if (count > 0) {
                            Vibrator vibe = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
                            vibe.vibrate(100);
                            if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                                UpdateCartApi(String.valueOf(count), data2.get(position).get("sub_id"));
                                holder.tvQuantityCount.setText((count) + "");
                            } else {
                                AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                            }
                        }
                    /*else if (count == 1) {
                            DeleteCartApi( count );
                            AppUtils.showErrorMessage( recyclerView, getString( R.string.minusQuantityError ), mActivity );
                        }*/

                    }
                  /*  int quant = Integer.parseInt( data2.get( position ).get( "quantity" ) );
                    if (quant > 1) {
                        quant = quant - 1;
                        Vibrator vibe = (Vibrator) mActivity.getSystemService( Context.VIBRATOR_SERVICE );
                        vibe.vibrate( 100 );
                        if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                            UpdateCartApi( String.valueOf( quant ), data2.get( position ).get( "sub_id" ), position );
                        } else {
                            AppUtils.showErrorMessage( recyclerView, getString( R.string.errorInternet ), mActivity );
                        }
                    } else {
                        DeleteCartApi( data2.get( position ).get( "sub_id" ) );
                        AppUtils.showErrorMessage( recyclerView, getString( R.string.minusQuantityError ), mActivity );
                    }*/

                   /* int quant = (Integer.parseInt( holder.tvQuantityCount.getText().toString() ));
                    if (quant > 1) {
                        quant = quant - 1;
                        Vibrator vibe = (Vibrator) mActivity.getSystemService( Context.VIBRATOR_SERVICE );
                        vibe.vibrate( 100 );

                        if (SimpleHTTPConnection.isNetworkAvailable( mActivity )) {
                            UpdateCartApi( String.valueOf( quant ), data.get( position ).get( "product_id" ) );
                        } else {
                            AppUtils.showErrorMessage( recyclerView, getString( R.string.errorInternet ), mActivity );
                        }
                    } else {
                      //  DeleteCartApi( data.get( position ).get( "cart_id" ) );
                        DeleteCartApi(quant );
                        AppUtils.showErrorMessage( recyclerView, getString( R.string.minusQuantityError ), mActivity );
                    }

*/

                });
                holder.imageView2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        count = (Integer.parseInt(holder.tvQuantityCount.getText().toString())) + 1;
                        if (count <= Integer.parseInt(data.get(position).get("quantity"))) {
                            // ((DashBoardFragment) getActivity()).tvCount.setText(Integer.toString(count));
                            AppSettings.putString(AppSettings.cartValue, tvCount.getText().toString());
                            //count = count + 1;
                            Vibrator vibe = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
                            vibe.vibrate(100);
                            if (SimpleHTTPConnection.isNetworkAvailable(mActivity)) {
                                UpdateCartApi(String.valueOf(count), data2.get(position).get("sub_id"));
                                holder.tvQuantityCount.setText((count) + "");
                            } else {
                                AppUtils.showErrorMessage(recyclerView, getString(R.string.errorInternet), mActivity);
                            }
                        } else {

                            AppUtils.showErrorMessage(recyclerView, getString(R.string.addQuantityError) + " " + data.get(position).get("quantity"), mActivity);
                        }
                    }
                });
            } catch (Exception e) {

            }
        }

        public int getItemCount() {
            return data.size();
        }

        private class OfferHolder extends RecyclerView.ViewHolder {
            ImageView ivPic, icWishlist, imageView2, imageView3;
            LinearLayout linearLayout, llCartBtn, llCartCountBtn;
            TextView tvDiscount;
            TextView tvFPrice;
            TextView tvName;
            TextView tvPrice;
            TextView tvSpinner, tvQuantityCount;
            Spinner spinners;

            RelativeLayout btnSpinner;

            public OfferHolder(View itemView) {
                super(itemView);
                linearLayout = itemView.findViewById(R.id.layout_item);
                tvQuantityCount = itemView.findViewById(R.id.tvQuantityCount);
                spinners = itemView.findViewById(R.id.spinners);
                ivPic = itemView.findViewById(R.id.ivProduct);
                icWishlist = itemView.findViewById(R.id.ic_wishlist);
                tvName = itemView.findViewById(R.id.tvProductName);
                tvSpinner = itemView.findViewById(R.id.tvSpinner);
                //plus minus btn in add to cart
                llCartCountBtn = itemView.findViewById(R.id.llCartCountBtn);
                //add to cart btn in add to cart
                llCartBtn = itemView.findViewById(R.id.llCartBtn);
                tvFPrice = itemView.findViewById(R.id.tvFPrice);
                tvPrice = itemView.findViewById(R.id.tvPrice);
                tvDiscount = itemView.findViewById(R.id.tvDiscount);
                btnSpinner = itemView.findViewById(R.id.btnSpinner);
                imageView2 = itemView.findViewById(R.id.imageView2);
                imageView3 = itemView.findViewById(R.id.imageView3);

                closeKeyboard();
            }

        }


    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {


            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (currentPage == NUM_PAGES) {

                        currentPage = 0;
                    }
                    mViewPager.setCurrentItem(currentPage++, true);
                    // mViewPager.setCurrentItem((mViewPager.getCurrentItem() + 1) % TopBannerList.size());

                    // mViewPager.post(new MyTimerTask());
                }
            });
        }

    }


    public void onSomeButtonClick() {
        if (!permissionsGranted()) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 123);
        } else {
            statusCheck();
        }
    }

    private Boolean permissionsGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            try {
                alert.dismiss();
            } catch (Exception e) {
            }
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        alert = builder.create();
        alert.show();
    }


    private void GetCatListApi() {
        //AppUtils.showRequestDialog(mActivity);
        Log.v("GetCatListApi", AppUrls.getCategory);
        JSONObject json = new JSONObject();
        JSONObject json_data = new JSONObject();

        try {
            //json_data.put("timestamp", AppSettings.getString(AppSettings.categoryTime));
            json_data.put("timestamp", "");
            json.put(AppConstants.result, json_data);
            Log.v("GetCatListApi", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(AppUrls.getCategory)
                .addJSONObjectBody(json)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseJsondata1(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvCat4, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvCat4, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseJsondata1(JSONObject response) {
        Log.d("response ", response.toString());
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                JSONArray categoryArray = jsonObject.getJSONArray(TableCategory.category);
                for (int i = 0; i < categoryArray.length(); i++) {
                    ContentValues mContentValues = new ContentValues();
                    JSONObject categoryobject = categoryArray.getJSONObject(i);
                    mContentValues.put(TableCategory.catColumn.category_id.toString(), categoryobject.get("category_id").toString());
                    mContentValues.put(TableCategory.catColumn.category_name.toString(), categoryobject.get("category_name").toString());
                    mContentValues.put(TableCategory.catColumn.featured_category.toString(), categoryobject.get("featured_category").toString());
                    mContentValues.put(TableCategory.catColumn.app_icon.toString(), categoryobject.get("app_icon").toString());
                    mContentValues.put(TableCategory.catColumn.status.toString(), "1");
                    mContentValues.put(TableCategory.catColumn.json.toString(), categoryobject.toString());
                    DatabaseController.insertUpdateData(mContentValues, TableCategory.category, TableCategory.catColumn.category_id.toString(), categoryobject.get("category_id").toString());
                    JSONArray subcategoryArray = categoryobject.getJSONArray(TableSubCategory.sub_category);
                    for (int j = 0; j < subcategoryArray.length(); j++) {
                        mContentValues = new ContentValues();
                        JSONObject subcategoryobject = subcategoryArray.getJSONObject(j);
                        mContentValues.put(TableSubCategory.subCatColumn.category_id.toString(), subcategoryobject.get("category_id").toString());
                        mContentValues.put(TableSubCategory.subCatColumn.sub_category_id.toString(), subcategoryobject.get("sub_category_id").toString());
                        mContentValues.put(TableSubCategory.subCatColumn.sub_category_name.toString(), subcategoryobject.get("sub_category_name").toString());
                        mContentValues.put(TableSubCategory.subCatColumn.icon.toString(), subcategoryobject.get("app_icon").toString());
                        mContentValues.put(TableSubCategory.subCatColumn.status.toString(), "1");
                        mContentValues.put(TableSubCategory.subCatColumn.json.toString(), subcategoryobject.toString());
                        DatabaseController.insertUpdateData(mContentValues, TableSubCategory.sub_category, TableSubCategory.subCatColumn.sub_category_id.toString(), subcategoryobject.get("sub_category_id").toString());
                    }
                }
                getCatData();
            }

            AppSettings.putString(AppSettings.categoryTime, jsonObject.getString("sync_time"));
        } catch (Exception e) {
            AppUtils.showErrorMessage(this.tvCat4, String.valueOf(e), this.mActivity);
        }

        AppUtils.hideDialog();

        if (SimpleHTTPConnection.isNetworkAvailable(this.mActivity)) {
            getSettingApi();
        } else {
            AppUtils.showErrorMessage(this.tvCat4, getString(R.string.error_connection_message), this.mActivity);
        }
    }

    private void getSettingApi() {

        //AppUtils.showRequestDialog(mActivity);

        Log.v("getProfileApi", AppUrls.settings);

        AndroidNetworking.get(AppUrls.settings)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseProfiledata2(response);
                    }

                    @Override
                    public void onError(ANError error) {

                        AppUtils.hideDialog();
                        // handle error
                        if (error.getErrorCode() != 0) {
                            AppUtils.showErrorMessage(tvCat4, String.valueOf(error.getErrorCode()), mActivity);
                            Log.d("onError errorCode ", "onError errorCode : " + error.getErrorCode());
                            Log.d("onError errorBody", "onError errorBody : " + error.getErrorBody());
                            Log.d("onError errorDetail", "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            AppUtils.showErrorMessage(tvCat4, String.valueOf(error.getErrorDetail()), mActivity);
                        }
                    }
                });
    }

    private void parseProfiledata2(JSONObject response) {
        Log.d("response ", response.toString());
        AppUtils.hideDialog();
        try {
            JSONObject jsonObject = response.getJSONObject(AppConstants.responseTag);
            if (jsonObject.getString("res_code").equals("1")) {
                JSONObject userObject = jsonObject.getJSONObject("data");
                AppSettings.putString(AppSettings.min_order_bal, userObject.getString("min_order_bal"));
                AppSettings.putString(AppSettings.shippingAmount, userObject.getString("shipping _amount"));
            } else {
                AppUtils.showErrorMessage(tvCat4, String.valueOf(jsonObject.getString("res_msg")), mActivity);
            }
        } catch (Exception e) {
            AppUtils.showErrorMessage(tvCat4, String.valueOf(e), mActivity);
        }
        AppSettings.putString(AppSettings.cartCount, "0");
        /*startActivity(new Intent(getBaseContext(), DashBoardFragment.class));
        finish();*/
    }

    AlertDialog alert = null;
}
